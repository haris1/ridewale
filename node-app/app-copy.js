const express = require('express');
const app     = express();
const http    = require('http').Server(app);
const io      = require('socket.io')(http);
const _       = require('lodash');
const mysql   = require('mysql');
const Redis   = require('ioredis');
const redis   = new Redis();
const bodyParser  = require('body-parser');
const socketioJwt = require("socketio-jwt");

let database = require('./config/database');
let mysqlConnection = database.connection;
let appConfig = require('./config/config');

/** Body Parser Middleware **/
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//Start Server
http.listen(appConfig.port, ()=> {
  console.log(`${appConfig.environment}`+` server is started on port number ${appConfig.port}`);
});

let drivers = [];
let riders = [];
let serverResopnse;
let serverDrivers;

redis.subscribe('FindTheDriverChannel', 'RideStartedChannel', function (err, count) {
  console.log(`we are subscribed to ${count} channels`);
});

redis.on('message', function (channel, message) {

  // console.log('\x1b[33m', 'Receive message %s from channel %s', message, channel);

  let data       = JSON.parse(message);
  let serverResopnse = data.data.data;
  if (channel == 'FindTheDriverChannel') {

    serverDrivers = serverResopnse.drivers;
    let riderId   = serverResopnse.user_id;
    let tripInfo  = serverResopnse.trip_info;
    letsFindTheDriver(riderId);
  }else if(channel == 'RideStartedChannel'){

    let riderId    = serverResopnse.rider;
    let driverId   = serverResopnse.driver;
    promise1  = findUserSocket(riderId, 'rider');
    promise2  = findUserSocket(driverId, 'driver');
    Promise.all([promise1, promise2]).then((result) => {

      let riderSocket  = result[0];
      let driverSocket = result[1];

      riderSocket.on_ride = true;
      driverSocket.on_ride = true;
    }).catch((error) => {
      exposeTheError("",error.message);
      letsFindTheDriver(riderId, null, driverId);
    });
  }
});

io.sockets.on('connection', 
socketioJwt.authorize({
  secret: 'N0i2jt6daNmhTeSVTwoNvYtmcF0je8Bcfn9GaWa0UUkFYnZ8QAUs82AsUl8bxINS',
  timeout: 10000,
  callback: false
}));

io.on('connection', function(socket) {
  
  let userId    = parseInt(socket.handshake.query.user_id);
  let username  = socket.handshake.query.username;
  let role      = socket.handshake.query.role;
  let latitude  = parseFloat(socket.handshake.query.latitude);
  let longitude = parseFloat(socket.handshake.query.longitude);

  logTheData([username, socket.id, userId, role]);
  if (!userId || !username || !role ||!latitude || !longitude) {
    exposeTheError(socket, 'Error. you dont have passed all required params.');
  }

  /** Assign required options in socket obj START **/
  socket.user_id  = userId;
  socket.username = username;
  socket.role     = role;
  socket.ride_accepted = false;
  socket.on_ride  = false;
  socket.location  = {latitude: latitude, longitude: longitude};
  if (role === 'driver') {
    socket.active_card = false;
    socket.rider = null;
    drivers.push(socket);
    makeTheUserOnline(userId, username, 'driver', socket);
  }else if(role === 'rider'){
    socket.rejected_by_drivers = [];
    socket.not_respoded_drivers = [];
    riders.push(socket);
    makeTheUserOnline(userId, username, 'rider', socket);
  }

  socket.on('tik', (data, callback) => {
    callback({status: 200, message: "Tok"});
  });

  socket.on('updateCarMove', (data, callback) => {

    let latitude  = parseFloat(data.latitude);
    let longitude = parseFloat(data.longitude);
    socket.location = {latitude: latitude, longitude: longitude};

    let rider     = socket.rider;
    if (!rider) {
      return;
    }else{

      let riderId   = rider.id;
      let driverId  = socket.user_id;
      findUserSocket(riderId, 'rider').then((riderSocket) => {
        riderSocket.emit('carMoved', socket.location);
        callback({status: 200, message: "Position updated successfully."});
      }).catch((error) => {
        exposeTheError("",error.message);
        callback({status: 500, message: error.message});
      });
    }
  });

  socket.on('updateLocationInDB', (data, callback) => {

    let latitude  = parseFloat(data.latitude);
    let longitude = parseFloat(data.longitude);
    let driverId  = parseInt(socket.user_id);

    mysqlConnection.query('UPDATE users SET current_latitude = ?, current_longitude = ? WHERE id = ?', [latitude, longitude, driverId], function (error, results, fields) {

      if (error) {
        exposeTheError(socket, error.message);
      }

      if (typeof results !== 'undefined') {

        if (results.changedRows === 1 && results.affectedRows === 1) {
          logTheData(`${socket.role}: ${socket.username}'s db location is updated.`);
          callback({status:200, message: `${socket.role}: ${socket.username}'s db location is updated.`});
        }else if(results.changedRows === 0 && results.affectedRows === 1) {
          logTheData(`${socket.role}: ${socket.username}'s db location already updated.`);
          callback({status:500, message: `${socket.role}: ${socket.username}'s db location already updated.`});
        }else{
          logTheData(`${socket.role}: ${socket.username}'s db info not found.`);
          callback({status:500, message: `${socket.role}: ${socket.username}'s db info not found.`});
        }
      }
    });
  });

  socket.on('acceptTheRequest', (data, callback) => {

    let rider     = socket.rider;
    let riderId   = rider.id;
    let driverId  = socket.user_id;
    let driver    = {id: driverId, name:socket.username, role:socket.role };
    findUserSocket(riderId, 'rider').then((riderSocket) => {

      activateTheRide(driverId).then((driverQueryRsult) => {

        driverInfo = _.find(serverDrivers, { user_id: driverId });
        Promise.all([driverInfo]).then((result) => {

          socket.ride_accepted = true;
          riderSocket.ride_accepted = true;
          riderSocket.emit('driverIsOnRide',{
            driver_location: socket.location, 
            driver_info: result[0], 
            trip_info:serverResopnse.trip_info
          });
          
          socket.emit('riderIsOnRide',{
            rider_location: riderSocket.location, 
            rider_info: rider, 
            trip_info:serverResopnse.trip_info
          });
          callback({status: 200, message: "Request accepted successfully."});
        }).catch((error) => {
          callback({status: 500, message: error.message});
          exposeTheError("",error.message);
        });
      }).catch((error) => {
        callback({status: 500, message: error.message});
        exposeTheError("",error.message);
      });
    }).catch((error) => {
      callback({status: 500, message: error.message});
      exposeTheError("",error.message);
    });
  });

  socket.on('notResponded', () => {
    
    if (!socket.rider) {
      return;
    }
    let rider     = socket.rider;
    let riderId   = rider.id;
    let role      = socket.role;
    let driverId  = socket.user_id;
    socket.rider  = null;
    socket.active_card = false;
    let promise1  = deactivateTheCard(driverId);
    // let promise2  = findUserSocket(riderId, 'rider');
    
    // Promise.all([promise1, promise2]).then((result) => {
    Promise.all([promise1]).then((result) => {

      let cardDeactivated = result[0];
      // let riderSocket = result[1];
      letsFindTheDriver(riderId, null, driverId);
    }).catch((error) => {
      exposeTheError("",error.message);
    });
  });

  socket.on('rejectTheRequest', (data, callback) => {
    
    if (!socket.rider) {
      return;
    }
    let rider     = socket.rider;
    let riderId   = rider.id;
    let role      = socket.role;
    let driverId  = socket.user_id;
    socket.rider  = null;
    socket.active_card = false;

    let promise1  = deactivateTheCard(driverId);
    // let promise2  = findUserSocket(riderId, 'rider');
    
    // Promise.all([promise1, promise2]).then((result) => {
    Promise.all([promise1]).then((result) => {

      let cardDeactivated = result[0];
      // let riderSocket = result[1];
      console.log("From rejectTheRequest", driverId);
      letsFindTheDriver(riderId, null, driverId);
      callback({status: 200, message: "Request rejected successfully."});
    }).catch((error) => {
      exposeTheError("",error.message);
      callback({status: 500, message: error.message});
    });
  });

  socket.on('disconnect', () => {
    makeUserUnavailable(socket);
  });
});

function letsFindTheDriver(riderId, riderSocket = null, removableDriverId = null){
  if (removableDriverId) {
    console.log("From letsFindTheDriver", removableDriverId);
    logTheData(serverDrivers.length, "serverDrivers.length");
    // _.reject(serverDrivers, {id: parseInt(removableDriverId)});
    _.pullAllWith(serverDrivers, [{ id: removableDriverId }], _.isMatch);
    logTheData(serverDrivers.length, "serverDrivers.length");
  }

  if (serverDrivers.length > 0) { 
    let driver    = serverDrivers[0];
    let driverId  = driver.user_id;
    promise1  = findUserSocket(riderId, 'rider');
    promise2  = findUserSocket(driverId, 'driver');
    Promise.all([promise1, promise2]).then((result) => {

      let riderSocket  = result[0];
      let driverSocket = result[1];

      activateTheCard(driverId).then((result) => {

        driverSocket.rider = Object.assign({}, serverResopnse.rider);
        driverSocket.active_card = true;
        driverSocket.emit('riderReqCame',{rider: serverResopnse.rider, trip_info: serverResopnse.trip_info});
        riderSocket.emit('findingDrivers');
     
      }).catch((error) => {
        exposeTheError("",error.message);
      });
    }).catch((error) => {
      exposeTheError("",error.message);
      letsFindTheDriver(riderId, null, driverId);
    });
  }else{
    findUserSocket(riderId, 'rider')
    .then((riderSocket) => {
      riderSocket.emit('noDriverAvailable');
    }).catch((error) => {
      exposeTheError("",error.message);
    });
  }
}

function makeTheUserOnline(userId, username, role, socket){

  let sql;
  // if (role == 'driver') {
  //   sql = mysql.format('UPDATE drivers SET online = ? WHERE id = ?', [1, userId]);
  // }else{
  //   sql = mysql.format('UPDATE users SET online = ? WHERE id = ?', [1, userId]);
  // }
  sql = mysql.format('UPDATE users SET online = ? WHERE id = ?', [1, userId]);
  mysqlConnection.query(sql, function (error, results, fields) {

    if (error) {
      exposeTheError(socket, error.message +' --> '+ sql);
    }

    if (typeof results !== 'undefined') {

      if (results.changedRows === 1 && results.affectedRows === 1) {
        logTheData(`${role}: ${username}'s db info is online.`);
      }else if(results.changedRows === 0 && results.affectedRows === 1) {
        logTheData(`${role}: ${username}'s db info already online.`);
      }else{
        logTheData(`${role}: ${username}'s db info not found.`);
      }
    }
  });
}

function makeTheUserOffline(userId, username, role, socket){

  let sql;
  // if (role == 'driver') {
  //   sql = mysql.format('UPDATE drivers SET online = ? WHERE id = ?', [0, userId]);
  // }else{
  //   sql = mysql.format('UPDATE users SET online = ? WHERE id = ?', [0, userId]);
  // }
  sql = mysql.format('UPDATE users SET online = ? WHERE id = ?', [0, userId]);
  mysqlConnection.query(sql, function (error, results, fields) {

    if (error) {
      exposeTheError(socket, error.message +' --> '+ sql);
    }
    
    if (typeof results !== 'undefined') {
      
      if (results.changedRows === 1 && results.affectedRows === 1) {
        logTheData(`${role}: ${username}'s db info is offline.`);
      }else if(results.changedRows === 0 && results.affectedRows === 1) {
        logTheData(`${role}: ${username}'s db info already offline.`);
      }else{
        logTheData(`${role}: ${username}'s db info not found.`);
      }
    }
  });
}

function findUserSocket(userId, role){
  return new Promise((resolve, reject) => {
    user = null;
    if (role === 'driver') {
      user = _.find(drivers, _.matches({ user_id: userId }));
    }else if(role === 'rider'){
      user = _.find(riders, _.matches({ user_id: userId }));
    }
    if (user) {
      resolve(user);
    }else{
      reject({message: 'user : '+role+' : is not online.'});
    }
  });
}

async function deactivateTheCard(driverId){
  return await mysqlConnection.query('UPDATE drivers SET active_card = ? WHERE id = ?', [0, driverId], function (error, results, fields) {

    if (error) {
      exposeTheError("", error.message);
    };

    if (results.changedRows === 1 && results.affectedRows === 1) {
      logTheData(`driver's card is inactivated now.`);
    }else if(results.changedRows === 0 && results.affectedRows === 1) {
      logTheData(`driver's card is already inactivated.`);
    }else{
      logTheData(`driver's record not found in db.`);
    }
  });
}

async function activateTheCard(driverId){
  return await mysqlConnection.query('UPDATE drivers SET active_card = ? WHERE id = ?', [1, driverId], function (error, results, fields) {

    if (error) {
      exposeTheError("", error.stack);
    };

    if (results.changedRows === 1 && results.affectedRows === 1) {
      logTheData(`driver's card is activated now.`);
    }else if(results.changedRows === 0 && results.affectedRows === 1) {
      logTheData(`driver's card is already activated.`);
    }else{
      logTheData(`driver's record not found in db.`);
    }
  });
}

async function activateTheRide(driverId){
  return await mysqlConnection.query('UPDATE drivers SET active_card = ?, on_ride = ? WHERE id = ?', [0, 1, driverId], function (error, results, fields) {

    if (error) {
      exposeTheError("", error.stack);
    };

    if (results.changedRows === 1 && results.affectedRows === 1) {
      logTheData(`driver's ride is activated now.`);
    }else if(results.changedRows === 0 && results.affectedRows === 1) {
      logTheData(`driver's ride was already activated.`);
    }else{
      logTheData(`driver's record not found in db.`);
    }
  });
}

function exposeTheError(socket, message){
  console.log('\x1b[36m', message);
  if (socket) {
    socket.emit('error_found', {error_message: message});
  }
}

function logTheData(data){
  console.warn('\x1b[37m',JSON.stringify(data));
}

function makeUserUnavailable(socket){
  
  if (socket.role === 'driver') {
    _.pullAllWith(drivers, [{ id: socket.id }], _.isMatch);
  }else if(socket.role === 'rider'){
    _.pullAllWith(riders, [{ id: socket.id }], _.isMatch);
  }

  makeTheUserOffline(socket.user_id, socket.username, socket.role, socket);
  logTheData(`DRIVERS are: ${drivers.length} & RIDERS are: ${riders.length}`);
  if (socket.active_card === true && socket.rider !== null) {
    letsFindTheDriver(rider.id);
  }
  if (socket.ride_accepted === true && socket.on_ride !== true) {
    letsFindTheDriver(rider.id);
  }
  return true;
}


// async function updateTheDoctorRecord(driverId, action){

//   /** 
//     activateTheCard
//     deactivateTheCard
//     activateTheRide
//     deActivateTheRide
//     onlineTheDriver
//     offlineTheDriver
//   **/
//   return new Promise((resolve, reject) => {

//     let sql;
//     let condition;
//     let successMessage;
//     let cannotUpdateMessage;
//     if (action == 'activateTheCard') {
//       sql = 'UPDATE drivers SET active_card = ? WHERE id = ?';
//       condition = [1, driverId];

//       successMessage = "driver's card is active now.";
//       cannotUpdateMessage = "driver's card was already acive.";
//     }else if(action == 'deactivateTheCard'){
      
//       sql = 'UPDATE drivers SET active_card = ? WHERE id = ?';
//       condition = [0, driverId];
      
//       successMessage = "driver's card is inacive now.";
//       cannotUpdateMessage = "driver's card was already inacive.";
//     }else if(action == 'activateTheRide'){
      
//       sql = 'UPDATE drivers SET active_card = ?, on_ride = ? WHERE id = ?';
//       condition = [0, 1,driverId];

//       successMessage = "driver is on ride now.";
//       cannotUpdateMessage = "driver was already on call.";
//     }else if(action == 'deActivateTheRide'){
      
//       sql = 'UPDATE drivers SET active_card = ?, on_ride = ? WHERE id = ?';
//       condition = [0, 0,driverId];

//       successMessage = "driver is not on ride now.";
//       cannotUpdateMessage = "driver was already not on call.";
//     }else if(action == 'onlineTheDriver'){
      
//       sql = 'UPDATE drivers SET online = ? WHERE id = ?';
//       condition = [1, driverId];

//       successMessage = "driver's db info is online.";
//       cannotUpdateMessage = "driver's db info was already online.";
//     }else if(action == 'offlineTheDriver'){
      
//       sql = 'UPDATE drivers SET online = ? WHERE id = ?';
//       condition = [0, driverId];

//       successMessage = "driver's db info is offline.";
//       cannotUpdateMessage = "driver's db info was already offline.";
//     }
    
//     mysqlConnection.query(sql, condition, function (error, results, fields) {

//       if (error) {
//         reject({message: error.stack});
//       };

//       if (results.changedRows === 1 && results.affectedRows === 1) {
//         logTheData(successMessage);
//         resolve(true);
//       }else if(results.changedRows === 0 && results.affectedRows === 1) {
//         logTheData(cannotUpdateMessage);
//         resolve(true);
//       }else{
//         reject({message: "driver's record not found in db."});
//       }
//     });
//   });
// }


// function findNextDriver(invalidDocIds){
  
//   return new Promise((resolve, reject) => {

//     let sql;
//     if (invalidDocIds.length) {
//       sql = mysql.format('SELECT id, first_name, last_name, email, online, active_card FROM `drivers` WHERE `online` IS NOT NULL AND `active_card` = ? AND id NOT IN (?) ORDER BY id ASC LIMIT 1', [0, 0, invalidDocIds]);
//     }else{
//       sql = mysql.format('SELECT id, first_name, last_name, email, online, active_card FROM `drivers` WHERE `online` IS NOT NULL AND `active_card` = ? ORDER BY id ASC LIMIT 1', [0, 0]);
//     }

//     mysqlConnection.query(sql, async function (error, results, fields) {
      
//       if (error) {
//         reject({message: error.stack});
//       }

//       if (results.length) {
//         findUserSocket(results[0].id, 'driver').then((driverSocket) => {
          
//           logTheData({results: results[0], driver_socket: driverSocket.id});
//           resolve({results: results, driver_socket: driverSocket});
//         }).catch((error) => {
          
//           reject({message: error.message});
//         });
//       }else{
//         reject({message: "No Driver is online at the moment."});
//       }
//     });
//   });
// }

// function makeThUserOnline(userId, username){
//   mysqlConnection.query('UPDATE users SET online = ? WHERE id = ?', [moment().utc().format('YYYY-MM-DD HH:mm:ss'), userId], function (error, results, fields) {

//     if (error) {
//       socket.emit('error_found', {error_message: error.stack});
//     };
//     console.log(results);
//     if (results.changedRows === 1 && results.affectedRows === 1) {
//       console.log(`${username}'s db info is online.`);
//     }else if(results.changedRows === 0 && results.affectedRows === 1) {
//       console.log(`${username}'s db info already online.`);
//     }else{
//       console.log(`${username}'s db info not found.`);
//     }
//   });
// }

// function makeTheUserOffline(userId, username){
//   mysqlConnection.query('UPDATE users SET online = ? WHERE id = ?', [null, userId], function (error, results, fields) {

//     if (error) {
//       socket.emit('error_found', {error_message: error.stack});
//     };

//     if (results.changedRows === 1 && results.affectedRows === 1) {
//       console.log(`${username}'s db info is offline.`);
//     }else if(results.changedRows === 0 && results.affectedRows === 1) {
//       console.log(`${username}'s db info already offline.`);
//     }else{
//       console.log(`${username}'s db info not found.`);
//     }
//   });
// }





// function letsConnect(patientId){
//   findUserSocket(patientId, 'patient')
//   .then((patientSocket) => {

//     if (typeof patientSocket !== 'undefined') {
//       let rejectedByDoctors = patientSocket.rejected_by_doctors;
//       let notRespodedDoctors = patientSocket.not_respoded_doctors;
//       let invalidDocIds = rejectedByDoctors.concat(notRespodedDoctors);
//       console.log('rejected_by_doctors', rejectedByDoctors);
//       console.log('not_respoded_doctors', notRespodedDoctors);
//       console.log('invalidDocIds', invalidDocIds);
//       findOnlineDoctors(invalidDocIds)
//       .then((doctorData) => {

//         let doctorRecord = doctorData.results[0];
//         let doctorId = doctorRecord.id;
//         let doctorSocket = doctorData.doctor_socket;
//         doctorSocket.secret_key = doctorRecord.secret_key;
        
//         mysqlConnection.query('UPDATE doctors SET active_card = ? WHERE id = ?', [1, doctorId], function (error, results, fields) {

//           if (error){
//             console.log(error.stack);
//             throw {message: error.stack};
//           }

//           // console.log(serverResopnse.user);
//           // doctorSocket.patient = serverResopnse.user;
//           doctorSocket.patient = Object.assign({}, serverResopnse.user);
//           doctorSocket.patient.card_id = serverResopnse.card_id;
//           doctorSocket.patient.visit_id = serverResopnse.id;
//           // doctorSocket.patient.patient_secret_key = serverResopnse.patient_secret_key;
//           // doctorSocket.patient.doctor_secret_key = serverResopnse.doctor_secret_key;
//           // console.log(serverResopnse.user);
//           // console.log(doctorSocket.patient);
//           // console.log(doctorSocket.secret_key);
          
//           doctorSocket.emit('patientReqCame',serverResopnse.user);
//           patientSocket.emit('findingDoctors');
//         });
//       })
//       .catch((error) => {
//         console.log(error.message);
//       });
//     }else{
//       throw {message: "The Patient which is online is actually not online from letsConnect fn."};
//     }
//   })
//   .catch((error) => {
//     console.log(error.message);
//   });
// }

// function findOnlineDoctors(invalidDocIds){
//   return new Promise((resolve, reject) => {

//     let sql;
//     if (invalidDocIds.length) {
//       sql = mysql.format('SELECT id, first_name, last_name, email, online, oncall, active_card, secret_key FROM `doctors` WHERE `online` IS NOT NULL AND `oncall` = ? AND `active_card` = ? AND id NOT IN (?) ORDER BY id ASC LIMIT 1', [0, 0, invalidDocIds]);
//     }else{
//       sql = mysql.format('SELECT id, first_name, last_name, email, online, oncall, active_card, secret_key FROM `doctors` WHERE `online` IS NOT NULL AND `oncall` = ? AND `active_card` = ? ORDER BY id ASC LIMIT 1', [0, 0]);
//     }

//     mysqlConnection.query(sql, async function (error, results, fields) {
//       if (error) {
//         reject({message: error.stack});
//       }

//       if (results.length) {

//         findUserSocket(results[0].id, 'doctor')
//         .then((doctorSocket) => {
//           if (typeof doctorSocket !== 'undefined') {
//             console.log({results: results[0], doctor_socket: doctorSocket.id});
//             resolve({results: results, doctor_socket: doctorSocket});
//           }else{
//             reject({message: "The Doctor which is online is actually not online."});
//           }
//         })
//         .catch((error) => {
//           console.log(error.message);
//         });
//       }else{
//         reject({message: "No Doctor is online at the moment."});
//       }
//     });
//   });
// }





// function emitErrorToUser(userSocket, err){
//   console.log(err);
//   userSocket.emit('error_found', {message: err});
// }

// function letsCutTheCharge(patient,doctorSecretKey){
//   console.log({
//     card_id: patient.card_id,
//     visit_id: patient.visit_id,
//     patient_secret_key: patient.secret_key,
//     doctor_secret_key: doctorSecretKey
//   });
//   return new Promise((resolve, reject) => {
//     axios.post('http://localhost/ohealth/api/v1/payForVisit', {
//       card_id: patient.card_id,
//       visit_id: patient.visit_id,
//       patient_secret_key: patient.secret_key,
//       doctor_secret_key: doctorSecretKey
//     })
//     .then(function (response) {
//       if (response.data.status === true) {
//         resolve(response)
//       }else{
//         reject({message: response.data.message})
//       }
//     })
//     .catch(function (error) {
//       reject({message: error.message})
//     });
//   });
// }

