const express = require('express');
const app     = express();
const http    = require('http').Server(app);
const io      = require('socket.io')(http);
const _       = require('lodash');
const mysql   = require('mysql');
const Redis   = require('ioredis');
const redis   = new Redis();
const redis1  = new Redis();
const bodyParser  = require('body-parser');
const socketioJwt = require("socketio-jwt");

let database = require('./config/database');
let mysqlConnection = database.connection;
let appConfig = require('./config/config');

/** Body Parser Middleware **/
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//Start Server
http.listen(appConfig.port, ()=> {
  console.log(`${appConfig.environment}`+` server is started on port number ${appConfig.port}`);
});

let users = [];
let drivers = [];
let riders = [];
let rideQueue = [];
// let serverDrivers;
let counts = 0;
redis.subscribe('FindTheDriverChannel', 'RideStartedChannel', function (err, count) {
  console.log(`we are subscribed to ${count} channels`);
});

redis.on('message', function (channel, message) {

  // console.log('\x1b[33m', 'Receive message %s from channel %s', message, channel);

  let data       = JSON.parse(message);
  let serverResopnse = data.data.data;
  if (channel == 'FindTheDriverChannel') {
    serverResopnse.proceeded = false;
    rideQueue.push(serverResopnse);
    let riderId   = serverResopnse.user_id;
    letsFindTheDriver(riderId);
  }else if(channel == 'RideStartedChannel'){

    let riderId    = serverResopnse.rider;
    let driverId   = serverResopnse.driver;
    promise1  = findUserSocket(riderId, 'rider');
    promise2  = findUserSocket(driverId, 'driver');
    Promise.all([promise1, promise2]).then((result) => {

      let riderSocket  = result[0];
      let driverSocket = result[1];

      riderSocket.on_ride = true;
      driverSocket.on_ride = true;
    }).catch((error) => {
      exposeTheError("",error.message);
      if (error.status != 'rider') {
        letsFindTheDriver(riderId, null, driverId);
      }
    });
  }
});

io.sockets.on('connection', socketioJwt.authorize({
  secret: 'N0i2jt6daNmhTeSVTwoNvYtmcF0je8Bcfn9GaWa0UUkFYnZ8QAUs82AsUl8bxINS',
  timeout: 15000,
  callback: false
})).on('authenticated', function(socket) {
  
  let userId    = parseInt(socket.handshake.query.user_id);
  let username  = socket.handshake.query.username;
  let role      = socket.handshake.query.role;
  let latitude  = parseFloat(socket.handshake.query.latitude);
  let longitude = parseFloat(socket.handshake.query.longitude);

  logTheData([username, socket.id, userId, role]);
  if (!userId || !username || !role ||!latitude || !longitude) {
    exposeTheError(socket, 'Error. you dont have passed all required params.');
  }

  /** Assign required options in socket obj START **/
  socket.user_id  = userId;
  socket.username = username;
  socket.role     = role;
  socket.ride_accepted = false;
  socket.on_ride  = false;
  socket.location = {latitude: latitude, longitude: longitude};
  if (role === 'driver') {
    socket.active_card = false;
    socket.rider = null;
  }else if(role === 'rider'){
    socket.rejected_by_drivers = [];
    socket.not_respoded_drivers = [];
    socket.online = true;
    makeTheUserOnline(userId, username, 'rider', socket);
  }
  users.push(socket);

  socket.on('tik', (data, callback) => {
    callback({status: 200, message: "Tok"});
  });

  socket.on('updateCarMove', (data, callback) => {

    let pureDriverId  = socket.driver_id;
    let latitude      = parseFloat(data.latitude);
    let longitude     = parseFloat(data.longitude);
    let rider         = socket.rider;
    socket.location   = {latitude: latitude, longitude: longitude};

    if (typeof pureDriverId !== 'undefined') {
      redis1
      .pipeline()
      .hmset('drivers:'+pureDriverId, { latitude: latitude, longitude: longitude })
      // .del('drivers:'+pureDriverId)
      // .hgetall('drivers:'+pureDriverId, function(err, result) {})
      .exec(function(err, result) {
        
        if (err) { callback({status: 500, message: err.message}); }
        console.log('--------------------------------------------------');
        console.log(result[0]);
        // console.log(result[1]);
        // console.log(result[2]);
      });
    }

    if (rider) {

      let riderId   = rider.id;
      let driverId  = socket.user_id;
      findUserSocket(riderId, 'rider').then((riderSocket) => {
        riderSocket.emit('carMoved', socket.location);
        callback({status: 200, message: "Position updated successfully."});
      }).catch((error) => {
        exposeTheError("",error.message);
        callback({status: 500, message: error.message});
      });
    }
  });

  socket.on('updateLocationInDB', (data, callback) => {

    let latitude  = parseFloat(data.latitude);
    let longitude = parseFloat(data.longitude);
    let driverId  = parseInt(socket.user_id);

    if (isNaN(latitude) || isNaN(longitude)) {
      callback({status: 500, message: 'Something went wrong while doing driver online.'});
      return;
    }


    mysqlConnection.query('UPDATE users SET current_latitude = ?, current_longitude = ? WHERE id = ?', [latitude, longitude, driverId], function (error, results, fields) {

      if (error) {
        exposeTheError(socket, error.message);
      }

      if (typeof results !== 'undefined') {

        if (results.changedRows === 1 && results.affectedRows === 1) {
          logTheData(`${socket.role}: ${socket.username}'s db location is updated.`);
          callback({status:200, message: `${socket.role}: ${socket.username}'s db location is updated.`});
        }else if(results.changedRows === 0 && results.affectedRows === 1) {
          logTheData(`${socket.role}: ${socket.username}'s db location already updated.`);
          callback({status:500, message: `${socket.role}: ${socket.username}'s db location already updated.`});
        }else{
          logTheData(`${socket.role}: ${socket.username}'s db info not found.`);
          callback({status:500, message: `${socket.role}: ${socket.username}'s db info not found.`});
        }
      }
    });
  });

  socket.on('makeMeOnline', (data, callback) => {
    
    let latitude  = parseFloat(data.latitude);
    let longitude = parseFloat(data.longitude);

    if (isNaN(latitude) || isNaN(longitude)) {
      callback({status: 500, message: 'Something went wrong while doing driver online.'});
      return;
    }

    let location  = [latitude, longitude];
    makeTheUserOnline(socket.user_id, socket.username, socket.role, socket, location).then((data) => {
      socket.online = true;
      socket.emit('yourOnlineStatus',{online: true});
      callback({status: 200, message: 'User is marked as online.'});
    }).catch((error) => {
      callback({status: 500, message: error.message});
    });
  });

  socket.on('makeMeOffline', (data, callback) => {

    let latitude  = parseFloat(data.latitude);
    let longitude = parseFloat(data.longitude);
    
    if (isNaN(latitude) || isNaN(longitude)) {
      callback({status: 500, message: 'Something went wrong while doing driver offline.'});
      return;
    }

    let location  = [latitude, longitude];
    makeTheUserOnline(socket.user_id, socket.username, socket.role, socket, location).then((data) => {
      socket.online = false;
      socket.emit('yourOnlineStatus',{online: false});
      callback({status: 200, message: 'User is marked as offline.'});
    }).catch((error) => {
      callback({status: 500, message: error.message});
    });
  });

  socket.on('acceptTheRequest', (data, callback) => {

    let rider     = socket.rider;
    let riderId   = rider.id;
    let driverId  = socket.user_id;
    let driver    = {id: driverId, name:socket.username, role:socket.role };
    findUserSocket(riderId, 'rider').then((riderSocket) => {

      activateTheRide(driverId).then((driverQueryRsult) => {

        driverInfo = _.find(serverDrivers, { user_id: driverId });
        Promise.all([driverInfo]).then((result) => {

          socket.ride_accepted = true;
          riderSocket.ride_accepted = true;
          riderSocket.emit('driverIsOnRide',{
            driver_location: socket.location, 
            driver_info: result[0], 
            trip_info:serverResopnse.trip_info
          });
          
          socket.emit('riderIsOnRide',{
            rider_location: riderSocket.location, 
            rider_info: rider, 
            trip_info:serverResopnse.trip_info
          });
          callback({status: 200, message: "Request accepted successfully."});
        }).catch((error) => {
          callback({status: 500, message: error.message});
          exposeTheError("",error.message);
        });
      }).catch((error) => {
        callback({status: 500, message: error.message});
        exposeTheError("",error.message);
      });
    }).catch((error) => {
      callback({status: 500, message: error.message});
      exposeTheError("",error.message);
    });
  });

  socket.on('notResponded', () => {
    
    if (!socket.rider) {
      return;
    }
    let rider     = socket.rider;
    let riderId   = rider.id;
    let driverId  = socket.user_id;
    let tripId    = socket.trip_info.trip_id;
    let pureDriverId = socket.driver_id;
    socket.rider        = null;
    socket.trip_info    = null;
    socket.active_card  = false;
    let promise1  = deactivateTheCard(driverId);
    let promise2  = invalidateARequest(tripId, pureDriverId);
    Promise.all([promise1, promise2]).then((result) => {

      let cardDeactivated = result[0];
      letsFindTheDriver(riderId, null, driverId);
    }).catch((error) => {
      exposeTheError("",error.message);
    });
  });

  socket.on('rejectTheRequest', (data, callback) => {
    
    if (!socket.rider) {
      return;
    }
    let rider     = socket.rider;
    let riderId   = rider.id;
    let driverId  = socket.user_id;
    let tripId    = socket.trip_info.trip_id;
    let pureDriverId = socket.driver_id;
    socket.rider        = null;
    socket.trip_info    = null;
    socket.active_card  = false;

    let promise1  = deactivateTheCard(driverId);
    let promise2  = invalidateARequest(tripId, pureDriverId);
    Promise.all([promise1, promise2]).then((result) => {

      let cardDeactivated = result[0];
      console.log(driverId+ ' has rejected the ride.');
      letsFindTheDriver(riderId, null, driverId);
      callback({status: 200, message: "Request rejected successfully."});
    }).catch((error) => {
      exposeTheError("",error.message);
      callback({status: 500, message: error.message});
    });
  });

  socket.on('disconnect', () => {
    makeUserUnavailable(socket);
  });
});

function letsFindTheDriver(riderId, riderSocket = null, removableDriverId = null){
  
  let rideInfo    = _.find(rideQueue, { user_id: riderId, proceeded:false });

  if (!rideInfo){
    exposeTheError("",'No ride info found for the rider having id '+riderId);
    return;
  }

  let rideDrivers = rideInfo.drivers;
  let rider       = rideInfo.rider;
  let tripInfo    = rideInfo.trip_info;

  if (rideInfo && removableDriverId) {
    console.log("REMOVED : ", removableDriverId);
    console.log("DRIVERS : ", rideDrivers.length);
    _.pullAllWith(rideDrivers, [{ user_id: removableDriverId }], _.isMatch);
    console.log("DRIVERS : ", rideDrivers.length);
  }

  if (rideDrivers.length > 0) { 
    let driver    = rideDrivers[0];
    let driverId  = driver.user_id;
    promise1  = findUserSocket(driverId, 'driver');
    promise2  = findUserSocket(riderId, 'rider');
    Promise.all([promise1, promise2]).then((result) => {

      let driverSocket = result[0];
      let riderSocket  = result[1];

      let pureDriverId = driverSocket.driver_id;
      promise1  = createARequest(tripInfo.trip_id, pureDriverId);
      promise2  = activateTheCard(driverId);
      Promise.all([promise1, promise2]).then((result) => {

        driverSocket.rider = Object.assign({}, rider);
        driverSocket.trip_info = Object.assign({}, tripInfo);
        driverSocket.active_card = true;
        // console.log('DRIVER SOCKET ID : '+driverSocket.id);
        // console.log('DRIVER SOCKET NAME : ',driverSocket.username);
        // console.log('DRIVER SOCKET ROLE : ',driverSocket.role);
        driverSocket.emit('riderReqCame',{rider: rider, trip_info: tripInfo});
        riderSocket.emit('findingDrivers');
      }).catch((error) => {
        exposeTheError("",error);
      });
    }).catch((error) => {
      exposeTheError("",error);
      if (error.status != 'rider') {
        letsFindTheDriver(riderId, null, driverId);
      }
    });
  }else{
    findUserSocket(riderId, 'rider')
    .then((riderSocket) => {
      if (riderSocket) {
        riderSocket.emit('noDriverAvailable');
      }else{
        _.pullAllWith(rideQueue, [{ user_id: riderId, proceeded:false }], _.isMatch);
        exposeTheError("",'Rider not conline to see the noDriverAvailable event.');
      }
    }).catch((error) => {
      exposeTheError("",error.message);
    });
  }
}

function makeTheUserOnline(userId, username, role, socket, location = false){

  return new Promise((resolve, reject) => {

    let sql;
    if (location) {
      sql = mysql.format('UPDATE users SET online = ?, current_latitude = ?, current_longitude = ? WHERE id = ?', [1, userId, location[0], location[1]]);
    }else{
      sql = mysql.format('UPDATE users SET online = ? WHERE id = ?', [1, userId]);
    }
    mysqlConnection.query(sql, function (error, results, fields) {

      if (error) {
        exposeTheError(socket, error.message +' --> '+ sql);
      }

      if (typeof results !== 'undefined') {
        
        let message;
        if (results.changedRows === 0 && results.affectedRows === 0) {
          reject(`${role}: ${username}'s db info not found.`);
        }else if(results.changedRows === 1 && results.affectedRows === 1){
          message = `${role}: ${username}'s db info is online.`;
        }else if(results.changedRows === 0 && results.affectedRows === 1){
          message = `${role}: ${username}'s db info already online.`;
        }

        getDriverId(userId).then((data) => {
          if (data.count) {
            socket.online = true;
            socket.driver_id = data.driver_id;
            logTheData(`${role}: ${username}'s db info is online.`);
            resolve(true);
          }else{
            reject({message: 'user does not have its driver account.'});
          }
        }).catch((error) => {
          reject(error);
        });
      }
    });
  });
}

function getDriverId(userId){
  return new Promise((resolve, reject) => {

    sql = mysql.format('SELECT id, user_id FROM drivers WHERE user_id = ?', [userId]);
    mysqlConnection.query(sql, function (error, results, fields) {
      if (error) {
        reject(socket, error.message);
      }
      resolve({count: results.length, driver_id: results[0].id});
    });
  });
}

function makeTheUserOffline(userId, username, role, socket){

  return new Promise((resolve, reject) => {

    let sql;
    sql = mysql.format('UPDATE users SET online = ? WHERE id = ?', [0, userId]);
    mysqlConnection.query(sql, function (error, results, fields) {

      if (error) {
        exposeTheError(socket, error.message +' --> '+ sql);
      }

      if (typeof results !== 'undefined') {
        socket.online = false;
        if (results.changedRows === 1 && results.affectedRows === 1) {
          logTheData(`${role}: ${username}'s db info is offline.`);
          resolve(`${role}: ${username}'s db info is offline.`);
        }else if(results.changedRows === 0 && results.affectedRows === 1) {
          logTheData(`${role}: ${username}'s db info already offline.`);
          resolve(`${role}: ${username}'s db info already offline.`);
        }else{
          reject(`${role}: ${username}'s db info not found.`);
        }
      }
    });
  });
}

function findUserSocket(userId, role){
  return new Promise((resolve, reject) => {
    user = _.find(users, _.matches({ user_id: userId }));
    if (!user && role != 'rider') {
      reject({message: `${user} is not online.`});
    }else{
      resolve(user);
    }
  });
}

async function deactivateTheCard(driverId){
  return await mysqlConnection.query('UPDATE drivers SET active_card = ? WHERE id = ?', [0, driverId], function (error, results, fields) {

    if (error) {
      exposeTheError("", error.message);
    };

    if (results.changedRows === 1 && results.affectedRows === 1) {
      logTheData(`driver's card is inactivated now.`);
    }else if(results.changedRows === 0 && results.affectedRows === 1) {
      logTheData(`driver's card is already inactivated.`);
    }else{
      logTheData(`driver's record not found in db.`);
    }
  });
}

async function activateTheCard(driverId){
  return await mysqlConnection.query('UPDATE drivers SET active_card = ? WHERE id = ?', [1, driverId], function (error, results, fields) {

    if (error) {
      reject({message: error.stack});
    }

    if (results.changedRows === 1 && results.affectedRows === 1) {
      logTheData(`driver's card is activated now.`);
      return true;
    }else if(results.changedRows === 0 && results.affectedRows === 1) {
      logTheData(`driver's card is already activated.`);
      return true;
    }else{
      logTheData(`driver's record not found in db.`);
      return false;
    }
  });
}

async function activateTheRide(driverId){
  return await mysqlConnection.query('UPDATE drivers SET active_card = ?, on_ride = ? WHERE id = ?', [0, 1, driverId], function (error, results, fields) {

    if (error) {
      exposeTheError("", error.stack);
    };

    if (results.changedRows === 1 && results.affectedRows === 1) {
      logTheData(`driver's ride is activated now.`);
    }else if(results.changedRows === 0 && results.affectedRows === 1) {
      logTheData(`driver's ride was already activated.`);
    }else{
      logTheData(`driver's record not found in db.`);
    }
  });
}

function exposeTheError(socket, message){
  console.log('\x1b[36m', message);
  if (socket) {
    socket.emit('error_found', {error_message: message});
  }
}

function logTheData(data){
  console.warn('\x1b[37m',JSON.stringify(data));
}

async function createARequest(tripId, driverId){
  return new Promise((resolve, reject) => {

    mysqlConnection.query('INSERT INTO in_city_trip_requests SET ?', {incity_trip_id: tripId, driver_id: driverId, status: 1}, function (error, results, fields) {

      if (error) {
        reject({message: error.stack});
      };

      console.log(results);
      logTheData(`driver's ride is created now.`);
      resolve(true);
    });
  });
}

// async function activateARequest(tripId, driverId){
//   return new Promise((resolve, reject) => {
//     mysqlConnection.query('UPDATE in_city_trip_requests SET status = ? WHERE incity_trip_id = ? AND driver_id = ?', [1, tripId, driverId], function (error, results, fields) {

//       if (error) {
//         exposeTheError("", error.stack);
//       };

//       if (results.changedRows === 1 && results.affectedRows === 1) {
//         logTheData(`driver's ride is activated now.`);
//         resolve(true);
//       }else if(results.changedRows === 0 && results.affectedRows === 1) {
//         logTheData(`driver's ride was already activated.`);
//         resolve(true);
//       }else{
//         reject({message: `driver's record not found in db.`});
//       }
//     });
//   });
// }

async function invalidateARequest(tripId, driverId){
  return new Promise((resolve, reject) => {
    mysqlConnection.query('UPDATE in_city_trip_requests SET status = ? WHERE incity_trip_id = ? AND driver_id = ?', [0, tripId, driverId], function (error, results, fields) {

      if (error) {
        exposeTheError("", error.stack);
      };

      if (results.changedRows === 1 && results.affectedRows === 1) {
        logTheData(`driver's ride is activated now.`);
        resolve(true);
      }else if(results.changedRows === 0 && results.affectedRows === 1) {
        logTheData(`driver's ride was already activated.`);
        resolve(true);
      }else{
        reject({message: `driver's record not found in db.`});
      }
    });
  });
}

function makeUserUnavailable(socket){
  
  _.pullAllWith(users, [{ id: socket.id, role: socket.role }], _.isMatch);

  if ((socket.role === 'rider') || (socket.role === 'driver' && typeof socket.online !== 'undefined' &&socket.online === true)) {
    makeTheUserOffline(socket.user_id, socket.username, socket.role, socket);
  }
  logTheData(`DRIVERS are: ${drivers.length} & RIDERS are: ${riders.length}`);
  
  /** 
    move ride to next driver IF:
      1. Driver's card is active and Driver haven't accpeted the request, and unfortunately he is getting disconnected from socket.
      2. Driver had accepted the ride but ride wasn't started and unfortunately he is getting disconnected from socket.
  **/
  if ((socket.active_card === true && socket.rider !== null) || 
      (socket.ride_accepted === true && socket.on_ride !== true)) {
    console.log(socket.trip_info.trip_id, socket.driver_id);
    invalidateARequest(socket.trip_info.trip_id, socket.driver_id).then((data) => {
      letsFindTheDriver(socket.rider.id);
    }).catch((error) => {
      exposeTheError("",error.message);
    });
  }
  return true;
}

// setInterval(function(){ 
  
//   drivers = _.filter(users, { role: 'driver' });

//   Promise.all([drivers]).then((result) => {
//     let drivers = result[0];
//     drivers.map((driver, index) => {

//       let driverId  = driver.user_id;
//       let latitude  = driver.location.latitude;
//       let longitude = driver.location.longitude;
//       // console.log('driver_id', driverId, 'latitude', latitude, 'longitude', longitude);
//       mysqlConnection.query('UPDATE users SET current_latitude = ?, current_longitude = ? WHERE id = ?', [latitude, longitude, driverId], function (error, results, fields) {

//         if (error) {
//           exposeTheError(socket, error.message);
//         }

//         if (typeof results !== 'undefined') {

//           if (results.changedRows === 1 && results.affectedRows === 1) {
//             logTheData(`${driver.role}: ${driver.username}'s db location is updated.`);
//           }else if(results.changedRows === 0 && results.affectedRows === 1) {
//             logTheData(`${driver.role}: ${driver.username}'s db location already updated.`);
//           }else{
//             logTheData(`${driver.role}: ${driver.username}'s db info not found.`);
//           }
//         }
//       });
//     });
//   });

// }, 2 * 60 * 60 * 1000);
// }, 3000);
