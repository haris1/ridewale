if (process.env.NODE_ENV == 'production') {
 
	module.exports = {
		environment: 'production',
		database : {
			db:'mongodb://localhost/ridechimp',
			username:'swooshdbuser',
			password:')ug_bN-%@3K)',
		},
		tokenExpiryTime: '1h',
		port: 4006,
		socketSecret: 'sl2sRTpENMvfZt4SP4P2RvN7JOMjTp3xyEHoz4N7QMJId3wCqFlP8xztHaaoyWoT',
		imagePath: 'https://ridechimp.co/rcpanel/public/storage/users/',
	}
}else if (process.env.NODE_ENV == 'development') {
 
	module.exports = {
		environment: 'staging',
		database : {
			db:'fddev_rchimpdb',
			username:'fddev_rchimpusr',
			password:'eC$_jW_O{A^s',
		},
		tokenExpiryTime: '10h',
		port: 4006,
		socketSecret: 'sl2sRTpENMvfZt4SP4P2RvN7JOMjTp3xyEHoz4N7QMJId3wCqFlP8xztHaaoyWoW',
		imagePath: 'http://foremostdigital.dev/ridechimp/public/storage/users/',
	}
}else{
	module.exports = {
		environment: 'local',
		database : {
			db:'ridewale1',
			username:'root',
			password:'',
		},
		tokenExpiryTime: '10h',
		port: 4006,
		socketSecret: 'sl2sRTpENMvfZt4SP4P2RvN7JOMjTp3xyEHoz4N7QMJId3wCqFlP8xztHaaoyWoP',
		imagePath: 'http://localhost/ridewale/public/storage/users/',
	}
}