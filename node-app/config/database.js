const mysql  = require('mysql');
const config = require('./config');

var connection = mysql.createConnection({
  host     : '127.0.0.1',
  user     : config.database.username,
  password : config.database.password,
  database : config.database.db
});

connection.connect(function(err) {
  if (err) {
    console.error('error while connecting: ' + err.stack);
    return;
  }
 
  console.log('mysql connected as id ' + connection.threadId);
});

module.exports = {
	connection: connection
}