const express = require('express');
const app     = express();
const http    = require('http').Server(app);
const io      = require('socket.io')(http);
const _       = require('lodash');
const mysql   = require('mysql');
const Redis   = require('ioredis');
const redis   = new Redis();
const bodyParser  = require('body-parser');
const socketioJwt = require("socketio-jwt");

let database = require('./config/database');
let mysqlConnection = database.connection;
let appConfig = require('./config/config');

/** Body Parser Middleware **/
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//Start Server
http.listen(appConfig.port, ()=> {
  console.log(`${appConfig.environment}`+` server is started on port number ${appConfig.port}`);
});

let drivers = [];
let riders = [];
let rideQueue = [];
// let serverDrivers;

redis.subscribe('FindTheDriverChannel', 'RideStartedChannel', function (err, count) {
  console.log(`we are subscribed to ${count} channels`);
});

redis.on('message', function (channel, message) {

  // console.log('\x1b[33m', 'Receive message %s from channel %s', message, channel);

  let data       = JSON.parse(message);
  let serverResopnse = data.data.data;
  if (channel == 'FindTheDriverChannel') {
    rideQueue.push(serverResopnse);
    // serverResopnseForRide = serverResopnse;
    // serverDrivers = serverResopnse.drivers;
    let riderId   = serverResopnse.user_id;
    let tripInfo  = serverResopnse.trip_info;
    letsFindTheDriver(riderId);
  }else if(channel == 'RideStartedChannel'){

    let riderId    = serverResopnse.rider;
    let driverId   = serverResopnse.driver;
    promise1  = findUserSocket(riderId, 'rider');
    promise2  = findUserSocket(driverId, 'driver');
    Promise.all([promise1, promise2]).then((result) => {

      let riderSocket  = result[0];
      let driverSocket = result[1];

      riderSocket.on_ride = true;
      driverSocket.on_ride = true;
    }).catch((error) => {
      exposeTheError("",error.message);
      if (error.status != 'rider') {
        letsFindTheDriver(riderId, null, driverId);
      }
    });
  }
});

io.sockets.on('connection', 
socketioJwt.authorize({
  secret: 'N0i2jt6daNmhTeSVTwoNvYtmcF0je8Bcfn9GaWa0UUkFYnZ8QAUs82AsUl8bxINS',
  timeout: 10000,
  callback: false
}));

io.on('connection', function(socket) {
  
  let userId    = parseInt(socket.handshake.query.user_id);
  let username  = socket.handshake.query.username;
  let role      = socket.handshake.query.role;
  let latitude  = parseFloat(socket.handshake.query.latitude);
  let longitude = parseFloat(socket.handshake.query.longitude);

  logTheData([username, socket.id, userId, role]);
  if (!userId || !username || !role ||!latitude || !longitude) {
    exposeTheError(socket, 'Error. you dont have passed all required params.');
  }

  /** Assign required options in socket obj START **/
  socket.user_id  = userId;
  socket.username = username;
  socket.role     = role;
  socket.ride_accepted = false;
  socket.on_ride  = false;
  socket.location = {latitude: latitude, longitude: longitude};
  if (role === 'driver') {
    socket.active_card = false;
    socket.rider = null;
    drivers.push(socket);
    // makeTheUserOnline(userId, username, 'driver', socket);
  }else if(role === 'rider'){
    socket.rejected_by_drivers = [];
    socket.not_respoded_drivers = [];
    riders.push(socket);
    makeTheUserOnline(userId, username, 'rider', socket);
  }

  socket.on('tik', (data, callback) => {
    callback({status: 200, message: "Tok"});
  });

  socket.on('updateCarMove', (data, callback) => {

    let latitude  = parseFloat(data.latitude);
    let longitude = parseFloat(data.longitude);
    socket.location = {latitude: latitude, longitude: longitude};

    let rider     = socket.rider;
    if (!rider) {
      return;
    }else{

      let riderId   = rider.id;
      let driverId  = socket.user_id;
      findUserSocket(riderId, 'rider').then((riderSocket) => {
        riderSocket.emit('carMoved', socket.location);
        callback({status: 200, message: "Position updated successfully."});
      }).catch((error) => {
        exposeTheError("",error.message);
        callback({status: 500, message: error.message});
      });
    }
  });

  socket.on('updateLocationInDB', (data, callback) => {

    let latitude  = parseFloat(data.latitude);
    let longitude = parseFloat(data.longitude);
    let driverId  = parseInt(socket.user_id);

    mysqlConnection.query('UPDATE users SET current_latitude = ?, current_longitude = ? WHERE id = ?', [latitude, longitude, driverId], function (error, results, fields) {

      if (error) {
        exposeTheError(socket, error.message);
      }

      if (typeof results !== 'undefined') {

        if (results.changedRows === 1 && results.affectedRows === 1) {
          logTheData(`${socket.role}: ${socket.username}'s db location is updated.`);
          callback({status:200, message: `${socket.role}: ${socket.username}'s db location is updated.`});
        }else if(results.changedRows === 0 && results.affectedRows === 1) {
          logTheData(`${socket.role}: ${socket.username}'s db location already updated.`);
          callback({status:500, message: `${socket.role}: ${socket.username}'s db location already updated.`});
        }else{
          logTheData(`${socket.role}: ${socket.username}'s db info not found.`);
          callback({status:500, message: `${socket.role}: ${socket.username}'s db info not found.`});
        }
      }
    });
  });

  socket.on('makeMeOnline', (data, callback) => {
    makeTheUserOnline(socket.user_id, socket.username, socket.role, socket).then((data) => {
      callback({status: 200, message: 'user is marked as online.'});
    }).catch((error) => {
      callback({status: 500, message: error.message});
    });
  });

  socket.on('makeMeOffline', (data, callback) => {
    makeTheUserOnline(socket.user_id, socket.username, socket.role, socket).then((data) => {
      callback({status: 200, message: 'user is marked as offline.'});
    }).catch((error) => {
      callback({status: 500, message: error.message});
    });
  });

  socket.on('acceptTheRequest', (data, callback) => {

    let rider     = socket.rider;
    let riderId   = rider.id;
    let driverId  = socket.user_id;
    let driver    = {id: driverId, name:socket.username, role:socket.role };
    findUserSocket(riderId, 'rider').then((riderSocket) => {

      activateTheRide(driverId).then((driverQueryRsult) => {

        driverInfo = _.find(serverDrivers, { user_id: driverId });
        Promise.all([driverInfo]).then((result) => {

          socket.ride_accepted = true;
          riderSocket.ride_accepted = true;
          riderSocket.emit('driverIsOnRide',{
            driver_location: socket.location, 
            driver_info: result[0], 
            trip_info:serverResopnse.trip_info
          });
          
          socket.emit('riderIsOnRide',{
            rider_location: riderSocket.location, 
            rider_info: rider, 
            trip_info:serverResopnse.trip_info
          });
          callback({status: 200, message: "Request accepted successfully."});
        }).catch((error) => {
          callback({status: 500, message: error.message});
          exposeTheError("",error.message);
        });
      }).catch((error) => {
        callback({status: 500, message: error.message});
        exposeTheError("",error.message);
      });
    }).catch((error) => {
      callback({status: 500, message: error.message});
      exposeTheError("",error.message);
    });
  });

  socket.on('notResponded', () => {
    
    if (!socket.rider) {
      return;
    }
    let rider     = socket.rider;
    let riderId   = rider.id;
    let role      = socket.role;
    let driverId  = socket.user_id;
    socket.rider  = null;
    socket.active_card = false;
    let promise1  = deactivateTheCard(driverId);
    // let promise2  = findUserSocket(riderId, 'rider');
    
    // Promise.all([promise1, promise2]).then((result) => {
    Promise.all([promise1]).then((result) => {

      let cardDeactivated = result[0];
      // let riderSocket = result[1];
      letsFindTheDriver(riderId, null, driverId);
    }).catch((error) => {
      exposeTheError("",error.message);
    });
  });

  socket.on('rejectTheRequest', (data, callback) => {
    
    if (!socket.rider) {
      return;
    }
    let rider     = socket.rider;
    let riderId   = rider.id;
    let role      = socket.role;
    let driverId  = socket.user_id;
    socket.rider  = null;
    socket.active_card = false;

    let promise1  = deactivateTheCard(driverId);
    // let promise2  = findUserSocket(riderId, 'rider');
    
    // Promise.all([promise1, promise2]).then((result) => {
    Promise.all([promise1]).then((result) => {

      let cardDeactivated = result[0];
      // let riderSocket = result[1];
      console.log("From rejectTheRequest", driverId);
      letsFindTheDriver(riderId, null, driverId);
      callback({status: 200, message: "Request rejected successfully."});
    }).catch((error) => {
      exposeTheError("",error.message);
      callback({status: 500, message: error.message});
    });
  });

  socket.on('disconnect', () => {
    makeUserUnavailable(socket);
  });
});

function letsFindTheDriver(riderId, riderSocket = null, removableDriverId = null){
  
  let rideInfo = _.find(rideQueue, { 'user_id': riderId });
  let rideDrivers = rideInfo.drivers;
  if (rideInfo) {

    if (removableDriverId) {
      console.log("removableDriverId", removableDriverId);
      console.log("drivers.length", rideDrivers.length);
      // _.reject(serverDrivers, {id: parseInt(removableDriverId)});
      _.pullAllWith(rideDrivers, [{ user_id: removableDriverId }], _.isMatch);
      console.log("drivers.length", rideDrivers.length);
    }
  }else{
    exposeTheError("",'No ride info found for the rider aving id '+riderId);
  }
  

  if (rideDrivers.length > 0) { 
    let driver    = rideDrivers[0];
    let driverId  = driver.user_id;
    promise1  = findUserSocket(riderId, 'rider');
    promise2  = findUserSocket(driverId, 'driver');
    Promise.all([promise1, promise2]).then((result) => {

      let riderSocket  = result[0];
      let driverSocket = result[1];

      activateTheCard(driverId).then((result) => {

        driverSocket.rider = Object.assign({}, serverResopnse.rider);
        driverSocket.active_card = true;
        driverSocket.emit('riderReqCame',{rider: serverResopnse.rider, trip_info: serverResopnse.trip_info});
        riderSocket.emit('findingDrivers');
     
      }).catch((error) => {
        exposeTheError("",error.message);
      });
    }).catch((error) => {
      exposeTheError("",error.message);
      if (error.status != 'rider') {
        letsFindTheDriver(riderId, null, driverId);
      }
    });
  }else{
    findUserSocket(riderId, 'rider')
    .then((riderSocket) => {
      riderSocket.emit('noDriverAvailable');
    }).catch((error) => {
      exposeTheError("",error.message);
    });
  }
}

function makeTheUserOnline(userId, username, role, socket){

  return new Promise((resolve, reject) => {

    let sql;
    sql = mysql.format('UPDATE users SET online = ? WHERE id = ?', [1, userId]);
    mysqlConnection.query(sql, function (error, results, fields) {

      if (error) {
        exposeTheError(socket, error.message +' --> '+ sql);
      }

      if (typeof results !== 'undefined') {
        if (results.changedRows === 1 && results.affectedRows === 1) {
          
          getDriverId(userId).then((data) => {
            socket.online = true;
            socket.driver_id = data.driver_id;
            logTheData(`${role}: ${username}'s db info already online.`);
            resolve(`${role}: ${username}'s db info is online.`);
          }).catch((error) => {
            exposeTheError("", error.message);
          });
        }else if(results.changedRows === 0 && results.affectedRows === 1) {
          getDriverId(userId).then((data) => {
            socket.online = true;
            socket.driver_id = data.driver_id;
            logTheData(`${role}: ${username}'s db info already online.`);
            resolve(`${role}: ${username}'s db info already online.`);
          }).catch((error) => {
            exposeTheError("", error.message);
          });
        }else{
          reject(`${role}: ${username}'s db info not found.`);
        }
      }
    });
  });
}

function getDriverId(userId){
  return new Promise((resolve, reject) => {

    let sql;
    sql = mysql.format('SELECT id, user_id FROM drivers WHERE user_id = ?', [userId]);
    mysqlConnection.query(sql, function (error, results, fields) {

      if (error) {
        exposeTheError(socket, error.message +' --> '+ sql);
      }

      if (results.length) {
        resolve({driver_id: results[0].id});
      }else{
        reject({message: `user does not have its driver account.`});
      }
    });
  });
}

function makeTheUserOffline(userId, username, role, socket){

  return new Promise((resolve, reject) => {

    let sql;
    sql = mysql.format('UPDATE users SET online = ? WHERE id = ?', [0, userId]);
    mysqlConnection.query(sql, function (error, results, fields) {

      if (error) {
        exposeTheError(socket, error.message +' --> '+ sql);
      }

      if (typeof results !== 'undefined') {
        socket.online = false;
        if (results.changedRows === 1 && results.affectedRows === 1) {
          logTheData(`${role}: ${username}'s db info is offline.`);
          resolve(`${role}: ${username}'s db info is offline.`);
        }else if(results.changedRows === 0 && results.affectedRows === 1) {
          logTheData(`${role}: ${username}'s db info already offline.`);
          resolve(`${role}: ${username}'s db info already offline.`);
        }else{
          reject(`${role}: ${username}'s db info not found.`);
        }
      }
    });
  });
}

function findUserSocket(userId, role){
  return new Promise((resolve, reject) => {
    user = null;
    if (role === 'driver') {
      user = _.find(drivers, _.matches({ user_id: userId }));
    }else if(role === 'rider'){
      user = _.find(riders, _.matches({ user_id: userId }));
    }
    if (user) {
      resolve(user);
    }else{
      reject({status:role, message: 'user role : '+role+' : is not online.'});
    }
  });
}

async function deactivateTheCard(driverId){
  return await mysqlConnection.query('UPDATE drivers SET active_card = ? WHERE id = ?', [0, driverId], function (error, results, fields) {

    if (error) {
      exposeTheError("", error.message);
    };

    if (results.changedRows === 1 && results.affectedRows === 1) {
      logTheData(`driver's card is inactivated now.`);
    }else if(results.changedRows === 0 && results.affectedRows === 1) {
      logTheData(`driver's card is already inactivated.`);
    }else{
      logTheData(`driver's record not found in db.`);
    }
  });
}

async function activateTheCard(driverId){
  return await mysqlConnection.query('UPDATE drivers SET active_card = ? WHERE id = ?', [1, driverId], function (error, results, fields) {

    if (error) {
      exposeTheError("", error.stack);
    };

    if (results.changedRows === 1 && results.affectedRows === 1) {
      logTheData(`driver's card is activated now.`);
    }else if(results.changedRows === 0 && results.affectedRows === 1) {
      logTheData(`driver's card is already activated.`);
    }else{
      logTheData(`driver's record not found in db.`);
    }
  });
}

async function activateTheRide(driverId){
  return await mysqlConnection.query('UPDATE drivers SET active_card = ?, on_ride = ? WHERE id = ?', [0, 1, driverId], function (error, results, fields) {

    if (error) {
      exposeTheError("", error.stack);
    };

    if (results.changedRows === 1 && results.affectedRows === 1) {
      logTheData(`driver's ride is activated now.`);
    }else if(results.changedRows === 0 && results.affectedRows === 1) {
      logTheData(`driver's ride was already activated.`);
    }else{
      logTheData(`driver's record not found in db.`);
    }
  });
}

function exposeTheError(socket, message){
  console.log('\x1b[36m', message);
  if (socket) {
    socket.emit('error_found', {error_message: message});
  }
}

function logTheData(data){
  console.warn('\x1b[37m',JSON.stringify(data));
}

async function unallocateTheRide(tripId, driverId){
  return await mysqlConnection.query('UPDATE in_city_trip_requests SET status = ?, on_ride = ? WHERE incity_trip_id = ? AND driver_id = ?', [0, tripId, driverId], function (error, results, fields) {

    if (error) {
      exposeTheError("", error.stack);
    };

    if (results.changedRows === 1 && results.affectedRows === 1) {
      logTheData(`driver's ride is activated now.`);
      resolve(true);
    }else if(results.changedRows === 0 && results.affectedRows === 1) {
      logTheData(`driver's ride was already activated.`);
      resolve(true);
    }else{
      reject({message: `driver's record not found in db.`});
    }
  });
}
function makeUserUnavailable(socket){
  
  if (socket.role === 'driver') {
    _.pullAllWith(drivers, [{ id: socket.id }], _.isMatch);
  }else if(socket.role === 'rider'){
    _.pullAllWith(riders, [{ id: socket.id }], _.isMatch);
  }

  if ((socket.role === 'rider') || (socket.role === 'driver' && socket.online === true)) {
    makeTheUserOffline(socket.user_id, socket.username, socket.role, socket);
  }
  logTheData(`DRIVERS are: ${drivers.length} & RIDERS are: ${riders.length}`);
  
  /** 
    move ride to next driver IF:
      1. Driver's card is active and Driver haven't accpeted the request, and unfortunately he is getting disconnected from socket.
      2. Driver had accepted the ride but ride wasn't started and unfortunately he is getting disconnected from socket.
  **/
  if ((socket.active_card === true && socket.rider !== null) || 
      (socket.ride_accepted === true && socket.on_ride !== true)) {
    unallocateTheRide(socket.trip_info.trip_id, socket.driver_id).then((data) => {
      letsFindTheDriver(rider.id);
    }).catch((error) => {
      exposeTheError("",error.message);
    });
  }
  return true;
}