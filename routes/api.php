<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
	return $request->user();
});

Route::group(['prefix' => 'v2', 'namespace' => 'Api\v2', 'middleware' => 'auth:api'], function(){

	Route::post('trip_detail','BookingController@tripDetail');
	Route::get('get_working_cities','BookingController@getWorkingCities');
	Route::post('book_the_trip','BookingController@bookTheTrip');
	Route::post('my_active_trips','BookingController@myActiveTrips');
	Route::post('my_confirmed_trips','BookingController@myConfimedTrips');
	Route::post('my_pending_trips','BookingController@myPendingTrips');
	Route::post('rider_cancelled_trips','BookingController@riderCancelledTrips');
	Route::post('rider_completed_trips','BookingController@riderCompletedTrips');
	Route::post('booking_detail','BookingController@bookingDetail');
	Route::post('booking_confirmation','BookingController@bookingConfirmation');

});

Route::group(['prefix' => 'v1','namespace' => 'Api','middleware'=>'enforcejson'],function(){

	Route::post('getUserNumber', 'v1\UserController@getUserNumber');
	Route::post('login', 'v1\UserController@login');
	Route::post('register', 'v1\UserController@register');
	Route::post('isMobileExists', 'v1\UserController@isMobileExists');
	Route::post('isEmailExists', 'v1\UserController@isEmailExists');
	Route::post('isMobileRegisted', 'v1\PasswordResetController@isMobileRegisted');
	Route::post('forgotPassword', 'v1\PasswordResetController@forgotPassword');
	Route::post('matchOtp', 'v1\PasswordResetController@matchOtp');
	Route::post('resetpassword', 'v1\PasswordResetController@resetpassword');
	Route::post('countryList', 'v1\UserController@CountryList');
	Route::post('app_setting', 'v1\UserController@app_setting');
	Route::post('refreshToken', 'v1\UserController@refreshToken');
	Route::get('country_info', 'v1\UserController@country_info');

	Route::group(['middleware' => 'auth:api'], function(){

		Route::post('send_message', 'v1\MessageController@sendMessage');
		Route::post('get_user_inbox', 'v1\MessageController@getUserInbox');
		Route::post('get_detail_messages', 'v1\MessageController@getDetailMessages');

		Route::post('logout', 'v1\UserController@logout');
		Route::get('acountData', 'v1\UserController@profileData');
		Route::get('driverDocuments', 'v1\UserController@driverDocuments');

		Route::post('change_password', 'v1\UserController@change_password');
		// Route::post('update_email', 'v1\UserController@update_email');
		Route::post('update_mobileno', 'v1\UserController@update_mobileno');
		Route::post('office_info', 'v1\UserController@office_info');
		
		Route::post('updateProfilePic', 'v1\ProfileController@updateProfilePic');
		Route::post('deleteProfilePic', 'v1\ProfileController@DeleteProfilePic');
		Route::post('update_profile', 'v1\ProfileController@update_profile');
		// Route::post('update_driver_info', 'v1\ProfileController@update_driver_info');
		Route::get('get_driver_info', 'v1\ProfileController@get_driver_info');

		Route::post('update_driver_languages', 'v1\ProfileController@update_driver_languages');
		// Route::get('get_driver_languages', 'v1\ProfileController@get_driver_languages');
		Route::post('update_driver_address', 'v1\ProfileController@update_driver_address');
		// Route::get('get_driver_address', 'v1\ProfileController@get_driver_address');
		Route::post('update_driver_additional_info', 'v1\ProfileController@update_driver_additional_info');
		// Route::get('get_driver_additional_info', 'v1\ProfileController@get_driver_additional_info');
		
		Route::post('switch_to_driver_mode', 'v1\UserController@switch_to_driver_mode');
		Route::post('create_trip', 'v1\TripController@create_trip');
		Route::post('search_trip', 'v1\TripController@search_trip');
		Route::post('search_places', 'v1\TripController@search_places');
		Route::post('search_cities', 'v1\TripController@search_cities');
		Route::post('search_places_by_city', 'v1\TripController@search_places_by_city');
		Route::post('vehicle_info', 'v1\UserController@vehicle_info');
		Route::post('trip_rules', 'v1\TripController@trip_rules');

		Route::post('book_trip', 'v1\TripController@book_trip');
		Route::post('start_trip', 'v1\TripController@start_trip');
		Route::post('complete_trip', 'v1\TripController@complete_trip');
		Route::post('cancel_trip', 'v1\TripController@cancel_trip');
		Route::get('active_trips', 'v1\TripController@active_trips');
		Route::get('upcoming_trips', 'v1\TripController@upcoming_trips');

		Route::get('completed_trips', 'v1\TripController@completed_trips');
		Route::get('driver_active_trips', 'v1\TripController@driver_active_trips');
		Route::get('driver_upcoming_trips', 'v1\TripController@driver_upcoming_trips');
		Route::post('pick_up_rider', 'v1\TripController@pick_up_rider');

		Route::get('driver_completed_trips', 'v1\TripController@driver_completed_trips');
		Route::post('trip_info', 'v1\TripController@trip_info');
		Route::post('user_cancel_trip', 'v1\TripController@user_cancel_trip');
		Route::post('booking_confirmation', 'v1\TripController@booking_confirmation');
		Route::post('trnsaction_history', 'v1\TripController@trnsaction_history');

		//user payment info
		Route::get('retriveCards', 'v1\PaymentController@retriveCards');
		Route::post('addCard', 'v1\PaymentController@addCard');
		Route::post('removeCard', 'v1\PaymentController@removeCard');
		Route::post('setDefaltCard', 'v1\PaymentController@setDefaltCard');
		Route::post('withdraw', 'v1\PaymentController@withdraw');

		Route::post('updateBankDetails','v1\PaymentController@updateBankDetails')->name('user.updateBankDetails');
		Route::post('getBankDetails','v1\PaymentController@getBankDetails')->name('user.getBankDetails');
		Route::post('getUserNotifications', 'v1\NotificationController@getUserNotifications')->name('getUserNotifications');
		Route::post('getDriverNotifications', 'v1\NotificationController@getDriverNotifications')->name('getDriverNotifications');
		Route::post('/sendVarificationCode','v1\UserController@sendVarificationCode');
		Route::post('/verifyEmail','v1\UserController@emailVarification');

		Route::post('/driver_active_trip','v1\TripController@driver_active_trip');
		Route::post('/driver_upcoming_trip','v1\TripController@driver_upcoming_trip');
		Route::post('/driver_completed_trip','v1\TripController@driver_completed_trip');
	});

});
Route::get('v3/clearTripHistory', 'Api\v3\ClearHistoryController@clearTripHistory');
Route::get('v3/clearTripRequests', 'Api\v3\ClearHistoryController@clearTripRequests');

Route::group(['prefix' => 'v3','namespace' => 'Api\v3'],function(){

	Route::post('login', 'AuthController@login');
	Route::post('register', 'AuthController@register');	
	Route::post('refreshToken', 'AuthController@refreshToken');
	Route::post('isMobileExists', 'AuthController@isMobileExists');
	Route::post('isEmailExists', 'AuthController@isEmailExists');
	Route::post('isMobileRegisted', 'PasswordResetController@isMobileRegisted');
	Route::post('resetpassword', 'PasswordResetController@resetpassword');
	Route::get('countryInfo', 'AuthController@countryInfo');

	Route::group(['middleware' => 'jwt-auth'], function(){

		Route::post('switchAccount', 'AuthController@switchAccount');
		Route::post('logout', 'AuthController@logout');
		Route::post('sendVarificationCode','AuthController@sendVarificationCode');
		Route::post('emailVarification','AuthController@emailVarification');
		Route::post('sendMessage', 'MessageController@sendMessage');
		Route::post('getUserInbox', 'MessageController@getUserInbox');
		Route::post('getDetailMessages', 'MessageController@getDetailMessages');
		Route::post('updateProfilePic', 'ProfileController@updateProfilePic');
		Route::post('deleteProfilePic', 'ProfileController@deleteProfilePic');
		Route::post('changePassword', 'ProfileController@changePassword');
		Route::post('updateMobile', 'ProfileController@updateMobile');
		// Route::post('updateProfile', 'ProfileController@updateProfile');
		Route::post('updateDriverLanguages', 'DriverController@updateDriverLanguages');
		Route::post('vehicleInfo', 'DriverController@vehicleInfo');
		Route::post('updateDriverAddress', 'DriverController@updateDriverAddress');
		Route::post('updateDriverAdditionalInfo', 'DriverController@updateDriverAdditionalInfo');
		Route::get('driverDocuments', 'DriverController@driverDocuments');
		Route::post('officeInfo', 'DriverController@officeInfo');
		Route::post('makeMeOnlineOffline','InCityTripDriverController@makeMeOnlineOffline');

		/** driver trip related apis **/
		Route::group(['prefix' => 'driver'], function(){

			/** driver side api **/
			Route::post('createTrip', 'TripController@createTrip');
			Route::post('editSeats', 'TripController@editSeats');
			Route::post('cancelTrip', 'TripController@cancelTrip');
			Route::post('startTrip', 'TripController@startTrip');
			Route::post('declineBooking', 'TripController@declineBooking');
			Route::post('confirmBooking', 'TripController@confirmBooking');
			Route::post('takeBookingAction', 'TripController@takeBookingAction');
			Route::post('completeTrip', 'TripController@completeTrip');
			Route::post('giveRiderRating', 'TripController@giveRiderRating');

			/** listing **/
			Route::get('upcomingTrips', 'TripController@upcomingTrips');
			Route::get('activeTrips', 'TripController@activeTrips');
			Route::get('completedTrips', 'TripController@completedTrips');
			Route::get('cancelledTrips', 'TripController@cancelledTrips');
			Route::post('tripDetail', 'TripController@tripDetail');

			/** driver earning **/
			Route::get('transactionHistory', 'PaymentController@transactionHistory');
			Route::post('estimatedEarning', 'TripController@estimatedEarning');

			/** payment detail driver **/
			Route::post('updateBankDetails','PaymentController@updateBankDetails');
			Route::post('getBankDetails','PaymentController@getBankDetails');
			Route::post('withdraw','PaymentController@withdraw');
			Route::get('getNotifications', 'NotificationController@getDriverNotifications');

			/** phase 2 incity trips **/
			Route::post('inCityTripRequestDetail','InCityTripDriverController@inCityTripRequestDetail');
			Route::post('acceptOrDeclineRideRequest', 'InCityTripDriverController@acceptOrDeclineRideRequest');
			Route::post('cancelRide', 'InCityTripDriverController@cancelRide');
			Route::post('takeRideAction', 'InCityTripDriverController@takeRideAction');
			Route::post('giveIncityTripRiderRating', 'InCityTripDriverController@giveIncityTripRiderRating');

		});

		/** Rider side api **/
		Route::group(['prefix' => 'rider'], function(){

			Route::get('getWorkingCities','BookingController@getWorkingCities');

			/** trip related rider api **/
			Route::post('searchCities', 'BookingController@searchCities');
			Route::post('searchPlacesByCity', 'BookingController@searchPlacesByCity');
			Route::post('searchTrip', 'BookingController@searchTrip');
			Route::post('bookTheTrip', 'BookingController@bookTheTrip');
			Route::post('markDriverAsNotSeen','Rider\TripController@markDriverAsNotSeen');
			Route::post('cancelBooking', 'BookingController@cancelBooking');
			Route::post('giveRatingToDriver','Rider\TripController@giveRatingToDriver');
			
			/** detail apis **/
			Route::post('tripInfo', 'BookingController@tripInfo');
			Route::post('tripDetail','BookingController@tripDetail');
			Route::post('bookingDetail','BookingController@bookingDetail');
			Route::get('tripMoreDetail','Rider\TripController@tripDetail');

			/** New APIs from Rider Side **/
			Route::get('myTrips', 'BookingController@myTrips');
			Route::get('activeTrips','Rider\TripController@activeTrips');
			Route::get('upcomingTrips','Rider\TripController@upcomingTrips');
			Route::get('completedTrips','Rider\TripController@completedTrips');
			Route::get('cancelledTrips','Rider\TripController@cancelledTrips');

			/** payment detail rider **/
			Route::get('retrieveCards', 'PaymentController@retrieveCards');
			Route::post('addCard', 'PaymentController@addCard');
			Route::post('removeCard', 'PaymentController@removeCard');
			Route::post('setDefaltCard', 'PaymentController@setDefaltCard');
			Route::get('getNotifications', 'NotificationController@getUserNotifications');

			/** phase 2 incity trips **/
			Route::post('seachForTrip', 'InCityTripController@seachForTrip');
			Route::post('requestRide', 'InCityTripController@requestRide');
			Route::post('inCityTripDetail', 'InCityTripController@inCityTripDetail');
			Route::post('cancelRide', 'InCityTripController@cancelRide');
			Route::post('giveIncityTripRatingToDriver','InCityTripController@giveIncityTripRatingToDriver');

		});

	});

});