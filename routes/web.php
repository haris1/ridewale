<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
// 	return view('welcome');
// });

// Route::get('socket', 'Admin\DashboardController@socket');
// Route::get('socket1', 'Admin\DashboardController@socket1');

Route::get('/rfpanel/become_driver', function() {
	return redirect()->route('website.become_driver');
});

Route::get('socket_driver', 'GeneralController@socket');
Route::get('socket_rider', 'GeneralController@socket_patient');


Route::group(['namespace'=>'website','as'=>'website.'],function(){
	Route::get('/','HomeController@index')->name('home');
	Route::get('become_driver','HomeController@becomeDriver')->name('become_driver');
	Route::get('cities','HomeController@cities')->name('cities');
	Route::get('driver_requirements','HomeController@driverRequirements')->name('driver_requirements');
	Route::get('privacy_policy','HomeController@privacyPolicy')->name('privacy_policy');
	Route::get('getting_started','HomeController@gettingStarted')->name('getting_started');
	Route::post('addBecomeDriver','HomeController@addBecomeDriver')->name('addBecomeDriver');
	Route::get('contact_us','HomeController@contactUs')->name('contact_us');
	Route::get('faq','HomeController@faq')->name('faq');
	Route::get('how_to_ride','HomeController@howToRide')->name('how_to_ride');
	Route::get('fares','HomeController@fares')->name('fares');
});

Route::group(['prefix'=> 'rfupanel', 'namespace'=>'Admin\Auth','as'=>'admin.'],function(){

	Route::get('login', 'LoginController@showLoginForm')->name('login');
	Route::post('login', 'LoginController@login');
	Route::post('logout', 'LoginController@logout')->name('logout');

	//password reset routes
	Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
	Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
	Route::post('password/reset', 'ResetPasswordController@reset');
	Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
});

Route::group(['prefix'=> 'rfupanel', 'namespace'=>'Admin','as'=>'admin.','middleware'=>'auth:admin'],function(){
	Route::get('/', 'DashboardController@dashboard')->name('dashboard');
	// other dashboards
	Route::get('/payment', 'DashboardController@payment')->name('payment');

	Route::get('/secondDashboard', 'DashboardController@secondDashboard');
	Route::get('/thirdDashboard', 'DashboardController@thirdDashboard');
	Route::get('/fourthDashboard', 'DashboardController@fourthDashboard');

	/** driver / Rider  **/
	Route::get('getUsersList', 'UserController@getUsersList')->name('users.getUsersList');
	Route::get('userProfileApprove/{id}','AdminUserAccessController@user_profile_approve_admin')->name('users.approve');
	Route::Resource('users','UserController');
	Route::get('editRiderProfile','UserController@edit_rider_profile')->name('rider.edit.profile.data');
	Route::post('riders/update/{id}', 'UserController@update')->name('users.update');

	Route::Resource('drivers','DriverController');

	/* route for listing trip for user and driver */
	Route::get('trips/{id}','TripController@list_user_trips')->name('displayUserTrips');
	Route::get('triplist/{id}','TripController@list_driver_trips')->name('displayDriverTrips');

	/* Admin approve driver profile */
	Route::get('/driverProfileApprove','AdminUserAccessController@driver_profile_approve_admin')->name('driver.approve');
	/* user details page */
	Route::get('/driverDetails/{id}','DriverController@driver_details_function')->name('driver.show');

	/*route for payment listing */
	Route::get('payments/{id}','PaymentController@list_user_payments')->name('displayUserpayments');
	Route::get('paymentlist/{id}','PaymentController@list_Driver_payments')->name('displayDriverpayments');

	/*code for add driver profile */
	Route::get('adddriver_profile/{id}','DriverController@adddriver_profile')->name('adddriver_profile');
	Route::get('addDriverProfile/{id}','DriverController@show_driver_profile')->name('addDriverProfile');
	Route::post('addDriveraddress/{id}','DriverController@add_driver_address')->name('updateDriverProfile');

	// Route::post('drivers/update/{id}', 'DriverController@update')->name('drivers.update');

	/* Manage driver documents */
	Route::Resource('driverManageDocuments','DriverManageController');
	Route::get('drivermanageDocument/{id}','DriverManageController@view')->name('driverManageDocuments.view');

	/*code for documents */
	Route::get('listDocuments/{id}','DriverCarDetailsController@list_documents')->name('listDocuments');
	Route::post('submitDocuments','DriverCarDetailsController@submit_Documents')->name('submitDocuments');
	Route::get('deleteDocuments/{id}','DriverCarDetailsController@delete_document')->name('deleteDocuments');

	/* trip history */
	Route::get('tripHistory','TripController@trip_history_upcoming')->name('upcoming.trips');
	Route::get('completeHistory','TripController@trip_history_complete')->name('complete.trips');
	Route::get('triplist','TripController@get_trip_list')->name('trip.history');
	Route::get('tripdetails/{id}','TripController@get_trip_details')->name('trip.details');
	Route::get('giveBookingRefund/{booking_id}','TripController@giveBookingRefund')->name('trip.giveBookingRefund');
	Route::post('bookingRefund/{booking_id}','TripController@bookingRefund')->name('trip.bookingRefund');

	Route::get('chargeDriver/{trip_id}','TripController@chargeDriver')->name('trip.chargeDriver');
	Route::post('tripCharge/{trip_id}','TripController@tripCharge')->name('trip.tripCharge');

	/* code for list vehicles */
	Route::get('listVehicles/{id}','DriverCarDetailsController@list_vehicles')->name('ViewVehicalesList');
	Route::get('addVehicles/{id}','DriverCarDetailsController@add_vehicles_page')->name('addVehicles');
	Route::post('addVehiclesform','DriverCarDetailsController@add_vehicles')->name('addVehiclesForm');
	Route::post('addVehicles','DriverCarDetailsController@delete_vehicles')->name('deleteVehiclesForm');

	/* Payment history */
	Route::get('paymentHistory','PaymentController@get_payment_history')->name('payment_history');
	Route::get('paymentlist','PaymentController@get_payment_list')->name('payment.history');
	Route::get('/totalEarning', 'PaymentController@totalEarning')->name('totalEarning');

	/* Route for add car details*/
	Route::post('addDriverCarDetails','DriverController@add_car_details')->name('add.carDetails');
	/* driver car details */
	Route::Resource('driverCarDetails','DriverCarDetailsController');
	Route::post('DriverCarDetailsList','DriverCarDetailsController@car_details_list')->name('driverCarDetails.list');
	Route::post('DriverCarDetailsEdit','DriverCarDetailsController@get_edit_car_data')->name('get.edit.data');

	Route::get('getProducts', 'GlobalSettingController@show_products_page')->name('products.product');
	Route::apiResource('products','GlobalSettingController');
	Route::post('appSettingStore', 'GlobalSettingController@appSettingStore')->name('products.appSetting');
	Route::post('adminFees', 'GlobalSettingController@adminFees')->name('products.adminFees');

	Route::post('get_work_countries', 'WorkCountryController@show_work_countries')->name('work_countries.work_countries');
	Route::Resource('work_countries','WorkCountryController');

	Route::post('get_work_cities', 'WorkCityController@show_work_cities')->name('work_cities.work_cities');
	Route::Resource('work_cities','WorkCityController');
	Route::get('get_state/{id}', 'WorkCityController@get_state')->name('work_city.get_state');



	/* STATE */
	Route::post('create_state', 'WorkStateController@create_state')->name('create_state');
	Route::get('getStateList', 'WorkStateController@getStateList')->name('getStateList');
	Route::put('update_state/{id}', 'WorkStateController@update_state')->name('update_state');

	Route::post('get_work_locations', 'WorkLocationController@show_work_locations')->name('work_locations.work_locations');
	Route::get('get_cities_by_country/{id}', 'WorkLocationController@get_cities_by_country')->name('work_locations.get_cities_by_country');
	Route::Resource('work_locations','WorkLocationController');

	//Route::post('getJobList', 'JobHistoryController@getJobList')->name('jobhistory.getJobList');
	//Route::apiResource('jobhistory','JobHistoryController');

	/* Admin approve user profile and send mail */
	Route::get('approveMail','UserController@user_approval_comment_mail')->name('approval.mail');
	/* Admin approve user profile and send mail */
	Route::get('approvedMail','DriverController@driver_approval_comment_mail')->name('approvals.mail');


	/* Car Route */
	Route::Resource('carCompany','CarCompanyController');
	Route::post('carCompanyList','CarCompanyController@list')->name('carcompany.list');
	Route::post('carCompanyname','CarCompanyController@get_edit_company_name')->name('carcompany.edit.name');

	/* OFFICE  INFOMATION*/
	Route::Resource('officeInformations','OfficeInformationController');
	Route::get('createOffice','OfficeInformationController@createOffice')->name('office.create');
	Route::post('addOffice','OfficeInformationController@addOffice')->name('office.add');
	Route::get('listOffice','OfficeInformationController@listOffice')->name('office.list');
	Route::delete('deleteOffice','OfficeInformationController@deleteOffice')->name('office.delete');
	Route::get('editOffice/{id}','OfficeInformationController@editOffice')->name('office.edit');
	Route::post('updateOffice','OfficeInformationController@updateOffice')->name('office.update');

	/* Car types*/
	Route::Resource('carType','CarTypesController');
	Route::post('carTypeList','CarTypesController@list')->name('cartype.list');
	Route::post('carTypename','CarTypesController@get_edit_type_name')->name('cartype.edit.name');
	Route::post('carTypeUpdate/{id}','CarTypesController@update_car_type')->name('carType.update_type');
	/* Car Models */
	Route::Resource('carModel','CarModelController');
	Route::post('carModelList','CarModelController@list')->name('carmodel.list');
	Route::post('carModelname','CarModelController@get_edit_model_name')->name('carmodel.edit.name');

	/* Driver detail page this one for get car detail of a driver */
	Route::post('driverCarDetails','DriverController@get_car_details_of_drivers')->name('driver.cardetails');
	Route::post('driverCarPhoto','DriverController@car_photo')->name('car.photo');
	Route::get('editBankDetails/{id}','DriverController@editBankDetails')->name('editBankDetails');
	Route::post('updateBankDetails','DriverController@updateBankDetails')->name('drivers.updateBankDetails');

	Route::get('editDriverProfile','DriverController@edit_driver_profile')->name('driver.edit.profile.data');
	Route::Resource('manageDocuments','DocumentController');
	Route::get('manageDocument/{id}','DocumentController@view')->name('manageDocuments.view');
	/* ajax category subcategory routes for get car company,type ane model in driver details page */
	Route::get('driverCartype/{id}','DriverController@get_type_by_company')->name('cartype.carcompany');
	Route::post('driverCarmodel','DriverController@get_model_by_type')->name('carmodel.cartype');

	/* ORDER */
	Route::get('orderList','OrderController@index')->name('orderList');
	Route::get('orderDetails/{id}','OrderController@orderDetails')->name('orderDetails');
	Route::get('getOrderList','OrderController@getOrderList')->name('getOrderList');
	Route::get('maps','OrderController@maps')->name('maps');

	//Route::get('cancelledBookings','OrderController@cancelledBookings')->name('cancelledBookings');
	Route::get('/cancelledBookings', 'TripController@cancelledBookings')->name('cancelledBookings');
	Route::get('/getRiderStatus', 'TripController@getRiderStatus')->name('rider_status');
	Route::get('/getDriverStatus', 'TripController@getDriverStatus')->name('driver_status');
	Route::get('/getReportStatus', 'TripController@getReportStatus')->name('report_status');

	/** allowed / notallowed things **/
	Route::post('get_allowed_things', 'AllowedThingsController@show_allowed_things')->name('allowed_things.get_allowed_things');
	Route::post('get_notallowed_things', 'AllowedThingsController@show_notallowed_things')->name('allowed_things.get_notallowed_things');

	Route::resource('allowed_things','AllowedThingsController');

	/** Booking/Trip Cancellation reasons **/
	Route::post('get_rider_trip_cancellation_reasons', 'TripCancellationReasonController@rider_trip_cancellation_reasons')->name('cancellation_reasons.rider_trip_cancellation_reasons');
	Route::post('get_driver_trip_cancellation_reasons', 'TripCancellationReasonController@driver_trip_cancellation_reasons')->name('cancellation_reasons.driver_trip_cancellation_reasons');
	Route::post('get_driver_booking_decline_reasons', 'TripCancellationReasonController@driver_booking_decline_reasons')->name('cancellation_reasons.driver_booking_decline_reasons');

	Route::resource('cancellation_reasons','TripCancellationReasonController');

	/* fare management */
	Route::get('/fareManagement', 'FareManagementController@index')->name('fareManagement');
	Route::post('/addFare', 'FareManagementController@addFare')->name('addFare');
	Route::get('/fareList', 'FareManagementController@fareList')->name('fareList');
	Route::post('/editFare', 'FareManagementController@editFare')->name('editFare');
	Route::delete('/deleteFare/{id}', 'FareManagementController@deleteFare')->name('deleteFare');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
