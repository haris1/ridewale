const express 	 = require('express');
const app 		 = express();
const http 	= require('http').Server(app);
const io 	= require('socket.io')(http);
const bodyParser = require('body-parser');
const _ 	= require('lodash');
var Redis 	= require('ioredis');
var redis 	= new Redis();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

var users 	 = [];

redis.subscribe(['sendMessageChannel']);
redis.subscribe(['newRideRequestChannel']);

redis.on('message', function (channel, message) {

  if (channel == 'sendMessageChannel') {

    var data           = JSON.parse(message);
    var serverResopnse = data.data.data;
    var chatRoomData   = serverResopnse.chat_room_data;
    var roomUsersIds   = serverResopnse.chat_room_user_ids;
    var roomId         = chatRoomData.id;
    var receiverId     = chatRoomData.last_message.receiver_id;
    var senderId       = chatRoomData.last_message.sender_id;
    
    let nowYouSeeMe = _.filter(users, _.matches({ user_id: receiverId, receiver_id: senderId }));

    if ((nowYouSeeMe.length > 0) && nowYouSeeMe[0]) {
      
      let userToJoin = nowYouSeeMe[0].socket;
      userToJoin.join(roomId);
      io.sockets.in(roomId).emit('message_found', chatRoomData);
    }
  }

  if (channel == 'newRideRequestChannel') {

    var data           = JSON.parse(message);
    console.log(data);
    // var serverResopnse = data.data.data;
    // var chatRoomData   = serverResopnse.chat_room_data;
    // var roomUsersIds   = serverResopnse.chat_room_user_ids;
    // var roomId         = chatRoomData.id;
    // var receiverId     = chatRoomData.last_message.receiver_id;
    // var senderId       = chatRoomData.last_message.sender_id;
    
    // let nowYouSeeMe = _.filter(users, _.matches({ user_id: receiverId, receiver_id: senderId }));

    // if ((nowYouSeeMe.length > 0) && nowYouSeeMe[0]) {
      
    //   let userToJoin = nowYouSeeMe[0].socket;
    //   userToJoin.join(roomId);
    //   io.sockets.in(roomId).emit('message_found', chatRoomData);
    // }
  }
});

io.on('connection', function (socket) {
	
  let user_id     = parseInt(socket.handshake.query.user_id);
  let receiver_id     = parseInt(socket.handshake.query.receiver_id);
  let user_name   = socket.handshake.query.user_name;
  
  // add the socket to the connected sockets list
  console.log(user_id);
  console.log(user_name);
  console.log(receiver_id);
  users.push({user_id:user_id,socket:socket,socket_id:socket.id,user_name:user_name,receiver_id:receiver_id});

  // check if user which recently connected is already exists with user_id or not
  let userExists = _.findIndex(users, function(o) { return o.user_id == user_id; });
  
  // leave the room once the user disconnected
  socket.on('room_leave', (roomId) => {
    console.log('Participant from room id: '+roomId+' is leaving.');
    socket.leave(roomId);
  });

  // if socket disconnected then remove that socket from the list of connected sockets
  socket.on('disconnect', () => {
    _.pullAllWith(users, [{ socket_id: socket.id }], _.isMatch);
    console.log('A socket having socket id : '+socket.id+' is disconnected from server');
    console.log('--------------- Available users are as follow ---------------');
    console.log(users);
  });
});

http.listen(4002, ()=> {
	console.log((process.env.NODE_ENV === 'production')?"Production":"Local"+" Server is started on port number 4002");
});