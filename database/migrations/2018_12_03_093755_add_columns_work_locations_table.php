<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsWorkLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('work_locations', function (Blueprint $table) {
            $table->string('city')->after('longitude')->nullable();
            $table->string('country')->after('city')->nullable();
            $table->string('postalCode')->after('country')->nullable();
            $table->string('state')->after('postalCode')->nullable();
            $table->string('street')->after('state')->nullable();
            $table->string('placeName')->after('street')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('work_locations', function (Blueprint $table) {
            $table->dropColumn(['city','country','postalCode','state','street','placeName']);
        });
    }
}
