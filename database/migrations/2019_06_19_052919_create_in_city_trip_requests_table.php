<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInCityTripRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('in_city_trip_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('incity_trip_id')->unsigned();
            $table->foreign('incity_trip_id')->references('id')->on('in_city_trips')->onDelete('cascade');
            $table->integer('driver_id')->unsigned();
            // $table->foreign('driver_id')->references('id')->on('drivers')->onDelete('cascade');           
            $table->tinyInteger('status')->default(0);
            $table->timestamp('cancelled_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('in_city_trip_requests');
    }
}
