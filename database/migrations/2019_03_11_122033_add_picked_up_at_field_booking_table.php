<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPickedUpAtFieldBookingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->timestamp('picked_up_at')->nullable()->after('luggage');
            $table->timestamp('no_show_at')->nullable()->after('luggage');
            $table->integer('otp')->nullable()->after('luggage');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn(['picked_up_at','otp','no_show_at']);
        });
    }
}
