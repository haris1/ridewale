<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayoutTripTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payout_trip', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('trip_id')->unsigned();
            // $table->foreign('trip_id')->references('id')->on('trips')->onDelete('cascade');
            $table->integer('payout_id')->unsigned();
            // $table->foreign('payout_id')->references('id')->on('payouts')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payout_trip');
    }
}
