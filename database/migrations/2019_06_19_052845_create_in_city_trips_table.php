<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInCityTripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('in_city_trips', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->text('from_location');
            $table->text('to_location');
            $table->enum('status',['pending', 'scheduled', 'picked_up', 'drop_off', 'cancelled'])->default('pending');
            $table->float('price',8,2)->default(0);
            $table->float('distance',8,2)->default(0);
            $table->timestamp('pickup_time')->nullable();
            $table->timestamp('dropoff_time')->nullable();
            $table->unsignedInteger('vehicle_id')->nullable();
            $table->integer('total_seats')->default(0);
            $table->integer('otp')->nullable();
            $table->timestamp('rider_cancelled_at')->nullable();
            $table->timestamp('driver_cancelled_at')->nullable();
            $table->string('duration',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('in_city_trips');
    }
}
