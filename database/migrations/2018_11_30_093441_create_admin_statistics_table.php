<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_statistics', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('transfer_cent',8,2)->default(0);
            $table->decimal('transfer_fees',8,2)->default(0);
            $table->decimal('admin_cent',8,2)->default(0);
            $table->decimal('admin_fees',8,2)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_statistics');
    }
}
