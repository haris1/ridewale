<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLocationIdColumnsTripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trips', function (Blueprint $table) {
           
            $table->integer('from_city_id')->unsigned()->after('driver_id');
            $table->foreign('from_city_id')->references('id')->on('work_cities')->onDelete('cascade');
            $table->integer('to_city_id')->unsigned()->after('from_city_id');
            $table->foreign('to_city_id')->references('id')->on('work_cities')->onDelete('cascade');
            $table->integer('pickup_id')->unsigned()->after('trip_date');
            $table->foreign('pickup_id')->references('id')->on('work_locations')->onDelete('cascade');
            $table->integer('dropoff_id')->unsigned()->after('pickup_id');
            $table->foreign('dropoff_id')->references('id')->on('work_locations')->onDelete('cascade');
            $table->datetime('dropoff_time')->after('pickup_time')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trips', function (Blueprint $table) {
            $table->dropColumn(['pickup_id','dropoff_id']);
        });
    }
}
