<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFareManagementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fare_managements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('to_city_id')->unsigned();
            $table->foreign('to_city_id')->references('id')->on('work_cities')->onDelete('cascade');
            $table->integer('from_city_id')->unsigned();
            $table->foreign('from_city_id')->references('id')->on('work_cities')->onDelete('cascade');            
            $table->float('price',8,2);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fare_managements');
    }
}
