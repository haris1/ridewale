<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_locations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->decimal('latitude',9,6)->nullable();
            $table->decimal('longitude',9,6)->nullable();
            $table->integer('work_city_id')->unsigned();
            $table->foreign('work_city_id')->references('id')->on('work_cities')->onDelete('cascade');
            $table->integer('work_country_id')->unsigned();
            $table->foreign('work_country_id')->references('id')->on('work_countries')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_locations');
    }
}
