<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('payment_id')->unsigned();
            $table->foreign('payment_id')->references('id')->on('payments')->onDelete('cascade');            
            $table->string('transaction_id',255);
            $table->string('charge_id',255);
            $table->decimal('amount',8,2);
            $table->string('charged_currency',10)->nullable();
            $table->timestamp('transaction_time')->nullable();
            $table->string('failure_code',20)->nullable();
            $table->string('failure_message',255)->nullable();
            $table->string('paid',10)->nullable();
            $table->string('status',20)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
