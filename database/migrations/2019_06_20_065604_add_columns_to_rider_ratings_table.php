<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToRiderRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rider_ratings', function (Blueprint $table) {
         $table->integer('booking_id')->unsigned()->nullable()->change();
         $table->integer('user_id')->unsigned()->nullable();
         // $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
         $table->integer('incity_trip_id')->unsigned()->nullable();
         // $table->foreign('incity_trip_id')->references('id')->on('in_city_trips')->onDelete('cascade');
         $table->text('description')->nullable();
     });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rider_ratings', function (Blueprint $table) {
            $table->dropColumn(['user_id','incity_trip_id','description']);
        });
    }
}
