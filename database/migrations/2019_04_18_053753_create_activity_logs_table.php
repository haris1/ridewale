<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivityLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('trip_id')->unsigned();
            $table->integer('booking_id')->unsigned()->nullable();
            $table->integer('driver_id')->unsigned();
            $table->integer('rider_id')->unsigned()->nullable();
            $table->enum('activity',['newTrip','updateSeat','updateTrip','newBooking','tripCancelled','bookingDeclined','bookingCancelled','bookingConfirmed','noShowByDriver','noShowByRider','tripStarted','tripCompleted','pickedUp','dropOff']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_logs');
    }
}
