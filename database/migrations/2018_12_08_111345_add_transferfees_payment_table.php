<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTransferfeesPaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->double('transfer_fees', 8, 2)->default(0)->after('admin_fees');
            $table->enum('user_withdrow_status',['0','1'])->nullable();
            $table->enum('status',['0','1','2','3'])->default('0')->after('total_person');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->dropColumn(['transfer_fees','user_withdrow_status']);
        });
    }
}
