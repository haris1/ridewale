<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('refunds', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('payment_id')->nullable();
            // $table->foreign('payment_id')->references('id')->on('payments')->onDelete('cascade'); 
            $table->integer('transaction_id')->nullable();
            // $table->foreign('transaction_id')->references('id')->on('transactions')->onDelete('cascade'); 
            $table->string('refund_id',255);
            $table->string('charge_id',255);
            $table->float('amount',8,2);
            $table->float('refund_charge',8,2);
            $table->float('refund_charge_cent',8,2);
            $table->timestamp('transaction_time')->nullable();
            $table->string('status',20)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('refunds');
    }
}
