<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRiderRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rider_ratings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('booking_id')->unsigned();
            // $table->foreign('booking_id')->references('id')->on('bookings')->onDelete('cascade');
            $table->float('rating', 2, 1)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rider_ratings');
    }
}
