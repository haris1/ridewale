<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCancellationReasonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trip_cancellation_reasons', function (Blueprint $table) {
            $table->enum('reason_for',['decline_booking','cancel_trip'])->default('cancel_trip');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trip_cancellation_reasons', function (Blueprint $table) {
            $table->dropColumn('reason_for');
        });
    }
}
