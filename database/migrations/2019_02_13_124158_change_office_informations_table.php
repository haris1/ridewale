<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeOfficeInformationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('office_informations', function (Blueprint $table) {
            $table->string('city',100)->change();
            $table->string('country',100)->change();
            $table->string('postal_code',20)->change();
            $table->string('longitude',100)->change();
            $table->string('latitude',100)->change();
            //$table->text('full_address');
            $table->string('work_days',100)->change();
            $table->string('inquiry_days',100)->change();
            $table->dropColumn('open_hour')->nullable();
            $table->dropColumn('close_hour')->nullable();
            $table->dropColumn('open_inquiry')->nullable();
            $table->dropColumn('close_inquiry')->nullable();
            $table->string('phone',50)->change();
            $table->string('email',225)->change();
            $table->renameColumn('work_days', 'location_hours');
            $table->renameColumn('inquiry_days', 'inquiry_hours');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
