<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_models', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('car_company_id')->unsigned();
            $table->foreign('car_company_id')->references('id')->on('car_companies')->onDelete('cascade');
            $table->integer('car_type_id')->unsigned();
            $table->foreign('car_type_id')->references('id')->on('car_types')->onDelete('cascade');
            $table->string('model_name');
            //$table->enum('capacity',['1','2','3','4']);
            $table->integer('capacity');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_models');
    }
}
