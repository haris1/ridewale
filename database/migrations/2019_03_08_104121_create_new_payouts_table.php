<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewPayoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payouts', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('payout_amount',8,2);
            $table->string('payout_transaction_id',255);
            $table->timestamp('payout_transaction_time')->nullable();
            $table->string('payout_failure_code',20)->nullable();
            $table->string('payout_failure_message',255)->nullable();
            $table->string('payout_status',20)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payouts');
    }
}
