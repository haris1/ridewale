<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewTripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trips', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('from_location_id');
            // $table->foreign('from_location_id')->references('id')->on('work_locations')->onDelete('cascade');
            $table->unsignedInteger('to_location_id');
            // $table->foreign('to_location_id')->references('id')->on('work_locations')->onDelete('cascade');
            $table->unsignedInteger('driver_id');
            // $table->foreign('driver_id')->references('id')->on('drivers')->onDelete('cascade');
            $table->timestamp('trip_date');
            $table->timestamp('pickup_time');
            $table->timestamp('dropoff_time');
            $table->unsignedInteger('vehicle_id');
            // $table->foreign('vehicle_id')->references('id')->on('car_drivers')->onDelete('cascade');
            $table->integer('total_seats')->default(0);
            $table->integer('available_seats')->default(0);
            $table->float('price', 8, 2)->default(0);
            $table->enum('status',['scheduled','started','completed','cancelled'])->default('scheduled');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trips');
    }
}
