<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfficeInformationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('office_informations', function (Blueprint $table) {
                $table->increments('id');
                $table->string('city');
                $table->string('country');
                $table->string('postal_code');
                $table->string('longitude');
                $table->string('latitude');
                $table->text('full_address');
                $table->string('work_days');
                $table->string('inquiry_days');
                $table->timestamp('open_hour')->nullable();
                $table->timestamp('close_hour')->nullable();
                $table->timestamp('open_inquiry')->nullable();
                $table->timestamp('close_inquiry')->nullable();
                $table->string('phone');
                $table->string('email');
                $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('office_informations');
    }
}
