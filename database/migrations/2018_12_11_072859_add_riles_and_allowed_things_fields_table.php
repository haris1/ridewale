<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRilesAndAllowedThingsFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('car_drivers', function (Blueprint $table) {
            $table->longText('rules_and_regulations')->nullable()->after('color');
            $table->longText('allowed_things')->nullable()->after('rules_and_regulations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('car_drivers', function (Blueprint $table) {
            //
        });
    }
}
