<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('booking_id')->unsigned();
            // $table->foreign('booking_id')->references('id')->on('bookings')->onDelete('cascade');
            $table->float('amount', 8, 2)->default(0);
            $table->string('transaction_id',255);
            $table->string('charge_id',255);
            $table->string('payment_status',20)->nullable();
            $table->timestamp('transaction_time')->nullable();
            $table->string('refunded_id',255);
            $table->float('refunded_amount',8,2);
            $table->string('refund_status',20)->nullable();
            $table->timestamp('refunded_time')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
