<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToDriverRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('driver_ratings', function (Blueprint $table) {
         $table->integer('booking_id')->unsigned()->nullable()->change();
         $table->integer('driver_id')->unsigned()->nullable();
         // $table->foreign('driver_id')->references('id')->on('drivers')->onDelete('cascade');
         $table->enum('trip_type',['intercity','incity'])->default('intercity');
         $table->text('description')->nullable();
     });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('driver_ratings', function (Blueprint $table) {
            //
        });
    }
}
