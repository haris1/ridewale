<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trips', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('driver_id')->unsigned();
            $table->foreign('driver_id')->references('id')->on('drivers')->onDelete('cascade');
            $table->string('from');
            $table->string('to');
            $table->date('trip_date');
            $table->string('pickup_location');
            $table->decimal('pickup_lat',9,6)->nullable();
            $table->decimal('pickup_long',9,6)->nullable();
            $table->string('dropoff_location');
            $table->decimal('dropoff_lat',9,6)->nullable();
            $table->decimal('dropoff_long',9,6)->nullable();
            $table->time('pickup_time')->nullable();
            $table->integer('total_seats');
            $table->integer('available_seats')->nullable();
            $table->decimal('price',8,2)->nullable();
            $table->enum('status',['upcoming','active','completed','decline'])->default('upcoming');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trips');
    }
}
