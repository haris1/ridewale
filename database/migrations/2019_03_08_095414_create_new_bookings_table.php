<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('trip_id')->unsigned();
            // $table->foreign('trip_id')->references('id')->on('trips')->onDelete('cascade');
            $table->integer('user_id')->unsigned();
            // $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('seats')->default(0);
            
            $table->float('base_fare', 8, 2)->default(0);
            $table->float('tax_fare', 8, 2)->default(0);
            $table->float('transfer_fare', 8, 2)->default(0);
            $table->float('chargeable_amount', 8, 2)->default(0);
            $table->float('admin_fare', 8, 2)->default(0);
            $table->float('driver_fare', 8, 2)->default(0);
            
            $table->enum('admin_fare_type',['fixed','percentage'])->default('percentage');
            $table->enum('transfer_fare_type',['fixed','percentage'])->default('percentage');
            $table->enum('tax_fare_type',['fixed','percentage'])->default('percentage');
            
            $table->float('admin_percentage', 8, 2)->default(0);
            $table->float('transfer_percentage', 8, 2)->default(0);
            $table->float('tax_percentage', 8, 2)->default(0);
            
            $table->enum('status',['pending','confirmed','cancelled','picked_up','no_show','drop_off'])->default('pending');


            $table->tinyInteger('handshake')->default(0);
            $table->tinyInteger('luggage')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
