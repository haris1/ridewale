<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeclinedBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('declined_bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('driver_id');
            // $table->foreign('driver_id')->references('id')->on('drivers')->onDelete('cascade');
            $table->integer('booking_id')->unsigned();
            // $table->foreign('booking_id')->references('id')->on('bookings')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('declined_bookings');
    }
}
