<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWithdrawsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('withdraws', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('driver_id')->unsigned();
            $table->foreign('driver_id')->references('id')->on('drivers')->onDelete('cascade');   
            $table->string('transaction_id',255);
            $table->decimal('amount',8,2);
            $table->date('transaction_time')->nullable();
            $table->string('failure_code',20)->nullable();
            $table->string('failure_message',255)->nullable();
            $table->string('status',20)->nullable();
            $table->string('payout_transaction_id',255);
            $table->date('payout_transaction_time')->nullable();
            $table->string('payout_failure_code',20)->nullable();
            $table->string('payout_failure_message',255)->nullable();
            $table->string('payout_status',20)->nullable();
            $table->text('on_behalf_of')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('withdraws');
    }
}
