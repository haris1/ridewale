<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriverPenaltiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('driver_penalties', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('trip_id');
            // $table->foreign('trip_id')->references('id')->on('requests')->nullable();
            $table->float('penalty_amount',8,2);
            $table->enum('penalty_type',['fixed','percentage']);
            $table->unsignedInteger('driver_id');
            // $table->foreign('driver_id')->references('id')->on('drivers')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('driver_penalties');
    }
}
