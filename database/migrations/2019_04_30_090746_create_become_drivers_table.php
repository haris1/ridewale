<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBecomeDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('become_drivers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name',100);
            $table->string('last_name',100);
            $table->string('phone_number',100);
            $table->enum('gender',['male','female'])->default('male');
            $table->string('email');
            $table->string('currently_live',100);
            $table->enum('going_to_drive',['5_times_week','3_times_week','once_week','once_month'])->default('5_times_week');
            $table->string('most_frequently',255);
            $table->enum('ridesharing_company',['yes','no'])->default('yes');
            $table->string('company_driving_name',255)->nullable();
            $table->enum('interested_driving',['full_time','part_time','occasionally']);
            $table->enum('work_permit',['yes','no']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('become_drivers');
    }
}
