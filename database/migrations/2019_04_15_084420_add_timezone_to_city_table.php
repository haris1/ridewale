<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTimezoneToCityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('work_cities', function (Blueprint $table) {
            $table->string('timezone_name')->nullable()->after('name');
            $table->string('timezone')->nullable()->after('timezone_name');
            // $table->string('timezone_carbon')->nullable()->after('timezone');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('work_cities', function (Blueprint $table) {
            //
        });
    }
}
