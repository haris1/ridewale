<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserTypeTripCancellationReasonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trip_cancellation_reasons', function (Blueprint $table) {
            $table->enum('user_type',['user','driver'])->after('status')->default('user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trip_cancellation_reasons', function (Blueprint $table) {
            $table->dropColumn('user_type');
        });
    }
}
