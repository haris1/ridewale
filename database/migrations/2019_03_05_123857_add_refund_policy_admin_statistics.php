<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRefundPolicyAdminStatistics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admin_statistics', function (Blueprint $table) {
            $table->text('refund_policy')->nullable();
            $table->text('cancellation_policy')->nullable();
            $table->enum('cancellation_charge_type',['$','%'])->default('%');
            $table->decimal('cancellation_charge',8,2)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('admin_statistics', function (Blueprint $table) {
            $table->dropColumn(['refund_policy','cancellation_policy','cancellation_charge_type','cancellation_charge']);
        });
    }
}
