const express 	 = require('express');
const app 		 = express();
const http 	= require('http').Server(app);
const io 	= require('socket.io')(http);
const bodyParser = require('body-parser');
const _ 	= require('lodash');
var Redis 	= require('ioredis');
var redis 	= new Redis();
// let AuthController 	= require('./node_storage/middleware/Authentication');
// let socketMania 	= require('./node_storage/controller/SocketController');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

let users 	 = [];

// io.on('connection', function(socket){
	
// 	console.log('a user with socket id '+socket.id+' connected');
	
// 	let user_id 	= parseInt(socket.handshake.query.user_id);
// 	let user_type 	= parseInt(socket.handshake.query.user_type);
	
// 	//push only if new user is connected
// 	// if (!_.isMatch(connectedUsers, [{ user_id: user_id }])) {
// 	// 	connectedUsers.push({user_id:user_id,socket_id:socket.id,user_type:user_type});
// 	// }


//  // socket.on('disconnect', () => {
//  //  		// remove the socket from the connected users which is going to be disconnected
//  //  		_.pullAllWith(connectedUsers, [{ socket_id: socket.id }], _.isMatch);
//  //  	});

// 	// switch(user_type){
// 	// 	case 'driver':
// 	// 			if (!_.isMatch(connectedDrivers, [{ user_id: user_id }])) {
// 	// 				connectedDrivers.push({user_id:user_id,socket_id:socket.id,user_type:user_type});
// 	// 			}
// 	// 		break;
// 	// 	case 'user':
// 	// 			if (!_.isMatch(connectedRiders, [{ user_id: user_id }])) {
// 	// 				connectedRiders.push({user_id:user_id,socket_id:socket.id,user_type:user_type});
// 	// 			}
// 	// 		break;
// 	// 	default:
// 	// 		socket.emit('error_found',{message:'Invalid User Type Found'});
// 	// 		break;
// 	// }
// });


io.on('connection', function (socket) {
	
	redis.subscribe(['sendMessageChannel']);
  	redis.on('message', function (channel, message) {
  		let data = JSON.parse(message);
    	if (channel == 'sendMessageChannel') {
    		socket.emit('message_found', {data:data});
    	}
  	});

	let user_id 	= parseInt(socket.handshake.query.user_id);
	let user_name 	= socket.handshake.query.user_name;
	
	// add him to the connected users list
	users.push({user_id:user_id,socket_id:socket.id,user_name:user_name});

	//check if user which recently connected is already exists with user_id or not
	let userExists = _.findIndex(users, function(o) { return o.user_id == user_id; });
	
	socket.on('typing', (data) => {
		var matchedResults = _.filter(users, {user_id:data.user_id});
		matchedResults.forEach((key,val) => {
			let socketId = val.socket_id;
			io.to(socketId).emit('typing_response',{message:matchedResults});
		});
	});

	socket.on('typing_stops', (data) => {
		var matchedResults = _.filter(users, {user_id:data.user_id});
		matchedResults.forEach((key,val) => {
			let socketId = val.socket_id;
			io.to(socketId).emit('typing_stops_response',{message:matchedResults});
		});
	});

	socket.on('disconnect', () => {
  		_.pullAllWith(users, [{ socket_id: socket.id }], _.isMatch);
  	});
});

// app.use(AuthController.areYouFromOtherPlanet);
// app.use('/node/v1',socketMania);

http.listen(4001, ()=> {
	console.log((process.env.NODE_ENV === 'production')?"Production":"Local"+" Server is started on port number 4001");
});