<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
      protected $fillable = [
		'name','code','dialing_code'
	];

	public function users(){
        return $this->hasMany('App\User','country_code','code');
    }
}
