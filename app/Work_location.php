<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Work_location extends Model
{
    protected $fillable = [
		'name','latitude','longitude','work_city_id','work_country_id','city','country','postalCode','state','street','placeName','place_id'
	];

	public function work_city(){
		return $this->belongsTo('App\Work_city','work_city_id');
	}

	public function work_country(){
		return $this->belongsTo('App\Work_country','work_country_id');
	}

	public function pickup_location(){
		return $this->hasMany('App\Trip','pickup_id','id');
	}

	public function dropoff_location(){
		return $this->hasMany('App\Trip','dropoff_id','id');
	}
}
