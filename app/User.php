<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
// use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable; //HasApiTokens

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password','dob','gender','mobile_no','profile_verified','languages_known','stripe_id','additional_info','address','profile_pic','firebase_android_id','firebase_ios_id','otp','country_code','is_approved','living_address'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'pivot'
    ];

    protected $casts = [
        'languages_known' => 'json',
        'address' => 'array',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function trips(){
        return $this->hasMany('App\Trip','driver_id','id');
    }

    public function driver(){
        return $this->hasOne('App\Driver','user_id','id');
    }

    public function payments(){
        return $this->hasMany('App\Payment','user_id','id');
    }

    public function getLanguagesKnownAttribute($value)
    {
        return (empty($value))?[]:json_decode($value);
    }
    // public function messages(){
    //     return $this->hasMany('App\Message','sender_id');
    // }

    // public function getProfilePicAttribute($value){
    //     if(is_null($value)){
    //         return "";
    //     }else{
    //         return asset('images/users/'.$this->id.'/profile_pic/'.$value);
    //     }
    // }

    public function chat_rooms()
    {
        return $this->belongsToMany('App\ChatRoom','room_participants')->withTimestamps();
    }

    public function user_country(){
        return $this->belongsTo('App\Country','country_code','code');
    }

    public function getProfilePicAttribute($value){
        if ($value=="") {
            $profile_pic = '';
        } else {
            $profile_pic = $value;
        }
        return $profile_pic;
    }

}
