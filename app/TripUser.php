<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TripUser extends Model
{
     protected $fillable = [
       'trip_id', 'user_id', 'total_seats'
    ];

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }

    public function trip(){
        return $this->belongsTo('App\Trip','trip_id');
    }
}
