<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Work_state extends Model
{
    protected $fillable = ['name','tax_percentage','work_country_id','created_at','updated_at'];

    public function work_country(){
		return $this->belongsTo('App\Work_country','work_country_id');
	}

	public function work_cities(){
		return $this->hasMany('App\Work_city');
	}
}
