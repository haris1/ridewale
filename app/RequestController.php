<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Sidewalk;
use App\Driveway;
use App\Service;
use App\Request as Req;
use App\CityTax;
use App\Admin_statistic;
use App\User;
use App\MailTemplate;
use DB;
use Validator;
use Route;
use Auth;
use App\Events\PublishRequestChannel;
use App\Events\RequestAcceptedChannel;
use App\Transaction_history;
use App\Snow_angel;
use App\UserReward;
use Notification;
use App\Notifications\JobAcceptedNotification;
use App\Notifications\JobAcceptedSnowangelNotification;
use Image;
use App\Notifications\RequestPlow;
use App\Helpers\NotificationHelper;
use Stripe\Stripe;
use Stripe\Customer;
use Carbon\Carbon;
use UserHelper;

class RequestController extends Controller
{

	public function __construct(){
		Stripe::setApiKey(config('services.stripe.secret'));
	}

	//get an estimation price
	public function getEstimation(Request $request)
	{

		$validator = Validator::make($request->all(),[
			'place_latitude' 	=> 'required|numeric',
			'place_longitude' => 'required|numeric',
			'sidewalk_id' => 'required|exists:sidewalks,id',
			'driveway_id' 	=> 'required|exists:driveways,id',
			'temprature' => 'required|nullable',
			'season_type' => 'required|in:rain,snow,nothing',
			'like_walkway_service'=>'required'
		]);

		if ($validator->fails()){
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
		}

		//get variables
		$latitude 			= $request->get('place_latitude');
		$longitude 			= $request->get('place_longitude');
		$sidewalkId 		= $request->get('sidewalk_id');
		$drivewayId 		= $request->get('driveway_id');
		$serviceWalkwayIds 	= array(1);
		$temprature 		= $request->get('temprature');
		$season_type 		= $request->get('season_type');
		$location_name 		= $request->get('location_name');
		$likeWalkwayService = $request->get('like_walkway_service');
		
		//calculation logic
		$sidewalk = Sidewalk::find($sidewalkId);
		$driveway = Driveway::find($drivewayId);
		$sidewalkRate = $sidewalk->rate;
		$drivewayRate = $driveway->rate;
		$sidewalkName = $sidewalk->name;
		$drivewayName = $driveway->name;

		$hasServiceWalkways = false;
		$servicWalkwayRate = 0;
		$servicWalkway = [];

		if ($likeWalkwayService == 'true') {
			$servicWalkwayRate = ($sidewalk->salt_rate + ($driveway->salt_rate * (int)$sidewalkName));
			$servicWalkway[0]['name'] = 'salt';
			$servicWalkway[0]['rate'] = $servicWalkwayRate;
			$hasServiceWalkways = true;
		}

		$admin_statistic 	= Admin_statistic::find(1);
		$transferFees 		= $admin_statistic->transfer_fees;
		$transferCent 		= $admin_statistic->transfer_cent;
		$minDistance 		= $admin_statistic->min_distance;
		$minTemprature 		= $admin_statistic->min_temprature;
		$tempCentCharge 	= $admin_statistic->temprature_cent_charge;
		$tempFixedCharge 	= $admin_statistic->temprature_fixed_charge;
		$perKMCharge 		= $admin_statistic->per_km_charge;
		
		//calculate material charges
		$subTotal = ($sidewalkRate + $drivewayRate + $servicWalkwayRate);

		//calculate the minimum percipIntensity in mm
		if ($season_type == 'snow' && $tempCentCharge > 0 && $minTemprature <= $temprature) {
			$percentageAmount = (($subTotal * $tempCentCharge)/100);
			$subTotal += round($percentageAmount,2);
		}else if($season_type == 'snow' && $tempFixedCharge > 0 && $minTemprature <= $temprature){
			$subTotal = $subTotal + $tempFixedCharge;
		}

		//calculate distance of angels
		$distanceQueryResult = User::where('active_mode','snow_angel')
		->with(['snow_angel' => function($query){
			$query->where('status','Active');
			$query->select('id as snow_angel_id','user_id');
		}])
		->having('distance', '<', $minDistance)
		->whereHas('snow_angel', function($query){
			$query->whereHas('requests', function($query){
				$query->where('status','!=','2');
			},'<',5);
		})
		->select(DB::raw('ROUND(( 6373 * acos ( cos ( radians('.$latitude.') ) * cos( radians( current_latitude ) ) * 
			cos( radians( current_longitude ) - radians('.$longitude.') ) + sin ( radians('.$latitude.') ) * 
			sin( radians( current_latitude ) ) ) ),1) AS `distance`'),'id','firebase_android_id','firebase_ios_id')
		->get();
		// print_r($distanceQueryResult->toArray());
		// exit;
		$noOfAngelsAvailable = count($distanceQueryResult);

		$minimumSubTotal = (($distanceQueryResult->min('distance')*$perKMCharge) + $subTotal);
		$maximumSubtotal = (($distanceQueryResult->max('distance')*$perKMCharge) + $subTotal);

		//admin fees
		if ($transferCent) {
			$minimumTotal = ($minimumSubTotal + (($minimumSubTotal * $transferCent)/100));
			$maximumTotal = ($maximumSubtotal + (($maximumSubtotal * $transferCent)/100));
		}else if($transferFees){
			$minimumTotal = $minimumSubTotal + $transferFees;
			$maximumTotal = $maximumSubtotal + $transferFees;
		}

		$data['minimum_total'] = number_format(round($minimumTotal,2),2);
		$data['maximum_total'] = number_format(round($maximumTotal,2),2);
		$data['no_of_angel_available'] = $noOfAngelsAvailable;
		$data['radius'] 	= $minDistance;
		if($likeWalkwayService == 'true'){
			$sidewalkRate=$sidewalkRate+$sidewalk->salt_rate;
			$drivewayRate=$drivewayRate+$driveway->salt_rate;
		}
		$tax1=CityTax::where('city_name',$request->get('place_city'))->first();

		if(count($tax1) > 0){
			$tax=$tax1->tax_percentage;
			$minimumTotal+=($tax*$minimumTotal)/100;
			$maximumTotal+=($tax*$maximumTotal)/100;
		}else{
			$tax2=CityTax::where('city_name','Custom')->first();
			$tax=$tax2->tax_percentage;
			$minimumTotal+=($tax*$minimumTotal)/100;
			$maximumTotal+=($tax*$maximumTotal)/100;
		}
		$data['tax'] = number_format(round($tax,2),2);
		$data['sidewalks'] 	= ['name' => $sidewalkName, 'charges' => $sidewalkRate];
		$data['driveway'] 	= ['name' => $drivewayName, 'charges' => $drivewayRate];
		$data['has_service_walkways'] 	= $hasServiceWalkways;
		$data['servic_walkway'] = $servicWalkway;
		$data['servic_walkway_charges'] = $servicWalkwayRate;
		$data['per_km_charge'] = $perKMCharge;
		$data['sub_total'] 	= $subTotal;
		$data['latitude'] 	= $latitude;
		$data['longitude'] 	= $longitude;
		$data['location_name'] 	= $location_name;
		
		$data['minimum_temprature_limit'] 	= $minTemprature;

		if ($request->has('push_call')) {
			$data['live_angels'] = $distanceQueryResult;
		}

		return response()->json(['status'=>true,'message'=>'Estimation ha been prepared.','data'=>$data]);
	}

    //request a plow
	public function makeRequest(Request $request){
		$validator = Validator::make($request->all(),[
			'place_latitude' 		=> 'required|numeric',
			'place_longitude' 		=> 'required|numeric',
			'place_name'			=> 'present|nullable',
			'place_street'			=> 'present|nullable',
			'place_city'			=> 'present|nullable',
			'place_zipcode'			=> 'present|nullable',
			'place_country'			=> 'present|nullable',
			'place_temprature'		=> 'required|numeric',
			'place_full_address'	=> 'required',
			'sidewalk_id' 			=> 'required|exists:sidewalks,id',
			'sidewalk_rate'			=> 'required',
			'driveway_id' 			=> 'required|exists:driveways,id',
			'driveway_rate'			=> 'required',
			'like_walkway_service'	=> 'required',
			'season_type'			=> 'required|in:rain,snow,nothing',
		]);

		if ($validator->fails()){
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
		}

		$request->request->add(['push_call'=>true]);
		$request->request->add(['season_type'=>$request->get('season_type')]);
		$request->request->add(['temprature'=>$request->get('place_temprature')]);
		$request 	= Request::create('/api/v1/getEstimation', 'POST', $request->all());
		$response 	= Route::dispatch($request);
		$liveAngels = $response->getData();

		if (!$liveAngels->status) {
			return response()->json(['status'=>false,'message'=>$liveAngels->message,'note'=>'internal api exception']);
		}

		// foreach ($liveAngels->data->live_angels as $index => $liveAngel) {
			$liveAngels->estimate = ($liveAngels->data->radius * $liveAngels->data->per_km_charge) + $liveAngels->data->sub_total;
		// }

    	//insert into db
		$user = Auth::user();
		$servicWalkwayIds=array(1);
		// $servicWalkwayIds = ($request->get('servic_walkway_ids') != "")?explode(',', $request->get('servic_walkway_ids')):[];
		$requestData['user_id']=$user->id;
		$requestData['sidewalk_id']=$request->get('sidewalk_id');
		$requestData['sidewalk_rate']=$request->get('sidewalk_rate');
		$requestData['driveway_id']=$request->get('driveway_id');
		$requestData['driveway_rate']=$request->get('driveway_rate');

		$requestData['place_temprature']=$request->get('place_temprature');
		$requestData['place_name']=$request->get('place_name');
		$requestData['place_street']=$request->get('place_street');
		$requestData['place_city']=$request->get('place_city');
		$requestData['place_zipcode']=$request->get('place_zipcode');
		$requestData['place_country']=$request->get('place_country');
		$requestData['place_latitude']=$request->get('place_latitude');
		$requestData['place_longitude']=$request->get('place_longitude');
		$requestData['place_full_address']=$request->get('place_full_address');
		$requestData['like_walkway_service']=($request->get('like_walkway_service') == 'true')?'1':'0';
		$requestData['snow_angel_id']=$request->get('snow_angel_id');
		$requestData['status']='0';
		$requestData['snow_angel_latitude']=$request->get('snow_angel_latitude');
		$requestData['season_type']=$request->get('season_type');
		$requestData['tip']=0;
		
		if($request->get('tip') != ''){
			$requestData['tip'] = $request->get('tip');
		}

		$requestData['per_temprature_price']  = $liveAngels->data->minimum_temprature_limit;
		$requestData['additional_temprature'] = ($request->get('place_temprature') == "")?0:$request->get('place_temprature');

		$admin_statistic = Admin_statistic::find(1);
		$requestData['per_km_charge'] = $admin_statistic->per_km_charge;

		$requestData['tax']  = $liveAngels->data->tax;

		// if($request->get('place_temprature') < $admin_statistic->min_temprature){

		// 	// $requestData['per_temprature_price']  = $admin_statistic->temprature_fixed_charge;
		// 	// $requestData['additional_temprature'] = abs($request->get('place_temprature'))-abs($admin_statistic->min_temprature);

		// 	// if($admin_statistic->temprature_cent_charge != 0){
		// 	// 	$requestData['per_temprature_price']=($admin_statistic->temprature_cent_charge*($requestData['additional_temprature']))/100;
		// 	// }

		// 	$requestData['per_temprature_price']  = $admin_statistic->temprature_fixed_charge;
		// 	$requestData['additional_temprature'] = abs($request->get('place_temprature'))-abs($admin_statistic->min_temprature);

		// 	if($admin_statistic->temprature_cent_charge != 0){
		// 		$requestData['per_temprature_price']=($admin_statistic->temprature_cent_charge*($requestData['additional_temprature']))/100;
		// 	}
		// }
		
		if($admin_statistic->transfer_cent != 0){
			$requestData['admin_fees_percentage']=($liveAngels->estimate*$admin_statistic->transfer_cent)/100;
		}else{
			$requestData['admin_fees']=$admin_statistic->transfer_fees;
		}

		$plow_request=Req::create($requestData);
		if($request->get('like_walkway_service') == '1'){
			foreach ($servicWalkwayIds as $key => $service) {
				$service_data = array('request_id' =>$plow_request->id , 'service_id' =>$service,'rate'=>$liveAngels->data->servic_walkway_charges );
				$plow_request->walkway_with_services()->create($service_data);
			}
		}
		
		// print_r($liveAngels->data->live_angels);
		// exit;
		//add the request id in estimated array
		$liveAngels->data->request_id = $plow_request->id;


		$getUnallocatedRequestsResult = Req::where('id',$plow_request->id)
		->with(['sidewalk'=> function ($query) {
			$query->select('id','name');
		}])
		->with(['driveway'=> function ($query) {
			$query->select('id','name');
		}])
		->select('id','sidewalk_id','driveway_id','place_name','place_street','place_city','place_zipcode','place_country','place_full_address','sidewalk_rate','driveway_rate','per_temprature_price','place_latitude','place_longitude','status','like_walkway_service')
		->get()
		->map(function($plow_req,$key) use ($admin_statistic,$liveAngels){
			$perKMCharge 		= $admin_statistic->per_km_charge;
			$plow_req->total_amount =($liveAngels->data->radius*$perKMCharge)+$plow_req->sidewalk_rate+$plow_req->driveway_rate+$plow_req->per_temprature_price+$plow_req->tip;

			// $plow_req->total_amount =($plow_req->distance*$perKMCharge)+$plow_req->sidewalk_rate+$plow_req->driveway_rate+$plow_req->per_temprature_price+$plow_req->tip;
			$plow_req->total_amount=number_format($plow_req->total_amount,2);
			$plow_req->distance = "";
			$plow_req->side_walk=$plow_req->sidewalk['name'];
			$plow_req->drive_way=$plow_req->driveway['name'];
			unset($plow_req->sidewalk);
			unset($plow_req->driveway);
			unset($plow_req->walkway_with_services);
			return $plow_req;
		});

		$liveAngels->data->api_data = $getUnallocatedRequestsResult;
		// foreach ($liveAngels->data->live_angels as $index => $liveAngel) {
		// 	$liveAngelsIds[] = $liveAngel->snow_angel->snow_angel_id;
		// 	$liveAngel->estimate = ($liveAngel->distance * $liveAngels->data->per_km_charge) + $liveAngels->data->sub_total;
		// }
		broadcast(new PublishRequestChannel($liveAngels))->toOthers();

		$jobrequest=MailTemplate::find(2);
		if($request->get('like_walkway_service') == '1'){
			$service='Yes';
		}else{
			$service='No';
		}
		$table='<table cellpadding="3" cellspacing="3" border="1"><thead>
		<tr>
		<th>Sidewalk Rate</th>
		<td>'.'$ '.number_format($requestData['sidewalk_rate'],2).'</td></tr>
		<tr><th>Driveway Rate</th>
		<td>'.'$ '.number_format($requestData['driveway_rate'],2).'</td>
		</tr>
		<tr><th>Place Temprature</th>
		<td>'.number_format($requestData['place_temprature'],2).' &#8451;</td>
		</tr>
		<tr><th>Use Walk Way Service</th>
		<td>'.$service.'</td></tr>
		<tr><th>Per Temprature Price</th>
		<td>'.'$ '.number_format($requestData['per_temprature_price'],2).'</td></tr>
		<tr><th>Additional Temprature</th>
		<td>'.number_format($requestData['additional_temprature'],2).'</td>
		</tr></thead><tbody>';
		
		$message=str_replace('{{text}}','Your request for '.$requestData['place_full_address'].' is created successfully.<br/>Your Request Detail: <br/>'.$table,str_replace("{{name}}",$user->first_name.' '.$user->last_name,$jobrequest->message));
		$requestinfo=array('message'=>$message,'request_id'=>$plow_request->id);
		$user->notify(new RequestPlow($requestinfo));
		// $workers=User::where('active_mode','snow_angel')->get();
		// print_r($liveAngels->data->live_angels);
		// exit;
		NotificationHelper::sendPushNotification($liveAngels->data->live_angels,'NewJobAvailable',[],true);
		NotificationHelper::sendPushNotification($user,'UserRequestPlow');

		return response()->json(['status'=>true,'message'=>'Estimation has been stored.','data'=>$liveAngels]);
	}

    //display unallocated jobs
	public function getUnallocatedRequests(Request $request){

		$validator = Validator::make($request->all(),[
			'current_latitude' 		=> 'required',
			'current_longitude' 	=> 'required',
		]);

		if ($validator->fails()){
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
		}

    	//get variables
		$latitude 	= $request->get('current_latitude');
		$longitude 	= $request->get('current_longitude');
		$admin_statistic 	= Admin_statistic::find(1);
		$minDistance 		= $admin_statistic->min_distance;

		//calculate distance of angels
		$getUnallocatedRequestsResult = Req::whereNull('snow_angel_id')
		// with(['walkway_with_services'=> function ($query) {
		// 	$query->select('request_id','service_id','rate');
		// 	$query->with(['service'=> function ($query) {
		// 		$query->select('id','name');
		// 	}]);
		// }])
		->with(['sidewalk'=> function ($query) {
			$query->select('id','name');
		}])
		->with(['driveway'=> function ($query) {
			$query->select('id','name');
		}])
		->having('distance', '<', $minDistance)
		->having('status', '0')
		->orderBy('id','desc')
		->select('id','sidewalk_id','driveway_id','place_name','place_street','place_city','place_zipcode','place_country','place_full_address','sidewalk_rate','driveway_rate','per_temprature_price','place_latitude','place_longitude','status','like_walkway_service',DB::raw('ROUND(( 6373 * acos ( cos ( radians('.$latitude.') ) * cos( radians( place_latitude ) ) * 
			cos( radians( place_longitude ) - radians('.$longitude.') ) + sin ( radians('.$latitude.') ) * 
			sin( radians( place_latitude ) ) ) ),1) AS `distance`'))
		->get();
		$getUnallocatedRequestsResult = $getUnallocatedRequestsResult->map(function($plow_req,$key) use ($admin_statistic){
			$perKMCharge 		= $admin_statistic->per_km_charge;
			// $walkway_services=[];
			$plow_req->total_amount =($plow_req->distance*$perKMCharge)+$plow_req->sidewalk_rate+$plow_req->driveway_rate+$plow_req->tip;
			if($plow_req->like_walkway_service == '1'){
				$plow_req->total_amount=$plow_req->sidewalk->salt_rate;
				$plow_req->total_amount=$plow_req->driveway->salt_rate;
			}

			// if(count($plow_req->walkway_with_services) > 0){
			// 	foreach ($plow_req->walkway_with_services as $key => $services) {
			// 		$plow_req->total_amount += $services->rate;
			// 		$plow_req->walkway_with_services[$key]->service->name='service with '.$plow_req->walkway_with_services[$key]->service->name;
			// 		$walkway_services[]='service with '.$plow_req->walkway_with_services[$key]->service->name;
			// 	}
			// }
			$plow_req->total_amount=number_format($plow_req->total_amount,2);
			$plow_req->distance=number_format($plow_req->distance,2);
			$plow_req->side_walk=$plow_req->sidewalk['name'];
			$plow_req->drive_way=$plow_req->driveway['name'];
			// $plow_req->walkway_services=$walkway_services;
			unset($plow_req->sidewalk);
			unset($plow_req->driveway);
			unset($plow_req->walkway_with_services);
			return $plow_req;
		});

		return response()->json(['status'=>true,'message'=>'Job list.','data'=>$getUnallocatedRequestsResult]);

	}

	public function acceptJob(Request $request){


		//check for validation
		$validator = Validator::make($request->all(),[
			'request_id'			=> 'required:exists,requests,id',
			'current_latitude' 		=> 'required',
			'current_longitude' 	=> 'required',
		]);

		if ($validator->fails()){
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
		}

		$user 		= Auth::user();
		$requestId 	= $request->get('request_id');
		
		//check if this worker has 5 active ongoing jobs
		$ongoingJobs = Req::where('snow_angel_id',$user->snow_angel->id)
		->where('status','!=','2')
		->count();

		if ($ongoingJobs >= 5){
			return response()->json(['status'=>false,'message'=>"You cann't accept more jobs."]);
		}

		//get the request object to be allocated to worker
		$plowRequest = Req::select('id','user_id','sidewalk_rate','driveway_rate','per_temprature_price','tip','place_latitude','place_longitude','percip_intensity_fees','percip_intensity_fees_percentage','per_km_charge','admin_fees_percentage','admin_fees',\DB::raw('( 6371 * acos( cos( radians(snow_angel_latitude) ) * cos( radians( place_latitude ) ) * cos( radians( place_longitude ) - radians(snow_angel_longitude) ) + sin( radians(snow_angel_latitude) ) * sin( radians( place_latitude ) ) ) )  as distance'))
		->with('walkway_with_services')
		->where('id',$requestId)
		->first();

		//calculate the material charge
		$total = 0;
		$subTotal = 0;
		$materialCharge = $plowRequest->sidewalk_rate + $plowRequest->driveway_rate;
		if ($plowRequest->walkway_with_services->count()) {
			$materialCharge += $plowRequest->walkway_with_services->sum('rate');
		}
		$plowRequest->snow_angel_distance =  round($plowRequest->distance,2);

		//calculate the distance charge
		$distanceCharge = round(($plowRequest->distance * $plowRequest->per_km_charge),2);
		$subTotal 		= $materialCharge + $distanceCharge;

		//calculate the minimum percipIntensity in mm
		if ($plowRequest->percip_intensity_fees_percentage > 0) {
			$subTotal += (($subTotal * $plowRequest->percip_intensity_fees_percentage)/100);
		}else if($plowRequest->percip_intensity_fees > 0){
			$subTotal += $plowRequest->percip_intensity_fees;
		}
		//add the tip as well
		if ($plowRequest->tip > 0) {
			$subTotal = $plowRequest->tip;
		}
		//calculate the admin/transfer charge 
		$adminFees = 0;
		if ($plowRequest->admin_fees_percentage > 0) {
			$adminFees 			= (($subTotal * $plowRequest->admin_fees_percentage)/100);
			$amountForWorker 	= ($subTotal - $adminFees);
			$plowRequest->admin_fees_percentage = $adminFees;
		}else{
			$adminFees 			=  $plowRequest->admin_fees;
			$amountForWorker 	=  $subTotal - $adminFees;
			$plowRequest->admin_fees = $adminFees;
		}

		$transactionType 	= 'primary';
		$discountedType  	= 'nothing';
		$discountedAmount  	= 0;
		$customer 			= $plowRequest->user;

		// //add the tip as well
		// if ($plowRequest->tip > 0) {
		// 	$subTotal = $plowRequest->tip;
		// }
		
		// online charge logic start
		$customerCards 	= Customer::retrieve($customer->stripe_id);
		$defaultCard 	= $customerCards->default_source;
		$charge = \Stripe\Charge::create(array(
			"amount" 	=> round($subTotal*100),
			"currency" 	=> "cad",
			"customer" 	=> $customer->stripe_id,
			"source" 	=> $defaultCard,
		));


		if($charge){
			$data['total_amount'] 		= $subTotal;
			$data['amount'] 			= $amountForWorker;
			$data['admin_amount'] 		= $adminFees;
			$data['request_id'] 		= $requestId;
			$data['transaction_id'] 	= $charge->balance_transaction;
			$data['charged_currency']   = $charge->currency;
			$data['transaction_time'] 	= date('Y-m-d H:i:s', $charge->created);
			$data['failure_code'] 		= ($charge->failure_code != "")?$charge->failure_code:"";
			$data['failure_message'] 	= ($charge->failure_message != "")?$charge->failure_message:"";
			$data['paid'] 				= $charge->paid;
			$data['status'] 			= $charge->status;
			$data['status'] 			= $charge->status;
			$data['withdrow_status'] 	= '0';
			$data['transaction_type'] 	= $transactionType;
			$data['discounted_type'] 	= 'nothing';
			$data['discounted_amount'] 	= $discountedAmount;
			Transaction_history::insert($data);
		}


		// online charge logic end
		
		// $snow_angeldata = array(
		// 					'snow_angel_id' =>$user->snow_angel->id ,
		// 					'snow_angel_latitude'=>$request->get('current_latitude'),
		// 					'snow_angel_longitude'=>$request->get('current_longitude')
		// 				);
		if ($customer->referred_by && $customer->requests->count() == 1) {
			
			$discountedType 	= 'percentage';
			$discountedAmount 	= (($subTotal * 10)/100);
			$subTotal 			= $subTotal - $discountedAmount;

			if (substr($customer->referred_by, 0,2) == 'wr') {
				$workerReffered = Snow_angel::where('referrel_id',$customer->referred_by)->first();
				Transaction_history::create(['request_id'=>$requestId,'amount'=>0,'charged_currency'=>'cad','transaction_time'=>Carbon::now(),'paid'=>'1','status'=>'paid','status'=>'succeeded','withdrow_status'=>0,'transaction_type'=>'discount','discounted_type'=>'percentage','discounted_amount'=>$discountedAmount,'total_amount'=>0,'admin_amount'=>0]);
			}else if (substr($customer->referred_by, 0,2) == 'ur') {
				$userReffered = User::where('referrel_id',$customer->referred_by)->first();
				UserReward::create(['user_id'=>$userReffered->id,'discounted_type'=>'percentage','discounted_amount'=>$discountedAmount,'request_id'=>$requestId]);
			}
			UserReward::create(['user_id'=>$customer->id,'discounted_type'=>'percentage','discounted_amount'=>$discountedAmount,'request_id'=>$requestId]);
		}

		$plowRequest->snow_angel_id = $user->snow_angel->id;
		$plowRequest->snow_angel_latitude = $request->get('current_latitude');
		$plowRequest->snow_angel_longitude = $request->get('current_longitude');
		$plowRequest->save();

		/** Mail Logic Start**/
		$jobaccept=MailTemplate::find(3);
		$request_obj=Req::find($requestId);
		$request_user=User::find($request_obj->user_id);
		$message=str_replace('{{text}}','Your request accepted by '.$user->first_name.' '.$user->last_name,str_replace("{{name}}",$request_user->first_name.' '.$request_user->last_name,$jobaccept->message));
		$requestinfo=array('message'=>$message,'request_id'=>$requestId);
		$request_user->notify(new JobAcceptedNotification($requestinfo));

		/********************************Internal Data for Push notification***************************************/
		$pushData = UserHelper::getRequestDetail($request_user, $requestId);
		NotificationHelper::sendPushNotification($request_user,'WorkerJobAccepted',$pushData->getData()->data);
		/********************************Internal Data for Push notification***************************************/

		$message=str_replace('{{text}}','You accept a job requested by '.$request_user->first_name.' '.$request_user->last_name,str_replace("{{name}}",$user->first_name.' '.$user->last_name,$jobaccept->message));
		$requestinfo=array('message'=>$message,'request_id'=>$requestId);
		$user->notify(new JobAcceptedSnowangelNotification($requestinfo));
		$broadcastData['request_id'] = $requestId;
		/** Mail Logic End**/
		broadcast(new RequestAcceptedChannel($broadcastData));

		return response()->json(['status'=>true,'message'=>'Job accepted successfully.']);
		exit;
		print_r($subTotal);
		// print_r($plowRequest->walkway_with_services->sum('rate'));
		// print_r($plowRequest->walkway_with_services->count());
		$snow_angeldata = array(
			'snow_angel_id' =>$user->snow_angel->id ,
			'snow_angel_latitude'=>$request->get('current_latitude'),
			'snow_angel_longitude'=>$request->get('current_longitude')
		);

				// $plow_request = Req::select('id','place_name','place_street','place_city','place_zipcode','place_country','place_full_address','					sidewalk_rate','driveway_rate','per_temprature_price','tip','place_latitude','place_longitude','started_at','completed_at			','per_km_charge',\DB::raw('( 6371 * acos( cos( radians(snow_angel_latitude) ) 
		// 				* cos( radians( place_latitude ) ) 
		// 				* cos( radians( place_longitude ) - radians(snow_angel_longitude) ) + sin( radians(snow_angel_latitude) ) 
		// 				* sin( radians( place_latitude ) ) ) )  as distance'))
		// 				->with(['walkway_with_services'=> function ($query) {
		// 					$query->select('request_id',\DB::raw('SUM(rate) as total'));
		// 					$query->groupBy('request_id');
		// 				}])
		// 				->where('snow_angel_id',$user->snow_angel->id)
		// 				->where('status','!=','2')
		// 				->orderBy('started_at')
		// 				->count();





		// if ($plow_requestlike_walkway_service == 'true') {
		// 	$servicWalkwayRate = ($sidewalk->salt_rate + ($driveway->salt_rate * (int)$sidewalkName));
		// 	$servicWalkway[0]['name'] = 'salt';
		// 	$servicWalkway[0]['rate'] = $servicWalkwayRate;
		// 	$hasServiceWalkways = true;
		// }

		// $plow_request = Req::select('id','place_name','place_street','place_city','place_zipcode','place_country','place_full_address','					sidewalk_rate','driveway_rate','per_temprature_price','tip','place_latitude','place_longitude','started_at','completed_at			','per_km_charge',\DB::raw('( 6371 * acos( cos( radians(snow_angel_latitude) ) 
		// 				* cos( radians( place_latitude ) ) 
		// 				* cos( radians( place_longitude ) - radians(snow_angel_longitude) ) + sin( radians(snow_angel_latitude) ) 
		// 				* sin( radians( place_latitude ) ) ) )  as distance'))
		// 				->with(['walkway_with_services'=> function ($query) {
		// 					$query->select('request_id',\DB::raw('SUM(rate) as total'));
		// 					$query->groupBy('request_id');
		// 				}])
		// 				->where('snow_angel_id',$user->snow_angel->id)
		// 				->where('status','!=','2')
		// 				->orderBy('started_at')
		// 				->count();

		Req::where('id',$requestId)->update($snow_angeldata);
		
		$jobaccept=MailTemplate::find(3);
		$request_obj=Req::find($requestId);
		$request_user=User::find($request_obj->user_id);
		$message=str_replace('{{text}}','Your request accepted by '.$user->first_name.' '.$user->last_name,str_replace("{{name}}",$request_user->first_name.' '.$request_user->last_name,$jobaccept->message));
		$requestinfo=array('message'=>$message,'request_id'=>$requestId);
		$user->notify(new JobAcceptedNotification($requestinfo));

		$message=str_replace('{{text}}','You accept a job requested by '.$request_user->first_name.' '.$request_user->last_name,str_replace("{{name}}",$user->first_name.' '.$user->last_name,$jobaccept->message));
		$requestinfo=array('message'=>$message,'request_id'=>$requestId);
		$user->notify(new JobAcceptedSnowangelNotification($requestinfo));
		NotificationHelper::sendPushNotification($user,'JobAccepted');

// $broadcastData['angel_id_who_accepted_request'] = $user->snow_angel->id;
		$broadcastData['request_id'] = $requestId;
// print_r($broadcastData);
// exit;
		broadcast(new RequestAcceptedChannel($broadcastData));

		return response()->json(['status'=>true,'message'=>'Job accepted successfully.']);

	}



    //request a plow
	// public function request_plow(Request $request){

	// 	$rules = [
	// 		'sidewalk_id' => 'required',
	// 		'driveway_id' => 'required',
	// 		'sidewalk_rate'=>'required',
	// 		'driveway_rate'=>'required',
	// 		'place_temprature'=>'required',
	// 		'place_name'=>'required',
	// 		'place_street'=>'required',
	// 		'place_city'=>'required',
	// 		'place_zipcode'=>'required',
	// 		'place_country'=>'required',
	// 		'place_full_address'=>'required',
	// 		'like_walkway_service'=>'required'
	// 	];

	// 	$validator = Validator::make($request->all(), $rules);

	// 	if ($validator->fails()){
	// 		$message = $validator->messages()->first();
	// 		return response()->json(['status'=>false,'message'=>$message]);
	// 	}

	// 	$user = Auth::user();
	// 	$requestData['user_id']=$user->id;
	// 	$requestData['sidewalk_id']=$request->get('sidewalk_id');
	// 	$requestData['sidewalk_rate']=$request->get('driveway_id');
	// 	$requestData['driveway_id']=$request->get('driveway_id');
	// 	$requestData['driveway_rate']=$request->get('driveway_rate');

	// 	$requestData['place_temprature']=$request->get('place_temprature');
	// 	$requestData['place_name']=$request->get('place_name');
	// 	$requestData['place_street']=$request->get('place_street');
	// 	$requestData['place_city']=$request->get('place_city');
	// 	$requestData['place_zipcode']=$request->get('place_zipcode');
	// 	$requestData['place_country']=$request->get('place_country');
	// 	$requestData['place_latitude']=$request->get('place_latitude');
	// 	$requestData['place_longitude']=$request->get('place_longitude');
	// 	$requestData['place_full_address']=$request->get('place_full_address');
	// 	$requestData['like_walkway_service']=$request->get('like_walkway_service');
	// 	$requestData['snow_angel_id']=$request->get('snow_angel_id');
	// 	$requestData['status']='0';
	// 	$requestData['snow_angel_latitude']=$request->get('snow_angel_latitude');
	// 	$requestData['tip']=0;
	// 	if($request->get('tip') != ''){
	// 		$requestData['tip']=$request->get('tip');
	// 	}
	// 	$requestData['per_temprature_price']=0;
	// 	$requestData['additional_temprature']=0;

	// 	$admin_statistic=Admin_statistic::find(1);

	// 	if($request->get('place_temprature') < $admin_statistic->min_temprature){

	// 		$requestData['per_temprature_price']=$admin_statistic->temprature_fixed_charge;
	// 		$requestData['additional_temprature']=abs($request->get('place_temprature'))-abs($admin_statistic->min_temprature);

	// 		if($admin_statistic->temprature_cent_charge != 0){
	// 			$requestData['per_temprature_price']=($admin_statistic->temprature_cent_charge*($requestData['additional_temprature']))/100;
	// 		}

	// 	}

	// 	$plow_request=Req::create($requestData);
	// 	if($request->get('like_walkway_service') == '1'){
	// 		foreach ($request->get('services') as $key => $service) {
	// 			$service_data = array('request_id' =>$plow_request->id , 'service_id' =>$key,'rate'=>$service );
	// 			$plow_request->walkway_with_services()->create($service_data);
	// 		}
	// 	}

	// 	return response()->json(['status'=>true,'data'=>$plow_request,'message'=>'Plow request has been submited.']);
	// }

    //make payment
	public function make_request_payment(Request $request){
		Stripe::setApiKey(config('services.stripe.secret'));

		$validator = Validator::make($request->all(),[
			'amount' 	=> 'required',
			'card' => 'required',
			'request_id'=>'required'
		]);

		if ($validator->fails()){
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
		}
		$user = Auth::user();

		$amount=$request->get('amount');
		try{
			$charge = \Stripe\Charge::create(array(
				"amount" => round($amount*100),
				"currency" => "cad",
				"customer" =>$user->stripe_id,
				"source" =>$request->get('card') ,
			));

			if($charge){
				$admin_statistic 	= Admin_statistic::find(1);

				$transferFees 		= $admin_statistic->transfer_fees;
				$transferCent 		= $admin_statistic->transfer_cent;
				if ($transferCent) {
					$transfer_fees = ($amount * $transferCent)/100;
				}else{
					$transfer_fees = $transferFees;
				}
				$data['total_amount'] = $amount;
				$commission=($amount*20)/100;
				$data['amount'] = number_format($amount-$commission-$transfer_fees,2);
				$data['commission'] = number_format($commission,2);
				$data['transfer_fees']=$transfer_fees;
				$data['request_id'] = $request->get('request_id');
        	// $data['charge_id'] = $charge->id;
				$data['transaction_id'] = $charge->balance_transaction;
				$data['charged_currency']   = $charge->currency;

				$data['transaction_time'] = date('Y-m-d H:i:s', $charge->created);
				$data['failure_code'] = ($charge->failure_code != "")?$charge->failure_code:"";
				$data['failure_message'] = ($charge->failure_message != "")?$charge->failure_message:"";
				$data['paid'] = $charge->paid;
				$data['status'] = $charge->status;
				$data['status'] = $charge->status;
				$data['withdrow_status'] = '0';

				Transaction_history::insert($data);

			}
		}catch(\Stripe\Error\Card $e){
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		} catch (\Stripe\Error\NotFound $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		}
		catch (\Stripe\Error\Authentication $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		}
		catch (\Stripe\Error\InvalidRequest $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		} catch (\Stripe\Error\RateLimit $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		} catch (\Stripe\Error\ApiConnection $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		} catch (\Stripe\Error\Base $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		}
		return response()->json(['status'=>true,'message'=>"Payment Successfully."]);
	}

}
