<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarPhoto extends Model
{
    protected $table = "car_photoes";

    protected $fillable = [
		'car_driver_id','photo'
	];

	public function car_driver(){
		return $this->belongsTo('App\CarDriver','car_driver_id');
	}
}
