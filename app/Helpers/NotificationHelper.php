<?php

namespace App\Helpers;

use DB;
use Auth;
use App\User;

class NotificationHelper{

	public static function sendPushNotification($receiver,$tag,$additionalData=[],$is_broadcast=false){

		$androidDeviceID = [];
		$iosDeviceID = [];
		
		$additionalDataToBeSent = [];
		switch ($tag) {

			case 'UserTripStart':
			$subject = "Trip Started";
			$message = 'Your trip from '.$additionalData['from_location']['work_city']['name'].' to '. $additionalData['to_location']['work_city']['name'].' is started';
			$additionalDataToBeSent = ['trip_id'=>$additionalData['id'],'booking_id'=>$additionalData['booking_id']];
			break;

			case 'UserTripCompleted':
			$subject = "Trip Completed";
			$message = 'Your trip from '.$additionalData['from_location']['work_city']['name'].' to '. $additionalData['to_location']['work_city']['name'].' is now completed. Please rate your experience.';
			$additionalDataToBeSent = ['trip_id'=>$additionalData['id'],'booking_id'=>$additionalData['booking_id']];
			break;

			case 'UserBookingConfirmation':
			$subject = "Booking Confirmation";
			$message = '#'.$additionalData['id'].' Your booking for '.$additionalData['trip']['from_location']['work_city']['name'].' to '. $additionalData['trip']['to_location']['work_city']['name'].' trip is confirmed.';
			$additionalDataToBeSent = ['trip_id'=>$additionalData['trip_id'],'booking_id'=>$additionalData['id']];
			break;

			case 'UserTripBooking':
			$subject = "Booking Confirmed";
			$message = 'Your trip booked successfully.';
			$additionalDataToBeSent = ['trip_id'=>$additionalData['id'],'booking_id'=>$additionalData['booking_id']];
			break;

			case 'UserTripBookingDeclined':
			$subject = "Booking Declined";
			$message = 'Your booking for '.$additionalData['trip']['from_location']['work_city']['name'].' to '. $additionalData['trip']['to_location']['work_city']['name'].' trip is declined by a driver.';
			$additionalDataToBeSent = ['trip_id'=>$additionalData['id'],'booking_id'=>$additionalData['booking_id']];
			break;

			case 'UserTripCancel':
			$subject = "Trip Cancelled by Driver";
			$message = 'Your driver has cancelled a trip from Edmonton to Calgary. Please check your booking details page to view refund status.';
			$additionalDataToBeSent = ['trip_id'=>$additionalData['id'],'booking_id'=>$additionalData['bookingInfo']['booking_id']];
			break;

			case 'UserBookingCancel':
			$subject = "Booking Cancelled";
			$message = 'Your booking for trip from '.$additionalData['from_location']['work_city']['name'].' to '. $additionalData['to_location']['work_city']['name'].' cancelled successfully.';
			$additionalDataToBeSent = ['trip_id'=>$additionalData['id'],'booking_id'=>$additionalData['booking_id']];
			break;

			case 'UserPaymentMethodInvalid':
			$subject = "Payment Method Invalid";
			$message = 'We couldn’t process the payment with card ending '.$additionalData['card_last4'].'. Your card is either expired or invalid.Please enter a new payment method.';
			$additionalDataToBeSent = ['trip_id'=>$additionalData['trip_id'],'booking_id'=>$additionalData['booking_id']];
			break;

			case 'UserMarkedAsNoShow':
			$subject = 'Marked as “No Show”';
			$message = 'Your driver has marked you as NO SHOW for your trip booking from '.$additionalData['from_location'].' to '. $additionalData['to_location'];
			$additionalDataToBeSent = ['trip_id'=>$additionalData['trip_id'],'booking_id'=>$additionalData['booking_id']];
			break;

			// case 'UserTripReminder':
			// $subject = "Ridechimp";
			// $message = 'Your snow removal request has been accepted.';
			// $additionalDataToBeSent = [];
			// break;

			case 'DriverTripBooking':
			$subject = "New booking received for your trip";
			$message = 'Please confirm the booking before it gets expired.';
			$additionalDataToBeSent = ['trip_id'=>$additionalData['id']];
			break;
			
			// case 'DriverTripStart':
			// $subject = "Ridechimp";
			// $message = 'Your trip from '.$additionalData['from_location']['work_city']['name'].' to '. $additionalData['to_location']['work_city']['name'].' on '.date('d M, Y',strtotime($additionalData['trip_date'])).' is started.';
			// $additionalDataToBeSent = ['trip_id'=>$additionalData['id']];
			// break;

			case 'DriverTripCompleted':
			$subject = "Earnings deposited";
			$message = 'Your earnings for Trip '.$additionalData['from_location']['work_city']['name'].' to '. $additionalData['to_location']['work_city']['name'].' is now deposited in your Ridechimp wallet. You can Withdraw It anytime.';
			$additionalDataToBeSent = ['trip_id'=>$additionalData['id']];
			break;

			// case 'DriverTripCancel':
			// $subject = "Ridechimp";
			// $message = 'You have cancel trip from '.$additionalData['from_location']['work_city']['name'].' to '. $additionalData['to_location']['work_city']['name'].' due to '.$additionalData['cancelled_reason'].'.Trip cancellation charge $'.$additionalData['penalty']['penalty_amount'].' deducted from your wallet.';
			// $additionalDataToBeSent = ['trip_id'=>$additionalData['id']];
			// break;

			case 'DriverBookingCancel':
			$subject = "Booking Cancelled by ".$additionalData['user_name'];
			$message = $additionalData['user_name'].' has cancelled the booking for a trip to '. $additionalData['to_location']['work_city']['name'];
			$additionalDataToBeSent = ['trip_id'=>$additionalData['id'],'booing_id'=>$additionalData['booking_id']];
			break;

			case 'WithdrawSuccess':
			$subject = "Money is deposited in your bank account";
			$message = 'Your withdrawal money is now deposited in your bank account.';
			$additionalDataToBeSent = [];
			break;

			case 'newRideRequest':
			$subject = "New Ride Request";
			$message = 'New ride request arrived';
			$additionalDataToBeSent = ['trip_id'=>$additionalData['trip_id']];
			break;

			case 'chatMessage':
			$subject = "Ridechimp";
			$message = $additionalData['message'];
			$additionalDataToBeSent = $additionalData;
			break;

			case 'NewLoginFromOtherDevice':
			$subject = "New Login";
			$message = 'Please logout';
			break;

		}

		$pushData 			  = [];
		$pushData['title'] 	  = $subject;
		$pushData['body'] 	  = $message;
		$pushData['priority'] = 10;
		$pushData['icon']  	  = "";
		$pushData['sound']    = 'mySound';

		$payLoad 			  = [];
		$payLoad['title'] 	  = $subject;
		$payLoad['body']  	  = $message;
		$payLoad['tag']  	  = $tag;
		$payLoad['vibrate']   = 1;
		$payLoad['sound']  	  = 1;
		$payLoad['additional_data'] = $additionalDataToBeSent;


		if ($is_broadcast == true) {
			foreach ($receiver as $key => $user) {

				$user = $user->toArray();
				
				if($tag == 'DriverTripBooking' || $tag == 'DriverTripCompleted' || $tag == 'DriverBookingCancel' || $tag == 'WithdrawSuccess'){
					if(isset($user['driver_firebase_android_id'])){
						$androidDeviceID[] = $user['driver_firebase_android_id'];
					}
					if(isset($user['driver_firebase_ios_id'])){
						$iosDeviceID[] = $user['driver_firebase_ios_id'];
					}
				}else{
					if(isset($user['firebase_android_id'])){
						$androidDeviceID[] = $user['firebase_android_id'];
					}
					if(isset($user['firebase_ios_id'])){
						$iosDeviceID[] = $user['firebase_ios_id'];
					}
				// $payLoad['additional_data'][$user['id']] = ['trip_id'=>$user['trip_id'],'booking_id'=>$user['booking_id']];;
				}
			}
			$androidDeviceID = array_values(array_filter($androidDeviceID));
			$iosDeviceID = array_values(array_filter($iosDeviceID));
		}else{

			if($tag == 'DriverTripBooking' || $tag == 'DriverTripCompleted' || $tag == 'DriverBookingCancel' || $tag == 'WithdrawSuccess'){
				$androidDeviceID[] = isset($receiver['driver_firebase_android_id'])?$receiver['driver_firebase_android_id']:"";
				$iosDeviceID[] 	 = isset($receiver['driver_firebase_ios_id'])?$receiver['driver_firebase_ios_id']:"";

			}else{

				$androidDeviceID[] = isset($receiver['firebase_android_id'])?$receiver['firebase_android_id']:"";
				$iosDeviceID[] 	 = isset($receiver['firebase_ios_id'])?$receiver['firebase_ios_id']:"";
			}

		}

		if ($androidDeviceID != "") {
			$fields = array('registration_ids' => $androidDeviceID,'data'=>$payLoad,'active_mode'=>$receiver['active_mode'],'tag'=>$tag);
			self::curlNotification($fields);
		}

		if ($iosDeviceID != "") {
			$fields = array('registration_ids' => $iosDeviceID,'notification'=> $pushData,'data'=>$payLoad,'active_mode'=>$receiver['active_mode'],'tag'=>$tag);
			self::curlNotification($fields);
		}
	}

	public static function curlNotificationV2($parameters,$additionalData){

		$pushData 			  = [];
		$pushData['title'] 	  = $parameters['subject'];
		$pushData['body'] 	  = $parameters['message'];
		$pushData['priority'] = 10;
		$pushData['icon']  	  = "";
		$pushData['sound']    = 'mySound';

		$payLoad 			  = [];
		$payLoad['title'] 	  = $parameters['subject'];
		$payLoad['body']  	  = $parameters['message'];
		$payLoad['tag']  	  = $parameters['tag'];
		$payLoad['vibrate']   = 1;
		$payLoad['sound']  	  = 1;
		$payLoad['additional_data'] = $additionalData;

		if ($parameters['androidDeviceID'] != "") {
			$fields = array('to' => $parameters['androidDeviceID'],'data'=>$payLoad);
			self::curlNotification($fields);
		}

		if ($parameters['iosDeviceID'] != "") {
			$fields = array('to' => $parameters['iosDeviceID'],'notification'=> $pushData,'data'=>$payLoad);
			self::curlNotification($fields);
		}
	}

	public static function curlNotification($fields){

		if($fields['tag'] == 'DriverTripBooking' || $fields['tag'] == 'DriverTripCompleted' || $fields['tag'] == 'DriverBookingCancel' || $fields['tag'] == 'WithdrawSuccess'){
			$headers = array('Authorization: key=AAAA26bLbpE:APA91bEWM5ZlaCavz96rhw35UqSeFc7m7QRjcHiRLUmqf9YBN55Igx72dgGkC0f-UmEnw939XDrrK8ocDME37BgsY8qAFs6FqzKuxi2PuGlATQreUZR21H-gYJk7kvttR_3ykfblMgbY','Content-Type: application/json');

		}else{
			$headers = array('Authorization: key=AAAAYnKyh84:APA91bFlTZlz1SFPoYN0JdpbVty443fZGpj2Ex869vXbF9F6Q-lP8TOeqfm8_zqyhBhjyHILVtoYKvaWkSsIgiZ1348mR0WVdvdq65OphQFfSGpPlVtRM4KHtkI44LxfwlWpBfmvJFN_','Content-Type: application/json');
		}

		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, true );
		unset($fields['active_mode']);
		unset($fields['tag']);
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );

		$result = curl_exec($ch);
		curl_close( $ch );
		// print_r($result);die();
		return $result;
	}

	
}