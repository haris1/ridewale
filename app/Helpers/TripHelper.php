<?php

namespace App\Helpers;

use App\Admin_statistic;
use App\Models\v3\Trip;
use App\Payment;
use App\Models\v3\Booking;

class TripHelper{

	public static function getTripFair($trip,$requiredSeats)
	{
		
		$adminStates = self::getAdminStatistics();
		$perSeatCost = $trip->price;

		/** Base Price **/
		$basePrice = ($perSeatCost * $requiredSeats);
		
		/** Tax **/
		$taxFareType 	= 'percentage';
		// $taxPercentage 	= $adminStates->tax_cent;
		$taxPercentage 	= $trip->from_location['work_city']['work_state']['tax_percentage'];
		$taxAmount = ($taxPercentage * $basePrice)/100;

		/** User Chargable Amount **/
		$chargeableAmount = $basePrice + $taxAmount;

		/** Transfer Fees **/
		$transferFareType 	= 'fixed';
		$transferPercentage = 0;
		if(!empty($adminStates->transfer_cent)){
			$transferFareType 	= 'percentage';
			$transferPercentage = $adminStates->transfer_cent;
			$transferFees = (($chargeableAmount * $adminStates->transfer_cent)/100) + 0.30;
		}else{
			$transferFees = $adminStates->transfer_fees;
		}

		/** Admin Fees **/
		$adminFareType 	 = 'fixed';
		$adminPercentage = 0;
		if(!empty($adminStates->admin_cent)){
			$adminFareType 	 = 'percentage';
			$adminPercentage = $adminStates->admin_cent;
			$adminFees = ($basePrice * $adminStates->admin_cent)/100;
		}else{
			$adminFees = $adminStates->admin_fees;
		}

		/** Driver Fees **/
		$driverFees = ($basePrice - $adminFees);

		return [
			'per_seat_cost' 	=> $perSeatCost,
			'requested_seats' 	=> $requiredSeats,
			
			'base_price' 		=> $basePrice,
			'tax' 				=> $taxAmount,
			'transfer_fees' 	=> $transferFees,
			'chargeable_amount' => $chargeableAmount,
			'admin_fees' 		=> $adminFees,
			'driver_fees' 		=> $driverFees,
			
			'transfer_fare_percentage'	=> $transferPercentage,
			'admin_fare_percentage'		=> $adminPercentage,
			'tax_fare_percentage'		=> $taxPercentage,
			
			'transfer_fare_type'=> $transferFareType,
			'admin_fare_type'	=> $adminFareType,
			'tax_fare_type'		=> $taxFareType,
		];
	}

	public static function getIncityTripFair()
	{
		
		$adminStates = self::getAdminStatistics();

		return [
			'per_km_charge' 	=> 10,
		];
	}

	public static function tripDetail($trip_id)
	{
		/* trip vehicle and driver detail */
		$trip 	= Trip::with(['from_location'=> function ($query) {
			$query->select('id','work_city_id','name','latitude','longitude','place_id')
				  ->with(['work_city'=>function($query){
				  		$query->with('work_state:id,tax_percentage')->select('id','name','work_state_id','timezone_name','timezone');
				  }]);
		}])
		->with(['to_location'=> function ($query) {
			$query->select('id','work_city_id','name','latitude','longitude','place_id')
				  ->with('work_city:id,name,timezone_name,timezone');
		}])
		->with(['vehicle'=> function ($query) {
			$query->select('id','car_model_id','color','year','rules_and_regulations','allowed_things')
			->with(['car_model'=> function ($query) {
				$query->select('id','car_company_id','car_type_id','capacity','model_name')
				->with(['car_company'=> function ($query) {
					$query->select('id','name');
				}])
				->with(['car_type'=> function ($query) {
					$query->select('id','name');
				}]);
			},'carPhotoes'=> function ($query) {
				$query->select('car_driver_id','photo');
			}]);
		}])
		->with(['driver'=> function ($query) {
			$query->select('id','user_id')
			->with(['user'=> function ($query) {
				$query->select('id','first_name','last_name','profile_pic');
			}]);
		}])	
		->where('id',$trip_id)
		->first();

		/* base urls */
		if($trip->vehicle){

		$trip->vehicle->vehicle_base_url = asset('storage/driver_car/'.$trip->driver_id).'/';
		}

		return $trip;
	}

	public static function bookingDetail($booking_id){
		/*booking detail*/
		$trip = Booking::with(['trip'=> function ($query) {
					$query->select('id','driver_id','vehicle_id','trip_date','pickup_time','from_location_id','to_location_id','price','available_seats','status','cancelled_reason','updated_at')
					->with(['from_location'=> function($query){
                        $query->with('work_city:id,name,timezone_name,timezone')->select('id','name','work_city_id','latitude','longitude');
                    },'to_location'=> function($query){
                        $query->with('work_city:id,name,timezone_name,timezone')->select('id','name','work_city_id','latitude','longitude');
                    }])
					->with(['driver'=> function ($query) {
						$query->select('id','user_id')
						->with(['user'=> function ($query) {
							$query->select('id','first_name','last_name','mobile_no','profile_pic','country_code');
						}]);
					}])
					->with(['vehicle'=> function ($query) {
						$query->select('id','car_model_id','color','year','rules_and_regulations','allowed_things')
						->with(['car_model'=> function ($query) {
							$query->select('id','car_company_id','car_type_id','capacity','model_name')
							->with(['car_company'=> function ($query) {
								$query->select('id','name');
							}])
							->with(['car_type'=> function ($query) {
								$query->select('id','name');
							}]);
						},'carPhotoes']);
					}]);
				},'payment'])
				->where('id',$booking_id)
				->first();
		
		    if ($trip->trip->driver->user->address && is_array($trip->trip->driver->user->address)) {
				$trip->trip->driver->user->address = implode($trip->trip->driver->user->address,', '); 
			}
			// $trip->trip->vehicle->vehicle_base_url = asset('storage/driver_car/'.$trip->trip->driver_id).'/';

			return $trip;
	}

	public static function getAdminStatistics()
	{
		return Admin_statistic::find(1);
	}

	public static function checkZeroValue($value){
		$result = ($value > 0) ? -$value : 0;
		return $result;
	}

}