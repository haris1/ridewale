<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarType extends Model
{
	protected $fillable = [
		'id','name','logo','colored_logo'
	];

	protected $table = "car_types";

	public function getLogoAttribute($value){
        if($value == null){
            return '';
        }else{
            $url = asset('storage/type_logo');
            if(file_exists(storage_path().'/app/public/type_logo/'.$value)){
                return $url.'/'.$value;
            }
        }
    }

    public function getColoredLogoAttribute($value){
        if($value == null){
            return '';
        }else{
            $url = asset('storage/type_logo');
            if(file_exists(storage_path().'/app/public/type_logo/'.$value)){
                return $url.'/'.$value;
            }
        }
    }

	public function car_company(){
		return $this->belongsTo('App\CarCompany','company_id');
	}

	public function car_models(){
        return $this->hasOne('App\CarModel','car_company_id','id');
	}
}
