<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BecomeDriver extends Model
{
    protected $fillable = [
		'id','first_name','last_name','phone_number','gender','email','currently_live','going_to_drive','most_frequently','ridesharing_company','company_driving_name','interested_driving','work_permit'
	];
}
