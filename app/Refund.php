<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Refund extends Model
{
    protected $fillable = [
    	'id', 'payment_id', 'transaction_id','charge_id', 'refund_id', 'amount','transaction_time','status'
    ];

    public $timestamps = false;

    public function payment(){
        return $this->belongsTo('App\Payment','payment_id');
    }

    public function transaction(){
        return $this->belongsTo('App\Transaction','transaction_id');
    }
}
