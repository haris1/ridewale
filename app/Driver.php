<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Driver extends Authenticatable
{
	use HasApiTokens,Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','user_id','status','stripe_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function car_driver()
    {
        return $this->hasMany('App\CarDriver','driver_id','id');
    }

    public function trips()
    {
        return $this->hasMany('App\Trip');
    }

    public function documents()
    {
        return $this->morphMany('App\Used_document','ownable');
    }
    
    public function withdraws()
    {
        return $this->hasMany('App\Withdraw');
    }
}
