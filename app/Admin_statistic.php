<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admin_statistic extends Model
{
	protected $fillable = [
		'transfer_cent', 'transfer_fees', 'admin_cent', 'admin_fees','refund_policy','cancellation_policy','cancellation_charge_type','cancellation_charge','tax_cent','refund_charge_cent'
	];
}
