<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Work_country extends Model
{
    protected $fillable = [
		'name',
	];

	public function work_cities(){
        return $this->hasMany('App\Work_city','work_country_id','id');
    }

    public function work_locations(){
        return $this->hasMany('App\Work_location','work_country_id','id');
    }

    public function work_states(){
        return $this->hasMany('App\Work_state','work_country_id','id');
    }
}
