<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{

    protected $fillable = [
        'user_id', 'trip_id','admin_fees', 'transfer_fees','driver_fees', 'total','total_person','withdrow_status','luggage','updated_at','handshake','tax_cent','tax','booking_otp'
    ];


    protected $attributes = [
        'cash_back' => 0,
        'user_withdrow_status' => '0',
    ];

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }

    public function trip(){
        return $this->belongsTo('App\Trip','trip_id');
    }

    public function transaction(){
        return $this->hasOne('App\Transaction','payment_id','id');
    }

    public function refund(){
        return $this->hasOne('App\Refund','payment_id','id');
    }

    public function driver(){
        return $this->hasOne('App\Driver');
    }
}
