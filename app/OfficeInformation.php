<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfficeInformation extends Model
{
    protected $table = "office_informations";

    protected $fillable = [
		'id','city','country','postal_code','longitude','latitude','full_address','location_hours','inquiry_hours','phone','email'
	];
}
