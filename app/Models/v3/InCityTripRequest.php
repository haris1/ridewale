<?php

namespace App\Models\v3;

use Illuminate\Database\Eloquent\Model;

class InCityTripRequest extends Model
{
	protected $fillable = [
		'incity_trip_id', 
		'driver_id',
		'status',
		'cancelled_at'
	];

	public function incity_trip(){
		return $this->belongsTo('App\Models\v3\InCityTrip','incity_trip_id');
	}

	public function driver(){
		return $this->belongsTo('App\Models\v3\Driver');
	}
}
