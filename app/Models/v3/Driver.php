<?php

namespace App\Models\v3;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    protected $fillable = [
        'id','user_id','status','stripe_id','online','active_card','on_ride'
    ];

    protected $appends = ['rating'];
    public function trips(){
        return $this->hasMany('App\Models\v3\Trip');
    }

    public function incity_trips(){
        return $this->hasMany('App\Models\v3\InCityTrip')->where('status',1);
    }

    public function incity_trip_requests(){
        return $this->hasMany('App\Models\v3\InCityTripRequest');
    }

    public function incity_trip_requests_pending(){
        return $this->hasMany('App\Models\v3\InCityTripRequest')
        ->whereHas('incity_trip',function($query){
            $query->where(function($query){
                $query->where('status','scheduled')
                ->orWhere('status','picked_up');
            });
        });
    }

    public function payouts(){
        return $this->hasMany('App\Models\v3\Payout');
    }

    public function driver_penalties(){
        return $this->hasMany('App\Models\v3\DriverPenalty');
    }

    public function declined_bookings(){
        return $this->hasMany('App\Models\v3\DeclinedBooking');
    }

    public function car_driver(){
        return $this->hasMany('App\CarDriver','driver_id','id');
    }

    public function user(){
        return $this->belongsTo('App\Models\v3\User','user_id');
    }

    public function activity_log(){
        return $this->hasMany('App\Models\v3\ActivityLog','driver_id');
    }

    public function ratings(){
        return $this->hasMany('App\Models\v3\DriverRating');
    }

    public function getRatingAttribute()
    {
        return $this->hasMany(DriverRating::class)->avg('rating')?: 0;
    }

}
