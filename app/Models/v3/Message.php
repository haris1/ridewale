<?php

namespace App\Models\v3;

use App\Collections\MessageCollection;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = ['sender_id','receiver_id','message','read_by_sender','read_by_receiver','deleted_by_sender','deleted_by_receiver','created_at'];

    protected $dates = [
        'read_by_sender',
        'created_at'
   	];

    /**
     * Mark the message as read.
     *
     * @return void
    */
    public function markAsRead()
    {
        if (is_null($this->read_by_sender)) {
            $this->forceFill(['read_by_sender' => $this->freshTimestamp()])->save();
        }
    }

    /**
     * Mark the message as unread.
     *
     * @return void
    */
    public function markAsUnread()
    {
        if (! is_null($this->read_by_sender)) {
            $this->forceFill(['read_by_sender' => null])->save();
        }
    }

	public function newCollection(array $models = [])
    {
        return new MessageCollection($models);
    }

    public function receiver()
    {
    	return $this->belongsTo('App\Models\v3\User','receiver_id');
    }

    public function sender()
    {
    	return $this->belongsTo('App\Models\v3\User','sender_id');
    }
}
