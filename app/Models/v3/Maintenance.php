<?php

namespace App\Models\v3;

use Illuminate\Database\Eloquent\Model;

class Maintenance extends Model
{
    protected $table = 'maintanances';

    protected $fillable = [
		'android_version','ios_version','maintanance_mode'
	];
}
