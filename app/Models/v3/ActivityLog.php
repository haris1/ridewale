<?php

namespace App\Models\v3;

use Illuminate\Database\Eloquent\Model;

class ActivityLog extends Model
{
	protected $fillable = ['trip_id','booking_id','driver_id','rider_id','activity'];

	public function user(){
		return $this->belongsTo('App\Models\v3\User','rider_id');
	}

	public function trip(){
		return $this->belongsTo('App\Models\v3\Trip','trip_id');
	}

	public function driver(){
		return $this->belongsTo('App\Models\v3\Driver','driver_id');
	}

	public function booking(){
        return $this->belongsTo('App\Models\v3\Booking','booking_id');
    }

}
