<?php

namespace App\Models\v3;

use Illuminate\Database\Eloquent\Model;

class TripCancellationReason extends Model
{
    protected $fillable = [
    	'content',
    	'status',
    	'user_type',
    	'reason_for'
    ];
}
