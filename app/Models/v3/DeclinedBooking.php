<?php

namespace App\Models\v3;

use Illuminate\Database\Eloquent\Model;

class DeclinedBooking extends Model
{
	protected $fillable = [
		'driver_id',
		'booking_id',
	];

	public function booking(){
		return $this->belongsTo('App\Models\v3\Booking');
	}

	public function driver(){
		return $this->belongsTo('App\Models\v3\Driver');
	}
}
