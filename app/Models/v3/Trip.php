<?php

namespace App\Models\v3;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Trip extends Model
{
  
  protected $fillable = [
    'from_location_id', 
    'to_location_id',
    'driver_id',
    'vehicle_id', 
    'trip_date',
    'pickup_time',
    'dropoff_time',
    'total_seats',
    'price',
    'available_seats',
    'cancelled_reason',
  ];

  protected $dates = [
    'trip_date',
    'pickup_time',
    'dropoff_time',
  ];

  public function driver(){
    return $this->belongsTo('App\Models\v3\Driver');
  }

  public function from_location(){
    return $this->belongsTo('App\Work_location','from_location_id');
  }

  public function to_location(){
    return $this->belongsTo('App\Work_location','to_location_id');
  }

  public function driver_penalties(){
    return $this->hasOne('App\Models\v3\DriverPenalty');
  }

  public function vehicle(){
    return $this->belongsTo('App\CarDriver','vehicle_id');
  }

  public function bookings(){
    return $this->hasMany('App\Models\v3\Booking','trip_id');
  }

  public function activity_log(){
    return $this->hasMany('App\Models\v3\ActivityLog','trip_id');
  }

  public function pending_bookings(){
    return $this->hasMany('App\Models\v3\Booking')->where('status','pending');
  }

  public function confirmed_bookings(){
    return $this->hasMany('App\Models\v3\Booking')->where('status','!=','pending')->where('status','!=','cancelled')->where('status','!=','rejected');
  }

  public function booking_counts(){
    return $this->hasMany('App\Models\v3\Booking')->where('status','!=','cancelled')->whereDoesntHave('declined_booking');
  }

  public function cancelled_bookings(){
    return $this->hasMany('App\Models\v3\Booking')->where('status','cancelled');
  }

  public function scopeStatus($query,$status){
    return $query->where('status',$status);
  }

  public function scopeAvailable($query){
    return $query->where('available_seats','>',0);
  }

  public function scopeUpcoming($query){
    return $query->whereDate('pickup_time','>=',Carbon::now());
  }

  public function driver_rating(){
    return $this->hasOne('App\Models\v3\DriverRating');
  }
}
