<?php

namespace App\Models\v3;

use Illuminate\Database\Eloquent\Model;

class RiderRating extends Model
{
	protected $fillable = [
		'id','booking_id','rating','incity_trip_id','user_id','description'
	];

	public function booking(){
		if(!is_null($this->booking_id)){
			return $this->belongsTo('App\Models\v3\Booking','booking_id');
		}
	}

	public function incity_trip(){
		if(!is_null($this->incity_trip_id)){
			return $this->belongsTo('App\Models\v3\InCityTrip','incity_trip_id');
		}
	}

	public function user(){
		return $this->belongsTo('App\Models\v3\User','user_id');
	}
}
