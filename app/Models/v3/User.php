<?php

namespace App\Models\v3;

use Tymon\JWTAuth\Contracts\JWTSubject;
// use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Country;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;//HasApiTokens,

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password','dob','gender','mobile_no','profile_verified','languages_known','stripe_id','additional_info','address','profile_pic','firebase_android_id','firebase_ios_id','otp','country_code','is_approved','living_address','driver_firebase_android_id','driver_firebase_ios_id','current_latitude','current_longitude','online','device_type','device_id'
    ];
    protected $appends = ['rating'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'pivot'
    ];

    protected $casts = [
        'languages_known' => 'json',
        'address' => 'array',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }


    public function getProfilePicAttribute($value)
    {
        if($value == null){
            return '';
        }else{
            $url = asset('storage/images/users/'.$this->id.'/profile_pic');
            if(file_exists(storage_path().'/app/public/images/users/'.$this->id.'/profile_pic/'.$value)){
                return $url.'/'.$value;
            }
        }
    }

    public function getCountryCodeAttribute($value)
    {
        $dialingCode = Country::where('code',$value)->select('dialing_code')->first();
        if($dialingCode != ''){
            return $dialingCode->dialing_code;
        }
    }

    public function driver(){
        return $this->hasOne('App\Models\v3\Driver','user_id','id');
    }

    public function trips(){
        return $this->hasMany('App\Models\v3\Trip','driver_id');
    }


    // public function payments(){
    //     return $this->hasMany('App\Models\v3\Payment','user_id','id');
    // }

    public function getLanguagesKnownAttribute($value)
    {
        return (empty($value))?[]:json_decode($value);
    }

    public function chat_rooms()
    {
        return $this->belongsToMany('App\ChatRoom','room_participants')->withTimestamps();
    }

    public function user_country(){
        return $this->belongsTo('App\Country','country_code','code');
    }

    // public function getProfilePicAttribute($value){
    //     if ($value=="") {
    //         $profile_pic = '';
    //     } else {
    //         $profile_pic = $value;
    //     }
    //     return $profile_pic;
    // }

    /* Get the user's bookings */
    public function bookings()
    {
        return $this->hasMany('App\Models\v3\Booking');
    }

    public function activity_log(){
        return $this->hasMany('App\Models\v3\ActivityLog','rider_id');
    }

    public function ratings(){
        return $this->hasMany('App\Models\v3\RiderRating');
    }

    public function getRatingAttribute()
    {
        return $this->hasMany(RiderRating::class)->avg('rating')?: 0;
    }
}
