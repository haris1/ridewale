<?php

namespace App\Models\v3;

use Illuminate\Database\Eloquent\Model;

class RiderPenalty extends Model
{
    
    protected $fillable = [
        'id','trip_id','penalty_amount','user_id','penalty_type','utilize'
    ];

    public function trip(){
        return $this->belongsTo('App\Models\v3\Trip','trip_id');
    }
}
