<?php

namespace App\Models\v3;

use Illuminate\Database\Eloquent\Model;

class DriverPenalty extends Model
{
    protected $fillable = [
        'id','trip_id','penalty_amount','driver_id','penalty_type','utilize'
    ];

    public function trip(){
        return $this->belongsTo('App\Models\v3\Trip','trip_id');
    }
}
