<?php

namespace App\Models\v3;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $fillable = [
    	'user_id',
    	'trip_id',
    	'seats',
    	'base_fare',
    	'tax_fare',
    	'transfer_fare',
    	'chargeable_amount',
    	'admin_fare',
    	'driver_fare',
    	'admin_fare_type',
    	'transfer_fare_type',
        'tax_fare_type',
        'admin_percentage',
        'transfer_percentage',
        'tax_percentage',
        'status',
        'handshake',
        'luggage',
        'otp',
        'no_show_at',
        'picked_up_at',
        'dropoff_at',
        'decline_reason',
        'cancellation_reason'
    ];

    public function getChargeableAmountAttribute($value){
        return round($value,2);
    }

    public function declined_booking(){
        return $this->hasOne('App\Models\v3\DeclinedBooking');
    }

    public function getTaxFareAttribute($value){
        return round($value,2);
    }

    public function user(){
        return $this->belongsTo('App\Models\v3\User','user_id');
    }

    public function trip(){
        return $this->belongsTo('App\Models\v3\Trip','trip_id');
    }

    public function payment(){
        return $this->hasOne('App\Models\v3\Payment','booking_id','id');
    }

    public function riderRating(){
        return $this->hasOne('App\Models\v3\RiderRating','booking_id','id');
    }

    public function payment_sum(){
        return $this->hasMany('App\Models\v3\Payment')->sum('refunded_amount');
    }

    public function rider_penalties(){
        return $this->hasOne('App\Models\v3\RiderPenalty');
    }

    public function driver_rating(){
        return $this->hasOne('App\Models\v3\DriverRating');
    }

    public function activity_log(){
        return $this->hasMany('App\Models\v3\ActivityLog','booking_id');
    }
}
