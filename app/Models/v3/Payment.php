<?php

namespace App\Models\v3;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
     protected $fillable = [
    	'booking_id',
    	'amount',
    	'transaction_id',
    	'charge_id',
    	'payment_status',
    	'transaction_time',
    	'refunded_id',
        'refunded_amount',
    	'refunded_tax',
    	'refund_status',
    	'refunded_time',
        'refund_charge_cent'
    ];

    public function booking(){
        return $this->belongsTo('App\Models\v3\Booking','booking_id');
    }

}
