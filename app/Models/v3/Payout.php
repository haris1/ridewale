<?php

namespace App\Models\v3;

use Illuminate\Database\Eloquent\Model;

class Payout extends Model
{
	protected $fillable = [
		'driver_id',
		'payment_info',
		'payout_amount',
		'payout_transaction_id',
		'payout_transaction_time',
		'payout_failure_code',
		'payout_failure_message',
		'payout_status',
	];

	protected $casts = [
    	'payment_info' => 'array'
    ];

	public function driver(){
		return $this->belongsTo('App\Models\v3\Driver');
	}

	public function trip(){
        return '';
    }
}
