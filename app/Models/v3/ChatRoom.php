<?php

namespace App\Models\v3;

use Illuminate\Database\Eloquent\Model;
use stdClass;
use App\Models\v3\Message;

class ChatRoom extends Model
{
    protected $fillable = ['chat_room_id','participants'];

    protected $casts = [
    	'participants' => 'array'
    ];

    protected $hidden = ['pivot'];

    public function messages(){
        return $this->hasMany('App\Models\v3\Message');
    }

    public function last_message(){
        return $this->hasOne('App\Models\v3\Message')->orderBy('created_at','desc');
    }

    // public function auth_user(){
    //     return User::whereIn('id',$this->participants)->select('id','first_name','last_name');
    // }

    public function room_participants(){
        return $this->belongsToMany('App\Models\v3\User','room_participants')->withTimestamps();
    }
}
