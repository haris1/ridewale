<?php

namespace App\Models\v3;

use Illuminate\Database\Eloquent\Model;

class AllowedNotallowedThing extends Model
{
    protected $fillable = ['allowed','description'];
}
