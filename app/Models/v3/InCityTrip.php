<?php

namespace App\Models\v3;

use Illuminate\Database\Eloquent\Model;

class InCityTrip extends Model
{
	protected $fillable = [
		'user_id',
		'from_location', 
		'to_location',
		'status',
		'price',
		'distance',
		'vehicle_id', 
		'pickup_time',
		'dropoff_time',
		'total_seats',
		'otp',
		'rider_cancelled_at',
		'driver_cancelled_at',
		'accept_request_location',
		'pickup_location',
		'dropoff_location',
		'footprint',
		'duration',
		'distance_string'
	];

	protected $dates = [
		'pickup_time',
		'dropoff_time',
		'rider_cancelled_at',
		'driver_cancelled_at',
	];

	protected $casts = [
		'from_location' => 'array',
		'to_location' => 'array',
		'accept_request_location' => 'array',
		'pickup_location' => 'array',
		'dropoff_location' => 'array',
		'footprint' => 'array',
	];

	public function user(){
		return $this->belongsTo('App\Models\v3\User');
	}

	public function vehicle(){
		return $this->belongsTo('App\CarDriver','vehicle_id');
	}

	public function trip_request(){
		return $this->hasMany('App\Models\v3\InCityTripRequest','incity_trip_id');
	}

	public function driver(){
		return $this->hasOne('App\Models\v3\InCityTripRequest','incity_trip_id')->where('status',1);
	}

	public function riderRating(){
		return $this->hasOne('App\Models\v3\RiderRating','incity_trip_id','id');
	}

	public function driver_rating(){
		return $this->hasOne('App\Models\v3\DriverRating','trip_id');
	}

}
