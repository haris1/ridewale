<?php

namespace App\Models\v3;

use Illuminate\Database\Eloquent\Model;

class DriverRating extends Model
{
	protected $fillable = [
		'id','trip_id','rating','booking_id','driver_id','trip_type','description'
	];

	public function trip(){
		if($this->trip_type == 'incity'){
			return $this->belongsTo('App\Models\v3\InCityTrip','trip_id');
		}
		return $this->belongsTo('App\Models\v3\Trip','trip_id');
	}

	public function booking(){
		return $this->belongsTo('App\Models\v3\Booking','booking_id');
	}

	public function driver(){
		return $this->belongsTo('App\Models\v3\Driver');
	}

}
