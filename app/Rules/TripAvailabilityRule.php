<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Trip;

class TripAvailabilityRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $trip = Trip::find($value);
        print_r($attribute);
        print_r($value);
        exit;
        if($trip->available_seats < $requiredSeats){
         return response()->json(['status'=>false,'message'=>"Sorry, there are only ".$trip->available_seats." left to book."]);
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Sorry, there are only ".$trip->available_seats." left to book.';
    }
}
