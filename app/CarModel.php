<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarModel extends Model
{
	protected $table = "car_models";
	
	protected $fillable = [
		'id','car_company_id','car_type_id','model_name','capacity'
	];

	public function car_company(){
		return $this->belongsTo('App\CarCompany','car_company_id','id');
	}

	public function car_type(){
		return $this->belongsTo('App\CarType','car_type_id');
	}

	public function car_drivers(){
        return $this->hasMany('App\CarDriver','car_model_id','id');
	}
}
