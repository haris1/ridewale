<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Work_city extends Model
{
	protected $fillable = [
		'name','work_country_id','work_state_id','timezone_name','timezone'
	];

	public function work_country(){
		return $this->belongsTo('App\Work_country','work_country_id');
	}

	public function work_state(){
		return $this->belongsTo('App\Work_state','work_state_id');
	}

	public function work_locations(){
		return $this->hasMany('App\Work_location','work_city_id','id');
	}

	public function from_city(){
		return $this->hasMany('App\Trip','from_city_id','id');
	}

	public function to_city(){
		return $this->hasMany('App\Trip','to_city_id','id');
	}
}
