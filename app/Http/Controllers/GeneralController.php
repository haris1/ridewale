<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GeneralController extends Controller
{
    
    public function socket(Request $request)
    {
    	return view('driverSocket');
    }  

    public function socket_patient(Request $request)
    {
    	return view('riderSocket');
    }
}
