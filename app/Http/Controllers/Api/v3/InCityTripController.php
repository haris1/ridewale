<?php

namespace App\Http\Controllers\Api\v3;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\v3\Driver;
use App\Models\v3\InCityTrip;
use App\CarType;
use Auth;
use DB;
use Validator;
use Carbon\Carbon;
use App\Helpers\TripHelper;
use App\Helpers\NotificationHelper;
use App\Events\FindTheDriverEvent;

class InCityTripController extends Controller
{
	/** search for trip **/
	public function seachForTrip(Request $request){

		$validator = Validator::make($request->all(),[
			'from_location_latitude'  => 'required|numeric',
			'from_location_longitude' => 'required|numeric',
		]);

		if ($validator->fails()){
			return response()->json(['status' => false,'message' => $validator->messages()->first()]);
		}

		$user   = Auth::user();
		$from_location_latitude  = $request->get('from_location_latitude');
		$from_location_longitude = $request->get('from_location_longitude');


		$vehicles = CarType::with(['car_models'=>function($query) use ($from_location_latitude,$from_location_longitude){
			$query->with(['car_drivers'=>function($query) use ($from_location_latitude,$from_location_longitude){
				$query->with(['driver'=>function($query) use ($from_location_latitude,$from_location_longitude){
					$query->with(['user'=>function($query) use ($from_location_latitude,$from_location_longitude){
						$query->whereRaw(DB::raw('( 6367 * acos( cos( radians('.$from_location_latitude.') ) * cos( radians( current_latitude ) ) * cos( radians( current_longitude ) - radians('.$from_location_longitude.') ) + sin( radians('.$from_location_latitude.') ) * sin( radians( current_latitude ) ) ) )').' < ?',[5])
											// ->where('distance','<=',5)
						->whereNotNull('current_latitude')
						->whereNotNull('current_longitude')
						->where('online',1);
					}]);
				}]);
								}]);//->whereNotNull('logo');
		}])
		->whereHas('car_models')
		->select('id','name','logo','colored_logo')
		->get()
		->map(function($cars)  use ($from_location_latitude,$from_location_longitude){
			$cars->price = 20;
			$cars->capacity =$cars->car_models['capacity']; 
			$drivers = [];
			if($cars->car_models->car_drivers){

				foreach ($cars->car_models->car_drivers as $key => $car_drivers) {
					if($car_drivers->driver->user['current_latitude'] != '' && $car_drivers->driver->user['current_longitude'] != ''){

						$drivers[] = array(
							'vehicle_id'=>$car_drivers->id,
							'name'=>  $car_drivers->driver->user['first_name'].' '.$car_drivers->driver->user['last_name'],
							'latitude'	=>	$car_drivers->driver->user['current_latitude'],
							'longitude'	=>	$car_drivers->driver->user['current_longitude'],
						);
					}
				}
			}
			$cars->cars = $drivers;
			unset($cars->car_models);
			return $cars;
		});


		if($vehicles->count() == 0){
			return response()->json(['status' => false,'message' => 'Sorry No cabs available']);
		}

		return response()->json(['status' => true,'message' => 'Available cabs retrieved successfully.','data' => $vehicles]);
	}

	/** request a ride **/
	public function requestRide(Request $request){

		$validator = Validator::make($request->all(),[
			'from_location_latitude'  	=> 'required|numeric',
			'from_location_longitude' 	=> 'required|numeric',
			'to_location_latitude'    	=> 'required|numeric',
			'to_location_longitude'   	=> 'required|numeric',
			'from_location_address' 	=> 'required',
			'to_location_address' 		=> 'required',
			'from_location_title' 		=> 'required',
			'to_location_title' 		=> 'required',
			// 'from_location_postal_code' => 'required',
			// 'to_location_postal_code' 	=> 'required',
			// 'city' 						=> 'required',
			// 'state' 					=> 'required',
			'distance_string' 			=> 'required',
			'distance'					=> 'required',
			'duration'					=> 'required',
			'vehicle_type_id'        	=> 'exists:car_types,id',
			'driver_id'        		    => 'exists:drivers,id',
		]);

		if ($validator->fails()){
			return response()->json(['status' => false,'message' => $validator->messages()->first()]);
		}

		$user   = Auth::user();

		$from_location['latitude'] 		= $request->from_location_latitude;
		$from_location['longitude'] 	= $request->from_location_longitude;
		$from_location['address'] 		= $request->from_location_address;
		$from_location['title'] 		= $request->from_location_title;
		// $from_location['postal_code'] 	= $request->from_location_postal_code;
		$from_location['city'] 			= 'Ahmedabad';//$request->city;
		$from_location['state'] 		= 'Gujarat';//$request->state;
		$from_location['country'] 		= 'India';//$request->country;

		$to_location['latitude'] 		= $request->to_location_latitude;
		$to_location['longitude'] 		= $request->to_location_longitude;
		$to_location['address'] 		= $request->to_location_address;
		$to_location['title'] 		= $request->to_location_title;
		// $to_location['postal_code'] 	= $request->to_location_postal_code;
		$to_location['city'] 			= 'Ahmedabad';//$request->city;
		$to_location['state'] 			= 'Gujarat';//$request->state;
		$to_location['country'] 		= 'India';//$request->country;
		
		$incityTrip['user_id'] 		 = $data['user_id']		= $user->id;
		$incityTrip['from_location'] = $data['trip_info']['from_location']	= $from_location;
		$incityTrip['to_location'] = $data['trip_info']['to_location']	= $to_location;
		$fare					= TripHelper::getIncityTripFair();
		$incityTrip['price'] =  $data['trip_info']['price']			= $fare['per_km_charge'];
		$incityTrip['distance'] =  $data['trip_info']['distance']       = $request->get('distance');
		$incityTrip['distance_string'] = $data['trip_info']['distance_string']  = $request->get('distance_string');
		$incityTrip['total_seats'] =  $data['trip_info']['total_seats']    = $request->get('total_seats');
		$incityTrip['duration'] =  $data['trip_info']['duration']       = $request->get('duration');

		$incityTrip['otp'] = $data['trip_info']['otp']  = rand(1000,9999);
		$from_location_latitude  = $request->get('from_location_latitude');
		$from_location_longitude = $request->get('from_location_longitude');
		if($request->get('vehicle_type_id') != ''){

			$vehicles = CarType::whereHas('car_models',function($query) use ($from_location_latitude,$from_location_longitude){
				$query->whereHas('car_drivers',function($query) use ($from_location_latitude,$from_location_longitude){
					$query->whereHas('driver',function($query) use ($from_location_latitude,$from_location_longitude){
						$query->whereHas('user',function($query) use ($from_location_latitude,$from_location_longitude){
							$query->whereRaw(DB::raw('( 6367 * acos( cos( radians('.$from_location_latitude.') ) * cos( radians( current_latitude ) ) * cos( radians( current_longitude ) - radians('.$from_location_longitude.') ) + sin( radians('.$from_location_latitude.') ) * sin( radians( current_latitude ) ) ) )').' < ?',[50000000])
							->whereNotNull('current_latitude')
							->whereNotNull('current_longitude')
							->select('id','first_name','last_name','email','dob','mobile_no','profile_pic','online');
						});
					});
								});//->whereNotNull('logo');
			})
			->select('id','name','logo','colored_logo')
			->where('id',$request->get('vehicle_type_id'))
			->first();

			if(!$vehicles){
				return response()->json(['status' => false,'message' => 'Sorry no drivers available']);
			}
			// $data['vehicle_id']		= $vehicles->car_models->car_drivers[0]->id;
			$trip = InCityTrip::create($incityTrip);
			$drivers = [];
			foreach ($vehicles->car_models->car_drivers as $key => $car_driver) {
				$car_driver->driver['car_info'] = ['car_name'=>$vehicles->car_models->model_name,
				'car_color'=>'White',
				'car_number'=>'XYZ123'];
				$car_driver->driver['user'] = $car_driver->driver->user()->select('id','first_name','last_name','email','dob','mobile_no','profile_pic','online')->first();
				$drivers[] = $car_driver->driver->only('id','user_id','online','active_card','on_ride','user','car_info','rating');
			}
			$data['drivers']  = $drivers;
			$incityTrip['trip_id'] = $data['trip_info']['trip_id']  = $trip->id;
			// $driver = Driver::where('id',$reqdata['driver_id'])->with('user')->first();

		}else{

			
			$drivers = Driver::
			whereHas('user',function($query) use ($from_location_latitude,$from_location_longitude){
				$query->select(DB::raw('( 6367 * acos( cos( radians('.$from_location_latitude.') ) * cos( radians( current_latitude ) ) * cos( radians( current_longitude ) - radians('.$from_location_longitude.') ) + sin( radians('.$from_location_latitude.') ) * sin( radians( current_latitude ) ) ) ) AS distance'))
				->whereRaw(DB::raw('( 6367 * acos( cos( radians('.$from_location_latitude.') ) * cos( radians( current_latitude ) ) * cos( radians( current_longitude ) - radians('.$from_location_longitude.') ) + sin( radians('.$from_location_latitude.') ) * sin( radians( current_latitude ) ) ) )').' < ?',[50000000])
				->orderBy('distance');
			})
			->with(['user'=>function($query){
				$query->select('id','first_name','last_name','email','dob','mobile_no','profile_pic','online');
			}])
			->whereHas('car_driver')
			->where('user_id','!=',$user->id)
			->where('online',1)
			->select('id','user_id','online','active_card','on_ride')
			->get()
			->map(function($query){
				$query->car_info = ['car_name'=>$query->car_driver[0]->car_model->model_name,
				'car_color'=>'White',
				'car_number'=>'XYZ123'];
				unset($query->car_driver);
				return $query;
			})
			->toArray();

			if(count($drivers) <= 0){
				return response()->json(['status' => false,'message' => 'Sorry no drivers available']);
			}

			$trip = InCityTrip::create($incityTrip);
			$data['drivers'] = $drivers;
			$incityTrip['trip_id'] = $data['trip_info']['trip_id'] = $trip->id;

		}
		$data['rider'] = $user->only('id','first_name','last_name','email','dob','mobile_no','profile_pic','online','rating');

		// $trip->trip_request()->create($reqdata);
		event(new FindTheDriverEvent($data));
		// print_r($data);die();
		// NotificationHelper::sendPushNotification($driver->user,'newRideRequest',$data);

		return response()->json(['status' => true,'message' => 'Ride request created successfully','data'=>$incityTrip]);
	}

	/** trip detail **/
	public function inCityTripDetail(Request $request){

		$validator = Validator::make($request->all(),[
			'incity_trip_id'   => 'exists:in_city_trips,id',
		]);

		if ($validator->fails()){
			return response()->json(['status' => false,'message' => $validator->messages()->first()]);
		}

		$user  = Auth::user();
		
		$trip  = InCityTrip::where('id',$request->get('incity_trip_id'))
		->with(['user:id,first_name,last_name,mobile_no,profile_pic,country_code','driver'=> function ($query) { 
			$query->with(['driver'=> function ($query) {
				$query->with(['user'=>function($query){
					$query->select('id','first_name','last_name','mobile_no','profile_pic','country_code')->with('ratings')->avg('rating');
				}]);
			}]);
		},'vehicle'=> function ($query) {
			$query->select('id','car_model_id')
			->with(['car_model' => function ($query) {
				$query->select('id','capacity','model_name','car_type_id')->with('car_type:id,name,logo,colored_logo');
			}]);
		}])
		->first();

		return response()->json(['status'=>true,'message'=>'Information retrieved successfully.','data'=>$trip]);

	}

	/** cancel rider ride **/
	public function cancelRide(Request $request) 
	{

		$validator = Validator::make($request->all(),[
			'incity_trip_id' 			=> 'required|exists:in_city_trips,id',
		]);

		if ($validator->fails()){
			$message 	= $validator->messages()->first();
			return response()->json(['status'=>false,'message'=>$message]);
		}

		$inCityTrip 		= InCityTrip::where('id',$request->get('incity_trip_id'))->first();
		$user  = Auth::user();

		if($inCityTrip->status ==  'picked_up' || $inCityTrip->status ==  'drop_off'){
			return response()->json(['status' => false,'message' => 'You can\'t cancel this ride.']);
		}
		if($inCityTrip->status == 'cancelled'){
			return response()->json(['status' => false,'message' => 'Ride already cancelled.']);
		} 

		$inCityTrip->status = 'cancelled';
		$inCityTrip->rider_cancelled_at = Carbon::now()->toDateTimeString();
		$inCityTrip->save();

		return response()->json(['status' => true,'message' => 'Your ride cancelled successfully.']);

	}

	/** Set the rating of driver **/
	public function giveIncityTripRatingToDriver(Request $request)
	{
		$validator = Validator::make($request->all(),[
			'incity_trip_id'	=> 'required|exists:in_city_trips,id',
			'rating'       		=> 'required|numeric|max:5',
			'description'  		=> 'nullable'
		]);

		if ($validator->fails()){
			return response()->json(['status' => false,'message' => $validator->messages()->first()]);
		}

		$user      		= Auth::user();
		$incity_trip_id = $request->get('incity_trip_id');
		$rating    		= $request->get('rating');
		$inCityTrip   	= InCityTrip::where('id',$incity_trip_id)->first();

		if(count($inCityTrip->driver_rating) > 0){
			return response()->json(['status' => false,'message' => 'Rating already given.']);
		}

		if($inCityTrip->status != 'drop_off'){
			return response()->json(['status' => false,'message' => 'Ride not complated.']);
		}

		$rating = $inCityTrip->driver_rating()->create(array('driver_id'=>$inCityTrip->driver->driver->id,'rating' => $rating,'description'=>$request->get('description'),'trip_type'=>'incity'));

		return response()->json([
			'status' => true,
			'message' => 'Rating submitted successfully.',
			'data' => $rating
		]);
	}


}
