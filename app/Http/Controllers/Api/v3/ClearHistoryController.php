<?php

namespace App\Http\Controllers\Api\v3;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\v3\Booking;
use App\Models\v3\DeclinedBooking;
use App\Models\v3\Payment;
use App\Models\v3\Trip;
use App\Models\v3\DriverPenalty;
use App\Models\v3\DriverRating;
use App\Models\v3\Payout;
use App\Models\v3\RiderPenalty;
use App\Models\v3\RiderRating;
use App\Models\v3\TripCancellationReason;
use App\Models\v3\InCityTrip;
use App\Models\v3\InCityTripRequest;
use DB;

class ClearHistoryController extends Controller
{
      /** truncate trip history **/
    public function clearTripHistory(){

		DeclinedBooking::truncate();
		Payment::truncate();
		RiderPenalty::truncate();
		RiderRating::truncate();
		Booking::truncate();
		DriverPenalty::truncate();
		DriverRating::truncate();
		Payout::truncate();
		Trip::truncate();
		DB::table('notifications')
		    ->where('type','!=','App\Notifications\PasswordResetSuccess')
		    ->where('type','!=','App\Notifications\VarificationCode')
		    ->delete();

        return response()->json(['status' => true,'message' => 'Trip history cleared.']);
    }

          /** truncate trip history **/
    public function clearTripRequests(){
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
		InCityTripRequest::truncate();
		InCityTrip::truncate();

        return response()->json(['status' => true,'message' => 'Trip request cleared.']);
    }

}
