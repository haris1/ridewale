<?php

namespace App\Http\Controllers\Api\v3;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Auth;
use App\Models\v3\Trip;
use Exception;
use Carbon\Carbon;
use App\Admin_statistic;
use App\Models\v3\Booking;
use App\Models\v3\DeclinedBooking;
use App\Models\v3\Payment;
use Notification;
use App\Helpers\NotificationHelper;
use App\Notifications\BookingDeclinedNotification;
use App\Notifications\BookingConfirmationNotification;
use App\Helpers\TripHelper;
use DB;
use Stripe\Stripe;
use Stripe\Refund;
use Stripe\Customer;
use App\Notifications\StartTripNotification;
use App\Notifications\CompleteTripNotification;
use App\Notifications\CancelTripNotification;
use App\Notifications\PaymentMethodInvalidNotification;
use App\Notifications\UserMarkedAsNoShowNotification;

class TripController extends Controller
{

    /** create trip **/   
    public function createTrip(Request $request)
    {

        $validator = Validator::make($request->all(),[
            'from_location_id'  => 'required|exists:work_locations,id',
            'to_location_id'    => 'required|exists:work_locations,id',
            'vehicle_id'        => 'required|exists:car_drivers,id',
            'pickup_time'       => 'required|date_format:Y-m-d H:i:s',
            'total_seats'       => 'required',
            'price'             => 'required',
        ]);

        if ($validator->fails()){
            return response()->json(['status' => false,'message' => $validator->messages()->first()]);
        }

        $user   = Auth::user();
        $driver = $user->driver;

        if($request->has('trip_id')){

            $trip_id    = $request->get('trip_id');
            $trip       = $user->driver->trips()->find($trip_id); 

            if($trip == '' || count($trip->booking_counts) > 0){
                return response()->json(['status' => false,'message' => 'You can\'t edit this trip.']);
            }
            $activityData = ['driver_id'=>$trip->driver_id,'trip_id'=>$trip->id,'activity'=>'updateTrip'];

            $message    = 'Trip updated successfully.';

        }else{
           $trip      = new Trip(); 
           $activityData = ['driver_id'=>$driver->id,'trip_id'=>$trip->id,'activity'=>'newTrip'];
           $message    = 'Trip created successfully.';
       }

       $trip['from_location_id']  = $request->get('from_location_id');
       $trip['to_location_id']    = $request->get('to_location_id');
       $trip['driver_id']         = $driver->id;
       $trip['vehicle_id']        = $request->get('vehicle_id');
       $trip['trip_date']         = $request->get('pickup_time');
       $trip['pickup_time']       = $request->get('pickup_time');
            // $trip['dropoff_time']        = $request->get('pickup_time');
       $trip['total_seats']       = $request->get('total_seats');
       $trip['price']             = $request->get('price');
       $trip['available_seats']   = $request->get('total_seats');
       $driver->trips()->save($trip);
       $trip->activity_log()->create($activityData);

       return response()->json(['status' => true,'message' => $message]);
   }

   /** edit seats **/   
   public function editSeats(Request $request)
   {

    $validator = Validator::make($request->all(),[
        'trip_id'           => 'required|exists:trips,id',
        'total_seats'       => 'required'
    ]);

    if ($validator->fails()){
        return response()->json(['status' => false,'message' => $validator->messages()->first()]);
    }

    $user   = Auth::user();
    $driver = $user->driver;

    $trip_id                = $request->get('trip_id');
    $total_seats            = (int)$request->get('total_seats');
    $trip                   = $user->driver->trips()->find($trip_id); 
    $seat_occupied          = $trip->total_seats - $trip->available_seats;
    if($trip == ''){
        return response()->json(['status' => false,'message' => 'You can\'t edit this trip.']);
    }

    if($total_seats < $seat_occupied){
        return response()->json(['status' => false,'message' => 'You can\'t edit seats. '.$seat_occupied.' seats already occupied.']);
    }

    $trip['available_seats']    = $total_seats - $seat_occupied;
    $trip['total_seats']        = $request->get('total_seats');

    $driver->trips()->save($trip);
    $trip->activity_log()->create(['driver_id'=>$trip->driver_id,'trip_id'=>$trip_id,'activity'=>'updateSeat']);

    return response()->json(['status' => true,'message' => 'Total seats updated successfully.','total_seats' => $total_seats,'available_seats' => $trip['available_seats']]);
}

/** driver upcoming trips **/
public function upcomingTrips(Request $request)
{

    $validator = Validator::make($request->all(),[
        'page'  => 'required|numeric|min:1',
    ]);

    if ($validator->fails()){
        return response()->json(['status' => false,'message' => $validator->messages()->first()]);
    }

    $user = Auth::user();
    $driver = $user->driver;
    $page = $request->get('page');

    $trips = $driver
    ->trips()
    ->where('trip_date','>=',Carbon::today())
    ->where('status','scheduled')
    ->with(['from_location'=> function($query){
       $query->with('work_city:id,name,timezone_name,timezone')->select('id','name','work_city_id','latitude','longitude');
   },'to_location'=> function($query){
    $query->with('work_city:id,name,timezone_name,timezone')->select('id','name','work_city_id','latitude','longitude');
},'pending_bookings'=>function($query){
    $query->with('user:id,first_name,last_name,profile_pic,mobile_no,country_code')
    ->select('id','trip_id','user_id','seats','luggage','driver_fare','created_at')
    ->whereDoesntHave('declined_booking');
},'confirmed_bookings'=>function($query){
   $query->with('user:id,first_name,last_name,profile_pic,mobile_no,country_code')
   ->with('riderRating:booking_id,rating')
   ->select('id','trip_id','user_id','driver_fare','status','no_show_at','picked_up_at','dropoff_at');
},'vehicle'=> function ($query) {
    $query->select('id','car_model_id')
    ->with(['car_model' => function ($query) {
        $query->select('id','capacity');
    }]);
}])
    ->withCount(['cancelled_bookings' => function($query) use ($user){
        $query->whereHas('payment');
    }])
    ->orderBy('pickup_time','DESC')
    ->paginate(10,['*'],'page');

    $trips->getCollection()->transform(function($upcoming_trip,$key){

        $upcoming_trip->estimated_earning = round($upcoming_trip->confirmed_bookings->where('status','!=','no_show')->sum('driver_fare'),2);
        return $upcoming_trip;
    });

    return response()->json(['status' => true,'message' => 'Upcoming trips has been retrieved successfully.','trips'=>$trips->all(),'last_page'=>$trips->lastPage()]);
}

/** driver active trips **/
public function activeTrips(Request $request)
{

    $user = Auth::user();
    $driver = $user->driver;

    $trips = $driver
    ->trips()
    ->where('status','started')
    ->with(['from_location'=> function($query){
        $query->with('work_city:id,name,timezone_name,timezone')->select('id','name','work_city_id','latitude','longitude');
    },'to_location'=> function($query){
        $query->with('work_city:id,name,timezone_name,timezone')->select('id','name','work_city_id','latitude','longitude');
    },'pending_bookings'=>function($query){
        $query->with('user:id,first_name,last_name,profile_pic,mobile_no,country_code')
        ->select('id','trip_id','user_id','seats','luggage','driver_fare','created_at')
        ->whereDoesntHave('declined_booking');
    },'confirmed_bookings'=>function($query){
        $query->with('user:id,first_name,last_name,profile_pic,mobile_no,country_code')
        ->with('riderRating:booking_id,rating')
        ->select('id','trip_id','user_id','driver_fare','status','no_show_at','picked_up_at','dropoff_at');
    },'vehicle'=> function ($query) {
        $query->select('id','car_model_id')
        ->with(['car_model' => function ($query) {
            $query->select('id','capacity');
        }]);
    }])
    ->withCount(['cancelled_bookings' => function($query) use ($user){
        $query->whereHas('payment');
    }])
    ->orderBy('pickup_time','DESC')
    ->get();

    $trips->map(function($active_trip,$key){

        $active_trip->estimated_earning = round($active_trip->confirmed_bookings->where('status','!=','no_show')->sum('driver_fare'),2);
        return $active_trip;
    });

    return response()->json(['status' => true,'message' => 'Active trips has been retrieved successfully.','trips'=>$trips]);
}

/** driver completed trips **/
public function completedTrips(Request $request)
{

    $validator = Validator::make($request->all(),[
        'page'  => 'required|numeric|min:1',
    ]);

    if ($validator->fails()){
        return response()->json(['status' => false,'message' => $validator->messages()->first()]);
    }

    $user = Auth::user();
    $driver = $user->driver;
    $page = $request->get('page');

    $trips = $driver
    ->trips()
    ->where('status','completed')
    ->with(['from_location'=> function($query){
        $query->with('work_city:id,name,timezone_name,timezone')->select('id','name','work_city_id','latitude','longitude');
    },'to_location'=> function($query){
        $query->with('work_city:id,name,timezone_name,timezone')->select('id','name','work_city_id','latitude','longitude');
    },'pending_bookings'=>function($query){
        $query->with('user:id,first_name,last_name,profile_pic,mobile_no,country_code')
        ->select('id','trip_id','user_id','seats','luggage','driver_fare','created_at')
        ->whereDoesntHave('declined_booking');
    },'confirmed_bookings'=>function($query){
        $query->with('user:id,first_name,last_name,profile_pic,mobile_no,country_code')
        ->with('riderRating:booking_id,rating')
        ->select('id','trip_id','user_id','driver_fare','status','no_show_at','picked_up_at','dropoff_at');
    },'vehicle'=> function ($query) {
        $query->select('id','car_model_id')
        ->with(['car_model' => function ($query) {
            $query->select('id','capacity');
        }]);
    }])
    ->withCount(['cancelled_bookings' => function($query) use ($user){
        $query->whereHas('payment');
    }])
    ->orderBy('pickup_time','DESC')
    ->paginate(10,['*'],'page');

    $trips->getCollection()->transform(function($completed_trip,$key){

        $completed_trip->estimated_earning = round($completed_trip->confirmed_bookings->where('status','!=','no_show')->sum('driver_fare'),2);
        return $completed_trip;

    });

    return response()->json(['status' => true,'message' => 'Completed trips has been retrieved successfully.','trips'=>$trips->all(),'last_page'=>$trips->lastPage()]);
}

/** driver cancelled trips **/
public function cancelledTrips(Request $request)
{

    $validator = Validator::make($request->all(),[
        'page'  => 'required|numeric|min:1',
    ]);

    if ($validator->fails()){
        return response()->json(['status' => false,'message' => $validator->messages()->first()]);
    }

    $user = Auth::user();
    $driver = $user->driver;
    $page = $request->get('page');

    $trips = $driver
    ->trips()
    ->where('status','cancelled')
    ->with(['from_location'=> function($query){
        $query->with('work_city:id,name,timezone_name,timezone')->select('id','name','work_city_id','latitude','longitude');
    },'to_location'=> function($query){
        $query->with('work_city:id,name,timezone_name,timezone')->select('id','name','work_city_id','latitude','longitude');
    },'pending_bookings'=>function($query){
        $query->with('user:id,first_name,last_name,profile_pic,mobile_no,country_code')
        ->select('id','trip_id','user_id','seats','luggage','driver_fare','created_at')
        ->whereDoesntHave('declined_booking');
    },'confirmed_bookings'=>function($query){
        $query->with('user:id,first_name,last_name,profile_pic,mobile_no,country_code')
        ->with('riderRating:booking_id,rating')
        ->select('id','trip_id','user_id','driver_fare','status','no_show_at','picked_up_at','dropoff_at');
    },'vehicle'=> function ($query) {
        $query->select('id','car_model_id')
        ->with(['car_model' => function ($query) {
            $query->select('id','capacity');
        }]);
    },'driver_penalties:trip_id,penalty_amount'])
    ->withCount(['cancelled_bookings' => function($query) use ($user){
        $query->whereHas('payment');
    }])
    ->orderBy('pickup_time','DESC')
    ->paginate(10,['*'],'page');

    $trips->getCollection()->transform(function($cancelled_trip,$key){

        $cancelled_trip->estimated_earning = round($cancelled_trip->confirmed_bookings->where('status','!=','no_show')->sum('driver_fare'),2);
        if($cancelled_trip->status == 'cancelled' && $cancelled_trip->driver_penalties != ''){
            $cancelled_trip->estimated_earning = TripHelper::checkZeroValue($cancelled_trip->driver_penalties->penalty_amount);
            unset($cancelled_trip->driver_penalties);
        }

        return $cancelled_trip;

    });
    return response()->json(['status' => true,'message' => 'Cancelled trips has been retrieved successfully.','trips'=>$trips->all(),'last_page'=>$trips->lastPage()]);
}

/** driver trip detail **/
public function tripDetail(Request $request)
{

    $validator = Validator::make($request->all(),[
        'trip_id'   => 'required|exists:trips,id',
    ]);

    if ($validator->fails()){
        return response()->json(['status' => false,'message' => $validator->messages()->first()]);
    }

    $user   = Auth::user();
    $tripId = $request->get('trip_id');

    $trip = Trip::where('id',$tripId)
    ->with(['from_location'=> function($query){
        $query->with('work_city:id,name,timezone_name,timezone')->select('id','name','work_city_id','latitude','longitude');
    },'to_location'=> function($query){
        $query->with('work_city:id,name,timezone_name,timezone')->select('id','name','work_city_id','latitude','longitude');
    },'pending_bookings'=>function($query){
        $query->with('user:id,first_name,last_name,profile_pic,mobile_no,country_code')
        ->select('id','trip_id','user_id','seats','luggage','driver_fare','created_at')
        ->whereDoesntHave('declined_booking');
    },'confirmed_bookings'=>function($query){
        $query->with('user:id,first_name,last_name,profile_pic,mobile_no,country_code')
        ->with('riderRating:booking_id,rating')
        ->select('id','trip_id','user_id','driver_fare','base_fare','status','no_show_at','picked_up_at','dropoff_at');
    },'vehicle'=> function ($query) {
        $query->select('id','car_model_id')
        ->with(['car_model' => function ($query) {
            $query->select('id','capacity');
        }]);
    },'driver_penalties:trip_id,penalty_amount'])
    ->withCount(['cancelled_bookings' => function($query) use ($user){
        $query->whereHas('payment');
    }])
    ->first();

    $trip->estimated_earning = round($trip->confirmed_bookings()->where('status','!=','no_show')->sum('driver_fare'),2);
    if($trip->status == 'cancelled' && $trip->driver_penalties != ''){
        $trip->estimated_earning                      = TripHelper::checkZeroValue($trip->estimated_earning);

        $trip->driver_penalties->penalty_amount       = TripHelper::checkZeroValue($trip->driver_penalties->penalty_amount);
        unset($trip->driver_penalties);
    }

    return response()->json(['status' => true,'message' => 'Trip detail has been retrieved successfully.','trips'=>$trip]);
}

/** Estimated earnings by a trip **/
public function estimatedEarning(Request $request){

    $validator = Validator::make($request->all(),[
        'trip_id'   => 'required|exists:trips,id',
    ]);

    if ($validator->fails()){
        return response()->json(['status' => false,'message' => $validator->messages()->first()]);
    }

    $user   = Auth::user();
    $tripId = $request->get('trip_id');

    $trip   = Trip::with(['from_location'=> function($query){
        $query->with('work_city:id,name,timezone_name,timezone')->select('id','name','work_city_id','latitude','longitude');
    },'to_location'=> function($query){
        $query->with('work_city:id,name,timezone_name,timezone')->select('id','name','work_city_id','latitude','longitude');
    },'bookings'=>function($query){
        $query->with('user:id,first_name,last_name,profile_pic,mobile_no,country_code')
        ->where('status','!=','pending')
        ->where('status','!=','rejected')
        ->select('id','trip_id','user_id','seats','base_fare','transfer_fare','admin_fare','luggage','status');
    }])
    ->select('id','from_location_id','to_location_id','trip_date','pickup_time','total_seats','available_seats','price','status','cancelled_reason','created_at','updated_at')
    ->where('id',$tripId)
    ->first();

    $trip->total              = round($trip->confirmed_bookings()->where('status','!=','no_show')->where('status','!=','cancelled')->sum('base_fare'),2);
    $tax_fare                 = round($trip->confirmed_bookings()->where('status','!=','no_show')->where('status','!=','cancelled')->sum('tax_fare'),2);
    $transfer_fare                 = round($trip->confirmed_bookings()->where('status','!=','no_show')->where('status','!=','cancelled')->sum('transfer_fare'),2);
        $trip->ridechimp_fees     = round($trip->confirmed_bookings()->where('status','!=','no_show')->where('status','!=','cancelled')->sum('admin_fare'),2); //+ $tax_fare +  $transfer_fare;   
        $trip->estimated_earning  = round($trip->confirmed_bookings()->where('status','!=','no_show')->where('status','!=','cancelled')->sum('driver_fare'),2);

        $trip->trip_cancelled_fare      = 0;
        $admin_statistics               = Admin_statistic::find(1);
        $trip->trip_cancelled_charge    = 0;

        if($trip->status == 'cancelled' && $trip->driver_penalties != ''){
            $trip->trip_cancelled_fare       = TripHelper::checkZeroValue($trip->estimated_earning);
            $trip->trip_cancelled_charge     = TripHelper::checkZeroValue($trip->driver_penalties->penalty_amount);
            unset($trip->driver_penalties);
        }

        $trip->actual_earning        = round($trip->estimated_earning + $trip->trip_cancelled_fare + $trip->trip_cancelled_charge,2);

        return response()->json(['status' => true,'message' => 'Earning detail has been retrieved successfully.','trips'=>$trip]);

    }

    /** driver start trip **/
    public function startTrip(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'trip_id'   => 'required|exists:trips,id',
        ]);

        if ($validator->fails()){
            return response()->json(['status' => false,'message' => $validator->messages()->first()]);
        }

        $user   = Auth::user();
        $tripId = $request->get('trip_id');
        try {

            $trip = $user->driver->trips()->where('id',$tripId)->first();

            if ($trip->status != 'scheduled') {
                return response()->json(['status' => false,'message' => 'Trip is has been already started.']);
            }

            $confirmedBooking   = $trip->bookings()
            ->where(function ($query) {
                $query->where('status','picked_up')
                ->orWhere('status','no_show');
            })
            ->sum('seats');;
            $pendingBooking     = $trip->bookings()->where('status','pending')->get();

            if (($confirmedBooking + $pendingBooking->sum('seats')) < ($trip->total_seats - $trip->available_seats)) {
                return response()->json(['status' => false,'message' => 'You can\'t start the trip until you pickup the all riders.']);
            }


            foreach ($pendingBooking as $key => $booking) {

                DeclinedBooking::updateOrCreate(['booking_id' => $booking->id,'driver_id' => $user->driver->id],['booking_id' => $booking->id,'driver_id' => $user->driver->id]); 
                $booking->decline_reason     = 'Sorry! Your booking request declined due to driver does not receive your trip request.';
                $booking->status             = 'rejected';
                $booking->save();
            }

            $now_time           = Carbon::now();
            $trip->updated_at   = $now_time;
            $trip->status       = 'started';
            $trip->save();
            $trip->activity_log()->create(['driver_id'=>$trip->driver_id,'trip_id'=>$trip->id,'activity'=>'tripStarted']);

            /** notifications **/
            $trip->user_type        = 'user';
            $trip->booking_id       = null;
            foreach ($trip->bookings as $key => $booking) {

                $trip->booking_id           = $booking->id;
                $booking->user->trip_date   = $booking->created_at;
                $booking->user->notify(new StartTripNotification($trip));
                $user_trip                  = $booking->user;

                NotificationHelper::sendPushNotification($user_trip,'UserTripStart',$trip->toArray());
                $booking->user->trip_id     = $booking->trip_id;
                $booking->user->booking_id  = $booking->id;

            }

            // $trip->user_type = 'driver';
            // $user->notify(new StartTripNotification($trip));

            // NotificationHelper::sendPushNotification($user,'DriverTripStart',$trip->toArray());

            return response()->json(['status' => true,'message' => 'Trip has been started successfully.','trip_status' => $trip->status,'updated_time' => $now_time->toDateTimeString()]);

        } catch (Exception $e) {

            return response()->json(['status' => false,'message' => $e->getMessage()]);

        }
    }


    /** driver complete trip **/
    public function completeTrip(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'trip_id'   => 'required|exists:trips,id',
        ]);

        if ($validator->fails()){
            return response()->json(['status' => false,'message' => $validator->messages()->first()]);
        }

        $user   = Auth::user();
        $tripId = $request->get('trip_id');
        try {

            $trip = $user->driver->trips()->where('id',$tripId)->first();

            if ($trip == '' && $trip->status != 'started') {
                return response()->json(['status' => false,'message' => 'You can\'t complete this trip..']);
            }

            $dropoffRiders      = $trip->bookings()
            ->where(function ($query) {
                $query->where('status','drop_off')
                ->orWhere('status','no_show');
            })
            ->sum('seats');

            if ($dropoffRiders < ($trip->total_seats - $trip->available_seats)) {
                return response()->json(['status' => false,'message' => 'You can\'t complete the trip until you dropoff the all riders.']);
            }

            $now_time           = Carbon::now();
            $trip->updated_at   = $now_time;
            $trip->status       = 'completed';
            $trip->save();
            $trip->activity_log()->create(['driver_id'=>$trip->driver_id,'trip_id'=>$trip->id,'activity'=>'tripCompleted']);

            /** Driver account balance **/
            $driver_trips       = $user->driver->trips()
            ->whereHas('bookings', function ($query) {
                $query->whereHas('payment')->where('status','drop_off');
            })
            ->with(['bookings'=> function ($query) {
                $query->where('status','drop_off');
            }])
            ->where('status','completed')
            ->select('id')
            ->get();

            $driver_trips->map(function($trip,$key){
                $trip->driver_fare += $trip->bookings->sum('driver_fare');
                return $trip;
            });

            $total_earning          = $driver_trips->sum('driver_fare');
            $total_withdraws        = $user->driver->payouts()->sum('payout_amount');
            $penalty                = $user->driver->driver_penalties->sum('penalty_amount');
            $account_balance        = $driver_trips->sum('driver_fare') - $penalty - $total_withdraws;

            if($account_balance > $penalty){
                $user->driver->driver_penalties()->update(['utilize' => 1]);
            }

            /** Notifications **/
            $trip->user_type = 'user';
            $trip->booking_id = null;

            foreach ($trip->bookings as $key => $booking) {

                if($booking->status == 'drop_off'){

                    $trip->booking_id           = $booking->id;
                    $booking->user->trip_id     = $booking->trip_id;
                    $booking->user->booking_id  = $booking->id;
                    $booking->user->notify(new CompleteTripNotification($trip));
                    NotificationHelper::sendPushNotification($booking->user,'UserTripCompleted',$trip->toArray());

                }

            }

            $trip->user_type = 'driver';
            $user->notify(new CompleteTripNotification($trip));
            NotificationHelper::sendPushNotification($user,'DriverTripCompleted',$trip->toArray());

            return response()->json(['status' => true,'message' => 'Trip has been completed successfully.','trip_status' => $trip->status,'updated_time' => $now_time->toDateTimeString()]);

        } catch (Exception $e) {

            return response()->json(['status' => false,'message' => $e->getMessage()]);

        }
    }

    /** driver cancel trip **/
    public function cancelTrip(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'trip_id'            => 'required | exists:trips,id',
            'cancelled_reason'   => 'required',// | exists:trip_cancellation_reasons,content
        ]);

        if ($validator->fails()){
            return response()->json(['status' => false,'message' => $validator->messages()->first()]);
        }

        $user   = Auth::user();
        $tripId = $request->get('trip_id');
        $trip   = $user->driver->trips()
        ->where('id',$tripId)
        ->where('trip_date',">",Carbon::today())
        ->with(['from_location'=> function($query){
            $query->with('work_city:id,name,timezone_name,timezone')->select('id','name','work_city_id','latitude','longitude');
        },'to_location'=> function($query){
            $query->with('work_city:id,name,timezone_name,timezone')->select('id','name','work_city_id','latitude','longitude');
        }])
        ->first();

        if ($trip == '' || $trip->status != 'scheduled') {
            return response()->json(['status' => false,'message' => 'You can\'t cancel the trip.']);
        }
        $data   = [];
        $estimated_earning      = 0;
        try {

            DB::transaction(function () use ($trip,$user,$request,&$estimated_earning){

                Stripe::setApiKey(config('services.stripe.secret'));

                $confirmed_bookings     = $trip->bookings->where('status','confirmed');
                $pending_bookings       = $trip->bookings->where('status','pending');
                $admin_statistic        = TripHelper::getAdminStatistics(); 
                $total_base_fare        = $confirmed_bookings->sum('base_fare');
                $driver_fare            = $confirmed_bookings->sum('driver_fare');
                $penalty['trip_id']     = $trip->id;

                /** Driver account balance **/
                $driver_trips       = $user->driver->trips()
                ->whereHas('bookings', function ($query) {
                    $query->whereHas('payment')->where('status','drop_off');
                })
                ->with(['bookings'=> function ($query) {
                    $query->where('status','drop_off');
                }])
                ->where('status','completed')
                ->select('id')
                ->get();

                $driver_trips->map(function($trip,$key){
                    $trip->driver_fare += $trip->bookings->sum('driver_fare');
                    return $trip;
                });

                $total_earning          = $driver_trips->sum('driver_fare');
                $total_withdraws        = $user->driver->payouts()->sum('payout_amount');
                $penaltyUser                = $user->driver->driver_penalties->sum('penalty_amount');
                $account_balance        = $driver_trips->sum('driver_fare') - $penaltyUser - $total_withdraws;

                if($admin_statistic->cancellation_charge_type == '%'){

                    $penalty['penalty_amount'] = ($driver_fare * $admin_statistic->cancellation_charge) / 100;
                    $penalty['penalty_type']   = 'percentage';

                }else{

                    $penalty['penalty_amount'] = $admin_statistic->cancellation_charge;
                    $penalty['penalty_type']   = 'fixed';

                }

                $penalty['utilize']            = 0;
                if($account_balance > $penalty['penalty_amount']){
                    $penalty['utilize']        = 1;
                }               

                $trip->cancelled_reason = $request->get('cancelled_reason');
                $trip->status           = 'cancelled';
                $trip->save();
                $trip->activity_log()->create(['driver_id'=>$trip->driver_id,'trip_id'=>$trip->id,'activity'=>'tripCancelled']);
                $trip->bookings()->where('status','confirmed')->update(['status'=>'cancelled']);

                $when = Carbon::now()->addMinutes(1);
                foreach ($confirmed_bookings as $key => $confirmed_booking) {

                    /** refund for confirmed booking **/
                    $refund = Refund::create(array(
                        'charge'  => $confirmed_booking->payment->charge_id ,
                        'metadata'=> array('booking_id' => $confirmed_booking->id)
                    ));

                    if($refund){

                        $data['refunded_amount']        = round($confirmed_booking->chargeable_amount ,2);
                        $data['refunded_tax']           = round($confirmed_booking->tax_fare ,2);
                        $data['refunded_id']            = $refund->id;
                        $data['refund_status']          = $refund->status;
                        $data['refunded_time']          = Carbon::createFromTimestamp($refund->created)->toDateTimeString();
                        $data['refund_charge_cent']     = 0;
                        $confirmed_booking->payment->update($data);

                    }

                    /** cancel trip notification **/
                    $bookingInfo['type']                = 'confirmed';
                    $bookingInfo['amount']              = $confirmed_booking->chargeable_amount;
                    $bookingInfo['booking_id']          = $confirmed_booking->id;

                    $charge     = \Stripe\Charge::retrieve(
                        $confirmed_booking->payment->charge_id,
                        ['api_key' => config('services.stripe.secret')]
                    );
                    $card_used                         = $charge->source->last4;
                    $bookingInfo['card_used']          = $card_used;

                    // if($confirmed_booking->user->active_mode == 'user'){
                        $confirmed_booking->user->notify((new CancelTripNotification($trip,$bookingInfo))->delay($when));
                        $trip->bookingInfo  = $bookingInfo;
                        NotificationHelper::sendPushNotification($confirmed_booking->user,'UserTripCancel',$trip->toArray());
                    // }

                }

                $user->driver->driver_penalties()->create($penalty);

                /** pending bookings **/
                foreach ($pending_bookings as $key => $pending_booking) {

                    /** cancel trip notification **/
                    // if($pending_booking->user->active_mode == 'user'){
                        $bookingInfo['type']        = 'pending';
                        $bookingInfo['booking_id']  = $pending_booking->id;
                        $pending_booking->user->notify((new CancelTripNotification($trip,$bookingInfo))->delay($when));
                        $trip->bookingInfo          = $bookingInfo;
                        NotificationHelper::sendPushNotification($pending_booking->user,'UserTripCancel',$trip->toArray());
                    // }

                }

                // Notification::send($users, new CancelTripNotification($trip));
                /** cancel trip notification **/
                $driver                 = $trip->driver->user;
                
                // if($driver->active_mode == 'driver'){

                    $bookingInfo['type']    = 'driver';
                    $bookingInfo['penalty'] = $penalty;
                    $bookingInfo['booking_id']  = '';
                    $driver->notify(new CancelTripNotification($trip,$bookingInfo));
                    $trip->penalty        = $penalty;
                    // NotificationHelper::sendPushNotification($driver,'DriverTripCancel',$trip->toArray());

                // }

                $estimated_earning      = TripHelper::checkZeroValue($penalty['penalty_amount']);

            });

} catch(\Stripe\Error\Card $e) {
    return response()->json(['status' => false,'message' => $e->getMessage()]);
} catch (\Stripe\Error\InvalidRequest $e) {
    return response()->json(['status' => false,'message' => $e->getMessage()]);
} catch (\Stripe\Error\Authentication $e) {
    return response()->json(['status' => false,'message' => $e->getMessage()]);
} catch (\Stripe\Error\ApiConnection $e) {
    return response()->json(['status' => false,'message' => $e->getMessage()]);
} catch (\Stripe\Error\Base $e) {
    return response()->json(['status' => false,'message' => $e->getMessage()]);
} catch (Exception $e) { 
    return response()->json(['status' => false,'message' => $e->getMessage()]);
}

$data['trip_status']            = $trip->status;
$data['estimated_earning']      = $estimated_earning;
$data['cancelled_reason']       = $request->get('cancelled_reason');
return response()->json(['status' => true,'message' => 'Trip has been cancelled successfully.','data'=>$data]);
}

/** driver decline Booking **/
public function declineBooking(Request $request)
{
    $validator = Validator::make($request->all(),[
        'booking_id'        => 'required|exists:bookings,id',
        'decline_reason'    => 'required'
    ]);

    if ($validator->fails()){
        return response()->json(['status' => false,'message' => $validator->messages()->first()]);
    }

    $user = Auth::user();
    $bookingId = $request->get('booking_id');
    $booking   = Booking::where('id',$bookingId)->first();

    if($booking->status != 'pending'){
        return response()->json(['status' => false,'message' => 'You can\'t decline booking.']);
    }

    $declinedBooking = DeclinedBooking::where(['booking_id'=>$bookingId,'driver_id'=>$user->driver->id])->first();

    if($booking->status == 'rejected'){
        return response()->json(['status' => false,'message' => 'booking already declined.']);
    }

    DeclinedBooking::updateOrCreate(['booking_id' => $bookingId,'driver_id' => $user->driver->id],['booking_id' => $bookingId,'driver_id' => $user->driver->id]); 
    $booking->decline_reason     = $request->get('decline_reason');
    $booking->status             = 'rejected';
    $booking->save();
    $booking->activity_log()->create(['driver_id'=>$booking->trip->driver_id,'trip_id'=>$booking->trip_id,'rider_id'=>$booking->user_id,'booking_id'=>$booking->id,'activity'=>'bookingDeclined']);

    /** declined booking rider notifications **/
    $booking->status             = 'rejected';
    $booking->trip->booking_id   = $booking->id;
    $booking->trip->seats        = $booking->seats;
    $booking->user->notify(new BookingDeclinedNotification($booking->trip));

    // if($booking->user->active_mode == 'user'){
        NotificationHelper::sendPushNotification($booking->user,'UserTripBookingDeclined',$booking->trip->toArray());
    // }

    return response()->json(['status' => true,'message' => 'Booking declined successfully.']);

}

/** driver Booking action **/
public function takeBookingAction(Request $request)
{
    $validator = Validator::make($request->all(),[
        'action'       => 'required | in:picked_up,no_show,drop_off,undo',
        'otp'          => 'required_if:action,==,picked_up',
        'booking_id'   => 'required | exists:bookings,id',
    ]);

    if ($validator->fails()){
        return response()->json(['status' => false,'message' => $validator->messages()->first()]);
    }

    $user      = Auth::user();
    $bookingId = $request->get('booking_id');
    $booking   = Booking::where('id',$bookingId)->first();
    $action    = $request->get('action');

    if($booking->status == 'pending'){
        return response()->json(['status' => false,'message' => 'Booking not confirmed.']);
    }

    if($action == 'picked_up' && $request->get('otp') != $booking->otp){
        return response()->json(['status' => false,'message' => 'Booking otp not matched.']);
    }

    $booking->status = $action;
    $now_time        = Carbon::now();

    switch ($action) {
        case 'picked_up':
        $booking->picked_up_at  = $now_time;
        $booking->save();
        $booking->activity_log()->create(['driver_id'=>$booking->trip->driver_id,'trip_id'=>$booking->trip_id,'rider_id'=>$booking->user_id,'booking_id'=>$booking->id,'activity'=>'pickedUp']);

        break;

        case 'no_show':
        $booking->no_show_at    = $now_time;
        $booking->save();
        $booking->activity_log()->create(['driver_id'=>$booking->trip->driver_id,'trip_id'=>$booking->trip_id,'rider_id'=>$booking->user_id,'booking_id'=>$booking->id,'activity'=>'noShowByDriver']);
        break;

        case 'drop_off':
        $booking->dropoff_at    = $now_time;
        $booking->save();
        $booking->activity_log()->create(['driver_id'=>$booking->trip->driver_id,'trip_id'=>$booking->trip_id,'rider_id'=>$booking->user_id,'booking_id'=>$booking->id,'activity'=>'dropOff']);
        break;

        case 'undo':
        $booking->status        = 'confirmed';
        $booking->save();
        break;

        default:
                    # code...
        break;
    }

    if($action == 'no_show'){
        $notification['booking_id']     = $booking->id;
        $notification['trip_id']        = $booking->trip_id;
        $notification['from_location']  = $booking->trip->from_location->work_city->name;
        $notification['to_location']    = $booking->trip->to_location->work_city->name;

        NotificationHelper::sendPushNotification($booking->user,'UserMarkedAsNoShow',$notification);
        $booking->user->notify(new UserMarkedAsNoShowNotification($notification));
    }

    return response()->json(['status' => true,'message' => 'Booking action taken successfully.','booking_status'=>$booking->status,'updated_time' => $now_time->toDateTimeString()]);

}

/** give rating to rider **/
public function giveRiderRating(Request $request)
{

    $validator = Validator::make($request->all(),[
        'booking_id'   => 'required | exists:bookings,id',
        'rating'       => 'required | numeric | max:5',
    ]);

    if ($validator->fails()){
        return response()->json(['status' => false,'message' => $validator->messages()->first()]);
    }

    $user      = Auth::user();
    $bookingId = $request->get('booking_id');
    $rating    = $request->get('rating');
    $booking   = Booking::where('id',$bookingId)->first();

    if(count($booking->riderRating) > 0){
        return response()->json(['status' => false,'message' => 'Rating already given.']);
    }

    if($booking->trip->status != 'completed'){
        return response()->json(['status' => false,'message' => 'Trip not completed.']);
    }

    if($booking->status != 'drop_off'){
        return response()->json(['status' => false,'message' => 'Rider not dropoff.']);
    }

    $booking->riderRating()->create(array('rating' => $rating,'user_id'=>$booking->user_id));
    $rating     = $booking->riderRating()->select('booking_id','rating')->first();

    return response()->json(['status' => true,'message' => 'Rating submited successfully.','data' => $rating]);

}


/** booking confirmation **/
public function confirmBooking(Request $request){

    $validator = Validator::make($request->all(),[
        'booking_id'     => 'required|exists:bookings,id',
    ]);

    if ($validator->fails()){
        return response()->json(['status' => false,'message' => $validator->messages()->first()]);
    }

    $user    = Auth::user();
    $booking = TripHelper::bookingDetail($request->get('booking_id'));

    if($booking->status == 'confirmed'){
        return response()->json(['status' => false,'message' => 'Booking already confirmed.']);
    }

    if($booking->status != 'pending'){
        return response()->json(['status' => false,'message' => 'You cann\'t confirm this booking.']);
    }
    Stripe::setApiKey(config('services.stripe.secret'));

    $trip_user      = $booking->user;
    $retriveResult  = \Stripe\Customer::retrieve($trip_user->stripe_id);
    $carddetail                         =  $retriveResult->sources->retrieve($retriveResult->default_source);
    $notification['card_last4']         =  $carddetail->last4;
    $notification['trip_id']            =  $booking->trip_id;
    $notification['booking_id']         =  $booking->id;

    try{

        DB::transaction(function () use ($booking,$user,$trip_user,$retriveResult){

            /** stripe charge **/
            $charge = \Stripe\Charge::create(array(
                "amount"    => round($booking->chargeable_amount * 100),
                "currency"  => "cad",
                "customer"  =>$trip_user->stripe_id,
                "source"    =>$retriveResult->default_source ,
            ));

            if($charge){
                $data['amount']             = number_format($booking->chargeable_amount,2);
                $data['booking_id']         = $booking->id;
                $data['charge_id']          = $charge->id;
                $data['transaction_id']     = $charge->balance_transaction;
                $data['payment_status']     = $charge->status;
                $data['transaction_time']   = Carbon::createFromTimestamp($charge->created)->toDateTimeString();
                $booking->trip->decrement('available_seats',$booking->seats);

                Payment::create($data);

                $booking->status        = 'confirmed';
                $booking->save();
                $booking->activity_log()->create(['driver_id'=>$booking->trip->driver_id,'trip_id'=>$booking->trip_id,'rider_id'=>$booking->user_id,'booking_id'=>$booking->id,'activity'=>'bookingConfirmed']);

                /* send confirm booking notification */
                $trip_user->notify(new BookingConfirmationNotification($booking));
                // if($trip_user->active_mode == 'user'){
                    NotificationHelper::sendPushNotification($trip_user,'UserBookingConfirmation',$booking->toArray());
                // }
            }

        });

    } catch(\Stripe\Error\Card $e){

        NotificationHelper::sendPushNotification($trip_user,'UserPaymentMethodInvalid',$notification);
        $trip_user->notify(new PaymentMethodInvalidNotification($notification));
        return response()->json(['status' => false,'message' => $e->getMessage()]);

    } catch (\Stripe\Error\NotFound $e) {

        // NotificationHelper::sendPushNotification($trip_user,'UserPaymentMethodInvalid',$notification);
        return response()->json(['status' => false,'message' => $e->getMessage()]);

    } catch (\Stripe\Error\Authentication $e) {

        // NotificationHelper::sendPushNotification($trip_user,'UserPaymentMethodInvalid',$notification);
        return response()->json(['status' => false,'message' => $e->getMessage()]);

    } catch (\Stripe\Error\InvalidRequest $e) {

        NotificationHelper::sendPushNotification($trip_user,'UserPaymentMethodInvalid',$notification);
        $trip_user->notify(new PaymentMethodInvalidNotification($notification));
        return response()->json(['status' => false,'message' => $e->getMessage()]);

    } catch (\Stripe\Error\RateLimit $e) {

        // NotificationHelper::sendPushNotification($trip_user,'UserPaymentMethodInvalid',$notification);
        return response()->json(['status' => false,'message' => $e->getMessage()]);

    } catch (\Stripe\Error\ApiConnection $e) {

        // NotificationHelper::sendPushNotification($trip_user,'UserPaymentMethodInvalid',$notification);
        return response()->json(['status' => false,'message' => $e->getMessage()]);

    } catch (\Stripe\Error\Base $e) {

        // NotificationHelper::sendPushNotification($trip_user,'UserPaymentMethodInvalid',$notification);
        return response()->json(['status' => false,'message' => $e->getMessage()]);

    }

    return response()->json(['status' => true,'message' => "Booking confirmed successfully.",'left_seats' => $booking->trip->available_seats]);

}

}
