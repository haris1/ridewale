<?php

namespace App\Http\Controllers\Api\v3;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Auth;
use App\Used_document;
use App\OfficeInformation;

class DriverController extends Controller
{
    /** driver language update **/
	public function updateDriverLanguages(Request $request) 
	{
		
		$user   = 	Auth::user();

		$validator = Validator::make($request->all(),[
			'languages_known' => "required | json",
		]);

		if ($validator->fails()) {
			return response()->json(['status'=>false,'message' => $validator->messages()->first()]);
		}

		$user->languages_known = json_decode($request->get('languages_known'));
		$user->save();

		return response()->json(['status'=>true,'message' => 'Information updated successfully.','data'=>$user->languages_known]); 
	}

	/**Vehivle info**/
	public function vehicleInfo() 
	{ 
		$user 		= Auth::user();
		$userdata	= $user->driver()->select('id')
										->with(['car_driver'=> function ($query) {
											$query->select('id','driver_id','car_model_id','color','year','rules_and_regulations','allowed_things');
										}])
										->where('user_id',$user->id)
										->first();
		
		$userdata->car_driver = collect($userdata->car_driver)->map(function ($cars)  {

			$cars->company 					= $cars->car_model->car_company['name'];
			$cars->type 					= $cars->car_model->car_type['name'];
			$cars->model 					= $cars->car_model['model_name'];
			$cars->capacity 				= $cars->car_model['capacity'];
			$cars->color 					= $cars->color;
			$cars->year 					= $cars->year;
			$cars->carPhoto 				= $cars->carPhotoes;
			$cars->rules_and_regulations 	= $cars->rules_and_regulations;
			$cars->allowed_things 			= $cars->allowed_things;
			$cars->logo 					= $cars->car_model->car_type['logo'];
			unset($cars->car_model);
			unset($cars->carPhotoes);

		});
		$userdata->base_url 				= asset('storage/driver_car').'/'.$userdata->id.'/';

		return response()->json(['status'=>true,'message'=>'Vehicle info retrieved Successfully.','data' => $userdata]); 
	} 

	/** update driver additional info **/
	public function updateDriverAdditionalInfo(Request $request) 
	{ 
		$input 	= $request->all();
		$user   = Auth::user();

		$validator = Validator::make($input,[
			'additional_info' => "required|string",
		]);

		if ($validator->fails()) {
			return response()->json(['status'=>false,'message' => $validator->messages()->first()]);
		}

		$user->additional_info = $request->get('additional_info');
		$user->save();

		return response()->json(['status'=>true,'message' => 'Information updated successfully.']); 
	} 

	/** driver document status api **/
	public function driverDocuments() 
	{ 
		$user = Auth::user();

		$userDocs = Used_document::where('ownable_id',$user->driver->id)->pluck('expiry_date','document_id')->toArray();

		$documents = collect([
			'1' => ['name' => 'driver_licence'],
			'2'	=> ['name' => 'proof_of_work_eligibility'],
			'3' => ['name' => 'criminal_certificate'],
			'4' => ['name' => 'profile_photo'],
		]);
		$uploadedDocs = array_keys($userDocs);
		$docs = [];
		foreach ($documents as $key => $doc) {
			if(in_array($key, $uploadedDocs)){
				$docs[$doc['name']] = array('status'=>"provided",'data'=>$userDocs[$key]);
				if($userDocs[$key] < date('Y-m-d')){
					$docs[$doc['name']] = array('status'=>"expired",'data'=>$userDocs[$key]);
				}
			}else{
				$docs[$doc['name']] = array('status'=>"not_provided",'data'=>'');
			}
			if($key == '4' && $user->profile_pic != null && file_exists(storage_path().'/app/public/images/users/'.$user->id.'/profile_pic/'.$user->profile_pic)){
				$docs[$doc['name']] = array('status'=>"provided",'data'=>asset('storage/images/users/'.$user->id.'/profile_pic/'.$user->profile_pic));
			}
		}
		$response = [];
		$response['driver_docs'] = $docs;
		$car_drivers=$user->driver->car_driver;

		if(count($car_drivers) > 0){
			$docs = [];
			$i=0;
			foreach ($car_drivers as $key => $car_driver) {

				$carDocs = Used_document::where('ownable_id',$car_driver->id)->pluck('expiry_date','document_id')->toArray();

				$documents = collect([
					'4'  => ['name' => 'vehicle_registration'],
					'5'  => ['name' => 'vehicle_insurance']
				]);

				$uploadedDocs = array_keys($carDocs);
				
				foreach ($documents as $key => $doc) {
					$docs[$i]['car_detail']=array(
						'car_no'=>$car_driver->car_number,
						'model'=>$car_driver->car_model['model_name'],
						'company'=>$car_driver->car_model->car_company['name'],
						'car_type'=>$car_driver->car_model->car_type['name'],
					);
					if(in_array($key, $uploadedDocs)){
						$docs[$i][$doc['name']] = array('status'=>"provided",'expiry_date'=>$carDocs[$key]);
						if($carDocs[$key] < date('Y-m-d')){
							$docs[$i][$doc['name']] = array('status'=>"expired",'expiry_date'=>$carDocs[$key]);
						}
					}else{
						$docs[$i][$doc['name']] = array('status'=>"not_provided",'expiry_date'=>'');
					}
				}
				$i++;
			}
		}

		$response['car_docs'] = $docs;

		// return $response;
		return response()->json(['status'=>true,'message'=>'Information retrived successfully.','data' => $response]); 
	} 

	/** office Info **/
	public function officeInfo(Request $request){

		$validator = Validator::make($request->all(),[
			'city' => "required"
		]);

		if ($validator->fails()) { 
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
		}
		$user = Auth::user();
		$officeInfo = OfficeInformation::where('city',$request->get('city'))
		->first();
		if ($officeInfo) {
			$location_hours=explode(',', $officeInfo['location_hours']);
			$inquiry_hours=explode(',', $officeInfo['inquiry_hours']);
			
			$officeInfo['location_hours'] = $location_hours[0].'-'.$location_hours[1].' : '.$location_hours[2].' - '.$location_hours[3];
			$officeInfo['inquiry_hours']  = $inquiry_hours[0].'-'.$inquiry_hours[1].' : '.$inquiry_hours[2].' - '.$inquiry_hours[3];
			if ($officeInfo) {
				return response()->json(['status'=>true,'message'=>'Office Information','data'=>$officeInfo]);
			} else {
				return response()->json(['status'=>false,'message'=>'Sorry! RideChimp currently not available in your city.','data'=>['id'=>0,
					'city'=>'',
					'country'=>'',
					'postal_code'=>'',
					'longitude'=>'',
					'latitude'=>'',
					'full_address'=>'',
					'location_hours'=>'',
					'inquiry_hours'=>'',
					'phone'=>'',
					'email'=>'',
					'created_at'=>'',
					'updated_at'=>'',
				]]);
			}
		} else {
			return response()->json(['status'=>false,'message'=>'Sorry! RideChimp currently not available in your city.','data'=>['id'=>0,
				'city'=>'',
				'country'=>'',
				'postal_code'=>'',
				'longitude'=>'',
				'latitude'=>'',
				'full_address'=>'',
				'location_hours'=>'',
				'inquiry_hours'=>'',
				'phone'=>'',
				'email'=>'',
				'created_at'=>'',
				'updated_at'=>'',
			]]);
		}
	}

	/** update driver address **/
	public function updateDriverAddress(Request $request) 
	{ 
		$input 	= $request->all();
		$user   = Auth::user();

		$validator = Validator::make($input,[
			'address' => "required",
		]);

		if ($validator->fails()) {
			return response()->json(['status'=>false,'message' => $validator->messages()->first()]);
		}
		$user->address = $request->get('address');
		$user->living_address = $request->get('address');
		$user->save(); 

		return response()->json(['status'=>true,'message' => 'Information updated successfully.']); 
	} 

}
