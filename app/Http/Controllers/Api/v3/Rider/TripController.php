<?php

namespace App\Http\Controllers\Api\v3\Rider;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use TripHelper;
use Carbon\Carbon;
use Validator;
use App\Models\v3\Booking;

class TripController extends Controller
{
    
    /** rider upcoming trips **/
    public function upcomingTrips(Request $request)
    {

        $validator = Validator::make($request->all(),[
            'page'  => 'required|numeric|min:1',
        ]);

        if ($validator->fails()){
            return response()->json(['status' => false,'message' => $validator->messages()->first()]);
        }

        $user   = Auth::user();
        $page   = $request->get('page');
        $admin_statistic    = TripHelper::getAdminStatistics();

        $trips = $user
                    ->bookings()
                    ->whereHas('trip',function ($query){
                        $query->where('status','scheduled');
                    })
                    ->where('status','confirmed')
                    ->with(['trip' => function($query) use ($user){
                        $query->with(['from_location'=> function($query){
                            $query->with('work_city:id,name,timezone_name,timezone')->select('id','name','work_city_id','latitude','longitude');
                        },'to_location'=> function($query){
                            $query->with('work_city:id,name,timezone_name,timezone')->select('id','name','work_city_id','latitude','longitude');
                        },'driver' => function($query){
                            $query->with('user:id,first_name,last_name,mobile_no,country_code,profile_pic')->select('id','user_id');
                        },'vehicle'=> function ($query) {
                            $query->select('id','car_model_id','color')
                            ->with(['car_model' => function ($query) {
                                $query->select('id','car_company_id','car_type_id','capacity')
                                ->with(['car_company'=> function ($query) {
                                    $query->select('id','name');
                                }]);
                            }]);
                        }])
                        ->select('id','from_location_id','to_location_id','driver_id','trip_date','pickup_time','vehicle_id','total_seats','available_seats','status','updated_at')
                        ->withCount(['bookings' => function($query) use ($user){
                            $query->where('user_id','!=',$user->id)->where('status','!=','pending')->where('status','!=','cancelled')->where('status','!=','rejected');
                        }]);
                    },'rider_penalties','driver_rating:id,booking_id,rating'])
                    ->select('id','trip_id','user_id','chargeable_amount','status','otp','driver_not_show_at','created_at','picked_up_at','dropoff_at','base_fare','updated_at')
                    ->orderBy('created_at','ASC')       
                    ->paginate(10, ['*'], 'page', $page);

        $trips->getCollection()
            ->transform(function($booking, $index) use ($admin_statistic){
                
                /** If booking cancelled then return the refund status and amount **/
                $booking->refunded_amount        = 0;
                $booking->cancellation_charge   = 0;
                if($booking->payment['refunded_id'] != ''){
                    $amount    = $booking->base_fare;
                    $booking->refunded_amount        = $booking->payment['refunded_amount'];
                    $booking->cancellation_charge   = ($amount * $admin_statistic->refund_charge_cent) / 100;
                }else{

                    $booking->cancellation_charge    = $booking->payment['amount'] - $booking->payment['refunded_amount'];
                }

                unset($booking->rider_penalties);
                unset($booking->payment);
                return $booking;
            });

        return response()->json([
            'status'    => true,
            'message'   => 'Upcoming trips has been retrieved successfully.',
            'trips'     => $trips->all(),
            'last_page' => $trips->lastPage()
        ]);
    }

    /** rider active trips **/
    public function activeTrips(Request $request)
    {

        $user = Auth::user();
        $admin_statistic    = TripHelper::getAdminStatistics();

        $trips = $user
                    ->bookings()
                    ->whereHas('trip',function ($query){
                        $query->where('status','started');
                    })
                    ->where(function($query){
                        $query->Where('status','!=','pending');
                        $query->Where('status','!=','cancelled');
                        $query->whereNull('decline_reason');
                    })
                    ->with(['trip' => function($query) use ($user){
                    	$query->with(['from_location'=> function($query){
                    		$query->with('work_city:id,name,timezone_name,timezone')->select('id','name','work_city_id','latitude','longitude');
                    	},'to_location'=> function($query){
                    		$query->with('work_city:id,name,timezone_name,timezone')->select('id','name','work_city_id','latitude','longitude');
                    	},'driver' => function($query){
                    		$query->with('user:id,first_name,last_name,mobile_no,country_code,profile_pic')->select('id','user_id');
                    	},'vehicle'=> function ($query) {
                    		$query->select('id','car_model_id','color')
                    		->with(['car_model' => function ($query) {
                    			$query->select('id','car_company_id','car_type_id','capacity')
                    			->with(['car_company'=> function ($query) {
                    				$query->select('id','name');
                    			}]);
                    		}]);
                    	}])
                        ->select('id','from_location_id','to_location_id','driver_id','trip_date','pickup_time','vehicle_id','total_seats','available_seats','status','updated_at')
                        ->withCount(['bookings' => function($query) use ($user){
                            $query->where('user_id','!=',$user->id)->where('status','!=','pending')->where('status','!=','cancelled')->where('status','!=','rejected');
                        }]);
                    },'rider_penalties','driver_rating:id,booking_id,rating'])
                    ->select('id','trip_id','user_id','chargeable_amount','status','otp','driver_not_show_at','decline_reason','picked_up_at','dropoff_at','base_fare','updated_at')
                    ->get()
                    ->map(function($booking, $index) use ($admin_statistic){
        				
        				/* Return the Rider Penalty if He/She cancell the booking */
        				$booking->rider_penalty = 0;
    					$booking->rider_penalty_type   = 'fixed';

                    	if($booking->status == 'cancelled' && $booking->rider_penalties != ''){
                    		if($admin_statistic->cancellation_charge_type == '%'){

                    			$booking->rider_penalty_amount = ($booking->chargeable_amount * $admin_statistic->cancellation_charge) / 100;
                    			$booking->rider_penalty_type   = 'percentage';
                    		}else{

                    			$booking->rider_penalty_amount = $admin_statistic->cancellation_charge;
                    			$booking->rider_penalty_type   = 'fixed';
                    		}         
                    	}

                    	/** If booking cancelled then return the refund status and amount **/
                    	$booking->refunded_amount		= 0;
                    	if($booking->payment['refunded_id'] != ''){
                    		$booking->refunded_amount	= $booking->payment['refunded_amount'];
                    	}
                    	unset($booking->rider_penalties);
                    	unset($booking->payment);
                    	return $booking;
                    });

        return response()->json([
            'status'  => true,
            'message' => 'Active trips has been retrieved successfully.',
            'trips'   => $trips
        ]);
    }

    /** rider completed trips **/
    public function completedTrips(Request $request)
    {

        $validator = Validator::make($request->all(),[
            'page'  => 'required|numeric|min:1',
        ]);

        if ($validator->fails()){
            return response()->json(['status' => false,'message' => $validator->messages()->first()]);
        }

        $user   = Auth::user();
        $page   = $request->get('page');
        $admin_statistic    = TripHelper::getAdminStatistics();

        $trips = $user
                    ->bookings()
                    ->whereHas('trip',function ($query){
                        $query->where('status','completed');
                    })
                    ->where(function($query){
                        $query->Where('status','!=','pending');
                        $query->Where('status','!=','cancelled');
                        $query->whereNull('decline_reason');
                    })
                    ->with(['trip' => function($query) use ($user){
                        $query->with(['from_location'=> function($query){
                            $query->with('work_city:id,name,timezone_name,timezone')->select('id','name','work_city_id','latitude','longitude');
                        },'to_location'=> function($query){
                            $query->with('work_city:id,name,timezone_name,timezone')->select('id','name','work_city_id','latitude','longitude');
                        },'driver' => function($query){
                            $query->with('user:id,first_name,last_name,mobile_no,country_code,profile_pic')->select('id','user_id');
                        },'vehicle'=> function ($query) {
                            $query->select('id','car_model_id','color')
                            ->with(['car_model' => function ($query) {
                                $query->select('id','car_company_id','car_type_id','capacity')
                                ->with(['car_company'=> function ($query) {
                                    $query->select('id','name');
                                }]);
                            }]);
                        }])
                        ->select('id','from_location_id','to_location_id','driver_id','trip_date','pickup_time','vehicle_id','total_seats','available_seats','status','updated_at')
                        ->withCount(['bookings' => function($query) use ($user){
                            $query->where('user_id','!=',$user->id)->where('status','!=','pending')->where('status','!=','cancelled')->where('status','!=','rejected');
                        }]);
                    },'rider_penalties','driver_rating:id,booking_id,rating'])
                    ->select('id','trip_id','user_id','chargeable_amount','status','otp','driver_not_show_at','created_at','decline_reason','picked_up_at','dropoff_at','base_fare','updated_at')
                    ->orderBy('created_at','ASC')       
                    ->paginate(10, ['*'], 'page', $page);

        $trips->getCollection()
            ->transform(function($booking, $index) use ($admin_statistic){
                
                /** If booking cancelled then return the refund status and amount **/
                $booking->refunded_amount        = 0;
                $booking->cancellation_charge   = 0;
                if($booking->payment['refunded_id'] != ''){
                    $amount    = $booking->base_fare;
                    $booking->refunded_amount        = $booking->payment['refunded_amount'];
                    $booking->cancellation_charge   = ($amount * $admin_statistic->refund_charge_cent) / 100;
                }else{

                    $booking->cancellation_charge    = $booking->payment['amount'] - $booking->payment['refunded_amount'];
                }

                unset($booking->rider_penalties);
                unset($booking->payment);
                return $booking;
            });

        return response()->json([
            'status'    => true,
            'message'   => 'Completed trips has been retrieved successfully.',
            'trips'     => $trips->all(),
            'last_page' => $trips->lastPage()
        ]);
    }

    /** rider cancelled trips **/
    public function cancelledTrips(Request $request)
    {

        $validator = Validator::make($request->all(),[
            'page'  => 'required|numeric|min:1',
        ]);

        if ($validator->fails()){
            return response()->json(['status' => false,'message' => $validator->messages()->first()]);
        }

        $user   = Auth::user();
        $page   = $request->get('page');
        $admin_statistic    = TripHelper::getAdminStatistics();

        $trips = $user
                    ->bookings()
                    ->where(function($query){
                        $query->orWhereHas('trip',function ($query){
                            $query->where('status','cancelled');
                        });
                        $query->orWhere('status','cancelled');
                    })
                    ->with(['trip' => function($query) use ($user){
                        $query->with(['from_location'=> function($query){
                            $query->with('work_city:id,name,timezone_name,timezone')->select('id','name','work_city_id','latitude','longitude');
                        },'to_location'=> function($query){
                            $query->with('work_city:id,name,timezone_name,timezone')->select('id','name','work_city_id','latitude','longitude');
                        },'driver' => function($query){
                            $query->with('user:id,first_name,last_name,mobile_no,country_code,profile_pic')->select('id','user_id');
                        },'vehicle'=> function ($query) {
                            $query->select('id','car_model_id','color')
                            ->with(['car_model' => function ($query) {
                                $query->select('id','car_company_id','car_type_id','capacity')
                                ->with(['car_company'=> function ($query) {
                                    $query->select('id','name');
                                }]);
                            }]);
                        }])
                        ->select('id','from_location_id','to_location_id','driver_id','trip_date','pickup_time','vehicle_id','total_seats','available_seats','status','updated_at')
                        ->withCount(['bookings' => function($query) use ($user){
                            $query->where('user_id','!=',$user->id)->where('status','!=','pending')->where('status','!=','cancelled')->where('status','!=','rejected');
                        }]);
                    },'rider_penalties','driver_rating:id,booking_id,rating'])
                    ->select('id','trip_id','user_id','chargeable_amount','status','otp','driver_not_show_at','created_at','picked_up_at','dropoff_at','base_fare','updated_at')
                    ->orderBy('created_at','ASC')       
                    ->paginate(10, ['*'], 'page', $page);

        $trips->getCollection()
            ->transform(function($booking, $index) use ($admin_statistic){
                
                /** If booking cancelled then return the refund status and amount **/
                $booking->refunded_amount        = 0;
                $booking->cancellation_charge   = 0;
                if($booking->payment['refunded_id'] != ''){
                    $amount    = $booking->base_fare;
                    $booking->refunded_amount        = $booking->payment['refunded_amount'];
                    $booking->cancellation_charge   = ($amount * $admin_statistic->refund_charge_cent) / 100;
                }else{

                    $booking->cancellation_charge    = $booking->payment['amount'] - $booking->payment['refunded_amount'];
                }

                unset($booking->rider_penalties);
                unset($booking->payment);
                return $booking;
            });
        return response()->json([
            'status'    => true,
            'message'   => 'Cancelled trips has been retrieved successfully.',
            'trips'     => $trips->all(),
            'last_page' => $trips->lastPage()
        ]);
    }

    /** rider trip's booking detail **/
    public function tripDetail(Request $request)
    {

        $validator = Validator::make($request->all(),[
            'booking_id'  => 'required|exists:bookings,id',
        ]);

        if ($validator->fails()){
            return response()->json(['status' => false,'message' => $validator->messages()->first()]);
        }

        $user   = Auth::user();
        $bookingId   = $request->get('booking_id');
        $admin_statistic    = TripHelper::getAdminStatistics();

        $booking = Booking::where('id',$bookingId)
                    ->with(['trip' => function($query) use ($user){
                        $query->with(['from_location'=> function($query){
                            $query->with('work_city:id,name,timezone_name,timezone')->select('id','name','work_city_id','latitude','longitude');
                        },'to_location'=> function($query){
                            $query->with('work_city:id,name,timezone_name,timezone')->select('id','name','work_city_id','latitude','longitude');
                        },'driver' => function($query){
                            $query->with('user:id,first_name,last_name,mobile_no,country_code,profile_pic')->select('id','user_id');
                        },'vehicle'=> function ($query) {
                            $query->select('id','car_model_id','color')
                            ->with(['car_model' => function ($query) {
                                $query->select('id','car_company_id','car_type_id','capacity')
                                ->with(['car_company'=> function ($query) {
                                    $query->select('id','name');
                                }]);
                            }]);
                        }])
                        ->select('id','from_location_id','to_location_id','driver_id','trip_date','pickup_time','vehicle_id','total_seats','available_seats','status','updated_at')
                        ->withCount(['bookings' => function($query) use ($user){
                            $query->where('user_id','!=',$user->id)->where('status','!=','pending')->where('status','!=','cancelled')->where('status','!=','rejected');
                        }]);
                    },'rider_penalties','driver_rating:id,booking_id,rating'])
                    ->select('id','trip_id','user_id','chargeable_amount','status','otp','driver_not_show_at','created_at','picked_up_at','dropoff_at','base_fare','updated_at')
                    ->first();

        /** If booking cancelled then return the refund status and amount **/
        $booking->refunded_amount       = 0;
        $booking->cancellation_charge   = 0;
        if($booking->payment['refunded_id'] != ''){
            $booking->refunded_amount        = $booking->payment['refunded_amount'];
            $booking->cancellation_charge    = $booking->payment['amount'] - $booking->payment['refunded_amount'];
        }else{
            $amount    = $booking->base_fare;
            $booking->cancellation_charge   = ($amount * $admin_statistic->refund_charge_cent) / 100;
        }

        unset($booking->rider_penalties);
        unset($booking->payment);

        return response()->json([
            'status'    => true,
            'message'   => 'Trip detail has been retrieved successfully.',
            'trip'     => $booking,
        ]);
    }


    /** Mark the driver as no show **/
    public function markDriverAsNotSeen(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'booking_id'   => 'required|exists:bookings,id',
        ]);

        if ($validator->fails()){
            return response()->json(['status' => false,'message' => $validator->messages()->first()]);
        }

        $user = Auth::user();
        $bookingId = $request->get('booking_id');
        $booking   = Booking::find($bookingId);
        $trip      = $booking->trip;

        if($trip->status != 'scheduled'){
            return response()->json(['status' => false,'message' => 'You cannot mark the driver as no show.']);
        }

        $now_time = Carbon::now();
        $booking->driver_not_show_at = $now_time;
        $booking->save();
        $booking->activity_log()->create(['driver_id'=>$booking->trip->driver_id,'trip_id'=>$booking->trip_id,'rider_id'=>$booking->user_id,'booking_id'=>$booking->id,'activity'=>'noShowByRider']);

        return response()->json([
            'status' => true,
            'message' => 'You have marked the driver as no show successfully.',
            'driver_not_show_at' => $now_time->toDateTimeString()

        ]);
    }

    /** Set the rating of driver **/
    public function giveRatingToDriver(Request $request)
    {

        $validator = Validator::make($request->all(),[
            'booking_id'   => 'required|exists:bookings,id',
            'rating'       => 'required|numeric|max:5',
        ]);

        if ($validator->fails()){
            return response()->json(['status' => false,'message' => $validator->messages()->first()]);
        }

        $user      = Auth::user();
        $bookingId = $request->get('booking_id');
        $rating    = $request->get('rating');
        $booking   = Booking::where('id',$bookingId)->first();

        if(count($booking->driver_rating) > 0){
            return response()->json(['status' => false,'message' => 'Rating already given.']);
        }

        if($booking->trip->status != 'completed'){
            return response()->json(['status' => false,'message' => 'Trip not completed.']);
        }

        if($booking->status != 'drop_off'){
            return response()->json(['status' => false,'message' => 'Rider not dropoff.']);
        }

        $rating = $booking->driver_rating()->create(array('rating' => $rating,'driver_id'=>$booking->trip->driver_id));

        return response()->json([
            'status' => true,
            'message' => 'Rating submitted successfully.',
            'data' => $rating
        ]);
    }
}
