<?php

namespace App\Http\Controllers\Api\v3;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Auth;
use App\Models\v3\Trip;
use App\Helpers\TripHelper;
use App\Models\v3\Booking;
use App\Models\v3\Driver;
use App\Work_city;
use App\Work_location;
use Notification;
use App\Helpers\NotificationHelper;
use App\Notifications\BookingNotification;
use App\Notifications\CancelBookingNotification;
use Stripe\Stripe;
use Stripe\Refund;
use Stripe\Customer;
use DB;
use Carbon\Carbon;
use App\FareManagement;

class BookingController extends Controller
{
	/**  **/
	public function getWorkingCities(Request $request) 
	{

		$cities = Work_city::select('id','name','timezone_name','timezone')->where('status',1)->get();
		
		$trips  = Trip::available()
					->with(['from_location' => function ($query) {
						$query->select('id','work_city_id')
						->with('work_city:id,name,timezone_name,timezone');
					},'to_location' => function ($query) {
						$query->select('id','work_city_id')
						->with('work_city:id,name,timezone_name,timezone');
					}])
					->where('trip_date',">",Carbon::today())
					->where('available_seats','>',0)
					->where('status','scheduled')
					->orderBy('created_at','desc')
					->groupBy('from_location_id','to_location_id')
					->select('id','from_location_id','to_location_id','created_at','updated_at')
					->limit(5)
					->get();
		
		return response()->json([
			'status'=> true,
			'message'=> "Cities has been retrieved successfully.",
			'available_trips'=> $trips,
			'data'=> $cities,
		]);
	}

	/** search places cities **/
	public function searchPlacesByCity(Request $request) 
	{

		$validator = Validator::make($request->all(),[
			'from_city_id' => 'required|exists:work_cities,id',
			'to_city_id' => 'required|exists:work_cities,id',
		]);

		if ($validator->fails())
		{
			$message = $validator->messages()->first();
			return response()->json(['status'=>false,'message'=>$message]);
		}

		$user = Auth::user();
		$toCity = $request->get('to_city_id');
		$fromCity = $request->get('from_city_id');

		$fareManagement = FareManagement::where([
											'to_city_id' => $toCity,
											'from_city_id' => $fromCity
										])
										->orWhere([
											'to_city_id' => $fromCity,
											'from_city_id' => $toCity
										])
										->first();
		
		$from_city_locations = Work_location::select('id','name','place_id','latitude','longitude','placeName','postalCode')
									->where('work_city_id',$request->get('from_city_id'))
									->get();

		$to_city_locations   = Work_location::select('id','name','place_id','latitude','longitude','placeName','postalCode')
									->where('work_city_id',$request->get('to_city_id'))
									->get();

		return response()->json(['status'=>true,'message'=>"Locations retrieved successfully.",'max_price'=>$fareManagement->price,'from_city'=>$from_city_locations,'to_city'=>$to_city_locations]);
	}

	/** search cities **/
	public function searchCities(Request $request) 
	{
		
		$user = Auth::user();
		$term = $request->get('city_term');

		$cities = Work_city::select('id','name','timezone','timezone_name')->where('status',1);

		if($term){
			$cities->where('name', 'LIKE','%'.$term.'%');
		}

		$cities=$cities->get();

		return response()->json(['status'=>true,'message'=>"Cities retrieved successfully.",'data'=>$cities]);
	}

	/** rider search trip **/
	public function searchTrip(Request $request) 
	{

		$validator = Validator::make($request->all(),[
			'from' 			=> 'required',
			'to' 			=> 'required',
			// 'total_seats' 	=> 'required',
			'page'			=> 'required'
		]);

		if ($validator->fails()){
			$message = $validator->messages()->first();
			return response()->json(['status'=>false,'message'=>$message]);
		}

		$user 				= Auth::user();
		$from 				= $request->get('from');
		$to 				= $request->get('to');
		$page 				= $request->page;

		/** from & to city **/
		$from_city			= Work_city::where('name','like','%'.$from.'%')->where('status',1)->first();
		$to_city			= Work_city::where('name','like','%'.$to.'%')->where('status',1)->first();

		$trip_date 			= date('Y-m-d',strtotime($request->get('trip_date')));
		// $total_seats		= $request->get('total_seats');
		$page 				= $request->get('page');

		$trips				= Trip::select('id','from_location_id','to_location_id','driver_id','trip_date','pickup_time','price','available_seats','vehicle_id')
								->whereHas('from_location' , function ($query) use ($from_city) {
									$query->select('id','name','latitude','longitude','work_city_id')
									->with('work_city:id,name,timezone_name,timezone')
									->whereHas('work_city' , function ($query) use ($from_city) {
										$query->where('id',$from_city->id);
									});
								})
								->whereHas('to_location', function ($query) use ($to_city) {
									$query->select('id','name','latitude','longitude','work_city_id')
									->with('work_city:id,name,timezone_name,timezone')
									->whereHas('work_city' , function ($query) use ($to_city) {
										$query->where('id',$to_city->id);
									});
								})
								->with(['driver'=> function ($query) {
									$query->with(['user'=> function ($query) {
										$query->select('id','first_name','last_name','mobile_no','profile_pic','country_code');
									}])->select('id','user_id');
								}])
								->with(['from_location'=> function ($query) use ($from_city) {
									$query->select('id','name','latitude','longitude','work_city_id')
									->with('work_city:id,name,timezone_name,timezone');
								}])
								->with(['to_location'=> function ($query) use ($to_city) {
									$query->select('id','name','latitude','longitude','work_city_id')
									->with('work_city:id,name,timezone_name,timezone');
								}])
								->with(['vehicle'=> function ($query) {
									$query->select('id','driver_id','car_model_id')
									->with(['car_model'=> function ($query) {
										$query->select('id','car_company_id','car_type_id','capacity')
										->with(['car_company'=> function ($query) {
											$query->select('id','name');
										}]);
									}]);
								}])	
								->where('trip_date',">",Carbon::today())
								->where('available_seats','>',0)
								->where('status','scheduled')
								// ->where('driver_id','!=',$user->driver->id)
								->paginate(10,['*'],'page',$page);

		return response()->json(['status'=>true,'message'=>'Data retrieved.','trips'=>$trips->all(),'last_page' => $trips->lastPage(),'total_trips' => $trips->total()]);
	}

	/** rider Book the trip **/
	public function bookTheTrip(Request $request)
	{
		$validator = Validator::make($request->all(),[
			'trip_id' 	 => 'required|exists:trips,id',
			'luggage' 	 => 'required|numeric|in:0,1,2',	// no bags, small carry bag, small carry bag with purse
			'total_seat' => 'required|numeric',
		]);
		
		if ($validator->fails()){
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
		}

		$tripId 		= $request->get('trip_id');
		$requiredSeats 	= $request->get('total_seat');
		$luggage 		= $request->get('luggage');
		
		$user = Auth::user();
		$trip = Trip::find($tripId);
		
		if ($trip->available_seats == 0) {
			return response()->json(['status'=>false,'message'=>"Sorry, there are no seat left to book."]);
		}else if($trip->available_seats < $requiredSeats){
			return response()->json(['status'=>false,'message'=>"Sorry, there are only ".$trip->available_seats." seat left to book."]);
		}

		$tripFair = TripHelper::getTripFair($trip,$requiredSeats);

		/** Insert record into DB Table **/
		$book['trip_id'] 			= $tripId;
		$book['user_id'] 			= $user->id;
		$book['seats'] 				= $requiredSeats;
		$book['base_fare']			= $tripFair['base_price'];
		$book['tax_fare']			= $tripFair['tax'];
		$book['transfer_fare']		= $tripFair['transfer_fees'];
		$book['chargeable_amount']	= $tripFair['chargeable_amount'];
		$book['admin_fare']			= $tripFair['admin_fees'];
		$book['driver_fare']		= $tripFair['driver_fees'];
		$book['transfer_fare_type']	= $tripFair['transfer_fare_type'];
		$book['admin_fare_type']	= $tripFair['admin_fare_type'];
		$book['tax_fare_type']		= $tripFair['tax_fare_type'];
		$book['transfer_percentage']= $tripFair['transfer_fare_percentage'];
		$book['admin_percentage']	= $tripFair['admin_fare_percentage'];
		$book['tax_percentage']		= $tripFair['tax_fare_percentage'];
		$book['luggage']			= $luggage;
		$booking_otp            	= rand(1000,9999);
		$book['otp']   				= $booking_otp;
		$userTrip 					= Booking::create($book);
		$userTrip->activity_log()->create(['driver_id'=>$userTrip->trip->driver_id,'trip_id'=>$userTrip->trip_id,'rider_id'=>$userTrip->user_id,'booking_id'=>$userTrip->id,'activity'=>'newBooking']);
		/** Reduce the available seats of trip **/
		// $trip->decrement('available_seats',$requiredSeats);

		/** send notification **/
		// $trip->user_type 	= 'user';
		$trip->booking_id 	= $userTrip->id;
		$trip->seats 		= $requiredSeats;
		// $user->notify(new BookingNotification($trip));
		

		// if($user->active_mode == 'user'){
			// NotificationHelper::sendPushNotification($user,'UserTripBooking',$trip->toArray());
		// }

		$trip->user_name 	= $user->first_name.' '.$user->last_name;
		$driver 			= $trip->driver->user;
		$trip->user_type 	= 'driver';
		$driver->notify(new BookingNotification($trip));

		// if($driver->active_mode == 'driver'){
			NotificationHelper::sendPushNotification($driver,'DriverTripBooking',$trip->toArray());
		// }

		return response()->json(['status'=>true,'message'=>"Your Trip has been booked successfully."]);
	}

	/** Cancel Booking **/
	public function cancelBooking(Request $request) 
	{

		$validator = Validator::make($request->all(),[
			'booking_id' 			=> 'required|exists:bookings,id',
			'cancellation_reason' 	=> 'required'
		]);

		if ($validator->fails()){
			$message 	= $validator->messages()->first();
			return response()->json(['status'=>false,'message'=>$message]);
		}

		$booking 		= Booking::where('id',$request->get('booking_id'))->first();

		if($booking->trip->status !=  'scheduled'){
			return response()->json(['status' => false,'message' => 'You can\'t cancel this booking.']);
		}
		if($booking->status == 'cancelled'){
			return response()->json(['status' => false,'message' => 'Booking already cancelled.']);
		}elseif($booking->status != 'confirmed' && $booking->status != 'pending'){
			return response()->json(['status' => false,'message' => 'You can\'t cancel this booking.']);
		}	

		Stripe::setApiKey(config('services.stripe.secret'));

		try {

			DB::transaction(function () use ($booking,$request){

				$user 					= Auth::user();
				if($booking->status == 'confirmed'){

					/** refund to user **/
					$admin_statistic 			=  TripHelper::getAdminStatistics();				
					$amount						=  $booking->chargeable_amount;
					$refunded_amount			=  $amount - (($amount * $admin_statistic->refund_charge_cent) / 100);
					$refunded_tax			     = $booking->tax_fare - (($booking->tax_fare * $admin_statistic->refund_charge_cent) / 100);

					$refund = Refund::create(array(
						'charge'  => $booking->payment->charge_id ,
						'amount'  => round(($refunded_amount * 100)),
						'metadata'=> array('booking_id' => $booking->id)
					));

					if($refund){

						$data['refunded_amount'] 		= round($refunded_amount ,2);
						$data['refunded_tax'] 			= round($refunded_tax ,2);
						$data['refunded_id'] 			= $refund->id;
						$data['refund_status'] 			= $refund->status;
						$data['refunded_time']			= Carbon::createFromTimestamp($refund->created)->toDateTimeString();
						$data['refund_charge_cent'] 	= $admin_statistic->refund_charge_cent;
						$booking->payment->update($data);

					}

					/** retrive stripe charge object **/
					$charge 	= \Stripe\Charge::retrieve(
						$booking->payment->charge_id,
						['api_key' => config('services.stripe.secret')]
					);
					$card_used	= $charge->source->last4;
					$booking->trip->increment('available_seats',$booking->seats);

				}else{

					$refund 				= true;
					$refunded_amount		= 0;
					$card_used				= false;

				}

				/** database update **/
				if($refund){
					$booking->cancellation_reason 	= $request->get('cancellation_reason');
					$booking->status 				= 'cancelled';
					$booking->save();
					$booking->activity_log()->create(['driver_id'=>$booking->trip->driver_id,'trip_id'=>$booking->trip_id,'rider_id'=>$booking->user_id,'booking_id'=>$booking->id,'activity'=>'bookingCancelled']);
					
					/** notifications **/
					$booking->trip->user_type 		= 'user';
					$booking->trip->user_name 		= $user->first_name.' '.$user->last_name;
					$booking->trip->booking_id 		= $booking->id;
					$booking->trip->refunded_amount = $refunded_amount;
					$booking->trip->card_used 		= $card_used;
					$user->notify(new CancelBookingNotification($booking->trip));
					$booking->trip->seats 			= $booking->seats;

					// if($user->active_mode == 'user'){
						NotificationHelper::sendPushNotification($user,'UserBookingCancel',$booking->trip->toArray());
					// }

					$driver 					= Driver::where('id',$booking->trip->driver_id)->with('user')->first();
					$booking->trip->user_name 	= $user->first_name.' '.$user->last_name;
					$booking->trip->user_type   = 'driver';
					$driver->user->notify(new CancelBookingNotification($booking->trip));

					// if($driver->user->active_mode == 'driver'){
						NotificationHelper::sendPushNotification($driver->user,'DriverBookingCancel',$booking->trip->toArray());
					// }

				}

			});

		} catch(\Stripe\Error\Card $e) {
			return response()->json(['status' => false,'message' => $e->getMessage()]);
		} catch (\Stripe\Error\InvalidRequest $e) {
			return response()->json(['status' => false,'message' => $e->getMessage()]);
		} catch (\Stripe\Error\Authentication $e) {
			return response()->json(['status' => false,'message' => $e->getMessage()]);
		} catch (\Stripe\Error\ApiConnection $e) {
			return response()->json(['status' => false,'message' => $e->getMessage()]);
		} catch (\Stripe\Error\Base $e) {
			return response()->json(['status' => false,'message' => $e->getMessage()]);
		} catch (Exception $e) {
			return response()->json(['status' => false,'message' => $e->getMessage()]);
		}


		return response()->json(['status' => true,'message' => 'Your booking cancelled successfully.']);

	}

	/** trip info **/
	public function tripInfo(Request $request) 
	{

		$validator = Validator::make($request->all(),[
			'trip_id' => 'required|exists:trips,id',
		]);

		if ($validator->fails())
		{
			$message = $validator->messages()->first();
			return response()->json(['status' => false,'message' => $message]);
		}

		$user = Auth::user();
		$trip = Trip::select('id','driver_id','vehicle_id','from_location_id','to_location_id','available_seats','trip_date','pickup_time','price')
					->with(['driver'=> function ($query) {
						$query->select('id','user_id')
						->with(['user'=> function ($query) {
							$query->select('id','first_name','last_name','profile_pic','address','languages_known','additional_info','mobile_no','country_code');
						}]);
					},'from_location'=> function ($query) {
						$query->select('id','name','latitude','longitude','work_city_id')
						->with('work_city:id,name,timezone_name,timezone');
					},'to_location'=> function ($query) {
						$query->select('id','name','latitude','longitude','work_city_id')
						->with('work_city:id,name,timezone_name,timezone');
					},'vehicle'=> function ($query) {
						$query->select('id','car_model_id','color','year','rules_and_regulations','allowed_things')
						->with(['car_model' => function ($query) {
							$query->select('id','car_company_id','car_type_id','capacity','model_name')
							->with(['car_company'=> function ($query) {
								$query->select('id','name');
							}])
							->with(['car_type'=> function ($query) {
								$query->select('id','name','logo');
							}]);
						},'carPhotoes:id,car_driver_id,photo']);
					}])
					->where('id',$request->get('trip_id'))
					->first();

		$trip->completed_rides 	= 0;
		$driver_first_trip		= $trip->driver->trips->first();
		// $timezone 				= $trip->from_location->work_city['timezone'];
		// if($driver_first_trip == ''){
		// 	$joined_date		= Carbon::now($timezone)->format('F, Y');
		// }else{
		// 	$joined_date		= Carbon::parse($driver_first_trip->created_at)->setTimeZone($timezone)->format('F, Y');
		// }
		if($driver_first_trip == ''){
			$joined_date		= Carbon::now()->format('F, Y');
		}else{
			$joined_date		= $driver_first_trip->created_at->format('F, Y');
		}

		$trip->joined_since   	= $joined_date;
		$trip->base_url 		= asset('storage/driver_car').'/'.$trip->driver_id.'/';
		$trip->vehical_base_url = $trip->vehicle->car_model->car_type['logo'];
		if ($trip->driver->user->address && is_array($trip->driver->user->address)) {
			$trip->driver->user->address = implode($trip->driver->user->address,', '); 
		}
		
		if($trip != ''){
			$trip->completed_rides  = Trip::where('driver_id',$trip->driver_id)->where('status','completed')->count();
		}
		unset($trip->driver->trips);

		return response()->json(['status' => true,'message' => 'Data retrieved.','trip' => $trip]);

	}

	/** trip detail  **/
	public function tripDetail(Request $request){

		$validator = Validator::make($request->all(),[
			'trip_id' 	 => 'required|exists:trips,id',
		]);
		
		if ($validator->fails()){
			return response()->json(['status' => false,'message' => $validator->messages()->first()]);
		}
		$user = Auth::user();
		Stripe::setApiKey(config('services.stripe.secret'));

		$tripId 						=  $request->get('trip_id');
		$tripDetail 					=  TripHelper::tripDetail($tripId);
		$admin_statistic 				=  TripHelper::getAdminStatistics();

		$data['vehicle']				=  $tripDetail->vehicle->car_model;
		$data['allowed_things']			=  $tripDetail->vehicle->allowed_things;
		$data['not_allowed_things']		=  $tripDetail->vehicle->rules_and_regulations;
		$data['driver']					=  $tripDetail->driver;
		$data['pickup_location'] 		=  $tripDetail->from_location;
		$data['trip_date']				=  $tripDetail->trip_date->toDatetimeString();
		$data['pickup_time']			=  $tripDetail->pickup_time->toDatetimeString();
		$data['available_seats']		=  $tripDetail->available_seats;

		$data['price']					=  round($tripDetail->price,2);
		$data['tax']					=  $tripDetail->from_location['work_city']['work_state']['tax_percentage'];
		$data['no_of_riders']			=  Booking::where('trip_id',$tripId)->where('status', '!=', 'pending')->where('status','!=','cancelled')->where('status','!=','rejected')->count();
		$data['refund_policy']			=  $admin_statistic->refund_policy;
		$data['cancellation_policy'] 	=  $admin_statistic->cancellation_policy;

		/** retrive card detail **/
		$data['card_detail']			= array();
		$retriveResult					= Customer::retrieve($user->stripe_id);

		if(count($retriveResult->sources['data']) > 0){

			$carddetail 	  			=  $retriveResult->sources->retrieve($retriveResult->default_source);
			$card['card_last4']			=  $carddetail->last4;
			$card['brand']				=  $carddetail->brand;

		}

		$data['card_detail'] 			= $card;

		return response()->json(['status' => true,'message' => "Trip detail retrieved successfully.",'data' => $data]);

	}


	/** rider active trips **/
	public function activeTrips(){

		$user = Auth::user();

		$active_trips = Booking::select('id','trip_id','user_id','seats','chargeable_amount','luggage','created_at','status','picked_up_at')
							->with(['trip'=> function ($query) use ($user) {
								$query->select('id','from_location_id','to_location_id','price','pickup_time')
								->with(['from_location'=> function ($query) {
									$query->with('work_city:id,name,timezone_name,timezone')->select('id','work_city_id','name');
								}])
								->with(['to_location'=> function ($query) {
									$query->with('work_city:id,name,timezone_name,timezone')->select('id','work_city_id','name');
								}]);
							}])
							->where(['status'=>'picked_up','user_id'=>$user->id])	
							->orderBy('created_at','ASC')		
							->get();

		$active_trips->map(function($active_trip,$key){

			$active_trip->chargeable_amount = round($active_trip->chargeable_amount,2);
			if($active_trip->picked_up_at == null){
				$active_trip->picked_up_at = '';
			}
			return $active_trip;

		});

		return response()->json(['status'=>true,'message'=>"Active trips retrieved successfully.",'data'=>$active_trips]);

	}

	/** rider upcoming trips **/
	public function upcomingTrips(Request $request){

		$validator = Validator::make($request->all(),[
			'page'  => 'required|numeric|min:1',
		]);

		if ($validator->fails()){
			return response()->json(['status' => false,'message' => $validator->messages()->first()]);
		}

		$user = Auth::user();
		$page = $request->get('page');

		$upcoming_trips = Booking::select('id','trip_id','user_id','seats','chargeable_amount','created_at','status','picked_up_at')
							->whereHas('trip',function ($query){
								$query->where('trip_date','>=',Carbon::today())->where('status','!=','cancelled');
							})				
							->with(['trip'=> function ($query) use ($user) {
								$query->select('id','from_location_id','to_location_id','price','pickup_time','status')
								->with(['from_location'=> function ($query) {
									$query->with('work_city:id,name,timezone_name,timezone')->select('id','work_city_id','name');
								}])
								->with(['to_location'=> function ($query) {
									$query->with('work_city:id,name,timezone_name,timezone')->select('id','work_city_id','name');
								}]);
							}])
							->where(['status'=>'confirmed','user_id'=>$user->id])	
							->orderBy('created_at','ASC')		
							->paginate(10, ['*'], 'page', $page);

		$upcoming_trips->getCollection()->transform(function($upcoming_trip,$key){
			$upcoming_trip->chargeable_amount = round($upcoming_trip->chargeable_amount,2);
			if($upcoming_trip->picked_up_at == null){
				$upcoming_trip->picked_up_at = '';
			}
			return $upcoming_trip;
		});

		return response()->json(['status' => true,'message'=>"Upcoming trips retrieved successfully.",'data' => $upcoming_trips->all(),'last_page' => $upcoming_trips->lastPage()]);
	}

	/** rider upcoming trips **/
	public function myTrips(Request $request){

		$validator = Validator::make($request->all(),[
			'page'  => 'required|numeric|min:1',
		]);

		if ($validator->fails()){
			return response()->json(['status' => false,'message' => $validator->messages()->first()]);
		}
		$user = Auth::user();
		$page = $request->get('page');

		$all_trips = Booking::select('bookings.id','trip_id','user_id','seats','chargeable_amount','bookings.created_at','bookings.status','picked_up_at')
						->with(['trip'=> function ($query) use ($user) {
							$query->select('id','from_location_id','to_location_id','price','pickup_time','status')
							->with(['from_location'=> function ($query) {
								$query->with('work_city:id,name,timezone_name,timezone')->select('id','work_city_id','name');
							}])
							->with(['to_location'=> function ($query) {
								$query->with('work_city:id,name,timezone_name,timezone')->select('id','work_city_id','name');
							}]);
						}])
						->where(['user_id'=>$user->id])	
						->orderBy('created_at','DESC')		
						->paginate(10, ['*'], 'page', $page);

		$all_trips->getCollection()->transform(function($trip,$key){
			$trip->chargeable_amount = round($trip->chargeable_amount,2);
			if($trip->picked_up_at == null){
				$trip->picked_up_at = '';
			}
			$trip->status 			 = $trip->status;
			unset($trip->declined_booking);
			return $trip;
		});

		return response()->json(['status' => true,'message'=>"Trips retrieved successfully.",'data' => $all_trips->all(),'last_page' => $all_trips->lastPage()]);
	}

	/** booking detail **/
	public function bookingDetail(Request $request){

		$validator = Validator::make($request->all(),[
			'booking_id' 	 => 'required|exists:bookings,id',
		]);

		if ($validator->fails()){
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
		}

		Stripe::setApiKey(config('services.stripe.secret'));
		$user 						= Auth::user();
		$booking 					= TripHelper::bookingDetail($request->get('booking_id'));
		$data['booking_id'] 		= $booking->id;
		$payment_status 			= 'Pending';

		if($booking->payment 	   != 	''){

			$payment_status 		= 'Processed';

			/** retrive charge object from stripe  **/
			$charge			 		= \Stripe\Charge::retrieve(
				$booking->payment->charge_id,
				['api_key' => config('services.stripe.secret')]
			);

			$card['card_last4']			=  $charge->source->last4;
			$card['brand']				=  $charge->source->brand;

		}else{

			$retriveResult		    	=  Customer::retrieve($user->stripe_id);

			/** card detail if payment not done **/
			$data['card_detail']		=  array();	

			if(count($retriveResult->sources['data']) > 0){
				$carddetail 	  			=  $retriveResult->sources->retrieve($retriveResult->default_source);
				$card['card_last4']			=  $carddetail->last4;
				$card['brand']				=  $carddetail->brand;
			}

		}

		/** If booking cancelled refund status and amount **/
		$refunded_amount			= 0;
		$cancellation_charge		= 0;

		if($booking->payment['refunded_id'] == ''){
			$amount 				= $booking->chargeable_amount;
			$admin_statistic 		=  TripHelper::getAdminStatistics();				
			$cancelled_charge		= ($amount * $admin_statistic->refund_charge_cent) / 100;
			$refund_amount			= $amount - $cancelled_charge;

		}else{

			$cancellation_charge	= $booking->payment['amount'] - $booking->payment['refunded_amount'];
			$refund_amount			= $booking->payment['refunded_amount'];
			$refunded_amount		= $booking->payment['refunded_amount'];
		}
		
		if($booking->status == 'cancelled' ||  $booking->trip->status == 'cancelled'){
			if($booking->payment['refunded_id'] == ''){
				$payment_status 	= 'Refund Pending';
			}else{
				$payment_status 	= 'Refunded';
			}
		}

		$data['refund_amount'] 		= $refund_amount;

		if($booking->status == 'rejected' && $booking->payment == ''){

				$payment_status 			= 'Not Charged';
				$data['refund_amount'] 		= 0;
		}

		$data['booking_status'] 	= $booking->status;
		$data['payment_status'] 	= $payment_status;
		$data['decline_reason'] 	= $booking->decline_reason;
		$data['otp'] 				= $booking->otp;
		$data['trip_id'] 			= $booking->trip_id;
		$data['cancelled_reason'] 	= $booking->trip->cancelled_reason;
		$data['booking_updated_at'] = $booking->updated_at->toDatetimeString();

		$data['booking_date'] 		= $booking->created_at->toDatetimeString();
		$data['from_location'] 		= $booking->trip->from_location;
		$data['to_location'] 		= $booking->trip->to_location;
		$data['trip_date'] 			= $booking->trip['trip_date']->toDatetimeString();
		$data['pickup_time'] 		= $booking->trip['pickup_time']->toDatetimeString();
		$data['pickup_at'] 			= $booking->picked_up_at;
		$data['trip_status'] 		= $booking->trip['status'];
		$data['trip_updated_at'] 	= $booking->trip['updated_at']->toDatetimeString();
		$data['vehicle'] 			= [ 'company'=>$booking->trip['vehicle']['car_model']['car_company']['name'],
		'capacity'=>$booking->trip['vehicle']['car_model']['capacity']
		];

		/** trip charges **/
		$payment_detail				= [ 'price'					=> round($booking->trip->price,2),
										'total_seats'			=> $booking->seats,
										'base_fare'  			=> round($booking->base_fare,2),
										'tax_fare'				=> round($booking->tax_fare,2),
										'total'					=> round($booking->chargeable_amount,2),
										'refunded_amount' 		=> round($refunded_amount,2),
										'cancellation_charge' 	=> round($cancellation_charge,2)
										];

		$data['payment_detail'] 	= $payment_detail;		
		$data['card_detail'] 		= $card;
		$data['driver_info'] 		= $booking->trip->driver;
		$output = array_map(function ($value) {
						return $value === null ? '' : $value;
				  } , $data); 

		return response()->json(['status'=>true,'message'=>"Booking detail retrieved successfully.",'data'=>$output]);
	}
}
