<?php

namespace App\Http\Controllers\Api\v3;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\v3\InCityTripRequest;
use App\Models\v3\InCityTrip;
use App\Events\RideStartedEvent;
use Carbon\Carbon;
use Validator;
use Auth;

class InCityTripDriverController extends Controller
{
	/** make me online / offline **/
	public function makeMeOnlineOffline(Request $request){

		$validator = Validator::make($request->all(),[
			'online' 	=> "required|boolean",
		]);

		if ($validator->fails()) { 
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
		}

		$user = Auth::user();
		$user->online = $request->get('online');
		$user->save();

		return response()->json(['status'=>true,'message'=>'Status updated.']);

	}

	/** accept ride request **/
	public function acceptOrDeclineRideRequest(Request $request){

		$validator = Validator::make($request->all(),[
			'incity_trip_id'   => 'exists:in_city_trips,id',
			'action'           => 'required|boolean',
			'current_latitude'  => 'required_if:action,==,1',
	        'current_longitude' => 'required_if:action,==,1'
		]);

		if ($validator->fails()){
			return response()->json(['status' => false,'message' => $validator->messages()->first()]);
		}

		$user   = Auth::user();

		$ride = InCityTrip::where('id',$request->get('incity_trip_id'))->first();
		$ride->accept_request_location  = ['latitude'=>$request->get('current_latitude'),'longitude'=>$request->get('current_longitude'),'created_at'=>Carbon::now()->toDateTimeString()];
		$ride->save();

		// $triRequest = InCityTripRequest::where(['incity_trip_id'=>$request->get('incity_trip_id'),'driver_id'=>$user->driver->id])->first();
		$triRequest = $ride->trip_request()->where('driver_id',$user->driver->id)->first();
		$triRequest->status = $request->get('action');
		$triRequest->save();

		if($request->get('action')){
			$ride_otp   = rand(1000,9999);
			$triRequest->incity_trip()->update(['otp'=>$ride_otp,'status'=>'scheduled']);
		}

		return response()->json(['status'=>true,'message'=>'Information updated successfully.']);

	}

	/** trip detail **/
	public function inCityTripRequestDetail(Request $request){

		$validator = Validator::make($request->all(),[
			'trip_request_id'   => 'exists:in_city_trips,id',
		]);

		if ($validator->fails()){
			return response()->json(['status' => false,'message' => $validator->messages()->first()]);
		}

		$user  = Auth::user();
		$trip  = InCityTripRequest::where('id',$request->get('trip_request_id'))
		->with(['incity_trip'=>function($query){
			$query->with(['user:id,first_name,last_name,mobile_no,profile_pic,country_code','vehicle'=> function ($query) {
				$query->select('id','car_model_id')
				->with(['car_model' => function ($query) {
					$query->select('id','capacity','model_name','car_type_id')
					->with('car_type:id,name,logo');
				}]);
			}])->select('id','user_id','from_location','to_location','status','price','distance','vehicle_id','pickup_time','dropoff_time','total_seats','rider_cancelled_at','driver_cancelled_at','duration');
		}])
		->select('incity_trip_id')
		->first();

		$trip->user_ratings = $trip->incity_trip->user->ratings()->avg('rating');

		return response()->json(['status'=>true,'message'=>'Information retrieved successfully.','data'=>$trip]);

	}

	/** driver Booking action **/
	public function takeRideAction(Request $request)
	{
		$validator = Validator::make($request->all(),[
	        'action'       		=> 'required | in:picked_up,drop_off',//no_show, undo
	        'otp'          		=> 'required_if:action,==,picked_up',
	        'incity_trip_id'  	=> 'required | exists:bookings,id',
	        'current_latitude'  => 'required',
	        'current_longitude' => 'required',
	        'footprint'  		=> 'required_if:action,==,drop_off | json',
	    ]);

		if ($validator->fails()){
			return response()->json(['status' => false,'message' => $validator->messages()->first()]);
		}

		$user      		= Auth::user();
		$inCityTripId 	= $request->get('incity_trip_id');
		$ride   		= InCityTrip::where('id',$inCityTripId)->first();
		$action   		= $request->get('action');

		if($ride->status == 'pending'){
			return response()->json(['status' => false,'message' => 'Ride not confirmed.']);
		}

		if($action == 'picked_up' && $request->get('otp') != $ride->otp){
			return response()->json(['status' => false,'message' => 'Ride otp not matched.']);
		}

		$ride->status = $action;
		$now_time        = Carbon::now();

		switch ($action) {
			case 'picked_up':
			$ride->pickup_time  = $now_time;
			$ride->pickup_location  = ['latitude'=>$request->get('current_latitude'),'longitude'=>$request->get('current_longitude'),'created_at'=>Carbon::now()->toDateTimeString()];
			$ride->save();
			$data = ['rider_id'=>$ride->user_id, 'driver_id'=>$user->id];
			event(new RideStartedEvent($data));
	        // $ride->activity_log()->create(['driver_id'=>$ride->trip->driver_id,'trip_id'=>$ride->trip_id,'rider_id'=>$ride->user_id,'ride_id'=>$ride->id,'activity'=>'pickedUp']);

			break;

	        // case 'no_show':
	        // $ride->no_show_at    = $now_time;
	        // $ride->save();
	        // $ride->activity_log()->create(['driver_id'=>$ride->trip->driver_id,'trip_id'=>$ride->trip_id,'rider_id'=>$ride->user_id,'ride_id'=>$ride->id,'activity'=>'noShowByDriver']);
	        // break;

			case 'drop_off':
			$ride->dropoff_time     = $now_time;
			$ride->dropoff_location = ['latitude'=>$request->get('current_latitude'),'longitude'=>$request->get('current_longitude'),'created_at'=>Carbon::now()->toDateTimeString()];
			$ride->footprint        = json_decode($request->get('footprint'));
			$ride->driver->driver()->update(['active_card'=>0,'on_ride'=>0]);
			$ride->save();
	        // $ride->activity_log()->create(['driver_id'=>$ride->trip->driver_id,'trip_id'=>$ride->trip_id,'rider_id'=>$ride->user_id,'ride_id'=>$ride->id,'activity'=>'dropOff']);
			break;

	        // case 'undo':
	        // $ride->status        = 'confirmed';
	        // $ride->save();
	        // break;

			default:
	                    # code...
			break;
		}

	    // if($action == 'no_show'){
	    //     $notification['ride_id']     = $ride->id;
	    //     $notification['trip_id']        = $ride->trip_id;
	    //     $notification['from_location']  = $ride->trip->from_location->work_city->name;
	    //     $notification['to_location']    = $ride->trip->to_location->work_city->name;

	    //     NotificationHelper::sendPushNotification($ride->user,'UserMarkedAsNoShow',$notification);
	    //     $ride->user->notify(new UserMarkedAsNoShowNotification($notification));
	    // }

		return response()->json(['status' => true,'message' => 'Ride action taken successfully.','ride_status'=>$ride->status,'updated_time' => $now_time->toDateTimeString()]);

	}

	/** cancel rider ride **/
	public function cancelRide(Request $request) 
	{

		$validator = Validator::make($request->all(),[
			'incity_trip_id' 			=> 'required|exists:in_city_trips,id',
		]);

		if ($validator->fails()){
			$message 	= $validator->messages()->first();
			return response()->json(['status'=>false,'message'=>$message]);
		}

		$incity_trip_id     = $request->get('incity_trip_id');

		$inCityTrip 		= InCityTrip::where('id',$incity_trip_id)->first();
		$user  = Auth::user();

		if($inCityTrip->status ==  'picked_up' || $inCityTrip->status ==  'drop_off'){
			return response()->json(['status' => false,'message' => 'You can\'t cancel this ride.']);
		}
		if($inCityTrip->status == 'cancelled'){
			return response()->json(['status' => false,'message' => 'Ride already cancelled.']);
		} 

		$current_time 		= Carbon::now()->toDateTimeString();
		$inCityTrip->status = 'cancelled';
		$inCityTrip->driver_cancelled_at = $current_time;
		$inCityTrip->save();

		$user->driver->incity_trip_requests()->where('incity_trip_id',$incity_trip_id)->update(['cancelled_at',$current_time]);

		return response()->json(['status' => true,'message' => 'Your ride cancelled successfully.']);

	}

	/** give rating to rider **/
	public function giveIncityTripRiderRating(Request $request)
	{

		$validator = Validator::make($request->all(),[
			'incity_trip_id'	=> 'required|exists:in_city_trips,id',
			'rating'       		=> 'required|numeric|max:5',
			'description'  		=> 'nullable'
		]);

		if ($validator->fails()){
			return response()->json(['status' => false,'message' => $validator->messages()->first()]);
		}

		$user      		= Auth::user();
		$incity_trip_id = $request->get('incity_trip_id');
		$rating    		= $request->get('rating');
		$inCityTrip   	= InCityTrip::where('id',$incity_trip_id)->first();

		if(count($inCityTrip->riderRating) > 0){
			return response()->json(['status' => false,'message' => 'Rating already given.']);
		}

		if($inCityTrip->status != 'drop_off'){
			return response()->json(['status' => false,'message' => 'Ride not complated.']);
		}

		$rating = $inCityTrip->riderRating()->create(array('user_id' => $inCityTrip->user_id,'rating' => $rating,'description' => $request->get('description')));

		return response()->json(['status' => true,'message' => 'Rating submited successfully.','data' => $rating]);

	}
}
