<?php

namespace App\Http\Controllers\Api\v3;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Stripe\Stripe;
use Stripe\Customer;
use Auth;
use Validator;
use App\Models\v3\Payment;
use App\Models\v3\Booking;
use App\Models\v3\Payout;
use Notification;
use App\Notifications\WithdrawNotification;
use App\Helpers\NotificationHelper;
use DB;

class PaymentController extends Controller
{
    public function __construct(){
		Stripe::setApiKey(config('services.stripe.secret'));
	}

	/** retrive cards **/
	public function retrieveCards()
	{

		$user 			= Auth::user();
		$customerCards	= array();
		$defaultCard 	= '';

		if($user->stripe_id != ''){
			$customerCards 		= Customer::retrieve($user->stripe_id);

			if(count($customerCards) > 0){
				$defaultCard 	= $customerCards->default_source;
			}

			$customerCards 		= $customerCards->sources['data'];
		}

		return response()->json(['status' => true,'message' => "Card retrieved successfully.",'data' => $customerCards,'default_card' => $defaultCard]);

	}

	/** add cards **/
	public function addCard(Request $request)
	{

		$validator = Validator::make($request->all(),[
			'card_number' 	=> "required|numeric|max:9999999999999999",
			'exp_month' 	=> "required|numeric|max:12",
			'exp_year' 		=> "required|numeric|max:9999",
			'cvc' 			=> "required|numeric|max:999"
		]);

		if ($validator->fails()) { 
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
		}

		$user = Auth::user();
		try{

			/** create stripe token for adding card  **/
			$tokenData = \Stripe\Token::create(array(
				"card" => array(
					"number"    => $request->get('card_number'),
					"exp_month" => $request->get('exp_month'),
					"exp_year"  => $request->get('exp_year'),
					"cvc"       => $request->get('cvc'),
				)));

			/** create stripe customer if stripe_id not created **/
			if($user->stripe_id == null){

				$customer 	= Customer::create(array(
					'email' 	=> $user->email,
					'source'  	=> $tokenData->id
				));

				$user->stripe_id  	= $customer->id;
				$user->save();
				$retriveResult 		= Customer::retrieve($customer->id);

			}else{

				$retriveResult 		= Customer::retrieve($user->stripe_id);
				$retriveResult->sources->create(array("source" => $tokenData->id));

			}

		} catch(\Stripe\Error\Card $e){
			return response()->json(['status' => false,'message' => $e->getMessage()]);
		} catch (\Stripe\Error\NotFound $e) {
			return response()->json(['status' => false,'message' => $e->getMessage()]);
		} catch (\Stripe\Error\Authentication $e) {
			return response()->json(['status' => false,'message' => $e->getMessage()]);
		} catch (\Stripe\Error\InvalidRequest $e) {
			return response()->json(['status' => false,'message' => $e->getMessage()]);
		} catch (\Stripe\Error\RateLimit $e) {
			return response()->json(['status' => false,'message' => $e->getMessage()]);
		} catch (\Stripe\Error\ApiConnection $e) {
			return response()->json(['status' => false,'message' => $e->getMessage()]);
		} catch (\Stripe\Error\Base $e) {
			return response()->json(['status' => false,'message' => $e->getMessage()]);
		}

		$retriveResult	= Customer::retrieve($user->stripe_id);
		$customerCards 	= $retriveResult->sources['data'];
		return response()->json(['status' => true,'message' => "Card added successfully.",'data' => $customerCards,'default_card' => $retriveResult->default_source]);

	}

	/** remove cards **/
	public function removeCard(Request $request)
	{

		$validator = Validator::make($request->all(),[
			'card_id' => "required",
		]);

		if ($validator->fails()) { 
			return response()->json(['status' => false,'message' => $validator->messages()->first()]);            
		}	

		$user 		= Auth::user();
		$stripe_id  = Auth::user()->stripe_id;
		$card_id	= $request->get('card_id');
		$default_source = '';
		$customerCards  = [];
		try{  

			/** retrieve stripe customer **/
			$retriveResult	= Customer::retrieve($stripe_id);
			$retriveResult->sources->retrieve($card_id)->delete();
			$retriveResult	= Customer::retrieve($stripe_id);
			$default_source = $retriveResult->default_source; 
			$customerCards 	= $retriveResult->sources['data'];

		} catch(\Stripe\Error\Card $e){
			return response()->json(['status' => false,'message' => $e->getMessage()]);
		} catch (\Stripe\Error\NotFound $e) {
			return response()->json(['status' => false,'message' => $e->getMessage()]);
		} catch (\Stripe\Error\Authentication $e) {
			return response()->json(['status' => false,'message' => $e->getMessage()]);
		} catch (\Stripe\Error\InvalidRequest $e) {
			return response()->json(['status' => false,'message' => $e->getMessage()]);
		} catch (\Stripe\Error\RateLimit $e) {
			return response()->json(['status' => false,'message' => $e->getMessage()]);
		} catch (\Stripe\Error\ApiConnection $e) {
			return response()->json(['status' => false,'message' => $e->getMessage()]);
		} catch (\Stripe\Error\Base $e) {
			return response()->json(['status' => false,'message' => $e->getMessage()]);
		}
		return response()->json(['status' => true,'message' => "Card removed successfully.",'data' => $customerCards,'default_card' => $default_source]);

	}

	/** set card as default **/
	public function setDefaltCard(Request $request)
	{

		$validator = Validator::make($request->all(),[
			'card_id' => "required",
		]);

		if ($validator->fails()) { 
			return response()->json(['status' => false,'message' => $validator->messages()->first()]);            
		}	

		$user 		= Auth::user();
		$stripe_id 	= Auth::user()->stripe_id;
		$card_id	= $request->get('card_id');

		try {

			$retriveResult 					= Customer::retrieve($stripe_id);
			$retriveResult->default_source  = $card_id;
			$retriveResult->save(); 

		} catch(\Stripe\Error\Card $e){
			return response()->json(['status' => false,'message' => $e->getMessage()]);
		} catch (\Stripe\Error\NotFound $e) {
			return response()->json(['status' => false,'message' => $e->getMessage()]);
		} catch (\Stripe\Error\Authentication $e) {
			return response()->json(['status' => false,'message' => $e->getMessage()]);
		} catch (\Stripe\Error\InvalidRequest $e) {
			return response()->json(['status' => false,'message' => $e->getMessage()]);
		} catch (\Stripe\Error\RateLimit $e) {
			return response()->json(['status' => false,'message' => $e->getMessage()]);
		} catch (\Stripe\Error\ApiConnection $e) {
			return response()->json(['status' => false,'message' => $e->getMessage()]);
		} catch (\Stripe\Error\Base $e) {
			return response()->json(['status' => false,'message' => $e->getMessage()]);
		}
		$customerCards = $retriveResult->sources['data'];

		return response()->json(['status' => true,'message' => "Default card set successfully.",'data' => $customerCards,'default_card' => $retriveResult->default_source]);

	}

	/** update snow angel card detail **/
	public function updateBankDetails(Request $request)
	{
		$user 				= Auth::user();
		$user_id 			= $user->_id;
		$strip_connect_id 	= $user->driver['stripe_id'];

		if ($strip_connect_id == '') {

			/** create stripe connect acccount **/
			$acct = \Stripe\Account::create(array(
				"country" => "CA",
				"type" => "custom",
				"email"=>$user->email
			));
			$strip_connect_id = $acct->id;

			$user->driver()->update(array('stripe_id'=>$strip_connect_id));

		}
		$account = \Stripe\Account::retrieve($strip_connect_id);

		try {

			$rules=[
				'account_holder_name'	=> 'required',
				'transit_number'	 	=> 'required',
				'institution_number' 	=> 'required',
				// 'account_number' 	=> 'required',
				// 'first_name' 		=> 'required',
				// 'last_name' 			=> 'required',
				// 'gender' 			=> 'required',
				'line1'	 				=> 'nullable',
				'line2' 				=> 'nullable',
				'city' 					=> 'required',
				'state' 				=> 'required',
				'postal_code' 			=> 'required',
				// 'email' 				=> 'required',
				// 'personal_id_number' => 'required',
				'document_front_pic' 	=> 'required|file',
			];

			if(count($account->external_accounts['data']) < 1){
				$rules['account_number'] = 'required';
			}

			if(!$account->legal_entity['personal_id_number_provided']){
				$rules['personal_id_number'] = 'required';
			}

			/** validation stuff **/
			$validator = Validator::make($request->all(),$rules);

			if ($validator->fails()) {
				return response()->json(['status'=>false,'message' => $validator->messages()->first()]);
			}

			/** get the parameters **/

			$firstName 		= $user->first_name;
			$lastName 		= $user->last_name;
			$fullName 		= $firstName.' '.$lastName;
			$gender 		= $user->gender;
			$dob 			= $user->dob; //it should be array ['day' => 02, 'month => 11', 'year' => 2022]
			$fullDate 		= explode('-', $dob);
			$day 			= $fullDate[2];
			$month 			= $fullDate[1];
			$year 			= $fullDate[0];
			$email 			= $user->email;//$request->get('email');
			$phoneNumber 	= $request->get('phone_number');

			$line1 			= $request->get('line1');
			$line2 			= $request->get('line2');
			$city 			= $request->get('city');
			$state 			= $request->get('state');
			$postalCode 	= $request->get('postal_code');
			$country 		= "CA";
			$docFrontPic 	= $request->file('document_front_pic');

			if(count($account->external_accounts['data']) < 1){

				$accountNumber 				= $request->get('account_number');
				$accountHolderName 			= $request->get('account_holder_name');
				$transit_number 			= $request->get('transit_number');
				$institution_number 		= $request->get('institution_number');
				$routingNumber 				= $transit_number.'-'.$institution_number;

				/** account information **/
				$account->external_account  = [
												"object"				=> "bank_account",
												"country"				=> "CA",
												"currency"				=> "CAD",
												"account_holder_name"	=> $fullName,
												"account_holder_type"	=> "individual",
												"routing_number"		=> $routingNumber,
												"account_number"		=> $accountNumber,
											];

			}

			if(!$account->legal_entity['personal_id_number_provided']){

				$personalIdNumber 							= $request->get('personal_id_number'); 
				$account->legal_entity->personal_id_number 	= $personalIdNumber;

			}

			/** upload the identity document **/
				$stripeDocumentFile = false;

				if ($request->hasFile('document_front_pic')) {
					$uploadedFile 		= \Stripe\FileUpload::create(
												array("purpose" => "identity_document",
													'file' => fopen($docFrontPic,'r')
												)
											);
					$stripeDocumentFile = $uploadedFile->id;
				}

				// $account 		= \Stripe\Account::retrieve($strip_connect_id);

				$account->email = $email;


				if ($account->legal_entity->verification->status == 'unverified') {
				    /** address **/
					$account->legal_entity['address'] = [
															"line1" 		=> $line1,
															"line2" 		=> $line2,
															"city" 			=> $city,
															"state" 		=> $state,
															"postal_code" 	=> $postalCode,
															"country" 		=> $country,
														];

					/** personal info **/
					$account->legal_entity->dob = [
													'day' 	=> $day,
													'month' => $month,
													'year' 	=> $year
												  ];


					$account->legal_entity->first_name = $firstName;

					if($lastName != ''){
						$account->legal_entity->last_name 	= $lastName;
					}

					$account->legal_entity->gender 			= $gender;
					$account->legal_entity->phone_number 	= $phoneNumber;
					$account->legal_entity->type 			= "individual";


					if ($stripeDocumentFile) {
						$account->legal_entity->verification->document = $stripeDocumentFile;
					}
				}

				//terms acceptance
				$account['tos_acceptance'] = [
												"date" 			=> now()->timestamp,
												"ip" 			=> $request->ip(),
												"user_agent" 	=> $request->server('HTTP_USER_AGENT'),
											 ];
											 // print_r($account->external_accounts);die();
				//personal information
				$account->save();
			$response[0]['email'] 				= $account->email;
			$response[0]['legal_entity'] 		= $account->legal_entity;
			$response[0]['external_accounts'] 	= $account->external_accounts;

			//give the response
			return response()->json(['status' => true,'message' => "Your bank information has been submitted successfully.",'data' =>  $response]);

		}  catch (\Stripe\Error\InvalidRequest $e) {
			$body = $e->getJsonBody();
			$err  = $body['error'];

			if (isset($err['code']) && $err['code'] == "resource_already_exists") {
				return ['status' => false,'message' => $err['message'].' in our records']; 
			}
			return response()->json(['status' => false,'message' => $e->getMessage()]); 
		} catch (Stripe\Error\Card $e) {
			return response()->json(['status' => false,'message' => 'Wrong Card information provided']);
		} catch (Stripe\Error\Authentication $e) {
			return response()->json(['status' => false,'message' => 'Problem occures with Authentications']);
		} catch (Stripe\Error\ApiConnection $e) {
			return response()->json(['status' => false,'message' => 'Something went wrong with api connection']);
		} catch (Stripe\Error\Base $e) {
			return response()->json(['status' => false,'message' => 'Base Error occures']);
		} catch(Exception $e){
			return response()->json(['status' => false,'message' => 'Something went wrong']);
		}
	}

	/** get driver bank account detail **/
	public function getBankDetails(Request $request)
	{
		$user 				= Auth::user();
		$user_id 			= $user->id;
		$strip_connect_id 	= $user->driver['stripe_id'];
		$response 			= [];

		try {

			$account 		= '';

			if ($strip_connect_id != null) {

				$account 							= \Stripe\Account::retrieve($strip_connect_id);
				$response[0]['email'] 				= $account->email;
				$response[0]['legal_entity'] 		= $account->legal_entity;
				$response[0]['external_accounts'] 	= $account->external_accounts;
				
			}

			return response()->json(['status'=>true,'data'=>$response,'message'=>'Bank details has been retrieved successfully.']);

		}  catch (\Stripe\Error\InvalidRequest $e) {
			$body = $e->getJsonBody();
			$err  = $body['error'];

			if (isset($err['code']) && $err['code'] == "resource_already_exists") {
				return ['status' => false,'message' => $err['message'].' in our records']; 
			}
			return response()->json(['status' => false,'message' => $e->getMessage()]); 
		} catch (Stripe\Error\Card $e) {
			return response()->json(['status' => false,'message' => 'Wrong Card information provided']);
		} catch (Stripe\Error\Authentication $e) {
			return response()->json(['status' => false,'message' => 'Problem occures with Authentications']);
		} catch (Stripe\Error\ApiConnection $e) {
			return response()->json(['status' => false,'message' => 'Something went wrong with api connection']);
		} catch (Stripe\Error\Base $e) {
			return response()->json(['status' => false,'message' => 'Base Error occures']);
		} catch(Exception $e){
			return response()->json(['status' => false,'message' => 'Something went wrong']);
		}
	}

	/** transaction History **/
	public function transactionHistory(Request $request){

		$validator = Validator::make($request->all(),[
            'page'  => 'required|numeric|min:1',
        ]);

        if ($validator->fails()){
            return response()->json(['status' => false,'message' => $validator->messages()->first()]);
        }

        $user 		= Auth::user();
        $driver_id 	= $user->driver->id;
        $page 		= $request->get('page');

        $tripBookings 		= Booking::select('trip_id as id',DB::raw('SUM(driver_fare) as amount'),'trips.trip_date as date',DB::raw('"booking" AS type'),'from_city.name as from_city','to_city.name as to_city')
        								->whereHas('payment', function ($query) {
			                                $query->where('refunded_id','')
			                                ->whereHas('booking',function ($query) {
				                                $query->where('status','drop_off');
				                            });
			                            })
        								->whereHas('trip', function ($query) use ($driver_id){
			                                $query->where('driver_id',$driver_id)->where('status','completed');
			                            })
        								->join('trips',function($requests){
								            $requests->on('bookings.trip_id','=','trips.id');
								        })
								        ->join('work_locations as from_location',function($requests){
								            $requests->on('from_location.id','=','trips.from_location_id');
								        })
								        ->join('work_cities as from_city',function($requests){
								            $requests->on('from_city.id','=','from_location.work_city_id');
								        })
								      	->join('work_locations as to_location',function($requests){
								            $requests->on('to_location.id','=','trips.to_location_id');
								        })
								        ->join('work_cities as to_city',function($requests){
								            $requests->on('to_city.id','=','to_location.work_city_id');
								        })
        								->groupBy('trip_id');
// print_r($tripBookings->get());die();

	     $withdraws 		= $user->driver->payouts()->select('id','payout_amount as amount','created_at as date',DB::raw('"withdraw" AS type'),DB::raw('"" AS from_city'),DB::raw('"" AS to_city'))->with('trip');

	     $driver_penalties  = $user->driver->driver_penalties()->select('trip_id as id','penalty_amount as amount','driver_penalties.created_at as date',DB::raw('"penalty" AS type'),'from_city.name as from_city','to_city.name as to_city')
			     								->join('trips',function($requests){
										            $requests->on('driver_penalties.trip_id','=','trips.id');
										        })
										        ->join('work_locations as from_location',function($requests){
										            $requests->on('from_location.id','=','trips.from_location_id');
										        })
										        ->join('work_cities as from_city',function($requests){
										            $requests->on('from_city.id','=','from_location.work_city_id');
										        })
										      	->join('work_locations as to_location',function($requests){
										            $requests->on('to_location.id','=','trips.to_location_id');
										        })
										        ->join('work_cities as to_city',function($requests){
										            $requests->on('to_city.id','=','to_location.work_city_id');
										        })
					     					 	->unionAll($tripBookings)
									            ->unionAll($withdraws)
									            ->orderBy('date','desc')
									            ->paginate(20);


		$total_withdraws 		= 0;
		$driver_trips 			= $user->driver->trips()
												 ->whereHas('bookings', function ($query) {
					                                $query->whereHas('payment')->where('status','drop_off');
					                              })
												 ->with(['bookings'=> function ($query) {
					                                $query->where('status','drop_off');
					                              }])
											 	 ->where('status','completed')
												 ->select('id')
												 ->get();

	    $driver_trips->map(function($trip,$key){
			$trip->driver_fare += $trip->bookings->sum('driver_fare');
			return $trip;
		});

		$total_earning 			= $driver_trips->sum('driver_fare');
		$total_withdraws  		= $user->driver->payouts()->sum('payout_amount');
		$penalty				= $user->driver->driver_penalties->sum('penalty_amount');
		$account_balance		= $driver_trips->sum('driver_fare') - $penalty - $total_withdraws;

		$total_completed_rides 	= $user->driver->trips()->where('status','completed')->count();

		return response()->json(['status' => true,'message' => 'Transaction history retrived successfully.','data' => $driver_penalties->all(),'account_balance'=>$account_balance,'total_completed_rides' => $total_completed_rides,'last_page' => $driver_penalties->lastPage()]);

	}

	/** withdraw  **/
	public function withdraw(Request $request){

		$user 				= Auth::user();
		$account_balance	= 0;
		$input 				= $request->all();

		$validator = Validator::make($input,[
			'amount' => "required",
		]);

		if ($validator->fails()) { 
			return response()->json(['status' => false,'message' => $validator->messages()->first()]);            
		}

		if($user->active_mode == 'driver'){

			$driver_trips 		=  $user->driver->trips()
												->whereHas('bookings', function ($query) {
						                                $query->whereHas('payment')->where('status','drop_off');
						                              })
												->with(['bookings'=> function ($query) {
						                                $query->where('status','drop_off');
						                              }])
												->where('status','completed')
												->select('id')
												->get();

			$driver_trips->map(function($trip,$key) use ($account_balance){
				$trip->driver_fare += $trip->bookings->sum('driver_fare');
				return $trip;
			});

			$total_withdraws  		= $user->driver->payouts()->sum('payout_amount');
			$penalty				= $user->driver->driver_penalties->sum('penalty_amount');
			$account_balance		= $driver_trips->sum('driver_fare') - $penalty - $total_withdraws;
			
			$withdraw_amnt	 		= $request->get('amount');
			if($account_balance <= 0){
				return response()->json(['status'=>false,'message'=>"Your wallet is empty",'account_balance'=>$account_balance]);
			}

			if($withdraw_amnt > $account_balance){
				return response()->json(['status'=>false,'message'=>"Balance not available in wallet.",'account_balance'=>$account_balance]);
			}

			try {

				/** Transfer to stripe connect **/
				$charge = \Stripe\Transfer::create([

					"amount" 		=> round($request->get('amount')*100),
					"currency" 		=> "CAD",
					"destination" 	=> $user->driver->stripe_id
				]);

				/** payout from stripe **/
				$payout=\Stripe\Payout::create([
					"amount" 	=> round($withdraw_amnt*100),
					"currency" 	=> "cad",
					"metadata"	=> array('charge_id'=>$charge->id)
				], [
					"stripe_account" => $user->driver->stripe_id
				]);

				if($charge && $payout){

					$data['payout_amount'] = round($withdraw_amnt,2);
					$data['driver_id'] = $user->driver->id;
					$data['payment_info'] = $charge;

					$data['payout_transaction_time'] = date('Y-m-d H:i:s', $payout->created);
					$data['payout_failure_code'] = ($payout->failure_code != "")?$payout->failure_code:"";
					$data['payout_failure_message'] = ($payout->failure_message != "")?$payout->failure_message:"";
					$data['payout_status'] = $payout->status;
					
					Payout::create($data);

					/** notification **/
					$user->notify(new WithdrawNotification(number_format($withdraw_amnt,2)));

					NotificationHelper::sendPushNotification($user,'WithdrawSuccess');

					$remainig_balance = $account_balance - $withdraw_amnt;
					return response()->json(['status'=>true,'message'=>'Withdrow amount successfully.','account_balance'=>round($remainig_balance,2)]);
				}

			} catch (\Stripe\Error\InvalidRequest $e) {
				$body = $e->getJsonBody();
				$err = $body['error'];
				if (isset($err['code']) && $err['code'] == "resource_already_exists") {
					return ['status'=>false,'message'=>$err['message'].' in our records']; 
				}
				return response()->json(['status'=>false,'message'=>$e->getMessage(),'account_balance'=>0]);
			} catch (Stripe\Error\Card $e) {
				return response()->json(['status'=>false,'message'=>$e->getMessage(),'account_balance'=>0]);
			} catch (Stripe\Error\Authentication $e) {
				return response()->json(['status'=>false,'message'=>$e->getMessage(),'account_balance'=>0]);
			} catch (Stripe\Error\ApiConnection $e) {
				return response()->json(['status'=>false,'message'=>$e->getMessage(),'account_balance'=>0]);
			} catch (Stripe\Error\Base $e) {
				return response()->json(['status'=>false,'message'=>$e->getMessage(),'account_balance'=>0]);
			} catch(Exception $e){
				return response()->json(['status'=>false,'message'=>$e->getMessage(),'account_balance'=>0]);
			}
		}
		return response()->json(['status'=>false,'message'=>'You have not access.']);

	}

	public function workerPayoutHistory(Request $request)
	{
		$withdraws = Auth::user()->snow_angel->withdraws()->get();
		return response()->json(['status'=>true,'message'=>'Withdraws listing.','data'=>$withdraws]);
	}

}
