<?php

namespace App\Http\Controllers\Api\v3;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Validator;
use Image;
use File;
use Storage;
use DB;
use Illuminate\Support\Facades\Hash;
use App\Models\v3\User;

class ProfileController extends Controller
{
    /** update profile **/
	public function updateProfilePic(Request $request)
	{	

		$validator = Validator::make($request->all(),[
			'profile_pic' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
		]);

		if ($validator->fails())
		{
			$message = $validator->messages()->first();
			return response()->json(['status'=>false,'message'=>$message]);
		}

		$image 			= $request->file('profile_pic');
		$user 			= Auth::user();
		$user_id 		= $user->id;
		$fileName 		= $image->getClientOriginalName();
		$fileExtension 	= $image->getClientOriginalExtension();

		$input['imagename'] = $user_id."_".uniqid(true).".".$fileExtension;
		$destinationPath 	= storage_path('app/public/images/users/'.$user_id.'/profile_pic');

		if(!File::exists(storage_path('app/public/images/users/'.$user_id))){
			$directoryPath 	= storage_path('app/public/images/users/'.$user_id);
			File::makeDirectory($directoryPath,0755,true);
		}

		if(!File::exists(storage_path('app/public/images/users/'.$user_id.'/profile_pic'))){
			$profilePath   	= storage_path('app/public/images/users/'.$user_id.'/profile_pic');
			File::makeDirectory($profilePath,0755,true);
		}

		$img 				= Image::make($image);

		$img->save(storage_path('app/public/images/users/'.$user_id.'/profile_pic/'.$input['imagename']));

		$user->profile_pic 	= $input['imagename'];
		$user->save();
		Auth::setUser($user);

		$response['profile_pic'] = $user->profile_pic;
		return response()->json(['status'=>true,'message'=>'Profile Picture Uploaded Successfully.','data'=>$response]);
		
	} 

	/** delete profile pic **/
	public function deleteProfilePic(Request $request)
	{
		
		$id 			= Auth::user()->id;
		$profile_pic 	= Auth::user()->getOriginal('profile_pic');
		$image_path 	= 'images/users/'.$id.'/profile_pic/'.$profile_pic;

		if(Storage::disk('public')->exists($image_path) && $profile_pic != null) {
			Storage::disk('public')->delete($image_path);
		}
		else{
			return response()->json(['status'=>false,'message'=>'Something went Wrong.']);
		}

		DB::table('users')->where(['id'=>(int)$id])->update(['profile_pic'=>'']);

		return response()->json(['status'=>true,'message'=>'Profile Picture Deleted successfully.']);

	}   

	/** update profile **/
	// public function updateProfile(Request $request){
	// 	$input  = $request->all();
	// 	$user   = Auth::user();

	// 	$validator = Validator::make($input,[
	// 		'first_name' 	=> "required|string|max:50",
	// 		'last_name' 	=> "required|string|max:50",
	// 		'email' 		=> "required|string|email|max:255|unique:users,email,". $user->id,
	// 		'dob' 			=> "required | date",
	// 		'gender' 		=> "required",
	// 		'mobile_no'		=> "required|unique:users,mobile_no,". $user->id,
	// 	]);

	// 	if ($validator->fails()) {
	// 		return response()->json(['status'=>false,'message' => $validator->messages()->first()]);
	// 	}

	// 	User::where('id',$user->id)->update($input);
	// 	return response()->json(['status'=>true,'message' => 'Profile updated successfully.','data'=>$input]); 

	// }

	/** change password **/
	public function changePassword(Request $request){ 

		$input = $request->all();
		$validator = Validator::make($input,[
			'old_password' => "required",
			'new_password' => "required",
		]);

		if ($validator->fails()) { 
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
		}		

		$user = Auth::user();

		/** check new password and old password **/
		if (Hash::check($request->get('new_password'), $user->password)){
			return response()->json(['status'=>false,'message'=>'Old password and new password must be different.']);
		}

		if (Hash::check($request->get('old_password'), $user->password))
		{
			$user->password = bcrypt($request->get('new_password'));
			$user->save();
			return response()->json(['status'=>true,'message'=>'Password change successfully.']);
		}

		return response()->json(['status'=>false,'message'=>'Old Password Does not match!']);
	}


	/** update mobile number **/
	public function updateMobile(Request $request){ 

		$user 		= Auth::user();
		$validator 	= Validator::make($request->all(),[
			'mobile_no'    => "required|numeric|unique:users,mobile_no,".$user->id,
			'country_code' =>"required|exists:countries,code",
		]);

		if ($validator->fails()) { 
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
		}		

		$user->mobile_no 	= $request->get('mobile_no');
		$user->country_code = $request->get('country_code');
		$user->save();

		return response()->json(['status'=>true,'message'=>'Mobile Number updated successfully.']);
	}

}
