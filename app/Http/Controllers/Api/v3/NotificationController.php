<?php

namespace App\Http\Controllers\Api\v3;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\v3\User;
use App\Models\v3\Trip;
use Auth;
use App\Models\v3\Booking;
use Stripe\Stripe;
use Carbon\Carbon;

class NotificationController extends Controller
{
	public function getUserNotifications(Request $request){

		$page 					= $request->get('page');
		$notifications 			= Auth::user()
		->notifications()
		->whereIn('type',[
			'App\Notifications\BookingNotification',
			'App\Notifications\StartTripNotification',
			'App\Notifications\CompleteTripNotification',
			'App\Notifications\CancelTripNotification',
			'App\Notifications\UserMarkedAsNoShowNotification',
			'App\Notifications\BookingConfirmationNotification',
			'App\Notifications\BookingDeclinedNotification',
		])
		->paginate(8);

		$trip_ids=[];
		foreach ($notifications as $index => $notification) {
			if (count($notification->data) > 0) {
				$trip_ids[] 	= $notification->data['trip_id'];
			}
		}

		$trip_ids 				= array_values(array_unique($trip_ids));

		$trips  				= Trip::whereIn('id',$trip_ids)
		->with(['from_location'=> function ($query) {
			$query->select('id','name','latitude','longitude','work_city_id')
			->with('work_city:id,name,timezone_name,timezone');
		},'to_location'=> function ($query) {
			$query->select('id','name','latitude','longitude','work_city_id')
			->with('work_city:id,name,timezone_name,timezone');
		},'driver' => function($query){
			$query->select('id','user_id');
			$query->with(['user' => function($query){
				$query->select('id','first_name','last_name','profile_pic');
			}]);
		},'bookings'=>function($query){
			$query->with(['user' => function($query){
				$query->select('id','first_name','last_name','profile_pic');
			}]);
		},
	])
		->get();

		$preparedArrayForTrip	= [];

		foreach ($trips as $key => $trip) {
			$preparedArrayForTrip[$trip->id] = $trip;
		}

		$responseNotification 	= [];

		$notifications->getCollection()->transform(function($notification,$index) use ($preparedArrayForTrip,$trip_ids,$notifications){
			$notification->markAsRead();
			$data 	= $notification->data;

			if (isset($notification->data['trip_id']) && in_array($notification->data['trip_id'], $trip_ids) && $notification->data['booking_id'] != null) {

				$trip_id 						= $notification->data['trip_id'];

				$drivername     				= $preparedArrayForTrip[$trip_id]->driver->user->first_name.' '.$preparedArrayForTrip[$trip_id]->driver->user->last_name;
				$booking 						= Booking::where('id',(int)$data['booking_id'])->first();

				$notification                 	= $notification->toArray();
				$notification['trip_id']      	= $trip_id;
				$notification['status']       	= $booking->status;
				$notification['drivername']   	= $drivername;
				$notification['city']      	  	= $preparedArrayForTrip[$trip_id]->from_location->work_city->name.' to '.$preparedArrayForTrip[$trip_id]->to_location->work_city->name;
				$notification['trip_date']    	= Carbon::parse($preparedArrayForTrip[$trip_id]->trip_date)->setTimeZone($preparedArrayForTrip[$trip_id]->from_location->work_city->timezone)->format('d M, Y');


				if($notification['type'] == 'App\Notifications\CancelTripNotification' ){

					$message						  = 'Your trip from '.$notification['city'].' on '.$notification['trip_date'].' is cancelled due to '. $preparedArrayForTrip[$trip_id]->cancelled_reason;

					if($booking->status == 'confirmed'){
						$amount				= $booking->chargeable_amount;
						$refunded_amount	= $booking->payment->refunded_amount;
						$charge     = \Stripe\Charge::retrieve(
							$booking->payment->charge_id,
							['api_key' => config('services.stripe.secret')]
						);
						$card_used          = $charge->source->last4;
						$message .= ' A refund of $'.number_format($amount,2).' will be issued to your card ending in '.$card_used.' within 3-5 bussiness days.';
					}
					$notification['message'] = $message;

				}

				if($notification['type'] == 'App\Notifications\CancelBookingNotification'){

					$decline_reason		= $booking->decline_reason;
					$message			= 'Your trip from '.$notification['city'].' on '.$notification['trip_date'].' is declined due to '. $decline_reason;

					if($booking->payment != ''){
						$refunded_amount	= $booking->payment->refunded_amount;
						$charge     = \Stripe\Charge::retrieve(
							$booking->payment->charge_id,
							['api_key' => config('services.stripe.secret')]
						);
						$card_used          = $charge->source->last4;
						$message .= ' A refund of $'.number_format($refunded_amount,2).' will be issued to your card ending in '.$card_used.' within 3-5 bussiness days.';
					}
					$notification['message'] = $message;

				}


			}else{
				$notifications->forget($index);
			}
			
			switch($notification['type']) {

				case 'App\Notifications\BookingNotification':
				$notification['title']            = 'Trip Booking';
				$notification['message']          = 'Your trip booked successfully.';
				$notification['tag']              = 'UserTripBooking';
				break;

				case 'App\Notifications\StartTripNotification':
				$notification['title']            = 'Trip Started';
				$notification['message']          = 'Your trip from '.$notification['city'].' is started';
				$notification['tag']              = 'UserTripStart';
				break;

				case 'App\Notifications\CompleteTripNotification':
				$notification['title']            = 'Thank you for riding with us';
				$notification['message']          = 'Your trip from '.$notification['city'].' is now completed. Please rate your experience.';
				$notification['tag']      		  = 'UserTripCompleted';
				break;

				case 'App\Notifications\UserMarkedAsNoShowNotification':
				$notification['title']            = 'Marked as “No Show”';
				$notification['message']          = 'Your driver has marked you as NO SHOW for your trip booking from '.$notification['city'];
				$notification['tag']      		  = 'UserMarkedAsNoShow';
				break;

				case 'App\Notifications\CancelTripNotification':
				$notification['title']            = 'Trip Cancelled';
				$notification['message']          = 'Your driver has cancelled a trip from Edmonton to Calgary. Please check your booking details page to view refund status.';
				$notification['tag']      		  = 'UserTripCancel';
				break;

				case 'App\Notifications\CancelBookingNotification':
				$notification['title']            = 'Booking Cancelled';
				$notification['message']          = 'Your booking for trip from '.$notification['city'].' cancelled successfully.';
				$notification['tag']      		  = 'UserBookingCancel';
				break;

				case 'App\Notifications\BookingConfirmationNotification':
				$notification['title']            = 'Booking Confirmation';
				$notification['message']          = '#'.$notification['data']['booking_id'].' Your booking for '.$notification['city'].' trip is confirmed.';
				$notification['tag']      		  = 'UserBookingConfirmation';
				break;

				case 'App\Notifications\BookingDeclinedNotification':
				$notification['title']            = 'Booking Declined';
				$notification['message']          = 'Your booking for '.$notification['city'].' trip is declined by a driver.';
				$notification['tag']      		  = 'UserTripBookingDeclined';
				break;

			}

			return $notification;

		});

return response()->json(['status'=>true,'message'=>'Notification retrieved.','data'=>$notifications]);
}

public function getDriverNotifications(Request $request){

	$page = $request->get('page');

	$notifications 	= Auth::user()
	->notifications()
	->whereIn('type',[
		'App\Notifications\BookingNotification',
		// 'App\Notifications\StartTripNotification',
		'App\Notifications\CompleteTripNotification',
		'App\Notifications\CancelTripNotification',
		'App\Notifications\WithdrawNotification',
	])
	->paginate(8,['*'],'page',$page);

	$trip_ids	= [];
	foreach ($notifications as $index => $notification) {

		if (count($notification->data) > 0 && $notification->type != 'App\Notifications\WithdrawNotification') {
			$trip_ids[] = $notification->data['trip_id'];
		}

	}

	$trip_ids 	= array_values(array_unique($trip_ids));

	$trips  	= Trip::whereIn('id',$trip_ids)
	->with(['from_location'=> function ($query) {
		$query->select('id','name','latitude','longitude','work_city_id')
		->with('work_city:id,name,timezone_name,timezone');
	},'to_location'=> function ($query) {
		$query->select('id','name','latitude','longitude','work_city_id')
		->with('work_city:id,name,timezone_name,timezone');
	},'driver' => function($query){
		$query->select('id','user_id');
		$query->with(['user' => function($query){
			$query->select('id','first_name','last_name');
		}]);
	},'bookings'=>function($query){
		$query->with(['user' => function($query){
			$query->select('id','first_name','last_name');
		}]);
	},'driver_penalties'])
	->get();

	$preparedArrayForTrip	= [];

	foreach ($trips as $key => $trip) {
		$preparedArrayForTrip[$trip->id] = $trip;
	}

	$responseNotification 	= [];

	$notifications->getCollection()->transform(function($notification,$index) use ($preparedArrayForTrip,$trip_ids){
		$notification->markAsRead();
		$data=$notification->data;
		$type=$notification->type;
		$notification  = $notification->toArray();
		$username='';
		$seats=0;
		if (isset($notification['data']['amount']) && $type == 'App\Notifications\WithdrawNotification'){
			$notification['amount']      	 = $notification['data']['amount'];
		}
		if (isset($notification['data']['trip_id']) && in_array($notification['data']['trip_id'], $trip_ids) && $type != 'App\Notifications\WithdrawNotification' ) {

			$trip_id = $notification['data']['trip_id'];

			$notification['trip_id']      	  = $trip_id;
			$notification['status']      	  = Trip::find($trip_id)->status;

			$booking 						  = Booking::where('id',(int)$data['booking_id'])->with('user')->first();
			if($booking != ''){
				$seats   						  = $booking->seats;	
				$username						  = $booking->user['first_name'].' '.$booking->user['last_name'];
			}
			$notification['seats']      	  = $seats;
			$notification['username']      	  = $username;
			if($preparedArrayForTrip[$trip_id]->driver_penalties != ''){
				$notification['cancelled_reason'] = $preparedArrayForTrip[$trip_id]->cancelled_reason;
				$notification['penalty_amount'] = $preparedArrayForTrip[$trip_id]->driver_penalties['penalty_amount'];
			}
				// if($notification['type'] == "App\Notifications\CancelTripNotification" && $preparedArrayForTrip[$trip_id]->driver_penalties == ''){
				// 	$notification['cancelled_reason'] = 'Trip cancelled.';
				// 	$notification['penalty_amount'] = 0;
				// }
			unset($notification['data']);

			$notification['city']      		  = $preparedArrayForTrip[$trip_id]->from_location->work_city->name.' to '.$preparedArrayForTrip[$trip_id]->to_location->work_city->name;;
			$notification['trip_date']        = Carbon::parse($preparedArrayForTrip[$trip_id]->trip_date)->setTimeZone($preparedArrayForTrip[$trip_id]->from_location->work_city->timezone)->format('d M, Y H:i a');

		}

		switch($notification['type']) {

			case 'App\Notifications\BookingNotification':
			$notification['title']            = 'New booking received for your trip';
			$notification['message']          = 'Please confirm the booking before it gets expired.';
			$notification['tag']              = 'DriverTripBooking';
			break;

			// case 'App\Notifications\StartTripNotification':
			// $notification['message']          = 'Your trip started for '.$notification['city'];
			// $notification['tag']              = 'DriverTripStart';
			// break;

			case 'App\Notifications\CompleteTripNotification':
			$notification['title']            = 'Earnings deposited';
			$notification['message']          = 'Your earnings for Trip '.$notification['city'].'is now deposited in your Ridechimp wallet. You can Withdraw It anytime.';
			$notification['tag']      		  = 'DriverTripCompleted';
			break;

			case 'App\Notifications\CancelBookingNotification':
			$notification['title']            = 'Booking cancelled';
			$notification['message']          = $notification['username'].' has cancelled the booking for a trip to '.$notification['city'];
			$notification['tag']      		  = 'DriverBookingCancel';
			break;

			case 'App\Notifications\CancelTripNotification':
			$notification['title']            = 'Trip cancelled';
			$notification['message']          = 'You have cancel trip for trip from '.$notification['city'].' due to '.$notification['cancelled_reason'].'Trip cancellation charge $'.number_format($notification['penalty_amount'],2).' deducted from your wallet.';
			$notification['tag']      		  = 'DriverTripCancel';
			unset($notification['penalty_amount']);
			unset($notification['cancelled_reason']);
			break;

			case 'App\Notifications\WithdrawNotification':
			$notification['title']            = 'Money is deposited in your bank account';
			$notification['message']          = 'Your withdrawal money is now deposited in your bank account.';
			$notification['tag']      		  = 'WithdrawSuccess';
			unset($notification['amount']);
			break;

		}
		unset($data['booking_id']);
		return $notification;

	});

	return response()->json(['status'=>true,'message'=>'Notification retrieved.','data'=>$notifications]);
}
}
