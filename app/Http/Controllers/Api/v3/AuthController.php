<?php

namespace App\Http\Controllers\Api\v3;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\v3\Maintenance;
use App\Models\v3\User;
use App\Models\v3\Payment;
use App\Models\v3\Booking;
use Stripe\Stripe;
use Stripe\Customer;
use Validator;
use App\Models\v3\TripCancellationReason;
use Notification;
use App\Notifications\VarificationCode;
use App\Helpers\NotificationHelper;
use App\Work_state;
use Carbon\Carbon;
use JWTAuth;

class AuthController extends Controller
{

	/** login api **/
	public function login(Request $request){ 
		
		/** App under maintanence **/
		$maintenance 	= Maintenance::select('android_version','ios_version','maintanance_mode')->first();
		if($maintenance->maintanance_mode){
			return response()->json([
				'status' 			=> false,
				'message'			=> 'Application is under Maintainance.',
				'maintanance_mode' 	=> true
			]);            
		}

		$validator = Validator::make($request->all(),[

			'mobile_no' =>  "exists:users,mobile_no|required",
			'password'  =>  "required"

		]);



		if ($validator->fails()) { 

			return response()->json(['status'=>false,'message'=>$validator->messages()->first(),'maintenance_mode'  => false]);            

		}

		if($token = auth('api')->attempt($request->only('mobile_no','password'))){ 

			$user 	=  Auth::guard('api')->user(); 
			// if ($request->filled('active_mode')) {

			// 	if($user->active_mode == 'user' || ($user->is_driver == '1' && $user->active_mode='driver')){
			// 		$user->active_mode	= $request->get('active_mode');
			// 	}
			// }
			if (request('active_mode')) {
				$user->active_mode	= request('active_mode');
			}
			$success['token'] 				=  $token;//$user->createToken('ridechimp')->accessToken; 
			// $success['base_url'] 			=  asset('storage/images/users/'.$user->id.'/profile_pic').'/';
			$success['android_version'] 	=  $maintenance->android_version;
			$success['ios_version'] 		=  $maintenance->ios_version;
			/** save firebase ids **/
			if(request('firebase_android_id')){
				$user->firebase_android_id  = request('firebase_android_id');
			}
			if(request('firebase_ios_id')){
				$user->firebase_ios_id 		= request('firebase_ios_id');
			}
			if(request('driver_firebase_android_id')){
				$user->driver_firebase_android_id  = request('driver_firebase_android_id');
			}
			if(request('driver_firebase_ios_id')){
				$user->driver_firebase_ios_id 		= request('driver_firebase_ios_id');
			}

			$user->device_type    = $request->get('device_type');

			if(request('device_id') != $user->device_id){ 
				NotificationHelper::sendPushNotification($user,'NewLoginFromOtherDevice',[]);
			}
			$user->device_id = request('device_id');

			$user->save();

			$success['user_detail'] 		=  $this->acountData(); 
			$success['rider_trip_cancellation_reasons']  	= TripCancellationReason::where(['status'=>true,'user_type'=>'user'])->pluck('content'); 
			$success['driver_trip_cancellation_reasons']    = TripCancellationReason::where(['status'=>true,'user_type'=>'driver','reason_for'=>'cancel_trip'])->pluck('content'); 
			$success['driver_booking_decline_reasons']      = TripCancellationReason::where(['status'=>true,'user_type'=>'driver','reason_for'=>'decline_booking'])->pluck('content'); 

			return response()->json(['status' => true,'message' => 'Login Successfully','data' => $success]); 
		} 
		else{ 
			return response()->json(['status' => false,'message' => 'Invalid credential.']); 
		} 

	}

	/** Register api **/
	public function register(Request $request) 
	{ 

		/** App under maintanence **/
		$maintanace 	= Maintenance::select('android_version','ios_version','maintanance_mode')->first();
		if($maintanace->maintanance_mode){
			return response()->json([
				'status'			=> false,
				'message'			=> 'Application is under Maintainance.',
				'maintanance_mode' 	=> true
			]);            
		}

		$input = $request->all();
		Stripe::setApiKey(config('services.stripe.secret'));
		/** validation **/
		$validator = Validator::make($input,[
			'first_name' 	=> "required|string|max:50",
			'last_name' 	=> "required|string|max:50",
			'email' 		=> "required|string|email|max:255|unique:users",
			'password' 		=> "required|string|min:6",
			'dob' 			=> "required|date|before : -18 years",
			'gender' 		=> "required",
			'country_code'	=> "required|exists:countries,code",
			'mobile_no'		=> "required|unique:users"
		],[
			"dob.before" 	=> "You are not 18 year Old!"
		]);

		if ($validator->fails()) { 
			return response()->json(['status' => false,'message' => $validator->messages()->first()]);            
		}		

		$input['password'] 	= bcrypt($input['password']); 
		$customer 			=  Customer::create(array(
			'email' => $input['email'],
		));
		$input['stripe_id'] 		= $customer->id;
		if ($request->filled('active_mode')) {
			$input['active_mode']	= $input->get('active_mode');
		}
		$input['profile_verified'] 	='false';

		unset($input['referral_code']);
		$user = User::create($input); 

		/** create driver stripe connect account **/
		$acct = \Stripe\Account::create(array(
			"country" 	=> "CA",
			"type" 		=> "custom",
			"email"		=> $user->email
		));

		$user->driver()->create(array('user_id' => $user->id,'stripe_id' => $acct->id));
		// Auth::attempt(['mobile_no' => request('mobile_no'), 'password' => request('password')]); 
		if($token = auth('api')->attempt(['mobile_no' => request('mobile_no'), 'password' => request('password')])){
			$success['token'] 			=  $token;//$user->createToken('ridechimp')->accessToken; 
			$success['android_version'] =  $maintanace->android_version;
			$success['ios_version'] 	=  $maintanace->ios_version;
			$success['user_detail'] 	=  $this->acountData(); 
			$success['rider_trip_cancellation_reasons']  	= TripCancellationReason::where(['status'=>true,'user_type'=>'user'])->pluck('content'); 
			$success['driver_trip_cancellation_reasons']    = TripCancellationReason::where(['status'=>true,'user_type'=>'driver','reason_for'=>'cancel_trip'])->pluck('content'); 
			$success['driver_booking_decline_reasons']      = TripCancellationReason::where(['status'=>true,'user_type'=>'driver','reason_for'=>'decline_booking'])->pluck('content'); 

			return response()->json(['status' => true,'message' => 'Register Successfully','data' => $success]); 

		}
		
		return response()->json(['status' => false,'message' => 'Something went wrong!']); 

	}

	/** logout **/
	public function logout(Request $request)
	{
		/** clear user firbase ids **/
		$user = Auth::user();

		if ($request->get('deviceType') == 'android') {
			$user->firebase_android_id = '';
		} else {
			$user->firebase_ios_id = '';
		}

		$user->save();

		auth('api')->logout();

		return response()->json(['status'=>true,'message' => 'Successfully logged out']);

		// $user 						= Auth::user(); 
		// $user->firebase_android_id 	= '';
		// $user->firebase_ios_id 		= '';
		// $user->driver_firebase_android_id 	= '';
		// $user->driver_firebase_ios_id 		= '';

		// $user->save();

		// $request->user()->token()->revoke();
		// return response()->json(['status'=>true,
		// 	'message' => 'Successfully logged out'
		// ]);
	}

	/** acountData api **/
	public function acountData() 
	{ 

		$user 			= Auth::guard('api')->user();
		$userdata 		= User::with('driver')->where('id',$user->id)->first();
		if ($userdata->address && is_array($userdata->address)) {
			$userdata->address = implode($userdata->address,', '); 
		}
		$carDetail 		= [];

		/** Driver assigned vehicles **/
		if(count($user->driver->car_driver) > 0){

			foreach ($user->driver->car_driver as $key => $cars) {

				$car_logo 	 = $cars->car_model->car_type['name'];
				$carDetail[] =array(
					'vehicle_id'=>  $cars->id,
					'company'	=>	$cars->car_model->car_type['name'],
					'type'		=>	$cars->car_model->car_type['name'],
					'model'		=>	$cars->car_model['model_name'],
					'capacity'	=>	$cars->car_model['capacity'],
					'logo'		=>	$cars->car_model->car_type['logo'],
				);

			}

		}
		$userdata['cars'] 		= $carDetail;

		/** Driver account balance **/
		$driver_trips 		= $user->driver->trips()
		->whereHas('bookings', function ($query) {
			$query->whereHas('payment')->where('status','drop_off');
		})
		->with(['bookings'=> function ($query) {
			$query->where('status','drop_off');
		}])
		->where('status','!=','cancelled')
		->where('status','completed')
		->select('id')
		->get();

		$driver_trips->map(function($trip,$key){
			$trip->driver_fare += $trip->bookings->sum('driver_fare');
			return $trip;
		});
		
		$total_earning 			= $driver_trips->sum('driver_fare');
		$total_withdraws  		= $user->driver->payouts()->sum('payout_amount');
		$penalty				= $user->driver->driver_penalties->sum('penalty_amount');
		$account_balance		= $driver_trips->sum('driver_fare') - $penalty - $total_withdraws;

		$userdata->driver_account_balance 	= round($account_balance,2);

		/** has payment information driver **/
		Stripe::setApiKey(config('services.stripe.secret'));
		$userdata->has_driver_payment_info = false;

		if($userdata->driver['stripe_id'] != ''){

			$account 	= \Stripe\Account::retrieve($user->driver['stripe_id']);
			$status 	= false;
			if($account->legal_entity != ''){
				$status = ($account->legal_entity->verification->status == 'verified') ? true : false;
			}

			$userdata->has_driver_payment_info 	= $status;
		}

		/** has payment information rider **/

		$userdata->has_user_payment_info		= false;
		if($userdata->stripe_id != ''){
			$customerCards = Customer::retrieve($user->stripe_id);
			$userdata->has_user_payment_info = (count($customerCards->sources['data']) > 0) ? true : false;			
		}

		unset($user->driver);
		unset($user->otp);
		if(count($user->languages_known) == 0){
			$user->languages_known				 = [];
		}

		$userdata->user_ratings   = $userdata->ratings()->avg('rating');
		$userdata->driver_ratings = $userdata->driver->ratings()->avg('rating');

		/** remove null from response **/
		return array_map(function ($value) {
			return $value === null ? '' : $value;
		} , $userdata->toArray()); 

	} 

	/** switch account api **/
	public function switchAccount(Request $request) 
	{ 
		$input 		= $request->all();
		$validator 	= Validator::make($input,[
			'active_mode' => "required",
		]);

		if ($validator->fails()) { 
			return response()->json(['status' => false,'message' => $validator->messages()->first()]);            
		}		
		$user 				= Auth::user();
		$user->active_mode	= $request->get('active_mode');

		if($user->active_mode == 'user'){
			$user->save();			
		}elseif($user->is_driver == '1' && $user->active_mode='driver'){
			$user->save();			
		}
		unset($user->otp);
		if ($user->address && is_array($user->address)) {
			$user->address = implode($user->address,', '); 
		}
		
		return response()->json(['status' => true,'message' => 'Account switched successfully.','data' => $user]); 
	} 

	/** refresh token **/
	public function refreshToken(Request $request)
	{

		config([
			'jwt.blacklist_enabled' => false
		]);
		$refreshed = JWTAuth::parseToken()->refresh(JWTAuth::getToken());
		$data['status']  = true;
		$data['message'] = 'Token has been generated successfully.';
		$data['token'] = $refreshed;
		return response()->json($data);

		// $validator = Validator::make($request->all(),[
		// 	'mobile_no' 			=> "required|numeric",
		// 	'password'  			=> "required",
		// 	'firebase_android_id' 	=> "nullable",
		// 	'firebase_ios_id'     	=> "nullable",
		// ]);

		// if ($validator->fails()) { 
		// 	return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
		// }		

		// $mobileNumber = $request->get('mobile_no');
		// $password 	  = $request->get('password');
		
		// if(Auth::attempt(['mobile_no' => $mobileNumber, 'password' => $password])){ 

		// 	$user  = Auth::user(); 
		// 	$token =  $user->createToken('ridechimp')->accessToken; 

		// 	/** save firebase ids **/
		// 	if($request->has('firebase_android_id')){
		// 		$user->firebase_android_id = $request->get('firebase_android_id');
		// 	}
		// 	if($request->has('firebase_ios_id')){
		// 		$user->firebase_ios_id = $request->get('firebase_ios_id');
		// 	}
		// 	if($request->has('driver_firebase_android_id')){
		// 		$user->driver_firebase_android_id = $request->get('driver_firebase_android_id');
		// 	}
		// 	if($request->has('driver_firebase_ios_id')){
		// 		$user->driver_firebase_ios_id = $request->get('driver_firebase_ios_id');
		// 	}

		// 	$user->save();

		// 	return response()->json([
		// 		'status'  => true,
		// 		'message' => 'Token has been generated successfully.',
		// 		'token'   => $token
		// 	]); 
		// }
		
		// return response()->json([
		// 	'status'  => false,
		// 	'message' => 'Sorry, Wrong credentials provided.',
		// 	'token'   => ''
		// ]); 
	}

	/** send email verification code **/
	public function sendVarificationCode(Request $request)
	{   

		$user 	   = Auth::user();
		$validator = Validator::make($request->all(),[
			'email' => "required|string|email|max:255|unique:users,email,".$user->id
		]);

		if ($validator->fails()) {
			$message = array_values(current($validator->messages()))[0][0];
			return response()->json(['status'=>false,'message'=>$message]);
		}
		
		if($user != ''){
			$vcode    				= rand(100000,999999);
			$user->email 			= $request->get('email');
			$user->profile_verified = 'false';
			$user->otp 				= $vcode;
			$user->save();

			$user->notify(new VarificationCode($vcode));
			return response()->json(['status'=>true,'message'=>"Verification code has been sent to your email."]);

		}
		return response()->json(['status'=>false,'message'=>"Email doesn't exist."]);

	}

	/** email verification **/
	public function emailVarification(Request $request)
	{

		$validator = Validator::make($request->all(),[
			'email' 			=> "required|string|email",//|unique:users,email,".$user->id,
			'verification_code' => "required"
		]);

		if ($validator->fails()) {
			$message = array_values(current($validator->messages()))[0][0];
			return response()->json(['status'=>false,'message'=>$message]);
		}

		$email = $request->get('email');
		$vcode = $request->get('verification_code');

		$checkEmail = User::select("otp")
		->where(['email'=>$email,'otp'=>$vcode])
		->first();

		if ($checkEmail == "") {
			return response()->json(['status'=>false,'message'=>"Please provide the valid OTP."]);
		}

		User::where(['email'=>$email])
		->update(array('profile_verified'=>'true','otp'=>'','email_verified_at'=>Carbon::now()->toDateTimeString()));

		return response()->json(['status'=>true,'message'=>"Congrats! Thank you for varification."]);
	}

	/** is mobile exist **/
	public function isMobileExists(Request $request)
	{

		$validator = Validator::make($request->all(),[
			'mobile_no' => "required|unique:users,mobile_no",
		]);

		if ($validator->fails()) { 
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
		}

		return response()->json(['status'=>true,'message'=>'You can use this phone number']);            
	}

	/** is email exist **/
	public function isEmailExists(Request $request)
	{

		$validator = Validator::make($request->all(),[
			'email' => "required|unique:users,email",
		]);

		if ($validator->fails()) { 
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
		}

		return response()->json(['status'=>true,'message'=>'You can use this email']);            
	}

	/** country information **/
	public function countryInfo(){

		$country = Work_state::select('id','work_country_id','name')
		->with(['work_country'=>function($query){
			$query->select('id','name'); 
		},'work_cities'=>function($query){
			$query->select('id','work_state_id','name'); 
		}])
		->get()
		->map(function($query){
			$query->country =$query->work_country->name;
			$query->state =$query->name;
			unset($query->id);
			unset($query->work_country_id);
			unset($query->name);
			unset($query->work_country);
			return $query;  
		});
		
		return response()->json([
			'status'  => true,
			'message' => 'data retrieval',
			'data'    => $country
		]);
	}

}
