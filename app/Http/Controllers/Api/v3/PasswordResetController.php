<?php

namespace App\Http\Controllers\Api\v3;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\v3\User;
use Validator;
use Notification;
use App\Notifications\PasswordResetSuccess;

class PasswordResetController extends Controller
{
	/** is email registered **/
    public function isMobileRegisted(Request $request)
	{
		$validator = Validator::make($request->all(),[
			'mobile_no' => "required|unique:users,mobile_no",
		]);

		if ($validator->fails()) { 
			$user=User::where('mobile_no',$request->get('mobile_no'))->with('user_country')->first();
			if($user->user_country != ''){
				return response()->json(['status'=>true,'message'=>'You can use this phone number','data'=>$user->user_country->dialing_code]);            
			}
			return response()->json(['status'=>true,'message'=>'You can use this phone number','data'=>'+91']);            
		}
		return response()->json(['status'=>false,'message'=>'This number is not registered.']);            
	}

	/** reset password **/
	public function resetpassword(Request $request)
	{   

		$validator = Validator::make($request->all(),[
			'password'  => 'required',
			'mobile_no' => 'required',
		]);

		if ($validator->fails()) { 
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
		}

		$user = User::where('mobile_no', $request->get('mobile_no'))->first();

		if (!$user){
			return response()->json(['status'=>false,'message' => 'Invalid mobile no.']);
		}

		$user->password = bcrypt($request->password);
		$user->save();
		$user->notify(new PasswordResetSuccess());

		return response()->json(['status'=>true,'message' => 'Password Reset Successfully.']);
	}

}
