<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Validator;
use App\Models\v3\Trip;
use App\Models\v3\Driver;
use App\TripUser;
use App\Models\v3\Payment;
use App\Work_city;
use App\Work_country;
use App\Work_location;
use App\Admin_statistic;
use App\Transaction;
use Stripe\Stripe;
use Illuminate\Contracts\Support\Jsonable;
use Notification;
use App\Helpers\NotificationHelper;
use App\Notifications\StartTripNotification;
use App\Notifications\CompleteTripNotification;
use App\Notifications\BookingNotification;
use App\Notifications\CancelTripNotification;
use Carbon\Carbon;
use DB;
use App\Refund;
use App\Helpers\TripHelper;

class TripController extends Controller
{
	//driver create trip
	public function create_trip(Request $request) 
	{
		$validator = Validator::make($request->all(),[
			'from_city_id' => 'required|exists:work_cities,id',
			'to_city_id' => 'required|exists:work_cities,id',
			'trip_date' => 'required',
			'pickup_id' => 'required|exists:work_locations,id',
			'dropoff_id' => 'required|exists:work_locations,id',
			'pickup_time' => 'required',
			'total_seats' => 'required',
			'price' => 'required',
			'vehicle_id'=>'required'
		]);
		if ($validator->fails())
		{
			$message = $validator->messages()->first();
			return response()->json(['status'=>false,'message'=>$message]);
		}
		$user = Auth::user();
		$tripData = new Trip(); 
		$tripData['driver_id']=$user->driver->id;
		$tripData['from_city_id']=$request->get('from_city_id');
		$tripData['to_city_id']=$request->get('to_city_id');
		$tripData['vehicle_id']=$request->get('vehicle_id');
		$tripData['trip_date']=$request->get('trip_date');
		$tripData['pickup_id']=$request->get('pickup_id');
		$tripData['dropoff_id']=$request->get('dropoff_id');
		$tripData['pickup_time']=$request->get('pickup_time');
		$tripData['total_seats']=$request->get('total_seats');
		$tripData['available_seats']=$request->get('total_seats');
		$tripData['price']=$request->get('price');
		
		Auth::user()->driver->trips()->save($tripData);
		return response()->json(['status'=>true,'message'=>'Trip created successfully.']);
		
	}

	//user search trip
	public function search_trip(Request $request) 
	{
		$validator = Validator::make($request->all(),[
			'from' => 'required',
			'to' => 'required',
			// 'trip_date' => 'required',
			'total_person' => 'required',
		]);
		if ($validator->fails())
		{
			$message = $validator->messages()->first();
			return response()->json(['status'=>false,'message'=>$message]);
		}
		$user = Auth::user();
		$from=$request->get('from');
		$to=$request->get('to');
		$page = $request->page;

		$trip_date=date('Y-m-d',strtotime($request->get('trip_date')));
		$available_seats=$request->get('total_person');
		$trips=Trip::
		select('id as trip_id','from_city_id','to_city_id','pickup_id','dropoff_id','driver_id','trip_date','pickup_time','price','available_seats','vehicle_id')
		->with(['driver'=> function ($query) {
			$query->with(['user'=> function ($query) {
				$query->select('id','first_name','last_name','mobile_no','profile_pic','gender');
			}]);
		}])
		->with(['from_city'=> function ($query) use ($from) {
			$query->select('id','name')->where('name','like','%'.$from.'%');
		}])
		->with(['to_city'=> function ($query) use ($to) {
			$query->select('id','name')->where('name','like','%'.$to.'%');
		}])	
		->with(['pickup_location'=> function ($query) {
			$query->select('id','name','latitude','longitude','place_id');
		}])
		->with(['dropoff_location'=> function ($query) {
			$query->select('id','name','latitude','longitude','place_id');
		}])
		->with(['vehicle'=> function ($query) {
			$query->select('id','driver_id','car_model_id','color','year')
			->with(['car_model'=> function ($query) {
				$query->select('id','car_company_id','car_type_id','capacity')
				->with(['car_company'=> function ($query) {
					$query->select('id','name');
				}])
				->with(['car_type'=> function ($query) {
					$query->select('id','name');
				}]);
			}]);
		}])	
		->whereHas('from_city' ,function($query) use($from){
			$query->where('name','like','%'.$from.'%');              
		})
		->whereHas('to_city',function($query) use($to){
			$query->where('name','like','%'.$to.'%');              
		})
		// ->where('trip_date',">=",$trip_date)
		->where('trip_date',">=",Carbon::now()->format('Y-m-d'))
		->where('available_seats','>=',$available_seats)
		->where('status','upcoming')
		// ->where('driver_id','!=',$user->driver->id)
		->paginate(10);
		$trips->getCollection()->transform(function($trip,$key) {

			$trip->base_url = asset('storage/images/users/'.$trip->driver->user_id.'/profile_pic');
			return $trip;
		});

		
		// $trips = $trips->map(function($trip,$key){
		// 	$trip->base_url = asset('storage/images/users/'.$trip->driver->user_id.'/profile_pic');
		//	return $trip;
		// });

		return response()->json(['status'=>true,'message'=>'Data retrived.','trips'=>$trips]);
	}

	//user search trip
	public function trip_rules(Request $request) 
	{
		$validator = Validator::make($request->all(),[
			'trip_id' => 'required|exists:trips,id',
		]);
		if ($validator->fails())
		{
			$message = $validator->messages()->first();
			return response()->json(['status'=>false,'message'=>$message]);
		}
		$user = Auth::user();
		$trip=Trip::
		select('id','driver_id','vehicle_id')
		
		->with(['vehicle'=> function ($query) {
			$query->select('id','car_model_id','color','year','rules_and_regulations','allowed_things')
			->with(['driver'=> function ($query) {
				$query->with(['user'=> function ($query) {
					$query->select('id','first_name','last_name','mobile_no','profile_pic');
				}]);
			}])
			->with(['car_model'=> function ($query) {
				$query->select('id','car_company_id','car_type_id','capacity','model_name')
				->with(['car_company'=> function ($query) {
					$query->select('id','name');
				}])
				->with(['car_type'=> function ($query) {
					$query->select('id','name');
				}]);
			},'carPhotoes']);
		}])	
		->where('id',$request->get('trip_id'))
		->first();

		if(count($trip) > 0){
			$trip['completed_rides']=Trip::where('driver_id',$trip->driver_id)->where('status','completed')->count();
			$trip['profile_base_url'] = asset('storage/images/users/'.$trip->driver->user_id.'/profile_pic');
		}else{
			$trip['completed_rides']=0;
		}
		$trip['base_url']=asset('storage/driver_car/'.$trip->driver_id).'/';

		return response()->json(['status'=>true,'message'=>'Data retrived.','trip'=>$trip]);

	}

	//user search trip
	public function trip_info(Request $request) 
	{
		$validator = Validator::make($request->all(),[
			'trip_id' => 'required|exists:trips,id',
		]);
		if ($validator->fails())
		{
			$message = $validator->messages()->first();
			return response()->json(['status'=>false,'message'=>$message]);
		}
		$user = Auth::user();
		$trip=Trip::
		select('id','driver_id','from_city_id','to_city_id','pickup_id','dropoff_id','driver_id','trip_date','pickup_time','price','available_seats','vehicle_id','created_at')
		->with(['driver'=> function ($query) {
			$query->select('id','user_id')
			->with(['user'=> function ($query) {
				$query->select('id','first_name','last_name','mobile_no','profile_pic','address','languages_known','additional_info');
			}]);
		}])
		->with(['from_city'=> function ($query) {
			$query->select('id','name');
		}])
		->with(['to_city'=> function ($query) {
			$query->select('id','name');
		}])	
		->with(['pickup_location'=> function ($query) {
			$query->select('id','name','latitude','longitude','place_id');
		}])
		->with(['dropoff_location'=> function ($query) {
			$query->select('id','name','latitude','longitude','place_id');
		}])
		->with(['vehicle'=> function ($query) {
			$query->select('id','car_model_id','color','year','rules_and_regulations','allowed_things')
			->with(['car_model'=> function ($query) {
				$query->select('id','car_company_id','car_type_id','capacity','model_name')
				->with(['car_company'=> function ($query) {
					$query->select('id','name');
				}])
				->with(['car_type'=> function ($query) {
					$query->select('id','name','logo');
				}]);
			},'carPhotoes']);
		}])	
		->with(['payments'=>function($query){
			$query->select('id','user_id','trip_id','total','total_person','status','created_at')
			->with(['user'=> function ($query) {
				$query->select('id','first_name','last_name','mobile_no','profile_pic','address','languages_known','additional_info');
			},'transaction'=>function($query){
				$query->select('id','payment_id','charge_id');
			}])
			->where('status','!=','3');
		}])
		->where('id',$request->get('trip_id'))
		->first();
		$trip->completed_rides=0;
		$trip->driving_since='November, 2016';
		$trip->base_url=asset('storage/driver_car').'/'.$trip->driver_id.'/';
		$trip->profile_base_url = '';
		$trip->vehical_base_url=asset('storage/type_logo').'/'.$trip->vehicle->car_model->car_type['logo'];
		$trip->payments=$trip->payments->map(function($payment) use ($trip){
			if($payment->transaction != ''){
				$charge=\Stripe\Charge::retrieve(
					$payment->transaction->charge_id,
					['api_key' => config('services.stripe.secret')]
				);
				$payment->card_used=$charge->source->last4;
				unset($payment->transaction);

			}
			$trip->reserved_seats+=$payment->total_person;
			if($payment->total != '3'){
				$trip->total_earning+=$payment->total;
			}
		});
		// $trip->reserved_seats=$trip->payments->sum('total_person');
		// $trip->total_earning=$trip->payments->where('status','!=','3')->sum('total');
		if($trip != ''){
			$trip->completed_rides=Trip::where('driver_id',$trip->driver_id)->where('status','completed')->count();
			$trip->profile_base_url = asset('storage/images/users/'.$trip->driver->user_id.'/profile_pic');
		}

		return response()->json(['status'=>true,'message'=>'Data retrived.','trip'=>$trip]);

	}

	//user search trip
	public function booking_confirmation(Request $request) 
	{
		$validator = Validator::make($request->all(),[
			'booking_id' => 'required|exists:payments,id',
		]);
		if ($validator->fails())
		{
			$message = $validator->messages()->first();
			return response()->json(['status'=>false,'message'=>$message]);
		}
		$user = Auth::user();
		$trip=Payment::select('id','trip_id','total','total_person as seats','created_at')
		->where('id',$request->get('booking_id'))
		->where('user_id',$user->id)
		->with(['trip'=> function ($query) use ($user) {
			$query->select('id','driver_id','vehicle_id','trip_date','pickup_time','from_city_id','to_city_id','pickup_id','dropoff_id','price')
			//->where('status','active')
			->with(['from_city'=> function ($query) {
				$query->select('id','name');
			}])
			->with(['to_city'=> function ($query) {
				$query->select('id','name');
			}])
			->with(['pickup_location'=> function ($query) {
				$query->select('id','name','latitude','longitude','place_id');
			}])
			->with(['dropoff_location'=> function ($query) {
				$query->select('id','name','latitude','longitude','place_id');
			}])
			->with(['driver'=> function ($query) {
				$query->select('id','user_id')
				->with(['user'=> function ($query) {
					$query->select('id','first_name','last_name','mobile_no','profile_pic','address','languages_known','additional_info');
				}]);
			}])
			->with(['vehicle'=> function ($query) {
				$query->select('id','car_model_id','color','year','rules_and_regulations','allowed_things')
				->with(['car_model'=> function ($query) {
					$query->select('id','car_company_id','car_type_id','capacity','model_name')
					->with(['car_company'=> function ($query) {
						$query->select('id','name');
					}])
					->with(['car_type'=> function ($query) {
						$query->select('id','name');
					}]);
				},'carPhotoes']);
			}]);
		},'transaction'])
		->first();
		if($trip == ''){
			return response()->json(['status'=>false,'message'=>'Invelid booking id.']);
		}
		if($trip != ''){
			$trip->total=number_format($trip->total,2);
			$trip->trip->profile_base_url = asset('storage/images/users/'.$trip->trip->driver->user_id.'/profile_pic');
			$trip->trip->vehicle_base_url = asset('storage/driver_car/'.$trip->trip->driver_id).'/';
		}
		try{
			Stripe::setApiKey(config('services.stripe.secret'));

			if($trip->transaction != ''){
				$charge=\Stripe\Charge::retrieve(
					$trip->transaction->charge_id,
					['api_key' => config('services.stripe.secret')]
				);
				$trip->card_used=$charge->source->last4;
			}else{
				$retriveResult   = \Stripe\Customer::retrieve($user->stripe_id);
				$card 	  		 = $retriveResult->sources->retrieve($retriveResult->default_source);
				$trip->card_used = $card->last4;

			}
			unset($trip->transaction);
		}catch(\Stripe\Error\Card $e){
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		} catch (\Stripe\Error\NotFound $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		}
		catch (\Stripe\Error\Authentication $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		}
		catch (\Stripe\Error\InvalidRequest $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		} catch (\Stripe\Error\RateLimit $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		} catch (\Stripe\Error\ApiConnection $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		} catch (\Stripe\Error\Base $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		}

		return response()->json(['status'=>true,'message'=>'Data retrived.','trip'=>$trip]);

	}

	//user cancel trip
	public function user_cancel_trip(Request $request) 
	{

		$validator = Validator::make($request->all(),[
			'booking_id' => 'required|exists:payments,id',
		]);

		if ($validator->fails())
		{
			$message 	= $validator->messages()->first();
			return response()->json(['status'=>false,'message'=>$message]);
		}

		$booking=Payment::where('id',$request->get('booking_id'))->first();
		if($booking->status == '3'){
			return response()->json(['status'=>false,'message'=>'Trip already cancelled.']);
		}elseif($booking->status == '1' || $booking->status == '2'){
			return response()->json(['status'=>false,'message'=>'You can\'t cancel this trip.']);
		}	
		Stripe::setApiKey(config('services.stripe.secret'));

		try {
			DB::transaction(function () use ($booking){

				$user 					= Auth::user();

				if($booking->handshake){
					/** refund to user **/
					$admin_statistic 			=  TripHelper::getAdminStatistics();				
					$amount						= $booking->total - $booking->tax;
					$refunded_amount			= $amount - (($amount * $admin_statistic->refund_charge_cent)/100);

					$refund = \Stripe\Refund::create(array(
						'charge'  => $booking->transaction->charge_id ,
						'amount'  => round(($refunded_amount*100)),
						'metadata'=> array('booking_id' => $booking->id)
					));
					if($refund){
						$data['amount'] 			= number_format($refunded_amount,2);
						$data['payment_id'] 		= $booking->id;
						$data['transaction_id'] 	= $booking->transaction->id;
						$data['charge_id'] 			= $refund->charge;
						$data['refund_id'] 			= $refund->id;
						$data['status'] 			= $refund->status;
						$data['transaction_time']	= Carbon::createFromTimestamp($refund->created)->toDateTimeString();;
						Refund::create($data);
					}

					$charge 						= \Stripe\Charge::retrieve(
						$booking->transaction->charge_id,
						['api_key' => config('services.stripe.secret')]
					);
					$card_used						= $charge->source->last4;


				}else{
					$refund 				= true;
					$refunded_amount		= 0;
					$card_used				= false;
				}

				/** database update **/
				if($refund){

					$booking->status 	= '3';
					$booking->save();

					$trip 					= Trip::find($booking->trip_id);
					$trip->available_seats 	= $trip->available_seats+$booking->total_person;
					$trip->save();

				//notifications
					$trip->user_type 		= 'user';
					$trip->user_name 		= $user->first_name.' '.$user->last_name;
					$trip->booking_id 		= $booking->id;
					$trip->refunded_amount 	= $refunded_amount;
					$trip->card_used 		= $card_used;
					$user->notify(new CancelTripNotification($trip));
					$trip->seats 			= $booking->total_person;

					if($user->active_mode == 'user'){
						NotificationHelper::sendPushNotification($user,'UserTripCancel',$trip->toArray());
					}

					$driver 				= Driver::where('id',$trip->driver_id)->with('user')->first();
					$trip->user_name 		= $user->first_name.' '.$user->last_name;
					$trip->user_type        = 'driver';
					$driver->user->notify(new CancelTripNotification($trip));
					if($driver->user->active_mode == 'driver'){
						NotificationHelper::sendPushNotification($driver->user,'DriverTripCancel',$trip->toArray());
					}

				}

			});
		} catch(\Stripe\Error\Card $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		} catch (\Stripe\Error\InvalidRequest $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		} catch (\Stripe\Error\Authentication $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		} catch (\Stripe\Error\ApiConnection $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		} catch (\Stripe\Error\Base $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		} catch (Exception $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		}


		return response()->json(['status'=>true,'message'=>'Your booking cancelled successfully.']);

	}

	//user search trip
	public function search_places(Request $request) 
	{
		$validator = Validator::make($request->all(),[
			'term' => 'required',
		]);
		if ($validator->fails())
		{
			$message = $validator->messages()->first();
			return response()->json(['status'=>false,'message'=>$message]);
		}
		$user = Auth::user();
		$term=$request->get('term');
		\DB::connection()->enableQueryLog();

		$locations=\DB::table('work_locations')->select(\DB::raw("CONCAT(work_locations.name,' ',work_cities.name,' ',work_countries.name) AS name"),'work_locations.place_id')
		->join('work_countries', 'work_countries.id', '=', 'work_locations.work_country_id')
		->join('work_cities', 'work_cities.id', '=', 'work_locations.work_city_id')
		->where(function ($query) use ($term) {
			$query->where('work_locations.name', 'LIKE','%'.$term.'%')
			->orWhere('work_countries.name', 'LIKE','%'.$term.'%')
			->orWhere('work_cities.name', 'LIKE','%'.$term.'%');
		})
		->get();
		return response()->json(['status'=>true,'message'=>"Places retrived successfully.",'data'=>$locations]);
	}

	// search cities
	public function search_cities(Request $request) 
	{
		// $validator = Validator::make($request->all(),[
		// 	'city_term' => 'required',
		// ]);
		// if ($validator->fails())
		// {
		// 	$message = $validator->messages()->first();
		// 	return response()->json(['status'=>false,'message'=>$message]);
		// }
		$user = Auth::user();
		$term=$request->get('city_term');

		$cities=Work_city::select('id','name');
		if($term){
			$cities->where('name', 'LIKE','%'.$term.'%');
		}
		$cities=$cities->get();

		return response()->json(['status'=>true,'message'=>"Cities retrived successfully.",'data'=>$cities]);
	}

	// search cities
	public function search_places_by_city(Request $request) 
	{
		$validator = Validator::make($request->all(),[
			'from_city_id' => 'required|exists:work_cities,id',
			'to_city_id' => 'required|exists:work_cities,id',
		]);
		if ($validator->fails())
		{
			$message = $validator->messages()->first();
			return response()->json(['status'=>false,'message'=>$message]);
		}
		$user = Auth::user();

		$from_city_locations=Work_location::select('id','name','place_id','latitude','longitude','placeName','postalCode')
		->where('work_city_id',$request->get('from_city_id'))
		->get();

		$to_city_locations=Work_location::select('id','name','place_id','latitude','longitude','placeName','postalCode')
		->where('work_city_id',$request->get('to_city_id'))
		->get();

		return response()->json(['status'=>true,'message'=>"Locations retrived successfully.",'from_city'=>$from_city_locations,'to_city'=>$to_city_locations]);
	}

	//user book trip
	public function book_trip(Request $request) 
	{
		$validator = Validator::make($request->all(),[
			'trip_id' => 'required|exists:trips,id',
			'luggage' => 'required',
			'total_seat' => 'required',
		]);
		if ($validator->fails())
		{
			$message = $validator->messages()->first();
			return response()->json(['status'=>false,'message'=>$message]);
		}
		$user = Auth::user();
		$trip=Trip::find($request->get('trip_id'));
		if($trip->available_seats < $request->get('total_seat')){
			return response()->json(['status'=>false,'message'=>"No ".$request->get('total_seat')." seats available."]);
		}
		$book['user_id']=$user->id;
		$book['trip_id']=$request->get('trip_id');
		$book['total']=$trip->price*$request->get('total_seat');
		$book['total_person']=$request->get('total_seat');
		$book['luggage']=$request->get('luggage');
		$admin_statastics=Admin_statistic::find(1);

		if($admin_statastics->transfer_cent != ''){
			$book['transfer_fees']=($book['total']*$admin_statastics->transfer_cent)/100;
		}else{
			$book['transfer_fees']=$admin_statastics->transfer_fees;
		}

		if($admin_statastics->admin_cent != ''){
			$book['admin_fees']=($book['total']*$admin_statastics->admin_cent)/100;
		}else{
			$book['admin_fees']=$admin_statastics->admin_fees;
		}
		$book['driver_fees']=$book['total']-$book['admin_fees']-$book['transfer_fees'];
		// $book['total']=$book['driver_fees']+$book['admin_fees']+$book['transfer_fees'];
		$book['withdrow_status']='0';
		$book['user_withdrow_status']='0';

		$payment=Payment::create($book);
		$trip->available_seats=$trip->available_seats-$book['total_person'];
		$trip->save();
		Stripe::setApiKey(config('services.stripe.secret'));

		try{
			$charge = \Stripe\Charge::create(array(
				"amount" => round($book['total']*100),
				"currency" => "cad",
				"customer" =>$user->stripe_id,
				"source" =>$request->get('card') ,
			));

			if($charge){

				$data['amount'] = number_format($book['total'],2);
				$data['payment_id'] = $payment->id;
				$data['charge_id'] = $charge->id;
				$data['transaction_id'] = $charge->balance_transaction;
				$data['charged_currency']   = $charge->currency;

				$data['transaction_time'] = date('Y-m-d H:i:s', $charge->created);
				$data['failure_code'] = ($charge->failure_code != "")?$charge->failure_code:"";
				$data['failure_message'] = ($charge->failure_message != "")?$charge->failure_message:"";
				$data['paid'] = $charge->paid;
				$data['status'] = $charge->status;

				Transaction::insert($data);
				$tripData=Payment::select('id','trip_id','total','total_person as seats','created_at')
				->where('id',$payment->id)
				->where('user_id',$user->id)
				->with(['trip'=> function ($query) use ($user) {
					$query->select('id','trip_date','pickup_time','from_city_id','to_city_id','pickup_id','dropoff_id','price')
			//->where('status','active')
					->with(['from_city'=> function ($query) {
						$query->select('id','name');
					}])
					->with(['to_city'=> function ($query) {
						$query->select('id','name');
					}])
					->with(['pickup_location'=> function ($query) {
						$query->select('id','name','latitude','longitude','place_id');
					}])
					->with(['dropoff_location'=> function ($query) {
						$query->select('id','name','latitude','longitude','place_id');
					}]);
				},'transaction'])
				->first();
				$charge=\Stripe\Charge::retrieve(
					$tripData->transaction->charge_id,
					['api_key' => config('services.stripe.secret')]
				);
				$tripData->card_used=$charge->source->last4;
				unset($tripData->transaction);

				//notifications
				$trip->user_type = 'user';
				$trip->booking_id=$payment->id;
				$trip->seats=$tripData->seats;
				$user->notify(new BookingNotification($trip));
				if($user->active_mode == 'user'){
					NotificationHelper::sendPushNotification($user,'UserTripBooking',$trip->toArray());
				}

				$trip->user_name=$user->first_name.' '.$user->last_name;
				$driver=Driver::where('id',$trip->driver_id)->first()->user;
				$trip->user_type = 'driver';
				$driver->notify(new BookingNotification($trip));
				if($driver->active_mode == 'driver'){
					NotificationHelper::sendPushNotification($driver,'DriverTripBooking',$trip->toArray());
				}
			}
		}catch(\Stripe\Error\Card $e){
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		} catch (\Stripe\Error\NotFound $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		}
		catch (\Stripe\Error\Authentication $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		}
		catch (\Stripe\Error\InvalidRequest $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		} catch (\Stripe\Error\RateLimit $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		} catch (\Stripe\Error\ApiConnection $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		} catch (\Stripe\Error\Base $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		}
		$tripData->total=number_format($tripData->total,2);

		return response()->json(['status'=>true,'message'=>"Trip booked successfully.",'data'=>$tripData]);
	}


	//start trip
	public function start_trip(Request $request) 
	{
		$data['pickup']=json_decode($request->get('pickup'));
		$data['trip_id']=$request->get('trip_id');
		// print_r($request->all());die();
		$validator = Validator::make($data,[
			'trip_id' => 'required|exists:trips,id',
			'pickup'  =>'required|array',
			'pickup.*' => 'exists:payments,id',
		]);
		if ($validator->fails())
		{
			$message = $validator->messages()->first();
			return response()->json(['status'=>false,'message'=>$message]);

		}
		$pickup=json_decode($request->get('pickup'));

		foreach ($pickup as $key => $tripuser_id) {
			Payment::where('id',$tripuser_id)->update(array('status'=>'1','updated_at'=>date('Y-m-d h:i:s')));
		}
		$user = Auth::user();
		Trip::where('id',$request->get('trip_id'))->update(array('status'=>'active','pickup_time'=>date('H:i:s')));

		//email notifications
		$trip_info=Trip::where('id',$request->get('trip_id'))
		->with(['from_city'=> function ($query) {
			$query->select('id','name');
		}])
		->with(['to_city'=> function ($query) {
			$query->select('id','name');
		}])
		->with(['payments'=> function ($query) {
			$query->select('id','user_id', 'trip_id','total','total_person','status','created_at','luggage')
			->with(['user'=> function ($query) {
				$query->select('id','first_name', 'last_name','email','firebase_android_id','firebase_ios_id');
			}])
			->where('status','!=','3');
		}])
		->first();
		$trip_info->user_type = 'user';
		$trip_info->booking_id = null;
		$users=[];
		foreach ($trip_info->payments as $key => $payment) {
			$trip_info->booking_id = $payment->id;
			$payment->user->trip_date=$payment->created_at;
			$payment->user->notify(new StartTripNotification($trip_info));
			$user_trip=$payment->user;
			NotificationHelper::sendPushNotification($user_trip,'UserTripStart',$trip_info->toArray());
			$payment->user->trip_id=$payment->trip_id;
			$payment->user->booking_id=$payment->id;
		}

		$trip_info->user_type = 'driver';
		$user->notify(new StartTripNotification($trip_info));

		/********************************Internal Data for Push notification***************************************/
		// echo "<pre>";print_r($trip_info->toArray());die();
		// print_r($users);die();

		NotificationHelper::sendPushNotification($user,'DriverTripStart',$trip_info->toArray());
		/********************************Internal Data for Push notification***************************************/

		return response()->json(['status'=>true,'message'=>'Trip started successfully.']);
	}

	//complete trip
	public function complete_trip(Request $request) 
	{
		$data['pickup']=json_decode($request->get('pickup'));
		$data['trip_id']=$request->get('trip_id');
		// print_r($request->all());die();
		$validator = Validator::make($data,[
			'trip_id' => 'required|exists:trips,id',
			'pickup'  =>'required|array',
			'pickup.*' => 'exists:payments,id',
		]);
		
		if ($validator->fails())
		{
			$message = $validator->messages()->first();
			return response()->json(['status'=>false,'message'=>$message]);
		}
		$pickup=json_decode($request->get('pickup'));
		foreach ($pickup as $key => $tripuser_id) {
			Payment::where('id',$tripuser_id)->update(array('status'=>'2','updated_at'=>date('Y-m-d h:i:s')));
		}
		$user = Auth::user();
		Trip::where('id',$request->get('trip_id'))
		->update(array('status'=>'completed','dropoff_time'=>date('Y-m-d H:i:s')));

		//email notifications
		$trip_info=Trip::where('id',$request->get('trip_id'))
		->with(['from_city'=> function ($query) {
			$query->select('id','name');
		}])
		->with(['to_city'=> function ($query) {
			$query->select('id','name');
		}])
		->with(['payments'=> function ($query) {
			$query->select('id','user_id', 'trip_id','total','total_person','status','created_at','luggage')
			->with(['user'=> function ($query) {
				$query->select('id','first_name', 'last_name','email','firebase_android_id','firebase_ios_id');
			}])
			->where('status','!=','3');
		}])
		->first();
		$trip_info->user_type = 'user';
		$users=[];
		$trip_info->booking_id = null;
		foreach ($trip_info->payments as $key => $payment) {
			if($payment->status == '2'){
				$trip_info->booking_id = $payment->id;
				$payment->user->trip_id=$payment->trip_id;
				$payment->user->booking_id=$payment->id;
				$payment->user->notify(new CompleteTripNotification($trip_info));
				NotificationHelper::sendPushNotification($payment->user,'UserTripCompleted',$trip_info->toArray());
			}
		}
		
		$trip_info->user_type = 'driver';
		$user->notify(new CompleteTripNotification($trip_info));

		/********************************Internal Data for Push notification***************************************/

		NotificationHelper::sendPushNotification($user,'DriverTripCompleted',$trip_info->toArray());
		/********************************Internal Data for Push notification***************************************/

		return response()->json(['status'=>true,'message'=>'Trip completed successfully.']);
	}


	//start trip
	public function cancel_trip(Request $request) 
	{
		$validator = Validator::make($request->all(),[
			'trip_id' => 'required|exists:trips,id',
		]);
		if ($validator->fails())
		{
			$message = $validator->messages()->first();
			return response()->json(['status'=>false,'message'=>$message]);
		}
		$user = Auth::user();
		Trip::where('id',$request->get('trip_id'))->update(array('status'=>'decline'));

		return response()->json(['status'=>true,'message'=>'Trip cancel successfully.']);
	}

	//user active and upcoming trips
	public function active_trips() 
	{

		$user = Auth::user();
		$active_trips=Payment::select('payments.id','trip_id','user_id','total_person','total as amount','luggage','payments.created_at')
		->with(['trip'=> function ($query) use ($user) {
			$query->select('id','trip_date','from_city_id','to_city_id','pickup_id','dropoff_id','price','available_seats','total_seats','pickup_time')
			//->where('status','active')
			->with(['from_city'=> function ($query) {
				$query->select('id','name');
			}])
			->with(['to_city'=> function ($query) {
				$query->select('id','name');
			}])
			->with(['pickup_location'=> function ($query) {
				$query->select('id','name','latitude','longitude','place_id');
			}])
			->with(['dropoff_location'=> function ($query) {
				$query->select('id','name','latitude','longitude','place_id');
			}]);
		}])
		->join('trips', 'trips.id', '=', 'payments.trip_id')	
		->where('trips.status','active')
		->where('payments.status','1')	
		->where('user_id',$user->id)
		->orderBy('trip_date','ASC')		
		->get()
		->map(function($active_trip){
			$active_trip->amount = number_format($active_trip->amount,2);;
			return $active_trip;
		});
		
		return response()->json(['status'=>true,'message'=>'Data retrived.','active_trips'=>$active_trips]);
	}

	public function upcoming_trips() 
	{

		$user = Auth::user();
		$upcoming_trips=Payment::select('payments.id','trip_id','user_id','total_person','total as amount','payments.luggage','payments.created_at')
		->with(['trip'=> function ($query) use ($user) {
			$query->select('id','trip_date','from_city_id','to_city_id','pickup_id','dropoff_id','price','pickup_time','available_seats')
			//->where('status','upcoming')
			->with(['from_city'=> function ($query) {
				$query->select('id','name');
			}])
			->with(['to_city'=> function ($query) {
				$query->select('id','name');
			}])
			->with(['pickup_location'=> function ($query) {
				$query->select('id','name','latitude','longitude','place_id');
			}])
			->with(['dropoff_location'=> function ($query) {
				$query->select('id','name','latitude','longitude','place_id');
			}]);
		}])
		->join('trips', 'trips.id', '=', 'payments.trip_id')	
		->where('trips.status','upcoming')	
		->where('trips.trip_date','>=',date('Y-m-d'))	
		->where('payments.status','0')	
		->where('user_id',$user->id)
		->orderBy('trip_date','ASC')		
		->paginate(10);

		$upcoming_trips->getCollection()->transform(function($upcoming_trip,$key){
			$upcoming_trip->amount = number_format($upcoming_trip->amount,2);;
			return $upcoming_trip;
		});
		return response()->json(['status'=>true,'message'=>'Data retrived.','upcoming_trips'=>$upcoming_trips]);
	}

	//user completed trips
	public function completed_trips() 
	{

		$user = Auth::user();

		$completed_trips=Payment::select('payments.id','trip_id','user_id','total_person','total','luggage','payments.created_at')
		->with(['trip'=> function ($query) use ($user) {
			$query->select('id','trip_date','from_city_id','to_city_id','pickup_id','dropoff_id','price','pickup_time',\DB::raw('DATE_FORMAT(dropoff_time,"%h:%i:%s") as dropoff_time'))
			//->where('status','completed')
			->with(['from_city'=> function ($query) {
				$query->select('id','name');
			}])
			->with(['to_city'=> function ($query) {
				$query->select('id','name');
			}])
			->with(['pickup_location'=> function ($query) {
				$query->select('id','name','latitude','longitude','place_id');
			}])
			->with(['dropoff_location'=> function ($query) {
				$query->select('id','name','latitude','longitude','place_id');
			}]);
		}])
		->join('trips', 'trips.id', '=', 'payments.trip_id')	
		->where('trips.status','completed')
		->where('payments.status','2')	
		->where('user_id',$user->id)	
		->orderBy('trip_date','ASC')		
		->paginate(10);
		$completed_trips->getCollection()->transform(function($completed_trip,$key){
			$completed_trip->total = number_format($completed_trip->total,2);;
			return $completed_trip;
		});

		return response()->json(['status'=>true,'message'=>'Data retrived.','completed_trips'=>$completed_trips]);
	}


	//user active and upcoming trips
	public function driver_active_trip(Request $request) 
	{
		$validator = Validator::make($request->all(),[
			'trip_id' => 'required|exists:trips,id',
		]);
		if ($validator->fails())
		{
			$message = $validator->messages()->first();
			return response()->json(['status'=>false,'message'=>$message]);
		}
		$user = Auth::user();
		$active_trip=Trip::select('id','driver_id','trip_date','from_city_id','to_city_id','pickup_id','dropoff_id','price','created_at','total_seats','available_seats','pickup_time')
		->with(['from_city'=> function ($query) {
			$query->select('id','name');
		}])
		->with(['to_city'=> function ($query) {
			$query->select('id','name');
		}])
		->with(['pickup_location'=> function ($query) {
			$query->select('id','name','latitude','longitude','place_id');
		}])
		->with(['dropoff_location'=> function ($query) {
			$query->select('id','name','latitude','longitude','place_id');
		}])
		->with(['payments'=> function ($query) {
			$query->select('id','user_id', 'trip_id','driver_fees','total','total_person','status','updated_at','luggage')
			->with(['user'=> function ($query) {
				$query->select('id','first_name', 'last_name','mobile_no','profile_pic');
			}])
			->where('status','!=','3');
			//->where('status','1');
		}])
		->with(['driver'=> function ($query) {
			$query->select('id','user_id')->with(['user'=> function ($query) {
				$query->select('id','first_name', 'last_name','profile_pic');
			}]);
		}])
		->where('id',$request->get('trip_id'))
		// ->where('status','active')
		// ->where('driver_id',$user->driver->id)		
		->first();
		$active_trip->payments_total=number_format($active_trip->payments->sum('driver_fees'),2);
		$active_trip->no_show_count=$active_trip->payments->where('status','0')->sum('total_person');
		$active_trip->pickup_count=$active_trip->payments->where('status','1')->sum('total_person');
		$active_trip->base_url=asset('storage/images/users/'.$active_trip->driver->user_id.'/profile_pic').'/';
		$active_trip->payments->map(function($payment,$key){
			$payment->total=number_format($payment->total,2);
			$payment->base_path=asset('storage/images/users/'.$payment->user_id.'/profile_pic').'/';
		});
		
		return response()->json(['status'=>true,'message'=>'Data retrived.','active_trips'=>$active_trip]);
	}

	public function driver_upcoming_trip(Request $request) 
	{
		$validator = Validator::make($request->all(),[
			'trip_id' => 'required|exists:trips,id',
		]);
		if ($validator->fails())
		{
			$message = $validator->messages()->first();
			return response()->json(['status'=>false,'message'=>$message]);
		}
		$user = Auth::user();
		$upcoming_trip=Trip::select('id','trip_date','from_city_id','to_city_id','pickup_id','dropoff_id','price','created_at','total_seats','available_seats','pickup_time')
		->with(['from_city'=> function ($query) {
			$query->select('id','name');
		}])
		->with(['to_city'=> function ($query) {
			$query->select('id','name');
		}])
		->with(['pickup_location'=> function ($query) {
			$query->select('id','name','latitude','longitude','place_id');
		}])
		->with(['dropoff_location'=> function ($query) {
			$query->select('id','name','latitude','longitude','place_id');
		}])
		->with(['payments'=> function ($query) {
			$query->select('id','user_id', 'trip_id','total','total_person','status','updated_at','luggage','driver_fees')
			->with(['user'=> function ($query) {
				$query->select('id','first_name', 'last_name','mobile_no','profile_pic');
			}])
			->where('status','!=','3');
		}])
		->where('id',$request->get('trip_id'))
		// ->where('status','upcoming')
		// ->where('driver_id',$user->driver->id)		
		->first();

		$upcoming_trip->payments_total=number_format($upcoming_trip->payments->sum('driver_fees'),2);
		$upcoming_trip->payments->map(function($payment,$key){
			$payment->total=number_format($payment->total,2);
			$payment->base_path=asset('storage/images/users/'.$payment->user_id.'/profile_pic').'/';
		});
		return response()->json(['status'=>true,'message'=>'Data retrived.','upcoming_trips'=>$upcoming_trip]);
	}

	//user completed trips
	public function driver_completed_trip(Request $request) 
	{
		$validator = Validator::make($request->all(),[
			'trip_id' => 'required|exists:trips,id',
		]);
		if ($validator->fails())
		{
			$message = $validator->messages()->first();
			return response()->json(['status'=>false,'message'=>$message]);
		}
		$user = Auth::user();
		$completed_trip=Trip::select('id','driver_id','trip_date','from_city_id','to_city_id','pickup_id','dropoff_id','price','created_at','available_seats','pickup_time',\DB::raw('DATE_FORMAT(dropoff_time,"%h:%i:%s") as dropoff_time'))
		->with(['from_city'=> function ($query) {
			$query->select('id','name');
		}])
		->with(['to_city'=> function ($query) {
			$query->select('id','name');
		}])
		->with(['pickup_location'=> function ($query) {
			$query->select('id','name','latitude','longitude','place_id');
		}])
		->with(['dropoff_location'=> function ($query) {
			$query->select('id','name','latitude','longitude','place_id');
		}])
		->with(['payments'=> function ($query) {
			$query->select('id','user_id', 'trip_id','total','total_person','status','updated_at','luggage','driver_fees')
			->with(['user'=> function ($query) {
				$query->select('id','first_name', 'last_name','mobile_no','profile_pic');
			}])
			->where('status','!=','3');
		}])
		->with(['driver'=> function ($query) {
			$query->select('id','user_id')->with(['user'=> function ($query) {
				$query->select('id','first_name', 'last_name','profile_pic');
			}]);
		}])
		->where('id',$request->get('trip_id'))
		// ->where('status','completed')
		// ->where('driver_id',$user->driver->id)		
		->first();

		$completed_trip->payments_total = number_format($completed_trip->payments->sum('driver_fees'),2);;
		$completed_trip->total_person=$completed_trip->payments->count();
		$completed_trip->no_show_count=$completed_trip->payments->where('status','0')->sum('total_person');
		$completed_trip->drop_off_count=$completed_trip->payments->where('status','2')->sum('total_person');
		$completed_trip->payments->map(function($payment,$key){
			$payment->base_url=asset('storage/images/users/'.$payment->user_id.'/profile_pic').'/';
		});
		$completed_trip->base_url=asset('storage/images/users/'.$completed_trip->driver->user_id.'/profile_pic').'/';
		return response()->json(['status'=>true,'message'=>'Data retrived.','completed_trips'=>$completed_trip]);
	}



	//driver active and upcoming trips
	public function driver_active_trips() 
	{
		$user = Auth::user();
		$active_trips=Trip::select('id','driver_id','trip_date','from_city_id','to_city_id','pickup_id','dropoff_id','price','created_at','total_seats','available_seats','pickup_time')
		->with(['from_city'=> function ($query) {
			$query->select('id','name');
		}])
		->with(['to_city'=> function ($query) {
			$query->select('id','name');
		}])
		->with(['pickup_location'=> function ($query) {
			$query->select('id','name','latitude','longitude','place_id');
		}])
		->with(['dropoff_location'=> function ($query) {
			$query->select('id','name','latitude','longitude','place_id');
		}])
		->with(['payments'=> function ($query) {
			$query->select('id','user_id', 'trip_id','total','total_person','status','updated_at','luggage','driver_fees')
			->with(['user'=> function ($query) {
				$query->select('id','first_name', 'last_name','mobile_no','profile_pic');
			}])
			->where('status','!=','3');
			// ->where('status','!=','0');
		}])
		->with(['driver'=> function ($query) {
			$query->select('id','user_id')->with(['user'=> function ($query) {
				$query->select('id','first_name', 'last_name','profile_pic');
			}]);
		}])
		->where('status','active')
		->where('driver_id',$user->driver->id)	
		->orderBy('trip_date','ASC')				
		->get();
		$active_trips = $active_trips->map(function($active_trip,$key){

			$active_trip->payments_total=number_format($active_trip->payments->sum('driver_fees'),2);
			$active_trip->no_show_count=$active_trip->payments->where('status','0')->sum('total_person');
			$active_trip->pickup_count=$active_trip->payments->where('status','1')->sum('total_person');
			$active_trip->base_url=asset('storage/images/users/'.$active_trip->driver->user_id.'/profile_pic').'/';
			$active_trip->payments->map(function($payment,$key){
				$payment->total=number_format($payment->total,2);
				$payment->base_path=asset('storage/images/users/'.$payment->user_id.'/profile_pic').'/';
			});
			return $active_trip;
		});
		
		return response()->json(['status'=>true,'message'=>'Data retrived.','active_trips'=>$active_trips]);
	}

	//driver active and upcoming trips
	public function driver_upcoming_trips() 
	{
		$user = Auth::user();
		$upcoming_trips=Trip::select('id','trip_date','from_city_id','to_city_id','pickup_id','dropoff_id','price','created_at','total_seats','available_seats','pickup_time')
		->with(['from_city'=> function ($query) {
			$query->select('id','name');
		}])
		->with(['to_city'=> function ($query) {
			$query->select('id','name');
		}])
		->with(['pickup_location'=> function ($query) {
			$query->select('id','name','latitude','longitude','place_id');
		}])
		->with(['dropoff_location'=> function ($query) {
			$query->select('id','name','latitude','longitude','place_id');
		}])
		->with(['payments'=> function ($query) {
			$query->select('id','user_id', 'trip_id','total','total_person','status','updated_at','luggage','driver_fees')
			->with(['user'=> function ($query) {
				$query->select('id','first_name', 'last_name','mobile_no','profile_pic');
			}])
			->where('status','!=','3');
		}])
		->where('status','upcoming')
		->where('trip_date','>=',date('Y-m-d'))	
		->where('driver_id',$user->driver->id)	
		->orderBy('trip_date','ASC')		
		->paginate(10);

		$upcoming_trips->getCollection()->transform(function($upcoming_trip,$key){
			$upcoming_trip->payments_total=number_format($upcoming_trip->payments->sum('driver_fees'),2);
			// $upcoming_trip->payments_total=$upcoming_trip->payments->sum('total');
			// $upcoming_trip->base_url=asset('storage/images/users/'.$upcoming_trip->driver->user_id.'/profile_pic').'/';
			$upcoming_trip->payments->map(function($payment,$key){
				$payment->total=number_format($payment->total,2);
				$payment->base_path=asset('storage/images/users/'.$payment->user_id.'/profile_pic').'/';
			});
			return $upcoming_trip;
		});
		
		return response()->json(['status'=>true,'message'=>'Data retrived.','upcoming_trips'=>$upcoming_trips]);
	}

	//pickup rider
	public function pick_up_rider(Request $request) 
	{
		$validator = Validator::make($request->all(),[
			'tripuser_id' => 'required|exists:payments,id',
		]);
		if ($validator->fails())
		{
			$message = $validator->messages()->first();
			return response()->json(['status'=>false,'message'=>$message]);
		}
		$user = Auth::user();
		Payment::where('id',$request->get('tripuser_id'))->where('user_id',$user->id)->update(array('status'=>'1','updated_at'=>date('Y-m-d h:i:s')));

		return response()->json(['status'=>true,'message'=>'Rider pickup successfully.']);
	}


	//driver completed trips
	public function driver_completed_trips() 
	{
		$user = Auth::user();
		$completed_trips=Trip::select('id','driver_id','trip_date','from_city_id','to_city_id','pickup_id','dropoff_id','price','created_at','available_seats','pickup_time',\DB::raw('DATE_FORMAT(dropoff_time,"%h:%i:%s") as dropoff_time'))
		->with(['from_city'=> function ($query) {
			$query->select('id','name');
		}])
		->with(['to_city'=> function ($query) {
			$query->select('id','name');
		}])
		->with(['pickup_location'=> function ($query) {
			$query->select('id','name','latitude','longitude','place_id');
		}])
		->with(['dropoff_location'=> function ($query) {
			$query->select('id','name','latitude','longitude','place_id');
		}])
		->with(['payments'=> function ($query) {
			$query->select('id','user_id', 'trip_id','total','total_person','status','updated_at','luggage','driver_fees')
			->with(['user'=> function ($query) {
				$query->select('id','first_name', 'last_name','mobile_no','profile_pic');
			}])
			->where('status','!=','3')
			->where('status','!=','0');
		}])
		->with(['driver'=> function ($query) {
			$query->select('id','user_id')->with(['user'=> function ($query) {
				$query->select('id','first_name', 'last_name','profile_pic');
			}]);
		}])
		->where('status','completed')
		->where('driver_id',$user->driver->id)	
		->orderBy('trip_date','ASC')				
		->paginate(10);
		$completed_trips->getCollection()->transform(function($completed_trip,$key){
		// 	$completed_trip->total = number_format($completed_trip->total,2);;
		// 	return $completed_trip;
		// });
		// $completed_trips = $completed_trips->map(function($completed_trip,$key){
			$completed_trip->payments_total = number_format($completed_trip->payments->sum('driver_fees'),2);;
			$completed_trip->total_person=$completed_trip->payments->count();
			$completed_trip->no_show_count=$completed_trip->payments->where('status','0')->sum('total_person');
			$completed_trip->drop_off_count=$completed_trip->payments->where('status','2')->count();//->sum('total_person');
			$completed_trip->payments->map(function($payment,$key){
				$payment->base_url=asset('storage/images/users/'.$payment->user_id.'/profile_pic').'/';
			});
			$completed_trip->base_url=asset('storage/images/users/'.$completed_trip->driver->user_id.'/profile_pic').'/';
			return $completed_trip;
		});
		return response()->json(['status'=>true,'message'=>'Data retrived.','completed_trips'=>$completed_trips]);
	}

		//driver transaction history
	public function trnsaction_history(Request $request) 
	{
		$user=Auth::user();
		$page = $request->page;

		$payment_history=Trip::select('id','trip_date','from_city_id','to_city_id','price')
		->with(['from_city'=> function ($query) {
			$query->select('id','name');
		}])
		->with(['to_city'=> function ($query) {
			$query->select('id','name');
		}])
		->with(['payments'=> function ($query) {
			$query->select('id','trip_id',\DB::raw('round(driver_fees,2) as driver_fees'))->where('status','!=','3');
			//$query->groupBy('trip_id');
		}])
		->whereHas('payments')
		->where('status','completed')
		->where('driver_id',$user->driver->id)
		->orderBy('trip_date','desc')		
		->paginate(10);
		$payment_history->getCollection()->transform(function($payment_hist,$key) use ($user){
			// $payment_hist->total_completed_rides=$payment_hist->count();
			$payment_hist->payments->transform(function($payment,$key){
				$payment->driver_fees=number_format($payment->driver_fees,2);
				return $payment;
			});
			$payment_hist->payments_total=number_format(round($payment_hist->payments->sum('driver_fees'),2),2);
			return $payment_hist;
		});

		
		$transactions=Payment::join('trips',function($requests){
			$requests->on('payments.trip_id','=','trips.id');
		})
		->where('trips.driver_id',$user->driver->id)
		->where('trips.status','completed')
		->whereHas('transaction')
		->where('payments.status','!=','3')
		// ->where('payments.withdrow_status','0')
		->get()
		->sum('driver_fees');

		$total_withdraws=$user->driver->withdraws()->sum('amount');
		$total_balance=($transactions>0)?number_format($transactions-$total_withdraws,2):'0.00';

		// $total_balance=($transactions>0)?number_format($transactions,2):0;
		$total_completed_rides=Trip::where('status','completed')
		->where('driver_id',$user->driver->id)
		->count();
		// $total_balance=number_format($payment_history->sum('payments_total'),2);
		return response()->json(['status'=>true,'message'=>'Transaction history retrived successfully.','data'=>$payment_history,'total_balance'=>$total_balance,'total_completed_rides'=>$total_completed_rides]);
	}
}
