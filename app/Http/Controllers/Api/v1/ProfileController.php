<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Validator;
use Image;
use File;
use Storage;
use DB;
use App\Models\v3\User;

class ProfileController extends Controller
{
	//update profile
	public function updateProfilePic(Request $request)
	{	
		$validator = Validator::make($request->all(),[
			'profile_pic' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
		]);
		if ($validator->fails())
		{
			$message = $validator->messages()->first();
			return response()->json(['status'=>false,'message'=>$message]);
		}else{
			
			$image = $request->file('profile_pic');
			$user = Auth::user();
			$user_id = $user->id;
			$fileName = $image->getClientOriginalName();
			$fileExtension = $image->getClientOriginalExtension();

			$input['imagename'] = $user_id."_".uniqid(true).".".$fileExtension;
			$destinationPath = storage_path('app/public/images/users/'.$user_id.'/profile_pic');
			
			if(!File::exists(storage_path('app/public/images/users/'.$user_id))){
				$directoryPath = storage_path('app/public/images/users/'.$user_id);
				File::makeDirectory($directoryPath,0755,true);
			}

			if(!File::exists(storage_path('app/public/images/users/'.$user_id.'/profile_pic'))){
				$profilePath   = storage_path('app/public/images/users/'.$user_id.'/profile_pic');
				File::makeDirectory($profilePath,0755,true);
			}

			$img = Image::make($image);
			// $height = $img->height();
			// $width = $img->width();

			// if ($height > $width) {
			// 	$img->resize(null, 800, function ($constraint) {
			// 		$constraint->aspectRatio();
			// 	});
			// }else{
			// 	$img->resize(800, null, function ($constraint) {
			// 		$constraint->aspectRatio();
			// 	});
			// }
			
			$img->save(storage_path('app/public/images/users/'.$user_id.'/profile_pic/'.$input['imagename']));
			
			$user->profile_pic = $input['imagename'];
			$user->save();
			Auth::setUser($user);

			$response['profile_pic'] = $input['imagename'];
			$response['base_url'] = asset('storage/images/users/'.$user->id.'/profile_pic');
			return response()->json(['status'=>true,'message'=>'Profile Picture Uploaded Successfully.','data'=>$response]);
		}
	} 

	//delete profile
	public function DeleteProfilePic(Request $request)
	{
		
		$id = Auth::user()->id;
		$profile_pic = Auth::user()->profile_pic;
		$image_path = 'images/users/'.$id.'/profile_pic/'.$profile_pic;
		if(Storage::disk('public')->exists($image_path) && $profile_pic != null) {
			Storage::disk('public')->delete($image_path);
		}
		else{
			return response()->json(['status'=>false,'message'=>'Something went Wrong.']);
		}
		DB::table('users')->where(['id'=>(int)$id])->update(['profile_pic'=>'']);
		return response()->json(['status'=>true,'message'=>'Profile Picture Deleted successfully.']);

	}   

	//update profile
	public function update_profile(Request $request){
		$input=$request->all();
		$user    = Auth::user();

		$validator = Validator::make($input,[
			'first_name' => "required|string|max:50",
			'last_name' => "required|string|max:50",
			'email' => "required|string|email|max:255|unique:users,email,". $user->id,
			'dob' => "required | date",
			'gender' => "required",
			'mobile_no'=>"required|unique:users,mobile_no,". $user->id,
		]);

		if ($validator->fails()) {
			return response()->json(['status'=>false,'message' => $validator->messages()->first()]);
		}

		User::where('id',$user->id)->update($input);
		return response()->json(['status'=>true,'message' => 'Profile updated successfully.','data'=>$input]); 

	}

	// driver language api 
	public function update_driver_languages(Request $request) 
	{
		
		$user   = 	Auth::user();

		$validator = Validator::make($request->all(),[
			'languages_known' => "required|json",
		]);

		if ($validator->fails()) {
			return response()->json(['status'=>false,'message' => $validator->messages()->first()]);
		}

		$user->languages_known = json_decode($request->get('languages_known'));
		$user->save();

		return response()->json(['status'=>true,'message' => 'Information updated successfully.','data'=>$user->languages_known]); 
	}

	// update driver language api 
	public function get_driver_info() 
	{ 
		$user = Auth::user();
		$userdata=User::select('languages_known','address','additional_info')->where('id',$user->id)->first();

		return response()->json(['status'=>true,'data' => $userdata]); 
	} 

	// update driver language api 
	// public function get_driver_languages() 
	// { 
	// 	$user = Auth::user();
	// 	$userdata=User::select('languages_known')->where('id',$user->id)->first();
		
	// 	return response()->json(['status'=>true,'data' => $userdata]); 
	// } 

	public function update_driver_address(Request $request) 
	{ 
		$input=$request->all();
		$user    = Auth::user();

		$validator = Validator::make($input,[
			'address' => "required",
		]);
		if ($validator->fails()) {
			return response()->json(['status'=>false,'message' => $validator->messages()->first()]);
		}

		$user->living_address=$request->get('address');
		$user->save();

		return response()->json(['status'=>true,'message' => 'Information updated successfully.','data'=>$input]); 
	} 

	// driver address api 
	// public function get_driver_address() 
	// { 
	// 	$user = Auth::user();
	// 	$userdata=User::select('address')->where('id',$user->id)->first();
		
	// 	return response()->json(['status'=>true,'data' => $userdata]); 
	// } 
	// update driver address api 

		public function update_driver_additional_info(Request $request) 
	{ 
		$input=$request->all();
		$user    = Auth::user();

		$validator = Validator::make($input,[
			'additional_info' => "required|string",
		]);
		if ($validator->fails()) {
			return response()->json(['status'=>false,'message' => $validator->messages()->first()]);
		}

		$user->additional_info=$request->get('additional_info');
		$user->save();

		return response()->json(['status'=>true,'message' => 'Information updated successfully.','data'=>$input]); 
	} 

	// driver additional info api 
	// public function get_driver_additional_info() 
	// { 
	// 	$user = Auth::user();
	// 	$userdata=User::select('additional_info')->where('id',$user->id)->first();
		
	// 	return response()->json(['status'=>true,'data' => $userdata]); 
	// } 
	//save drivers licence
	// public function saveDriverLicence(Request $request){
 //        $validator = Validator::make($request->all(),[
 //            'photo_id_type'   => 'required|in:licence',
 //            'doc_front_pic'   => 'required|file|mimes:jpg,jpeg,png,bmp,pdf',
 //            'doc_back_pic'    => 'required|file|mimes:jpg,jpeg,png,bmp,pdf',
 //            'expiry_date'    => 'required',
 //        ]);

 //        if ($validator->fails()) {
 //            return response()->json(['status'=>false,'message' => $validator->messages()->first()]);
 //        }

 //        $user    = Auth::user();

 //        if ($user->is_profile_verified == '1') {
 //            return response()->json(['status'=>false,'message' => 'You cann\'t change your Photo ID. Please contact support team.']);
 //        }

 //        $user_id = $user->id;
 //        $photo_id_type = $request->get('photo_id_type');
 //        $expiry_date = $request->get('expiry_date');
 //        $disk    = Storage::disk('public');

 //        $userDetails = [];
 //        $userDetails['doc_type'] = 'licence'; 
 //        $userDetails['verified'] = '4';
 //        $userDetails['photo_id_type'] = $photo_id_type;
 //        $userDetails['expiry_date'] = $expiry_date;
 //        $userDetails['updated_at'] = date('Y-m-d h:i:s') //Carbon::now('UTC');

 //        if ($request->hasFile('doc_front_pic')) {
 //            $doc = $request->file('doc_front_pic');
 //            $frontPicName = $doc->hashName();
 //            $disk->put('images/users/'.$user_id.'/licence/',$doc);
 //            $userDetails['front_side_pic'] = $frontPicName;
 //        }

 //        if ($request->hasFile('doc_back_pic')) {
 //            $doc = $request->file('doc_back_pic');
 //            $backPicName = $doc->hashName();
 //            $disk->put('images/users/'.$user_id.'/licence/',$doc);
 //            $userDetails['back_side_pic'] = $backPicName;
 //        }

 //        Auth::user()->details()->updateOrCreate(['user_id'=>$user_id,'doc_type'=>'licence'],$userDetails);
 //        return response()->json(['status'=>true,'message' => 'Photo ID saved successfully']);
 //    }

 //    //get licence
 //     public function getDriverLicence(Request $request){
 //        $user = Auth::user();
 //        $data = $user->details()->where('doc_type','licence')->select('front_side_pic','back_side_pic','photo_id_type','expiry_date')->first();

 //        if ($data != null) {
 //            $data['base_url'] = asset('storage/images/users/'.$user->id.'/licence');
 //        }else{
 //            $data = new stdClass();
 //        }

 //        return response()->json(['status'=>true,'message'=>'Licence retrieved successfully','photo_id_info'=>$data]);
 //    }
}
