<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\v3\Message;
use App\Models\v3\User;
use App\Models\v3\ChatRoom;
use App\Events\SendMessageChannel;
use Validator;
use Auth;
use DB;

class MessageControllerFair extends Controller
{

    public function sendMessage(Request $request){

    	/** validate the data **/
    	$validator = Validator::make($request->all(),[
			"receiver_id" => "required|exists:users,id",
			"message"	  => "required"
		]);

		if ($validator->fails()) { 
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
		}

		/** get the parameters **/
		$user 			 	 = Auth::user();
		$senderId 			 = $user->id;
		$receiverId 	 	 = $request->get('receiver_id');
		
		$data 				 = new Message(); 
		$data['sender_id'] 	 = $senderId;
		$data['receiver_id'] = $receiverId;
		$data['message'] 	 = $request->get('message');
		
		/** get the chat room where the auth user and the receiver was involved **/
		$chatRoom = $user->chat_rooms()
				->whereHas('room_participants',function($query) use ($receiverId){
					$query->where('user_id',$receiverId);
				})
				->first();

		/** If involvement in chat room found then add the message under that chat room else create a room and then add the message under that chat room **/
		if ($chatRoom) {
    		$chatRoom->messages()->save($data);
		}else{
			$chatRoom = ChatRoom::create(['chat_room_id' => uniqid()]);
			$chatRoom->room_participants()->attach([$senderId,$receiverId]);
    		$chatRoom->messages()->save($data);
		}

		/** return the response **/
    	return response()->json([
    		'status'=>true,
    		'message'=>'Message has been send successfully.'
    	]);
    }

    public function getUserInbox(Request $request){

    	/** get the parameters **/
		$user 		 = Auth::user();
		$senderId  	 = $user->id;

		/** get the user's chat rooms **/
		$chatRooms = $user->chat_rooms()
					->with(['last_message' => function($query) use ($senderId){
						$query->select('id','sender_id','receiver_id','message','created_at','chat_room_id');
					},'room_participants' => function($query) use ($senderId){ 
						$query->select('room_participants.id','first_name','last_name','profile_pic');
					}])
					->get();
		
		$chatRooms->map(function($chatRoom,$index) use ($senderId){
			$chatRoom->room_participants->map(function($participant,$index) use ($senderId){
				$participant->me = ($participant->id == $senderId)?true:false;
				return $participant;
			});
			return $chatRoom;
		});

		/** return the response **/
		return response()->json([
			'status'		=> true,
			'message'		=> 'User\'s message inbox has been retrieved successfully.' ,
			'data'			=> $chatRooms
		]);
    }

 	public function getDetailMessages(Request $request){
    	
    	/** validate the data **/
    	$validator = Validator::make($request->all(),[
			"chat_room_id" => "required|exists:chat_rooms,id",
			"page"	  	   => "required|numeric|min:1"
		]);

		if ($validator->fails()) { 
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
		}

    	/** get the parameters **/
		$user 		 = Auth::user();
		$senderId  	 = $user->id;
		$chatRoomId  = $request->get('chat_room_id');
		$pageNo 	 = $request->get('page');

		/** get the room **/
		$chatRoom 	  		= ChatRoom::select('id','chat_room_id','created_at')->find($chatRoomId);
		
		/** get the room participants **/
		// $roomParticipants 	= $chatRoom->room_participants()
		// 						->select('room_participants.id','first_name','last_name')
		// 						->get()
		// 						->map(function($participant,$index) use ($senderId){
		// 							$participant->me = ($participant->id == $senderId)?true:false;
		// 							return $participant;
		// 						});

		/** get the room's messages **/
		$chatRoomMessages 	= $chatRoom->messages()->select('id','sender_id','receiver_id','message','created_at')->paginate(20);

		/** provide the flag which represent who is sender and who is receiver **/
		// $chatRoomMessages->getCollection()->transform(function($message,$index) use ($senderId){
		// 	$message->mine = ($message->sender_id == $senderId)?true:false; 
		// 	return $message;
		// });

		/** check if there is anything remaining data available for next paging **/
		$hasNextPage = ($pageNo < $chatRoomMessages->lastPage())?true:false;

    	/** return the response **/
    	return response()->json([
    		'status'		=> true,
    		'message'		=> 'Messages are retrieved successfully.',
    		'has_next_page'		=> $hasNextPage,
    		'room_info'			=> $chatRoom,
    		// 'room_participants'	=> $roomParticipants,
    		'room_messages' 	=> $chatRoomMessages->all()
    	]);
    }
}
