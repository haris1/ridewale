<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\v3\User; 
use App\Models\v3\Driver; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use Stripe\Stripe;
use Stripe\Customer;
use App\Models\v3\Payment;
use Illuminate\Support\Facades\Hash;
use App\Used_document;
use App\Country;
use App\Notifications\VarificationCode;
use App\CarType;
use App\CarModel;
use App\OfficeInformation;
use App\Maintanance;
use GuzzleHttp\Client;
use App\Work_city;
use App\Work_country;
use App\Work_state;


class UserController extends Controller
{
	public $successStatus = 200;

	// login api 
	public function login(){ 
		
		$maintanance=Maintanance::select('android_version','ios_version','maintanance_mode')->first();
		if($maintanance->maintanance_mode){
			return response()->json([
				'status'=>false,
				'message'=>'Application is under Maintainance.',
				'maintanance_mode' => true
			]);            
		}

		if(Auth::attempt(['mobile_no' => request('mobile_no'), 'password' => request('password')])){ 
			$user = Auth::user(); 
			// if($user->is_approved == '0'){
			// 	return response()->json(['status'=>false,'message'=>'You cann\'t login. Your account suspended or not approved. Please, Contact admin.']);            
			// }
			$success['token'] =  $user->createToken('ridechimp')->accessToken; 
			$success['user_detail'] =  $this->acountData(); 
			$success['base_url'] = asset('storage/images/users/'.$user->id.'/profile_pic').'/';
			$success['android_version'] =  $maintanance->android_version;
			$success['ios_version'] =  $maintanance->ios_version;
			/** save firebase ids **/
			if(request('firebase_android_id')){
				$user->firebase_android_id = request('firebase_android_id');
			}
			if(request('firebase_ios_id')){
				$user->firebase_ios_id = request('firebase_ios_id');
			}
			$user->save();
			return response()->json(['status'=>true,'message' => 'Login Successfully','data'=>$success]); 
		} 
		else{ 
			return response()->json(['status'=>false,'message' => 'Invalid credential.']); 
		} 
	}

	//Register api 
	public function register(Request $request) 
	{ 
		$maintanace=Maintanance::select('android_version','ios_version','maintanance_mode')->first();
		if($maintanace->maintanance_mode){
			return response()->json([
				'status'=>false,
				'message'=>'Application is under Maintainance.',
				'maintanance_mode' => true
			]);            
		}
		// print_r($maintanace->android_version);die();
		$input = $request->all();
		Stripe::setApiKey(config('services.stripe.secret'));

		$validator = Validator::make($input,[
			'first_name' => "required|string|max:50",
			'last_name' => "required|string|max:50",
			'email' => "required|string|email|max:255|unique:users",
			'password' => "required|string|min:6",
			'dob' => "required | date | before : -18 years",
			'gender' => "required",
			'country_code'=>"required|exists:countries,code",
			'mobile_no'=>"required|unique:users"
		],[
			"dob.before" => "You are not 18 year Old!"
		]);

		if ($validator->fails()) { 
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
		}		

		$input['password'] = bcrypt($input['password']); 
		$customer = Customer::create(array(
			'email' => $input['email'],
		));
		$input['stripe_id'] =$customer->id;
		$input['profile_verified'] ='false';
		// $input['is_approved'] = '1';

		unset($input['referral_code']);
		// print_r($input);die();
		$user = User::create($input); 
		$acct = \Stripe\Account::create(array(
			"country" => "CA",
			"type" => "custom",
			"email"=>$user->email
		));

		$user->driver()->create(array('user_id'=>$user->id,'stripe_id'=>$acct->id));
		// Driver::create(array('user_id'=>$user->id));
		Auth::attempt(['mobile_no' => request('mobile_no'), 'password' => request('password')]); 
		$success['token'] =  $user->createToken('ridechimp')->accessToken; 
		$success['android_version'] =  $maintanace->android_version;
		$success['ios_version'] =  $maintanace->ios_version;
		$success['user_detail'] =  $this->acountData(); 
		$success['base_url'] = asset('storage/images/users/'.$user->id.'/profile_pic').'/';
		return response()->json(['status'=>true,'message' => 'Register Successfully','data'=>$success]); 
	}

	//logout
	public function logout(Request $request)
	{
		$user = Auth::user(); 
		$user->firebase_android_id = '';
		$user->firebase_ios_id = '';
		$user->save();

		$request->user()->token()->revoke();
		return response()->json(['status'=>true,
			'message' => 'Successfully logged out'
		]);
	}

	// acountData api 
	public function acountData() 
	{ 
		$user = Auth::user();
		// print_r($user->address);
		// exit;
		if ($user->address && is_array($user->address)) {
			$user->address = $user->address['street'].' '.$user->address['city'].' '.$user->address['state'].' '.$user->address['country'].' '.$user->address['pincode']; 
		}
		$userdata=User::with('driver')->where('id',$user->id)->first();
		// if($user->active_mode == 'driver'){
		$carDetail=[];

		if(count($user->driver->car_driver) > 0){
			foreach ($user->driver->car_driver as $key => $cars) {
				$car_logo=$cars->car_model->car_type['name'];
				$carDetail[]=array(
					'vehicle_id'=>$cars->id,
					'company'=>$cars->car_model->car_type['name'],
					'type'=>$cars->car_model->car_type['name'],
					'model'=>$cars->car_model['model_name'],
					'capacity'=>$cars->car_model['capacity'],
					'logo'=>asset('storage/type_logo/'.$cars->car_model->car_type['logo']),
				);
			}
		}
		
		$userdata['cars']=$carDetail;
		$transactions= 0;
		// Payment::join('trips',function($requests){
		// 	$requests->on('payments.trip_id','=','trips.id');
		// })
		// ->where('trips.driver_id',$user->driver->id)
		// ->where('trips.status','completed')
		// ->whereHas('transaction')
		// ->where('payments.status','!=','3')
		// // ->where('payments.withdrow_status','0')
		// ->get()
		// ->sum('driver_fees');

		$total_withdraws=$user->driver->withdraws()->sum('amount');
		$userdata->withdrow=($transactions>0)?number_format($transactions-$total_withdraws,2):'0.00';
		// }
		Stripe::setApiKey(config('services.stripe.secret'));
		$userdata->has_driver_payment_info = false;
		if($userdata->driver['stripe_id'] != ''){
			$account = \Stripe\Account::retrieve($user->driver['stripe_id']);
			$status=false;
			// print_r($account);die();
			if($account->legal_entity != ''){

				$status=($account->legal_entity->verification->status == 'verified')?true:false;
			}
			$userdata->has_driver_payment_info = $status;
		}
		$userdata->has_user_payment_info=false;
		if($userdata->stripe_id != ''){

			$customerCards = Customer::retrieve($user->stripe_id);
			$userdata->has_user_payment_info = (count($customerCards->sources['data']) > 0)?true:false;			
		}
		unset($user->driver);
		return array_map(function ($value) {
			return $value === null ? '' : $value;
		},$userdata->toArray()); 
	} 

	// acountData api 
	public function profileData() 
	{ 
		$user = Auth::user();
		$userdata=User::with('driver')->where('id',$user->id)->first();
		if($user->active_mode == 'driver'){
			$carDetail=[];

			if(count($user->driver->car_driver) > 0){
				foreach ($user->driver->car_driver as $key => $cars) {
					$carDetail[]=array(
						'vehicle_id'=>$cars->id,
						'company'=>$cars->car_model->car_type['name'],
						'type'=>$cars->car_model->car_type['name'],
						'model'=>$cars->car_model['model_name'],
						'capacity'=>$cars->car_model['capacity'],
						'logo'=>asset('storage/type_logo/'.$cars->car_model->car_type['logo']),
					);
				}
			}
			
			$userdata['cars']=$carDetail;
			$userdata->driver_account_balance=0; 
			$transactions=Payment::
			join('trips',function($trips){
				$trips->on('payments.trip_id','=','trips.id');
			})
			->where('trips.driver_id',$user->driver->id)
			->where('trips.status','completed')
			->where('payments.withdrow_status','0')
			->get();
			
			$user->driver_account_balance=number_format($transactions->sum('driver_fees'),2);
		}
		unset($user->driver);
		$userdata=array_map(function ($value) {
			return $value === null ? '' : $value;
		},$userdata->toArray());
		return response()->json(['status'=>true,'message'=>'Information retrived successfully.','data' => $userdata]); 
	} 

	// driver document status api 
	public function driverDocuments() 
	{ 
		$user = Auth::user();

		$userDocs = Used_document::where('ownable_id',$user->driver->id)->pluck('expiry_date','document_id')->toArray();

		$documents = collect([
			'1' => ['name' => 'driver_licence'],
			'2'	=> ['name' => 'proof_of_work_eligibility'],
			'3' => ['name' => 'criminal_certificate'],
			'4' => ['name' => 'profile_photo'],
		]);
		$uploadedDocs = array_keys($userDocs);
		$docs = [];
		foreach ($documents as $key => $doc) {
			if(in_array($key, $uploadedDocs)){
				$docs[$doc['name']] = array('status'=>"provided",'data'=>$userDocs[$key]);
				if($userDocs[$key] < date('Y-m-d')){
					$docs[$doc['name']] = array('status'=>"expired",'data'=>$userDocs[$key]);
				}
			}else{
				$docs[$doc['name']] = array('status'=>"not_provided",'data'=>'');
			}
			if($key == '4' && $user->profile_pic != null && file_exists(storage_path().'/app/public/images/users/'.$user->id.'/profile_pic/'.$user->profile_pic)){
				$docs[$doc['name']] = array('status'=>"provided",'data'=>asset('storage/images/users/'.$user->id.'/profile_pic/'.$user->profile_pic));
			}
		}
		$response = [];
		$response['driver_docs'] = $docs;
		$car_drivers=$user->driver->car_driver;

		if(count($car_drivers) > 0){
			$docs = [];
			$i=0;
			foreach ($car_drivers as $key => $car_driver) {

				$carDocs = Used_document::where('ownable_id',$car_driver->id)->pluck('expiry_date','document_id')->toArray();

				$documents = collect([
					'4'  => ['name' => 'vehicale_registration'],
					'5'  => ['name' => 'vehicle_insurance']
				]);

				$uploadedDocs = array_keys($carDocs);
				
				foreach ($documents as $key => $doc) {
					$docs[$i]['car_detail']=array(
						'car_no'=>$car_driver->car_number,
						'model'=>$car_driver->car_model['model_name'],
						'company'=>$car_driver->car_model->car_company['name'],
						'car_type'=>$car_driver->car_model->car_type['name'],
					);
					if(in_array($key, $uploadedDocs)){
						if($carDocs[$key] < date('Y-m-d')){
							$docs[$i][$doc['name']] = array('status'=>"expired",'expiry_date'=>$carDocs[$key]);
						}
					}else{
						$docs[$i][$doc['name']] = array('status'=>"not_provided",'expiry_date'=>'');
					}
				}
				$i++;
			}
		}

		$response['car_docs'] = $docs;

		// return $response;
		return response()->json(['status'=>true,'message'=>'Information retrived successfully.','data' => $response]); 
	} 


	// vehicle info api 
	public function vehicle_info() 
	{ 
		$user = Auth::user();
		$userdata=Driver::select('id')->with(['car_driver'=> function ($query) {
			$query->select('id','driver_id','car_model_id','color','year','rules_and_regulations','allowed_things');
		}])->where('user_id',$user->id)->first();
		
		$userdata->car_driver=collect($userdata->car_driver)->map(function ($cars)  {
			$cars->company=$cars->car_model->car_company['name'];
			$cars->type=$cars->car_model->car_type['name'];
			$cars->model=$cars->car_model['model_name'];
			$cars->capacity=$cars->car_model['capacity'];
			$cars->color=$cars->color;
			$cars->year=$cars->year;
			$cars->carPhoto=$cars->carPhotoes;
			$cars->rules_and_regulations=$cars->rules_and_regulations;
			$cars->allowed_things=$cars->allowed_things;
			unset($cars->car_model);
			unset($cars->carPhotoes);
		});
		$userdata->base_url=asset('storage/driver_car').'/'.$userdata->id.'/';

		return response()->json(['status'=>true,'message'=>'Vehicle info retrived Successfully.','data' => $userdata]); 
	} 

	// switch account api 
	public function switch_to_driver_mode(Request $request) 
	{ 
		$input = $request->all();
		$validator = Validator::make($input,[
			'active_mode' => "required",
		]);

		if ($validator->fails()) { 
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
		}		
		$user = Auth::user();
		$user->active_mode=$request->get('active_mode');
		if($user->active_mode=='user'){
			$user->save();			
		}elseif($user->is_driver == '1' && $user->active_mode='driver'){
			$user->save();			
		}
		// if($request->get('active_mode') == 'driver' && !$user->driver->is_approved){
		// 	return response()->json(['status'=>false,'message'=>'You cann\'t login. Your account suspended or not approved. Please, Contact admin.']);            
		// }
		$data=$this->acountData();
		return response()->json(['status'=>true,'message'=>'Account switched successfully.','data' => $user]); 
	} 

	public function change_password(Request $request){ 

		$input = $request->all();
		$validator = Validator::make($input,[
			'old_password' => "required",
			'new_password' => "required",
		]);

		if ($validator->fails()) { 
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
		}		
		$user = Auth::user();

		if (Hash::check($request->get('old_password'), $user->password))
		{
			$user->password = bcrypt($request->get('new_password'));
			$user->save();
			return response()->json(['status'=>true,'message'=>'Password change successfully.']);
		}

		return response()->json(['status'=>false,'message'=>'Old Password Does not match!']);
	}

	// public function update_email(Request $request){ 

	// 	$user = Auth::user();
	// 	$validator = Validator::make($request->all(),[
	// 		'email' => "required|string|email|max:255|unique:users,email,".$user->id
	// 	]);

	// 	if ($validator->fails()) { 
	// 		return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
	// 	}		

	// 	$user->email = $request->get('email');
	// 	$user->save();
	// 	return response()->json(['status'=>true,'message'=>'Email updated successfully.']);
	// }

	public function update_mobileno(Request $request){ 

		$user = Auth::user();
		$validator = Validator::make($request->all(),[
			'mobile_no' => "required|numeric|unique:users,mobile_no,".$user->id,
			'country_code'=>"required|exists:countries,code",
		]);

		if ($validator->fails()) { 
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
		}		

		$user->mobile_no = $request->get('mobile_no');
		$user->country_code = $request->get('country_code');
		$user->save();
		return response()->json(['status'=>true,'message'=>'Mobile No updated successfully.']);
	}

	//is mobile exist
	public function isMobileExists(Request $request)
	{
		$validator = Validator::make($request->all(),[
			'mobile_no' => "required|unique:users,mobile_no",
		]);

		if ($validator->fails()) { 
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
		}
		return response()->json(['status'=>true,'message'=>'You can use this phone number']);            
	}

	public function isEmailExists(Request $request)
	{
		$validator = Validator::make($request->all(),[
			'email' => "required|unique:users,email",
		]);

		if ($validator->fails()) { 
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
		}
		return response()->json(['status'=>true,'message'=>'You can use this email']);            
	}

	// send email verification code
	public function sendVarificationCode(Request $request)
	{   
		$user = Auth::user();
		$validator = Validator::make($request->all(),[
			'email' => "required|string|email|max:255|unique:users,email,".$user->id
		]);
		if ($validator->fails()) {
			$message = array_values(current($validator->messages()))[0][0];
			return response()->json(['status'=>false,'message'=>$message]);
		}
		// $user=User::where(['email'=>$request->get('email')])->first();
		if($user != ''){
			$vcode = rand(100000,999999);
			$user->email=$request->get('email');
			$user->profile_verified='false';
			$user->otp=$vcode;
			$user->save();
			// $EmailVarification = User::update(['email'=>$request->get('email')],['otp'=>$vcode]);

			$user->notify(new VarificationCode($vcode));
			return response()->json(['status'=>true,'message'=>"Verification code has been sent to your email."]);

		}
		return response()->json(['status'=>false,'message'=>"Email doesn't exist."]);

	}

	// email verification
	public function emailVarification(Request $request)
	{
		// $user = Auth::user();
		$validator = Validator::make($request->all(),[
			'email' => "required|string|email",//|unique:users,email,".$user->id,
			'verification_code'=>"required"
		]);
		if ($validator->fails()) {
			$message = array_values(current($validator->messages()))[0][0];
			return response()->json(['status'=>false,'message'=>$message]);
		}

		$email = $request->get('email');
		$vcode = $request->get('verification_code');

		$checkEmail = User::select("otp")
		->where(['email'=>$email,'otp'=>$vcode])
		->first();

		if ($checkEmail == "") {
			return response()->json(['status'=>false,'message'=>"Please provide the valid OTP."]);
		}

		User::where(['email'=>$email])
		->update(array('profile_verified'=>'true','otp'=>'','email_verified_at'=>date('Y-m-d h:i:s')));

		return response()->json(['status'=>true,'message'=>"Congrats! Thank you for varification."]);
	}

	public function CountryList(){
		$countries=Country::select('dialing_code')->get();
		return response()->json(['status'=>true,'message'=>'Information retrived.','data'=>$countries]);            
	}

    //OFFICE INFOMATION
	public function office_info(Request $request){

		$validator = Validator::make($request->all(),[
			'city' => "required"
		]);

		if ($validator->fails()) { 
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
		}
		$user = Auth::user();
		$officeInfo = OfficeInformation::where('city',$request->get('city'))
		->first();
		if ($officeInfo) {
			$location_hours=explode(',', $officeInfo['location_hours']);
			$inquiry_hours=explode(',', $officeInfo['inquiry_hours']);
			
			$officeInfo['location_hours']=$location_hours[0].'-'.$location_hours[1].' : '.$location_hours[2].' - '.$location_hours[3];
			$officeInfo['inquiry_hours']=$inquiry_hours[0].'-'.$inquiry_hours[1].' : '.$inquiry_hours[2].' - '.$inquiry_hours[3];
			if ($officeInfo) {
				return response()->json(['status'=>true,'message'=>'Office Information','data'=>$officeInfo]);
			} else {
				return response()->json(['status'=>false,'message'=>'Sorry! RideChimp currently not available in your city.','data'=>['id'=>0,
					'city'=>'',
					'country'=>'',
					'postal_code'=>'',
					'longitude'=>'',
					'latitude'=>'',
					'full_address'=>'',
					'location_hours'=>'',
					'inquiry_hours'=>'',
					'phone'=>'',
					'email'=>'',
					'created_at'=>'',
					'updated_at'=>'',
				]]);
			}
		} else {
			return response()->json(['status'=>false,'message'=>'Sorry! RideChimp currently not available in your city.','data'=>['id'=>0,
				'city'=>'',
				'country'=>'',
				'postal_code'=>'',
				'longitude'=>'',
				'latitude'=>'',
				'full_address'=>'',
				'location_hours'=>'',
				'inquiry_hours'=>'',
				'phone'=>'',
				'email'=>'',
				'created_at'=>'',
				'updated_at'=>'',
			]]);
		}
	}

	public function app_setting(){

		$maintanace=Maintanance::select('android_version','ios_version','maintanance_mode')->first();

		if($maintanace != ''){
			return response()->json(['status'=>true,'message'=>'Information retrieved successfully.','data'=>$maintanace]);
		}
		return response()->json(['status'=>false,'message'=>'Information not provided.']);

	}

	public function refreshToken(Request $request)
	{

		$validator = Validator::make($request->all(),[
			'mobile_no' => "required|numeric",
			'password'  => "required",
			'firebase_android_id' => "nullable",
			'firebase_ios_id'     => "nullable",
		]);

		if ($validator->fails()) { 
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
		}		

		$mobileNumber = $request->get('mobile_no');
		$password 	  = $request->get('password');
		
		if(Auth::attempt(['mobile_no' => $mobileNumber, 'password' => $password])){ 
			$user  = Auth::user(); 
			$token =  $user->createToken('ridechimp')->accessToken; 
			
			/** save firebase ids **/
			if($request->has('firebase_android_id')){
				$user->firebase_android_id = $request->get('firebase_android_id');
			}
			if($request->has('firebase_ios_id')){
				$user->firebase_ios_id = $request->get('firebase_ios_id');
			}
			$user->save();

			return response()->json([
				'status'  => true,
				'message' => 'Token has been generated successfully.',
				'token'   => $token
			]); 
		}
		
		return response()->json([
			'status'  => false,
			'message' => 'Sorry, Wrong credentials provided.',
			'token'   => ''
		]); 
	}


	public function refreshToken1(Request $request)
	{

		
		$user = Auth::user(); 
			// if($user->is_approved == '0'){
			// 	return response()->json(['status'=>false,'message'=>'You cann\'t login. Your account suspended or not approved. Please, Contact admin.']);            
			// }
		$success['token'] =  $user->createToken('ridechimp')->accessToken; 
			
		$token = $request->get('token');
		// print_r($token);
		// exit;
		$http = new \GuzzleHttp\Client;

		$response = $http->post('http://ridechimp.co/rcpanel/oauth/token', [
			'form_params' => [
				'grant_type' => 'refresh_token',
				'refresh_token' => 'QbEtHmbiJbxMwYsfRY92GNHHeAZffWYChsnptf0w',
				'client_id' => '11', //rcclient
				'client_secret' => 'LY40viQjHZIkKOdS0TAVtFFtfJlda1AeuFAop2oX',
				'scope' => '*',
			],
		]);
		return response()->json(['status'=>false,'message'=>'Token Refreshed.','data'=>$response]);
	}


	public function country_info(){
		$country = Work_state::select('id','work_country_id','name')
							->with(['work_country'=>function($query){
								$query->select('id','name'); 
							},'work_cities'=>function($query){
								$query->select('id','work_state_id','name'); 
							}])
							->get()
							->map(function($query){
								$query->country =$query->work_country->name;
								$query->state =$query->name;
								unset($query->id);
								unset($query->work_country_id);
								unset($query->name);
								unset($query->work_country);
								return $query;  
							});
							
		return response()->json([
							'status'=>true,
							'message'=>'data retrieval',
							'data'=>$country
						]);
	}
}
