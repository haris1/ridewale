<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\v3\User;
use App\Models\v3\Trip;
use Auth;
use App\Payment;

class NotificationController extends Controller
{
	public function getUserNotifications(Request $request){

		$page 					= $request->page;
		$notifications 			= Auth::user()
		->notifications()
		->whereIn('type',[
			'App\Notifications\BookingNotification',
			'App\Notifications\StartTripNotification',
			'App\Notifications\CompleteTripNotification',
			'App\Notifications\CancelTripNotification',
		])
		->paginate(8);

		$trip_ids=[];
		foreach ($notifications as $index => $notification) {
			if (count($notification->data) > 0) {
				$trip_ids[] 	= $notification->data['trip_id'];
			}
		}

		$trip_ids 				= array_values(array_unique($trip_ids));

		$trips  				= Trip::whereIn('id',$trip_ids)
		->with(['driver' => function($query){
			$query->select('id','user_id');
			$query->with(['user' => function($query){
				$query->select('id','first_name','last_name','profile_pic');
			}]);
		},'payments'=>function($query){
			$query->with(['user' => function($query){
				$query->select('id','first_name','last_name','profile_pic');
			}]);
		},
	])
		->with(['from_city'=> function ($query) {
			$query->select('id','name');
		}])
		->with(['to_city'=> function ($query) {
			$query->select('id','name');
		}])
		->get();

		$preparedArrayForTrip	= [];

		foreach ($trips as $key => $trip) {
			$preparedArrayForTrip[$trip->id] = $trip;
		}

		$responseNotification 	= [];

		$notifications->getCollection()->transform(function($notification,$index) use ($preparedArrayForTrip,$trip_ids){
			$notification->markAsRead();
			$data=$notification->data;

			if (isset($notification->data['trip_id']) && in_array($notification->data['trip_id'], $trip_ids) ) {

				$trip_id = $notification->data['trip_id'];

				$drivername     = $preparedArrayForTrip[$trip_id]->driver->user->first_name.' '.$preparedArrayForTrip[$trip_id]->driver->user->last_name;
				$payment=Payment::where('id',(int)$data['booking_id'])->first();

				$notification                 = $notification->toArray();
				$notification['trip_id']      = $trip_id;
				$notification['status']      = $payment->status;

				$notification['drivername']   = $drivername;
				$notification['city']      	  = $preparedArrayForTrip[$trip_id]->from_city->name.' to '.$preparedArrayForTrip[$trip_id]->to_city->name;
				$notification['trip_date']    = date('d M, Y',strtotime($preparedArrayForTrip[$trip_id]->trip_date));

			}
			// echo $notification['type'];die()
			switch($notification['type']) {

				case 'App\Notifications\BookingNotification':
				$notification['message']          = 'Your trip booked successfully from '.$notification['city'];
				$notification['tag']              = 'UserTripBooking';
				break;

				case 'App\Notifications\StartTripNotification':
				$notification['message']          = 'Your trip started from '.$notification['city'].' at '.$notification['trip_date'];
				$notification['tag']              = 'UserTripStart';
				break;

				case 'App\Notifications\CompleteTripNotification':
				$notification['message']          = 'Your trip completed from '.$notification['city'].' at '.$notification['trip_date'];
				$notification['tag']      		  = 'UserTripCompleted';
				break;

				case 'App\Notifications\CompleteTripNotification':
				$notification['message']          = 'Your trip from '.$notification['city'].' at '.$notification['trip_date'].' cancelled successfully.';
				$notification['tag']      		  = 'UserTripCancel';
				break;

			}

			return $notification;

		});

		return response()->json(['status'=>true,'message'=>'Notification Retrived.','data'=>$notifications]);
	}

	public function getDriverNotifications(Request $request){

		$page = $request->page;

		$notifications 	= Auth::user()
		->notifications()
		->whereIn('type',[
			'App\Notifications\BookingNotification',
			'App\Notifications\StartTripNotification',
			'App\Notifications\CompleteTripNotification',
			'App\Notifications\CancelTripNotification',
			'App\Notifications\WithdrawNotification',
		])
		->paginate(8);

		$trip_ids	= [];
		foreach ($notifications as $index => $notification) {
			if (count($notification->data) > 0 && $notification->type != 'App\Notifications\WithdrawNotification') {
				$trip_ids[] = $notification->data['trip_id'];
			}
		}

		$trip_ids 	= array_values(array_unique($trip_ids));

		$trips  	= Trip::whereIn('id',$trip_ids)
		->with(['driver' => function($query){
			$query->select('id','user_id');
			$query->with(['user' => function($query){
				$query->select('id','first_name','last_name');
			}]);
		},'payments'=>function($query){
			$query->with(['user' => function($query){
				$query->select('id','first_name','last_name');
			}]);
		}])
		->get();

		$preparedArrayForTrip	= [];

		foreach ($trips as $key => $trip) {
			$preparedArrayForTrip[$trip->id] = $trip;
		}

		$responseNotification 	= [];

		$notifications->getCollection()->transform(function($notification,$index) use ($preparedArrayForTrip,$trip_ids){
			$notification->markAsRead();
			$data=$notification->data;
			$type=$notification->type;
			$notification  = $notification->toArray();
			$username='';
			$seats=0;
			if (isset($notification['data']['amount']) && $type == 'App\Notifications\WithdrawNotification'){
				$notification['amount']      	 = $notification['data']['amount'];
			}
			if (isset($notification['data']['trip_id']) && in_array($notification['data']['trip_id'], $trip_ids) && $type != 'App\Notifications\WithdrawNotification' ) {

				$trip_id = $notification['data']['trip_id'];

				$notification['trip_id']      	  = $trip_id;
				$notification['status']      	  = Trip::find($trip_id)->status;

				$username=Payment::where('id',(int)$data['booking_id'])->with('user')->first();
				$seats=$username->total_person;	
				$username=$username->user['first_name'].' '.$username->user['last_name'];
				$notification['seats']      	  = $seats;
				$notification['username']      	  = $username;

				$notification['city']      		  = $preparedArrayForTrip[$trip_id]->from_city->name.' to '.$preparedArrayForTrip[$trip_id]->to_city->name;
				$notification['trip_date']        = date('d M, Y H:i a',strtotime($preparedArrayForTrip[$trip_id]->trip_date));

			}

			switch($notification['type']) {

				case 'App\Notifications\BookingNotification':
				$notification['message']          = $notification['username'].' has booked '.$notification['seats'].' seats for trip '.$notification['city'];
				$notification['tag']              = 'DriverTripBooking';
				break;

				case 'App\Notifications\StartTripNotification':
				$notification['message']          = 'Your trip started for '.$notification['city'];
				$notification['tag']              = 'DriverTripStart';
				break;

				case 'App\Notifications\CompleteTripNotification':
				$notification['message']          = 'Your trip completed for '.$notification['city'];
				$notification['tag']      		  = 'DriverTripCompleted';
				break;

				case 'App\Notifications\CancelTripNotification':
				$notification['message']          = $notification['username'].' has cancel booking for trip from '.$notification['city'];
				$notification['tag']      		  = 'DriverTripCancel';
				break;

				case 'App\Notifications\WithdrawNotification':
				$notification['message']          = 'Your withdraw request for $'.$notification['amount'].' completed successfully.';
				$notification['tag']      		  = 'WithdrawSuccess';
				break;

			}

			return $notification;

		});

		return response()->json(['status'=>true,'message'=>'Notification Retrived.','data'=>$notifications]);
	}
}
