<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use App\Models\v3\User;
use App\Country;
use App\PasswordReset;
use Validator;

class PasswordResetController extends Controller
{

	public function isMobileRegisted(Request $request)
	{
		$validator = Validator::make($request->all(),[
			'mobile_no' => "required|unique:users,mobile_no",
		]);

		if ($validator->fails()) { 
			$user=User::where('mobile_no',$request->get('mobile_no'))->with('user_country')->first();
			if($user->user_country != ''){
				return response()->json(['status'=>true,'message'=>'You can use this phone number','data'=>$user->user_country->dialing_code]);            
			}
			return response()->json(['status'=>true,'message'=>'You can use this phone number','data'=>'+91']);            
		}
		return response()->json(['status'=>false,'message'=>'This number is not registered.']);            
	}

	public function forgotPassword(Request $request)
	{
		$validator = Validator::make($request->all(),[
			'email' => "required|email",
		]);
		if ($validator->fails()) { 
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
		}

		$user = User::where('email', $request->email)->first();
		if (!$user){
			return response()->json(['status'=>false,'message' => 'We cant find a user with that e-mail address.']);
		}
		$otp = $user->otp;

		if($user->otp == null){
			$otp = rand(100000,999999);
			$user->otp = $otp;
			$user->save();
		}
		
		$user->notify(
			new PasswordResetRequest($otp)
		);

		return response()->json(['status'=>true,'message' => 'We have e-mailed your password reset OTP!']);
	}

	public function matchOtp(Request $request)
	{
		$validator = Validator::make($request->all(),[
			'otp' => "required",
			'email' => 'required|string|email',
		]);
		if ($validator->fails()) { 
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
		}
		$user = User::where('email', $request->get('email'))->where('otp', $request->get('otp'))->first();

		if (!$user){
			return response()->json(['status'=>false,'message' => 'This password reset otp is invalid.'
		]);
		}
		return response()->json(['status'=>true,'message' => 'OTP matched.']);
	}

	public function resetpassword(Request $request)
	{   

		$validator = Validator::make($request->all(),[
			'password'  => 'required',
			'mobile_no' => 'required',
		]);
		if ($validator->fails()) { 
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
		}

		$user = User::where('mobile_no', $request->get('mobile_no'))->first();
		if (!$user){
			return response()->json(['status'=>false,'message' => 'Invalid mobile no.']);
		}
		$user->password = bcrypt($request->password);
		// $user->otp = null;
		$user->save();
		$user->notify(new PasswordResetSuccess());
		return response()->json(['status'=>true,'message' => 'Password Reset Successfully.']);
	}
}
