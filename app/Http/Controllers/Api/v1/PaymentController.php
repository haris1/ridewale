<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Stripe\Stripe;
use Stripe\Customer;
use Auth;
use Validator;
use App\Models\v3\User;
use App\Models\v3\Payment;
use App\Withdraw;
use Notification;
use App\Notifications\WithdrawNotification;
use App\Helpers\NotificationHelper;

class PaymentController extends Controller
{
	public function __construct(){
		Stripe::setApiKey(config('services.stripe.secret'));
	}

	//retrive cards
	public function retriveCards()
	{
		$user = Auth::user();
		$customerCards=array();
		$defaultCard='';
		if($user->stripe_id != ''){
			$customerCards = Customer::retrieve($user->stripe_id);
			if(count($customerCards) > 0){
				$defaultCard=$customerCards->default_source;
			}
			$customerCards = $customerCards->sources['data'];

		}
		return response()->json(['status'=>true,'message'=>"",'data'=>$customerCards,'default_card'=>$defaultCard]);
	}

	//add cards
	public function addCard(Request $request)
	{
		$validator = Validator::make($request->all(),[
			'card_number' => "required|numeric|max:9999999999999999",
			'exp_month' => "required|numeric|max:12",
			'exp_year' => "required|numeric|max:9999",
			'cvc' => "required|numeric|max:999"
		]);

		if ($validator->fails()) { 
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
		}		
		$user = Auth::user();
		try{
			$tokenData = \Stripe\Token::create(array(
				"card" => array(
					"number"    => $request->get('card_number'),
					"exp_month" => $request->get('exp_month'),
					"exp_year"  => $request->get('exp_year'),
					"cvc"       => $request->get('cvc'),
				)));
			//print_r($tokenData);die();
			if($user->stripe_id == null){//die();
				$customer = Customer::create(array(
					'email' => $user->email,
					'source'  => $tokenData->id
				));
				User::where('id',$user->id)->update(array('stripe_id'=>$customer->id));
				$retriveResult=Customer::retrieve($customer->id);

			}else{
				$retriveResult=Customer::retrieve($user->stripe_id);
				$retriveResult->sources->create(array("source" =>$tokenData->id));
			}
		}catch(\Stripe\Error\Card $e){
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		} catch (\Stripe\Error\NotFound $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		}
		catch (\Stripe\Error\Authentication $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		}
		catch (\Stripe\Error\InvalidRequest $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		} catch (\Stripe\Error\RateLimit $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		} catch (\Stripe\Error\ApiConnection $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		} catch (\Stripe\Error\Base $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		}
		$retriveResult=Customer::retrieve($user->stripe_id);
		$customerCards = $retriveResult->sources['data'];
		return response()->json(['status'=>true,'message'=>"Card added successfully.",'data'=>$customerCards,'default_card'=>$retriveResult->default_source]);
	}


//remove cards
	public function removeCard(Request $request)
	{
		$validator = Validator::make($request->all(),[
			'card_id' => "required",
		]);

		if ($validator->fails()) { 
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
		}		
		$user = Auth::user();
		try{
			$retriveResult=Customer::retrieve(Auth::user()->stripe_id);
			$retriveResult->sources->retrieve($request->get('card_id'))->delete();
		}catch(\Stripe\Error\Card $e){
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		} catch (\Stripe\Error\NotFound $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		}
		catch (\Stripe\Error\Authentication $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		}
		catch (\Stripe\Error\InvalidRequest $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		} catch (\Stripe\Error\RateLimit $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		} catch (\Stripe\Error\ApiConnection $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		} catch (\Stripe\Error\Base $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		}
		return response()->json(['status'=>true,'message'=>"Card removed successfully."]);
	}

	/** set card as default cards **/
	public function setDefaltCard(Request $request)
	{
		$validator = Validator::make($request->all(),[
			'card_id' => "required",
		]);

		if ($validator->fails()) { 
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
		}		
		$user = Auth::user();
		try{

			$retriveResult=Customer::retrieve(Auth::user()->stripe_id);
			$retriveResult->default_source=$request->get('card_id');
			$retriveResult->save();  
		}catch(\Stripe\Error\Card $e){
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		} catch (\Stripe\Error\NotFound $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		}
		catch (\Stripe\Error\Authentication $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		}
		catch (\Stripe\Error\InvalidRequest $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		} catch (\Stripe\Error\RateLimit $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		} catch (\Stripe\Error\ApiConnection $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		} catch (\Stripe\Error\Base $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		}
		$customerCards = $retriveResult->sources['data'];

		return response()->json(['status'=>true,'message'=>"Default card set successfully.",'data'=>$customerCards,'default_card'=>$retriveResult->default_source]);
	}


	//update driver card detrail
	public function updateBankDetails(Request $request)
	{

		try {

			//validation stuff
			$validator = Validator::make($request->all(),[
				'account_holder_name'=> 'required',
				'transit_number' => 'required',
				'institution_number' => 'required',
				'account_number' => 'required',
				// 'first_name' => 'required',
				// 'last_name' => 'required',
				// 'gender' => 'required',
				'line1' => 'nullable',
				'line2' => 'nullable',
				'city' => 'required',
				'state' => 'required',
				'postal_code' => 'required',
				// 'country' => 'required',
				// 'dob' => 'required',
				'personal_id_number' => 'required',
				'document_front_pic' => 'required|file',
			]);

			if ($validator->fails()) {
				return response()->json(['status'=>false,'message' => $validator->messages()->first()]);
			}

			//get the parameters
			$user = Auth::user();
			$user_id = $user->_id;

			$strip_connect_id = $user->driver['stripe_id'];
			$accountHolderName = $request->get('account_holder_name');
			$transit_number = $request->get('transit_number');
			$institution_number = $request->get('institution_number');
			$routingNumber = $transit_number.'-'.$institution_number;
			$accountNumber = $request->get('account_number');

			$firstName = $user->first_name;
			$lastName = $user->last_name;
			$fullName = $firstName.' '.$lastName;
			$gender = $user->gender;
			$dob = $user->dob; //it should be array ['day' => 02, 'month => 11', 'year' => 2022]
			$fullDate = explode('-', $dob);
			$day = $fullDate[2];
			$month = $fullDate[1];
			$year = $fullDate[0];
			$email = $user->email;
			$phoneNumber = $request->get('phone_number');

			$line1 = $request->get('line1');
			$line2 = $request->get('line2');
			$city = $request->get('city');
			$state = $request->get('state');
			$postalCode = $request->get('postal_code');
			$country = "CA";

			$personalIdNumber = $request->get('personal_id_number'); // CA ID numbers must be 9 characters long
			$docFrontPic = $request->file('document_front_pic');

			if ($strip_connect_id) {

			//upload the identity document
				$stripeDocumentFile = false;

				
				if ($request->hasFile('document_front_pic')) {
					$uploadedFile = \Stripe\FileUpload::create(
						array("purpose" => "identity_document",
							"file" => fopen($_FILES["document_front_pic"]["tmp_name"], 'r'), 
						)
					);
					$stripeDocumentFile = $uploadedFile->id;
				}
				$account = \Stripe\Account::retrieve($strip_connect_id);
				$account->email = $email;

			//account information
				$account->external_account = [
					"object"=>"bank_account",
					"country"=>"CA",
					"currency"=>"CAD",
					"account_holder_name"=>$fullName,
					"account_holder_type"=>"individual",
					"routing_number"=>$routingNumber,
					"account_number"=>$accountNumber,
				];

				if ($account->legal_entity->verification->status == 'unverified') {
			//address
					$account->legal_entity['address'] = [
						"line1" => $line1,
						"line2" => $line2,
						"city" => $city,
						"state" => $state,
						"postal_code" => $postalCode,
						"country" => $country,
					];

			//personal info
					$account->legal_entity->dob = [
						'day' => $day,
						'month' => $month,
						'year' => $year
					];
					$account->legal_entity->first_name = $firstName;
					$account->legal_entity->last_name = $lastName;
					$account->legal_entity->gender = $gender;
					$account->legal_entity->phone_number = $phoneNumber;
					$account->legal_entity->type = "individual";
					$account->legal_entity->personal_id_number = $personalIdNumber;

					if ($stripeDocumentFile) {
						$account->legal_entity->verification->document = $stripeDocumentFile;
					}
				}

			//terms acceptance
				$account['tos_acceptance'] = [
					"date" => now()->timestamp,
					"ip" => $request->ip(),
					"user_agent" => $request->server('HTTP_USER_AGENT'),
				];
			//personal information
				$account->save();
			}else{
			//upload the identity document
				$stripeFile = \Stripe\FileUpload::create(array(
					'purpose' => 'identity_document',
					'file' => fopen($docFrontPic,'r')
				));
				$stripeFileId = $stripeFile->id;

			//create a connect account
				$account = \Stripe\Account::create(array(
					"type" => "custom",
					"country" => "CA",
					"email" => $email,
					"external_account" => array (
						"object"=>"bank_account",
						"country"=>"CA",
						"currency"=>"CAD",
						"account_holder_name"=>$fullName,
						"account_holder_type"=>"individual",
						"routing_number"=>$routingNumber,
						"account_number"=>$accountNumber,
					),
					"legal_entity" => array (
						"address" => array (
							"line1" => $line1,
							"line2" => $line2,
							"city" => $city,
							"state" => $state,
							"postal_code" => $postalCode,
							"country" => $country,
						),
						"dob" => array(
							'day' => $day,
							'month' => $month,
							'year' => $year
						),
						"type" => "individual",
						"first_name" => $firstName,
						"last_name" => $lastName,
						"gender" => $gender,
						"phone_number" => $phoneNumber,
						"personal_id_number" => $personalIdNumber,
						"verification" => array(
							"document" => $stripeFileId
						),
					),
					"tos_acceptance" => array (
						"date" => now()->timestamp,
						"ip" => $request->ip(),
						"user_agent" => $request->server('HTTP_USER_AGENT'),
					)
				));
				$strip_connect_id = $account->id;

			//store the account id inside the database
				$user->driver()->update(array('stripe_id'=>$strip_connect_id));
			}

			//give the response
			// $account = \Stripe\Account::retrieve($strip_connect_id);

			return response()->json(['status'=>true,'message' => "Your bank information has been submitted successfully.",'data'=>$account]);

		}catch (\Stripe\Error\InvalidRequest $e) {
			// Invalid parameters were supplied to Stripe's API
			$body = $e->getJsonBody();
			$err = $body['error'];

			if (isset($err['code']) && $err['code'] == "resource_already_exists") {
				return ['status'=>false,'message'=>$err['message'].' in our records']; 
			}
			return response()->json(['status'=>false,'message'=>$e->getMessage()]); 
		} catch (Stripe\Error\Card $e) {
			// Authentication with Stripe's API failed
			// (maybe you changed API keys recently)
			return response()->json(['status'=>false,'message'=>'Wrong Card information provided']);
		} catch (Stripe\Error\Authentication $e) {
			// Authentication with Stripe's API failed
			// (maybe you changed API keys recently)
			return response()->json(['status'=>false,'message'=>'Problem occures with Authentications']);
		} catch (Stripe\Error\ApiConnection $e) {
			// Network communication with Stripe failed
			return response()->json(['status'=>false,'message'=>'Something went wrong with api connection']);
		} catch (Stripe\Error\Base $e) {
			// Display a very generic error to the user, and maybe send
			// yourself an email
			return response()->json(['status'=>false,'message'=>'Base Error occures']);
		} catch(Exception $e){
			return response()->json(['status'=>false,'message'=>'Something went wrong']);
		}
	}

		//get driver bank account detail
	public function getBankDetails(Request $request)
	{
		$user = Auth::user();
		$user_id = $user->id;
		$strip_connect_id = $user->driver['stripe_id'];
		$response = [];
		try {
			$account='';
			if ($strip_connect_id != null) {
				$account = \Stripe\Account::retrieve($strip_connect_id);
				// $response[0]['email'] = $account->email;
				// $response[0]['legal_entity'] = $account->legal_entity;
				// $response[0]['external_accounts'] = $account->external_accounts;
			}
			return response()->json(['status'=>true,'data'=>$account,'message'=>'Bank details has been retrieved successfully.']);
		}catch (\Stripe\Error\InvalidRequest $e) {
		// Invalid parameters were supplied to Stripe's API
			$body = $e->getJsonBody();
			$err = $body['error'];
			if (isset($err['code']) && $err['code'] == "resource_already_exists") {
				return ['status'=>false,'message'=>$err['message'].' in our records']; 
			}
			return response()->json(['status'=>false,'message'=>$e->getMessage(),'data'=>[]]);
		} catch (Stripe\Error\Card $e) {
		// Authentication with Stripe's API failed
		// (maybe you changed API keys recently)
			return response()->json(['status'=>false,'message'=>$e->getMessage(),'data'=>[]]);
		} catch (Stripe\Error\Authentication $e) {
		// Authentication with Stripe's API failed
		// (maybe you changed API keys recently)
			return response()->json(['status'=>false,'message'=>$e->getMessage(),'data'=>[]]);
		} catch (Stripe\Error\ApiConnection $e) {
		// Network communication with Stripe failed
			return response()->json(['status'=>false,'message'=>$e->getMessage(),'data'=>[]]);
		} catch (Stripe\Error\Base $e) {
		// Display a very generic error to the user, and maybe send
		// yourself an email
			return response()->json(['status'=>false,'message'=>$e->getMessage(),'data'=>[]]);
		} catch(Exception $e){
			return response()->json(['status'=>false,'message'=>$e->getMessage(),'data'=>[]]);
		}
	}


	public function withdraw(Request $request){

		$user = Auth::user();
		$account_balance=0;

		$input = $request->all();
		$validator = Validator::make($input,[
			'amount' => "required",
		]);

		if ($validator->fails()) { 
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
		}

		if($user->active_mode == 'driver'){
			$transactions = Payment::join('trips',function($requests){
				$requests->on('payments.trip_id','=','trips.id');
			})
			->where('trips.driver_id',$user->driver->id)
			->where('trips.status','completed')
			->whereHas('transaction')
			//->where('payments.withdrow_status','0')
			->where('payments.status','!=','3')

			->get();
			$driver = $user->driver;
			$trips=[];
			$account_balance =  $transactions->sum('driver_fees');

			$withdraw_amnt	 =	$request->get('amount');
			if($account_balance <= 0){
				return response()->json(['status'=>false,'message'=>"Your wallet is empty",'account_balance'=>'0.00']);
			}

			if($withdraw_amnt > $account_balance){
				return response()->json(['status'=>false,'message'=>"Balance not available in wallet.",'account_balance'=>'0.00']);
			}

			try {
				// $account_balance=0.01;
				$charge = \Stripe\Transfer::create([

					"amount" => round($request->get('amount')*100),
					"currency" => "CAD",
					"destination" => $driver->stripe_id
				]);

				$payout=\Stripe\Payout::create([
					"amount" => round($withdraw_amnt*100),

					"currency" => "cad",
				//	"method"=>"instant",
					"metadata"=>array('charge_id'=>$charge->id)
				], ["stripe_account" => $driver->stripe_id]);

				if($charge && $payout){

					$data['amount'] = number_format($withdraw_amnt,2);
					//$data['amount'] = number_format($account_balance,2);
					$data['driver_id'] = $user->driver->id;
        			// $data['charge_id'] = $charge->id;
					$data['transaction_id'] = $charge->balance_transaction;

					$data['transaction_time'] = date('Y-m-d H:i:s', $charge->created);
					$data['failure_code'] = ($charge->failure_code != "")?$charge->failure_code:"";
					$data['failure_message'] = ($charge->failure_message != "")?$charge->failure_message:"";
					$data['status'] = $charge->status;
					$data['payout_transaction_id'] = $payout->balance_transaction;

					$data['payout_transaction_time'] = date('Y-m-d H:i:s', $payout->created);
					$data['payout_failure_code'] = ($payout->failure_code != "")?$payout->failure_code:"";
					$data['payout_failure_message'] = ($payout->failure_message != "")?$payout->failure_message:"";
					$data['payout_status'] = $payout->status;

					foreach ($transactions as $key => $transaction) {
						$trips[]=$transaction->trip_id;
					}
					//print_r($requests);die();
					$data['on_behalf_of'] = $trips;

					Withdraw::create($data);

					// Payment::whereIn('trip_id',$trips)
					// ->whereHas('transaction')
					// ->update(array('withdrow_status' => '1'));


					$user->notify(new WithdrawNotification(number_format($withdraw_amnt,2)));

					NotificationHelper::sendPushNotification($user,'WithdrawSuccess');
					$total_withdraws=$user->driver->withdraws()->sum('amount');

					$remainig_balance=$account_balance-$total_withdraws;
					return response()->json(['status'=>true,'message'=>'Withdrow amount successfully.','account_balance'=>number_format($remainig_balance,2)]);

				}

			}catch (\Stripe\Error\InvalidRequest $e) {
			// Invalid parameters were supplied to Stripe's API
				$body = $e->getJsonBody();
				$err = $body['error'];
				if (isset($err['code']) && $err['code'] == "resource_already_exists") {
					return ['status'=>false,'message'=>$err['message'].' in our records']; 
				}
				return response()->json(['status'=>false,'message'=>$e->getMessage(),'account_balance'=>'0.00']);
			} catch (Stripe\Error\Card $e) {
			// Authentication with Stripe's API failed
			// (maybe you changed API keys recently)
				return response()->json(['status'=>false,'message'=>$e->getMessage(),'account_balance'=>'0.00']);
			} catch (Stripe\Error\Authentication $e) {
			// Authentication with Stripe's API failed
			// (maybe you changed API keys recently)
				return response()->json(['status'=>false,'message'=>$e->getMessage(),'account_balance'=>'0.00']);
			} catch (Stripe\Error\ApiConnection $e) {
			// Network communication with Stripe failed
				return response()->json(['status'=>false,'message'=>$e->getMessage(),'account_balance'=>'0.00']);
			} catch (Stripe\Error\Base $e) {
			// Display a very generic error to the user, and maybe send
			// yourself an email
				return response()->json(['status'=>false,'message'=>$e->getMessage(),'account_balance'=>'0.00']);
			} catch(Exception $e){
				return response()->json(['status'=>false,'message'=>$e->getMessage(),'account_balance'=>'0.00']);
			}
		}
		return response()->json(['status'=>false,'message'=>'You have not access.']);

	}

	public function workerPayoutHistory(Request $request)
	{
		$withdraws = Auth::user()->snow_angel->withdraws()->get();
		return response()->json(['status'=>true,'message'=>'Withdraws listing.','data'=>$withdraws]);
	}

}
