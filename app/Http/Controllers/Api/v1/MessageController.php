<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\v3\Message;
use App\Models\v3\User;
use App\Models\v3\ChatRoom;
use App\Events\SendMessageChannel;
use Validator;
use Auth;
use DB;
use Carbon\Carbon;
use App\Helpers\NotificationHelper;

class MessageController extends Controller
{

	public function sendMessage(Request $request){

		/** validate the data **/
		$validator = Validator::make($request->all(),[
			"receiver_id" => "required|exists:users,id",
			"message"	  => "required"
		]);

		if ($validator->fails()) { 
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
		}

		/** get the parameters **/
		$user 			 	 = Auth::user();
		$senderId 			 = $user->id;
		$receiverId 	 	 = (int)$request->get('receiver_id');
		
		$data 				 = new Message(); 
		$data['sender_id'] 	 = $senderId;
		$data['receiver_id'] = $receiverId;
		$data['message'] 	 = $request->get('message');
		
		/** get the chat room where the auth user and the receiver was involved **/
		$chatRoom = $user->chat_rooms()
		->whereHas('room_participants',function($query) use ($receiverId){
			$query->where('user_id',$receiverId);
		})
		->first();

		/** If involvement in chat room found then add the message under that chat room else create a room and then add the message under that chat room **/
		if ($chatRoom) {
			$message = $chatRoom->messages()->save($data);
		}else{
			$chatRoom = ChatRoom::create(['chat_room_id' => uniqid()]);
			$chatRoom->room_participants()->attach([$senderId,$receiverId]);
			$message = $chatRoom->messages()->save($data);
		}

		$message = Collect($message)->only(['id','created_at','sender_id','receiver_id','message']);

		$chatRoom->load(['last_message' => function($query) use ($senderId){
			$query->select('id','sender_id','receiver_id','message','created_at','chat_room_id');
		},'room_participants' => function($query) use ($senderId){ 
			$query->select('users.id','first_name','last_name','profile_pic');
		}])
		->first();

		$chatRoom->room_participants->transform(function($participant,$index) use ($senderId,$receiverId){
			$participant->profile_pic = asset('storage/images/users/'.$participant->id.'/profile_pic').'/'.$participant->profile_pic;
			$participant->me = ($participant->id == $receiverId)?true:false;
			return $participant;
		});

		$receiver = User::find($receiverId);
		$roomUserIds = $chatRoom->room_participants()->pluck('user_id');
		event(new SendMessageChannel(['chat_room_data'=>$chatRoom,'chat_room_user_ids'=>$roomUserIds]));
		NotificationHelper::sendPushNotification($receiver,'chatMessage',$chatRoom);

		/** return the response **/
		return response()->json([
			'status'	=> true,
			'message'	=> 'Message has been send successfully.',
			'data' 		=> $message,
			'chat_room_data' => $chatRoom,
		]);
	}

	public function getUserInbox(Request $request){

		/** validate the data **/
		// $validator = Validator::make($request->all(),[
		// 	"page"	  	   => "required|numeric|min:1"
		// ]);

		// if ($validator->fails()) { 
		// 	return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
		// }

		/** get the parameters **/
		$user 		 = Auth::user();
		$senderId  	 = $user->id;
		$pageNo 	 = $request->get('page');

		/** get the user's chat rooms **/
		$chatRooms = $user->chat_rooms()
		->whereHas('last_message')
		->with(['last_message' => function($query) use ($senderId){
			$query->select('id','sender_id','receiver_id','message','created_at','chat_room_id');
		},'room_participants' => function($query) use ($senderId){ 
			$query->select('users.id','first_name','last_name','profile_pic');
		}])
		->get();

		$chatRooms = $chatRooms->mapWithKeys(function ($chatRoom, $index) {
			return [$chatRoom['last_message']['created_at']->format('Y-m-d H:i:s') => $chatRoom];
		})
		->sortKeysDesc()
		->values();

		$chatRooms->map(function($chatRoom,$index) use ($senderId){
			$chatRoom->room_participants->map(function($participant,$index) use ($senderId){
				$participant->profile_pic = asset('storage/images/users/'.$participant->id.'/profile_pic').'/'.$participant->profile_pic;
				$participant->me = ($participant->id == $senderId)?true:false;
				return $participant;
			});
			return $chatRoom;
		});

		/** check if there is anything remaining data available for next paging **/
		// $hasNextPage = ($pageNo < $chatRooms->lastPage())?true:false;

		/** return the response **/
		return response()->json([
			'status'		=> true,
			'message'		=> 'User\'s message inbox has been retrieved successfully.' ,
			// 'has_next_page'	=> $hasNextPage,
			'data'			=> $chatRooms->all()
		]);
	}

	public function getDetailMessages(Request $request){

		/** validate the data **/
		$validator = Validator::make($request->all(),[
			// "chat_room_id" => "required|exists:chat_rooms,id",
			"receiver_id" => "required|exists:users,id",
			"page"	  	   => "required|numeric|min:1"
		]);

		if ($validator->fails()) { 
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
		}

		/** get the parameters **/
		$user 		 = Auth::user();
		$senderId  	 = $user->id;
		// $chatRoomId  = $request->get('chat_room_id');
		$receiverId  = $request->get('receiver_id');
		$pageNo 	 = $request->get('page');

		/** get the room **/
		// $chatRoom 	 = ChatRoom::select('id','chat_room_id','created_at')->find($chatRoomId);
		
		/** get the room's messages **/
		// $chatRoomMessages 	= $chatRoom->messages()->select('id','sender_id','receiver_id','message','created_at')->orderBy('id','desc')->paginate(20);
		$chatRoomMessages 	= Message::where(function($query) use ($senderId,$receiverId){
								$query->where('sender_id',$senderId)->where('receiver_id',$receiverId);
							})
							->orWhere(function($query) use ($senderId,$receiverId){
								$query->where('sender_id',$receiverId)->where('receiver_id',$senderId);
							})
							->select('id','sender_id','receiver_id','message','created_at')
							->orderBy('id','desc')
							->paginate(20);

		/** check if there is anything remaining data available for next paging **/
		$hasNextPage = ($pageNo < $chatRoomMessages->lastPage())?true:false;

		/** return the response **/
		return response()->json([
			'status'		=> true,
			'message'		=> 'Messages are retrieved successfully.',
			'has_next_page'	=> $hasNextPage,
			// 'room_info'		=> $chatRoom,
			'room_messages' => $chatRoomMessages->all()
		]);
	}
}
