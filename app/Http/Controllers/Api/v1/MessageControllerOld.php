<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\v3\Message;
use App\Models\v3\User;
use App\Models\v3\ChatRoom;
use App\Events\SendMessageChannel;
use Validator;
use Auth;
use DB;

class MessageControllerOld extends Controller
{
    public function sendMessage(Request $request){

    	$validator = Validator::make($request->all(),[
			"receiver_id" => "required|exists:users,id",
			"message"	  => "required"
		]);

		if ($validator->fails()) { 
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
		}

		$user 			 	 = Auth::user();
		$senderId 			 = $user->id;
		$receiverId 	 	 = $request->get('receiver_id');
		
		$data 				 = new Message(); 
		$data['sender_id'] 	 = $senderId;
		$data['receiver_id'] = $receiverId;
		$data['message'] 	 = $request->get('message');
		
		$chatRoom = $user->chat_rooms()
				->whereHas('room_participants',function($query) use ($receiverId){
					$query->where('user_id',$receiverId);
				})
				->first();

		if ($chatRoom) {
    		$chatRoom->messages()->save($data);
		}else{
			$chatRoom = ChatRoom::create(['chat_room_id' => uniqid()]);
			$chatRoom->room_participants()->attach([$senderId,$receiverId]);
    		$chatRoom->messages()->save($data);
		}
    	return response()->json(['status'=>true,'message'=>'Message has been send successfully.']);

		exit;
		// $chatRoom = ChatRoom::where(function($query) use ($senderId, $receiverId) {
		// 				$query->orWhere(function($query) use ($senderId, $receiverId) {
		// 					$query->where('participants','like',"%\"{$receiverId}\"%");
		// 					$query->where('participants','like',"%\"{$senderId}\"%");
		// 				});
		// 				$query->orWhere(function($query) use ($senderId, $receiverId) {
		// 					$query->where('participants','like',"%\"{$receiverId}\"%");
		// 					$query->where('participants','like',"%\"{$senderId}\"%");
		// 				});
		// 			})
		// 			->first();
		
		// if ($chatRoom) {
  //   		$chatRoom->messages()->save($data);
		// }else{
		// 	$chatRoom = ChatRoom::create(['chat_room_id' => uniqid(),'participants' => [$data['sender_id'],$data['receiver_id']]])->first();
  //   		$chatRoom->messages()->save($data);
		// }
  //   	broadcast(new SendMessageChannel($request->all()))->toOthers();
  //   	return response()->json(['status'=>true,'message'=>'Message has been send successfully.']);

		// $senderId 			 = Auth::user()->id;
		// $receiverId 	 	 = $request->get('receiver_id');
		
		// $data 				 = new Message(); 
		// $data['sender_id'] 	 = (string)$senderId;
		// $data['receiver_id'] = $receiverId;
		// $data['message'] 	 = $request->get('message');
		
		// $chatRoom = ChatRoom::where(function($query) use ($senderId, $receiverId) {
		// 				$query->orWhere(function($query) use ($senderId, $receiverId) {
		// 					$query->where('participants','like',"%\"{$receiverId}\"%");
		// 					$query->where('participants','like',"%\"{$senderId}\"%");
		// 				});
		// 				$query->orWhere(function($query) use ($senderId, $receiverId) {
		// 					$query->where('participants','like',"%\"{$receiverId}\"%");
		// 					$query->where('participants','like',"%\"{$senderId}\"%");
		// 				});
		// 			})
		// 			->first();
		
		// if ($chatRoom) {
  //   		$chatRoom->messages()->save($data);
		// }else{
		// 	$chatRoom = ChatRoom::create(['chat_room_id' => uniqid(),'participants' => [$data['sender_id'],$data['receiver_id']]])->first();
  //   		$chatRoom->messages()->save($data);
		// }
  //   	broadcast(new SendMessageChannel($request->all()))->toOthers();
  //   	return response()->json(['status'=>true,'message'=>'Message has been send successfully.']);
    }

    public function getUserInbox(Request $request){

    	/** get the parameters **/
		$user 		 = Auth::user();
		$senderId  	 = $user->id;

		$chatRooms = $user->chat_rooms()->with(['last_message','room_participants' => function($query) use ($senderId){ 
						$query->where('user_id','!=',$senderId)->select('room_participants.id','first_name','last_name','profile_pic');
					}])
					->get();
		// $data = ChatRoom::where('participants','like',"%\"{$senderId}\"%")
		// 			->whereHas('last_message')
		// 			->with(['last_message'])
		// 			->get();


		return response()->json([
			'status'		=> true,
			'message'		=> 'Messages are retrieved successfully.',
			'data'			=> $chatRooms
		]);
		// DB::connection()->enableQueryLog();
		// $queries = DB::getQueryLog();
		// $data = ChatRoom::create([
		// 			'chat_room_id' => uniqid(),
		// 			'participants' => [1,2]
		// 		])
		// 		->get();
		print_r($data);
		exit;
    	/** get the latest message **/

		// $sendMessages 	= $user->messages()
		// 					->select('*',DB::raw('MAX(id) as latest_message_id'))
		// 					->with('receiver')
		// 					->groupBy('receiver_id')
		// 					->orderBy('latest_message_id','desc');

  		// $receivedMesages = Message::where('receiver_id',$senderId)
				// 			->select('*',DB::raw('MAX(id) as latest_message_id'))
				// 			->with('sender')
				// 			->groupBy('sender_id')
				// 			->orderBy('latest_message_id','desc');


    	$messages = Message::where(function($query) use ($senderId){
			    		$query->orWhere(function($query) use ($senderId){
			    			$query->where('sender_id',$senderId);
			    		})
			    		->orWhere(function($query) use ($senderId){
			    			$query->where('receiver_id',$senderId);
			    		});
			    	})
    				->select('*')
    				->get();


		
		// $sendMessagesResult 	= $sendMessages->get();
		// $receivedMesagesResult 	= $receivedMesages->get();
	
		// $messages->transform(function($message,$index) use ($messages){
		// 	$countableMessage = $messages->where('sender_id' , $message->receiver_id)->where('receiver_id' , $message->sender_id);
		// 	if (count($countableMessage)) {
		// 		$message = $countableMessage->first();
		// 	}
		// 	return $message;
		// });

		// $messages = Message::orderBy('id','desc')
  //   					->whereIn('id', $sendMessagesResult->pluck('latest_message_id'))
  //   					->get();

    	/** return the response **/
    	return response()->json([
    		'status'		=> true,
    		'message'		=> 'Messages are retrieved successfully.',
    		'data'			=> $messages
    	]);
    	// echo "<pre>";print_r($finalData->toArray());
    	// echo "<pre>";print_r($sendMessagesResult->pluck('latest_message_id'));
    	// // echo "<pre>";print_r($sendMessagesResult->toArray());
    	// // echo "<pre>";print_r($receivedMesagesResult->toArray());
    	// exit;
		// $messages = Message::where(function($query) use ($senderId, $receiverId){
		// 	$query->orWhere(function($query) use ($senderId,$receiverId){
		// 			$query->where('sender_id',$senderId);
		// 			$query->where('receiver_id',$receiverId);
		// 		})
		// 		->orWhere(function($query) use ($senderId,$receiverId){
		// 			$query->where('sender_id',$receiverId);
		// 			$query->where('receiver_id',$senderId);
		// 		});
		// })
		// ->select('id','sender_id','receiver_id','message','created_at')
		// ->orderBy('id','desc')
		// ->get();
		//   	 	echo "<pre>";print_r($messages->toArray());
		//   	exit;
  //   	$receivedMesages = Message::where('receiver_id',$senderId)
		// 					->select('*',DB::raw('MAX(id) as latest_message_id'))
		// 					->groupBy('sender_id')
		// 					->orderBy('latest_message_id','desc');
		
		// $sendMessages 	= $user->messages()
		// 					->select('*',DB::raw('MAX(id) as latest_message_id'))
		// 					->groupBy('receiver_id')
		// 					->orderBy('latest_message_id','desc');

		// $wholeMessages = $receivedMesages->union($sendMessages)
		// 					->get()
		// 					->transform(function($message,$index){
		// 						$
		// 						return $message;
		// 					});

    	$wholeMessages = DB::table('messages AS m1')
    	->leftjoin('messages AS m2', function($join) {
    		$join->on('m1.sender_id', '=', 'm2.receiver_id');
    		// $join->on('m1.id', '<', 'm2.id');
    	})
    	// ->where('m1.sender_id',$senderId)
    	// ->whereNull('m2.id')
    	->select('*')
    	->orderBy('m1.created_at', 'm1.desc')
    	->get();
    	echo "<pre>";print_r($wholeMessages->toArray());


    	exit;
    	echo "<pre>";print_r($receivedMesages->get()->toArray());
    	echo "<pre>";print_r($sendMessages->get()->toArray());
    	

    	$recipients = $user->messages()
    					// ->select('*', DB::raw('MAX(created_at) AS created_at_timestamp'))
    					// ->orderBy('created_at_timestamp','desc')
    					// ->groupBy('receiver_id')
    					->orderBy('id','desc')
    					->whereIn('id', $latestMessages)
    					->get();

    	// $recipients = $user->messages()->groupBy('receiver_id')->get();

    	echo "<pre>";print_r($latestMessages->toArray());
    	echo "<pre>";print_r($recipients->toArray());
    	exit;
		$messages = Message::where(function($query) use ($senderId){
						$query->orWhere(function($query) use ($senderId){
								$query->where('sender_id',$senderId);
							})
							->orWhere(function($query) use ($senderId){
								$query->where('receiver_id',$senderId);
							});
					})
					->select('id','sender_id','receiver_id','message','created_at')
					->groupBy('receiver_id')
					->orderBy('created_at','desc')
					->paginate(20);

		/** return the response **/
    	return response()->json([
    		'status'		=> true,
    		'message'		=> 'Messages are retrieved successfully.',
    		'data'			=> $messages->all()
    	]);
    }

 	public function getDetailMessages(Request $request){
    	
    	/** validate the data **/
    	$validator = Validator::make($request->all(),[
			"chat_room_id" => "required|exists:chat_rooms,id",
			"page"	  	   => "required|numeric|min:1"
		]);

		if ($validator->fails()) { 
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
		}

    	/** get the parameters **/
		$user 		 = Auth::user();
		$senderId  	 = $user->id;
		$chatRoomId  = $request->get('chat_room_id');
		$pageNo 	 = $request->get('page');

		/** get all the chat messages between sender and receiver **/
		$chatRoom = ChatRoom::find($chatRoomId)->with('messages')->paginate(20);
		// $messages = Message::where(function($query) use ($senderId, $receiverId){
		// 				$query->orWhere(function($query) use ($senderId,$receiverId){
		// 						$query->where('sender_id',$senderId);
		// 						$query->where('receiver_id',$receiverId);
		// 					})
		// 					->orWhere(function($query) use ($senderId,$receiverId){
		// 						$query->where('sender_id',$receiverId);
		// 						$query->where('receiver_id',$senderId);
		// 					});
		// 			})
		// 			->select('id','sender_id','receiver_id','message','created_at')
		// 			->orderBy('id','desc')
		// 			->paginate(20);

		/** provide the flag which represent who is sender and who is receiver **/

		$chatRoom->getCollection()->transform(function($room,$index) use ($senderId){
			$room->messages->transform(function($message,$index) use ($senderId){
				$message->mine = ($message->sender_id == $senderId)?true:false; 
				return $message;
			});
			return $room;
		});

		/** check if there is some data available to search **/
		// $hasNextPage = ($pageNo < $messages->lastPage())?true:false;

		/** get receiver data **/
		// $receiver = User::where('id',$receiverId)->select('id','first_name','last_name','profile_pic')->firstOrFail();

    	/** return the response **/
    	return response()->json([
    		'status'		=> true,
    		'message'		=> 'Messages are retrieved successfully.',
    		// 'receiver'		=> $receiver,
    		'has_next_page'	=> false,
    		'data'			=> $chatRoom->all()
    	]);
    }
  //   public function getDetailMessages(Request $request){
    	
  //   	/** validate the data **/
  //   	$validator = Validator::make($request->all(),[
		// 	"receiver_id" => "required|exists:users,id",
		// 	"page"	  	  => "required|numeric|min:1"
		// ]);

		// if ($validator->fails()) { 
		// 	return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
		// }

  //   	/** get the parameters **/
		// $user 		 = Auth::user();
		// $senderId  	 = $user->id;
		// $receiverId  = $request->get('receiver_id');
		// $pageNo 	 = $request->get('page');

		// /** get all the chat messages between sender and receiver **/
		// $messages = Message::where(function($query) use ($senderId, $receiverId){
		// 				$query->orWhere(function($query) use ($senderId,$receiverId){
		// 						$query->where('sender_id',$senderId);
		// 						$query->where('receiver_id',$receiverId);
		// 					})
		// 					->orWhere(function($query) use ($senderId,$receiverId){
		// 						$query->where('sender_id',$receiverId);
		// 						$query->where('receiver_id',$senderId);
		// 					});
		// 			})
		// 			->select('id','sender_id','receiver_id','message','created_at')
		// 			->orderBy('id','desc')
		// 			->paginate(20);

		// /** provide the flag which represent who is sender and who is receiver **/
		// $messages->getCollection()->transform(function($message,$index) use ($senderId){
		// 			$message->mine = ($message->sender_id == $senderId)?true:false; 
		// 			return $message;
		// 		});

		// /** check if there is some data available to search **/
		// $hasNextPage = ($pageNo < $messages->lastPage())?true:false;

		// /** get receiver data **/
		// // $receiver = User::where('id',$receiverId)->select('id','first_name','last_name','profile_pic')->firstOrFail();

  //   	/** return the response **/
  //   	return response()->json([
  //   		'status'		=> true,
  //   		'message'		=> 'Messages are retrieved successfully.',
  //   		// 'receiver'		=> $receiver,
  //   		'has_next_page'	=> $hasNextPage,
  //   		'data'			=> $messages->all()
  //   	]);
  //   }
}
