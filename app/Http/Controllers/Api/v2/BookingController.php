<?php

namespace App\Http\Controllers\Api\v2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Work_city;
use App\Models\v3\Trip;
use App\Admin_statistic;
use Validator;
use Auth;
use App\Helpers\TripHelper;
use App\Models\v3\Payment;
use App\Models\v3\User;
use App\Notifications\BookingNotification;
use App\Notifications\BookingConfirmationNotification;
use App\Helpers\NotificationHelper;
use Stripe\Stripe;
use Stripe\Customer;
use App\Transaction;
use DB;
use Carbon\Carbon;

class BookingController extends Controller
{

	public function getWorkingCities(Request $request) 
	{

		$cities = Work_city::select('id','name')->get();
		
		$tripes = Trip::available()
		->with('from_city:id,name','to_city:id,name')
		->status('active')
		->orderBy('created_at','desc')
		->groupBy('from_city_id','to_city_id')
		->select('id','from_city_id','to_city_id','created_at','updated_at')
		->limit(5)
		->get();
		
		return response()->json([
			'status'=> true,
			'message'=> "Cities has been retrived successfully.",
			'available_trips'=> $tripes,
			'data'=> $cities,
		]);
	}

	public function bookTheTrip(Request $request) 
	{

		$validator = Validator::make($request->all(),[
			'trip_id' 	 => 'required|exists:trips,id',
			'luggage' 	 => 'required|numeric|in:0,1,2',	// no bags, small carry bag, small carry bag with purse
			'total_seat' => 'required|numeric',
		]);
		
		if ($validator->fails()){
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
		}

		$tripId 		= $request->get('trip_id');
		$requiredSeats 	= $request->get('total_seat');
		$luggage 		= $request->get('luggage');
		
		$user = Auth::user();
		$trip = Trip::find($tripId);
		
		if($trip->available_seats < $requiredSeats){
			return response()->json(['status'=>false,'message'=>"Sorry, there are only ".$trip->available_seats." seat left to book."]);
		}

		$tripFair = TripHelper::getTripFair($trip,$requiredSeats);

		/** Insert record into DB Table **/
		$book['user_id'] 			= $user->id;
		$book['trip_id'] 			= $tripId;
		$book['total'] 	 	  		= $tripFair['chargable_amount'];
		$book['total_person'] 		= $requiredSeats;
		$book['luggage']			= $luggage;
		$book['transfer_fees']		= $tripFair['transfer_fees'];
		$book['admin_fees']			= $tripFair['admin_fees'];
		$book['driver_fees']		= $tripFair['driver_fees'];
		$book['tax_cent']			= $tripFair['tax_cent'];
		$book['tax']				= $tripFair['tax'];
		$book['withdrow_status']	= '0';
		$userTrip = Payment::create($book);

		/** Reduce the available seats of trip **/
		$trip->decrement('available_seats',$requiredSeats);

		/** send notification **/
		$trip->user_type 	= 'user';
		$trip->booking_id 	= $userTrip->id;
		$trip->seats 		= $requiredSeats;
		$user->notify(new BookingNotification($trip));
		if($user->active_mode == 'user'){
			NotificationHelper::sendPushNotification($user,'UserTripBooking',$trip->toArray());
		}

		$trip->user_name 	= $user->first_name.' '.$user->last_name;
		$driver 			= $trip->driver->user;
		$trip->user_type 	= 'driver';
		$driver->notify(new BookingNotification($trip));
		if($driver->active_mode == 'driver'){
			NotificationHelper::sendPushNotification($driver,'DriverTripBooking',$trip->toArray());
		}

		return response()->json(['status'=>true,'message'=>"Your Trip has been booked successfully.",'data'=>$userTrip]);
	}

	/** trip detail  **/
	public function tripDetail(Request $request){

		$validator = Validator::make($request->all(),[
			'trip_id' 	 => 'required|exists:trips,id',
		]);
		
		if ($validator->fails()){
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
		}
		$user = Auth::user();
		Stripe::setApiKey(config('services.stripe.secret'));

		$tripId 						=  $request->get('trip_id');
		$tripDetail 					=  TripHelper::tripDetail($tripId);
		$data['vehicle']				=  $tripDetail->vehicle->car_model;
		$data['allowed_things']			=  $tripDetail->vehicle->allowed_things;
		$data['not_allowed_things']		=  $tripDetail->vehicle->rules_and_regulations;

		$data['driver']					=  $tripDetail->driver;
		$data['pickup_location'] 		=  $tripDetail->pickup_location;

		$data['trip_date']				=  $tripDetail->trip_date;
		$data['pickup_time']			=  $tripDetail->pickup_time;
		$data['available_seats']		=  $tripDetail->available_seats;

		$admin_statistic 				=  TripHelper::getAdminStatistics();
		$data['price']					=  number_format($tripDetail->price,2);
		$data['tax']					=  $admin_statistic->tax_cent;
		$data['no_of_riders']			=  Payment::where(['trip_id'=>$tripId,'handshake'=>1])->count();
		$data['card_last4']				=  '';
		$data['refund_policy']			=  $admin_statistic->refund_policy;
		$data['cancellation_policy'] 	=  $admin_statistic->cancellation_policy;

		/*card detail*/
		$data['card_detail']			= array();
		$retriveResult					= Customer::retrieve($user->stripe_id);

		if(count($retriveResult->sources['data']) > 0){
			$carddetail 	  			=  $retriveResult->sources->retrieve($retriveResult->default_source);
			$card['card_last4']			=  $carddetail->last4;
			$card['brand']				=  $carddetail->brand;
		}
		$data['card_detail'] 			= $card;

		return response()->json(['status'=>true,'message'=>"Trip detail retrived successfully.",'data'=>$data]);

	}

	/** my active trips **/
	public function myActiveTrips(Request $request){

		$user = Auth::user();
		$pageNumber=1;
		if($request->has('page')){
			$pageNumber=$request->get('page');
		}
		$active_trips = Payment::select('payments.id','trip_id','user_id','total_person','total as amount','luggage','payments.created_at')
		->with(['trip'=> function ($query) use ($user) {
			$query->select('id','from_city_id','to_city_id')
			->with(['from_city'=> function ($query) {
				$query->select('id','name');
			}])
			->with(['to_city'=> function ($query) {
				$query->select('id','name');
			}]);
		}])
		->where(['status'=>'1','user_id'=>$user->id,'handshake'=>1])	
		->orderBy('created_at','ASC')		
		->paginate(1, ['*'], 'page', $pageNumber);

		$active_trips->getCollection()->transform(function($active_trip,$key){
			$active_trip->amount = number_format($active_trip->amount,2);;
			return $active_trip;
		});

		$has_next = true;
		if($active_trips->nextPageUrl() == null){
			$has_next = false;
		}

		return response()->json(['status'=>true,'message'=>"Active trips retrived successfully.",'data'=>$active_trips->all(),'has_next'=>$has_next]);

	}

	/** my confirmed trips **/
	public function myConfimedTrips(Request $request){

		$user = Auth::user();
		$pageNumber=1;
		if($request->has('pending_trip')){
			$pageNumber=$request->get('page');
		}

		$pending_trips = Payment::select('payments.id','trip_id','user_id','total_person','total as amount','luggage','payments.created_at')
		->with(['trip'=> function ($query) use ($user) {
			$query->select('id','from_city_id','to_city_id')
			->with(['from_city'=> function ($query) {
				$query->select('id','name');
			}])
			->with(['to_city'=> function ($query) {
				$query->select('id','name');
			}]);
		}])
		->where(['status'=>'0','user_id'=>$user->id,'handshake'=>0])	
		->orderBy('created_at','ASC')		
		->paginate(2, ['*'], 'page', $pageNumber);

		$pending_trips->getCollection()->transform(function($pending_trip,$key){
			$pending_trip->amount = number_format($pending_trip->amount,2);;
			return $pending_trip;
		});

		$has_next = true;
		if($pending_trips->nextPageUrl() == null){
			$has_next = false;
		}

		return response()->json(['status'=>true,'message'=>"Confirmed trips retrived successfully.",'data'=>$pending_trips->all(),'has_next'=>$has_next]);
	}

	/** my pending trips **/
	public function myPendingTrips(Request $request){

		$user = Auth::user();
		$pageNumber=1;
		if($request->has('confirmed_trip')){
			$pageNumber=$request->get('page');
		}

		$confirmed_trips = Payment::select('payments.id','trip_id','user_id','total_person','total as amount','luggage','payments.created_at')
		->with(['trip'=> function ($query) use ($user) {
			$query->select('id','from_city_id','to_city_id')
			->with(['from_city'=> function ($query) {
				$query->select('id','name');
			}])
			->with(['to_city'=> function ($query) {
				$query->select('id','name');
			}]);
		}])
		->where(['status'=>'0','user_id'=>$user->id,'handshake'=>1])	
		->orderBy('created_at','ASC')		
		->paginate(2, ['*'], 'page', $pageNumber);

		$confirmed_trips->getCollection()->transform(function($confirmed_trip,$key){
			$confirmed_trip->amount = number_format($confirmed_trip->amount,2);;
			return $confirmed_trip;
		});

		$has_next = true;
		if($confirmed_trips->nextPageUrl() == null){
			$has_next = false;
		}

		return response()->json(['status'=>true,'message'=>"Pending trips retrived successfully.",'data'=>$confirmed_trips->all(),'has_next'=>$has_next]);

	}

	/** rider cancelled trips **/
	public function riderCancelledTrips(Request $request){

		$user = Auth::user();
		$pageNumber=1;
		if($request->has('confirmed_trip')){
			$pageNumber=$request->get('page');
		}

		$cancelled_trips = Payment::select('payments.id','trip_id','user_id','total_person','total as amount','luggage','payments.created_at')
		->with(['trip'=> function ($query) use ($user) {
			$query->select('id','from_city_id','to_city_id')
			->with(['from_city'=> function ($query) {
				$query->select('id','name');
			}])
			->with(['to_city'=> function ($query) {
				$query->select('id','name');
			}]);
		}])
		->where(['status'=>'3','user_id'=>$user->id])	
		->orderBy('created_at','ASC')		
		->paginate(2, ['*'], 'page', $pageNumber);

		$cancelled_trips->getCollection()->transform(function($cancelled_trip,$key){
			$cancelled_trip->amount = number_format($cancelled_trip->amount,2);;
			return $cancelled_trip;
		});

		$has_next = true;
		if($cancelled_trips->nextPageUrl() == null){
			$has_next = false;
		}

		return response()->json(['status'=>true,'message'=>"Cancelled trips retrived successfully.",'data'=>$cancelled_trips->all(),'has_next'=>$has_next]);

	}

	/** rider completed trips **/
	public function riderCompletedTrips(Request $request){

		$user = Auth::user();
		$pageNumber=1;
		if($request->has('confirmed_trip')){
			$pageNumber=$request->get('page');
		}

		$completed_trips = Payment::select('payments.id','trip_id','user_id','total_person','total as amount','luggage','payments.created_at')
		->with(['trip'=> function ($query) use ($user) {
			$query->select('id','from_city_id','to_city_id')
			->with(['from_city'=> function ($query) {
				$query->select('id','name');
			}])
			->with(['to_city'=> function ($query) {
				$query->select('id','name');
			}]);
		}])
		->where(['status'=>'2','user_id'=>$user->id,'handshake'=>1])	
		->orderBy('created_at','ASC')		
		->paginate(2, ['*'], 'page', $pageNumber);

		$completed_trips->getCollection()->transform(function($completed_trip,$key){
			$completed_trip->amount = number_format($completed_trip->amount,2);;
			return $completed_trip;
		});

		$has_next = true;
		if($completed_trips->nextPageUrl() == null){
			$has_next = false;
		}

		return response()->json(['status'=>true,'message'=>"Completed trips retrived successfully.",'data'=>$completed_trips->all(),'has_next'=>$has_next]);

	}

	/** booking confirmation **/
	public function bookingConfirmation(Request $request){

		$validator = Validator::make($request->all(),[
			'booking_id' 	 => 'required|exists:payments,id',
		]);
		
		if ($validator->fails()){
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
		}

		$user 	 = Auth::user();
		$booking = TripHelper::bookingDetail($request->get('booking_id'));
		if($booking->handshake){
			return response()->json(['status'=>false,'message'=>'Booking already confirmed.']);
		}

		try{

			DB::transaction(function () use ($booking,$user){

				Stripe::setApiKey(config('services.stripe.secret'));
				$trip_user 		= $booking->user;
				$retriveResult 	= \Stripe\Customer::retrieve($trip_user->stripe_id);

				//stripe charge
				$charge = \Stripe\Charge::create(array(
					"amount" 	=> round($booking->total * 100),
					"currency"  => "cad",
					"customer"  =>$trip_user->stripe_id,
					"source"    =>$retriveResult->default_source ,
				));

				if($charge){
					$data['amount'] 			= number_format($booking->total,2);
					$data['payment_id'] 		= $booking->id;
					$data['transaction_id'] 	= $booking->transaction->id;
					$data['charge_id'] 			= $charge->id;
					$data['transaction_id'] 	= $charge->balance_transaction;
					$data['charged_currency']   = $charge->currency;
					$data['failure_code'] 		= ($charge->failure_code != "")?$charge->failure_code:"";
					$data['failure_message'] 	= ($charge->failure_message != "")?$charge->failure_message:"";
					$data['status'] 			= $charge->status;
					$data['transaction_time']	= Carbon::createFromTimestamp($charge->created)->toDateTimeString();;
					Transaction::create($data);

					$booking->handshake 	= true;
					$booking_otp 			= rand(1000,9999);
					$booking->booking_otp   = $booking_otp;
					$booking->save();

					/* send confirm booking notification */
					$trip_user->notify(new BookingConfirmationNotification($booking));
					if($trip_user->active_mode == 'user'){
						NotificationHelper::sendPushNotification($trip_user,'UserBookingConfirmationOtp',$booking->toArray());
					}
				}

			});

		}catch(\Stripe\Error\Card $e){
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		} catch (\Stripe\Error\NotFound $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		}
		catch (\Stripe\Error\Authentication $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		}
		catch (\Stripe\Error\InvalidRequest $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		} catch (\Stripe\Error\RateLimit $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		} catch (\Stripe\Error\ApiConnection $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		} catch (\Stripe\Error\Base $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		}

		return response()->json(['status'=>true,'message'=>"Booking confirmed successfully."]);

	}

	/** booking detail **/
	public function bookingDetail(Request $request){

		$validator = Validator::make($request->all(),[
			'booking_id' 	 => 'required|exists:payments,id',
		]);

		if ($validator->fails()){
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
		}

		$user 						= Auth::user();
		$trip 						= TripHelper::bookingDetail($request->get('booking_id'));
		$data['booking_id'] 		= $trip->id;
		$payment_status 			= 'Pending';
		if($trip->transaction 	   != 	''){
			$payment_status 		= 'Processed';
		}

		$booking_status 			= 'Pending';
		if($trip->handshake){
			$booking_status 		= 'Confirmed';
		}
		$refunded_amont				= 0;
		if($trip->status == '3'){

			$booking_status 		= 'Cancelled';
			if($trip->refund == '' && $trip->handshake){

				$payment_status 		= 'Refund Pending';
				$admin_statistic 		=  TripHelper::getAdminStatistics();				
				$refunded_amont			= $trip->total - $trip->tax;
				$refunded_amont			= $amount - (($amount * $admin_statistic->refund_charge_cent)/100);

			}elseif($trip->handshake){

				$payment_status 		= 'Refunded';
				$refunded_amont			= $trip->refund['amount'];

			}

		}
		$data['booking_status'] 	= $booking_status;

		$data['payment_status'] 	= $payment_status;
		$data['refunded_amont'] 	= $refunded_amont;
		$data['booking_otp'] 		= $trip->booking_otp;

		$data['booking_date'] 		= $trip->created_at;
		$data['from_city'] 			= $trip->trip->from_city;
		$data['to_city'] 			= $trip->trip->to_city;
		$data['pickup_location'] 	= $trip->trip->pickup_location;
		$data['dropoff_location'] 	= $trip->trip->dropoff_location;
		$data['pickup_time'] 		= $trip->trip['trip_date'];
		$data['trip_date'] 			= $trip->trip['pickup_time'];
		$data['trip_status'] 		= TripHelper::trip_status($trip->status);
		$payment_detail				= [ 'price'			=> number_format($trip->trip->price,2),
		'total_seats'	=> $trip->total_person,
		'base_fair'  	=> number_format($trip->trip->price * $trip->total_person,2),
		'tax'			=> number_format($trip->tax),
		'total'			=> number_format($trip->total,2)
	];
	$data['payment_detail'] 	= $payment_detail;

	Stripe::setApiKey(config('services.stripe.secret'));

	$retriveResult				=  Customer::retrieve($user->stripe_id);

	/** card detail **/
	$data['card_detail']		=  array();	

	if(count($retriveResult->sources['data']) > 0){
		$carddetail 	  			=  $retriveResult->sources->retrieve($retriveResult->default_source);
		$card['card_last4']			=  $carddetail->last4;
		$card['brand']				=  $carddetail->brand;
	}
	
	$data['card_detail'] 		= $card;
	$data['driver_info'] 		= $trip->trip->driver;

	return response()->json(['status'=>true,'message'=>"Booking detail retrived successfully.",'data'=>$data]);
}

}
