<?php

namespace App\Http\Controllers\website;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BecomeDriver;
use Validator;
use Mail;
use App\Mail\BecomeDriverMail;

class HomeController extends Controller
{
	public function index(){
        return view('website.index');
    }

    public function becomeDriver(){
        return view('website.become_driver');
    }

    public function cities(){
        return view('website.cities');
    }

    public function driverRequirements(){
        return view('website.driver_requirements');
    }

    public function privacyPolicy()
    {
        return view('website.privacy_policy');
    }

    public function gettingStarted()
    {
        return view('website.getting_started');
    }

    public function contactUs()
    {
        return view('website.contact_us');
    }

    public function faq()
    {
        return view('website.faq');
    }

    public function howToRide()
    {
        return view('website.how_to_ride');
    }

    public function fares()
    {
        return view('website.fares');
    }

    public function addBecomeDriver(Request $request)
    {
        $validator=Validator::make($request->all(), [
                        'first_name' => 'required',
                        'last_name' => 'required',
                        'phone_number' => 'required|numeric',
                        'email' => 'required|email',
                        'currently_live' => 'required',
                        'going_to_drive' => 'required',
                        'most_frequently' => 'required',
                        'ridesharing_company' => 'required',
                        //'company_driving_name' => 'required',
                        'interested_driving' => 'required',
                        'work_permit' => 'required',
                    ]);

        if ( $validator->fails()) {
             $message = array_values(current($validator->messages()))[0][0];
            return response()->json(['status'=>500,'message'=>$message]);
        }

        $data = $request->all();
        $data['most_frequently'] = implode(',', $request->get('most_frequently'));

        $BecomeDriver = BecomeDriver::create($data);

        try{

            Mail::to(['foremostdigital@gmail.com','parthesh@foremostdigital.com','dharmik@foremostdigital.com'])->send(new BecomeDriverMail($BecomeDriver));
            return response()->json([
                'status'  => true,
                'message' => "Thank you! Your request has been submitted",
            ]);
        } catch(Exception $ex){
            return response()->json(['status'=>false,'message'=>"Please try again"]);
        }
    }
}
