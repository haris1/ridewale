<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\Work_state;
use App\Work_country;
use Validator;

class WorkStateController extends Controller
{
    public function create_state(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data,[
                                    'name' => "required",
                                    'tax_percentage' => "required|numeric|between:0,99.99",
                                    'work_country_id' => "required"
                                ]);
        if ($validator->fails()) { 
            return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
        }

        if(Work_state::create($data)){
            return response()->json(['status'=>true,'message'=>'State added successfully.']);  
        } else {
            return response()->json(['status'=>false,'message'=>'Please try again']);
        }
    }

    public function getStateList(Request $request){
        return Datatables::of(Work_state::select('id','name','tax_percentage','work_country_id')
                            ->with('work_country')->get())
        ->addIndexColumn()
        ->addColumn('country', function($state){
            return $state->work_country['name'];
        })
        ->addColumn('tax_percentage', function($state){
            return $state->tax_percentage.'%';
        })
        ->addColumn('action', function($state){
            return '<a href="javascript:void(0)" class="pr-5" title="Edit" data-toggle="modal" data-target="#stateEdit_modal" data-id="'.$state->id.'"  data-name="'.$state->name.'" data-taxPercentage="'.$state->tax_percentage.'" data-country="'.$state->work_country_id.'">
            <i class="zmdi zmdi-edit"></i>
            </a>';
        })
        ->rawColumns(['action','country'])
        ->make(true);
    }


    public function update_state(Request $request){
        $data = $request->all();
        $data=array(
            'name'=>$request->get('name'),
            'tax_percentage'=>$request->get('tax_percentage'),
            'work_country_id'=>$request->get('work_country_id'),
        );
        $validator = Validator::make($data,[
                                    'name'=>"required",
                                    'tax_percentage' => "required|numeric|between:0,99.99",
                                    'work_country_id'=>"required"
                                ]);
        if ($validator->fails()) { 
            return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
        }
        if(Work_state::where('id',$request->get('state_id'))
                       ->update($data)){
            return response()->json(['status'=>true,'message'=>'State update successfully.']);  
        } else {
            return response()->json(['status'=>false,'message'=>'Please try again']);
        }
    }
}
