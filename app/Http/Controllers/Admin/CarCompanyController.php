<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\CarModel;
use App\CarType;
use App\CarCompany;
use Validator;
class CarCompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $types = CarType::get();
        $coms = CarCompany::get();
        $companies = CarCompany::get();
        return view('admin.carcompany',['types'=>$types,'coms'=>$coms,'companies'=>$companies]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data=array(
        'carname'=>$request->get('name'),
        );
        
        $validator=Validator::make($data, [
            'carname' => 'required|string|max:255|unique:car_companies,name',
        ]
        );

        if ( $validator->fails()) { 
             $message = array_values(current($validator->messages()))[0][0];
            return response()->json(['status'=>500,'message'=>$message]);
        } else {

            $addcarcom=array(
                'name'=>$request->get('name'),
                'created_at'=>date('Y-m-d H:i:s'),
            );
            $carcom_id=CarCompany::insertGetId($addcarcom);
            //$taxCatInserted=DB::table('tax_categories')->find($taxCat_id);
            return response()->json(['status'=>200,'message'=>"Tax Categorie added successfully."]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data=array(
        'name'=>$request->get('name'),
        'id'=>$request->get('id'),
        );
        
        $validator=Validator::make($data, [
            'name' => 'required|string|max:255',
            'id' => 'required',
        ]
        );

        if ( $validator->fails()) { 
            $message = array_values(current($validator->messages()))[0][0];
            return response()->json(['status'=>500,'message'=>$message]);
        }else{
            $update_data = array('name'=>$data['name']);
            $update = CarCompany::where('id',$data['id'])->update($update_data);
            
            if($update > 0){
                $notification = array('status'=>200,'message'=>'Car name updatetd successfully');
            }else{
                $notification = array('status'=>500,'message'=>'Something went wrong please try again');
            }

            return response()->json($notification);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $company_name_delete = CarCompany::where('id',$id)->delete();
         if($company_name_delete > 0){
            $notification = array('status'=>200,'message'=>'Car company name deleted successfully');
         }else{
            $notification = array('status'=>500,'message'=>'Something went wrong please try again');
         }
         return response()->json($notification);
    }

    public function list(Request $request){

         return Datatables::of(CarCompany::get())
        ->addIndexColumn()
        ->addColumn('action', function($user){
            return '<a href="javascript:void(0)" class="edit_company" data-toggle="modal" data-target="#editcarcom" data-original-title="Edit" data-id="'.$user->id.'" class="pr-5" data-toggle="tooltip" title="Edit" >
            <i class="zmdi zmdi-edit"></i>
            </a>
            <a href="javascript:void(0)" class="delete_company" data-original-title="Delete" data-id="'.$user->id.'" onclick="deleteCompany('.$user->id.')" class="text-inverse" title="Delete" data-toggle="tooltip"><i class="zmdi zmdi-delete"></i>
            </a>';
        })
        ->rawColumns(['active_mode','action'])
        ->make(true);

    }

    public function get_edit_company_name(Request $request){

        $data = $request->all();

        $company_name = CarCompany::where('id',$data['id'])->get();
        $notification = array('status'=>200,'data'=>$company_name[0]->name,'dataid'=>$company_name[0]->id);
        return response()->json($notification);
    }
}
