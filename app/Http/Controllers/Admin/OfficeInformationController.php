<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\OfficeInformation;
use Yajra\Datatables\Datatables;


class OfficeInformationController extends Controller
{
    //OFFICE LISTING
    public function index()
    {   
        return view('admin.office_informations.officeInfomation');
    }

    //CREATE FORM
    public function createOffice(){
    	return view('admin.office_informations.createOffice');
    }

    //ADD OFFICE
 	public function addOffice(Request $request){
 		$data = $request->all();
 		$data=array(
        'city'=>$request->get('city'),
        'country'=>$request->get('country'),
        'postal_code'=>$request->get('postal_code'),
        'longitude'=>$request->get('longitude'),
        'latitude'=>$request->get('latitude'),
        'full_address'=>$request->get('full_address'),
        'open_day'=>$request->get('open_day'),
        'close_day'=>$request->get('close_day'),
        'open_time'=>$request->get('open_time'),
        'close_time'=>$request->get('close_time'),
        'inquiry_open_day'=>$request->get('inquiry_open_day'),
        'inquiry_close_day'=>$request->get('inquiry_close_day'),
        'inquiry_open_time'=>$request->get('inquiry_open_time'),
        'inquiry_close_time'=>$request->get('inquiry_close_time'),
        'inquiry_hours'=>$request->get('inquiry_hours'),
        'phone'=>$request->get('phone'),
        'email'=>$request->get('email'),
        );
        
        $validator=Validator::make($data, [
	            'city' => 'required|string',
	            'country' => 'required|string',
	            'postal_code' => 'required|string',
	            'longitude' => 'required|numeric',
	            'latitude' => 'required|numeric',
	            'full_address' => 'required|string',
	            'open_day' => 'required|string',
	            'close_day' => 'required|string',
	            'open_time' => 'required|string',
	            'close_time' => 'required|string',
	            'inquiry_open_day' => 'required|string',
	            'inquiry_close_day' => 'required|string',
	            'inquiry_open_time' => 'required|string',
	            'inquiry_close_time' => 'required|string',
	            'phone' => 'required|string',
	            'email' => 'required|string|email',
	        ]
        );

        if ( $validator->fails()) {
             $message = array_values(current($validator->messages()))[0][0];
            return response()->json(['status'=>500,'message'=>$message]);
        } else {
        	$location_hours=$request->get('open_day').','.$request->get('close_day').','.$request->get('open_time').','.$request->get('close_time');
        	$inquiry_hours=$request->get('inquiry_open_day').','.$request->get('inquiry_close_day').','.$request->get('inquiry_open_time').','.$request->get('inquiry_close_time');
            $addData=array(
                'city'=>$request->get('city'),
                'country'=>$request->get('country'),
                'postal_code'=>$request->get('postal_code'),
                'longitude'=>$request->get('longitude'),
                'latitude'=>$request->get('latitude'),
                'full_address'=>$request->get('full_address'),
                'location_hours'=>$location_hours,
                'inquiry_hours'=>$inquiry_hours,
                'phone'=>$request->get('phone'),
                'email'=>$request->get('email'),
            );

            $createOffice=OfficeInformation::create($addData);
            if($createOffice){
            	$response = array('status'=>200,'message'=>"Office Infomation added successfully");
            }else{
                $response = array('status'=>500,'message'=>"Something went wrong please try again");
            }
            return response()->json($response);
        }
    }

    public function listOffice(Request $request){
        return Datatables::of(OfficeInformation::get())
        ->addIndexColumn()
        ->addColumn('action', function($data){
            return '<a href="'.route("admin.office.edit",$data->id).'" class="edit_company" class="pr-5" data-toggle="tooltip" title="Edit" >
            <i class="zmdi zmdi-edit"></i>
            </a>
            <a href="javascript:void(0)" class="delete_company" data-original-title="Delete" data-id="'.$data->id.'" onclick="deleteCompany('.$data->id.')" class="text-inverse" title="Delete" data-toggle="tooltip"><i class="zmdi zmdi-delete"></i>
            </a>';
        })
        ->rawColumns(['action'])
        ->make(true);
    }
       
    public function deleteOffice(request $request){
        $id=$request->get('id');
        $res = OfficeInformation::where('id',$id)->delete();
        if($res){
            return response()->json(['status'=>200,'message'=>'Office Infomation deleted successfully']);
        } else {
           return response()->json(['status'=>500,'alert-type'=>'error','message'=>'Something went wrong please try again']);
        }
    }

    public function editOffice($id){
        $office=OfficeInformation::where('id',$id)->first();
        return view('admin.office_informations.editOffice',compact('office'));           
    }

    public function updateOffice(request $request){
        $id=$request->get('id');
        $OfficeInformation=OfficeInformation::where('id',$id)->first();

        $data = $request->all();
        $data=array(
        'city'=>$request->get('city'),
        'country'=>$request->get('country'),
        'postal_code'=>$request->get('postal_code'),
        'longitude'=>$request->get('longitude'),
        'latitude'=>$request->get('latitude'),
        'full_address'=>$request->get('full_address'),
        'open_day'=>$request->get('open_day'),
        'close_day'=>$request->get('close_day'),
        'open_time'=>$request->get('open_time'),
        'close_time'=>$request->get('close_time'),
        'inquiry_open_day'=>$request->get('inquiry_open_day'),
        'inquiry_close_day'=>$request->get('inquiry_close_day'),
        'inquiry_open_time'=>$request->get('inquiry_open_time'),
        'inquiry_close_time'=>$request->get('inquiry_close_time'),
        'inquiry_hours'=>$request->get('inquiry_hours'),
        'phone'=>$request->get('phone'),
        'email'=>$request->get('email'),
        );
        
        $validator=Validator::make($data, [
                'city' => 'required|string',
                'country' => 'required|string',
                'postal_code' => 'required|string',
                'longitude' => 'required|numeric',
                'latitude' => 'required|numeric',
                'full_address' => 'required|string',
                'open_day' => 'required|string',
                'close_day' => 'required|string',
                'open_time' => 'required|string',
                'close_time' => 'required|string',
                'inquiry_open_day' => 'required|string',
                'inquiry_close_day' => 'required|string',
                'inquiry_open_time' => 'required|string',
                'inquiry_close_time' => 'required|string',
                'phone' => 'required|string',
                'email' => 'required|string',
            ]
        );

        if ( $validator->fails()) {
             $message = array_values(current($validator->messages()))[0][0];
            return response()->json(['status'=>500,'message'=>$message]);
        } else {
            $location_hours=$request->get('open_day').','.$request->get('close_day').','.$request->get('open_time').','.$request->get('close_time');
            $inquiry_hours=$request->get('inquiry_open_day').','.$request->get('inquiry_close_day').','.$request->get('inquiry_open_time').','.$request->get('inquiry_close_time');
            $addData=array(
                'city'=>$request->get('city'),
                'country'=>$request->get('country'),
                'postal_code'=>$request->get('postal_code'),
                'longitude'=>$request->get('longitude'),
                'latitude'=>$request->get('latitude'),
                'full_address'=>$request->get('full_address'),
                'location_hours'=>$location_hours,
                'inquiry_hours'=>$inquiry_hours,
                'phone'=>$request->get('phone'),
                'email'=>$request->get('email'),
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s'),
            );

            if($OfficeInformation->update($addData)){
                $response = array('status'=>200,'message'=>"Office information updated successfully");
            }else{
                $response = array('status'=>500,'message'=>"Something went wrong please try again");
            }
            return response()->json($response);
        }
    }
}
