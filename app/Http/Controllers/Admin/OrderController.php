<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\v3\Booking;
use App\Models\v3\User;
use App\Models\v3\Driver;
use DataTables;
use App\Http\Controllers\Admin\DriverController;
use Carbon\Carbon;

class OrderController extends Controller
{
	//ORDER LIST
    public function index(){
    	return view('admin.order.orderList');
    }

    //GET ORDER LIST
    public function getOrderList(){

        return Datatables::of(Booking::with(['trip'])->orderBy('created_at', 'DESC')->get())
        ->addIndexColumn()
        ->addColumn('date', function($payments){
            return Carbon::parse($payments->created_at)->setTimeZone('Canada/Mountain')->format('d-M-Y');
        })
        ->addColumn('order_id', function($payments){
            return '<a href="'.route('admin.orderDetails',$payments->id).'">'.$payments->id.'</a>';
        })
        ->addColumn('name', function($payments){
            return ($payments->user)?$payments->user->first_name.' '.$payments->user->last_name:'' ;
        })
        ->addColumn('total_person', function($payments){
            return $payments->total_person;
        })
        ->addColumn('price', function($payments){
            return ($payments->trip->price)?'$'.number_format($payments->trip->price,2):'';
        })
        ->addColumn('driver_fare', function($payments){
            return ($payments->driver_fare)?'$'.number_format($payments->driver_fare,2):'';
        })
        ->addColumn('tax_fare', function($payments){
            return ($payments->tax_fare)?'$'.number_format($payments->tax_fare,2):'';
        })
        ->addColumn('transfer_fare', function($payments){
            return ($payments->transfer_fare)?'$'.number_format($payments->transfer_fare,2):'';
        })
        ->addColumn('admin_fare', function($payments){
            return ($payments)?'$'.number_format($payments->admin_fare,2):'';
        })
        ->addColumn('total', function($payments){
            return ($payments->chargeable_amount)?'$'.number_format($payments->chargeable_amount,2):'';
        })
        ->addColumn('pending_seat', function($payments){
            $currentDate = Carbon::now(); 
            $bookingDate = Carbon::parse($payments->created_at);
            $diffTime = $bookingDate->diff($currentDate)->format('%h:%i');
            
            //$pendingSeat = $payments->trip->available_seats;
            return $pendingSeat = ($payments->status == 'pending')?$payments->seats.' ('.$diffTime.')':'-';
        })
        ->addColumn('status', function($payments){
            if($payments->status == 'pending'){
                $status = 'Pending';
            } elseif ($payments->status == 'confirmed') {
             $status = 'Confirmed';
         } elseif ($payments->status == 'cancelled') {
             $status = '<sapn class="text-danger"> Cancelled </span>';
         } elseif ($payments->status == 'picked_up') {
             $status = 'Confirmed';
         } elseif ($payments->status == 'no_show') {
             $status = '<sapn class="text-danger"> No Show</span>';
         } elseif ($payments->status == 'drop_off') {
             $status = 'Confirmed';
         } elseif ($payments->status == 'rejected') {
             $status = '<sapn class="text-danger"> Rejected </sapn>';
         }
         return $status;
     })
        ->rawColumns(['name','admin_fare','tax_fare','driver_fare','transfer_fare','total','order_id','status','pending_seat'])
        ->make(true);

    }

    //ORDER DETAILS
    public function orderDetails($id){
        $booking=Booking::where('id',$id)
        ->with(['user','trip'])
        ->first();

        if($booking->payment != ''){
            $charge=\Stripe\Charge::retrieve(
                $booking->payment->charge_id,
                ['api_key' => config('services.stripe.secret')]
            );
        } else {
            $charge = "";
        }

        $booking->created_date          =  Carbon::parse($booking->created_at)->setTimeZone('Canada/Mountain')->format('d-M-Y @ h:i A');
        $booking->updated_at            =  Carbon::parse($booking->updated_at)->setTimeZone('Canada/Mountain')->toDateTimeString();
        $booking->trip->updated_at      =  Carbon::parse($booking->trip->updated_at)->setTimeZone('Canada/Mountain')->toDateTimeString();


        $booking->trip->trip_dated      =  Carbon::parse($booking->trip->pickup_time)->setTimeZone('Canada/Mountain')->format('d-M-Y');
        $booking->picked_uptime         =  strtotime($booking->picked_up_at) ?Carbon::parse($booking->picked_up_at)->setTimeZone('Canada/Mountain')->format('h:i A'):'';
        //$booking->trip->dropoff_on      =  (strtotime($booking->trip->dropoff_time))?Carbon::parse($booking->trip->dropoff_time)->setTimeZone('Canada/Mountain')->format('h:i A').' MDT on '.Carbon::parse($booking->trip->dropoff_time)->setTimeZone('Canada/Mountain')->format('d-M-Y'):'';
        $booking->dropoff_on      =  strtotime($booking->dropoff_at)?Carbon::parse($booking->dropoff_at)->setTimeZone('Canada/Mountain')->format('h:i A').' MDT on '.Carbon::parse($booking->dropoff_at)->setTimeZone('Canada/Mountain')->format('d-M-Y'):'';

        $totalRides = Booking::where('trip_id',$booking->trip->id)->get()->count();

        return view('admin.order.orderDetails',compact('booking','charge','totalRides'));
    }


    //MAPS
    public function maps(){
        return view('admin.order.maps');
    }

    public function cancelledBookings(Request $request)
    {
        return view('admin.order.cancelled-orders');
    }
}
