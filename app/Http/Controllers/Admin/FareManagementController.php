<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Work_city;
use Validator;
use App\FareManagement;
use DataTables;

class FareManagementController extends Controller
{
    public function index()
    {
    	$cites = Work_city::select('id','name','work_country_id')
    						->where('work_country_id','2')
    						->get();

    	
        return view('admin.fare_management.fare_management',compact('cites'));
    }


    //ADD STORE
    public function addFare(Request $request)
    {
    	$validator = Validator::make($request->all(),[
    								'to_city'=>"required",
    								'from_city'=>"required",
    								'price'=>"required"
    							]);

        if ($validator->fails()) { 
            return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
        }

        $toCity = $request->get('to_city');
        $fromCity = $request->get('from_city');

        /*FareManagement::where([
                                'to_city_id'    => $toCity,
                                'from_city_id'  => $fromCity,
                            ])
                        ->;*/
        FareManagement::updateOrCreate([
		        						'to_city_id' 	=> $toCity,
		        						'from_city_id' 	=> $fromCity,
	        						],[
		        						'to_city_id' 	=> $toCity,
		        						'from_city_id' 	=> $fromCity,
		        						'price' 		=> $request->get('price')
	        						]);
    	return response()->json([
			'status'  => true,
			'message' => "Fare has been successfully created",
		]);
    }

    //LIST FARE
    public function fareList(Request $request){

        return Datatables::of(FareManagement::get())
            ->addIndexColumn()
            ->addColumn('to_city',function($fare){
                return ($fare->to_city)?$fare->to_city->name:'-';
            })
            ->addColumn('from_city',function($fare){
                return ($fare->from_city)?$fare->from_city->name:'-';
            })
            ->addColumn('action', function($fare){
                return '<a href="javascript:void(0)" class="edit_type" data-toggle="modal" data-target="#editFare" data-original-title="Edit" data-id="'.$fare->id.'" data-to-city="'.$fare->to_city_id.'" data-from-city="'.$fare->from_city_id.'" data-price="'.$fare->price.'" class="pr-5" data-toggle="tooltip" title="Edit" >
                    <i class="zmdi zmdi-edit"></i>
                    </a>
                    <a href="javascript:void(0)" class="delete_type" data-toggle="modal" data-target="#deletecartype" data-original-title="Delete" data-id="'.$fare->id.'" onclick="deleteType('.$fare->id.')" class="text-inverse" title="Delete" data-toggle="tooltip"><i class="zmdi zmdi-delete"></i>
                    </a>';
            })
            ->rawColumns(['active_mode','to_city','action'])
            ->make(true);
    }

    //EDIT DARE
    public function editFare(Request $request){
        $validator = Validator::make($request->all(),[
                                    'to_city'=>"required",
                                    'from_city'=>"required",
                                    'price'=>"required"
                                ]);

        if ($validator->fails()) { 
            return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
        }

        $toCity = $request->get('to_city');
        $fromCity = $request->get('from_city');

        FareManagement::updateOrCreate([
                                        'to_city_id'    => $toCity,
                                        'from_city_id'  => $fromCity,
                                    ],[
                                        'to_city_id'    => $toCity,
                                        'from_city_id'  => $fromCity,
                                        'price'         => $request->get('price')
                                    ]);
        return response()->json([
            'status'  => true,
            'message' => "Fare has been successfully updated",
        ]);
    }

    //DELETE FARE
    public function deleteFare($id){
        FareManagement::destroy($id);
        
        return response()->json([
            'status'  => true,
            'message' => "Fare has been successfully delete",
        ]);
    }
}
