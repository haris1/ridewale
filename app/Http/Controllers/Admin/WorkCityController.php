<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\Work_city;
use App\Work_state;
use App\Work_country;
use Validator;

class WorkCityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countries=Work_country::all();
        return view('admin.work_cities.work_cities',compact('countries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=array(
            'name'=>$request->get('name'),
            'work_country_id'=>$request->get('work_country_id'),
            'work_state_id'=>$request->get('work_state_id'),
            'timezone_name'=>$request->get('timezone_name'),
            'timezone'=>$request->get('timezone')
        );
        $validator = Validator::make($data,[
            'name'=>"required",
            'work_country_id'=>"required",
            'work_state_id'=>"required",
            'timezone'=>"required",
            'timezone_name'=>"required"
        ]);

        if ($validator->fails()) { 
            return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
        }       
        Work_city::create($data);
        return response()->json(['status'=>true,'message'=>'City added successfully.']);  
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Work_city  $work_city
     * @return \Illuminate\Http\Response
     */
    public function show(Work_city $work_city)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Work_city  $work_city
     * @return \Illuminate\Http\Response
     */
    public function edit(Work_city $work_city)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Work_city  $work_city
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Work_city $work_city)
    {
        $data=[
            'name'=>$request->get('name'),
            'work_country_id'=>$request->get('work_country_id'),
            'work_state_id'=>$request->get('work_state_id'),
            'timezone_name'=>$request->get('timezone_name'),
            'timezone'=>$request->get('timezone')
        ];
        $validator = Validator::make($data,['name'=>"required",
            'work_country_id'=>'required',
            'timezone'=>"required",
            'timezone_name'=>"required"
        ]);

        if ($validator->fails()) { 
            return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
        }       
        Work_city::where('id',$work_city->id)->update($data);
        return response()->json(['status'=>true,'message'=>'City updated successfully.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Work_city  $work_city
     * @return \Illuminate\Http\Response
     */
    public function destroy(Work_city $work_city)
    {
        Work_city::where('id',$work_city->id)->delete();
        return response()->json(['status'=>true,'message'=>'City deleted successfully.']); 
    }

    public function show_work_cities(Request $request){
        return Datatables::of(Work_city::select('id','name','work_country_id','work_state_id','timezone_name','timezone')->with('work_country')->with('work_state'))
        ->addIndexColumn()
        ->addColumn('country', function($city){
            return $city->work_country['name'];
        })
        ->addColumn('state', function($city){
            return $city->work_state['name'];
        })
        ->editColumn('timezone', function($city){
            return $city['timezone'].'('.$city['timezone_name'].')';
        })
        ->addColumn('action', function($city){
            return '<a href="javascript:void(0)" class="pr-5" title="Edit" data-toggle="modal" data-target="#cityEdit_modal" data-id="'.$city->id.'" data-name="'.$city->name.'" data-timezone="'.$city->timezone.'" data-timezone_name="'.$city->timezone_name.'" data-country="'.$city->work_country_id.'" data-state="'.$city->work_state_id.'">
            <i class="zmdi zmdi-edit"></i>
            </a>';
        })
        ->rawColumns(['action'])
        ->make(true);
    }


    public function get_state($id){
        $states=Work_state::where('work_country_id',$id)->get();
        $html='<option value="">Select City</option>';
        foreach ($states as $key => $state) {
            $html.='<option value="'.$state->id.'">'.$state->name.'</option>';
        }
        if(count($states) <= 0){
            $html.='<option value="">No state Found.</option>';
        }
        return $html;//die();
    }
}
