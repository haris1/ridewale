<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use Validator;
use App\Models\v3\AllowedNotallowedThing;

class AllowedThingsController extends Controller
{
	public function index()
	{   
		return view('admin.allowed_things.index');
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    	$validator = Validator::make($request->all(),['description'=>"required"]);

    	if ($validator->fails()) { 
    		return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
    	}       

    	AllowedNotallowedThing::create($request->all());

    	if($request->get('allowed')){
    		return response()->json(['status'=>true,'message'=>'Allowed things added successfully.']);            
    	}

    	return response()->json(['status'=>true,'message'=>'Not allowed things added successfully.']);   
    	         
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Work_country  $work_country
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Work_country  $work_country
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Work_country  $work_country
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

    	$validator = Validator::make($request->all(),['description'=>"required"]);

    	if ($validator->fails()) { 
    		return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
    	}

    	$id = $request->get('id');

    	AllowedNotallowedThing::where('id',$id)->update(array('description'=>$request->get('description')));

    	if(AllowedNotallowedThing::find($id)->allowed){
    		return response()->json(['status'=>true,'message'=>'Allowed things updated successfully.']);   
    	}

    	return response()->json(['status'=>true,'message'=>'Not-allowed things updated successfully.']); 

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Work_country  $work_country
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    	AllowedNotallowedThing::where('id',$id)->delete();
    	return response()->json(['status'=>true,'message'=>'Information deleted successfully.']);

    }

    public function show_allowed_things(Request $request){

    	return Datatables::of(AllowedNotallowedThing::where('allowed',1)->select('id','description'))
			    	->addIndexColumn()
			    	->addColumn('action', function($allowed_things){

			    		return '<a href="javascript:void(0)" class="pr-5" title="Edit" data-toggle="modal" data-target="#allwedthingsEdit_modal" data-id="'.$allowed_things->id.'" data-description="'.$allowed_things->description.'"><i class="zmdi zmdi-edit"></i></a><a href="javascript:void(0)" onclick="deleteAllowedthing('.$allowed_things->id.')" class="text-inverse" title="Delete" data-toggle="tooltip"><i class="zmdi zmdi-delete"></i>
			    		</a>';
			    	})
			    	->rawColumns(['action'])
			    	->make(true);

    }

    public function show_notallowed_things(Request $request){

    	return Datatables::of(AllowedNotallowedThing::where('allowed',0)->select('id','description'))
			    	->addIndexColumn()
			    	->addColumn('action', function($allowed_things){

			    		return '<a href="javascript:void(0)" class="pr-5" title="Edit" data-toggle="modal" data-target="#allwedthingsEdit_modal" data-id="'.$allowed_things->id.'" data-description="'.$allowed_things->description.'"><i class="zmdi zmdi-edit"></i></a><a href="javascript:void(0)" onclick="deleteAllowedthing('.$allowed_things->id.')" class="text-inverse" title="Delete" data-toggle="tooltip"><i class="zmdi zmdi-delete"></i>
			    		</a>';
			    	})
			    	->rawColumns(['action'])
			    	->make(true);

    }
}
