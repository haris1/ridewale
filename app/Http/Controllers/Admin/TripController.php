<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect;
use DataTables;
use App\Models\v3\Trip;
use App\Models\v3\Payment;
use App\Models\v3\User;
use App\Models\v3\Driver;
use App\Models\v3\Booking;
use DB;
use App\Http\Controllers\Admin\DriverController;
use Carbon\Carbon;
use Validator;
use Stripe\Stripe;
use Stripe\Refund;
use Stripe\Charge;

class TripController extends Controller
{
    public function trip_history_upcoming(){
        return view('admin.tripHistory')->with('type', 'upcoming');
    }

    public function trip_history_complete(){
        return view('admin.tripHistory')->with('type', 'complete');
    }


    public function get_trip_list(Request $request){
        // $data = $request->all();
        // $dates=explode('-',$request->get('started_at'));
        // $from=date('Y-m-d 00:00:00',strtotime($dates[0]));
        // $to=date('Y-m-d 00:00:00',strtotime($dates[1]));
        //$trips  = Trip::with(['from_city','to_city','payments','vehicle'])->where('trips.status',$request->get('status'))->whereBetween('trips.created_at', array($from,$to))->get();
        $trips = Trip::with(['from_location','to_location','bookings','vehicle'])->get();
        return DataTables::of($trips)
        ->addIndexColumn()
        ->addColumn('created_date', function($trips){
            return Carbon::parse($trips->created_at)->setTimeZone('Canada/Mountain')->format('M-d');
        })
        ->addColumn('trip_date', function($trips){
            return Carbon::parse($trips->trip_date)->setTimeZone('Canada/Mountain')->format('M-d');
        })
        ->addColumn('from_station', function($trips){
            return ($trips->from_location->work_city)?$trips->from_location->work_city->name:'';
        })
        ->addColumn('to_station', function($trips){
            return ($trips->to_location->work_city)?$trips->to_location->work_city->name:'';
        })
        ->addColumn('trip_by', function($trips){
            return ($trips->driver->user)?$trips->driver->user->first_name.' '.$trips->driver->user->last_name:'';
        })
        ->addColumn('seat_amount', function($trips){
            return ($trips->price)?'$'.$trips->price:'';
        })
        ->addColumn('total_seats', function($trips){
            return ($trips->total_seats)?$trips->total_seats:'';
        })
        ->addColumn('booked_seats', function($trips){
            return $trips->total_seats-$trips->available_seats;
        })
        ->addColumn('total_riders', function($trips){
            return $trips->total_seats;
        })
        ->addColumn('trip_no', function($trips){
            return '<a href="'.route("admin.trip.details",$trips->id).'" class="text-inverse" title="View Detail" data-toggle="tooltip">'.$trips->id.'</a>';
        })
        ->addColumn('trip_status', function($trips){
            if($trips->status == 'scheduled'){
                $status = 'Scheduled';
            } elseif ($trips->status == 'started') {
                $status = 'Started';
            } elseif ($trips->status == 'completed') {
                $status = 'Completed';
            } elseif ($trips->status == 'cancelled') {
                $status = '<span class="text-danger"> Cancelled by driver </span>';
            }
            return $status;
        })
        ->addColumn('pending_confirmation', function($trips){
            return $trips->bookings()->where('status','pending')->count();
        })
        ->rawColumns(['created_date','trip_date','from_station','to_station','trip_by','seat_amount','total_seats','booked_seats','total_riders','trip_no','trip_status','pending_confirmation'])
        ->make(true);
    }

    public function get_trip_details($id){
        $trip  = Trip::where('id',$id)
        ->with('bookings','activity_log')
        ->firstOrFail();
        
        $trip->bookings->map(function($query){
            $query->created_date = Carbon::parse($query->created_at)->setTimeZone('Canada/Mountain')->format('d-M-Y @ h:i A');
        });
        
        $trip->trip_dated      =  Carbon::parse($trip->pickup_time)->setTimeZone('Canada/Mountain')->format('d-M-Y');
        $trip->trip_date       =  Carbon::parse($trip->trip_date)->setTimeZone('Canada/Mountain')->toDateTimeString();
        $trip->picked_uptime   =  Carbon::parse($trip->pickup_time)->setTimeZone('Canada/Mountain')->format('h:i A');
        $trip->dropoff_on      =  (strtotime($trip->dropoff_time) && $trip->dropoff_time != '-0001-11-30 00:00:00')?Carbon::parse($trip->dropoff_time)->setTimeZone('Canada/Mountain')->format('h:i A').' MDT on '.Carbon::parse($trip->dropoff_time)->setTimeZone('Canada/Mountain')->format('d-M-Y'):'';

        $html = '';
        foreach($trip->activity_log as $activity_log){

            $logFor = $activity_log->activity;
            switch ($logFor) {
                case 'newTrip':
                    $html .= '<li>- Trip created by <b>'.$trip->driver->user->first_name.' '.$trip->driver->user->last_name.'</b> on '.Carbon::parse($activity_log->created_at)->setTimeZone('Canada/Mountain')->format('d-M-Y @ h:i A').' MDT </li>';
                break;

                case 'updateSeat':
                    $html .= '<li>- Seat updated by <b>'.$trip->driver->user->first_name.' '.$trip->driver->user->last_name.'</b> on '.Carbon::parse($activity_log->created_at)->setTimeZone('Canada/Mountain')->format('d-M-Y @ h:i A').' MDT </li>';
                break;

                case 'updateTrip':
                    $html .= '<li>- Trip updated by <b>'.$trip->driver->first_name.' '.$trip->driver->last_name.'</b> on '.Carbon::parse($activity_log->created_at)->setTimeZone('Canada/Mountain')->format('d-M-Y @ h:i A').' MDT </li>';
                break;

                case 'newBooking':
                    $html .= '<li>- New booking by <b>'.$activity_log->user->first_name.' '.$activity_log->user->last_name.'</b> on '.Carbon::parse($activity_log->created_at)->setTimeZone('Canada/Mountain')->format('d-M-Y @ h:i A').' MDT </li>';
                break;

                case 'tripCancelled':
                    $html .= '<li>- Trip <span class="danger">cancelled</span> by <b>'.$trip->driver->first_name.' '.$trip->driver->last_name.'</b> on '.Carbon::parse($activity_log->created_at)->setTimeZone('Canada/Mountain')->format('d-M-Y @ h:i A').' MDT </li>';
                break;

                case 'bookingDeclined':
                    $html .= '<li>- Driver <span class="danger">decline booking</span> for Booking ID <b>#'.$activity_log->booking_id.'</b> on '.Carbon::parse($activity_log->created_at)->setTimeZone('Canada/Mountain')->format('d-M-Y @ h:i A').' MDT </li>';
                break;

                case 'bookingCancelled':
                    $html .= '<li>- Booking <span class="danger">cancelled</span> by <b>'.$activity_log->user->first_name.' '.$activity_log->user->last_name.'</b> on '.Carbon::parse($activity_log->created_at)->setTimeZone('Canada/Mountain')->format('d-M-Y @ h:i A').' MDT </li>';
                break;

                case 'bookingConfirmed':
                    $html .= '<li>- Driver <span class="success">confirm booking</span> for Booking ID <b>#'.$activity_log->booking_id.'</b> on '.Carbon::parse($activity_log->created_at)->setTimeZone('Canada/Mountain')->format('d-M-Y @ h:i A').' MDT </li>';
                break;

                case 'noShowByDriver':
                    $html .= '<li>- Driver reported <b>'.$activity_log->user->first_name.' '.$activity_log->user->last_name.'</b> as <span class="info">NO SHOW</span> on '.Carbon::parse($activity_log->created_at)->setTimeZone('Canada/Mountain')->format('d-M-Y @ h:i A').' MDT </li>';
                break;

                case 'noShowByRider':
                    $html .= '<li>- <b>'.$activity_log->user->first_name.' '.$activity_log->user->last_name.'</b> reported Driver as <span class="info">NO SHOW</span> on '.Carbon::parse($activity_log->created_at)->setTimeZone('Canada/Mountain')->format('d-M-Y @ h:i A').' MDT </li>';
                break;

                case 'tripStarted':
                    $html .= '<li>- Trip <span class="success">started</span> on '.Carbon::parse($activity_log->created_at)->setTimeZone('Canada/Mountain')->format('d-M-Y @ h:i A').' MDT </li>';
                break;

                case 'tripCompleted':
                    $html .= '<li>- Trip <span class="success">completed</span> on '.Carbon::parse($activity_log->created_at)->setTimeZone('Canada/Mountain')->format('d-M-Y @ h:i A').' MDT </li>';
                break;

                case 'pickedUp':
                    $html .= '<li>- Driver picked up <b>'.$activity_log->user->first_name.' '.$activity_log->user->last_name.'</b> on '.Carbon::parse($activity_log->created_at)->setTimeZone('Canada/Mountain')->format('d-M-Y @ h:i A').' MDT </li>';
                break;

                case 'dropOff':
                    $html .= '<li>- Driver dropped off <b>'.$activity_log->user->first_name.' '.$activity_log->user->last_name.'</b> on '.Carbon::parse($activity_log->created_at)->setTimeZone('Canada/Mountain')->format('d-M-Y @ h:i A').' MDT </li>';
                break;
                
                default:
                    # code...
                    break;
            }
        }
                       
        return view('admin.trip_details',compact('trip','html'));
    }

    public function list_user_trips($id){
        $drivercontroller  = new DriverController;
        $data              = $drivercontroller->get_sidebar_data($id);
        $bookings  = Booking::with(['trip'])
        ->where('user_id',$id)
        ->get();
        return view('admin.users.triplist',compact('bookings','data'));
    }

    public function list_driver_trips($id){
        $drivercontroller = new DriverController;
        $data             = $drivercontroller->get_sidebar_data($id);
        $driver           = Driver::where('user_id',$id)->first();
        $trips            = Trip::with(['from_location','to_location','bookings','vehicle'])->where('trips.driver_id',$driver['id'])->get();
        return view('admin.drivers.triplist',compact('trips','data'));
    }


    public function cancelledBookings(){
        return view('admin.cancelled_trips');
    }



    //GET RIDER STATUS
    public function getRiderStatus(Request $request){
        $status = $request->get('status');

        $bookings = Booking::with(['trip'])->where('status','cancelled');

        if ($status == 'refund') {
            $bookings->whereHas('payment', function ($query)  {
                $query->whereNotNull('refunded_id');
            });
        } else {
            $bookings->whereHas('payment', function ($query) {
                $query->whereNull('refunded_id');
            });
        }
        $bookings = $bookings->get();
        return Datatables::of($bookings)
        ->addIndexColumn()
        ->addColumn('date', function($bookings){
            return Carbon::parse($bookings->created_at)->setTimeZone('Canada/Mountain')->format('M-d');
        })
        ->addColumn('order_id', function($bookings){
            return '<a href="'.route('admin.orderDetails',$bookings->id).'">'.$bookings->id.'</a>';
        })
        ->addColumn('cancel_date', function($bookings){
            //return ($bookings->trip->driver_penalties)?date('Y-M-d',strtotime($bookings->trip->driver_penalties->created_at)):'-';
            return Carbon::parse($bookings->created_at)->setTimeZone('Canada/Mountain')->format('M-d');
        })
        ->addColumn('penalty_amount', function($bookings){
            return ($bookings->trip->driver_penalties)?'$'.number_format($bookings->trip->driver_penalties->penalty_amount,2):'';
        })
        ->addColumn('refund_amount', function($bookings){
            return ($bookings->payment)?'$'.number_format($bookings->payment->refunded_amount,2):'';
        })
        ->addColumn('amount', function($bookings){
            return ($bookings->base_fare)?'$'.number_format($bookings->base_fare,2):'';
        })
        ->addColumn('tax_fare', function($bookings){
            return ($bookings->tax_fare)?'$'.number_format($bookings->tax_fare,2):'';
        })
        ->addColumn('net_amount', function($bookings){
            return ($bookings->chargeable_amount)?'$'.number_format($bookings->chargeable_amount,2):'';
        })
        ->addColumn('status', function($bookings){
            if (!$bookings->payment) {
                return "Not Charged";
            }
            return ($bookings->payment->refunded_id!="")?'Refund Pending':'Refunded';
        })
        ->addColumn('decline_reason', function($bookings){
            return ($bookings->decline_reason)?$bookings->decline_reason:'-';
        })
        ->rawColumns(['date','cancel_date','tax_fare','refund_amount','penalty_amount','net_amount','amount','order_id'])
        ->make(true);
    }


    //GET DRIVER STATUS
    public function getDriverStatus(Request $request){
        $status = $request->get('status');

        $bookings = Booking::whereHas('trip', function ($query) {
            $query->where('status','cancelled');
        })
        ->with(['trip']);

        if ($status == 'refund') {
            $bookings->whereHas('payment', function ($query)  {
                $query->whereNotNull('refunded_id');
            });
        } else {
            $bookings->whereHas('payment', function ($query) {
                $query->whereNull('refunded_id');
            });
        }

        $bookings = $bookings->get();
        return Datatables::of($bookings)
        ->addIndexColumn()
        ->addColumn('date', function($bookings){
            return Carbon::parse($bookings->created_at)->setTimeZone('Canada/Mountain')->format('M-d');
        })
        ->addColumn('order_id', function($bookings){
            return '<a href="'.route('admin.orderDetails',$bookings->id).'">'.$bookings->id.'</a>';
        })
        ->addColumn('cancel_date', function($bookings){
            //return ($bookings->trip->driver_penalties)?date('Y-M-d',strtotime($bookings->trip->driver_penalties->created_at)):'';
            return Carbon::parse($bookings->trip->created_at)->setTimeZone('Canada/Mountain')->format('M-d');
        })
        ->addColumn('penalty_amount', function($bookings){
            return ($bookings->trip->driver_penalties)?'$'.number_format($bookings->trip->driver_penalties->penalty_amount,2):'';
        })
        ->addColumn('refund_amount', function($bookings){
            return ($bookings->payment)?'$'.number_format($bookings->payment->refunded_amount,2):'';
        })
        ->addColumn('amount', function($bookings){
            return ($bookings->base_fare)?'$'.number_format($bookings->base_fare,2):'';
        })
        ->addColumn('tax_fare', function($bookings){
            return ($bookings->tax_fare)?'$'.number_format($bookings->tax_fare,2):'';
        })
        ->addColumn('net_amount', function($bookings){
            return ($bookings->chargeable_amount)?'$'.number_format($bookings->chargeable_amount,2):'';
        })
        ->addColumn('status', function($bookings){
            if (!$bookings->payment) {
                return "Not Charged";
            }
            return ($bookings->payment->refunded_id!="")?'Refund Pending':'Refunded';
        })
        ->addColumn('cancelled_reason', function($bookings){
            return ($bookings->trip->cancelled_reason)?$bookings->trip->cancelled_reason:'-';
        })
        ->rawColumns(['date','cancel_date','tax_fare','refund_amount','penalty_amount','net_amount','amount','cancelled_reason','order_id'])
        ->make(true);
    }    

    //REPORTED AS NO SHOW
    public function getReportStatus(Request $request){
        $status = $request->get('status');

        $bookings = Booking::with(['trip'])->where('status','no_show');

        if ($status == 'refund') {
            $bookings->whereHas('payment', function ($query)  {
                $query->whereNotNull('refunded_id');
            });
        } else {
            $bookings->whereHas('payment', function ($query) {
                $query->whereNull('refunded_id');
            });
        }

        $bookings = $bookings->get();

        return Datatables::of($bookings)
        ->addIndexColumn()
        ->addColumn('date', function($bookings){
            return Carbon::parse($bookings->created_at)->setTimeZone('Canada/Mountain')->format('M-d');
        })
        ->addColumn('order_id', function($bookings){
            return '<a href="'.route('admin.orderDetails',$bookings->id).'">'.$bookings->id.'</a>';
        })
        ->addColumn('cancel_date', function($bookings){
            //return ($bookings->trip->driver_penalties)?date('Y-M-d',strtotime($bookings->trip->driver_penalties->created_at)):'';
            return Carbon::parse($bookings->no_show_at)->setTimeZone('Canada/Mountain')->format('M-d');
        })
        ->addColumn('penalty_amount', function($bookings){
            return ($bookings->trip->driver_penalties)?'$'.number_format($bookings->trip->driver_penalties->penalty_amount,2):'';
        })
        ->addColumn('refund_amount', function($bookings){
            return ($bookings->payment)?'$'.number_format($bookings->payment->refunded_amount,2):'';
        })
        ->addColumn('amount', function($bookings){
            return ($bookings->base_fare)?'$'.number_format($bookings->base_fare,2):'';
        })
        ->addColumn('tax_fare', function($bookings){
            return ($bookings->tax_fare)?'$'.number_format($bookings->tax_fare,2):'';
        })
        ->addColumn('net_amount', function($bookings){
            return ($bookings->chargeable_amount)?'$'.number_format($bookings->chargeable_amount,2):'';
        })
        ->addColumn('status', function($bookings){
            if (!$bookings->payment) {
                return "Not Charged";
            }
            return ($bookings->payment->refunded_id!="")?'Refund Pending':'Refunded';
        })
        ->editColumn('decline_reason', function($bookings){
            return ($bookings->decline_reason)?$bookings->decline_reason:'-';
        })
        ->rawColumns(['date','cancel_date','tax_fare','refund_amount','penalty_amount','net_amount','amount','decline_reason','order_id'])
        ->make(true);
    }    

    public function giveBookingRefund($booking_id){
        $trip_id = Booking::find($booking_id)->trip_id;

        return view('admin.refund',compact('booking_id','trip_id'));
    }

    public function bookingRefund(Request $request,$booking_id){

        $validator = Validator::make($request->all(),[
            'amount'     => "required"
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->messages()->first()]);
        }
        
        Stripe::setApiKey(config('services.stripe.secret'));

        try {
            $booking = Booking::find($booking_id);
            DB::transaction(function () use ($booking,$request){

                /** refund to user **/
                $amount                     =  $request->get('amount');

                $refund = Refund::create(array(
                    'charge'  => $booking->payment->charge_id ,
                    'amount'  => round(( $amount * 100)),
                    'metadata'=> array('booking_id' => $booking->id)
                ));

                if($refund){

                    $data['refunded_amount']        = round($amount ,2);
                    $data['refunded_id']            = $refund->id;
                    $data['refund_status']          = $refund->status;
                    $data['refunded_time']          = Carbon::createFromTimestamp($refund->created)->toDateTimeString();
                    $booking->payment->update($data);

                }

            });

        } catch(\Stripe\Error\Card $e) {
            return response()->json(['status' => false,'message' => $e->getMessage()]);
        } catch (\Stripe\Error\InvalidRequest $e) {
            return response()->json(['status' => false,'message' => $e->getMessage()]);
        } catch (\Stripe\Error\Authentication $e) {
            return response()->json(['status' => false,'message' => $e->getMessage()]);
        } catch (\Stripe\Error\ApiConnection $e) {
            return response()->json(['status' => false,'message' => $e->getMessage()]);
        } catch (\Stripe\Error\Base $e) {
            return response()->json(['status' => false,'message' => $e->getMessage()]);
        } catch (Exception $e) {
            return response()->json(['status' => false,'message' => $e->getMessage()]);
        }

        return response()->json(['status'=>true,'message'=>"Amount refunded successfully."]);
    }

    public function chargeDriver($trip_id){
        return view('admin.charge_driver',compact('trip_id'));
    }

    public function tripCharge(Request $request,$trip_id){

        $validator = Validator::make($request->all(),[
            'amount'     => "required"
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->messages()->first()]);
        }
        
        $trip = Trip::find($trip_id);

        /** Driver account balance **/
        $driver_trips       = $trip->driver->trips()
                                    ->whereHas('bookings', function ($query) {
                                        $query->whereHas('payment')->where('status','drop_off');
                                    })
                                    ->with(['bookings'=> function ($query) {
                                        $query->where('status','drop_off');
                                    }])
                                    ->where('status','completed')
                                    ->select('id')
                                    ->get();

        $driver_trips->map(function($trip,$key){
            $trip->driver_fare += $trip->bookings->sum('driver_fare');
            return $trip;
        });

        $total_earning          = $driver_trips->sum('driver_fare');
        $total_withdraws        = $trip->driver->payouts()->sum('payout_amount');
        $penaltyUser            = $trip->driver->driver_penalties->sum('penalty_amount');
        $account_balance        = $driver_trips->sum('driver_fare') - $penaltyUser - $total_withdraws;

        $penalty['trip_id']        = $trip_id;
        $penalty['penalty_amount'] = $request->get('amount');
        $penalty['penalty_type']   = 'fixed';
        
        $penalty['utilize']            = 0;
        if($account_balance > $penalty['penalty_amount']){
            $penalty['utilize']        = 1;
        }             

        $trip->driver->driver_penalties()->create($penalty);  

        return response()->json(['status'=>true,'message'=>"Amount charge from driver wallet successfully."]);
    }
}

