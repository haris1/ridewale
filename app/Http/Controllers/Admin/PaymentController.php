<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\v3\Payment;
use App\Models\v3\Booking;
use App\Models\v3\User;
use App\Models\v3\Trip;
use App\Models\v3\Driver;
use DataTables;
use App\Http\Controllers\Admin\DriverController;
use App\Models\v3\DriverPenalty;
use App\Models\v3\Payout;


class PaymentController extends Controller
{
    public function get_payment_history(){
    	return view('admin.paymentHistory');
    }

    public function get_payment_list(Request $request){

        $dates=explode('-',$request->get('started_at'));
        $from=date('Y-m-d 00:00:00',strtotime($dates[0]));
        $to=date('Y-m-d 00:00:00',strtotime($dates[1]));

        return Datatables::of(Booking::with(['trip'])->whereBetween('bookings.created_at', array($from,$to))->orderBy('id', 'DESC')->get())
        ->addIndexColumn()
        ->addColumn('user_first_name', function($payments){
                //return ($payments->user)?$payments->user->first_name:''.' '.($payments->user)?$payments->user->last_name:'' ;
            return "<a href=".route('admin.users.show',$payments->user->id)." class='a_tag_color' title='View Detail' data-toggle='tooltip'><span class='txt-dark weight-500'>".$payments->user->first_name.' '.$payments->user->last_name."</span></a>";
                //return $payments->user->first_name.' '.$payments->user->last_name;
        })
        ->addColumn('from', function($payments){
            //return ($payments->trip->from_city)?$payments->trip->from_city->name:'';
            return ($payments->trip->from_location->work_city->name)?$payments->trip->from_location->work_city->name:'';
        })
        ->addColumn('to', function($payments){
            //return ($payments->trip->to_city)?$payments->trip->to_city->name:'';
            return ($payments->trip->to_location->work_city->name)?$payments->trip->to_location->work_city->name:'';
        })
        ->addColumn('admin_fees', function($payments){
            return ($payments)?'$'.number_format($payments->admin_fare,2):'';
        })
        ->addColumn('driver_fees', function($payments){
            return ($payments)?'$'.number_format($payments->driver_fare,2):'';
        })
            // ->addColumn('cash_back', function($payments){
            //     return ($payments)?'$'.number_format($payments->cash_back,2):'';
            // })
        ->addColumn('total', function($payments){
            return ($payments)?'$'.number_format($payments->chargeable_amount,2):'';
        })
        ->rawColumns(['user_first_name'])
        ->make(true);
    }

    public function get_upcoming_trip_list(){

        $data   = array();
        $result = Trip::join('drivers', 'trips.driver_id', '=', 'drivers.id')
        ->join('users', 'drivers.user_id', '=', 'users.id')
        ->where('trips.status', '=','scheduled')
        ->select('trips.*','drivers.id as driver_id','users.id as userid','users.first_name as driver_first_name','users.last_name as driver_last_name')
        ->orderBy('id', 'DESC')
        ->limit(5)
        ->get();

        $upcoming_trip_list = array();
        foreach($result as $res){
            
            $filled_seats = $res->seats - $res->available_seats;
            $data = array('driver_first_name'=>$res->driver_first_name,'driver_last_name'=>$res->driver_last_name,'trip_date'=>$res->trip_date,'pickup_time'=>$res->pickup_time,'total'=>$res->price,'status'=>$res->status,'filled_seats'=>$filled_seats,'driver_id'=>$res->driver_id,'trip_id'=>$res->id);

            array_push($upcoming_trip_list,$data);

        }
        return $upcoming_trip_list;        
    }

    public function get_complete_trip_list(){

        $data   = array();
        $result = Booking::join('trips', 'bookings.trip_id', '=', 'trips.id')
        ->join('drivers', 'trips.driver_id', '=', 'drivers.id')
        ->join('users', 'drivers.user_id', '=', 'users.id')
        ->where('trips.status', '=','completed')
        ->select('bookings.*','drivers.id as driver_id','users.id as userid','users.first_name as driver_first_name','users.last_name as driver_last_name','trips.trip_date as trip_date','trips.pickup_time as time','trips.status as status','trips.total_seats as total_seats','trips.available_seats as available_seats')
        ->orderBy('id', 'DESC')
        ->limit(5)
        ->get();

        $upcoming_trip_list = array();
        foreach($result as $res){
            $users = User::where('id',$res->user_id)->get();
            $userid = Driver::where('id',$res->driver_id)->get();
            $drivers = User::where('id',$userid[0]->user_id)->get();
            $filled_seats = $res->total_seats - $res->available_seats;
            $data = array('payment_id'=>$res->id,'driver_first_name'=>$drivers[0]->first_name,'driver_last_name'=>$drivers[0]->last_name,'trip_date'=>$res->trip_date,'pickup_time'=>$res->pickup_time,'total'=>$res->total,'user_first_name'=>$users[0]->first_name,'user_last_name'=>$users[0]->last_name,'status'=>$res->status,'filled_seats'=>$filled_seats,'driver_id'=>$res->driver_id,'trip_id'=>$res->id);

            array_push($upcoming_trip_list,$data);
        }     
        return $upcoming_trip_list;        
    }

    public function list_user_payments($id){

       $drivercontroller = new DriverController;
       $data             = $drivercontroller->get_sidebar_data($id);
       $bookings         = Booking::with(['trip','user'])->whereHas('payment')->where('user_id',$id)->get();
       
       return view('admin.users.paymentlist',compact('bookings','data'));
   }

   public function list_Driver_payments($id){

        // $data = User::where('id',$id)->get();
        $drivercontroller   = new DriverController;
        $data               = $drivercontroller->get_sidebar_data($id);
        $driver          = Driver::where('user_id',$id)->first();  

        $Payments           = Booking::with(['trip','user'])->where('user_id',$driver['id'])->get();
        return view('admin.drivers.paymentlist',compact('Payments','data'));
    }


    //TOTAL EARNING
    public function totalEarning(){
        $bookings = Booking::whereHas('payment')
                            ->with(['payment'])
                            ->get();


        $payment = Payment::whereHas('booking')
                            ->get();
        $refundedBooking = Payment::whereNotNull('refunded_id');
        /*$bookingRefund = Booking::whereIn('id',$refundedBookingId)
                                ->get();*/
        $bookingUpcoming = Booking::whereDoesntHave('payment')
                                    ->where('status','!=','cancelled')
                                    ->where('status','!=','rejected')
                                    ->with(['payment'])
                                    ->get();
        
        $driverPenalty = DriverPenalty::whereHas('trip')
                                ->get();

        $payouts = Payout::get();
        $pendingCredits  = $bookings->where('status','!=','drop_off')
                                    ->where('status','!=','cancelled')
                                    ->sum('driver_fare');

        $data['totalCollect']           = $bookings->sum('chargeable_amount');
        $data['stripFees']              = $bookings->sum('transfer_fare');
        $data['totalReceived']          = $data['totalCollect']-$data['stripFees'];
        $data['taxReceived']            = $bookings->sum('tax_fare');
        //$data['taxRefunded']            = $refundedBookingId->sum('refunded_tax');
        $data['taxRefunded']            = $refundedBooking->sum('refunded_tax');
        $data['estimatedEarnings']      = $bookings->where('status','!=','cancelled')->sum('driver_fare');
        $data['totalRefunded']          = $payment->sum('refunded_amount');
        $data['totalPenaltie']          = $driverPenalty->sum('penalty_amount');       
        //$data['netEarnings']            = $data['estimatedEarnings']-$data['totalPenaltie'];
        $data['paidTodriver']           = $payouts->sum('payout_amount');
        //$data['credits']                = ($bookings->where('status','==','drop_off')->sum('driver_fare')-$data['totalPenaltie'])-$data['paidTodriver'];
        $data['pendingCredits']         = $pendingCredits;
        $data['penaltiePais']           = $driverPenalty->where('utilize','1')->sum('penalty_amount');
        $data['penaltiePending']        = $driverPenalty->where('utilize','0')->sum('penalty_amount');
        $data['credits']                = (($bookings->where('status','==','drop_off')->sum('driver_fare')-$data['totalPenaltie'])-$data['paidTodriver']) + $data['penaltiePending'];
        $data['netEarnings']            = $data['pendingCredits'] + $data['paidTodriver'] + $data['credits'];
        $data['collect_otherAmount']    = $data['totalReceived']-$data['taxReceived'];
        $data['refund_otherAmount']     = $data['totalRefunded']-$data['taxRefunded'];
        $data['toRidechimp']            =  $data['collect_otherAmount']-$data['refund_otherAmount'];
        $data['earning']                = ($data['toRidechimp']-$data['netEarnings']);
        $data['upcoming']               = $bookingUpcoming->sum('chargeable_amount');

        return view('admin.totalEarning',compact('data'));
    }
}
