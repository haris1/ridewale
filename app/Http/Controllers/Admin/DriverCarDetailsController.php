<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use Validator;
use App\CarPhoto;
use App\CarDriver;
use App\CarCompany;
use App\CarModel;
use App\CarType;
use App\Models\v3\Driver;
use App\Models\v3\User;
use App\Document;
use App\Used_document;
use File;
use Image;
use App\Http\Controllers\Admin\DriverController;
use Carbon\Carbon;
use App\Models\v3\AllowedNotallowedThing;

class DriverCarDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $coms = CarCompany::get();
        // $res =  CarDriver::join('car_models', 'car_drivers.car_model_id', '=', 'car_models.id')
        //     ->join('car_photoes', 'car_drivers.id', '=', 'car_photoes.car_driver_id')
        //     ->select('car_drivers.*','car_models.car_company_id as comid','car_models.car_type_id as typeid','car_models.model_name as modelname','car_models.capacity as capacity','car_photoes.photo')
        //     ->where('car_drivers.id',$id)
        //     ->get();

        $res =  CarDriver::where('id',$id)
                            ->with('carPhotoes')
                            ->first();

        $carCompanyId = $res->car_model->car_company->id;
        $carTypeId = $res->car_model->car_type->id;
        
        $CarType = CarType::get();
        $CarModels = CarModel::where(['car_company_id'=>$carCompanyId])->get();
        return view('admin.drivers.editCarDetails',compact('res','coms','CarModels','CarType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $res_photo = "";
        $data = $request->all();
        $count = "";
        $rulesAndRegulations = array_filter($data['rules_and_regulations']);
        $allowedThings = array_filter($data['allowed_things']);
        $datas = [
                    'id'                    => $id,
                    'car_model_id'          => $data['editmodel'],
                    'year'                  => $data['editmodel_year'],
                    'car_number'            => $data['editcarnumber'],
                    'color'                 => $data['editcarcolor'],
                    'allowed_things'        => $allowedThings,
                    'rules_and_regulations' => $rulesAndRegulations
                ];
      
        $res = CarDriver::updateOrCreate(['id'=>$id],$datas);
        if($res){  
            $res_delete_photo = CarPhoto::where('car_driver_id',$id)->delete();
                
                if($request->hasfile('file')){

          
                    if(!File::exists(storage_path('app/public/driver_car/'.$data['driver_id'].''))){
                        $directoryPath = storage_path('app/public/driver_car/'.$data['driver_id'].'');
                        File::makeDirectory($directoryPath,0755,true);
                    }

                    $count = count($_FILES['file']['name']);
                    $image_name = array();
                    $i=0;
                    foreach($data['file'] as $key => $value ){
                        
                    /*$image = $request->file('image');
                    if(!File::exists(storage_path('app/public/'.$id.'/clothes'))){
                        $directoryPath = storage_path('app/public/'.$id.'/clothes');
                        File::makeDirectory($directoryPath,0755,true);
                    }            
                    $extension = $image->getClientOriginalExtension();
                    $img = Image::make($image);
                    $height = $img->height();
                    $width = $img->width();
                    $imageUniqueName = uniqid('cloth_'.uniqid(true).'_').'.'.$extension;
                    $img->save(storage_path('app/public/'.$id.'/clothes/'.$imageUniqueName));*/


                        $photo = $_FILES['file']['name'][$i];
                        $extension = $value->getClientOriginalExtension();
                        $img = Image::make($value);
                        $height = $img->height();
                        $width = $img->width();
                        $imageUniqueName = uniqid(uniqid(true).'_').'.'.$extension;
                        if ($height > $width) {
                                // resize the image to a height of 200 and constrain aspect ratio (auto width)
                                $img->resize(null, 500, function ($constraint) {
                                $constraint->aspectRatio();
                            });
                        }else{
                            // prevent possible upsizing
                            $img->resize(500, null, function ($constraint) {
                            $constraint->aspectRatio();
                            });
                        }
            
                        $img->save(storage_path('app/public/driver_car/'.$data['driver_id'].'/'.$imageUniqueName));

                        
                        $photo_data = array('car_driver_id'=>$data['car_driver_id'],'photo'=>$photo);
                       
                        $res_photo = CarPhoto::create($photo_data);

                        if($i!=$count){
                            $i++;
                        }
                    }
                                                           
                }
        
            
                $notification = array('status'=>true,'alert-type'=>'success','message'=>'Data updated successfully');
        }else{
             $notification = array('status'=>false,'alert-type'=>'error','message'=>'Something went wrong please try again');  
            
        }
        return response()->json($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Car_drivers_res = CarDriver::where('id',$id)->delete();
        $Car_photoes_res = CarPhoto::where('car_driver_id',$id)->delete();
        
        if($Car_drivers_res > 0){

             $notification = array('status'=>true,'alert-type'=>'success','message'=>'Car deleted successfully');
        }else{
             $notification = array('status'=>false,'alert-type'=>'error','message'=>'Something went wrong please try again');
        }

        return response()->json($notification);
    }

    public function car_details_list(Request $request){
        $data = $request->all();
        
        return Datatables::of( \DB::table('car_drivers')
            ->join('car_models', 'car_drivers.car_model_id', '=', 'car_models.id')
            ->join('drivers', 'car_drivers.driver_id', '=', 'drivers.id')
            ->join('car_types', 'car_models.car_type_id', '=', 'car_types.id')
            ->join('car_companies', 'car_models.car_company_id', '=', 'car_companies.id')
            ->where('car_drivers.driver_id',$data['id'])
            ->select('car_drivers.*','car_models.model_name','car_models.capacity','car_types.name as typename','car_companies.name as companyname')
            ->get())
        ->addIndexColumn()
        ->addColumn('action', function($user){
            return '<a href="javascript:void(0);" class="text-inverse view_car_details_popup pr-5"  data-id="'.$user->id.'" title="View Detail" data-toggle="tooltip"  data-toggle="modal" data-target="#Viewcardetails"><i class="zmdi zmdi-eye"></i>
            </a>
            <a href="'.route('admin.manageDocuments.view',$user->id).'"  data-id="'.$user->id.'" class="text-inverse" title="Manage Documents" data-toggle="tooltip"><i class="fa fa-file"></i>
            </a>
            <a href="javascript:void(0)" class="pr-5 edit_car_details_popup" data-id="'.$user->id.'" data-toggle="tooltip" title="Edit" >
            <i class="zmdi zmdi-edit"></i>
            </a>
            </a>
            <a href="javascript:void(0)"  data-id="'.$user->id.'" class="text-inverse" onclick="deletecar('.$user->id.')" title="Delete" data-toggle="tooltip"><i class="zmdi zmdi-delete"></i>
            </a>
            ';
        })
        ->rawColumns(['active_mode','action'])
        ->make(true);
    }

    public function get_edit_car_data(Request $request){
        $data = $request->all();

        $res =  \DB::table('car_drivers')
            ->join('car_models', 'car_drivers.car_model_id', '=', 'car_models.id')
            ->join('car_photoes', 'car_drivers.id', '=', 'car_photoes.car_driver_id')
            ->where('car_drivers.id',$data['id'])
            ->select('car_drivers.*','car_models.car_company_id as comid','car_models.car_type_id as typeid','car_models.model_name as modelname','car_models.capacity as capacity','car_photoes.photo')
            ->get();
        
         $notification = array('status'=>200,'data'=>$res);
        return response()->json($notification);
    }

    public function list_vehicles($id){
        
        $driver_id = Driver::where('user_id',$id)->first();
        //  echo "<pre>";
        // print_r($driver_id);
        // die;
        $datass = CarDriver::with('car_model')->where(['driver_id'=>$driver_id['id'],'status'=>1])->get();
        $vehicles_list = $datass;
        // $data = User::where('id',$id)->get();
        $drivercontroller=new DriverController;
        $data =$drivercontroller->get_sidebar_data($id);
        
        return view('admin.drivers.vehiclesList',compact('datass','data','vehicles_list'));
    }

    public function add_vehicles_page($id){

        $models = CarModel::get();
        $data = User::where('id',$id)->get();
        $allowed_things = AllowedNotallowedThing::where('allowed',1)->get();
        $not_allowed_things = AllowedNotallowedThing::where('allowed',0)->get();
        return view('admin.drivers.add_vehicles',compact('models','data','allowed_things','not_allowed_things'));

    }

    public function add_vehicles(Request $request){

        $data = $request->all();
    
        // echo "<pre>";
        // print_r($data);
        // die;
        $carPhotoes    = $request->file('file');
        $photo_count = count($carPhotoes);
        $validator=Validator::make($data, [
            'model' => 'required|Integer',
            'year' => 'required|Integer',
            'color' => 'required|alpha',
            'licence_plate' => 'required|string',
            'file' => 'required|array',
        ]
        );

        if ( $validator->fails()) { 
             $message = array_values(current($validator->messages()))[0][0];
            return response()->json(['status'=>false,'message'=>$message]);
        }

        $driver_id = Driver::where('user_id',$data['id'])->first();

        $array = array('driver_id'=>$driver_id['id'],'car_model_id'=>$data['model'],'year'=>$data['year'],'car_number'=>$data['licence_plate'],'color'=>$data['color'],'rules_and_regulations'=>$data['rules_and_regulations'],'allowed_things'=>$data['allowed_things']);

        $datass = CarDriver::create($array);

        if($request->hasFile('file')){

            foreach ($carPhotoes as $key => $carPhotoe) {
              
                // if(!File::exists(storage_path('car_image/'.$data['licence_plate'].'/'))){
                //     $directoryPath = storage_path('car_image/'.$data['licence_plate'].'/');
                //     File::makeDirectory($directoryPath,0755,true);
                // }
                if(!File::exists(storage_path('app/public/driver_car/'.$driver_id['id']))){
                    $directoryPath = storage_path('app/public/driver_car/'.$driver_id['id']);
                    File::makeDirectory($directoryPath,0755,true);
                }

                $img    = Image::make($carPhotoe);
                $height = $img->height();
                $width  = $img->width();

                if ($height > $width) {
                    // resize the image to a height of 200 and constrain aspect ratio (auto width)
                    $img->resize(null, 500, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }else{
                    // prevent possible upsizing
                    $img->resize(500, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
                $encryptedName = $carPhotoe->hashName();
                // Storage::put('driver_documents/'.$encryptedName, $img, 'public');
                $photo_data = array('car_driver_id'=>$datass->id,'photo'=>$encryptedName,'created_at'=>Carbon::now()->toDateTimeString(),'updated_at'=>Carbon::now()->toDateTimeString());
                CarPhoto::insert($photo_data);
                $img->save(storage_path('app/public/driver_car/'.$driver_id['id'].'/'.$encryptedName));
                // storage_path('app/public/driver_documents/'.$encryptedName);
                // exit;
            }
        }

        if($datass->id > 0){

            $notification = array('status'=>true,'alert-type'=>'success','message'=>'Vehicle has been added successfully','user_id'=>$data['id'],'location'=>route('admin.ViewVehicalesList',$data['id']));
        }else{
             $notification = array('status'=>false,'alert-type'=>'error','message'=>'Something went wrong please try again');
        }
        return response()->json($notification);
    }

    public function delete_vehicles(Request $request){

        $data = $request->all();

        $driver_id = CarDriver::where('id',$data['id'])->update(['status'=>0]);
        // $photo_id = CarPhoto::where('car_driver_id',$data['id'])->delete();
       
        if($driver_id > 0){

             $notification = array('status'=>true,'alert-type'=>'success','message'=>'Vehical deleted successfully');
        }else{
             $notification = array('status'=>false,'alert-type'=>'error','message'=>'Something went wrong please try again');
        }
        return response()->json($notification);
    }

    public function list_documents($id){

        $drivercontroller   = new DriverController;
        $data               = $drivercontroller->get_sidebar_data($id);
        $datas              = User::where('id',$id)->firstOrFail();

        $driver_id          = Driver::where('user_id',$id)->first();
      
        $id                 = $driver_id['id'];

        $cars_datas         = $datas->driver->car_driver;
         
        $Cardocuments = Document::where('type','car')->get()->toArray();
        $countCD = count($Cardocuments);

        $Driverdocuments = Document::where('type','driver')->get()->toArray();
        $countDD = count($Driverdocuments);

        $usedDD_doc = Used_document::where('ownable_id',$id)->where('ownable_type','driver')->get()->toArray();

        $usedCD_doc = $cars_datas;
        
        if(isset($usedDD_doc)){
            $DriverdocumentsCount = count($usedDD_doc);
        }else{
            $usedDD_doc = 0;
        }
        if(isset($usedCD_doc)){
            $CardocumentsCount = count($usedCD_doc);
        }else{
            $usedCD_doc = 0;
        }
        
        $CDstatus = $countCD - $CardocumentsCount;
        $DDstatus = $countDD - $DriverdocumentsCount;

        $cars = CarDriver::with('car_model')->where('driver_id',$id)->get();               
        return view('admin.documents.listDocuments',compact('data','Cardocuments','Driverdocuments','CDstatus','DDstatus','usedDD_doc','usedCD_doc','countCD','countDD','id','cars','cars_datas'));
    }

    public function submit_Documents(Request $request){

        $data = $request->all();

        $driver_id = Driver::where('user_id',$data['userid'])->first();

         if(isset($data['car_docid'])){
            $ownable_id = $data['car_id'];
            $ownable_path = 'car_'.$data['car_id'];
            $docid = $data['car_docid'];
            $field = 'file_'.$data['car_id'].'_'.$data['car_docid'];
        }else{
            $ownable_id = $driver_id['id'];
            $ownable_path = 'driver_'.$driver_id['id'];
            $docid = $data['docid'];
            $field = 'file_'.$data['docid'];
        }
        if($field != 'file_1'){
            $rules=[
            $field => 'required|array|max:2240',
            'datetimepicker' => 'required',
        ];
    }else{
        $rules=[
            'file_back_'.$data['docid'] => 'required',
            $field => 'required',
            'datetimepicker' => 'required',
        ];
    }
        
        $validator=Validator::make($data, $rules);


        if ( $validator->fails()) { 
             $message = "All fields are required.";
            return response()->json(['status'=>false,'message'=>$message]);
        }

        if($field != 'file_1'){
                $CarDocuments = $request->file($field);

        if($request->hasFile($field)){

            $image_array = array();
            foreach ($CarDocuments as $key => $carDocument) {
              
                 if(!File::exists(storage_path('app/public/documents/'.$ownable_path.'/'))){
                        $directoryPath = storage_path('app/public/documents/'.$ownable_path.'/');
                        File::makeDirectory($directoryPath,0777,true);
                    }

                    $img    = Image::make($carDocument);
                    $height = $img->height();
                    $width  = $img->width();

                    if ($height > $width) {
                        // resize the image to a height of 200 and constrain aspect ratio (auto width)
                        $img->resize(null, 500, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                    }else{
                        // prevent possible upsizing
                        $img->resize(500, null, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                    }
                    $encryptedName = $carDocument->hashName();
                    array_push($image_array,$encryptedName);
                    $img->save(storage_path('app/public/documents/'.$ownable_path.'/'.$encryptedName));
            }
        }
         $image_name = implode(',', $image_array);
      
        $photo_data = array('document_id'=>$docid,'ownable_id'=>$ownable_id,'name'=>$image_name,'expiry_date'=>Carbon::createFromFormat('m-d-Y',$data['datetimepicker'])->format('Y-m-d'),'ownable_type'=>$data['type']);
               
        $res_photo = Used_document::insertGetId($photo_data);
    }else{
                $carDocument  = $request->file('file_1');
                $carDocument1 = $request->file('file_back_1');

        if(!File::exists(storage_path('app/public/documents/'.$ownable_path.'/'))){
                        $directoryPath = storage_path('app/public/documents/'.$ownable_path.'/');
                        File::makeDirectory($directoryPath,0777,true);
                    }

                    $img    = Image::make($carDocument->getRealPath());
                    $height = $img->height();
                    $width  = $img->width();

                    if ($height > $width) {
                        // resize the image to a height of 200 and constrain aspect ratio (auto width)
                        $img->resize(null, 500, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                    }else{
                        // prevent possible upsizing
                        $img->resize(500, null, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                    }
                    $encryptedName = $carDocument->hashName();
                    // array_push($image_array,$encryptedName);
                    $img->save(storage_path('app/public/documents/'.$ownable_path.'/'.$encryptedName));  

                    //back photo
                     $img    = Image::make($carDocument1->getRealPath());
                    $height = $img->height();
                    $width  = $img->width();

                    if ($height > $width) {
                        // resize the image to a height of 200 and constrain aspect ratio (auto width)
                        $img->resize(null, 500, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                    }else{
                        // prevent possible upsizing
                        $img->resize(500, null, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                    }
                    $encryptedName1 = $carDocument1->hashName();
                    // array_push($image_array,$encryptedName);
                    $img->save(storage_path('app/public/documents/'.$ownable_path.'/'.$encryptedName1));  
      
        $photo_data = array('document_id'=>$docid,'ownable_id'=>$ownable_id,'name'=>$encryptedName,'back_photo'=>$encryptedName1,'expiry_date'=>Carbon::createFromFormat('m-d-Y',$data['datetimepicker'])->format('Y-m-d'),'ownable_type'=>$data['type']);

        // echo "<pre>";
        // print_r($photo_data);
        // die;
               
        $res_photo = Used_document::insertGetId($photo_data);
    }
      

        if($res_photo > 0){

             $notification = array('status'=>true,'alert-type'=>'success','message'=>'Document uploaded successfully');
        }else{
             $notification = array('status'=>false,'alert-type'=>'error','message'=>'Something went wrong please try again');
        }
        return response()->json($notification);
    }

    public function delete_document($id){
        
        $delete = Used_document::where('id',$id)->delete();

        if($delete > 0){

             $notification = array('status'=>true,'alert-type'=>'success','message'=>'Document deleted successfully');
        }else{
             $notification = array('status'=>false,'alert-type'=>'error','message'=>'Something went wrong please try again');
        }
        return response()->json($notification);
    }
}
