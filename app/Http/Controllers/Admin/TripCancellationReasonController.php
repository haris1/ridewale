<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use Validator;
use App\Models\v3\TripCancellationReason;

class TripCancellationReasonController extends Controller
{
    public function index()
    {   
        return view('admin.tripCancellationReasons.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(),['content'=>"required"]);

        if ($validator->fails()) { 
            return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
        }       

        TripCancellationReason::create($request->all());

        return response()->json(['status'=>true,'message'=>'Information added successfully.']);   
                 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Work_country  $work_country
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Work_country  $work_country
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Work_country  $work_country
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $validator = Validator::make($request->all(),['content'=>"required"]);

        if ($validator->fails()) { 
            return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
        }

        $id = $request->get('id');

        TripCancellationReason::where('id',$id)->update(array('content'=>$request->get('content')));

        return response()->json(['status'=>true,'message'=>'Information updated successfully.']); 

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Work_country  $work_country
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        TripCancellationReason::where('id',$id)->delete();
        return response()->json(['status'=>true,'message'=>'Information deleted successfully.']);

    }

    public function driver_trip_cancellation_reasons(Request $request){

        return Datatables::of(TripCancellationReason::where(['user_type'=>'driver','reason_for'=>'cancel_trip','status'=>1])->select('id','content'))
                    ->addIndexColumn()
                    ->addColumn('action', function($reason){

                        return '<a href="javascript:void(0)" class="pr-5" title="Edit" data-toggle="modal" data-target="#cancellationResonEdit_modal
                        " data-id="'.$reason->id.'" data-content="'.$reason->content.'"><i class="zmdi zmdi-edit"></i></a><a href="javascript:void(0)" onclick="deleteCancellationReason('.$reason->id.')" class="text-inverse" title="Delete" data-toggle="tooltip"><i class="zmdi zmdi-delete"></i>
                        </a>';
                    })
                    ->rawColumns(['action'])
                    ->make(true);

    }

    public function driver_booking_decline_reasons(Request $request){

        return Datatables::of(TripCancellationReason::where(['user_type'=>'driver','reason_for'=>'decline_booking','status'=>1])->select('id','content'))
                    ->addIndexColumn()
                    ->addColumn('action', function($reason){

                        return '<a href="javascript:void(0)" class="pr-5" title="Edit" data-toggle="modal" data-target="#cancellationResonEdit_modal
                        " data-id="'.$reason->id.'" data-content="'.$reason->content.'"><i class="zmdi zmdi-edit"></i></a><a href="javascript:void(0)" onclick="deleteCancellationReason('.$reason->id.')" class="text-inverse" title="Delete" data-toggle="tooltip"><i class="zmdi zmdi-delete"></i>
                        </a>';
                    })
                    ->rawColumns(['action'])
                    ->make(true);

    }

    public function rider_trip_cancellation_reasons(Request $request){

        return Datatables::of(TripCancellationReason::where(['user_type'=>'user','status'=>1])->select('id','content'))
                    ->addIndexColumn()
                    ->addColumn('action', function($reason){

                        return '<a href="javascript:void(0)" class="pr-5" title="Edit" data-toggle="modal" data-target="#cancellationResonEdit_modal
                        " data-id="'.$reason->id.'" data-content="'.$reason->content.'"><i class="zmdi zmdi-edit"></i></a><a href="javascript:void(0)" onclick="deleteCancellationReason('.$reason->id.')" class="text-inverse" title="Delete" data-toggle="tooltip"><i class="zmdi zmdi-delete"></i>
                        </a>';
                    })
                    ->rawColumns(['action'])
                    ->make(true);

    }
}
