<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use App\Http\Controllers\Controller;
use App\Models\v3\Trip;
use App\Models\v3\User;
use PaymentController;
use App\Models\v3\Payment;
use App\Models\v3\Booking;

class DashboardController extends Controller
{

    public function dashboard(Request $request)
    {

        $array              = array();
        $controller         = App::make('\App\Http\Controllers\Admin\PaymentController');
        $upcoming_trips     = $controller->callAction('get_upcoming_trip_list',$array);
        $complete_trips     = $controller->callAction('get_complete_trip_list',$array);
        $users              = User::where('active_mode','user')->count();
        $drivers            = User::where('active_mode','driver')->count();
        $commission         = Booking::whereHas('payment')
                                ->where('status','!=','cancelled')
                                ->where('status','!=','pending')
                                ->select('admin_fare','chargeable_amount')
                                ->get();
        $totalIncome        = number_format($commission->sum('chargeable_amount'),2);

        $totalCommission    = number_format($commission->sum('admin_fare'),2);

        return view('admin.dashboard')->with('upcoming_trips',$upcoming_trips)->with('complete_trips',$complete_trips)->with(compact('users','drivers','totalIncome','totalCommission'));
    }

    public function secondDashboard(){
    	return view('admin.dashboards.second-dashboard');
    }

    public function thirdDashboard(){
    	return view('admin.dashboards.third-dashboard');
    }

    public function fourthDashboard(){
        return view('admin.dashboards.fourth-dashboard');
    }

    public function payment(){
        return view('admin.payment');
    }

    public function socket(){
    	return view('socket');
    }

    public function socket1(){
        return view('socket1');
    }
}
