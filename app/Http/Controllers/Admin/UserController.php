<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\Models\v3\Payment;
use App\Models\v3\Booking;
use App\Models\v3\User;
use App\Mail\approval_comments;
use Mail;
use File;
use Image;
use Carbon\Carbon;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        return view('admin.users.users');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        
       $drivercontroller    = new DriverController;
       $data                = $drivercontroller->get_sidebar_data($id);
       $user_paid           = Booking::where('user_id',$id)->select('chargeable_amount')->get();
       $totaluser_paid      = number_format($user_paid->sum('chargeable_amount'),2);
       $user_trips          = Booking::with(['user','trip'])->where('user_id',$id)->get();
       $uid = base64_encode($id);
       return view('admin.users.detail',['data'=>$data,'totaluser_paid'=>$totaluser_paid,'user_trips'=>$user_trips,'uid'=>$uid]);
   }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        // echo "<pre>";
        // print_r($data);
        // die;

        if($request->hasfile('file')){

            if(!File::exists(storage_path('app/public/images/users/'.$id.'/profile_pic'))){
                $directoryPath = storage_path('app/public/images/users/'.$id.'/profile_pic');
                File::makeDirectory($directoryPath,0755,true);
            }                       
            $photo = $_FILES['file']['tmp_name'];
            $pro_image = $_FILES['file']['name'];

            $profile_image = str_replace(" ","_",$pro_image);

            $img = Image::make($request->file('file')->getRealPath());
            $height = $img->height();
            $width = $img->width();

            if ($height > $width) {
            // resize the image to a height of 200 and constrain aspect ratio (auto width)
                $img->resize(null, 500, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }else{
            // prevent possible upsizing
                $img->resize(500, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }

            $img->save(storage_path('app/public/images/users/'.$id.'/profile_pic/'.$pro_image));
        }else{
            $get_image_name = User::where('id',$id)->get();

            $profile_image = $get_image_name[0]->profile_pic;
        }

        $driver_data = array('first_name'=>$data['first_name'],'last_name'=>$data['last_name'],'gender'=>$data['gender'],'profile_pic'=>$profile_image);

        $res_photo = User::where('id',$data['id'])->update($driver_data);

        if($res_photo > '0'){
            $notification = array('status'=>true,'alert-type'=>'success','message'=>'Data updated successfully');
        }else{
           $notification = array('status'=>false,'alert-type'=>'error','message'=>'Something went wrong please try again');
       }

       return response()->json($notification);    
   }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $id = base64_decode($id);
        $res = User::where('id',$id)->delete();

        if($res > 0){
            $notification = array('status'=>true,'alert-type'=>'success','message'=>'Rider profile deleted successfully');
        }else{
            $notification = array('status'=>false,'alert-type'=>'error','message'=>'Something went wrong please try again');
        }

       return response()->json($notification);   
   }

   public function getUsersList(Request $request)
   {
        //$driver_id = Driver::where('user_id',$data['userid'])->first();
    return Datatables::of(User::with('driver')
        ->select('id','first_name','last_name','email','mobile_no','active_mode','is_driver','created_at')->orderBy('created_at','desc'))
    ->addIndexColumn()
    ->addColumn('name', function($user){
        return $user->first_name.' '.$user->last_name;
    })
    ->addColumn('signup_date', function($user){
            return Carbon::parse($user->created_at)->setTimeZone('Canada/Mountain')->format('M d, Y h:i a');
   })
    ->editColumn('active_mode', function($user){
        if ($user->active_mode == 'driver' && $user->is_driver == '1') {
            return '<a href="javascript:void(0)" class="pr-5" data-toggle="tooltip" title="Driver" >
            <i class="zmdi zmdi-car"></i> Driver
            </a>';
        }else{
           return '<a href="javascript:void(0)" class="pr-5" data-toggle="tooltip" title="Normal User" >
           <i class="zmdi zmdi-account"></i> Rider
           </a>';
       }
   })
    ->addColumn('action', function($user){

        if ($user->active_mode == 'driver' && $user->is_driver == '1') {
            return '<a href="'.route("admin.addDriverProfile",$user->id).'" class="text-inverse" title="View Detail" data-toggle="tooltip"><i class="zmdi zmdi-eye"></i></a>';
        } else {
            return '<a href="'.route("admin.users.show",$user->id).'" class="text-inverse" title="View Detail" data-toggle="tooltip"><i class="zmdi zmdi-eye"></i></a>';
        }
    })
        ->rawColumns(['active_mode','action','name','signup_date'])
        ->make(true);
    }

public function user_approval_comment_mail(Request $request){

    $data = User::where('id',$request->all('id'))->get();

    if(!empty($data)){

        $comment = array('comment'=>$request->all('comment'));
        Mail::to($data[0]['email'])->send(new approval_comments($comment));
        $notification = array('status'=>true,'alert-type'=>'success','message'=>'Message has been sent successfully'); 
        return response()->json($notification);
    }else{
        $notification = array('status'=>false,'alert-type'=>'error','message'=>'Something went wrong. Please try again');  
        return response()->json($notification);
    }
}

public function edit_rider_profile(Request $request){

    $data = $request->all();
    echo $data['id'];
    die;
    $data = User::where('id',base64_decode($data['id']))->get();
        //$id = base64_encode($data['id']);
    return view('admin.users.editUserProfile',compact('data'));
}

}
