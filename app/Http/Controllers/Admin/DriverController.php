<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\Models\v3\User;
use App\Models\v3\Driver;
use App\CarType;
use App\CarCompany;
use App\CarModel;
use App\CarPhoto;
use App\CarDriver;
use App\Models\v3\Trip;
use App\Models\v3\Payment;
use App\Models\v3\Booking;
use App\Mail\approval_comments;
use Mail;
use File;
use Image;
use DB;
use Validator;
use Stripe\Stripe;
use Stripe\Customer;
use App\Used_document;
use App\Document;

class DriverController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.drivers.drivers');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $rulesAndRegulations = array_filter($data['rules_and_regulations']);
        $allowedThings = array_filter($data['allowed_things']);
        $count = "";
        $datas = [
                    'driver_id'             => $data['driver_id'],
                    'car_model_id'          => $data['model'],
                    'year'                  => $data['model_year'],
                    'car_number'            => $data['carnumber'],
                    'color'                 => $data['carcolor'],
                    'rules_and_regulations' => $rulesAndRegulations,
                    'allowed_things'        => $allowedThings
                ];
        $res = CarDriver::create($datas);
        $res = $res->id;
        if($res > 0){

            if($request->hasfile('file')){

                if(!File::exists(storage_path('app/public/driver_car/'.$data['driver_id'].''))){
                    $directoryPath = storage_path('app/public/driver_car/'.$data['driver_id'].'');
                    File::makeDirectory($directoryPath,0755,true);
                }

                $count = count($_FILES['file']['name']);
                $image_name = array();
                $i=0;
                foreach($data['file'] as $key => $value ){

                    $photo = $_FILES['file']['name'][$i];

                    $img = Image::make($value);
                    $height = $img->height();
                    $width = $img->width();

                    if ($height > $width) {
                        // resize the image to a height of 200 and constrain aspect ratio (auto width)
                        $img->resize(null, 500, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                    }else{
                        // prevent possible upsizing
                        $img->resize(500, null, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                    }

                    $img->save(storage_path('app/public/driver_car/'.$data['driver_id'].'/'.$photo));


                    $photo_data = array('car_driver_id'=>$res,'photo'=>$photo);

                    $res_photo = CarPhoto::create($photo_data);

                    if($i!=$count){
                        $i++;
                    }
                }
            }
            if($res_photo > '0'){
                $notification = array('status'=>true,'alert-type'=>'success','message'=>'Car assigned successfully');
            }else{
             $notification = array('status'=>false,'alert-type'=>'error','message'=>'Something went wrong please try again');
         }

     }else{
         $notification = array('status'=>false,'alert-type'=>'error','message'=>'Something went wrong please try again');  

     }
     return response()->json($notification);
 }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
      //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // echo base64_code($id);
        // echo $id;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        // echo "<pre>";
        // print_r($data);
        // exit;
        //app/public/images/users/'.$user_id.'/profile_pic

        if($request->hasfile('file')){
            if(!File::exists(storage_path('app/public/images/users/'.$id.'/profile_pic'))){
                $directoryPath = storage_path('app/public/images/users/'.$id.'/profile_pic');
                File::makeDirectory($directoryPath,0755,true);
            }                       
            $photo = $_FILES['file']['tmp_name'];
            $pro_image = $_FILES['file']['name'];

            $profile_image = str_replace(" ","_",$pro_image);

            $img = Image::make($request->file('file')->getRealPath());
            $height = $img->height();
            $width = $img->width();

            if ($height > $width) {
            // resize the image to a height of 200 and constrain aspect ratio (auto width)
                $img->resize(null, 500, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }else{
            // prevent possible upsizing
                $img->resize(500, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }

            $img->save(storage_path('app/public/images/users/'.$id.'/profile_pic/'.$pro_image));

        }else{
            $driver_photo = User::where('id',$data['id'])->get();
            $profile_image = $driver_photo[0]['profile_pic'];
        }

        $driver_data = array('first_name'=>$data['first_name'],'last_name'=>$data['last_name'],'address'=>$data['address'],'gender'=>$data['gender'],'profile_pic'=>$profile_image);

        $res_photo = User::where('id',$data['id'])->update($driver_data);

        if($res_photo > '0'){
            $notification = array('status'=>true,'alert-type'=>'success','message'=>'Data updated successfully');
        }else{
         $notification = array('status'=>false,'alert-type'=>'error','message'=>'Something went wrong please try again');
     }

     return response()->json($notification);   
 }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $res = User::where('id',$id)->delete();
        Driver::where('user_id',$id)->delete();

        if($res > 0){
            $notification = array('status'=>true,'alert-type'=>'success','message'=>'Driver profile deleted successfully');
        }else{
         $notification = array('status'=>false,'alert-type'=>'error','message'=>'Something went wrong please try again');
     }

     return response()->json($notification);   
 }

 public function getDriversList(Request $request)
 {
    return Datatables::of(DB::table('drivers')->join('users', 'drivers.user_id', '=', 'users.id')->where('users.active_mode','driver')->select('users.*','drivers.profile_verified as profile_verified','drivers.is_approved as is_approved')->get())
    ->addIndexColumn()
    ->editColumn('active_mode', function($user){
        if ($user->active_mode == 'driver') {
            return '<a href="javascript:void(0)" class="pr-5" data-toggle="tooltip" title="Driver" >
            <i class="zmdi zmdi-car"></i> Driver
            </a>';
        }else{
           return '<a href="javascript:void(0)" class="pr-5" data-toggle="tooltip" title="Normal User" >
           <i class="zmdi zmdi-account"></i> Normal User
           </a>';
       }
   })
    ->addColumn('action', function($user){
        return '<a href="'.route("admin.driver.show",base64_encode($user->id)).'" class="text-inverse" title="View Detail" data-toggle="tooltip"><i class="zmdi zmdi-eye"></i>
        </a>
        <a href="javascript:void(0);" data-id="'.$user->id.'"  class="pr-5 edit_driver" data-toggle="tooltip" title="Edit" >
        <i class="zmdi zmdi-edit"></i>
        </a>
        <a href="javascript:void(0)" class="text-inverse delete_driver" data-id="'.$user->id.'" title="Delete" data-toggle="tooltip"><i class="zmdi zmdi-delete"></i>
        </a>';
    })
    ->rawColumns(['active_mode','action'])
    ->make(true);
}

public function driver_details_function($id){
    $did = base64_decode($id);
    $data = Driver::join('users', 'drivers.user_id', '=', 'users.id')->where('users.id',$did)->select('users.*','drivers.profile_verified as profile_verified','drivers.is_approved as is_approved','drivers.id as driver_id','drivers.stripe_id as stripe_con_id')->get();
        //$data = Driver::where('id',$did)->get();
    $coms = CarCompany::get();
    $types = CarType::get();

    $trip=Trip::where('driver_id',$did)->select('driver_id')->get();
    $totaltrip=$trip->count('driver_id');

    //$earning_detail = Payment::join('trips', 'payments.trip_id', '=', 'trips.id')->where('trips.driver_id',$did)->select(DB::raw('SUM(total) as total_income'),DB::raw('SUM(admin_fees) as admin_commission'))->get();
    $earning_detail = Booking::join('trips', 'bookings.trip_id', '=', 'trips.id')->where('trips.driver_id',$did)->select(DB::raw('SUM(chargeable_amount) as total_income'),DB::raw('SUM(admin_fare) as admin_commission'))->get();

    
    $accont_info=array();
    if($data[0]->stripe_con_id != null){
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));
        $accont_info = \Stripe\Account::retrieve($data[0]->stripe_con_id);
    }
    return view('admin.drivers.detail',['data'=>$data,'coms'=>$coms,'types'=>$types,'totaltrip'=>$totaltrip, 'earning_detail'=>$earning_detail,'accont_info'=>$accont_info]);
}

public function driver_approval_comment_mail(Request $request){

    $datas = Driver::where('id',$request->get('id'))->get();
    $data = User::where('id',$datas[0]->user_id)->get();

    if(!empty($data)){

        $comment = array('comment'=>$request->all('comment'));
        Mail::to($data[0]['email'])->send(new approval_comments($comment));
        $notification = array('status'=>true,'alert-type'=>'success','message'=>'Email has been sent successfully'); 
        return response()->json($notification);
        
    }else{
        $notification = array('status'=>false,'alert-type'=>'error','message'=>'Something went wrong please try again');  
        return response()->json($notification);
    }
}

public function get_type_by_company($id){
    $types=CarType::where('company_id',$id)->get();
    $html='<option value="">Select Type</option>';
    foreach ($types as $key => $type) {
        $html.='<option value="'.$type->id.'">'.$type->name.'</option>';
    }
    if(count($types) <= 0){
        $html.='<option value="">No Type Found.</option>';
    }
        return $html;//die();
    }

    public function get_model_by_type(Request $request){
        $data = $request->all();
        $models=CarModel::where('car_company_id',$data['com_id'])->get();
        $html='<option value="">Select Model</option>';
        foreach ($models as $key => $model){
            $html.='<option value="'.$model->id.'">'.$model->model_name.'</option>';
        }
        if(count($models) <= 0){
            $html.='<option value="">No data available</option>';
        }
        $notification = array('data'=>$html);
        return response()->json($notification);
    }

    public function add_car_details(Request $request){
     $notification = array('status'=>true,'alert-type'=>'success','message'=>'Email has been sent successfully','id'=>1); 
     return response()->json($notification);
 }

 public function edit_driver_profile(Request $request){

    $data = $request->all();
        // echo $data['id'];
        // die;
    $data = Driver::join('users', 'drivers.user_id', '=', 'users.id')->where('users.id',$data['id'])->select('users.*','drivers.profile_verified as profile_verified','drivers.is_approved as is_approved')->get();
    return view('admin.drivers.editProfile',compact('data'));
}

public function editBankDetails($id){
    $data = $this->get_sidebar_data($id);

    $accont_info=array();
    if($data[0]->driver->stripe_id != null){
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));
        $accont_info = \Stripe\Account::retrieve($data[0]->driver->stripe_id);
    }
    return view('admin.drivers.editBankDetail',['data'=>$data,'accont_info'=>$accont_info]);
}
    //update driver card detrail
public function updateBankDetails(Request $request)
{
    Stripe::setApiKey(config('services.stripe.secret'));

    try {
            //validation stuff
        $validator = Validator::make($request->all(),[
            'account_holder_name'=> 'required',
            'transit_number' => 'required',
            'institution_number' => 'required',
            'account_no' => 'required',
                // 'first_name' => 'required',
                // 'last_name' => 'required',
                // 'gender' => 'required',
            'line1' => 'nullable',
            'line2' => 'nullable',
            'city' => 'required',
            'state' => 'required',
            'postal_code' => 'required',
                // 'country' => 'required',
                // 'dob' => 'required',
            'personal_id_number' => 'required',
            'varification_doc' => 'required|file',
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->messages()->first()]);
        }

            //get the parameters
        $user = User::find($request->user_id);
        $user_id = $user->driver_id;

        $strip_connect_id = $user->driver['stripe_id'];
        $accountHolderName = $request->get('account_holder_name');
        $transit_number = $request->get('transit_number');
        $institution_number = $request->get('institution_number');
        $routingNumber = $transit_number.'-'.$institution_number;
        $accountNumber = $request->get('account_no');

        $firstName = $user->first_name;
        $lastName = $user->last_name;
        $fullName = $firstName.' '.$lastName;
        $gender = $user->gender;
            $dob = $user->dob; //it should be array ['day' => 02, 'month => 11', 'year' => 2022]
            $fullDate = explode('-', $dob);
            $day = $fullDate[2];
            $month = $fullDate[1];
            $year = $fullDate[0];
            $email = $user->email;
            $phoneNumber = '(989) 844-5123';//$request->get('phone_number');

            $line1 = $request->get('line1');
            $line2 = $request->get('line2');
            $city = $request->get('city');
            $state = $request->get('state');
            $postalCode = $request->get('postal_code');
            $country = "CA";

            $personalIdNumber = $request->get('personal_id_number'); // CA ID numbers must be 9 characters long
            $docFrontPic = $request->file('varification_doc');

            if ($strip_connect_id) {

            //upload the identity document
                $stripeDocumentFile = false;

                
                if ($request->hasFile('varification_doc')) {
                    $uploadedFile = \Stripe\FileUpload::create(
                        array("purpose" => "identity_document",
                            "file" => fopen($_FILES["varification_doc"]["tmp_name"], 'r'), 
                        )
                    );
                    $stripeDocumentFile = $uploadedFile->id;
                }
                $account = \Stripe\Account::retrieve($strip_connect_id);
                $account->email = $email;

            //account information
                $account->external_account = [
                    "object"=>"bank_account",
                    "country"=>"CA",
                    "currency"=>"CAD",
                    "account_holder_name"=>$fullName,
                    "account_holder_type"=>"individual",
                    "routing_number"=>$routingNumber,
                    "account_number"=>$accountNumber,
                ];

                if ($account->legal_entity->verification->status == 'unverified') {
            //address
                    $account->legal_entity['address'] = [
                        "line1" => $line1,
                        "line2" => $line2,
                        "city" => $city,
                        "state" => $state,
                        "postal_code" => $postalCode,
                        "country" => $country,
                    ];

            //personal info
                    $account->legal_entity->dob = [
                        'day' => $day,
                        'month' => $month,
                        'year' => $year
                    ];
                    $account->legal_entity->first_name = $firstName;
                    $account->legal_entity->last_name = $lastName;
                    $account->legal_entity->gender = $gender;
                    $account->legal_entity->phone_number = $phoneNumber;
                    $account->legal_entity->type = "individual";
                    $account->legal_entity->personal_id_number = $personalIdNumber;

                    if ($stripeDocumentFile) {
                        $account->legal_entity->verification->document = $stripeDocumentFile;
                    }
                }

            //terms acceptance
                $account['tos_acceptance'] = [
                    "date" => now()->timestamp,
                    "ip" => $request->ip(),
                    "user_agent" => $request->server('HTTP_USER_AGENT'),
                ];
            //personal information
                $account->save();
            }else{
            //upload the identity document
                $stripeFile = \Stripe\FileUpload::create(array(
                    'purpose' => 'identity_document',
                    'file' => fopen($docFrontPic,'r')
                ));
                $stripeFileId = $stripeFile->id;

            //create a connect account
                $stripeConnect = \Stripe\Account::create(array(
                    "type" => "custom",
                    "country" => "CA",
                    "email" => $email,
                    "external_account" => array (
                        "object"=>"bank_account",
                        "country"=>"CA",
                        "currency"=>"CAD",
                        "account_holder_name"=>$fullName,
                        "account_holder_type"=>"individual",
                        "routing_number"=>$routingNumber,
                        "account_number"=>$accountNumber,
                    ),
                    "legal_entity" => array (
                        "address" => array (
                            "line1" => $line1,
                            "line2" => $line2,
                            "city" => $city,
                            "state" => $state,
                            "postal_code" => $postalCode,
                            "country" => $country,
                        ),
                        "dob" => array(
                            'day' => $day,
                            'month' => $month,
                            'year' => $year
                        ),
                        "type" => "individual",
                        "first_name" => $firstName,
                        "last_name" => $lastName,
                        "gender" => $gender,
                        "phone_number" => $phoneNumber,
                        "personal_id_number" => $personalIdNumber,
                        "verification" => array(
                            "document" => $stripeFileId
                        ),
                    ),
                    "tos_acceptance" => array (
                        "date" => now()->timestamp,
                        "ip" => $request->ip(),
                        "user_agent" => $request->server('HTTP_USER_AGENT'),
                    )
                ));
                $stripeConnectId = $stripeConnect->id;

            //store the account id inside the database
                $user->driver()->update(array('stripe_id'=>$stripeConnectId));
            }

            //give the response
            return response()->json(['status'=>true,'message' => "Your bank information has been submitted successfully."]);

        }catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API
            $body = $e->getJsonBody();
            $err = $body['error'];

            if (isset($err['code']) && $err['code'] == "resource_already_exists") {
                return ['status'=>false,'message'=>$err['message'].' in our records']; 
            }
            return response()->json(['status'=>false,'message'=>$e->getMessage()]); 
        } catch (Stripe\Error\Card $e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            return response()->json(['status'=>false,'message'=>'Wrong Card information provided']);
        } catch (Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            return response()->json(['status'=>false,'message'=>'Problem occures with Authentications']);
        } catch (Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
            return response()->json(['status'=>false,'message'=>'Something went wrong with api connection']);
        } catch (Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            return response()->json(['status'=>false,'message'=>'Base Error occures']);
        } catch(Exception $e){
            return response()->json(['status'=>false,'message'=>'Something went wrong']);
        }
    }

    public function adddriver_profile($id){

        $data1              = User::where('id',$id)->first();
        $data1->is_driver   = '1';
        $data1->save();
        $data               = $this->get_sidebar_data($id);
        $notification       = array('status'=>true,'alert-type'=>'success','message'=>'Data updated successfully');
        return $notification;

    }

    public function show_driver_profile($id){

        $data =$this->get_sidebar_data($id);

        $User_id = $data[0]->id;

        if(count($data) == 0){
            return view('admin.drivers.add_driver_profile',compact('id'));    
        }else{

            if(is_array($data[0]->address) && $data[0]->address != ''){

                $array  = $data[0]->address;
                $datas  = array('is_approved'=>$data[0]->is_approved,'street_address'=>$array['street'],'city'=>$array['city'],'state'=>$array['state'],'country'=>$array['country'],'pincode'=>$array['pincode']);

            }else{
                $datas  = array('is_approved'=>'','street_address'=>'','city'=>'','state'=>'','country'=>'','pincode'=>'');
            }
            
            return view('admin.drivers.add_driver_profile',compact('id','data','datas','User_id'));    
        }
    }

    public function add_driver_address(Request $request,$id){

        $datas = $request->all();

        $array = array('street'=>$datas['street_address'],'city'=>$datas['city'],'state'=>$datas['state'],'country'=>$datas['country'],'pincode'=>$datas['pincode']);
        
        $res                = User::where('id',$id)->first();
        $res->address       = $array;
        $res->active_mode   ='driver';
        $res->save();

        $Darray = array('user_id'=>$id);
        Driver::updateOrcreate(['user_id'=>$id],$Darray);

        if($res){
            $notification = array('status'=>true,'alert-type'=>'success','message'=>'Address has been added successfully');
        }else{
         $notification = array('status'=>false,'alert-type'=>'error','message'=>'Something went wrong please try again');
     }

     return response()->json($notification);   
 }

 public function get_sidebar_data($id){

    $data               = User::where('id',$id)->with('driver')->get();
    $data[0]->profile   = 0;
    $data[0]->vehicles  = 0;
    $data[0]->documents = 0;
    $data[0]->payment_detail = 0;
    $driver_id               = $data[0]->driver->id;

    if($data[0]->address && isset($data[0]->address['street'])  && $data[0]->address['city']  && $data[0]->address['state'] && $data[0]->address['country'] && $data[0]->address['pincode']){
        $data[0]->profile=1;
    }

    $vehicles = CarDriver::with('car_model')->where('driver_id',$driver_id)->get();
    if(count($vehicles) > 0){
        $data[0]->vehicles = 1;
    }

    $Cardocuments       = Document::where('type','car')->get()->toArray();
    $countCD            = count($Cardocuments);

    $Driverdocuments    = Document::where('type','driver')->get()->toArray();
    $countDD            = count($Driverdocuments);

    $driver_docs        = Used_document::where('ownable_id',$driver_id)->where('ownable_type','driver')->count();
    $car_docs           = CarDriver::where('driver_id',$driver_id)->with('documents')->get();
    $cardoc_status      = 0;
    $totalcount         = 0;
    foreach ($car_docs as $key => $car_doc) {
        $totalcount += count($car_doc->documents);
    }

    if($driver_docs == $countDD && $totalcount == (count($car_docs)*$countCD)){
        $data[0]->documents     = 1;
    }

    if($data[0]->driver->stripe_id != null){
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));
        $accont_info = \Stripe\Account::retrieve($data[0]->driver->stripe_id);
        if($accont_info->legal_entity->verification->status == 'verified'){
            $data[0]->payment_detail = 1;
        }
    }

    return $data;
}
}
