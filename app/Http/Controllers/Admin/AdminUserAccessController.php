<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\v3\User;
use App\Models\v3\Driver;


class AdminUserAccessController extends Controller
{
	public function user_profile_approve_admin($id){

		// $data = $request->all();
		$user=User::where('id',$id)->first();
		if($user->is_approved == '1'){
			$user->is_approved='0';
			$user->save();
			$notification = array('status'=>true,'alert-type'=>'success','message'=>'Profile deactived successfully');
		}else{
			$user->is_approved='1';
			$user->save();
			$notification = array('status'=>true,'alert-type'=>'success','message'=>'Profile actived successfully');
		}
		// if($data['value'] > 0){
		// $notification = array('status'=>true,'alert-type'=>'success','message'=>'Account enabled successfully');
		// }else{
		// 	$notification = array('status'=>200,'alert-type'=>'success','message'=>'Account disabled successfully');	
		// }
		return response()->json($notification);
	}	

	public function driver_profile_approve_admin(Request $request){

		$data = $request->all();
		$param = array('is_approved'=>$data['value']);
		try{
			Driver::where('id',$data['id'])->update($param);
		}catch(Exception $e){
			$notification = array('alert-type'=>'error','message'=>$e);
			return response()->with($notification);
		}
		if($data['value'] > 0){
			$notification = array('status'=>200,'alert-type'=>'success','message'=>'Driver profile approved successfully');
		}else{
			$notification = array('status'=>200,'alert-type'=>'success','message'=>'Driver profile temporary blocked');	
		}
		return response()->json($notification);
	}	

	public function user_details_page(){
		return view('admin.users.detail');
	}
}
