<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\CarModel;
use App\CarType;
use App\CarCompany;
use Validator;


class CarModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $types = CarType::get();
        $coms = CarCompany::get();
        return view('admin.carModel',['types'=>$types,'coms'=>$coms]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = $request->all();

        $data=array(
        'company'=>$request->get('company'),
        'type'=>$request->get('type'),
        'carname'=>$request->get('carname'),
        'capacity'=>$request->get('capacity'),
        );
        
        $validator=Validator::make($data, [
            'company' => 'required|numeric',
            'type' => 'required|numeric',
            'capacity' => 'required|numeric',
            'carname' => 'required|string|max:255',
        ]
        );

        if ( $validator->fails()) {
             $message = array_values(current($validator->messages()))[0][0];
            return response()->json(['status'=>500,'message'=>$message]);
        } else {
            
            $addcarcom=array(
                'car_type_id'=>$request->get('type'),
                'car_company_id'=>$request->get('company'),
                'model_name'=>$request->get('carname'),
                'capacity'=>$request->get('capacity'),
            );
            $carmodel_id=CarModel::insertGetId($addcarcom);
            if($carmodel_id > 0){
                $notification = array('status'=>200,'message'=>"Car model added successfully");
            }else{
                $notification = array('status'=>500,'message'=>"Something went wrong please try again");
            }
            return response()->json($notification);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        
        $data=array(
        'editcompany'=>$request->get('editcompany'),
        'edittype'=>$request->get('edittype'),
        'editname'=>$request->get('editcarname'),
        'editcapacity'=>$request->get('editcapacity'),
        'id'=>$request->get('edit_Model_id'),
        );
        
        $validator=Validator::make($data, [
            'editcompany' => 'required|numeric',
            'edittype' => 'required|numeric',
            'editcapacity' => 'required|numeric',
            'editname' => 'required|string|max:255',
            'id' => 'required|numeric',
        ]
        );

        if ( $validator->fails()) {
             $message = array_values(current($validator->messages()))[0][0];
            return response()->json(['status'=>500,'message'=>$message]);
        }else{
            $update_data = array('car_company_id'=>$data['editcompany'],'car_type_id'=>$data['edittype'],'model_name'=>$data['editname'],'capacity'=>$data['editcapacity']);
            $update = CarModel::where('id',$data['id'])->update($update_data);
            
            if($update > 0){
                $notification = array('status'=>200,'message'=>'Car name updatetd successfully');
            }else{
                $notification = array('status'=>500,'message'=>'Something went wrong please try again');
            }
            return response()->json($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $company_name_delete = CarModel::where('id',$id)->delete();
         if($company_name_delete > 0){
            $notification = array('status'=>200,'message'=>'Car company name deleted successfully');
         }else{
            $notification = array('status'=>500,'message'=>'Something went wrong please try again');
         }
         return response()->json($notification);
    }

    public function list(Request $request){

         return Datatables::of(\DB::table('car_models')
                ->join('car_types', 'car_models.car_type_id', '=', 'car_types.id')
                ->join('car_companies', 'car_models.car_company_id', '=', 'car_companies.id')
                ->select('car_models.*','car_companies.name as comname','car_types.name as cartype')
                ->get())
        ->addIndexColumn()
        ->addColumn('action', function($user){
            return '<a href="javascript:void(0)" class="edit_Model" data-toggle="modal" data-target="#editcarModel" data-original-title="Edit" data-id="'.$user->id.'" class="pr-5" data-toggle="tooltip" title="Edit" >
            <i class="zmdi zmdi-edit"></i>
            </a>
            <a href="javascript:void(0)" class="delete_Model" data-toggle="modal" data-target="#deletecarModel" data-original-title="Delete" data-id="'.$user->id.'" onclick="deleteModel('.$user->id.')" class="text-inverse" title="Delete" data-toggle="tooltip"><i class="zmdi zmdi-delete"></i>
            </a>';
        })
        ->rawColumns(['active_mode','action'])
        ->make(true);

    }

    public function get_edit_model_name(Request $request){

        $data = $request->all();

        $company_name = CarModel::where('id',$data['id'])->get();
        $notification = array('status'=>200,'data'=>$company_name[0]);
        return response()->json($notification);
    }
}
