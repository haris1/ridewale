<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Exception\NotReadableException;
use App\Document;
use App\Used_document;
use Validator;
use DataTables;
use File;
use Image;
use Storage;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       //  $this->validate($request,[
       //      'car_documents[]' => 'nullable|array|mimes:jpg,jpeg,bmp,png,pdf',
       //      'driver_documents[]' => 'nullable|array|mimes:jpg,jpeg,bmp,png,pdf',
       //  ]);
       //  $encryptedName = "";
       //  $carDocuments    = $request->file('car_documents');
       //  $driverDocuments = $request->file('driver_documents');
       //  $document_name = $request->get('document_name');
        
       //  if($request->hasFile('car_documents')){
        
       //      foreach ($carDocuments as $key => $carDocument) {
              
       //          if ($carDocument->getMimeType() == 'application/pdf') {
       //              if(!File::exists(storage_path('driver_documents/'))){
       //                  $directoryPath = storage_path('driver_documents/');
       //                  File::makeDirectory($directoryPath,0755,true);
       //              }

       //              $carDocument->store('driver_documents/','public');

       //              $encryptedName = $carDocument->hashName();
                    
       //          }else{
                    
       //              $img    = Image::make($carDocument);
       //              $height = $img->height();
       //              $width  = $img->width();

       //              if ($height > $width) {
       //                  // resize the image to a height of 200 and constrain aspect ratio (auto width)
       //                  $img->resize(null, 500, function ($constraint) {
       //                      $constraint->aspectRatio();
       //                  });
       //              }else{
       //                  // prevent possible upsizing
       //                  $img->resize(500, null, function ($constraint) {
       //                      $constraint->aspectRatio();
       //                  });
       //              }
       //              $encryptedName = $carDocument->hashName();
       //              $img->save(storage_path('documents/'.$encryptedName));
       //          }
       //      }
       //  }

       //  $d = date("Y-m-d H:i:s",strtotime($request->get('datetimepicker1')));

       //  $Useddocument_data = array('document_id'=>$document_name,'name'=>$encryptedName,'expiry_date'=>$d,'ownable_id'=>$request->get('car_id'),'ownable_type'=>$request->get('user_type'));
       //  $ress = Used_document::insertGetId($Useddocument_data);

       //  if($ress > '0'){
       //      $notification = array('status'=>true,'alert-type'=>'success','message'=>'Document created successfully.');
       //  }else{
       //     $notification = array('status'=>false,'alert-type'=>'error','message'=>'Document not created.');
       // }
       // return response()->json($notification); 

       $this->validate($request,[
            'car_documents[]' => 'nullable|array|mimes:jpg,jpeg,bmp,png,pdf',
        ]);
        $carDocuments    = $request->file('car_documents');
        //$driverDocuments = $request->file('driver_documents');
        $document_name = $request->get('document_name');
        
        if($request->hasFile('car_documents')){

            
            foreach ($carDocuments as $key => $carDocument) {
              
                if ($carDocument->getMimeType() == 'application/pdf') {

                    if(!File::exists(storage_path('documents/'))){
                        $directoryPath = storage_path('documents/');
                        File::makeDirectory($directoryPath,0755,true);
                    }

                    $carDocument->store('documents/','public');

                    $encryptedName = $carDocument->hashName();
                    
                }else{
                    
                    if(!File::exists(storage_path('documents/'))){
                        $directoryPath = storage_path('documents/');
                        File::makeDirectory($directoryPath,0755,true);
                    }

                    $img    = Image::make($carDocument);
                    $height = $img->height();
                    $width  = $img->width();

                    if ($height > $width) {
                        // resize the image to a height of 200 and constrain aspect ratio (auto width)
                        $img->resize(null, 500, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                    }else{
                        // prevent possible upsizing
                        $img->resize(500, null, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                    }
                    $encryptedName = $carDocument->hashName();
                    // Storage::put('driver_documents/'.$encryptedName, $img, 'public');
                    $img->save(storage_path('app/public/documents/'.$encryptedName));
                    // storage_path('app/public/driver_documents/'.$encryptedName);
                    // exit;
                }
            }
        }

         $d = date("Y-m-d H:i:s",strtotime($request->get('datetimepicker1')));

            $Useddocument_data = array('document_id'=>$document_name,'name'=>$encryptedName,'expiry_date'=>$d,'ownable_id'=>$request->get('car_id'),'ownable_type'=>$request->get('user_type'));
            $ress = Used_document::insertGetId($Useddocument_data);

            if($ress > '0'){
                    $notification = array('status'=>true,'alert-type'=>'success','message'=>'Document created successfully');
            }else{
                 $notification = array('status'=>false,'alert-type'=>'error','message'=>'Something went wrong please try again');
            }
            return response()->json($notification);  


        //     if($request->hasFile('driver_documents')){
        
        //     foreach ($driverDocuments as $key => $driverDocument) {
        //         if ($driverDocument->getMimeType() == 'application/pdf') {
        //             $driverDocument->store('documents/','public');
        //         }else{
                    
        //             $img    = Image::make($driverDocument);
        //             $height = $img->height();
        //             $width  = $img->width();

        //             if ($height > $width) {
        //                 // resize the image to a height of 200 and constrain aspect ratio (auto width)
        //                 $img->resize(null, 500, function ($constraint) {
        //                     $constraint->aspectRatio();
        //                 });
        //             }else{
        //                 // prevent possible upsizing
        //                 $img->resize(500, null, function ($constraint) {
        //                     $constraint->aspectRatio();
        //                 });
        //             }
        //             $encryptedName = $driverDocument->hashName();
        //             $img->save(storage_path('app/public/documents/'.$encryptedName));
        //         }
        //     }
        // }

                // Storage::disk('public')->storeAs('documents',$file);
            // check if folder exists
            // if(!File::exists(storage_path('app/public/documents/'))){
            //     $directoryPath = storage_path('app/public/documents/');
            //     File::makeDirectory($directoryPath,0755,true);
            // }

            // $encryptedName = $file->hashName().'.'.$file->getClientOriginalExtension();

            // exit;
            // print_r($image->getMimeType());
            // upload image
            // $file_name_rpls = str_replace(" ","_",$_FILES['name']['name']);
         
            // $file_name = time().$_FILES['name']['name'];
            // $source = $_FILES['name']['tmp_name'];
            // $destination = $directoryPath.$file_name;

            // move_uploaded_file($source, $destination);

        //     if ($request->hasFile('name')) {
        //         $profilePhoto = $request->get('name');

        //         if (!file_exists($path)) {
        //             Storage::disk('public')->makeDirectory('documents/');
        //         }
        //         //$profilePhotoName = uniqid();
                
        //         $file_name = $_FILES['name']['name'];
        //     }
            
        // }       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Used_document::join('documents', 'used_documents.document_id', '=', 'documents.id')
        ->where('used_documents.id',$id)->select('used_documents.*','documents.name as doc_name')->get();
        $document_datas = Document::get();

        if($data > '0'){
                $notification = array('status'=>true,'data'=>$data);
        }else{
             $notification = array('status'=>false,'alert-type'=>'error','message'=>'No date found');
        }
        return response()->json($notification);  

        //return view('admin.documents.edit_document',compact('data','document_datas'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        $this->validate($request,[
            'car_documents[]' => 'nullable|array|mimes:jpg,jpeg,bmp,png,pdf',
            'driver_documents[]' => 'nullable|array',
        ]);
        $encryptedName = "";
        $carDocuments    = $request->file('car_documents');
        $driverDocuments = $request->file('driver_documents');
        $document_name = $request->get('document_name');
        
        if($request->hasFile('car_documents')){

            
            foreach ($carDocuments as $key => $carDocument) {
              
                if ($carDocument->getMimeType() == 'application/pdf') {

                    if(!File::exists(storage_path('documents/'))){
                        $directoryPath = storage_path('documents/');
                        File::makeDirectory($directoryPath,0755,true);
                    }

                    $carDocument->store('documents/','public');

                    $encryptedName = $carDocument->hashName();
                    
                }else{
                    
                    if(!File::exists(storage_path('documents/'))){
                        $directoryPath = storage_path('documents/');
                        File::makeDirectory($directoryPath,0755,true);
                    }

                    $img    = Image::make($carDocument);
                    $height = $img->height();
                    $width  = $img->width();

                    if ($height > $width) {
                        // resize the image to a height of 200 and constrain aspect ratio (auto width)
                        $img->resize(null, 500, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                    }else{
                        // prevent possible upsizing
                        $img->resize(500, null, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                    }
                    $encryptedName = $carDocument->hashName();
                    // Storage::put('driver_documents/'.$encryptedName, $img, 'public');
                    $img->save(storage_path('app/public/documents/'.$encryptedName));
                    // storage_path('app/public/driver_documents/'.$encryptedName);
                    // exit;
                }
            }
        }

        // if($request->hasFile('driver_documents')){
        
        //     foreach ($driverDocuments as $key => $driverDocument) {
        //         if ($driverDocument->getMimeType() == 'application/pdf') {
        //             $driverDocument->store('documents/','public');
        //         }else{
                    
        //             $img    = Image::make($driverDocument);
        //             $height = $img->height();
        //             $width  = $img->width();

        //             if ($height > $width) {
        //                 // resize the image to a height of 200 and constrain aspect ratio (auto width)
        //                 $img->resize(null, 500, function ($constraint) {
        //                     $constraint->aspectRatio();
        //                 });
        //             }else{
        //                 // prevent possible upsizing
        //                 $img->resize(500, null, function ($constraint) {
        //                     $constraint->aspectRatio();
        //                 });
        //             }
        //             $encryptedName = $driverDocument->hashName();
        //             $img->save(storage_path('app/public/documents/'.$encryptedName));
        //         }
        //     }
        // }

        $d = date("Y-m-d H:i:s",strtotime($request->get('datetimepicker1')));

        if(empty($encryptedName)){

            $get_name = Used_document::where('id',$id)->get();
                $encryptedName = $get_name[0]->name;
        }

        $Useddocument_data = array('document_id'=>$request->get('editdocument_name'),'name'=>$encryptedName,'expiry_date'=>$d,'ownable_type'=>$request->get('edit_user_type'));
        $ress = Used_document::where('id',$id)->update($Useddocument_data);

        if($ress > '0'){
                $notification = array('status'=>true,'alert-type'=>'success','message'=>'Document updated successfully');
        }else{
             $notification = array('status'=>false,'alert-type'=>'error','message'=>'Something went wrong please try again');
        }
        return response()->json($notification);

                // Storage::disk('public')->storeAs('documents',$file);
            // check if folder exists
            // if(!File::exists(storage_path('app/public/documents/'))){
            //     $directoryPath = storage_path('app/public/documents/');
            //     File::makeDirectory($directoryPath,0755,true);
            // }

            // $encryptedName = $file->hashName().'.'.$file->getClientOriginalExtension();

            // exit;
            // print_r($image->getMimeType());
            // upload image
            // $file_name_rpls = str_replace(" ","_",$_FILES['name']['name']);
         
            // $file_name = time().$_FILES['name']['name'];
            // $source = $_FILES['name']['tmp_name'];
            // $destination = $directoryPath.$file_name;

            // move_uploaded_file($source, $destination);

        //     if ($request->hasFile('name')) {
        //         $profilePhoto = $request->get('name');

        //         if (!file_exists($path)) {
        //             Storage::disk('public')->makeDirectory('documents/');
        //         }
        //         //$profilePhotoName = uniqid();
                
        //         $file_name = $_FILES['name']['name'];
        //     }
            
        // }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Used_document::where('id',$id)->delete();
        if($data > '0'){
                $notification = array('status'=>true,'alert-type'=>'success','message'=>'Document deleted successfully');
        }else{
             $notification = array('status'=>false,'alert-type'=>'error','message'=>'Something went wrong please try again');
        }
        return response()->json($notification);  
    }

    public function view($id){

        $Used_document_data = Used_document::where('ownable_type','car')->where('ownable_id',$id)->get();
        $document_data = Document::get();
        return view('admin.documents.document',['id'=>$id,'Used_document_data'=>$Used_document_data,'document_datas'=>$document_data]);
    }
}
