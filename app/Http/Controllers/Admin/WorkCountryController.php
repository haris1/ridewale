<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\Work_country;
use Validator;
use App\Work_state;

class WorkCountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $countries=Work_country::all();
        $states = Work_state::all();
        return view('admin.work_countries.work_countries',compact('countries','states'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),['name'=>"required"]);

        if ($validator->fails()) { 
            return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
        }       
        Work_country::create($request->all());
        return response()->json(['status'=>true,'message'=>'Country added successfully.']);            
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Work_country  $work_country
     * @return \Illuminate\Http\Response
     */
    public function show(Work_country $work_country)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Work_country  $work_country
     * @return \Illuminate\Http\Response
     */
    public function edit(Work_country $work_country)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Work_country  $work_country
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(),['name'=>"required"]);

        $id = $request->get('id');

        if ($validator->fails()) { 
            return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
        }
        
        Work_country::where('id',$id)->update(array('name'=>$request->get('name')));
        return response()->json(['status'=>true,'message'=>'Country added successfully.']);   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Work_country  $work_country
     * @return \Illuminate\Http\Response
     */
    public function destroy(Work_country $work_country)
    {
        Work_country::where('id',$work_country->id)->delete();
        return response()->json(['status'=>true,'message'=>'Country deleted successfully.']);  
    }

    public function show_work_countries(Request $request){
        return Datatables::of(Work_country::select('id','name'))
        ->addIndexColumn()
        ->addColumn('action', function($country){
            //delete
            /*<a href="javascript:void(0)" onclick="deleteCountry('.$country->id.')" class="text-inverse" title="Delete" data-toggle="tooltip"><i class="zmdi zmdi-delete"></i>
            </a>*/
            return '<a href="javascript:void(0)" class="pr-5" title="Edit" data-toggle="modal" data-target="#countryEdit_modal" data-id="'.$country->id.'" data-name="'.$country->name.'"><i class="zmdi zmdi-edit"></i></a>';
        })
        ->rawColumns(['action'])
        ->make(true);
    }
}
