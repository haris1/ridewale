<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin_statistic;
use Validator;
use App\Maintanance;
use App\CarModel;
use App\Work_city;
use App\Work_location;
use App\OfficeInformation;
use Carbon\Carbon;

class GlobalSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$admin_statistic = Admin_statistic::find(1);
        $maintanance = Maintanance::find(1);
        $total_vehicles = CarModel::count();
        //$total_city=Work_city::count();
        $total_city=OfficeInformation::count();
        $total_location=Work_location::count();

        //return view('admin.global-settings',compact('sidewalks','services','driveways','admin_statistic'));
        return view('admin.global-settings',compact('admin_statistic','maintanance','total_vehicles','total_city','total_location'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$rules=[
    		'admin_charge'=>"required",
    		'transfer_charge'=>"required"
    	];
    	if($request->get('admin_charge') == 'fixed'){
    		$rules['admin_fees']="required";
    		$data['admin_fees']=$request->get('admin_fees');
    		$data['admin_cent']=0;
    	}else if($request->get('admin_charge') == 'percentage'){
    		$rules['admin_cent']="required";
    		$data['admin_fees']=0;
    		$data['admin_cent']=$request->get('admin_cent');
    	}

    	if($request->get('transfer_charge') == 'fixed'){
    		$rules['transfer_fees']="required";
    		$data['transfer_cent']=0;
    		$data['transfer_fees']=$request->get('transfer_fees');
    	}else if($request->get('transfer_charge') == 'percentage'){
    		$rules['transfer_cent']="required";
    		$data['transfer_cent']=$request->get('transfer_cent');
    		$data['transfer_fees']=0;
    	}

    	$validator = Validator::make($request->all(),$rules);

    	if ($validator->fails()) { 
    		return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
    	}		
    	Admin_statistic::where('id',1)->update($data);
        $maintanance['maintanance_mode']=$request->get('maintanance_mode');
        $maintanance['android_version']=$request->get('android_version');
        $maintanance['ios_version']=$request->get('ios_version');

        Maintanance::UpdateOrCreate(['id'=>1],$maintanance);
    	return response()->json(['status'=>true,'message'=>'Global settings updated successfully.']);            

    }

    //APP STORE FORM
    public function appSettingStore(Request $request){
        $rules=[
            'ios_version'=>"required",
            'android_version'=>"required"
        ];

        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) { 
            return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
        }
        $maintanance['maintanance_mode']=$request->get('maintanance_mode');
        $maintanance['android_version']=$request->get('android_version');
        $maintanance['ios_version']=$request->get('ios_version');
        $maintanance['updated_at']=Carbon::now()->toDateTimeString();

        Maintanance::UpdateOrCreate(['id'=>1],$maintanance);
        return response()->json(['status'=>true,'message'=>'App settings updated successfully.']); 

    }


    //RIDECHIM FEES UPDATE
    public function adminFees(Request $request){
        $rules=[
            'admin_cent'=>"required|numeric|max:100|min:0",
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) { 
            return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
        }
        $data['admin_cent']=$request->get('admin_cent');
        $data['updated_at']=date('Y-m-d H:i:s');

        Admin_statistic::where('id',1)->update($data);
        return response()->json(['status'=>true,'message'=>'RideForU Fees updated successfully.']); 
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function show_products_page(Request $request)
    {
    	$sidewalks = Sidewalk::all();
    	$services = Service::all();
    	$driveways = Driveway::all();
    	return view('admin.product_management',compact('sidewalks','services','driveways'));
    }
}
