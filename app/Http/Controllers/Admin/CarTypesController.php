<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CarCompany;
use App\CarType;
use Validator;
use DataTables;
use File;
use Image;
use Illuminate\Support\Facades\Storage;

class CarTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = CarCompany::get();
        return view('admin.carType',['companies'=>$companies]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        
       
        $carImage = "";
        if($request->hasfile('file')){

            if(!File::exists(storage_path('app/public/type_logo/'))){
                $directoryPath = storage_path('app/public/type_logo/');
                File::makeDirectory($directoryPath,0755,true);
            }


            $image = $request->file('file');
            $extension = $image->getClientOriginalExtension();
            $img = Image::make($image);
            $height = $img->height();
            $width = $img->width();
            $carImage = uniqid('car_'.uniqid(true).'_').'.'.$extension;

            if ($height > $width) {
                $img->resize(null, 500, function ($constraint) {
                    $constraint->aspectRatio();
                });
            } else {
                $img->resize(500, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }
            $img->save(storage_path('app/public/type_logo/'.$carImage));
        }

        $data = array('cartype'=>$request->get('cartype'));
        
        $validator = Validator::make($data, [
            'cartype' => 'required|string|max:255',
        ]);

        if ( $validator->fails()) {
             $message = array_values(current($validator->messages()))[0][0];
            return response()->json(['status'=>500,'message'=>$message]);
        } else {
            
            $addcarcom = array(
                'name'       => $request->get('cartype'),
                //'company_id'=>$request->get('company'),
                'logo'       => $carImage,
                'created_at' => date('Y-m-d H:i:s'), 
                'updated_at' => date('Y-m-d H:i:s'),
            );

            $carcom_id=CarType::insertGetId($addcarcom);
            if($carcom_id > 0){
                $notification = array('status'=>200,'message'=>"Car type added successfully");
            }else{
                $notification = array('status'=>500,'message'=>"Something went wrong please try again");
            }
            return response()->json($notification);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $companies = CarCompany::get();
        $Car_type = CarType::where('id',$id)->get();
        return view('admin.carTypeEdit',compact('companies','Car_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //  echo "<pre>";
        // print_r($request->all());
        // die;

        $carImage = "";
        $data = array(
            // /'editcompany'   =>$request->get('editcompany'),
            'edit_type' =>$request->get('edit_type'),
            'edit_type_id'=>$request->get('edit_type_id'),
        );
        
        $validator=Validator::make($data, [
            //'editcompany' => 'required',
            'edit_type' => 'required',
            'edit_type_id' => 'required',
        ]);

        if ( $validator->fails()) {
             $message = array_values(current($validator->messages()))[0][0];
            return response()->json(['status'=>500,'message'=>$message]);
        }else{

            if($request->hasfile('editfile')){
                if(!File::exists(storage_path('/app/public/type_logo/'))){
                    $directoryPath = storage_path('/app/public/type_logo/');
                    File::makeDirectory($directoryPath,0755,true);
                }


                $image = $request->file('editfile');
                $extension = $image->getClientOriginalExtension();
                $img = Image::make($image);
                $height = $img->height();
                $width = $img->width();
                $carImage = uniqid('car_'.uniqid(true).'_').'.'.$extension;
                $img->save(storage_path('app/public/type_logo/'.$carImage));

                $data['logo'] = $carImage;
            } else {
                $notification = array('status'=>500,'message'=>'Car type not update please try again.');
            }

            $id = $data['edit_type_id'];

            $data['name'] = $data['edit_type'];
            unset($data['edit_type'] , $data['edit_type_id']);

            $update = CarType::where('id',$id)->update($data);
                
                if($update > 0){
                    $notification = array('status'=>200,'message'=>'Car type updated successfully');
                }else{
                    $notification = array('status'=>500,'message'=>'Something went wrong please try again');
                }
                return response()->json($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      
        $company_name_delete = CarType::where('id',$id)->delete();
         if($company_name_delete > 0){
            $notification = array('status'=>200,'message'=>'Car type deleted successfully');
         }else{
            $notification = array('status'=>500,'message'=>'Something went wrong please try again');
         }
         return response()->json($notification);
    }

     public function list(Request $request){

         return Datatables::of(CarType::get())
        ->addIndexColumn()
        ->addColumn('action', function($user){
            return '<a href="javascript:void(0)" class="edit_type" data-toggle="modal" data-target="#editcartype" data-original-title="Edit" data-id="'.$user->id.'" class="pr-5" data-toggle="tooltip" title="Edit" >
            <i class="zmdi zmdi-edit"></i>
            </a>
            <a href="javascript:void(0)" class="delete_type" data-toggle="modal" data-target="#deletecartype" data-original-title="Delete" data-id="'.$user->id.'" onclick="deleteType('.$user->id.')" class="text-inverse" title="Delete" data-toggle="tooltip"><i class="zmdi zmdi-delete"></i>
            </a>';
        })
        ->rawColumns(['active_mode','action'])
        ->make(true);

    }

    public function get_edit_type_name(Request $request){

        $data = $request->all();

        $company_name = CarType::where('id',$data['id'])->get();

        $notification = array('status'=>200,'company_id'=>$company_name[0]->company_id,'logo'=>$company_name[0]->logo,'data'=>$company_name[0]->name,'dataid'=>$company_name[0]->id);
        return response()->json($notification);
    }

    public function update_car_type(Request $request)
    {
        // print_r($request->get('edit_type_id'));
       
        // die;

        $data=array(
        'name'=>$request->get('name'),
        'id'=>$request->get('id'),
        );
        
        $validator=Validator::make($data, [
            'name' => 'required|string|max:255',
            'id' => 'required',
        ]
        );

        if ( $validator->fails()) { 
             $message = array_values(current($validator->messages()))[0][0];
            return response()->json(['status'=>500,'message'=>$message]);
        }else{
            $update_data = array('name'=>$data['name']);
            $update = CarType::where('id',$data['id'])->update($update_data);
            
            if($update > 0){
                $notification = array('status'=>200,'message'=>'Car type updated successfully');
            }else{
                $notification = array('status'=>500,'message'=>'Something went wrong please try again');
            }

            return response()->json($notification);
        }
    }
}
