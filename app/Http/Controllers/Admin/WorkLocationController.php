<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\Work_city;
use App\Work_country;
use App\Work_location;
use Validator;

class WorkLocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $countries=Work_country::all();
      return view('admin.work_locations.work_locations',compact('countries'));
  }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),['name'=>"required",'work_country_id'=>'required','work_city_id'=>'required']);

        if ($validator->fails()) { 
            return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
        }       
        Work_location::create($request->all());
        return response()->json(['status'=>true,'message'=>'Location added successfully.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $location=Work_location::where('id',$id)->first();
        $cities=Work_city::where('work_country_id',$location->work_country_id)->get();
        $countries=Work_country::all();

        return view('admin.work_locations.edit',compact('location','cities','countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),['name'=>"required",'work_country_id'=>'required','work_city_id'=>'required']);

        if ($validator->fails()) { 
            return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
        }       
        Work_location::where('id',$id)->update($request->all());
        return response()->json(['status'=>true,'message'=>'Location updated successfully.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Work_location::where('id',$id)->delete();
        return response()->json(['status'=>true,'message'=>'Location deleted successfully.']);
    }

    public function show_work_locations(Request $request){
        return Datatables::of(Work_location::select('id','name','work_country_id','work_city_id')->with('work_country','work_city'))
        ->addIndexColumn()
        ->addColumn('country', function($location){
            return $location->work_country['name'];
        })
        ->addColumn('city', function($location){
            return $location->work_city['name'];
        })
        ->addColumn('action', function($location){
            //delete
            /*<a href="javascript:void(0)" onclick="deleteLocation('.$location->id.')" class="text-inverse" title="Delete" data-toggle="tooltip"><i class="zmdi zmdi-delete"></i>
            </a>*/
            return '<a href="javascript:void(0)" class="pr-5" title="Edit" data-toggle="modal" data-target="#locationEdit_modal" data-id="'.$location->id.'"  data-name="'.$location->name.'">
            <i class="zmdi zmdi-edit"></i>
            </a>';
        })
        ->rawColumns(['action'])
        ->make(true);
    }

    public function get_cities_by_country($id){
       
        $cities=Work_city::where('work_country_id',$id)->get();
        $html='<option value="">Select City</option>';
        foreach ($cities as $key => $city) {
            $html.='<option value="'.$city->id.'">'.$city->name.'</option>';
        }
        if(count($cities) <= 0){
            $html.='<option value="">No City Found.</option>';
        }
        return $html;//die();
    }
}
