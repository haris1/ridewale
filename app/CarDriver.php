<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarDriver extends Model
{
	protected $fillable = [
		'id','driver_id','car_model_id','car_number','color','year','allowed_things','rules_and_regulations','status'
	];

	/*protected $attributes = [
		'rules_and_regulations' => '<h1>HARIS</h2>',
		'allowed_things' => '<h1>HARIS</h2>'
	];*/

	protected $casts = [
    	'rules_and_regulations' => 'array',
    	'allowed_things' => 'array'
    ];

	public function car_model(){
		return $this->belongsTo('App\CarModel','car_model_id');
	}

	public function driver(){
		return $this->belongsTo('App\Driver','driver_id');
	}

	public function carPhotoes(){
		return $this->hasMany('App\CarPhoto','car_driver_id','id');
	}

	public function documents(){
		return $this->morphMany('App\Used_document', 'ownable');
	}
}
