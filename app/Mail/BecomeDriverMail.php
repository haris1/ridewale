<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\BecomeDriver;

class BecomeDriverMail extends Mailable
{
    use Queueable, SerializesModels;
    public $BecomeDriver;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($BecomeDriver)
    {
        $this->BecomeDriver = $BecomeDriver;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Become Driver')
                    ->markdown('website.mail.become_driver')
                    ->with('BecomeDriver',$this->BecomeDriver);
    }
}
