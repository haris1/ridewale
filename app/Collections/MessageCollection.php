<?php

namespace App\Collections;

use Illuminate\Database\Eloquent\Collection;

class MessageCollection extends Collection
{
    /**
     * Mark all messages as read.
     *
     * @return void
     */
    public function markAsRead()
    {
        $this->each(function ($message) {
            $message->markAsRead();
        });
    }

    /**
     * Mark all messages as unread.
     *
     * @return void
     */
    public function markAsUnread()
    {
        $this->each(function ($message) {
            $message->markAsUnread();
        });
    }
}
