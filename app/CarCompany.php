<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarCompany extends Model
{
     protected $fillable = [
        'name'
    ];

	public function car_type(){
        return $this->hasMany('App\CarType','company_id','id');
	}

	public function car_model(){
        return $this->hasMany('App\CarModel','car_company_id','id');
	}
}
