<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Maintanance extends Model
{
    protected $fillable = [
		'android_version','ios_version','maintanance_mode'
	];
}
