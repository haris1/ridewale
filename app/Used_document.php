<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Used_document extends Model
{

    protected $fillable = [
		'id','document_id','ownable_id','name','expiry_date','ownable_type','back_pic'
	];

	public function ownable()
	{
		return $this->morphTo();
	}

}
