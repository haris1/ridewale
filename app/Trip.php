<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    protected $fillable = [
       'driver_id', 'from_city_id', 'to_city_id', 'trip_date','pickup_id','dropoff_id', 'pickup_time','dropoff_time','total_seats','price','available_seats','vehicle_id','status'
    ];

    public function driver(){
        return $this->belongsTo('App\Driver','driver_id');
    }

     public function trip_users(){
        return $this->hasMany('App\TripUser','trip_id','id');
    }

    public function from_city(){
		return $this->belongsTo('App\Work_city','from_city_id');
	}

	public function to_city(){
		return $this->belongsTo('App\Work_city','to_city_id');
	}

	public function pickup_location(){
		return $this->belongsTo('App\Work_location','pickup_id');
	}

	public function dropoff_location(){
		return $this->belongsTo('App\Work_location','dropoff_id');
	}

	public function payments(){
		return $this->hasMany('App\Payment','trip_id','id');
	}

	public function vehicle(){
		return $this->belongsTo('App\CarDriver','vehicle_id');
	}

	public function scopeStatus($query,$status)
	{
		return $query->where('status',$status);
	}

	public function scopeAvailable($query)
	{
		return $query->where('available_seats','>',0);
	}
}
