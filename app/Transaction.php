<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = [
    	'id', 'payment_id','charge_id', 'transaction_id', 'amount','charged_currency','transaction_time','status'
    ];
    // public $timestamps = false;

    public function payment(){
        return $this->belongsTo('App\Payment','payment_id');
    }

    public function refund(){
        return $this->hasOne('App\Refund','transaction_id','id');
    }
}
