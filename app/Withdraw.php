<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Withdraw extends Model
{
    protected $fillable = [
		'id', 'driver_id', 'transaction_id', 'amount','transaction_time','status','payout_transaction_id','payout_transaction_time','payout_failure_code','payout_failure_message','payout_status','on_behalf_of'
	];
	public $timestamps = false;
	protected $casts = [
		'on_behalf_of' => 'array'
	];
	public function driver(){
		return $this->belongsTo('App\Driver','driver_id');
	}
}
