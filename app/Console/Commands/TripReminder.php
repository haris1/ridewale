<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Trip;
use Notification;
use App\Helpers\NotificationHelper;
use App\Notifications\TripReminderNotification;

class TripReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'minuteUpdate:TripReminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Trip reminder for rider before 30 min priorer to trip start.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //email notifications
        $trips=Trip::
        with(['from_city'=> function ($query) {
            $query->select('id','name');
        }])
        ->with(['to_city'=> function ($query) {
            $query->select('id','name');
        }])
        ->with(['payments'=> function ($query) {
            $query->select('id','user_id', 'trip_id','total','total_person','status','created_at','luggage')
            ->with(['user'=> function ($query) {
                $query->select('id','first_name', 'last_name','email','firebase_android_id','firebase_ios_id');
            }])
            ->where('status','0');
        }])
        ->where('trip_date',date('Y-m-d'))
        ->where('pickup_time','>=',date('h:i:s',strtotime('-30 minutes')))
        ->get();
        
        foreach ($trips as $key => $trip) {
            if(count($trip->payments) > 0){
                foreach ($trip->payments as $key => $payment) {
                    $payment->user->notify(new TripReminderNotification($trip));
                    NotificationHelper::sendPushNotification($payment->user,'UserTripReminder',$trip->toArray());
                }  
            }
        }
    }
}
