<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Models\v3\Booking;
use App\Models\v3\DeclinedBooking;
use App\Notifications\BookingDeclinedNotification;
use App\Helpers\NotificationHelper;

class DeclineTheBookings extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'decline:booking';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This Cron will decline all the bookings which are in there idle state since 1h.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $nowTime = Carbon::now();
        $oneHAgo = $nowTime->subHour();

        Booking::where('created_at','<=',$oneHAgo)
            ->get()
            ->map(function($booking) use ($nowTime){

                $bookingId = $booking->id;
                $booking   = Booking::where('id',$bookingId)->first();

                if($booking->status != 'pending'){
                    return response()->json(['status' => false,'message' => 'You can\'t decline booking.']);
                }

                $declinedBooking = DeclinedBooking::where(['booking_id'=>$bookingId,'driver_id'=>$booking->trip->driver->id])->first();

                if($booking->status == 'rejected'){
                    return response()->json(['status' => false,'message' => 'booking already declined.']);
                }

                DeclinedBooking::updateOrCreate(['booking_id' => $bookingId,'driver_id' => $booking->trip->driver->id],['booking_id' => $bookingId,'driver_id' => $booking->trip->driver->id]); 
                $booking->decline_reason     = 'Sorry! Your booking request declined due to driver inactivity.';
                $booking->status             = 'rejected';
                $booking->save();

                /** declined booking rider notifications **/
                $booking->trip->booking_id   = $booking->id;
                $booking->trip->seats        = $booking->seats;
                $booking->user->notify(new BookingDeclinedNotification($booking->trip));

                if($booking->user->active_mode == 'user'){
                    NotificationHelper::sendPushNotification($booking->user,'UserTripBookingDeclined',$booking->trip->toArray());
                }
            });
    }
}
