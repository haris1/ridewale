<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FareManagement extends Model
{
    protected $fillable = [
		'to_city_id','from_city_id','price'
	];

	public function to_city(){
		return $this->belongsTo('App\Work_city','to_city_id');
	}

	public function from_city(){
		return $this->belongsTo('App\Work_city','from_city_id');
	}
}
