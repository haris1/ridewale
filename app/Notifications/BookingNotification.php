<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class BookingNotification extends Notification
{
    use Queueable;
    public $tripInfo;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($tripInfo)
    {
        $this->tripInfo = $tripInfo;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail','database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $tripInfo   = $this->tripInfo;
        $booking    = 'Booking Id : #'.$tripInfo->booking_id;
        $seats      = 'Total Seats : '.$tripInfo->seats;

        if($tripInfo->user_type == 'driver'){
            $message    = $tripInfo->user_name.' booked trip from '.$tripInfo->from_location->work_city->name.' to '. $tripInfo->to_location->work_city->name;
        }
        else{
            $message    = 'Your trip from '.$tripInfo->from_location->work_city->name.' to '. $tripInfo->to_location->work_city->name.' booked successfully.';
        }

        return (new MailMessage)
                ->line($message)
                ->line($booking)
                ->line($seats)
                ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'trip_id'       => $this->tripInfo->id,
            'booking_id'    => $this->tripInfo->booking_id
        ];
    }
}
