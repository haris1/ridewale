<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class TripReminderNotification extends Notification
{
    use Queueable;
    public $tripInfo;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($tripInfo)
    {
        $this->tripInfo=$tripInfo;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail','database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $tripInfo=$this->tripInfo;

        $message='Your trip from '.$tripInfo->from_city->name.' to '. $tripInfo->to_city->name.' on '.date('d M, Y',strtotime($tripInfo->trip_date)).' is stared at '.date('H:i a',strtotime($tripInfo->pickup_time));
        
        return (new MailMessage)
        ->line($message)
        ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'trip_id' => $this->tripInfo['trip_id'],
        ];
    }
}
