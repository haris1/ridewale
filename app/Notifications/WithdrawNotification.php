<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class WithdrawNotification extends Notification
{
    use Queueable;
    public $account_balance;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($account_balance)
    {
        $this->account_balance=$account_balance;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail','database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $account_balance = $this->account_balance;

        $message='You withdraw $ '.number_format($account_balance,2).' on '.date('d M, Y H:i:s');

        return (new MailMessage)
        ->line($message);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'amount'=>$this->account_balance,
        ];
    }
}
