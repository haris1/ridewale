<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CancelTripNotification extends Notification implements ShouldQueue
{
    use Queueable;
    public $tripInfo;
    public $bookingInfo;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($tripInfo,$bookingInfo)
    {
        $this->tripInfo     =   $tripInfo;
        $this->bookingInfo  =   $bookingInfo;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail','database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $tripInfo       = $this->tripInfo;
        $bookingInfo    = $this->bookingInfo;
        $message2       = '';
        // print_r($bookingInfo);die();
        $message        = 'Your trip from '.$tripInfo->from_location->work_city->name.' to '. $tripInfo->to_location->work_city->name.' is cancelled by driver due to '.$tripInfo->cancelled_reason.'.';
        $message1       = '';

        if($bookingInfo['type'] == 'pending'){

            $message1   = 'We haven\'t charged your card yet. You won\'t need to worry about the Refund.';
            $message2   = 'Booking ID : '.$bookingInfo['booking_id'];

        }elseif($bookingInfo['type'] == 'confirmed'){

            $message1   = 'A refund of $'.number_format($bookingInfo['amount'],2).' will be issued to your card ending in '.$bookingInfo['card_used'].' within 3-5 bussiness days.';
            $message2   = 'Booking ID : '.$bookingInfo['booking_id'];

        }else{

             $message   = 'You have cancel trip from '.$tripInfo->from_location->work_city->name.' to '. $tripInfo->to_location->work_city->name.' due to '.$tripInfo->cancelled_reason.'.';
            $message1   = 'Trip cancellation charge $'.number_format($bookingInfo['penalty']['penalty_amount'],2).' deducted from your wallet.' ;
        }
        
        return (new MailMessage)
        ->line($message)
        ->line($message1)
        ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'trip_id'   => $this->tripInfo->id,
            'booking_id'=> $this->bookingInfo['booking_id']
        ];
    }
}
