<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CompleteTripNotification extends Notification
{
    use Queueable;
    public $tripInfo;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($tripInfo)
    {
        $this->tripInfo=$tripInfo;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail','database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $tripInfo=$this->tripInfo;

        // if($tripInfo->user_type == 'driver'){
            $message='Your trip from '.$tripInfo->from_location->work_city->name.' to '. $tripInfo->to_location->work_city->name.' on '.date('d M, Y',strtotime($tripInfo->trip_date)).' is complete.';
        // }
        // else{
        //     $message='Your trip from '.$tripInfo->from_city->name.' to '. $tripInfo->to_city->name.' on '.date('d M, Y',strtotime($tripInfo->trip_date)).' booked on '.date('d M, Y',strtotime($notifiable->trip_date)).' is started.';
        // }
        return (new MailMessage)
        ->line($message)
        ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'trip_id' => $this->tripInfo->id,
            'booking_id'=>$this->tripInfo->booking_id
        ];
    }
}
