<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class BookingConfirmationNotification extends Notification
{
    use Queueable;
    public $tripInfo;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($tripInfo)
    {
        $this->tripInfo=$tripInfo;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail','database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
     $tripInfo = $this->tripInfo;
     $booking  = 'Booking Id : #'.$tripInfo->id;
     $seats    = 'Total Seats : '.$tripInfo->seats;


     $message  = 'Your trip from '.$tripInfo->trip->from_location->work_city->name.' to '. $tripInfo->trip->to_location->work_city->name.' is confirmed.';
     $message1 = 'Your booking confirmation otp : '.$tripInfo->otp.'.' ;
     $message3 = 'Please keep the one time password with you. This will helps you to verify your identity.';

     return (new MailMessage)
     ->line($message)
     ->line($message1)
     ->line($message3)
     ->line($booking)
     ->line($seats)
     ->line('Thank you for using our application!');
 }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'trip_id'       => $this->tripInfo->trip->id,
            'booking_id'=>$this->tripInfo->id
        ];
    }
}
