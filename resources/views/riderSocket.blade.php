<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.2.0/socket.io.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div id="app">
            <div class="container mt-5">
                <h3 class="display-3 text-center"> {{ __('socket.testing_page_for_patient') }} </h3>
            </div>
        </div>

    </body>
        <script src="{{ asset('js/app.js') }}"></script>
        <script>
            
            let userId = 2, userName = 'Muhammad Danish', role = 'rider'
            let options = {
                query:"user_id="+userId+"&username="+userName+"&role="+role+"&latitude="+23.5+"&longitude="+72.5
            };
            let socket = io("http://localhost:4006",options);

            socket.on('connect', function () {
                let jwt = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL3JpZGV3YWxlXC9hcGlcL3YzXC9sb2dpbiIsImlhdCI6MTU2MjkyNjc4NiwiZXhwIjoxNTYyOTI2ODQ2LCJuYmYiOjE1NjI5MjY3ODYsImp0aSI6IkhlNWFYdnZEc09tSnR5UjUiLCJzdWIiOjIsInBydiI6IjdhM2MxNWY4MmMwMWJjNTExOTZhODc2NTk1YTNjNjBmOTFlZGNlY2IifQ.QF7GawKdMtn7EO0mi3aDScXusSq2t-W_GrpIUMpy8zg';
                socket
                .on('authenticated', function () {
                })
                .emit('authenticate', {token: jwt});

                socket.on("unauthorized", function(error, callback) {
                    if (error.data.type == "UnauthorizedError" || error.data.code == "invalid_token") {
                        callback();
                        console.log("User's token has expired");
                    }
                });

                socket.on('driverIsOnRide', (data) => {
                    console.log('driverIsOnRide');
                    console.log(data);
                });
            });
        </script>
</html>
