<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.2.0/socket.io.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div id="app">
            <div class="container mt-5">
                <h3 class="display-3 text-center"> {{ __('socket.testing_page_for_doctor') }} </h3>

                <div id="riderReqCame" style="display: none;">
                    <h6 class="text-primary" id="event-data"></h6>
                    <h6 class="text-primary" id="counter-data"></h6>
                    <div class="controls">
                        <button class="btn btn-success accept">Accept</button>
                        <button class="btn btn-danger reject">Reject</button>
                    </div>
                </div>
                <div id="riderIsOnRide" style="display: none;">
                    <button class="btn btn-danger endTheCallBtn">EndTheCall</button>
                </div>

                <div id="callEndedByDoctor" style="display: none;">
                    <h3 class="text-success">Call Ended Successfully</h3>
                </div>
            </div>

             <button class="btn btn-danger tik">Tik</button>
        </div>

    </body>
        {{-- <script src="{{ asset('js/app.js') }}"></script> --}}
        <script>
            
            let userId = 3, userName = 'Mark Zukerberg', role = 'driver';

            let options = {
                query:"user_id="+userId+"&username="+userName+"&role="+role+"&latitude="+23.6+"&longitude="+72.6

            };
            let socket = io("http://localhost:4006",options);

            $(document).on('click', '.tik', () => {
                // socket.emit('tik', null, function(confirmation){
                //     console.log(confirmation);
                // });

                socket.emit('updateLocationInDB', {latitude:23.45678, longitude:72.12345},(confirmation) => {
                    console.log(confirmation);
                });
            })
            // socket.on('connect', function (socket) {
            //     // socket.emit('authenticate', {token: your_jwt});
                
            //     socket.on('authenticated', function () {
            //         console.log('Authenticated');
            //     });
            // });
            let counts = 1;
            socket.on('connect', function () {
                console.log(socket.id);
                let jwt = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL3JpZGV3YWxlXC9hcGlcL3YzXC9sb2dpbiIsImlhdCI6MTU2MzQzNTYwNywiZXhwIjoxNTYzNTIyMDA3LCJuYmYiOjE1NjM0MzU2MDcsImp0aSI6ImdVYUxiakV1NjRzVTJsekgiLCJzdWIiOjIsInBydiI6IjdhM2MxNWY4MmMwMWJjNTExOTZhODc2NTk1YTNjNjBmOTFlZGNlY2IifQ.h3tcaBwc9LhIZNQ_sNDO3l0fDCeqpi00FI9cwyPiQxo';
                let initialized = false;
                socket.on('authenticated', function () {
                    if(!initialized){
                        console.log('bhai bhai '+ counts);
                        counts += 1;
                        initialized = true;
                        socket.emit('makeMeOnline', '',(confirmation) => {
                            console.log(confirmation);
                        });
                        // socket.emit('updateCarMove', {latitude: 23.45, longitude:70.25},(confirmation) => {
                        //     console.log(confirmation);
                        // });
                    }
                });
                socket.emit('authenticate', {token: jwt});

                socket.on("unauthorized", function(error, callback) {
                    if (error.data.type == "UnauthorizedError" || error.data.code == "invalid_token") {
                        // callback();
                        console.log("User's token has expired");
                    }
                });

                socket.on('riderIsOnRide', (data) => {
                    console.log('riderIsOnRide');
                    $('#riderReqCame').hide();
                    $('#riderIsOnRide').show();
                    console.log(socket);
                    console.log(socket.patient);
                    console.log(data);
                });


            });

            let rejected = false;
            socket.emit('readys');
            socket.on('riderReqCame', (data) => {
                console.log('=---------------------------------');
                console.log(data);
                $('#riderReqCame').show();
                console.log($('#riderReqCame #event-data'));
                $('#riderReqCame #event-data').text(JSON.stringify(data));
                rejected = false;
                startCounter();
                // document.getElementById("text-ptimary").innerHTML = "Request Came";
                // document.getElementById('text-ptimary').innerHTML('Request Came', data);
            })
            socket.on('disconnect', (data) => {
                console.log('You were disconnected');
                // location.reload();
            })

            function startCounter(){

                // Set the date we're counting down to
                let countDownDate = new Date();
                countDownDate.setSeconds(countDownDate.getSeconds() + 20);

                // Update the count down every 1 second
                var x = setInterval(function() {

                    if(rejected){
                        
                        console.log('rejected');
                        console.log(rejected);
                        document.getElementById("counter-data").innerHTML = "REJECTED";
                        clearInterval(x);
                    }else{

                        // Get today's date and time
                        var now = new Date().getTime();

                        // Find the distance between now and the count down date
                        var distance = countDownDate - now;

                        // Time calculations for days, hours, minutes and seconds
                        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                        // Display the result in the element with id="demo"
                        document.getElementById("counter-data").innerHTML = days + "d " + hours + "h "
                        + minutes + "m " + seconds + "s ";

                        // If the count down is finished, write some text 
                        if (distance < 0) {
                            clearInterval(x);
                            document.getElementById("counter-data").innerHTML = "EXPIRED";
                            socket.emit('notResponded');
                        }
                    }

                }, 1000);
            }

            $(document).on('click', '.reject', () => {
                rejected = true;
                console.log('clicked');
                socket.emit('rejectTheRequest', null, function(confirmation){
                    console.log(confirmation);
                });
            })

            $(document).on('click', '.accept', () => {
                console.log('clicked');
                socket.emit('acceptTheRequest', null, function(confirmation){
                    console.log(confirmation);
                });
            })

            $('.endTheCallBtn').on('click', () => {
                console.log('clicked');
                socket.emit('endTheCall');
            })
            socket.on('callEndedByDoctor', () => {
                $('#callEndedByDoctor').show();
            });
        </script>
</html>
