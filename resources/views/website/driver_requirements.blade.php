@section('title','RideChimp | Driver Requirements')
@extends('website.layouts.app')
@section('styles')

@endsection
@section('content')
<div class="container">
    <div class="mentallactbox">
        <div class="surrentlyavbl">
            <h3>RideChimp is currently available in <br>
            provinces & cities mentioned below.</h3>
            <p>Please click on the city you live to get started</p>
        </div>
        <div class="availablecurrlist">
            <div class="avaiblecurlst_left">
                <ul>
                    <li><h4>ALBERTA</h4></li>
                    <li><a href="javascript:void(0)">Airdrie</a></li>
                    <li><a href="javascript:void(0)">Beaumount</a></li>
                    <li><a href="javascript:void(0)">Brooks</a></li>
                    <li><a href="javascript:void(0)">Calgary</a></li>
                    <li><a href="javascript:void(0)">Camrose</a></li>
                    <li><a href="javascript:void(0)">Edmonton</a></li>
                    <li><a href="javascript:void(0)">Grand Pairie</a></li>
                    <li><a href="javascript:void(0)">Leduc</a></li>
                    <li><a href="javascript:void(0)">Lethbridge</a></li>
                    <li><a href="javascript:void(0)">Loydminster</a></li>
                    <li><a href="javascript:void(0)">Medicine Hat</a></li>
                    <li><a href="javascript:void(0)">Red Deer</a></li>
                    <li><a href="javascript:void(0)">Spruce Grove</a></li>
                    <li><a href="javascript:void(0)">St. Albert</a></li>
                    <li><a href="javascript:void(0)">Wetaskiwin</a></li>
                </ul>
                <ul>
                    <li><h4>ONTARIO</h4></li>
                    <li><a href="javascript:void(0)">Toronto</a></li>
                    <li><a href="javascript:void(0)">Mississauga</a></li>
                    <li><a href="javascript:void(0)">Brampton</a></li>
                    <li><a href="javascript:void(0)">Hamilton</a></li>
                    <li><a href="javascript:void(0)">Niagara Falls</a></li>
                    <li><a href="javascript:void(0)">Oshawa</a></li>
                </ul>
            </div>
            <div class="avaiblecurlst_right">
                <div class="custom-select">
                    <select>
                        <option value="0">Select your city</option>
                        <option value="1">Toronto</option>
                        <option value="2">Mississauga</option>
                        <option value="3">Brampton</option>
                        <option value="4">Hamilton</option>
                        <option value="5">Niagara Falls</option>
                        <option value="6">Oshawa</option>
                    </select>
                </div>
                <a href="{{ route('website.getting_started') }}">GET STARTED</a>
            </div>
        </div>
    </div>

</div>
@endsection

@section('scripts')
<script>    var x, i, j, selElmnt, a, b, c;
    /*look for any elements with the class "custom-select":*/
    x = document.getElementsByClassName("custom-select");
    for (i = 0; i < x.length; i++) {
        selElmnt = x[i].getElementsByTagName("select")[0];
        /*for each element, create a new DIV that will act as the selected item:*/
        a = document.createElement("DIV");
        a.setAttribute("class", "select-selected");
        a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
        x[i].appendChild(a);
        /*for each element, create a new DIV that will contain the option list:*/
        b = document.createElement("DIV");
        b.setAttribute("class", "select-items select-hide");
        for (j = 1; j < selElmnt.length; j++) {
            /*for each option in the original select element,
            create a new DIV that will act as an option item:*/
            c = document.createElement("DIV");
            c.innerHTML = selElmnt.options[j].innerHTML;
            c.addEventListener("click", function(e) {
            /*when an item is clicked, update the original select box,
            and the selected item:*/
            var y, i, k, s, h;
            s = this.parentNode.parentNode.getElementsByTagName("select")[0];
            h = this.parentNode.previousSibling;
            for (i = 0; i < s.length; i++) {
                if (s.options[i].innerHTML == this.innerHTML) {
                    s.selectedIndex = i;
                    h.innerHTML = this.innerHTML;
                    y = this.parentNode.getElementsByClassName("same-as-selected");
                    for (k = 0; k < y.length; k++) {
                        y[k].removeAttribute("class");
                    }
                    this.setAttribute("class", "same-as-selected");
                    break;
                }
            }
            h.click();
            });
            b.appendChild(c);
        }
        x[i].appendChild(b);
        a.addEventListener("click", function(e) {
            /*when the select box is clicked, close any other select boxes,
            and open/close the current select box:*/
            e.stopPropagation();
            closeAllSelect(this);
            this.nextSibling.classList.toggle("select-hide");
            this.classList.toggle("select-arrow-active");
        });
    }
    function closeAllSelect(elmnt) {
        /*a function that will close all select boxes in the document,
        except the current select box:*/
        var x, y, i, arrNo = [];
        x = document.getElementsByClassName("select-items");
        y = document.getElementsByClassName("select-selected");
        for (i = 0; i < y.length; i++) {
            if (elmnt == y[i]) {
                arrNo.push(i)
            } else {
                y[i].classList.remove("select-arrow-active");
            }
        }
        for (i = 0; i < x.length; i++) {
            if (arrNo.indexOf(i)) {
                x[i].classList.add("select-hide");
            }
        }
    }
    /*if the user clicks anywhere outside the select box,
    then close all select boxes:*/
    document.addEventListener("click", closeAllSelect);
</script>
@endsection
