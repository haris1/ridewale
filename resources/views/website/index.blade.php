@section('title','RideChimp | Home')
@extends('website.layouts.app')
@section('styles')

@endsection
@section('content')
<div class="finditercitybox">
    <div class="leftrihtboxinercover">
        <div class="intercityFindtextvox">
          <div class="topmidtextinerpatrset">
              <h3>Going out of town?</h3>
              <p>We've got a RideChimp</p>
              <ul>
                  <li><a href="javascript:void(0)"><img src="{{ asset('web_assets/svg/android-icon2.svg') }}" alt=""><span>PLAY STORE</span></a></li>
                  <li><a href="javascript:void(0)"><img src="{{ asset('web_assets/svg/ios-icon2.svg') }}" alt=""><span>APP STORE</span></a></li>
              </ul>
          </div>
        </div>
        <div class="rihtimgtobnset">
           <img src="{{ asset('web_assets/images/ride_righticon.png') }}">
        </div>
        <!-- <div class="rideimageboxpart">
            <img src="{{ asset('web_assets/images/ridecover_4.gif') }}" alt="">
        </div>
        <div class="respoimgboxsetr">
            <img src="{{ asset('web_assets/images/bg-img4.png') }}" alt="">
        </div> -->
    </div>
</div>
<div class="container">
    <div class="driverideboxiner">
        <div class="drieidboxcenter">
            <div class="rightpedinr">
                <div class="driridinerbox">
                    <h3>DRIVE</h3>
                    <p>Earn money by dropping riders to nearby <br>
                    cities</p>
                    <a href="javascript:void(0)">Learn More <img src="{{ asset('web_assets/svg/right-arrow.svg') }}"></a>
                </div>
            </div>
            <div class="leftpedinr">
                <div class="driridinerbox">
                    <h3>RIDE</h3>
                    <p>Book scheduled rides right from our app
                    when you are going out of town.</p>
                    <a href="javascript:void(0)">Learn More <img src="{{ asset('web_assets/svg/right-arrow.svg') }}"></a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">

  <div class="searchridercov">
      <a href="javascript:void(0)">
        <div class="inersearchbox">
          <div class="lefttextsrc">
            <h6>Search Rides</h6>
            <p>Find scheduled rides to nearby cities</p>
          </div>
          <img src="{{ asset('web_assets/svg/right-arrow2.svg') }}">
        </div>
      </a>
  </div>

</div>
<div class="appdetail_textimg">
    <div class="container">
        <div class="imgtextcoverpart ditboxpart1">
            <div class="appdetailleft">
                <img src="{{ asset('web_assets/svg/app-detail-img1.svg') }}" alt="">
            </div>
            <div class="appdetailright">
                <div class="imganimpulse rowonesetincol">
                    <div class="pulse-container">
                        <div class="pulse">
                            <svg height="240" width="240">
                                <circle class="still" cx="65" cy="65" r="65" />
                                <circle cx="65" cy="65" r="65" />
                                <circle cx="65" cy="65" r="65" />
                                <circle cx="65" cy="65" r="65" />
                                <circle cx="65" cy="65" r="65" />
                            </svg>
                        </div>
                    </div>
                    <img src="{{ asset('web_assets/svg/find-icon.svg') }}" alt="">
                </div>
                <div class="alditalext">
                    <p>Tap a button to find <br>
                        intercity rides in <br>
                    minutes.</p>
                </div>
            </div>
        </div>
        <div class="imgtextcoverpart ditboxpart2">
            <div class="appdetailright">
                <div class="imganimpulse rowonesetincol">
                    <div class="pulse-container">
                        <div class="pulse">
                            <svg height="240" width="240">
                                <circle class="still" cx="65" cy="65" r="65" />
                                <circle cx="65" cy="65" r="65" />
                                <circle cx="65" cy="65" r="65" />
                                <circle cx="65" cy="65" r="65" />
                                <circle cx="65" cy="65" r="65" />
                            </svg>
                        </div>
                    </div>
                    <img src="{{ asset('web_assets/svg/credit-icon.svg') }}" alt="">
                </div>
                <div class="alditalext">
                    <p>Book the ride you like <br>
                        instantly with credit <br>
                    or master card</p>
                </div>
            </div>
            <div class="appdetailleft">
                <img src="{{ asset('web_assets/svg/app-detail-img2.svg') }}" alt="">
            </div>
        </div>
        <div class="imgtextcoverpart ditboxpart3">
            <div class="appdetailleft">
                <img src="{{ asset('web_assets/svg/app-detail-img3.svg') }}" alt="">
            </div>
            <div class="appdetailright">
                <div class="imganimpulse rowonesetincol">
                    <div class="pulse-container">
                        <div class="pulse">
                            <svg height="240" width="240">
                                <circle class="still" cx="65" cy="65" r="65" />
                                <circle cx="65" cy="65" r="65" />
                                <circle cx="65" cy="65" r="65" />
                                <circle cx="65" cy="65" r="65" />
                                <circle cx="65" cy="65" r="65" />
                            </svg>
                        </div>
                    </div>
                    <img src="{{ asset('web_assets/svg/track-icon.svg') }}" alt="">
                </div>
                <div class="alditalext">
                    <p>Keep a track of active<br>
                        and completed<br>
                    rides</p>
                </div>
            </div>
        </div>
        <div class="imgtextcoverpart ditboxpart4">
            <div class="appdetailright">
                <div class="imganimpulse rowonesetincol">
                    <div class="pulse-container">
                        <div class="pulse">
                            <svg height="240" width="240">
                                <circle class="still" cx="65" cy="65" r="65" />
                                <circle cx="65" cy="65" r="65" />
                                <circle cx="65" cy="65" r="65" />
                                <circle cx="65" cy="65" r="65" />
                                <circle cx="65" cy="65" r="65" />
                            </svg>
                        </div>
                    </div>
                    <img src="{{ asset('web_assets/svg/driver-icon.svg') }}" alt="">
                </div>
                <div class="alditalext">
                    <p>Become a driver <br>
                        to offer intercity rides <br>
                    & earn money.</p>
                </div>
            </div>
            <div class="appdetailleft">
                <img src="{{ asset('web_assets/svg/app-detail-img4.svg') }}" alt="">
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    // $(function(){
    //   setInterval(threeSecondFunction, 2000);
    // });

    // function threeSecondFunction() {
    //      var scroll = $(window).scrollTop();
    //     if( scroll >= $("#section-1").offset().top-180){
    //         if(!$("#section-1").hasClass("effect") && !$("#section-1").hasClass("removeeffect")) {
    //             $("#section-1").addClass("effect");
    //             setTimeout(function(){ $("#section-1").addClass("removeeffect"); $("#section-1").removeClass("effect"); }, 5000);
    //             setTimeout(function(){ $("#section-1").removeClass("removeeffect"); }, 8000);
    //         }
    //     }
    //     if( scroll >= $("#section-2").offset().top-150){
    //         if(!$("#section-2").hasClass("effect2")  && !$("#section-2").hasClass("removeeffect")) {
    //             $("#section-2").addClass("effect2");
    //             setTimeout(function(){ $("#section-2").addClass("removeeffect"); $("#section-2").removeClass("effect2");}, 5000);
    //             setTimeout(function(){ $("#section-2").removeClass("removeeffect"); }, 8000);
    //         }
    //     }
    //     if( scroll >= $("#section-3").offset().top-150){
    //         if(!$("#section-3").hasClass("effect3")  && !$("#section-3").hasClass("removeeffect")) {
    //             $("#section-3").addClass("effect3");
    //             setTimeout(function(){ $("#section-3").addClass("removeeffect"); $("#section-3").removeClass("effect3");}, 5000);
    //             setTimeout(function(){ $("#section-3").removeClass("removeeffect"); }, 8000);
    //         }
    //     }
    //     if( scroll >= $("#section-4").offset().top-150){
    //         if(!$("#section-4").hasClass("effect4")  && !$("#section-4").hasClass("removeeffect")) {
    //             $("#section-4").addClass("effect4");
    //             setTimeout(function(){ $("#section-4").addClass("removeeffect"); $("#section-4").removeClass("effect4");}, 5000);
    //             setTimeout(function(){ $("#section-4").removeClass("removeeffect"); }, 8000);
    //         }
    //     }
    // }


</script>
<script type="text/javascript">

//     jQuery(document).ready(function () {
//         jQuery(window).load(function() {
//             setTimeout(function(){ jQuery(".rideimageboxpart").css("display", "inline-block"); }, 200);
//         })
//
//     });
//     jQuery(document).on('click','#bodyhide', function(){
// $(".rightmeniimg").find('img').attr("src", "https://rideforu.co/public/web_assets/images/ridecover_4.gif");
//     });


</script>

@endsection
