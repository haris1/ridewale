@section('title','RideChimp | Cities')
@extends('website.layouts.app')
@section('styles')

@endsection
@section('content')
<div class="container">
    <div class="mentallactbox">
        <div class="surrentlyavbl">
            <h3>RideChimp is currently available in <br>
            provinces & cities mentioned below.</h3>
            <p>Please click on the city you live to get started</p>
        </div>
        <div class="availablecurrlist">
            <div class="avaiblecurlst_left">
                <ul>
                    <li><h4>ALBERTA</h4></li>
                    <li><a href="javascript:void(0)">Airdrie</a></li>
                    <li><a href="javascript:void(0)">Beaumount</a></li>
                    <li><a href="javascript:void(0)">Brooks</a></li>
                    <li><a href="javascript:void(0)">Calgary</a></li>
                    <li><a href="javascript:void(0)">Camrose</a></li>
                    <li><a href="javascript:void(0)">Edmonton</a></li>
                    <li><a href="javascript:void(0)">Grand Pairie</a></li>
                    <li><a href="javascript:void(0)">Leduc</a></li>
                    <li><a href="javascript:void(0)">Lethbridge</a></li>
                    <li><a href="javascript:void(0)">Loydminster</a></li>
                    <li><a href="javascript:void(0)">Medicine Hat</a></li>
                    <li><a href="javascript:void(0)">Red Deer</a></li>
                    <li><a href="javascript:void(0)">Spruce Grove</a></li>
                    <li><a href="javascript:void(0)">St. Albert</a></li>
                    <li><a href="javascript:void(0)">Wetaskiwin</a></li>
                </ul>
                <ul>
                    <li><h4>ONTARIO</h4></li>
                    <li><a href="javascript:void(0)">Toronto</a></li>
                    <li><a href="javascript:void(0)">Mississauga</a></li>
                    <li><a href="javascript:void(0)">Brampton</a></li>
                    <li><a href="javascript:void(0)">Hamilton</a></li>
                    <li><a href="javascript:void(0)">Niagara Falls</a></li>
                    <li><a href="javascript:void(0)">Oshawa</a></li>
                </ul>
            </div>
            <div class="avaiblecurlst_right">
                <div class="custom-select">
                    <select>
                        <option value="0">Select your city</option>
                        <option value="1">Toronto</option>
                        <option value="2">Mississauga</option>
                        <option value="3">Brampton</option>
                        <option value="4">Hamilton</option>
                        <option value="5">Niagara Falls</option>
                        <option value="6">Oshawa</option>
                    </select>
                </div>
                <a href="javascript:void(0)">GET STARTED</a>
            </div>
        </div>
    </div>

</div>
@endsection

@section('scripts')

@endsection
