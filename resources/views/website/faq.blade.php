@section('title','RideChimp | Home')
@extends('website.layouts.app')
@section('styles')

@endsection
@section('content')


<div class="container">
  <div class="faqpagetitlein">
    <h3>Frequently Asked Questions</h3>
  </div>
</div>
<div class="container">
  <div class="driversriderstabdit">

    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#home">FOR DRIVERS</a></li>
      <li><a data-toggle="tab" href="#menu1">FOR RIDERS</a></li>
    </ul>

    <div class="tab-content">
      <div id="drivers" class="tab-pane fade in active">
        <div class="allcollspsecover">
          <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
           <div class="panel panel-default">
             <div class="panel-heading" role="tab" id="headingOne">
               <div class="panel-title">
               <a class="pagehedpart collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
               How to become a driver?
               </a>
             </div>
             </div>
             <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
               <div class="panel-body">
                 <div class="acodinbodypart">
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
    standard dummy text ever since the 1500s, when an unknown printer took a galley. Lorem Ipsum is simply dummy
    text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since
    the 1500s, when an unknown printer took a galley  </p>
                 </div>
               </div>
             </div>
           </div>

           <div class="panel panel-default">
             <div class="panel-heading" role="tab" id="headingTwo">
               <div class="panel-title">
               <a class="pagehedpart collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
               How does it work?
             </a>
           </div>
             </div>
             <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
               <div class="panel-body">
                 <div class="acodinbodypart">
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
    standard dummy text ever since the 1500s, when an unknown printer took a galley. Lorem Ipsum is simply dummy
    text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since
    the 1500s, when an unknown printer took a galley  </p>
                 </div>
               </div>
             </div>
           </div>

           <div class="panel panel-default">
             <div class="panel-heading" role="tab" id="headingThree">
               <div class="panel-title">
               <a class="pagehedpart collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
              	How does it work?
               </a>
             </div>
             </div>
             <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
               <div class="panel-body">
                 <div class="acodinbodypart">
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
    standard dummy text ever since the 1500s, when an unknown printer took a galley. Lorem Ipsum is simply dummy
    text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since
    the 1500s, when an unknown printer took a galley  </p>
                 </div>
               </div>
             </div>
           </div>
         </div>
       </div>

      </div>
      <div id="riders" class="tab-pane fade">
        <h3>FOR RIDERS</h3>

      </div>
    </div>

  </div>
</div>

@endsection

@section('scripts')

@endsection
