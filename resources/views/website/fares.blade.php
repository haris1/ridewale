@section('title','RideChimp | Home')
@extends('website.layouts.app')
@section('styles')

@endsection
@section('content')


<div class="container">
	<div class="pagetitletopbox">
		<div class="lefttext_title">
			<h3>Heading out of town?</h3>
			<p>Ride with us & save money for your <br>
			next trip</p>
		</div>
		<div class="rightimg_title">
			<img src="{{ asset('web_assets/svg/topimgside2.svg') }}">
		</div>
	</div>
	<div class="calculateafarebox">
		<h3>Let's calculate a fare...</h3>
		<p>For your next trip with RideChimp</p>
		<div class="iputgroupcover">
			<div class="form-group">
				<img src="{{ asset('web_assets/svg/location-icon.svg') }}">
			  	<input type="text" class="form-control" id="" placeholder="Going From?">
			</div>
			<div class="form-group">
				<img src="{{ asset('web_assets/svg/location-icon.svg') }}">
		  		<input type="text" class="form-control" id="" placeholder="Going To?">
			</div>
		</div>
		<button><img src="{{ asset('web_assets/svg/search-icon.svg') }}">Search</button>
		<div class="findoutherebox">
			<h5>Not sure in which cities RideChimp is currently operating? <a href="javascript:void(0)">Find out here</a></h5>
		</div>
	</div>
</div>


@endsection

@section('scripts')

@endsection
