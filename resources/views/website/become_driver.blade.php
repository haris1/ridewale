@section('title','RideChimp | Become Driver')
@extends('website.layouts.app')
@section('styles')

@endsection
@section('content')
<div class="container">
    <div class="becmdriv_pagetitle">
        <h3>Become A Driver</h3>
        <p>Please complete a form provided below & we will get in touch with you.</p>
    </div>
    <div class="becdriformcove">
        <div class="inerformpart">

            <form method="post" id="becomeDriverForm">

                <div class="formboxingrp1">
                    <div class="formgroupinput">
                        <div class="form-group">
                            <label for="">First Name*</label>
                            <input type="text" class="form-control" id="" name="first_name" placeholder="Enter First Name">
                        </div>
                    </div>
                    <div class="formgroupinput">
                        <div class="form-group">
                            <label for="">Last Name*</label>
                            <input type="text" class="form-control" id="" name="last_name" placeholder="Enter First Name">
                        </div>
                    </div>
                    <div class="formgroupinput phonenumberset">
                        <div class="form-group">
                            <label for="">Phone Number*</label>
                            <span>(+1)</span>
                            <input type="text" class="form-control" id="" name="phone_number" placeholder="Your phone number" maxlength="10">
                        </div>
                    </div>
                    <div class="selectboxcovedis">
                        <label for="">Select Gender*</label>
                        <select class="custom-select" name="gender">
                            <option value="">Select Gender*</option>
                            <option value="male">Male</option>
                            <option value="female">Female</option>
                        </select>
                    </div>
                    <div class="formgroupinput">
                        <div class="form-group">
                            <label for="">Email Address*</label>
                            <input type="text" class="form-control" id="" name="email" placeholder="Enter Email Address">
                        </div>
                    </div>
                    <div class="formgroupinput">
                        <div class="form-group">
                            <label for="">Where do you currently live?*</label>
                            <input type="text" class="form-control" id="" name="currently_live" placeholder="Enter City Name">
                        </div>
                    </div>
                </div>

                <div class="formboxingrp2">
                    <div class="rediobtnbox1">
                        <h4>How frequently are you going to drive?*</h4>
                        <div class="rdbtnwidthset">
                            <label class="radiocust">5 Times A Week
                                <input type="radio" value="5_times_week" name="going_to_drive">
                                <span class="checkmark"></span>
                            </label>
                            <label class="radiocust">3 Times A Week
                                <input type="radio" value="3_times_week" name="going_to_drive">
                                <span class="checkmark"></span>
                            </label>
                        </div>

                        <div class="rdbtnwidthset">
                            <label class="radiocust">Once A Week
                                <input type="radio" value="once_week" name="going_to_drive">
                                <span class="checkmark"></span>
                            </label>
                            <label class="radiocust">Once A Month
                                <input type="radio" value="once_month" name="going_to_drive">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    </div>

                    <div class="rediobtnbox1">
                        <h4>What cities you are driving to most frequently?*</h4>
                        <div class="rdbtnwidthset">
                            <label class="checkoxsustom">Athabasca
                                <input type="checkbox" value="athabasca" name="most_frequently[]">
                                <span class="checkmark"></span>
                            </label>
                            <label class="checkoxsustom">Calgary
                                <input type="checkbox" value="calgary" name="most_frequently[]">
                                <span class="checkmark"></span>
                            </label>

                        </div>

                        <div class="rdbtnwidthset">
                            <label class="checkoxsustom">BonnyVille
                                <input type="checkbox" value="bonny_ville" name="most_frequently[]">
                                <span class="checkmark"></span>
                            </label>
                            <label class="checkoxsustom">Camrose
                                <input type="checkbox" value="camrose" name="most_frequently[]">
                                <span class="checkmark"></span>
                            </label>
                        </div>

                        <div class="rdbtnwidthset">
                            <label class="checkoxsustom">Cold Lake
                                <input type="checkbox" value="Cold_Lake" name="most_frequently[]">
                                <span class="checkmark"></span>
                            </label>
                            <label class="checkoxsustom">Fort Mcmurray
                                <input type="checkbox" value="Fort_Mcmurray" name="most_frequently[]">
                                <span class="checkmark"></span>
                            </label>
                        </div>

                        <div class="rdbtnwidthset">
                            <label class="checkoxsustom">Edson
                                <input type="checkbox" value="Edson" name="most_frequently[]">
                                <span class="checkmark"></span>
                            </label>
                            <label class="checkoxsustom">Fort Saskatchewan
                                <input type="checkbox" value="Fort_Saskatchewan" name="most_frequently[]">
                                <span class="checkmark"></span>
                            </label>
                        </div>

                        <div class="rdbtnwidthset">
                            <label class="checkoxsustom">Jasper
                                <input type="checkbox" value="Jasper" name="most_frequently[]">
                                <span class="checkmark"></span>
                            </label>
                            <label class="checkoxsustom">Lloydminster
                                <input type="checkbox" value="Lloydminster" name="most_frequently[]">
                                <span class="checkmark"></span>
                            </label>
                        </div>

                        <div class="rdbtnwidthset">
                            <label class="checkoxsustom">Leduc
                                <input type="checkbox" value="Leduc" name="most_frequently[]">
                                <span class="checkmark"></span>
                            </label>
                            <label class="checkoxsustom">Sherwood Park
                                <input type="checkbox" value="Sherwood_Park" name="most_frequently[]">
                                <span class="checkmark"></span>
                            </label>
                        </div>

                        <div class="rdbtnwidthset">
                            <label class="checkoxsustom">Slave Lake
                                <input type="checkbox" value="Slave_Lake" name="most_frequently[]">
                                <span class="checkmark"></span>
                            </label>
                            <label class="checkoxsustom">Sylvan Lake
                                <input type="checkbox" value="Sylvan_Lake" name="most_frequently[]">
                                <span class="checkmark"></span>
                            </label>
                        </div>

                        <div class="rdbtnwidthset">
                            <label class="checkoxsustom">Spruce Grove
                                <input type="checkbox" value="Spruce_Grove" name="most_frequently[]">
                                <span class="checkmark"></span>
                            </label>
                            <label class="checkoxsustom">Red deer
                                <input type="checkbox" value="Red_deer" name="most_frequently[]">
                                <span class="checkmark"></span>
                            </label>
                        </div>

                        <div class="rdbtnwidthset">
                            <label class="checkoxsustom">Rockey Mountain House
                                <input type="checkbox" value="Rockey_Mountain_House" name="most_frequently[]">
                                <span class="checkmark"></span>
                            </label>
                            <label class="checkoxsustom">White Court
                                <input type="checkbox" value="White_Court" name="most_frequently[]">
                                <span class="checkmark"></span>
                            </label>
                        </div>

                        <div class="rdbtnwidthset">
                            <label class="checkoxsustom">Wetaskiwin
                                <input type="checkbox" value="Wetaskiwin" name="most_frequently[]">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    </div>

                    <div class="rediobtnbox2">
                        <h4>Are you currently driving any ridesharing company?*</h4>
                        <ul>
                            <li>
                                <label class="radiocust">Yes
                                    <input type="radio" value="yes" name="ridesharing_company">
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                                <label class="radiocust">No
                                    <input type="radio" value="no" name="ridesharing_company">
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                        </ul>
                    </div>

                    <div class="rediobtnbox3" id="company_driving_div" style="display: none;">
                        <div class="form-group">
                            <label for="">What company you are driving with?*</label>
                            <input type="text" class="form-control" id="company_driving_name" name="company_driving_name" placeholder="Enter Name">
                        </div>
                    </div>

                    <div class="rediobtnbox2">
                        <h4>I am interested in driving*</h4>
                        <ul>
                            <li>
                                <label class="radiocust">Full Time
                                    <input type="radio" value="full_time" name="interested_driving">
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                                <label class="radiocust">Part Time
                                    <input type="radio" value="part_time" name="interested_driving">
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                                <label class="radiocust">Occasionally
                                    <input type="radio" value="occasionally" name="interested_driving">
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                        </ul>
                    </div>

                    <div class="rediobtnbox2">
                        <h4>Do you have a valid Canadian work permit?*</h4>
                        <ul>
                            <li>
                                <label class="radiocust">Yes
                                    <input type="radio" value="yes" name="work_permit">
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                                <label class="radiocust">No
                                    <input type="radio" value="no" name="work_permit">
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                        </ul>
                    </div>

                    <div class="rediobtnbox4">
                        <button type="submit">SUBMIT</button>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('.custom-select').select2({
            minimumResultsForSearch: -1
        });

        $('input[name="ridesharing_company"]').click(function(){
            var ridesharingCompany =  $("input[name='ridesharing_company']:checked").val();
            if(ridesharingCompany == 'yes'){
                $('#company_driving_div').show();
            } else {
                $('#company_driving_div').hide();
            }
        })

        //ADD BECOME DRIVER
            $('#becomeDriverForm').submit(function(e) {
                    e.preventDefault();
                }).validate({
                focusInvalid: false,
                ignore: "",
                rules: {
                    first_name: {
                        required: true
                    },
                    last_name: {
                        required: true
                    },
                    gender: {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    phone_number: {
                        required: true,
                        number: true,
                        maxlength: 10
                    },
                    currently_live: {
                        required: true,
                    },
                    going_to_drive: {
                        required: true
                    },
                    'most_frequently[]': {
                        required: true
                    },
                    ridesharing_company: {
                        required: true
                    },
                    company_driving_name: {
                        required : function (element) {
                            var ridesharing_company = $("input[name='ridesharing_company']:checked").val();
                            if (ridesharing_company == 'yes') {
                                return true;
                            } else {
                                return false;
                            }
                        },
                        //required: true
                    },
                    interested_driving: {
                        required: true
                    },
                    work_permit: {
                        required: true
                    },
                },

                errorPlacement: function(error, element) {
                    if (element.attr("name") == "going_to_drive" || element.attr("name") == "most_frequently[]") {
                        $(element).parents('.rediobtnbox1').append(error);
                    } else if(element.attr("name") == "ridesharing_company" || element.attr("name") == "interested_driving" || element.attr("name") == "work_permit") {
                        $(element).parents('ul').after(error);
                    } else if($(element).is("select")) {
                        $(element).parents('.selectboxcovedis').append(error);
                    } else {
                        error.insertAfter(element);
                    }
                },
                submitHandler: function (form) {
                    $.ajax({
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: '{{ route("website.addBecomeDriver") }}',
                        data:  $("#becomeDriverForm").serialize(),
                        success: function(response)
                        {
                            if (response.status == true) {
                                toastr.success(response.message);
                                $('input[type="text"] , input[type="email"]').val('');
                                $("input[type='radio'] , input[type='checkbox']").prop("checked", false);
                            } else {
                                toastr.warning(response.message);
                            }
                        }
                    });
                }
            });
    });
</script>
@endsection
