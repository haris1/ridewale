@section('title','RideChimp | Home')
@extends('website.layouts.app')
@section('styles')

@endsection
@section('content')

  <div class="container">
    <div class="howtorideboxincover">
      <h3>How to ride with RideChimp</h3>
      <div class="ridestepinercover">
        <div class="ridestepinerdit">
          <div class="ridestepinerdit_left">
            <img src="{{ asset('web_assets/svg/step1.svg') }}" alt="">
          </div>
          <div class="ridestepinerdit_right">
            <h6>STEP 1</h6>
            <p>Install our app for riders. Create your account <br>
              and enter your payment information.</p>
          </div>
        </div>
        <div class="ridestepinerdit">
          <div class="ridestepinerdit_left">
            <img src="{{ asset('web_assets/svg/step2.svg') }}" alt="">
          </div>
          <div class="ridestepinerdit_right">
            <h6>STEP 2</h6>
            <p>Enter the names of cities from and to <br>
            you would like to travel.</p>
          </div>
        </div>
        <div class="ridestepinerdit">
          <div class="ridestepinerdit_left">
            <img src="{{ asset('web_assets/svg/step3.svg') }}" alt="">
          </div>
          <div class="ridestepinerdit_right">
            <h6>STEP 3</h6>
            <p>Pick a ride from the list scheduled trips <br>
              & book a one you like. </p>
          </div>
        </div>
        <div class="ridestepinerdit">
          <div class="ridestepinerdit_left">
            <img src="{{ asset('web_assets/svg/step4.svg') }}" alt="">
          </div>
          <div class="ridestepinerdit_right">
            <h6>STEP 4</h6>
            <p>When a driver confirm your booking, your <br>
            credit card will be charged for the trip.</p>
          </div>
        </div>
        <div class="ridestepinerdit">
          <div class="ridestepinerdit_left">
            <img src="{{ asset('web_assets/svg/step5.svg') }}" alt="">
          </div>
          <div class="ridestepinerdit_right">
            <h6>STEP 5</h6>
            <p>When onboarding, provide a 4 digit <br>
              code to confirm & pick-up.</p>
          </div>
        </div>
        <div class="ridestepinerdit">
          <div class="ridestepinerdit_left">
            <img src="{{ asset('web_assets/svg/step6.svg') }}" alt="">
          </div>
          <div class="ridestepinerdit_right">
            <h6>STEP 6</h6>
            <p>The driver will drop you off at the <br>
              drop-off location.</p>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection

@section('scripts')

@endsection
