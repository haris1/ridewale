@section('title','RideChimp | Home')
@extends('website.layouts.app')
@section('styles')

@endsection
@section('content')
<div class="container">

    <div class="gettstadrived">
        <div class="drivestartedtitle">
            <h3>Getting started - Drive in Edmonton</h3>
            <h6>Visit us at our Edmonton office with documents mentioned below to become a driver.</h6>
            <p>For people's safety, we require all drivers to complete out verification <br>
            process. </p>
            <p>Documents mentioned below are required to be submitted to one of our <br>support offices locations: </p>
        </div>
        <div class="drivestarticontextset">
            <div class="driartintextset_left">
                <img src="{{ asset('web_assets/svg/icon1.svg') }}">
            </div>
            <div class="driartintextset_right">
                <h3>Minimum vehicle requirements</h3>
                <p>Your vehicle must meet the requirements to drive with Ricechimp in Edmonton. Check if your vehicle is qualified.</p>
                <a href="javascript:void(0)">VIEW VEHICLE REQUIREMENTS</a>
            </div>
        </div>
        <div class="drivestarticontextset">
            <div class="driartintextset_left">
                <img src="{{ asset('web_assets/svg/icon2.svg') }}">
            </div>
            <div class="driartintextset_right">
                <h3>Class 4 licence</h3>
                <p>All partners on the Uber platform in Edmonton are required to carry a valid Class 1, 2, or 4 licenses. Already have one? Read on.</p>
                <a href="javascript:void(0)">LEARN MORE ABOUT CLASS 4 LICENCING </a>
            </div>
        </div>
        <div class="drivestarticontextset">
            <div class="driartintextset_left">
                <img src="{{ asset('web_assets/svg/icon3.svg') }}">
            </div>
            <div class="driartintextset_right">
                <h3>Complete background screening</h3>
                <p>Your vehicle must meet the requirements to drive with Uber in Edmonton. Check if your vehicle is qualified.</p>
                <a href="javascript:void(0)">BACKGROUND SCREENING REQUIREMENTS</a>
            </div>
        </div>
        <div class="drivestarticontextset">
            <div class="driartintextset_left">
                <img src="{{ asset('web_assets/svg/icon4.svg') }}">
            </div>
            <div class="driartintextset_right">
                <h3>Vehicle inspection</h3>
                <p>Your vehicle must meet the requirements to drive with Uber in Edmonton. Check if your vehicle is qualified.</p>
                <a href="javascript:void(0)">LEARN MORE ABOUT VEHICLE INSPECTION</a>
            </div>
        </div>
        <div class="drivestarticontextset">
            <div class="driartintextset_left">
                <img src="{{ asset('web_assets/svg/icon5.svg') }}">
            </div>
            <div class="driartintextset_right">
                <h3>3- year driver abstract </h3>
                <p>Your vehicle must meet the requirements to drive with Uber in Edmonton. Check if your vehicle is qualified.</p>
                <a href="javascript:void(0)">DRIVER ABSTRACT REQUIREMENTS</a>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection
