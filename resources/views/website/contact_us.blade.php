@section('title','RideChimp | Home')
@extends('website.layouts.app')
@section('styles')

@endsection
@section('content')
  <div class="contactmaincovercl">
    <div class="container">
      <div class="allcontactdetailcover">
        <div class="pagetoptitle">
          <h3>How can we help?</h3>
          <p>Please make your request as detailed as possible, and we'll do our best to get back to you as
soon as we can.</p>
        </div>
        <div class="retrhesaih_btnbox">
          <ul>
            <li><a href="javascript:void(0)" class="active">REPORT A TECHNICAL PROBLEM</a></li>
            <li><a href="javascript:void(0)">TROUBLE WITH ACCOUNT LOGIN</a></li>
            <li><a href="javascript:void(0)">HELP WITH PAYMENTS</a></li>
            <li><a href="javascript:void(0)">SAFETY OR PRIVACY CONCERN</a></li>
            <li><a href="javascript:void(0)">I HAVE FEEDBACK</a></li>
            <li><a href="javascript:void(0)">I DON'T KNOW HOW IT WORKS</a></li>
          </ul>
        </div>

        <div class="formcontactdit">
          <div class="fullwidthalset">
            <div class="forminputlabel">
              <div class="selectboxcovedis">
                <label for="">What feature this is about? <span>*</span></label>
                <div class="custselectbox">
                  <select class="custom-select" name="gender">
                      <option value="">Select Feature</option>
                      <option value="male">Select Feature</option>
                      <option value="female">Select Feature</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="fullwidthalset">
            <div class="forminputlabel">
              <div class="selectboxcovedis">
                <label for="">What platform are you using? <span>*</span></label>
                <div class="custselectbox">
                  <select class="custom-select" name="gender">
                      <option value="">Select A Platform</option>
                      <option value="male">Select A Platform</option>
                      <option value="female">Select A Platform</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>


        <div class="formcontactdit">
          <div class="fullwidthalset">
            <div class="forminputlabel">
              <div class="form-group">
                <label for="">Email Address <span>*</span></label>
                <input type="text" class="form-control" id="" placeholder="Enter Email Address">
              </div>
            </div>
          </div>
          <div class="fullwidthalset">
            <div class="formtextarealabel">
              <div class="form-group">
                <label for="">Description <span>*</span></label>
                <textarea name="name"></textarea>
              </div>
            </div>
          </div>
        </div>
        <div class="addfilebox">
          <p>Attachments</p>
          <div class="file">
            <input type="file" name="file" id="file" />
            <label for="file">ADD FILES</label>
          </div>
        </div>
        <div class="subbtn">
          <button type="button" name="button">SUBMIT REQUEST</button>
        </div>


      </div>
    </div>
  </div>
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
  $(document).ready(function() {
    $('.custom-select').select2({
        minimumResultsForSearch: -1
    });
  });
</script>
@endsection
