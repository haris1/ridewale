<div class="allcontcoveral">


<header class="newhed_designcl">

  <div class="mainhedpartcover">
    <div class="containerinhedpart">
      <!-- <nav class="navbar navbar-default">
        <div class="navbar-header page-scroll">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <img src="{{ asset('web_assets/svg/hamburger.svg') }}" alt="" class="hamburger">
            <img src="{{ asset('web_assets/svg/hamburger-close.svg') }}" alt="" class="hamburger_close">
          </button>
          <a class="navbar-brand" href="{{ route('website.home') }}"><img src="{{ asset('web_assets/svg/logo.svg') }}" alt=""></a>
          <div class="plappstorbtn">
            <a href="{{ route('website.become_driver') }}"><span>BECOME A DRIVER</span></a>
          </div>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav navbar-left">
            <li class="hidden">
              <a href="#page-top"></a>
            </li>
            <li class="dropdown @if(Route::currentRouteName() == 'website.become_driver') {{ 'activemen' }} @endif">
              <a href="{{ route('website.become_driver') }}" class="dropdown-toggle dropbtn mainmenu" >BECOME A DRIVER</a>
              <ul class="dropdown-menu dropdown-content">
                <li><a href="{{ route('website.driver_requirements') }}">Driver Requirements</a></li>
              </ul>
            </li>
            <li class="page-scroll rsponsve_showwnu @if(Route::currentRouteName() == 'website.driver_requirements') {{ 'activemen' }} @endif">
              <a href="{{ route('website.driver_requirements') }}">Driver Requirements</a>
            </li>
            <li class="page-scroll">
              <a href="javascript:void(0)" class="mainmenu">CITIES</a>
            </li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li class="page-scroll dowloadapp">
              <h3>DOWNLOAD APP</h3>
              <a href="javascript:void(0)"><img src="{{ asset('web_assets/svg/android-icon.svg') }}" alt=""></a>
              <a href="javascript:void(0)"><img src="{{ asset('web_assets/svg/ios-icon.svg') }}" alt=""></a>
            </li>
          </ul>
        </div>
      </nav> -->

      <nav role="navigation" class="containerinhedpart">
        <div class="leftlogodibo">
          <a class="" href="{{ route('website.home') }}"><img src="{{ asset('web_assets/svg/logo.svg') }}" alt=""></a>
        </div>
        <div class="hemiconset menu-toggle" id="bodyhide">
          <img src="{{ asset('web_assets/svg/hamburger.svg') }}" alt="" class="hamburger">
          <img src="{{ asset('web_assets/svg/hamburger-close.svg') }}" alt="" class="hamburger_close">
        </div>
        <div class="menu menualldetailiner">
          <div class="leftrightboxcovercl">
            <div class="leftmenudit">
              <div class="inerhem_menu1">
                <ul>
                  <!-- <li><a href="javascript:void(0)">How To Ride</a></li> -->
                  <li><a href="{{ route('website.how_to_ride') }}">How To Ride</a></li>
                  <!-- <li><a href="{{ route('website.become_driver') }}">Become A Driver</a></li> -->
                  <li><a href="{{ route('website.become_driver') }}">Become A Driver</a></li>
                  <li><a href="{{ route('website.contact_us') }}">Contact Us</a></li>
                </ul>
                <ul>
                  <li><a href="{{ route('website.cities') }}">Cities</a></li>
                  <li><a href="{{ route('website.fares') }}">Fares</a></li>
                  <!-- <li><a href="javascript:void(0)">FAQs</a></li> -->
                  <li><a href="{{ route('website.faq') }}">FAQs</a></li>
                </ul>
              </div>
              <div class="inerhem_menu2">
                <ul>
                  <li><a href="{{ route('website.privacy_policy') }}">Privacy Policy</a></li>
                  <li><a href="javascript:void(0)">Terms Of Use</a></li>
                </ul>
              </div>
              <div class="inerhem_menu1">
                <ul>
                  <li><a href="javascript:void(0)">Blog</a></li>
                </ul>
              </div>
              <div class="Installourapp">
                <p>Install our app. Available on</p>
                <ul>
                  <li><a href="javascript:void(0)"><img src="{{ asset('web_assets/svg/android-icon2.svg') }}" alt=""><span>PLAY STORE</span></a></li>
                  <li><a href="javascript:void(0)"><img src="{{ asset('web_assets/svg/ios-icon2.svg') }}" alt=""><span>APP STORE</span></a></li>
                </ul>
              </div>
            </div>
            <div class="rightmeniimg">
                <img src="{{ asset('web_assets/images/ride-menuimg.png') }}" alt="">
            </div>
            <div class="respoimgboxsetr">
                <img src="{{ asset('web_assets/images/bg-img4.png') }}" alt="">
            </div>
          </div>
        </div>
      </nav>



    </div>
  </div>
</header>
<style>

.rideimageboxpart{
  display: none;
}

  </style>
<div class="alldit_paretboxpaet">
