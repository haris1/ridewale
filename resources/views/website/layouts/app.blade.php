<!DOCTYPE html>
<html lang="en">
<head>

    <title>@yield('title','RideForU | Index')</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=""/>
    <meta name="application-name" content="&nbsp;"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="msapplication-TileColor" content="#FFFFFF" />
    <meta name="msapplication-TileImage" content="favicon/mstile-144x144.png" />
    <meta name="msapplication-square70x70logo" content="favicon/mstile-70x70.png" />
    <meta name="msapplication-square150x150logo" content="favicon/mstile-150x150.png" />
    <meta name="msapplication-wide310x150logo" content="favicon/mstile-310x150.png" />
    <meta name="msapplication-square310x310logo" content="favicon/mstile-310x310.png" />

    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="{{ asset('web_assets/favicon/apple-touch-icon-57x57.png') }}" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('web_assets/favicon/apple-touch-icon-114x114.png') }}" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('web_assets/favicon/apple-touch-icon-72x72.png') }}" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('web_assets/favicon/apple-touch-icon-144x144.png') }}" />
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="{{ asset('web_assets/favicon/apple-touch-icon-60x60.png') }}" />
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="{{ asset('web_assets/favicon/apple-touch-icon-120x120.png') }}" />
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="{{ asset('web_assets/favicon/apple-touch-icon-76x76.png') }}" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="{{ asset('web_assets/favicon/apple-touch-icon-152x152.png') }}" />
    <link rel="icon" type="image/png" href="{{ asset('web_assets/favicon/favicon-196x196.png') }}" sizes="196x196" />
    <link rel="icon" type="image/png" href="{{ asset('web_assets/favicon/favicon-96x96.png') }}" sizes="96x96" />
    <link rel="icon" type="image/png" href="{{ asset('web_assets/favicon/favicon-32x32.png') }}" sizes="32x32" />
    <link rel="icon" type="image/png" href="{{ asset('web_assets/favicon/favicon-16x16.png') }}" sizes="16x16" />
    <link rel="icon" type="image/png" href="{{ asset('web_assets/favicon/favicon-128.png') }}" sizes="128x128" />
    <link rel="stylesheet" href="{{ asset('web_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('web_assets/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('web_assets/css/styles.css') }}">
    <link rel="stylesheet" href="{{ asset('web_assets/css/toastr.min.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <meta property="og:image" content="{{ asset('web_assets/favicon/favicon-196x196.png') }}"/>
    @yield('styles')
    <!-- Start of  Zendesk Widget script -->
    <script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=edcf9f85-26bc-4d7b-bbfb-9f2649aa843f"> </script>
    <!-- End of  Zendesk Widget script -->
</head>
<body>


<body>
        @include('website.layouts.header')
            @yield('content')
        @include('website.layouts.footer')

</body>

<script type="text/javascript" src="{{ asset('web_assets/js/jquery.js') }}"></script>
<script src="{{ asset('web_assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('web_assets/js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('web_assets/js/toastr.min.js') }}"></script>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-138220992-1"></script>

<script>

    var menu = document.querySelector(".menu"),
    toggle = document.querySelector(".menu-toggle");

    function toggleToggle() {
      toggle.classList.toggle("menu-open");
    };

    function toggleMenu() {
      menu.classList.toggle("active");
    };

    toggle.addEventListener("click", toggleToggle, false);
    toggle.addEventListener("click", toggleMenu, false);

</script>

<script>
    $('#bodyhide').click(function() {
      $('.alldit_paretboxpaet').toggleClass('scroll_hide');
    });
</script>

@yield('scripts')

</html>
