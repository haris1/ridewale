</div>
<footer class="footerstc">

	<div class="container">

		<div class="menuleftprt">

			<ul>

				<li><a href="{{ route('website.privacy_policy') }}">PRIVACY POLICY</a></li>

				<li><a href="javascript:void(0)">CONTACT</a></li>

				<li><a href="javascript:void(0)">TERMS OF USE</a></li>

				<li><a href="{{ route('website.faq') }}">FAQ</a></li>

			</ul>

		</div>

		<div class="instyoutrightprt">

			<a href="javascript:void(0)"><img src="{{ asset('web_assets/svg/instagram-icono.svg') }}" alt=""></a>

			<a href="javascript:void(0)"><img src="{{ asset('web_assets/svg/youtube-icon.svg') }}" alt=""></a>

		</div>

		<div class="cpyrigmidprt">

			<p>Copyright © {{ date('Y') }} RideChimp Inc.</p>

		</div>

	</div>

</footer>
</div>
