@extends('admin.layouts.admin-app')

@section('content')

@section('css_ref')
    <link href="{{ asset('admin_assets/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection
 <!-- Main Content -->
 <style type="text/css">
	td > a{
    	color: #482080;
    }
</style>
<div class="page-wrapper">
    
    <div class="container-fluid">
				<!-- Title -->
				<div class="row heading-bg">
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
					  <h5 class="txt-dark">Trip History</h5>
					</div>
					<!-- Breadcrumb -->
					<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
					  <ol class="breadcrumb">
						<li><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
						<li class="active"><span>Trip History</span></li>
					  </ol>
					</div>
					<!-- /Breadcrumb -->
				</div>
				<!-- /Title -->
				
				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default card-view settableboxadd">
							<div class="panel-heading">
								<div class="pull-left">
									<h6 class="panel-title txt-dark">Trip List</h6>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div class="table-wrap">
										<div class="table-responsive users_detateblebox">
											<table id="paymet_history" class="table table-striped table-bordered" style="width:100%">
												<thead>
													<tr>
														<th>Sr.</th>
														<th>Created Date</th>
														<th>Trip Date</th>
														<th>Trip No.</th>
														<th>Trip By</th>
														<th>Price / seat</th>
														<th>Total Seats</th>
														<th>Booked Seats</th>
														<th>Trip Status</th>
														<th>Pending Confirmations</th>
													</tr>
												</thead>
												<tbody>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>	
					</div>
				</div>
				<!-- /Row -->
			</div>
    @include('admin.layouts.footer')
</div>
	
@section('scripts')
<script>

var table = $('#paymet_history').DataTable({
				dom: 'Bfrtip',
				"order"      : [[ 0, 'desc' ]],
				"serverSide": true,
				"sServerMethod": "GET",	
				"lengthMenu": [[10, 50, 100, -1], [10, 50, 100, "All"]],
				"ajax": {
				url: "{{route('admin.trip.history')}}",
				data: function ( d ) {
					d._token = "{{ csrf_token() }}";
					d.started_at=$('#started_at').val();
					d.completed_at=$('#completed_at').val();
					d.status=$('#status').val();
				},
				complete:function(){
					if( $('[data-toggle="tooltip"]').length > 0 )
						$('[data-toggle="tooltip"]').tooltip();
					}
				},

				"columns":[
					{ data:'DT_RowIndex',name:'id'},
					{ data:'created_date',name:'created_date',"width": "15%" },
					{ data:'trip_date',name:'trip_date',"width": "10%" },
					{ data:'trip_no',name:'trip_no' },
					{ data:'trip_by',name:'trip_by' },
					{ data:'seat_amount',name:'seat_amount' },
					{ data:'total_seats',name:'total_seats' },
					{ data:'booked_seats',name:'booked_seats' },
					{ data:'trip_status',name:'trip_status' },
					{ data:'pending_confirmation',name:'pending_confirmation' },
				// { data:'action',name:'action', orderable:false, searchable:false },
				]
			});
	$('#filter').click(function(){
		table.draw(); 
	});

	$(function() {
		var start = moment().subtract(30, 'days');
		var end = moment();
		function cb(start, end) {
			$('input[name=started_at],input[name=completed_at]').val(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
		}
		$('input[name=started_at],input[name=completed_at]').daterangepicker({
			startDate: start,
			endDate: end,
			autoUpdateInput:true,
			autoApply: true,
			locale: {
				format: 'MMM D, YYYY',
				cancelLabel: 'Clear'
			},
			ranges: {
				'Today': [moment(), moment()],
				'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Last 7 Days': [moment().subtract(6, 'days'), moment()],
				'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				'This Month': [moment().startOf('month'), moment().endOf('month')],
				'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			}
		},cb);
		cb(start, end);
	});
</script>

@endsection
@endsection
