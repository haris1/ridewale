@extends('admin.layouts.admin-app')

@section('content')

@section('css_ref')
<link href="{{ asset('admin_assets/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
<style type="text/css">
.pac-container {
	background-color: #FFF;
	z-index: 1051;
	position: fixed;
	display: inline-block;
	float: left;
}
.modal{
	z-index: 20;   
}
.modal-backdrop{
	z-index: 10;        
	}​
</style>

@endsection
<!-- Main Content -->
<div class="page-wrapper">

	<div class="container-fluid">

		<!-- Title -->
		<div class="row heading-bg">
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
				<!-- <h5 class="txt-dark">Allowed Not-allowed things management</h5> -->
			</div>
			<!-- Breadcrumb -->
			<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
				<ol class="breadcrumb">
					<li><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
					<li class="active"><span>Allowed Not-allowed things management</span></li>
				</ol>
			</div>
			<!-- /Breadcrumb -->
		</div>
		<!-- /Title -->

		<!-- Row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default card-view settableboxadd">
					<div class="panel-heading">
						<div class="pull-left">
							<h6 class="panel-title txt-dark">Allowed Things List</h6>
						</div>
						<div class="pull-right">
							<a href="javascript:void(0);" class="btn btn-success" data-toggle="modal" data-target="#allowedAdd_modal" data-title="Add"><span class="btn-text">Add</span></a>								
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="panel-wrapper collapse in">
						<div class="panel-body">
							<div class="table-wrap">
								<div class="table-responsive users_detateblebox">
									<table id="allowedTable" class="table table-striped table-bordered" style="width:100%">
										<thead>
											<tr>
												<th>Sr. No</th>
												<th>Description</th>
												<th>Action</th>
											</tr>
										</thead>
										<tfoot>
											<tr>
												<th>Sr. No</th>
												<th>Description</th>
												<th>Action</th>
											</tr>
										</tfoot>
										<tbody>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</div>
		<!-- /Row -->

		<!-- Row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default card-view settableboxadd">
					<div class="panel-heading">
						<div class="pull-left">
							<h6 class="panel-title txt-dark">Not-allowed Things List</h6>
						</div>
						<div class="pull-right">
							<a href="javascript:void(0);" class="btn btn-success" data-toggle="modal" data-target="#notallowedAdd_modal" data-title="Add"><span class="btn-text">Add</span></a>								
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="panel-wrapper collapse in">
						<div class="panel-body">
							<div class="table-wrap">
								<div class="table-responsive users_detateblebox">
									<table id="notallowedTable" class="table table-striped table-bordered" style="width:100%">
										<thead>
											<tr>
												<th>Sr. No</th>
												<th>Description</th>
												<th>Action</th>
											</tr>
										</thead>
										<tfoot>
											<tr>
												<th>Sr. No</th>
												<th>Description</th>
												<th>Action</th>
											</tr>
										</tfoot>
										<tbody>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</div>
		<!-- /Row -->

		<!-- code for work city maintain -->


	</div>

	@include('admin.layouts.footer')
</div>
<div class="modal fade in" id="allowedAdd_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Add Allowed Things</h5>
			</div>
			<form id="addAllowedThingsFrm" data-toggle="validator">
				<div class="modal-body">
					<div class="form-group">
						<label for="recipient-name" class="control-label mb-10 title">Description:</label>
						<textarea name="description" class="form-control" required=""></textarea>
						<div class="help-block with-errors"></div>
					</div>
				</div>
				<input type="hidden" name="allowed" value="1">
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Add</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- END EDIT STATE MODEL -->
<div class="modal fade in" id="allwedthingsEdit_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h5 class="modal-title" id="allowedThingsEdit">Edit Allowed Things</h5>
			</div>
			<form id="editAllowedThingsFrm" data-toggle="validator">
				<div class="modal-body">
					<div class="form-group">
						<input type="hidden" name="id" id="allowed_id">
						<label for="recipient-name" class="control-label mb-10 title">Description:</label>
						<textarea name="description" class="form-control" id="description" required=""></textarea>

						<div class="help-block with-errors"></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Update</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade in" id="notallowedAdd_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Add Not-allowed Things</h5>
			</div>
			<form id="addnotAllowedThingsFrm" data-toggle="validator">
				<div class="modal-body">
					<div class="form-group">
						<label for="recipient-name" class="control-label mb-10 title">Description:</label>
						<textarea name="description" class="form-control" required=""></textarea>
						<div class="help-block with-errors"></div>
					</div>
				</div>
				<input type="hidden" name="allowed" value="0">
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Add</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- /Main Content -->

@section('scripts')
<script>
	/** allowed things datatable **/
	var table = $('#allowedTable').DataTable({
		pageLength:10,
		serverSide: true,
		lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
		ajax: {
			type: "post",
			url: "{{ route('admin.allowed_things.get_allowed_things') }}",
			data: function ( d ) {
				d._token = "{{ csrf_token() }}";
			},
			complete:function(){
				if( $('[data-toggle="tooltip"]').length > 0 )
					$('[data-toggle="tooltip"]').tooltip();
			}
		},
		columns:[
		{ data:'DT_RowIndex',name:'id' },
		{ data:'description',name:'description' },
		{ data:'action',name:'action', orderable:false, searchable:false },
		]
	});

	/** add allowed things **/
	$( "#addAllowedThingsFrm" ).submit(function( event ) {
		event.preventDefault();
		var data=$( "#addAllowedThingsFrm" ).serialize();
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:'POST',
			url:'{{route('admin.allowed_things.store')}}',
			data:data,
			dataType: "json",
			success:function(data) {
				switch (data.status) {
					case true:
					toastr.success(data.message);
					table.ajax.url( "{{ route('admin.allowed_things.get_allowed_things') }}" ).load();
    				$('#allowedAdd_modal').modal('hide');
					break;
					case false:
					toastr.warning(data.message);
					break;
					default:
					toastr.error("You are not Authorized to access this page");
					break;
				}
			},
		});
	});

	/** edit allowed things model **/
	$('#allwedthingsEdit_modal').on('shown.bs.modal', function(e) {
		$("#allowed_id").val($(e.relatedTarget).data('id'));
		$("#description").val($(e.relatedTarget).data('description'));
	});

	/** edit form submit **/
	$( "#editAllowedThingsFrm" ).submit(function( event ) {
		event.preventDefault();
		var data=$( "#editAllowedThingsFrm" ).serialize();
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:'PUT',
			url:'{{route('admin.allowed_things.update','')}}/'+$("#allowed_id").val(),
			data:data,
			dataType: "json",
			success:function(data) {
				switch (data.status) {
					case true:
					toastr.success(data.message);
					table.ajax.url( "{{ route('admin.allowed_things.get_allowed_things') }}" ).load();
					table2.ajax.url( "{{ route('admin.allowed_things.get_notallowed_things') }}" ).load();
    				$('#allwedthingsEdit_modal').modal('hide');
					break;
					case false:
					toastr.warning(data.message);
					break;
					default:
					toastr.error("You are not Authorized to access this page");
					break;
				}
			},
		});
	});

	/** not allowed things datatable **/
	var table2 = $('#notallowedTable').DataTable({
		pageLength:10,
		serverSide: true,
		lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
		ajax: {
			type: "post",
			url: "{{ route('admin.allowed_things.get_notallowed_things') }}",
			data: function ( d ) {
				d._token = "{{ csrf_token() }}";
			},
			complete:function(){
				if( $('[data-toggle="tooltip"]').length > 0 )
					$('[data-toggle="tooltip"]').tooltip();
			}
		},
		columns:[
		{ data:'DT_RowIndex',name:'id' },
		{ data:'description',name:'description' },
		{ data:'action',name:'action', orderable:false, searchable:false },
		]
	});

	/** add not allowed things **/
	$( "#addnotAllowedThingsFrm" ).submit(function( event ) {
		event.preventDefault();
		var data=$( "#addnotAllowedThingsFrm" ).serialize();
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:'POST',
			url:'{{route('admin.allowed_things.store')}}',
			data:data,
			dataType: "json",
			success:function(data) {
				switch (data.status) {
					case true:
					toastr.success(data.message);
					table2.ajax.url( "{{ route('admin.allowed_things.get_notallowed_things') }}" ).load();
    				$('#notallowedAdd_modal').modal('hide');
					break;
					case false:
					toastr.warning(data.message);
					break;
					default:
					toastr.error("You are not Authorized to access this page");
					break;
				}
			},
		});
	});





	function deleteAllowedthing(id){ 
		var token = "{{ csrf_token() }}";

		swal({
			title: "Are you sure?",   
			type: "warning",   
			showCancelButton: true,   
			confirmButtonColor: "#e69a2a",   
			confirmButtonText: "Yes, delete it!",   
			cancelButtonText: "No, cancel please!",   
			closeOnConfirm: false,   
			closeOnCancel: false 
		},
		function(isConfirm){   
			if (isConfirm) {
				$.ajax({
					headers: {
						'X-CSRF-TOKEN':  "{{ csrf_token() }}"
					},
					method:'DELETE',
					url:'{{route('admin.allowed_things.destroy','')}}/'+id,
					dataType: "json",
					success:function(data) {

						switch (data.status) {
							case true:
							swal("Deleted!", "Your information has been deleted.", "success");  
							toastr.success(data.message);
							location.reload();
							break;
							case false:
							toastr.error(data.message);
							break;
							default:
							toastr.error("You are not Authorized to access this page");
							break;
						}
					},
				});
			} else {
				swal("Cancelled", "Your information is safe! :)", "error"); 
			}
		});
	}		

</script>
@endsection
@endsection