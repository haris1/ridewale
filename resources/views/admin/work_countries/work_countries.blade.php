@extends('admin.layouts.admin-app')

@section('content')

@section('css_ref')
<link href="{{ asset('admin_assets/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
<style type="text/css">
.pac-container {
	background-color: #FFF;
	z-index: 1051;
	position: fixed;
	display: inline-block;
	float: left;
}
.modal{
	z-index: 20;   
}
.modal-backdrop{
	z-index: 10;        
	}​
</style>

@endsection
<!-- Main Content -->
<div class="page-wrapper">

	<div class="container-fluid">

		<!-- Title -->
		<div class="row heading-bg">
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
				<h5 class="txt-dark">Location management</h5>
			</div>
			<!-- Breadcrumb -->
			<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
				<ol class="breadcrumb">
					<li><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
					<li class="active"><span>Location management</span></li>
				</ol>
			</div>
			<!-- /Breadcrumb -->
		</div>
		<!-- /Title -->

		<!-- Row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default card-view settableboxadd">
					<div class="panel-heading">
						<div class="pull-left">
							<h6 class="panel-title txt-dark">Working Countries List</h6>
						</div>
						<div class="pull-right">
							<a href="javascript:void(0);" class="btn btn-success" data-toggle="modal" data-target="#countryAdd_modal" data-title="Add"><span class="btn-text">Add</span></a>								
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="panel-wrapper collapse in">
						<div class="panel-body">
							<div class="table-wrap">
								<div class="table-responsive users_detateblebox">
									<table id="snowAngelTable" class="table table-striped table-bordered" style="width:100%">
										<thead>
											<tr>
												<th>Sr. No</th>
												<th>Country</th>
												<th>Action</th>
											</tr>
										</thead>
										<tfoot>
											<tr>
												<th>Sr. No</th>
												<th>Country</th>
												<th>Action</th>
											</tr>
										</tfoot>
										<tbody>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</div>
		<!-- /Row -->

		<!-- Row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default card-view settableboxadd">
					<div class="panel-heading">
						<div class="pull-left">
							<h6 class="panel-title txt-dark">Working States List</h6>
						</div>
						<div class="pull-right">
							<a href="javascript:void(0);" class="btn btn-success" data-toggle="modal" data-target="#stateAdd_modal" data-title="Add"><span class="btn-text">Add</span></a>								
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="panel-wrapper collapse in">
						<div class="panel-body">
							<div class="table-wrap">
								<div class="table-responsive users_detateblebox">
									<table id="stateDataTable" class="table table-striped table-bordered" style="width:100%">
										<thead>
											<tr>
												<th>Sr. No</th>
												<th>State</th>
												<th>Tax Percentage</th>
												<th>Country</th>
												<th>Action</th>
											</tr>
										</thead>
										<tfoot>
											<tr>
												<th>Sr. No</th>
												<th>State</th>
												<th>Tax Percentage</th>
												<th>Country</th>
												<th>Action</th>
											</tr>
										</tfoot>
										<tbody>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</div>
		<!-- /Row -->

		<!-- code for work city maintain -->

		<!-- Row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default card-view settableboxadd">
					<div class="panel-heading">
						<div class="pull-left">
							<h6 class="panel-title txt-dark">Working Cities List</h6>
						</div>
						<div class="pull-right">
							<a href="javascript:void(0);" class="btn btn-success" data-toggle="modal" data-target="#cityAdd_modal" data-title="Add"><span class="btn-text">Add</span></a>								
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="panel-wrapper collapse in">
						<div class="panel-body">
							<div class="table-wrap">
								<div class="table-responsive users_detateblebox">
									<table id="workCity" class="table table-striped table-bordered" style="width:100%">
										<thead>
											<tr>
												<th>Sr. No</th>
												<th>City</th>
												<th>State</th>
												<th>Country</th>
												<th>Timezone</th>
												<th>Action</th>
											</tr>
										</thead>
										<tfoot>
											<tr>
												<th>Sr. No</th>
												<th>City</th>
												<th>State</th>
												<th>Country</th>
												<th>Timezone</th>
												<th>Action</th>
											</tr>
										</tfoot>
										<tbody>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</div>
		<!-- /Row -->

		<!-- Row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default card-view settableboxadd">
					<div class="panel-heading">
						<div class="pull-left">
							<h6 class="panel-title txt-dark">Working Locations List</h6>
						</div>
						<div class="pull-right">
							<button class="btn btn-success" data-toggle="modal" data-target="#locationAdd_modal" data-title="Add"><span class="btn-text">Add</span></button>								
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="panel-wrapper collapse in">
						<div class="panel-body">
							<div class="table-wrap">
								<div class="table-responsive users_detateblebox">
									<table id="workLocations" class="table table-striped table-bordered" style="width:100%">
										<thead>
											<tr>
												<th>Sr. No</th>
												<th>Location</th>
												<th>Country</th>
												<th>City</th>
												<th>Action</th>
											</tr>
										</thead>
										<tfoot>
											<tr>
												<th>Sr. No</th>
												<th>Location</th>
												<th>Country</th>
												<th>City</th>
												<th>Action</th>
											</tr>
										</tfoot>
										<tbody>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</div>
		<!-- /Row -->

	</div>

	@include('admin.layouts.footer')
</div>
<div class="modal fade in" id="countryAdd_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Add Country</h5>
			</div>
			<form id="addCountryFrm" data-toggle="validator">
				<div class="modal-body">
					<div class="form-group">
						<label for="recipient-name" class="control-label mb-10 title">Country:</label>
						<input type="text" name="name" class="form-control" required="">
						<div class="help-block with-errors"></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Add</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- ADD STATE -->
<div class="modal fade in" id="stateAdd_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Add State</h5>
			</div>
			<form id="addStateFrm" data-toggle="validator">
				<div class="modal-body">
					<div class="form-group">
						<label for="recipient-name" class="control-label mb-10 title">State:</label>
						<input type="text" name="name" class="form-control" required="">
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
						<label for="recipient-name" class="control-label mb-10 title">Tax Percentage:</label>
						<input type="text" name="tax_percentage" class="form-control" required="">
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
						<label for="recipient-name" class="control-label mb-10 title">Country:</label>
						<select name="work_country_id" required=""  class="form-control">
							<option value=""></option>
							@foreach($countries as $country)
							<option value="{{$country->id}}">{{$country->name}}</option>
							@endforeach
						</select>
						<div class="help-block with-errors"></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Add</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- END STATE -->

<!-- EDIT STATE MODEL -->
<div class="modal fade in" id="stateEdit_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Edit State</h5>
			</div>
			<form id="editStateFrm" data-toggle="validator">
				<div class="modal-body">
					<div class="form-group">
						<label for="recipient-name" class="control-label mb-10 title">State:</label>
						<input type="text" name="name" class="form-control" required="" id="state">
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
						<label for="recipient-name" class="control-label mb-10 title">Tax Percentage:</label>
						<input type="text" name="tax_percentage" class="form-control" required="" id='tax_percentage'>
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
						<label for="recipient-name" class="control-label mb-10 title">Country:</label>
						<select name="work_country_id" id="work1_country_id" required=""  class="form-control">
							<option value=""></option>
							@foreach($countries as $country)
							<option value="{{$country->id}}">{{$country->name}}</option>
							@endforeach
						</select>
						<div class="help-block with-errors"></div>
					</div>
					<input type="hidden" name="state_id" id="state_id" value="">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Update</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- END EDIT STATE MODEL -->
<div class="modal fade in" id="countryEdit_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Edit Country</h5>
			</div>
			<form id="editCountryFrm" data-toggle="validator">
				<div class="modal-body">
					<div class="form-group">
						<input type="hidden" name="id" id="country_id">
						<label for="recipient-name" class="control-label mb-10 title">Name:</label>
						<input type="text" name="name" class="form-control" required="" id="country">
						<div class="help-block with-errors"></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Update</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- /Main Content -->

<!-- code for maintain work city -->
<div class="modal fade in" id="cityAdd_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Add City</h5>
			</div>
			<form id="addCityFrm" data-toggle="validator">
				<div class="modal-body">
					<div class="form-group">
						<label for="recipient-name" class="control-label mb-10 title">City:</label>
						<input type="text" name="name" class="form-control" required="">
						<div class="help-block with-errors"></div>
					</div>

					<div class="form-group">
						<label for="recipient-name" class="control-label mb-10 title">Country:</label>
						<select name="work_country_id" required="" id="country_id_city"  class="form-control">
							<option value=""></option>
							@foreach($countries as $country)
							<option value="{{$country->id}}">{{$country->name}}</option>
							@endforeach
						</select>
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
						<label for="recipient-name" class="control-label mb-10 title">State:</label>
						<select name="work_state_id" required=""  class="form-control work_state_id">
							
						</select>
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
						<label for="recipient-name" class="control-label mb-10 title">Timezone Name:</label>
						<input type="text" name="timezone_name" class="form-control" required="">
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
						<label for="recipient-name" class="control-label mb-10 title">Timezone:</label>
						<input type="text" name="timezone" class="form-control" required="">
						<div class="help-block with-errors"></div>
					</div>

					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Add</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade in" id="cityEdit_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Edit City</h5>
			</div>
			<form id="editCityFrm" data-toggle="validator">
				<div class="modal-body">
					<div class="form-group">
						<label for="recipient-name" class="control-label mb-10 title">City:</label>
						<input type="text" name="name" class="form-control" required="" id="city">
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
						<label for="recipient-name" class="control-label mb-10 title">Country:</label>
						<select name="work_country_id" id="work_country_id" required=""  class="form-control">
							<option value=""></option>
							@foreach($countries as $country)
							<option value="{{$country->id}}">{{$country->name}}</option>
							@endforeach
						</select>
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
						<label for="recipient-name" class="control-label mb-10 title">State:</label>
						<select name="work_state_id" required=""  class="form-control work_state_id">
							<option value=""></option>
							@foreach($states as $state)
							<option value="{{$state->id}}">{{$state->name}}</option>
							@endforeach
						</select>
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
						<label for="recipient-name" class="control-label mb-10 title">Timezone Name:</label>
						<input type="text" name="timezone_name" id="timezone_name" class="form-control" required="">
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
						<label for="recipient-name" class="control-label mb-10 title">Timezone:</label>
						<input type="text" name="timezone" id="timezone" class="form-control" required="">
						<div class="help-block with-errors"></div>
					</div>
					<input type="hidden" name="city_id" id="city_id" value="">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Update</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- code for maintain work location  -->

<div class="modal fade in" id="locationAdd_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Add Location</h5>
			</div>
			<form id="addLocationFrm" data-toggle="validator">
				<div class="modal-body">
					<div class="form-group">
						<label for="recipient-name" class="control-label mb-10 title">Location:</label>
						<input type="text" name="name" id="locationsrch" class="form-control" required="">
						<input type="hidden" placeholder="Place id" name="place_id" value="{{ old('place_id') }}" id="place_id">
						<input type="hidden" name="latitude" value="{{ old('latitude') }}" id="latitude">
						<input type="hidden" required placeholder="City" name="city" value="{{ old('city') }}" id="city">
						<input type="hidden" required placeholder="Country" name="country" value="{{ old('country') }}" id="country">
						<input type="hidden" name="longitude" value="{{ old('longitude') }}" id="longitude">
						<input type="hidden" required placeholder="Postal Code" name="postalCode" value="{{ old('postalCode') }}" id="postalCode">
						<input type="hidden" required placeholder="State" name="state" value="{{ old('state') }}" id="state">
						<input type="hidden" required placeholder="Street" name="street" value="{{ old('street') }}" id="street">
						<input type="hidden" placeholder="Place Name" name="placeName" value="{{ old('placeName') }}" id="placeName">
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
						<label for="recipient-name" class="control-label mb-10 title">Country:</label>
						<select name="work_country_id" id="work_country_id" required=""  class="form-control">
							<option value="">Select Country</option>
							@foreach($countries as $country)
							<option value="{{$country->id}}">{{$country->name}}</option>
							@endforeach
						</select>
						<div class="form-group">
							<label for="recipient-name" class="control-label mb-10 title">City:</label>
							<select name="work_city_id" id="work_city_id" required=""  class="form-control">
								<option value=""></option>
							</select>
							<div class="help-block with-errors"></div>
						</div>
						<div class="help-block with-errors"></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Add</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade in" id="locationEdit_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Edit Location</h5>
			</div>
			<form id="editLocationFrm" data-toggle="validator">
				<!--<input type="text" name="name" class="form-control" required="" value="" id="locationsrh">-->
				<div class="modal-body" id="editContent">

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Update</button>
				</div>
			</form>
		</div>
	</div>
</div>
@section('scripts')
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAOzSrdzRrerVtmmGbMY1vvSWr-TobQr-U&libraries=places&callback=initialize"
></script>
<script>
	$('#snowAngelTable').DataTable({
		pageLength:10,
		// processing: true,
		serverSide: true,
		// sServerMethod: "POST",
		lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
		ajax: {
			type: "post",
			url: "{{ route('admin.work_countries.work_countries') }}",
			data: function ( d ) {
				d._token = "{{ csrf_token() }}";
			},
			complete:function(){
				if( $('[data-toggle="tooltip"]').length > 0 )
					$('[data-toggle="tooltip"]').tooltip();
			}
		},
		columns:[
		{ data:'DT_RowIndex',name:'id' },
		{ data:'name',name:'name' },
		{ data:'action',name:'action', orderable:false, searchable:false },
		]
	});

	$( "#addCountryFrm" ).submit(function( event ) {
		event.preventDefault();
		var data=$( "#addCountryFrm" ).serialize();
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:'POST',
			url:'{{route('admin.work_countries.store')}}',
			data:data,
			dataType: "json",
			success:function(data) {
				switch (data.status) {
					case true:
					toastr.success(data.message);
					location.reload();
					break;
					case false:
					toastr.warning(data.message);
					break;
					default:
					toastr.error("You are not Authorized to access this page");
					break;
				}
			},
		});
	});

	$('#countryEdit_modal').on('shown.bs.modal', function(e) {
		$("#country_id").val($(e.relatedTarget).data('id'));
		$("#country").val($(e.relatedTarget).data('name'));
	});

	$( "#editStateFrm" ).submit(function( event ) {
		event.preventDefault();
		var data=$( "#editStateFrm" ).serialize();
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:'PUT',
			url:'{{route('admin.update_state','')}}/'+$("#state_id").val(),
			data:data,
			dataType: "json",
			success:function(data) {
				switch (data.status) {
					case true:
					toastr.success(data.message);
					location.reload();
					break;
					case false:
					toastr.warning(data.message);
					break;
					default:
					toastr.error("You are not Authorized to access this page");
					break;
				}
			},
		});
	});

	function deleteCountry(id){ 
		var token = "{{ csrf_token() }}";

		swal({
			title: "Are you sure?",   
			text: "You will not be able to recover this information!",   
			type: "warning",   
			showCancelButton: true,   
			confirmButtonColor: "#e69a2a",   
			confirmButtonText: "Yes, delete it!",   
			cancelButtonText: "No, cancel please!",   
			closeOnConfirm: false,   
			closeOnCancel: false 
		},
		function(isConfirm){   
			if (isConfirm) {
				$.ajax({
					headers: {
						'X-CSRF-TOKEN':  "{{ csrf_token() }}"
					},
					method:'DELETE',
					url:'{{route('admin.work_countries.destroy','')}}/'+id,
					dataType: "json",
					success:function(data) {

						switch (data.status) {
							case true:
							swal("Deleted!", "Your information has been deleted.", "success");  
							toastr.success(data.message);
							location.reload();
							break;
							case false:
							toastr.error(data.message);
							break;
							default:
							toastr.error("You are not Authorized to access this page");
							break;
						}
					},
				});
			} else {
				swal("Cancelled", "Your information is safe! :)", "error"); 
			}
		});
	}

	/* code for maintain city js*/

	/* STATE CODE */
	/* ADD STATE */
	$( "#addStateFrm" ).submit(function( event ) {
		event.preventDefault();
		var data=$( "#addStateFrm" ).serialize();
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:'POST',
			url:'{{route('admin.create_state')}}',
			data:data,
			dataType: "json",
			success:function(data) {
				switch (data.status) {
					case true:
					toastr.success(data.message);
					location.reload();
					break;
					case false:
					toastr.warning(data.message);
					break;
					default:
					toastr.error("You are not Authorized to access this page");
					break;
				}
			},
		});
	});
	/* END ADD STATE */


	/* STATE TABLE GRID VIEW */
	$('#stateDataTable').DataTable({
		pageLength:10,
			// processing: true,
			serverSide: true,
			// sServerMethod: "POST",
			lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
			ajax: {
				type: "get",
				url: "{{ route('admin.getStateList') }}",
				data: function ( d ) {
					d._token = "{{ csrf_token() }}";
				},
				complete:function(){
					if( $('[data-toggle="tooltip"]').length > 0 )
						$('[data-toggle="tooltip"]').tooltip();
				}
			},
			columns:[
			{ data:'DT_RowIndex',name:'id' },
			{ data:'name',name:'name' },
			{ data:'tax_percentage',name:'tax_percentage' },
			{ data:'country',name:'country'},
			{ data:'action',name:'action', orderable:false, searchable:false },
			]
		});

	/* EDIT MODEL */		
	$( "#editCountryFrm" ).submit(function( event ) {
		event.preventDefault();
		var data=$( "#editCountryFrm" ).serialize();
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:'PUT',
			url:'{{route('admin.work_countries.update','')}}/'+$("#country_id").val(),
			data:data,
			dataType: "json",
			success:function(data) {
				switch (data.status) {
					case true:
					toastr.success(data.message);
					location.reload();
					break;
					case false:
					toastr.warning(data.message);
					break;
					default:
					toastr.error("You are not Authorized to access this page");
					break;
				}
			},
		});
	});
	/* STATE END CODE */
	$('#stateEdit_modal').on('shown.bs.modal', function(e) {
		$("#state_id").val($(e.relatedTarget).data('id'));
		$("#state").val($(e.relatedTarget).data('name'));
		$("#work1_country_id").val($(e.relatedTarget).data('country'));
		$("#tax_percentage").val($(e.relatedTarget).data('taxpercentage'));
		console.log($(e.relatedTarget).data('taxpercentage'));
	});

	$('#workCity').DataTable({
		pageLength:10,
		// processing: true,
		serverSide: true,
		// sServerMethod: "POST",
		lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
		ajax: {
			type: "post",
			url: "{{ route('admin.work_cities.work_cities') }}",
			data: function ( d ) {
				d._token = "{{ csrf_token() }}";
			},
			complete:function(){
				if( $('[data-toggle="tooltip"]').length > 0 )
					$('[data-toggle="tooltip"]').tooltip();
			}
		},
		columns:[
		{ data:'DT_RowIndex',name:'id' },
		{ data:'name',name:'name' },
		{ data:'state',name:'state' },
		{ data:'country',name:'country' },
		{ data:'timezone',name:'timezone' },
		{ data:'action',name:'action', orderable:false, searchable:false },
		]
	});

	$( "#addCityFrm" ).submit(function( event ) {
		event.preventDefault();
		var data=$( "#addCityFrm" ).serialize();
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:'POST',
			url:'{{route('admin.work_cities.store')}}',
			data:data,
			dataType: "json",
			success:function(data) {
				switch (data.status) {
					case true:
					toastr.success(data.message);
					location.reload();
					break;
					case false:
					toastr.warning(data.message);
					break;
					default:
					toastr.error("You are not Authorized to access this page");
					break;
				}
			},
		});
	});

	$('#cityEdit_modal').on('shown.bs.modal', function(e) {
		$("#city_id").val($(e.relatedTarget).data('id'));
		$("#city").val($(e.relatedTarget).data('name'));
		$("#work_country_id").val($(e.relatedTarget).data('country'));
		$(".work_state_id").val($(e.relatedTarget).data('state'));
		$("#timezone").val($(e.relatedTarget).data('timezone'));
		$("#timezone_name").val($(e.relatedTarget).data('timezone_name'));
	});

	$( "#editCityFrm" ).submit(function( event ) {
		event.preventDefault();
		var data=$( "#editCityFrm" ).serialize();
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:'PUT',
			url:'{{route('admin.work_cities.update','')}}/'+$("#city_id").val(),
			data:data,
			dataType: "json",
			success:function(data) {
				switch (data.status) {
					case true:
					toastr.success(data.message);
					location.reload();
					break;
					case false:
					toastr.warning(data.message);
					break;
					default:
					toastr.error("You are not Authorized to access this page");
					break;
				}
			},
		});
	});

	function deleteCity(id){ 
		var token = "{{ csrf_token() }}";

		swal({
			title: "Are you sure?",   
			text: "You will not be able to recover this information!",   
			type: "warning",   
			showCancelButton: true,   
			confirmButtonColor: "#e69a2a",   
			confirmButtonText: "Yes, delete it!",   
			cancelButtonText: "No, cancel please!",   
			closeOnConfirm: false,   
			closeOnCancel: false 
		},
		function(isConfirm){   
			if (isConfirm) {
				$.ajax({
					headers: {
						'X-CSRF-TOKEN':  "{{ csrf_token() }}"
					},
					method:'DELETE',
					url:'{{route('admin.work_cities.destroy','')}}/'+id,
					dataType: "json",
					success:function(data) {
						switch (data.status) {
							case true:
							swal("Deleted!", "Your information has been deleted.", "success");  
							toastr.success(data.message);
							location.reload();
							break;
							case false:
							toastr.error(data.message);
							break;
							default:
							toastr.error("You are not Authorized to access this page");
							break;
						}
					},
				});
			} else {
				swal("Cancelled", "Your information is safe! :)", "error"); 
			}
		});
	}

	/* code for maintain  work location js */
	$('#workLocations').DataTable({
		pageLength:10,
		// processing: true,
		serverSide: true,
		// sServerMethod: "POST",
		lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
		ajax: {
			type: "post",
			url: "{{ route('admin.work_locations.work_locations') }}",
			data: function ( d ) {
				d._token = "{{ csrf_token() }}";
			},
			complete:function(){
				if( $('[data-toggle="tooltip"]').length > 0 )
					$('[data-toggle="tooltip"]').tooltip();
			}
		},
		columns:[
		{ data:'DT_RowIndex',name:'id' },
		{ data:'name',name:'name' },
		{ data:'country',name:'country' },
		{ data:'city',name:'city' },
		{ data:'action',name:'action', orderable:false, searchable:false },
		]
	});

	$( "#addLocationFrm" ).submit(function( event ) {
		event.preventDefault();
		var data=$( "#addLocationFrm" ).serialize();
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:'POST',
			url:'{{route('admin.work_locations.store')}}',
			data:data,
			dataType: "json",
			success:function(data) {
				switch (data.status) {
					case true:
					toastr.success(data.message);
					location.reload();
					break;
					case false:
					toastr.warning(data.message);
					break;
					default:
					toastr.error("You are not Authorized to access this page");
					break;
				}
			},
		});
	});

	$(document).on('shown.bs.modal','#locationEdit_modal', function(e) {
		var url = '{{ route("admin.work_locations.edit", ":id") }}';
		url = url.replace(':id', $(e.relatedTarget).data('id'));
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:'GET',
			url:url,
			dataType: "html",
			success:function(data) {
				//alert(data);
				$('#editContent').html(data);
				initialize1();
			},
		});
		// $("#city").val($(e.relatedTarget).data('name'));
		// $("#work_country_id").val($(e.relatedTarget).data('country'));
	});

	$(document).on('change','#country_id_city , #work_country_id',function(e){
		e.preventDefault();
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:'GET',
			url:'{{route('admin.work_city.get_state','')}}/'+$(this).val(),
			dataType: "html",
			success:function(data) {
				$('.work_state_id').html(data);
			},
		});
	});

	$(document).on('change','#work_country_id',function(e){
		e.preventDefault();
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:'GET',
			url:'{{route('admin.work_locations.get_cities_by_country','')}}/'+$(this).val(),
			dataType: "html",
			success:function(data) {
				$('#work_city_id').html(data);
			},
		});
	});
	$(document).on('change', '#work_country_idedit', function(e) {
		e.preventDefault();
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:'GET',
			url:'{{route('admin.work_locations.get_cities_by_country','')}}/'+$(this).val(),
			dataType: "html",
			success:function(data) {
				$('#work_city_idedit').html(data);
			},
		});
	});

	$( "#editLocationFrm" ).submit(function( event ) {
		event.preventDefault();
		var data=$( "#editLocationFrm" ).serialize();
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:'PUT',
			url:'{{route('admin.work_locations.update','')}}/'+$("#location_id").val(),
			data:data,
			dataType: "json",
			success:function(data) {
				switch (data.status) {
					case true:
					toastr.success(data.message);
					location.reload();
					break;
					case false:
					toastr.warning(data.message);
					break;
					default:
					toastr.error("You are not Authorized to access this page");
					break;
				}
			},
		});
	});

	function deleteLocation(id){ 
		var token = "{{ csrf_token() }}";
		
		swal({
			title: "Are you sure?",   
			text: "You will not be able to recover this information!",   
			type: "warning",   
			showCancelButton: true,   
			confirmButtonColor: "#e69a2a",   
			confirmButtonText: "Yes, delete it!",   
			cancelButtonText: "No, cancel please!",   
			closeOnConfirm: false,   
			closeOnCancel: false 
		},
		function(isConfirm){   
			if (isConfirm) {
				$.ajax({
					headers: {
						'X-CSRF-TOKEN':  "{{ csrf_token() }}"
					},
					method:'DELETE',
					url:'{{route('admin.work_locations.destroy','')}}/'+id,
					dataType: "json",
					success:function(data) {
						switch (data.status) {
							case true:
							swal("Deleted!", "Your information has been deleted.", "success");  
							toastr.success(data.message);
							location.reload();
							break;
							case false:
							toastr.error(data.message);
							break;
							default:
							toastr.error("You are not Authorized to access this page");
							break;
						}
					},
				});
			} else {
				swal("Cancelled", "Your information is safe! :)", "error"); 
			}
		});
	}

	function initialize() {
        //static coordinates
        var coordinates = {lat: 42.345573, lng: -71.098326};

        //simple map instance
        var map = new google.maps.Map(document.getElementById('locationsrch'), {
        	center: coordinates,
        	zoom: 14,
            streetViewControl: false,   // it hides the street view control (i.e. person icon) from map
        });

        // Set up the markers on the map
        var marker = new google.maps.Marker({
        	map: map,
        	center: coordinates,
        	draggable: false,
        	animation: google.maps.Animation.DROP,
        });
        marker.setVisible(true);

        //simple street view
        // setStreetViewMethod(map,coordinates);  

        //set the autocomplete
        var input = document.getElementById('locationsrch');
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);
        autocomplete.setFields(['address_components', 'geometry', 'icon', 'name','place_id']); // Set the data fields to return when the user selects a place.

        //change listener on each autocomplete action
        autocomplete.addListener('place_changed', function(){
        	marker.setVisible(false);

        	var place = autocomplete.getPlace();
        	if (!place.geometry) {
        		window.alert("No details available for input: '" + place.name + "'");
        		return;
        	}

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
            	map.fitBounds(place.geometry.viewport);
            } else {
            	map.setCenter(place.geometry.location);
                map.setZoom(17);  // Why 17? Because it looks good.
            }
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);

            var address = '';
            if (place.address_components) {
            	address = [
            	(place.address_components[0] && place.address_components[0].short_name || ''),
            	(place.address_components[1] && place.address_components[1].short_name || ''),
            	(place.address_components[2] && place.address_components[2].short_name || '')
            	].join(' ');
            }
            console.log(place);
            placeInfo = getPlaceInformation(place);
            $('#place_id').val(place.place_id);
            $('#latitude').val(placeInfo['latitude']);
            $('#city').val(placeInfo['city']);
            $('#country').val(placeInfo['country']);
            $('#longitude').val(placeInfo['longitude']);
            $('#postalCode').val(placeInfo['postalCode']);
            $('#state').val(placeInfo['state']);
            $('#street').val(placeInfo['street']);
            $('#placeName').val(placeInfo['name']);

            // setStreetViewMethod(map,place.geometry.location);  
        });


        //update the street view on dragging of marker
        google.maps.event.addListener(marker, 'dragend', function (event) {
        	var newPosition = marker.getPosition();
        	setStreetViewMethod(map,newPosition);
        	geocodePosition(newPosition);
        });
    }    
    function initialize1() {
        //static coordinates
        var coordinates = {lat: 42.345573, lng: -71.098326};
        
        //simple map instance
        var map = new google.maps.Map(document.getElementById('locationsrh'), {
        	center: coordinates,
        	zoom: 14,
            streetViewControl: false,   // it hides the street view control (i.e. person icon) from map
        });

        // Set up the markers on the map
        var marker = new google.maps.Marker({
        	map: map,
        	center: coordinates,
        	draggable: false,
        	animation: google.maps.Animation.DROP,
        });
        marker.setVisible(true);

        //simple street view
        // setStreetViewMethod(map,coordinates);  

        //set the autocomplete
        var input = document.getElementById('locationsrh');
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);
        autocomplete.setFields(['address_components', 'geometry', 'icon', 'name','place_id']); // Set the data fields to return when the user selects a place.

        //change listener on each autocomplete action
        autocomplete.addListener('place_changed', function(){
        	marker.setVisible(false);

        	var place = autocomplete.getPlace();
        	if (!place.geometry) {
        		window.alert("No details available for input: '" + place.name + "'");
        		return;
        	}

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
            	map.fitBounds(place.geometry.viewport);
            } else {
            	map.setCenter(place.geometry.location);
                map.setZoom(17);  // Why 17? Because it looks good.
            }
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);

            var address = '';
            if (place.address_components) {
            	address = [
            	(place.address_components[0] && place.address_components[0].short_name || ''),
            	(place.address_components[1] && place.address_components[1].short_name || ''),
            	(place.address_components[2] && place.address_components[2].short_name || '')
            	].join(' ');
            }
            console.log(place);
            placeInfo = getPlaceInformation(place);
            $('#place_id1').val(place.place_id);
            $('#latitude1').val(placeInfo['latitude']);
            $('#city1').val(placeInfo['city']);
            $('#country1').val(placeInfo['country']);
            $('#longitude1').val(placeInfo['longitude']);
            $('#postalCode1').val(placeInfo['postalCode']);
            $('#state1').val(placeInfo['state']);
            $('#street1').val(placeInfo['street']);
            $('#placeName1').val(placeInfo['name']);
            // setStreetViewMethod(map,place.geometry.location);  
        });

        

        //update the street view on dragging of marker
        google.maps.event.addListener(marker, 'dragend', function (event) {
        	var newPosition = marker.getPosition();
        	setStreetViewMethod(map,newPosition);
        	geocodePosition(newPosition);
        });
    }    
    function getPlaceInformation(place){
    	placeInfo = [];
    	placeInfo['latitude'] = "";
    	placeInfo['longitude'] = "";
    	placeInfo['street'] = "";
    	placeInfo['city'] = "";
    	placeInfo['state'] = "";
    	placeInfo['postalCode'] = "";
    	placeInfo['country'] = "";

    	placeInfo['name'] = place.name;
    	placeInfo['latitude'] = place.geometry.location.lat();
    	placeInfo['longitude'] = place.geometry.location.lng();
    	$.each(place.address_components,function(index,value){
    		if(value.types[0] == 'postal_code'){
    			placeInfo['postalCode'] = value['long_name'];
    		}else if(value.types[0] == 'locality' || value.types[0] == 'administrative_area_level_3'){
    			placeInfo['city'] = value['long_name'];
    		}else if(value.types[0] == 'administrative_area_level_1'){
    			placeInfo['state'] = value['long_name'];
    		}else if(value.types[0] == 'street_number1'){
    			placeInfo['street'] = value['long_name'];
    		}else if(value.types[0] == 'country'){
    			placeInfo['country'] = value['long_name'];
    		}
    	});
    	return placeInfo;
    }
    $('#locationsrch').keypress(function(){
    	initialize();
    });

    $('window').load(function(){
    	initialize();
        //initialize1();
    })
</script>
@endsection
@endsection