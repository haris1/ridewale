@extends('admin.layouts.admin-app')
@section('styles')
<style type="text/css">
.disply_temp, .disply_transfer{
	display: none;
}
</style>
@endsection
@section('content')
<div class="page-wrapper">
    
    <div class="container-fluid">
    	<!-- Title -->
		<div class="row heading-bg">
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			  <h5 class="txt-dark"> Fare Management</h5>
			</div>
			<!-- Breadcrumb -->
			<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
			  <ol class="breadcrumb">
				<li><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
				<li class="active"><span>Fare Management</span></li>
			  </ol>
			</div>
			<!-- /Breadcrumb -->
		</div>
		<!-- /Title -->
		
		<!-- MODEL LIST -->
		<!-- Row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default card-view settableboxadd">
					<div class="panel-heading">
						<div class="pull-left">
							<h6 class="panel-title txt-dark">Fare Management List</h6>
						</div>
						<div class="pull-right">
							<a class="btn btn-success" data-toggle="modal" data-target="#addFareManagementModal" data-original-title="Add">Add</a>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="panel-wrapper collapse in">
						<div class="panel-body">
							<div class="table-wrap">
								<div class="table-responsive users_detateblebox">
									<table id="datatable" class="table table-striped table-bordered" style="width:100%">
										<thead>
											<tr>
												<th>Sr. No</th>
												<th>City To</th>
												<th>City From</th>
												<th>Price</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</div>
		<!-- row -->
	</div>
</div>

<!-- modal Fare Management Model -->
<!-- ADD -->
<div class="modal fade in" id="addFareManagementModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Add Fare Management</h5>
			</div>
			<form id="fareManagementForm" method="POST">
				@csrf
				<div class="modal-body">
					<div class="form-group">
						<label class="control-label mb-10 text-left">City (To)</label>
						<select class="form-control" name="to_city" required="">
							<option value="">Select City</option>
							@forelse($cites as $city)
							<option value="{{ $city->id }}">{{ $city->name }}</option>
							@empty
							@endforelse
						</select>
					</div>
					<div class="form-group ">
						<label class="control-label mb-10 text-left">City (From)</label>
						<select class="form-control" name="from_city" required="">
							<option value="">Select City</option>
							@foreach($cites as $city)
							<option value="{{ $city->id }}">{{ $city->name }}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group ">
						<label class="control-label mb-10 text-left">Price</label>
						<input type="text" name="price" class="form-control">
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-success">ADD</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- EDIT -->
<div class="modal fade in" id="editFare" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Edit Fare Management</h5>
			</div>
			<form id="editfareForm" data-toggle="validator" method="POST">
				@csrf
				<input type="hidden" name="id">
				<div class="modal-body">
					<div class="form-group">
						<label class="control-label mb-10 text-left">City (To)</label>
						<select class="form-control" name="to_city" required="">
							<option value="">Select City</option>
							@forelse($cites as $city)
							<option value="{{ $city->id }}">{{ $city->name }}</option>
							@empty
							@endforelse
						</select>
					</div>
					<div class="form-group ">
						<label class="control-label mb-10 text-left">City (From)</label>
						<select class="form-control" name="from_city" required="">
							<option value="">Select City</option>
							@foreach($cites as $city)
							<option value="{{ $city->id }}">{{ $city->name }}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group ">
						<label class="control-label mb-10 text-left">Price</label>
						<input type="text" name="price" class="form-control">
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-success">Save</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- END modal Fare Management Model -->


@endsection
@section('scripts')

<script>	

		/*$("select[name='to_city']").change(function(){

		})*/
		//AUTO SEARCH IN CITY
		/*$(document).ready(function() {
		    $('#to_city').select2();
		});*/

		//ADD FARE MANAGEMENT
		$("#fareManagementForm").submit(function(e) {
                e.preventDefault();
            }).validate({
            focusInvalid: false, 
            ignore: "",
            rules: {
                to_city: { 
                    required: true 
                },
                from_city: {
                    required: true
                },
                price: {
                    required: true,
                    number: true,
                },
            },

            errorPlacement: function(error, element) {
                error.insertAfter(element);
            },
            submitHandler: function (form) {
                $.ajax({
                    type: "POST",
                    url: '{{ route("admin.addFare") }}',
                    data:  $("#fareManagementForm").serialize(),
                    success: function(response)
                    {
                        if (response.status == true) {
                            toastr.success(response.message);
                            $('input[name="price"] ,select').val('');
                            $('#addFareManagementModal').modal('hide');
                            datatable.ajax.reload();
                        } else {
                            toastr.warning(response.message);
                        }
                    }
                });
            }
        });

        /* DATA TABLE */
		var datatable = $('#datatable').DataTable({
			pageLength:10,
			// processing: true,
			ajax: "data.json",
			serverSide: true,
			// sServerMethod: "POST",
			lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
				ajax: {
					type: "get",
					url: "{{ route('admin.fareList') }}",
					data: function ( d ) {
						d._token = "{{ csrf_token() }}";
					},
					complete:function(){
						if( $('[data-toggle="tooltip"]').length > 0 )
							$('[data-toggle="tooltip"]').tooltip();
					}
				},
				columns:[
					{ data:'DT_RowIndex',name:'id' },
					{ data:'to_city',name:'to_city' },
					{ data:'from_city',name:'from_city' },
					{ data:'price',name:'price'},
					{ data:'action',name:'action', orderable:false, searchable:false },
				]
			});

		$('#editFare').on('shown.bs.modal', function(e) {
			console.log($(e.relatedTarget).data('to-city'));
			$("#editFare select[name='to_city']").val($(e.relatedTarget).data('to-city'));
			$("#editFare select[name='from_city']").val($(e.relatedTarget).data('from-city'));
			$("#editFare input[name='price']").val($(e.relatedTarget).data('price'));
			$("#editFare input[name='id']").val($(e.relatedTarget).data('id'));
		});

		//EDIT 
		$( "#editfareForm" ).submit(function(e) {
                e.preventDefault();
            }).validate({
            focusInvalid: false, 
            ignore: "",
            rules: {
                to_city: { 
                    required: true 
                },
                from_city: {
                    required: true
                },
                price: {
                    required: true,
                    number: true,
                },
            },

            errorPlacement: function(error, element) {
                error.insertAfter(element);
            },
            submitHandler: function (form) {
                $.ajax({
                    type: "POST",
                    url: '{{ route("admin.editFare") }}',
                    data:  $("#editfareForm").serialize(),
                    success: function(response)
                    {
                        if (response.status == true) {
                            toastr.success(response.message);
                            $('input[name="price"] , select').val('');
                            $('#editFare').modal('hide');
                            datatable.ajax.reload();
                        } else {
                            toastr.warning(response.message);
                        }
                    }
                });
            }
        });



        //DELETE

        function deleteType(id){ 
			var token = "{{ csrf_token() }}";		
			swal({
				title: "Are you sure?",   
	            text: "You will not be able to recover this information!",   
	            type: "warning",   
	            showCancelButton: true,   
	            confirmButtonColor: "#e69a2a",   
	            confirmButtonText: "Yes, delete it!",   
	            cancelButtonText: "No, cancel please!",   
	            closeOnConfirm: false,   
	            closeOnCancel: false 
			},
			function(isConfirm){
				if (isConfirm) {
					$.ajax({
						/*headers: {
							'X-CSRF-TOKEN':  "{{ csrf_token() }}"
						},*/
						headers: {
					        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					    },
						method:'DELETE',
						url:'deleteFare/'+id,
						dataType: "json",
						success:function(data) {
							switch (data.status) {
							case true:
							datatable.ajax.reload();
							swal("Deleted!", "Your information has been deleted.", "success");  
							toastr.success(data.message);
							break;
							case false:
							toastr.error(data.message);
							break;
						}
						},
					});
				} else {
					swal("Cancelled", "Your information is safe! :)", "error"); 
				}
			}
		)};
</script>
@endsection