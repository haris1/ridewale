@extends('admin.layouts.admin-app')

@section('content')

@section('css_ref')
<link href="{{ asset('admin_assets/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection
<!-- Main Content -->
<div class="page-wrapper">

	<div class="container-fluid">

		<!-- Title -->
		<div class="row heading-bg">
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
				<h5 class="txt-dark">Work Cities List</h5>
			</div>
			<!-- Breadcrumb -->
			<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
				<ol class="breadcrumb">
					<li><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
					<li class="active"><span>Work Cities List</span></li>
				</ol>
			</div>
			<!-- /Breadcrumb -->
		</div>
		<!-- /Title -->

		<!-- Row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default card-view settableboxadd">
					<div class="panel-heading">
						<div class="pull-left">
							<h6 class="panel-title txt-dark"></h6>
						</div>
						<div class="pull-right">
							<a href="javascript:void(0);" class="btn btn-success" data-toggle="modal" data-target="#cityAdd_modal" data-title="Add"><span class="btn-text">Add</span></a>								
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="panel-wrapper collapse in">
						<div class="panel-body">
							<div class="table-wrap">
								<div class="table-responsive">
									<table id="workCity" class="table  table-striped table-bordered mb-0 alltextbtnandiconset1 iconsize_set pb-30" >
										<thead>
											<tr>
												<th>Sr. No</th>
												<th>City</th>
												<th>Country</th>
												<th>Action</th>
											</tr>
										</thead>
										<tfoot>
											<tr>
												<th>Sr. No</th>
												<th>City</th>
												<th>Country</th>
												<th>Action</th>
											</tr>
										</tfoot>
										<tbody>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</div>
		<!-- /Row -->
	</div>

	@include('admin.layouts.footer')
</div>
<div class="modal fade in" id="cityAdd_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Add Work City</h5>
			</div>
			<form id="addCityFrm" data-toggle="validator">
				<div class="modal-body">
					<div class="form-group">
						<label for="recipient-name" class="control-label mb-10 title">City:</label>
						<input type="text" name="name" class="form-control" required="">
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
						<label for="recipient-name" class="control-label mb-10 title">Country:</label>
						<select name="work_country_id" required=""  class="form-control">
							<option value=""></option>
							@foreach($countries as $country)
							<option value="{{$country->id}}">{{$country->name}}</option>
							@endforeach
						</select>
						<div class="help-block with-errors"></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Add</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade in" id="cityEdit_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Add Work Country</h5>
			</div>
			<form id="editCityFrm" data-toggle="validator">
				<div class="modal-body">
					<div class="form-group">
						<label for="recipient-name" class="control-label mb-10 title">City:</label>
						<input type="text" name="name" class="form-control" required="" id="city">
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
						<label for="recipient-name" class="control-label mb-10 title">Country:</label>
						<select name="work_country_id" id="work_country_id" required=""  class="form-control">
							<option value=""></option>
							@foreach($countries as $country)
							<option value="{{$country->id}}">{{$country->name}}</option>
							@endforeach
						</select>
						<div class="help-block with-errors"></div>
					</div>
					<input type="hidden" name="city_id" id="city_id" value="">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Update</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- /Main Content -->
@section('scripts')
<script>
	$('#workCity').DataTable({
		pageLength:10,
		// processing: true,
		serverSide: true,
		// sServerMethod: "POST",
		lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
		ajax: {
			type: "post",
			url: "{{ route('admin.work_cities.work_cities') }}",
			data: function ( d ) {
				d._token = "{{ csrf_token() }}";
			},
			complete:function(){
				if( $('[data-toggle="tooltip"]').length > 0 )
					$('[data-toggle="tooltip"]').tooltip();
			}
		},
		columns:[
		{ data:'DT_RowIndex',name:'id' },
		{ data:'name',name:'name' },
		{ data:'country',name:'country' },
		{ data:'action',name:'action', orderable:false, searchable:false },
		]
	});

	$( "#addCityFrm" ).submit(function( event ) {
		event.preventDefault();
		var data=$( "#addCityFrm" ).serialize();
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:'POST',
			url:'{{route('admin.work_cities.store')}}',
			data:data,
			dataType: "json",
			success:function(data) {
				switch (data.status) {
					case true:
					toastr.success(data.message);
					location.reload();
					break;
					case false:
					toastr.warning(data.message);
					break;
					default:
					toastr.error("You are not Authorized to access this page");
					break;
				}
			},
		});
	});

	$('#cityEdit_modal').on('shown.bs.modal', function(e) {
		$("#city_id").val($(e.relatedTarget).data('id'));
		$("#city").val($(e.relatedTarget).data('name'));
		$("#work_country_id").val($(e.relatedTarget).data('country'));
	});

	$( "#editCityFrm" ).submit(function( event ) {
		event.preventDefault();
		var data=$( "#editCityFrm" ).serialize();
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:'PUT',
			url:'{{route('admin.work_cities.update','')}}/'+$("#city_id").val(),
			data:data,
			dataType: "json",
			success:function(data) {
				switch (data.status) {
					case true:
					toastr.success(data.message);
					location.reload();
					break;
					case false:
					toastr.warning(data.message);
					break;
					default:
					toastr.error("You are not Authorized to access this page");
					break;
				}
			},
		});
	});

	function deleteCity(id){ 
		var token = "{{ csrf_token() }}";

		swal({
			title: "Are you sure?",   
            text: "You will not be able to recover this information!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#e69a2a",   
            confirmButtonText: "Yes, delete it!",   
            cancelButtonText: "No, cancel please!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
		},
		function(isConfirm){   
			if (isConfirm) {
				$.ajax({
					headers: {
						'X-CSRF-TOKEN':  "{{ csrf_token() }}"
					},
					method:'DELETE',
					url:'{{route('admin.work_cities.destroy','')}}/'+id,
					dataType: "json",
					success:function(data) {
						switch (data.status) {
						case 200:
						table.ajax.reload();
						swal("Deleted!", "Your information has been deleted.", "success");  
						toastr.success(data.message);
						break;
						case 500:
						toastr.error(data.message);
						break;
						default:
						toastr.error("You are not Authorized to access this page");
						break;
					}
					},
				});
			} else {
				swal("Cancelled", "Your information is safe! :)", "error"); 
			}
		});
	}
</script>
@endsection
@endsection