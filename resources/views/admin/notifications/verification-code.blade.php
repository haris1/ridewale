@component('mail::message')

Welcome to Rideforu!

Your Email Verification Code :{{$otp}}

Thanks,<br>
{{ config('app.name') }}
@endcomponent