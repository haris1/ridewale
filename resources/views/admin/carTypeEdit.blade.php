<div class="form-group">
	<label class="control-label mb-10 text-left"> Select Company</label>
	<select class="form-control" name="editcompany" id="editcompany" required="">
		<option value="">Select Company</option>
		@foreach($companies as $company)
		<option value="{{$company['id']}}" @if($Car_type[0]->company_id == $company->id) {{'selected'}} @endif>{{ucfirst($company['name'])}}</option>
		@endforeach
	</select>
</div>
<div class="form-group">
	<label class="control-label mb-10 text-left" for="title_edit">Car Type Name</label>
	<input type="text" id="edit_carname" name="edit_carname"  class="form-control" placeholder="Car Type Name" data-error="Car Type Name is required !" required="" value="{{$Car_type[0]->name}}">
	<input type="hidden" name="edit_type_id" id="edit_type_id" value="{{$Car_type[0]->id}}">
	<div class="help-block with-errors"></div>
</div>
<div class="form-group">
	<label class="control-label mb-10 text-left" for="title">Car Type Icon</label>
	<input type="file" id="editfile" name="editfile" class="form-control" placeholder="Car Type Icon"></br>
	<img id="editimage" src="{{storage_path('images/type_logo/'.$Car_type[0]->logo)}}" height="150" width="150" />
	<div class="help-block with-errors"></div>
</div>