@extends('admin.layouts.admin-app')

@section('content')

<style type="text/css">
	td.details-rider::after , td.details-driver::after , td.details-report::after {
    	content: 'View';
    	font-family: 'Rubik-Medium';
    	color: #482080;
    	cursor: pointer;
	}
	tr.shown td.details-rider::after , tr.shown td.details-driver::after , tr.shown td.details-report::after {
		content: 'Close';
		font-family: 'Rubik-Medium';
    	color: #482080;
	}
</style>
<!-- Main Content -->
<div class="page-wrapper">

	<div class="container-fluid">
		<!-- Title -->
		<div class="row heading-bg">
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
				<!-- <h5 class="txt-dark">Cancelled by Riders</h5> -->
			</div>
			<!-- Breadcrumb -->
			<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
				<ol class="breadcrumb">
					<li><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
					<li class="active"><span>Cancelled Booking</span></li>
				</ol>
			</div>
			<!-- /Breadcrumb -->
		</div>
		<!-- /Title -->

		<!-- Row -->
		<div class="Cancelledbyride_table">
			<div class="riders_statustext">
				<h3>Cancelled by Riders</h3>
				<div class="maintanacradio_button setrefunded_box">					
					<p>Status :</p>
					<div class="switch-field">
						<input type="radio" name="rider_status" id="rider_status_left" value="refund" checked>
						<label for="rider_status_left">REFUNDED</label>
						<input type="radio" name="rider_status" id="rider_status_right" value="pending">
						<label for="rider_status_right">PENDING</label>
					</div>							
				</div>
			<!-- <div class="statusbtnandtx">
				<p>Status :</p>
				<a href="javascript:void(0)">Pending</a>
				<a href="javascript:void(0)">Refunded</a>
			</div> -->
			</div>
			<div class="statusride_table">
				<table id="ridersstatus" class="table table-striped table-bordered" style="width:100%">
					<thead>
						<tr>
							<th>Sr.</th>
							<th>Order Date</th>
							<th>Booking No.</th>
							<th>Cancelled D.</th>
							<th>Amount Rcvd.</th>
							<th>Refund Amt.</th>
							<th>Penalty Amt.</th>
							<th>Taxes</th>
							<th>Net Amt.</th>
							<th>More</th>
						</tr>
					</thead>
					<tbody>
						
					</tbody>
				</table>
			</div>


			<div class="riders_statustext">
				<h3>Cancelled by Drivers</h3>
				<div class="maintanacradio_button setrefunded_box">					
					<p>Status :</p>
					<div class="switch-field">
						<input type="radio" name="driver_status" id="driver_status_left" value="refund" checked>
						<label for="driver_status_left">REFUNDED</label>
						<input type="radio" name="driver_status" id="driver_status_right" value="pending">
						<label for="driver_status_right">PENDING</label>
					</div>							
				</div>
				<!-- <div class="statusbtnandtx">
					<p>Status :</p>
					<input type="radio" name="driver_status" value="refund" checked>
					<label for="switch_left">PENDING</label>
					<input type="radio" name="driver_status" value="pending">
					<label for="switch_right">REFUNDED</label>
				</div> -->
			</div>
			<div class="statusride_table">
				<table id="driversstatus" class="table table-striped table-bordered" style="width:100%">
					<thead>
						<tr>
							<th>Sr.</th>
							<th>Order Date</th>
							<th>Booking No.</th>
							<th>Cancelled D.</th>
							<th>Amount Rcvd.</th>
							<th>Refund Amt.</th>
							<th>Penalty Amt.</th>
							<th>Taxes</th>
							<th>Net Amt.</th>
							<th>More</th>
						</tr>
					</thead>
					<tbody>
						
					</tbody>
				</table>
			</div>


			<div class="riders_statustext">
				<h3>Cancelled by Drivers - Reported as No-show</h3>
				<div class="maintanacradio_button setrefunded_box">					
					<p>Status :</p>
					<div class="switch-field">
						<input type="radio" name="report_status" id="report_status_left" value="refund" checked>
						<label for="report_status_left">REFUNDED</label>
						<input type="radio" name="report_status" id="reposrt_status_right" value="pending">
						<label for="reposrt_status_right">PENDING</label>
					</div>							
				</div>
				<!-- <div class="statusbtnandtx">
					<p>Status :</p>
					<input type="radio" name="report_status" value="refund" checked>
					<label for="switch_left">PENDING</label>
					<input type="radio" name="report_status" value="pending">
					<label for="switch_right">REFUNDED</label>
				</div> -->
			</div>
			<div class="statusride_table">
				<table id="reportedstatus" class="table table-striped table-bordered" style="width:100%">
					<thead>
						<tr>
							<th>Sr.</th>
							<th>Order Date</th>
							<th>Booking No.</th>
							<th>Cancelled D.</th>
							<th>Amount Rcvd.</th>
							<th>Refund Amt.</th>
							<th>Penalty Amt.</th>
							<th>Taxes</th>
							<th>Net Amt.</th>
							<th>More</th>
						</tr>
					</thead>
					<tbody>
						
					</tbody>
				</table>
			</div>



		</div>
		<!-- /Row -->
	</div>
	
</div>
@endsection
@section('scripts')

<script>
	
	//Cancelled by Riders DATA TABLE
	$(document).ready(function ($){
		//RIDE STATUS
		var rider_table=$('#ridersstatus').DataTable({
			dom: 'Bfrtip',
			"serverSide": true,
			"sServerMethod": "GET",	
			"lengthMenu": [[10, 50, 100, -1], [10, 50, 100, "All"]],
			"ajax": {
				url: "{{route('admin.rider_status')}}",
				data: function ( d ) {
					d._token = "{{ csrf_token() }}";
					d.status = $("input[name='rider_status']:checked").val();
				}
			},
			"columns":[
				{ data:'DT_RowIndex',name:'id'},
				{ data:'date',name:'date',"width": "12%"},
				{ data:'order_id',name:'order_id',"width": "10%"},
				{ data:'cancel_date',name:'cancel_date',"width": "12%"},
				{ data:'amount',name:'amount',"width": "12%"},
				{ data:'refund_amount',name:'refund_amount',"width": "10%"},
				{ data:'penalty_amount',name:'penalty_amount',"width": "12%"},
				{ data:'tax_fare',name:'tax_fare',"width": "10%"},
				{ data:'net_amount',name:'net_amount',"width": "10%"},
				{
	                "className": 'details-rider',
	                "orderable": false,
	                "data": null,
	               	"name": null, 
	                "defaultContent": ''
	            },
			]
		});

		function RiderViewformat ( d ) {
		    return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
		        '<tr>'+
		            '<td>Status: '+d.status+'</td>'+
		        '</tr>'+
		        '<tr>'+
		            '<td>Reason for Cancellation: '+d.decline_reason+'</td>'+
		        '</tr>'+
		    '</table>';
		}
		$('#ridersstatus tbody').on('click', 'td.details-rider', function () {
	        var tr = $(this).closest('tr');
	        var row = rider_table.row( tr );
	 
	        if ( row.child.isShown() ) {
	            row.child.hide();
	            tr.removeClass('shown');
	        }
	        else {
	            row.child( RiderViewformat(row.data()) ).show();
	            tr.addClass('shown');
	        }
	    });

		$("input[name='rider_status']").click(function(){
            rider_table.ajax.reload();
        });


	    //DRIVER STATUS
	    var driver_table=$('#driversstatus').DataTable({
			dom: 'Bfrtip',
			"serverSide": true,
			"sServerMethod": "GET",	
			"lengthMenu": [[10, 50, 100, -1], [10, 50, 100, "All"]],
			"ajax": {
				url: "{{route('admin.driver_status')}}",
				data: function ( d ) {
					d._token = "{{ csrf_token() }}";
					d.status = $("input[name='driver_status']:checked").val();
				}
			},
			"columns":[
				{ data:'DT_RowIndex',name:'id'},
				{ data:'date',name:'date',"width": "12%"},
				{ data:'order_id',name:'order_id',"width": "10%"},
				{ data:'cancel_date',name:'cancel_date',"width": "12%"},
				{ data:'amount',name:'amount',"width": "12%"},
				{ data:'refund_amount',name:'refund_amount',"width": "10%"},
				{ data:'penalty_amount',name:'penalty_amount',"width": "12%"},
				{ data:'tax_fare',name:'tax_fare',"width": "10%"},
				{ data:'net_amount',name:'net_amount',"width": "10%"},
				{
	                "className": 'details-driver',
	                "orderable": false,
	                "data": null,
	               	"name": null, 
	                "defaultContent": ''
	            },
			]
		});

		function DriverViewformat ( d ) {
		    return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
		        '<tr>'+
		            '<td>Status: '+d.status+'</td>'+
		        '</tr>'+
		        '<tr>'+
		            '<td>Reason for Cancellation: '+d.cancelled_reason+'</td>'+
		        '</tr>'+
		    '</table>';
		}
		$('#driversstatus').on('click', 'td.details-driver', function () {
	        var tr = $(this).closest('tr');
	        var row = driver_table.row( tr );
	        if (row.child.isShown() ) {
	            row.child.hide();
	            tr.removeClass('shown');
	        }
	        else {
	        	row.child(DriverViewformat(row.data()) ).show();
	            tr.addClass('shown');
	        }
	    });
		$("input[name='driver_status']").click(function(){
            driver_table.ajax.reload();
        });


	    //REPORTED AS NO SHOW
		var report_table=$('#reportedstatus').DataTable({
			dom: 'Bfrtip',
			"serverSide": true,
			"sServerMethod": "GET",	
			"lengthMenu": [[10, 50, 100, -1], [10, 50, 100, "All"]],
			"ajax": {
				url: "{{route('admin.report_status')}}",
				data: function ( d ) {
					d._token = "{{ csrf_token() }}";
					d.status = $("input[name='report_status']:checked").val();
				}
			},
			"columns":[
				{ data:'DT_RowIndex',name:'id'},
				{ data:'date',name:'date',"width": "12%"},
				{ data:'order_id',name:'order_id',"width": "10%"},
				{ data:'cancel_date',name:'cancel_date',"width": "12%"},
				{ data:'amount',name:'amount',"width": "12%"},
				{ data:'refund_amount',name:'refund_amount',"width": "10%"},
				{ data:'penalty_amount',name:'penalty_amount',"width": "12%"},
				{ data:'tax_fare',name:'tax_fare',"width": "10%"},
				{ data:'net_amount',name:'net_amount',"width": "10%"},
				{
	                "className": 'details-report',
	                "orderable": false,
	                "data": null,
	               	"name": null, 
	                "defaultContent": ''
	            },
			]
		});

		function ReportViewformat ( d ) {
		    return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
		        '<tr>'+
		            '<td>Status: '+d.status+'</td>'+
		        '</tr>'+
		        '<tr>'+
		            '<td>Reason for Cancellation: '+d.decline_reason+'</td>'+
		        '</tr>'+
		    '</table>';
		}
		$('#reportedstatus tbody').on('click', 'td.details-report', function () {
	        var tr = $(this).closest('tr');
	        var row = report_table.row( tr );
	 
	        if ( row.child.isShown() ) {
	            row.child.hide();
	            tr.removeClass('shown');
	        }
	        else {
	            row.child( ReportViewformat(row.data()) ).show();
	            tr.addClass('shown');
	        }
	    });
	    $("input[name='report_status']").click(function(){
            report_table.ajax.reload();
        });
	});
</script>

@endsection