@extends('admin.layouts.admin-app')

@section('content')

@section('css_ref')
    <!-- <link href="{{ asset('admin_assets/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/> -->
@endsection
 <!-- Main Content -->
<div class="page-wrapper">
    
    <div class="container-fluid">
				
				<!-- Title -->
				<div class="row heading-bg">
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
					  <h5 class="txt-dark"> Vehicle management </h5>
					</div>
					<!-- Breadcrumb -->
					<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
					  <ol class="breadcrumb">
						<li><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
						<li class="active"><span>Vehicle management</span></li>
					  </ol>
					</div>
					<!-- /Breadcrumb -->
				</div>
				<!-- /Title -->
				
				<!-- MODEL LIST -->
				<!-- Row -->
				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default card-view settableboxadd">
							<div class="panel-heading">
								<div class="pull-left">
									<h6 class="panel-title txt-dark">Model List</h6>
								</div>
								<div class="pull-right">
									<a class="btn btn-success" data-toggle="modal" data-target="#addcarModel" data-original-title="Add">Add</a>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div class="table-wrap">
										<div class="table-responsive users_detateblebox">
											<table id="car_Model" class="table table-striped table-bordered" style="width:100%">
												<thead>
													<tr>
														<th>Sr. No</th>
														<th>Company Name</th>
														<th>Model Name</th>
														<th>Car Type</th>
														<th>Car Capacity</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>	
					</div>
				</div>
				<!-- row -->

				<!-- COMPANY LISY -->
				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default card-view settableboxadd">
							<div class="panel-heading">
								<div class="pull-left">
									<h6 class="panel-title txt-dark">Company list</h6>
								</div>
								<div class="pull-right">
									<a class="btn btn btn-success" data-toggle="modal" data-target="#addcarcom" data-original-title="Add">Add</a>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div class="table-wrap">
										<div class="table-responsive users_detateblebox">
											<table id="car_company" class="table table-striped table-bordered" style="width:100%">
												<thead>
													<tr>
														<th>Sr. No</th>
														<th>Company Name</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>	
					</div>
				</div>
				<!-- /Row -->

				<!-- CAR TYPE LIST -->
				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default card-view settableboxadd">
							<div class="panel-heading">
								<div class="pull-left">
									<h6 class="panel-title txt-dark">Car Type List</h6>
								</div>
								<div class="pull-right">
									<a class="btn btn-success" data-toggle="modal" data-target="#addcartype" data-original-title="Add">Add</a>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div class="table-wrap">
										<div class="table-responsive users_detateblebox">
											<table id="car_Types" class="table table-striped table-bordered" style="width:100%">
												<thead>
													<tr>
														<th>Sr. No</th>
														<th> Car Type </th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>	
					</div>
				</div>
				<!-- /Row -->

			</div>
    @include('admin.layouts.footer')
</div>
<!-- /Main Content -->

<!-- modal for add new car company -->
<div class="modal fade in" id="addcarcom" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Add Company Name</h5>
			</div>
			<form id="addcarcomform" data-toggle="validator">
				<div class="modal-body">
					<div class="form-group">
						<label class="control-label mb-10 title" for="carname">Company Name</label>
						<input type="text" id="carname" name="carname" class="form-control" placeholder="Company Name" data-error="Company name is required !" Autofocus required="">
						<div class="help-block with-errors"></div>
						@if ($errors->has('title'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('carname') }}</strong>
						</span>
						@endif
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
					<button type="submit" id="insert" class="btn btn-success mr-10 btn-sm">Add</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!--  Edit new car company modal -->
	<div class="modal fade in" id="editcarcom" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Update Company Name</h5>
			</div>
			<form id="editcarcomform" data-toggle="validator">
				@csrf
				{{ method_field('patch') }}
				<div class="modal-body">
					<div class="form-group">
						<label class="control-label mb-10 text-left" for="title_edit">{{ __('Company Name') }}</label>
						<input type="text" id="edit_carname" name="title"  class="form-control" placeholder="Company Name" data-error="Company Name is required !" required="">
						<input type="hidden" id="edit_com_id">
						<div class="help-block with-errors"></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</a>
					<button type="submit" id="update" class="btn btn-success mr-10 btn-sm" >Update</button>
				</div>
			</form>
		</div>
	</div>
</div>

	<!-- end of edit modal -->

<!--  modals for maintain models -->

<!-- modal for add new car Model -->

	<div class="modal fade in" id="addcarModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Add Car Model</h5>
			</div>
			<form id="addcarModelform" data-toggle="validator">
				<div class="modal-body">
					<div class="form-group">
						<label class="control-label mb-10 text-left"> Select Company</label>
						<select class="form-control" id="company" name="company" required="">
							<option value="">Select Company</option>
							@foreach($coms as $com)
							<option value="{{$com['id']}}">{{ucfirst($com['name'])}}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group ">
						<label class="control-label mb-10 text-left"> Select Type </label>
						<select class="form-control" id="type" name="type" required="">
							<option value="">Select Type</option>
							@foreach($types as $type)
							<option value="{{$type['id']}}">{{ucfirst($type['name'])}}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group ">
						<label class="control-label mb-10 text-left"> Select Capacity </label>
						<select class="form-control" id="capacity" name="capacity" required="">
							<option value="">Select Capacity</option>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
							<option value="6">6</option>
							<option value="7">7</option>
							<option value="8">8</option>
							<option value="9">9</option>
							<option value="10">10</option>
						</select>
					</div>
					<div class="form-group ">
						<label class="control-label mb-10 text-left" for="title">Car Model Name</label>
						<input type="text" id="carname" name="carname" class="form-control" placeholder="Car Model Name" data-error="Tax Category Title is required !" required="">
						<div class="help-block with-errors"></div>
						@if ($errors->has('title'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('title') }}</strong>
						</span>
						@endif
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
					<button type="submit" id="insert" class="btn btn-success mr-10 btn-sm">ADD</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!--  Edit new car Model modal -->

	<div class="modal fade in" id="editcarModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Update Car Model</h5>
			</div>
			<form id="editcarModelform" data-toggle="validator">
				<div class="modal-body">
					<div class="form-group">
						<label class="control-label mb-10 text-left"> Select Company</label>
						<select class="form-control" id="editcompany" name="editcompany" required="">
							<option value="">Select Company</option>
							@foreach($coms as $com)
							<option value="{{$com['id']}}">{{ucfirst($com['name'])}}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<label class="control-label mb-10 text-left"> Select Type </label>
						<select class="form-control" id="edittype" name="edittype" required="">
							<option value="">Select Type</option>
							@foreach($types as $type)
							<option value="{{$type['id']}}">{{ucfirst($type['name'])}}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<label class="control-label mb-10 text-left"> Select Capacity </label>
						<select class="form-control" id="editcapacity" name="editcapacity" required="">
							<option value="">Select Capacity</option>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
							<option value="6">6</option>
							<option value="7">7</option>
							<option value="8">8</option>
							<option value="9">9</option>
							<option value="10">10</option>
						</select>
					</div>
					<div class="form-group">
						<label class="control-label mb-10 text-left" for="title">Car Model Name</label>
						<input type="text" id="editcarname" name="editcarname" class="form-control" placeholder="Car Model Name" data-error="Tax Category Title is required !" required="">
						
						<div class="help-block with-errors"></div>
						@if ($errors->has('title'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('title') }}</strong>
						</span>
						@endif
					</div>
					<input type="hidden" name="edit_Model_id" id="edit_Model_id">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
					<button type="submit" id="update" class="btn btn-success mr-10 btn-sm">Submit</button>
				</div>
			</form>
		</div>
	</div>
</div>
	<!-- end of edit modal -->

<!-- code for maintain type of car modals-->

<div class="modal fade in" id="addcartype" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Add Car Type </h5>
			</div>
			<form id="addcartypeform" enctype="multipart/form-data" data-toggle="validator">
				@csrf
				<div class="modal-body">
					<!-- <div class="form-group">
						<label class="control-label mb-10 text-left"> Select Company</label>
						<select class="form-control" name="company" id="company" required="">
							<option value="">Select Company</option>
							@foreach($companies as $company)
							<option value="{{$company['id']}}">{{ucfirst($company['name'])}}</option>
							@endforeach
						</select>
					</div> -->
					<div class="form-group">
						<label class="control-label mb-10 text-left" for="title">Car Type</label>
						<input type="text" id="cartype" name="cartype" class="form-control" placeholder="Car Type" data-error="Tax Category Title is required !" required="">
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
						<label class="control-label mb-10 text-left" for="title">Car Type Icon</label>
						<input type="file" id="file" name="file" class="form-control" placeholder="Car Type Icon"></br>
						<img id="image" height="150" width="150"/>
						<input type="hidden" name="path" id="storage_path" value="{{asset('storage/type_logo')}}">
						<div class="help-block with-errors"></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
					<button type="submit" id="insert" class="btn btn-success mr-10 btn-sm">Add</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!--  Edit new car Type modal -->
<div class="modal fade in" id="editcartype" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Update Car Type</h5>
			</div>
			<form id="editcartypeform" method="PUT" enctype="multipart/form-data" data-toggle="validator">
				@csrf
				{{ method_field('PUT') }}
				<div class="modal-body" id="edit_type_model_data">
					<!-- <div class="form-group">
						<label class="control-label mb-10 text-left"> Select Company</label>
						<select class="form-control" name="editcompany" id="editcompany" required="">
							<option value="">Select Company</option>
							@foreach($companies as $company)
							<option value="{{$company['id']}}">{{ucfirst($company['name'])}}</option>
							@endforeach
						</select>
					</div> -->
					<div class="form-group">
						<label class="control-label mb-10 text-left" for="title_edit">Car Type </label>
						<input type="text" id="edit_type" name="edit_type"  class="form-control" placeholder="Car Type " data-error="Car Type is required !" required="">
						<input type="hidden" name="edit_type_id" id="edit_type_id">
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
						<label class="control-label mb-10 text-left" for="title">Car Type Icon</label>
						<input type="file" id="editfile" name="editfile" class="form-control" placeholder="Car Type Icon"></br>
						<img id="editimage" height="150" width="150" />
						<div class="help-block with-errors"></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-success mr-10 btn-sm">Update</button>
				</div>
			</form>
		</div>
	</div>
</div>



@section('scripts')
<script>
	var compan_table = $('#car_company').DataTable({
		pageLength:5,
		// processing: true,
		// ajax: "data.json",
		serverSide: true,
		// sServerMethod: "POST",
		lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
		ajax: {
			type: "post",
			url: "{{ route('admin.carcompany.list') }}",
			data: function ( d ) {
				d._token = "{{ csrf_token() }}";
			},
			complete:function(){
				if( $('[data-toggle="tooltip"]').length > 0 )
					$('[data-toggle="tooltip"]').tooltip();
			}
		},
		columns:[
			{ data:'DT_RowIndex',name:'id' },
			{ data:'name',name:'name' },
			{ data:'action',name:'action', orderable:false, searchable:false },
		]
	});

	


	
	$("#addcarcomform").submit(function( event ) {
			event.preventDefault();
			var data=$( "#addcarcomform" ).serialize();
			$('#insert').attr('disabled',true);
			var name = $('#carname').val();
			if ($('#addcarcomform').validator('validate').has('.has-error').length === 0) {

				$.ajax({
				method:'POST',
				url:'{{ route('admin.carCompany.store') }}',
				data:{'name':name,'_token':'{{csrf_token()}}'},
				success:function(data) {
					
					$('#insert').attr('disabled',false);
					$("#addcarcom").modal('hide');
					$("#carname").val('');
					compan_table.ajax.reload();
					switch (data.status) {
						case 200:
						toastr.success(data.message);
						break;
						case 500:
						toastr.error(data.message);
						break;
					}
				}
			});
			}
			
		});


		$(document).on('click',".edit_company",function(){
			$('#editcarcomform')[0].reset();
			var id = $(this).data('id');
			$.ajax({
				method:'POST',
				url:'{{ route('admin.carcompany.edit.name') }}',
				data:{'id':id,'_token':'{{csrf_token()}}'},
				success:function(data) {
					
					$('#insert').attr('disabled',false);
					$("#addcarcom").modal('hide');
					$("#carname").val('');
					switch (data.status) {
						case 200:
						$('#edit_com_id').val(data.dataid);
						$('#edit_carname').val(data.data);
						compan_table.ajax.reload();
						break;
						case 500:
						toastr.error(data.message);
						break;
					}
				},
				complete:function(){},
				error:function(){}
			});
		});

			$("#editcarcomform").submit(function( event ) {
			event.preventDefault();
			var id = $('#edit_com_id').val();
			var name = $('#edit_carname').val();

			var data = {'id':id,'name':name,'_token':'{{csrf_token()}}'};
			if ($('#editcarcomform').validator('validate').has('.has-error').length === 0) {

				$.ajax({
					method:'PATCH',
					url:'carCompany/'+id,
					data:data,
					success:function(data) {
						
						$('#insert').attr('disabled',false);
						$("#editcarcom").modal('hide');
						$("#edit_carname").val('');
						switch (data.status) {
							case 200:
							compan_table.ajax.reload();
							toastr.success(data.message);
							break;
							case 500:
							toastr.error(data.message);
							break;
						}
					},
					complete:function(){},
					error:function(){}
				});
			}
		});

		$(document).on('click','.delete_company',function(){

			var id = $(this).data('id');
			$('.carcompanyDelete').attr("data-id",id);
			$('.carcompanyDelete').data("id",id);
		});

		function deleteCompany(id){ 
		var token = "{{ csrf_token() }}";
		
		swal({
			title: "Are you sure?",   
            text: "You will not be able to recover this information!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#e69a2a",   
            confirmButtonText: "Yes, delete it!",   
            cancelButtonText: "No, cancel please!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
		},
		function(isConfirm){   
			if (isConfirm) {
				$.ajax({
					headers: {
						'X-CSRF-TOKEN':  "{{ csrf_token() }}"
					},
					method:'DELETE',
					url:'carCompany/'+id,
					dataType: "json",
					success:function(data) {
						switch (data.status) {
						case 200:
						compan_table.ajax.reload();
						swal("Deleted!", "Your information has been deleted.", "success");  
						toastr.success(data.message);
						break;
						case 500:
						toastr.error(data.message);
						break;
					}
					},
				});
			} else {
				swal("Cancelled", "Your information is safe! :)", "error"); 
			}
		});
	}

	/* CODE FOR MAINTAIN CAR MODEL */

	var model_table = $('#car_Model').DataTable({
		pageLength:5,
		// processing: true,
		ajax: "data.json",
		serverSide: true,
		// sServerMethod: "POST",
		lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
			ajax: {
				type: "post",
				url: "{{ route('admin.carmodel.list') }}",
				data: function ( d ) {
					d._token = "{{ csrf_token() }}";
				},
				complete:function(){
					if( $('[data-toggle="tooltip"]').length > 0 )
						$('[data-toggle="tooltip"]').tooltip();
				}
			},
			columns:[
				{ data:'DT_RowIndex',name:'id' },
				{ data:'comname',name:'comname' },
				{ data:'model_name',name:'model_name' },
				{ data:'cartype',name:'cartype'},
				{ data:'capacity',name:'capacity' },
				{ data:'action',name:'action', orderable:false, searchable:false },
			]
		});

	
	$("#addcarModelform").submit(function( event ) {
				event.preventDefault();
			// var type = $('#type').val(); 
			// var company = $('#company').val(); 
			// var name = $('#carname').val();
			// var capacity = $('#capacity').val();

			// var data = {'type':type,'company':company,'name':name,'capacity':capacity,'_token':'{{csrf_token()}}'};

			var form = $('#addcarModelform').serialize();

			$('#insert').attr('disabled',true);
			if ($('#addcarModelform').validator('validate').has('.has-error').length === 0) {
				$.ajax({
					method:'POST',
					url:'{{ route('admin.carModel.store') }}',
					data:form,
					success:function(data) {
						
						$('#insert').attr('disabled',false);
						$("#addcarModel").modal('hide');
						$("#carname").val('');
						model_table.ajax.reload();
						switch (data.status) {
							case 200:
							toastr.success(data.message);
							break;
							case 500:
							toastr.error(data.message);
							break;
						}
					}
				});
			}
		});


		$(document).on('click',".edit_Model",function(){
			$('#editcarModelform')[0].reset();
			var id = $(this).data('id');
			$.ajax({
				method:'POST',
				url:'{{ route('admin.carmodel.edit.name') }}',
				data:{'id':id,'_token':'{{csrf_token()}}'},
				success:function(data) {
							


					$('select[name^="edittype"] option[value="'+data.data.car_type_id+'"]').attr("selected","selected");
					$('select[name^="editcompany"] option[value="'+data.data.car_company_id+'"]').attr("selected","selected");
					$('select[name^="editcapacity"] option[value="'+data.data.capacity+'"]').attr("selected","selected");

					// $('#edittype option:selected').val(data.data.car_type_id).attr('selected','selected'); 
					// $('#editcompany option:selected').val(data.data.car_company_id).attr('selected','selected');
					// $('#editcapacity option:selected').val(data.data.capacity).attr('selected','selected');
					//alert(data.data.id);

					$('#editcarname').val(data.data.model_name);
					$('#edit_Model_id').val(data.data.id);

					$('#insert').attr('disabled',false);
					$("#addcarModel").modal('hide');
					$("#carname").val('');
					switch (data.status) {
						case 200:
						$('#edit_carname').val(data.data);
						// toastr.success(data.message);
						model_table.ajax.reload();
						break;
						case 500:
						toastr.error(data.message);
						break;
					}
				},
				complete:function(){},
				error:function(){}
			});
		});


		$("#editcarModelform").submit(function( event ) {
				event.preventDefault();

			// var type = $('#edittype').val(); 
			// var company = $('#editcompany').val(); 
			// var name = $('#editcarname').val();
			// var capacity = $('#editcapacity').val();
			 var id = $('#edit_Model_id').val();
			// var data = {'edittype':type,'id':id,'editcompany':company,'editname':name,'editcapacity':capacity,'_token':'{{csrf_token()}}'};
			
			var form = $('#editcarModelform').serialize();

			$.ajax({
				method:'PATCH',
				url:'carModel/'+id,
				data:form,
				success:function(data) {
					
					$('#insert').attr('disabled',false);
					$("#editcarModel").modal('hide');
					$("#edit_carname").val('');
					switch (data.status) {
						case 200:
						model_table.ajax.reload();
						toastr.success(data.message);
						break;
						case 500:
						toastr.error(data.message);
						break;
					}
				},
				complete:function(){},
				error:function(){}
			});

		});

		function deleteModel(id){ 
		var token = "{{ csrf_token() }}";

		swal({
			title: "Are you sure?",   
            text: "You will not be able to recover this information!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#e69a2a",   
            confirmButtonText: "Yes, delete it!",   
            cancelButtonText: "No, cancel please!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
		},
		function(isConfirm){   
			if (isConfirm) {
				$.ajax({
					headers: {
						'X-CSRF-TOKEN':  "{{ csrf_token() }}"
					},
					method:'DELETE',
					url:'carModel/'+id,
					dataType: "json",
					success:function(data) {
						switch (data.status) {
						case 200:
						model_table.ajax.reload();
						swal("Deleted!", "Your information has been deleted.", "success");  
						toastr.success(data.message);
						break;
						case 500:
						toastr.error(data.message);
						break;
					}
					},
				});
			} else {
				swal("Cancelled", "Your information is safe! :)", "error"); 
			}
		});
	}

	$('#image').hide();

	/* code for maintain car type */

	<!-- code for display image before upload -->

		$(document).on('change','#file',function(){
		    var reader = new FileReader();

		    reader.onload = function (e) {
		        // get loaded data and render thumbnail.
		        $('#image').attr('src',e.target.result);
		        $('#image').attr('height','150');
		        $('#image').attr('width','150');
		    };

		    // read the image file as a data URL.
		    reader.readAsDataURL(this.files[0]);
		});

		$(document).on('change','#editfile',function(){
		
		    var reader = new FileReader();

		    reader.onload = function (e) {
		        // get loaded data and render thumbnail.
		        $('#editimage').attr('src',e.target.result);
		        $('#editimage').attr('height','150');
		        $('#editimage').attr('width','150');
		    };

		    // read the image file as a data URL.
		    reader.readAsDataURL(this.files[0]);
		});


		var type_table = $('#car_Types').DataTable({
		pageLength:5,
		// processing: true,
		ajax: "data.json",
		serverSide: true,
		// sServerMethod: "POST",
		lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
			ajax: {
				type: "post",
				url: "{{ route('admin.cartype.list') }}",
				data: function ( d ) {
					d._token = "{{ csrf_token() }}";
				},
				complete:function(){
					if( $('[data-toggle="tooltip"]').length > 0 )
						$('[data-toggle="tooltip"]').tooltip();
				}
			},
			columns:[
				{ data:'DT_RowIndex',name:'id' },
				{ data:'name',name:'name' },
				{ data:'action',name:'action', orderable:false, searchable:false },
			]
		});

	

		$("#addcartypeform").submit(function( event ) {
			event.preventDefault();
			var name = $('#carname').val();
			var formData = new FormData(this);
			if ($('#addcartypeform').validator('validate').has('.has-error').length === 0) {
				$.ajax({
					method:'POST',
					url:'{{ route('admin.carType.store') }}',
					data:formData,
		            cache:false,
		            contentType: false,
		            processData: false,
					success:function(data) {
						$('#addcartypeform')[0].reset();
						$('#insert').attr('disabled',true);			
						$('#insert').attr('disabled',false);
						$("#addcartype").modal('hide');
						$("#carname").val('');
						type_table.ajax.reload();
						switch (data.status) {
							case 200:
							toastr.success(data.message);
							break;
							case 500:
							toastr.error(data.message);
							break;
						}
					}
				});	
			}
		});


		$(document).on('click',".edit_type",function(){
			$('#editcartypeform')[0].reset();
			var id = $(this).data('id');
			$.ajax({
				method:'POST',
				url:'{{ route('admin.cartype.edit.name') }}',
				data:{'id':id,'_token':'{{csrf_token()}}'},
				success:function(data) {
					
					$('select[name^="editcompany"] option[value="'+data.company_id+'"]').attr("selected","selected");
					//var path = $('#storage_path').val();
					var photo = data.logo;
					$('#editimage').attr('src',photo);
					$('#insert').attr('disabled',false);
					$("#addcartype").modal('hide');
					$("#carname").val('');
					switch (data.status) {
						case 200:
						$('#edit_type_id').val(data.dataid);
						$('#edit_type').val(data.data);
						type_table.ajax.reload();
						break;
						case 500:
						toastr.error(data.message);
						break;
					}
				},
				complete:function(){},
				error:function(){}
			});
		});

		$("#editcartypeform").submit(function( e ) {
			e.preventDefault();
			var id = $('#edit_type_id').val();
			var form = $('#editcartypeform')[0];
            var data = new FormData(form);
			
			$.ajax({
				method:'POST',
				url:"{{route('admin.carType.update','')}}"+'/'+id,
				data:data,
				enctype:'multipart/form-data',
              	processData: false,  // Important!
              	contentType: false,
              	cache: false,
				success:function(data){
					
					$('#insert').attr('disabled',false);
					$("#editcartype").modal('hide');
					$("#edit_carname").val('');
					switch (data.status){
						case 200:
						type_table.ajax.reload();
						toastr.success(data.message);
						break;
						case 500:
						toastr.error(data.message);
						break;
					}
				},
				complete:function(){},
				error:function(){}
			});

		});

		$(document).on('click','.delete_type',function(){

			var id = $(this).data('id');
			$('.carTypeDelete').attr("data-id",id);
			$('.carTypeDelete').data("id",id);
			//$('#delete_Type').val(id);

			
		});

		function deleteType(id){ 
		var token = "{{ csrf_token() }}";
		
		swal({
			title: "Are you sure?",   
            text: "You will not be able to recover this information!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#e69a2a",   
            confirmButtonText: "Yes, delete it!",   
            cancelButtonText: "No, cancel please!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
		},
		function(isConfirm){   
			if (isConfirm) {
				$.ajax({
					headers: {
						'X-CSRF-TOKEN':  "{{ csrf_token() }}"
					},
					method:'DELETE',
					url:'carType/'+id,
					dataType: "json",
					success:function(data) {
						switch (data.status) {
						case 200:
						type_table.ajax.reload();
						swal("Deleted!", "Your information has been deleted.", "success");  
						toastr.success(data.message);
						break;
						case 500:
						toastr.error(data.message);
						break;
					}
					},
				});
			} else {
				swal("Cancelled", "Your information is safe! :)", "error"); 
			}
		});
	}

</script>
@endsection
@endsection