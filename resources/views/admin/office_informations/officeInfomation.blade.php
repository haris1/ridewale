@extends('admin.layouts.admin-app')

@section('content')

@section('css_ref')
    <!-- <link href="{{ asset('admin_assets/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/> -->
@endsection
 <!-- Main Content -->
<div class="page-wrapper">
    
    <div class="container-fluid">
				
		<!-- Title -->
		<div class="row heading-bg">
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			  <h5 class="txt-dark">Office Management</h5>
			</div>
			<!-- Breadcrumb -->
			<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
			  <ol class="breadcrumb">
				<li><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
				<li class="active"><span>Office Management</span></li>
			  </ol>
			</div>
			<!-- /Breadcrumb -->
		</div>
		<!-- /Title -->
		
		<!-- Row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default card-view settableboxadd">
					<div class="panel-heading">
						<div class="pull-left">
							<h6 class="panel-title txt-dark">Office list</h6>
						</div>
						<div class="pull-right">
							<a class="btn btn btn-success" href="{{ route('admin.office.create') }}">Add</a>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="panel-wrapper collapse in">
						<div class="panel-body">
							<div class="table-wrap">
								<div class="table-responsive users_detateblebox">
									<table id="car_company" class="table table-striped table-bordered" style="width:100%">
										<thead>
											<tr>
												<th width="7%">Sr. No</th>
												<th>City</th>
												<th>Country</th>
												<th>Postal Code</th>
												<th>Email</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</div>
		<!-- /Row -->

	</div>
    @include('admin.layouts.footer')
</div>in Content -->
<div class="modal fade" id="myModal" role="dialog">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Office Infomation</h4>
			</div>
			<div class="modal-body">
				<p><b>City : </b>data.city</p>
				<p><b>Country : </b>asd</p>
				<p><b>Postal_code : </b>asd</p>
				<p><b>Longitude : </b>asd</p>
				<p><b>Latitude : </b>asd</p>
				<p><b>Full Address : </b>asd</p>
				<p><b>Location Hours : </b>asd</p>
				<p><b>Inquiry Hours : </b>asd</p>
				<p><b>Phone : </b>asd</p>
				<p><b>Email : </b>asd</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>


@section('scripts')
<script>
	//GET DATA TABLE
	var officeTable = $('#car_company').DataTable({
		pageLength:5,
		serverSide: true,
		// sServerMethod: "POST",
		lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
		ajax: {
			type: "get",
			url: "{{ route('admin.office.list') }}",
			data: function ( d ) {
				d._token = "{{ csrf_token() }}";
			},
			complete:function(){
				if( $('[data-toggle="tooltip"]').length > 0 )
					$('[data-toggle="tooltip"]').tooltip();
			}
		},
		columns:[
			{ data:'DT_RowIndex',name:'id' },
			{ data:'city',name:'city' },
			{ data:'country',name:'country' },
			{ data:'postal_code',name:'postal_code' },
			{ data:'email',name:'email' },
			{ data:'action',name:'action', orderable:false, searchable:false },
		]
	});
	



	//DELETE OFFICE INFOMATION
	$(document).on('click','.delete_company',function(){
		var id = $(this).data('id');
		$('.carcompanyDelete').attr("data-id",id);
		$('.carcompanyDelete').data("id",id);
	});

	function deleteCompany(id){ 
		var token = "{{ csrf_token() }}";
		
		swal({
			title: "Are you sure?",   
            text: "You will not be able to recover this information!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#e69a2a",   
            confirmButtonText: "Yes, delete it!",   
            cancelButtonText: "No, cancel please!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
		},
		function(isConfirm){   
			if (isConfirm) {
				$.ajax({
					headers: {
						'X-CSRF-TOKEN':  "{{ csrf_token() }}"
					},
					data:{id:id},
					type:'DELETE',
					url:'{{ route('admin.office.delete') }}',
					dataType: "json",
					success:function(data) {
						switch (data.status) {
						case 200:
						officeTable.ajax.reload();
						swal("Deleted!", "Your information has been deleted.", "success");  
						toastr.success(data.message);
						break;
						case 500:
						toastr.error(data.message);
						break;
					}
					},
				});
			} else {
				swal("Cancelled", "Your information is safe! :)", "error"); 
			}
		});
	}

	//VIEW OFFICE INFOMATION
	$("body").delegate("a[data-action='view']","click", function() {
		var id=$(this).attr('data-id');
		$.ajax({
			headers: {
				'X-CSRF-TOKEN':  "{{ csrf_token() }}"
			},
			//data:{id:id},
			type:'get',
			url:'{{ route('admin.office.edit',"/")}}'+'/'+id,
			dataType: "json",
			success:function(data) {
				//$('#myModal').modal('show');
				alert('hello');
			},
		});
	});
</script>
@endsection
@endsection