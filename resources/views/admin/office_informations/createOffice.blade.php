@extends('admin.layouts.admin-app')

@section('content')

@section('css_ref')
    <!-- <link href="{{ asset('admin_assets/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/> -->
@endsection
 <!-- Main Content -->
<div class="page-wrapper">
    
    <div class="container-fluid">
				
				<!-- Title -->
				<div class="row heading-bg">
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
					  <h5 class="txt-dark">Create Office Infomation</h5>
					</div>
					<!-- Breadcrumb -->
					<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
					  <ol class="breadcrumb">
						<li><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
						<li class="active"><span>Office Infomation</span></li>
					  </ol>
					</div>
					<!-- /Breadcrumb -->
				</div>
				<!-- /Title -->
				
				<!-- Row -->
				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default card-view settableboxadd">
							<div class="panel-heading">
								<div class="pull-left">
									<h6 class="panel-title txt-dark">Office list</h6>
								</div>
								<div class="pull-right">
									<a href="{{ route('admin.officeInformations.index') }}" class="btn btn btn-danger">Back</a>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div class="form-wrap">
										<form id="addOfficeInfomation" data-toggle="validator" method="post">
											@csrf
											<div class="form-group">
												<label class="control-label mb-10 title" for="carname">City</label>
												<input type="text" id="city" name="city" class="form-control" placeholder="City Name" data-error="City Title is required !" Autofocus required="">
												<div class="help-block with-errors"></div>
											</div>
											<div class="form-group">
												<label class="control-label mb-10 title" for="carname">Country</label>
												<input type="text" id="country" name="country" class="form-control" placeholder="Country Name" data-error="Country Title is required !" Autofocus required="">
												<div class="help-block with-errors"></div>
											</div>
										
											<div class="form-group">
												<label class="control-label mb-10 title" for="carname">Postal Code</label>
												<input type="text" id="postal_code" name="postal_code" class="form-control" placeholder="Postal Code" data-error="Postal Code is required !" Autofocus required="">
												<div class="help-block with-errors"></div>
											</div>
											<div class="form-group">
												<label class="control-label mb-10 title" for="carname">Longitude</label>
												<input type="text" id="longitude" name="longitude" class="form-control" placeholder="Longitude" data-error="Longitude is required !" Autofocus required="">
												<div class="help-block with-errors"></div>
											</div>
											<div class="form-group">
												<label class="control-label mb-10 title" for="carname">Latitude</label>
												<input type="text" id="latitude" name="latitude" class="form-control" placeholder="Latitude" data-error="latitude is required !" Autofocus required="">
												<div class="help-block with-errors"></div>
											</div>
											<div class="form-group">
												<label class="control-label mb-10 title" for="carname">Full Address</label>
												<textarea id="full_address" name="full_address" class="form-control" placeholder="Full Address" data-error="Full Address is required !" Autofocus required=""></textarea>
												<div class="help-block with-errors"></div>
											</div>
											<div class="form-group">
												<label class="control-label mb-10 title" for="carname">Location Hours (Mon-Fri : 9AM - 7PM)</label>
												<div class="row">
													<div class="col-md-3">
														<input type="text" id="open_day" name="open_day" class="form-control" placeholder="Open Days (Mon)" data-error="Open Days is required !" Autofocus required="">
														<div class="help-block with-errors"></div>
														
													</div>
													<div class="col-md-3">
														<input type="text" id="close_day" name="close_day" class="form-control" placeholder="Close Days (Fri)" data-error="Close Days is required !" Autofocus required="">
														<div class="help-block with-errors"></div>

													</div>
													<div class="col-md-3">
														<input type="text" id="open_time" name="open_time" class="form-control" placeholder="Open Time (9AM)" data-error="Open Time is required !" Autofocus required="">
														<div class="help-block with-errors"></div>
														
													</div>
													<div class="col-md-3">
														<input type="text" id="close_time" name="close_time" class="form-control" placeholder="Close Time (7PM)" data-error="Close Time is required !" Autofocus required="">
														<div class="help-block with-errors"></div>
													</div>
												</div>

											</div>
											<div class="form-group">
												<label class="control-label mb-10 title" for="carname">Inquiry Hours (Mon-Fri : 9AM - 7PM)</label>
												<div class="row">
													<div class="col-md-3">
														<input type="text" id="inquiry_open_day" name="inquiry_open_day" class="form-control" placeholder="Open Days (Mon)" data-error="Open Days is required !" Autofocus required="">
														<div class="help-block with-errors"></div>
													</div>
													<div class="col-md-3">
														<input type="text" id="inquiry_close_day" name="inquiry_close_day" class="form-control" placeholder="Close Days (Fri)" data-error="Close Days is required !" Autofocus required="">
														<div class="help-block with-errors"></div>
													</div>
													<div class="col-md-3">
														<input type="text" id="inquiry_open_time" name="inquiry_open_time" class="form-control" placeholder="Open Time (9AM)" data-error="Open Time is required !" Autofocus required="">
														<div class="help-block with-errors"></div>
													</div>
													<div class="col-md-3">
														<input type="text" id="inquiry_close_time" name="inquiry_close_time" class="form-control" placeholder="Close Time (7PM)" data-error="Close Time is required !" Autofocus required="">
														<div class="help-block with-errors"></div>
													</div>
												</div>

											</div>
											<div class="form-group">
												<label class="control-label mb-10 title">Phone</label>
												<input type="text" id="phone" name="phone" class="form-control" placeholder="Phone Number" data-error="Phone Number is required !" Autofocus required="">
												<div class="help-block with-errors"></div>
											</div>
											<div class="form-group">
												<label class="control-label mb-10 title">Email</label>
												<input type="text" id="email" name="email" class="form-control" placeholder="Email" data-error="Email id is required !" Autofocus required="">
												<div class="help-block with-errors"></div>
											</div>									
											<div class="form-group text-right">
												<button type="submit" class="btn btn-success mr-10 btn-sm">Submit</button>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>	
					</div>
				</div>
				<!-- /Row -->

			</div>
    @include('admin.layouts.footer')
</div>
<!-- /Main Content -->





@section('scripts')
<script>
	//ADD OFFICE INFOMATION
	$("#addOfficeInfomation").submit(function(event) {
		event.preventDefault();
		var data=$( "#addOfficeInfomation" ).serialize();
		if ($('#addOfficeInfomation').validator('validate').has('.has-error').length === 0) {
			$.ajax({
				type:'POST',
				url:'{{ route('admin.office.add') }}',
				data: data,
				success:function(data) {
					switch (data.status) {
						case 200:
						toastr.success(data.message);
						$('#addOfficeInfomation input , #addOfficeInfomation textarea').val('');
						break;
						case 500:
						toastr.error(data.message);
						break;
					}
				}
			});
		}
	});
</script>
@endsection
@endsection