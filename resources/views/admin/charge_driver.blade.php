@extends('admin.layouts.admin-app')
<?php //die(); ?>
@section('content') 
<div class="page-wrapper">
    <!-- Main Content -->
    <div class="container-fluid pt-25">
        <div class="back_button">
            <a href="{{route('admin.trip.details',$trip_id)}}"><button class="btn btn btn-success">Back</button></a>
        </div>
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
              <h5 class="txt-dark">Charge Driver</h5>
            </div>
          <!-- Breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
                    <li><a href="{{route('admin.trip.details',$trip_id)}}">Trip Detail</a></li>
                    <li class="active"><span>Charge Driver</span></li>
                </ol>
            </div>
            <!-- /Breadcrumb -->
        </div>
    <!--  -->
   
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default card-view BoxDesignCoverInerCla1">
                    <div class="panel-heading">
                        <div class="pull-left">
                            
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="form-wrap">
                                <form  method="POST" id="refundForBookingFrm" data-toggle="validator">
                                    <div class="colmd12fullwidth">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label mb-10 text-left" for="example-email"><span class="help">Amount</span></label>
                                                <input type="text" id="amount" name="amount" class="form-control" placeholder="Amount" required=""  value="">
                                                <input type="hidden" name="trip_id" id="trip_id" value="{{$trip_id}}">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="colmd12fullwidth2">
                                        <button type="submit" id="Update" data-loading-text="<i class='fa fa-spinner'></i>" class="btnsetm">Charge</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Row -->
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">

    

    /*code for add driver address*/

    $('#refundForBookingFrm').submit(function(e){
        e.preventDefault();
        form=$('#refundForBookingFrm');
        var formData = new FormData(form[0]);       
        var id = $('#trip_id').val();

        if ($('#refundForBookingFrm').validator('validate').has('.has-error').length === 0) {

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method:'POST',
                url:"{{route('admin.trip.tripCharge','')}}/"+id,
                data:formData,
                cache:false,
                processData: false,
                contentType: false,
                 beforeSend: function(){
                   $('#update').button('loading');
                },
                success:function(data) {
                    switch (data.status) {
                        case true:
                        toastr.success(data.message);
                        location.replace('{{route('admin.trip.details',$trip_id)}}');
                        break;
                        case false:
                        toastr.error(data.message);
                        break;
                    }
                },
                complete: function(){
                  $('#update').button('reset');
                }
            });
        }
    });
</script>
@endsection

