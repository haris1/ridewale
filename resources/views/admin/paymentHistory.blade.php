@extends('admin.layouts.admin-app')

@section('content')

@section('css_ref')
    <link href="{{ asset('admin_assets/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection
 <!-- Main Content -->
<div class="page-wrapper">
    
    <div class="container-fluid">
				<!-- Title -->
				<div class="row heading-bg">
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
					  <h5 class="txt-dark">Payment History</h5>
					</div>
					<!-- Breadcrumb -->
					<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
					  <ol class="breadcrumb">
						<li><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
						<li class="active"><span>Payment History</span></li>
					  </ol>
					</div>
					<!-- /Breadcrumb -->
				</div>
				<!-- /Title -->
				
				<!-- Row -->
				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default card-view settableboxadd">
							<div class="panel-heading">
								<div class="pull-left">
									<!-- <h6 class="panel-title txt-dark">Select Filter</h6> -->
								</div>
								
								<div class="clearfix"></div>
							</div>
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<form >
										<div class="col-lg-4">
											<div class="form-group mb-0">
												<label class="control-label mb-10 text-left">Payment Date</label>
												<input class="form-control input-daterange-datepicker" type="text" name="started_at" id="started_at" value="{{date('M d, Y',strtotime('-30days'))}} - {{date(' M d, Y',strtotime('+1 days')) }}">
											</div>
										</div>
										<!-- <div class="col-lg-4">
											<div class="form-group mb-0">
												<label class="control-label mb-10 text-left">Completed Between</label>
												<input class="form-control input-daterange-datepicker" type="text" name="completed_at" id="completed_at" value="{{date('M d, Y',strtotime('-30days'))}} - {{date('M d, Y',strtotime('+1 days')) }}">
											</div>
										</div> -->
										<div class="col-lg-4">
											<div class="form-group mt-30">
												<button type="button" id="filter" class="btn btn-success mr-10">Filter</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>	
					</div>
				</div>
				<!-- /Row -->

				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default card-view settableboxadd">
							<div class="panel-heading">
								<div class="pull-left">
									<h6 class="panel-title txt-dark">Payment List</h6>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div class="table-wrap">
										<div class="table-responsive">
											<table id="paymet_history" class="table  table-striped table-bordered mb-0 alltextbtnandiconset1 iconsize_set pb-30" >
												<thead>
													<tr>
														<th>Sr. No</th>
														<th>User</th>
														<th>Trip From</th>
														<th>Trip To</th>
														<th>Seats</th>
														<th>Admin Fare</th>
														<th>Driver Fare</th>
														<!-- <th>Cashback</th> -->
														<th>Total</th>
													</tr>
												</thead>
												<tbody>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>	
					</div>
				</div>
				<!-- /Row -->
			</div>
    @include('admin.layouts.footer')
</div>
	
@section('scripts')
<script>

var table=$('#paymet_history').DataTable({
dom: 'Bfrtip',
// buttons: [
// 'copy', 'csv', 'excel', 'pdf', 'print'
// ],
"serverSide": true,
"order"      : [[ 0, 'desc' ]],
"sServerMethod": "GET",	
"lengthMenu": [[10, 50, 100, -1], [10, 50, 100, "All"]],
"ajax": {
url: "{{route('admin.payment.history')}}",
data: function ( d ) {
d._token = "{{ csrf_token() }}";
d.started_at=$('#started_at').val();
d.completed_at=$('#completed_at').val();
}
},
"columns":[
{ data:'DT_RowIndex',name:'id' },
{ data:'user_first_name',name:'user_first_name' },
{ data:'from',name:'from' },
{ data:'to',name:'to' },
{ data:'seats',name:'seats' },
{ data:'admin_fees',name:'admin_fees' },
{ data:'driver_fees',name:'driver_fees' },
// { data:'cash_back',name:'Cashback' },
{ data:'total',name:'total' },
// { data:'action',name:'action', orderable:false, searchable:false },
]
});
$('#filter').click(function(){
table.draw(); 
});
$(function() {
var start = moment().subtract(30, 'days');
var end = moment();
function cb(start, end) {
$('input[name=started_at],input[name=completed_at]').val(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
}
$('input[name=started_at],input[name=completed_at]').daterangepicker({
startDate: start,
endDate: end,
autoUpdateInput:true,
autoApply: true,
locale: {
format: 'MMM D, YYYY',
cancelLabel: 'Clear'
},
ranges: {
'Today': [moment(), moment()],
'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
'Last 7 Days': [moment().subtract(6, 'days'), moment()],
'Last 30 Days': [moment().subtract(29, 'days'), moment()],
'This Month': [moment().startOf('month'), moment().endOf('month')],
'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
}
},cb);
cb(start, end);
});
</script>

@endsection
@endsection
