<div class="col-md-12">
   	<div class="form-group">
		<label class="control-label mb-10 text-left"> Select Document Type</label>
		<select class="form-control" id="editdocument_name" name="editdocument_name" required="">
			<option value="">Select Document</option>
			@foreach($document_datas as $document_data)
			<option value="{{$document_data['id']}}" @if($data[0]->document_id == $document_data['id']) {{'selected'}} @endif >{{ucfirst(str_replace('_', ' ', $document_data['name']))}}</option>
			@endforeach
		</select>
	</div>
</div>
<div class="col-md-12">
   <div class="form-group">
		<label class="control-label mb-10 text-left">Expiry Date</label>
		<div class='input-group date' id='datetimepicker2'>
			<input type='text' name="datetimepicker2" class="form-control datetimepicker2"/>
			@php
				$date = $newDateTime = date('m/d/Y h:i A', strtotime($data[0]['expiry_date']));
				
			@endphp
			<input type="hidden" name="exp_date_val" id="exp_date_val" value="{{$date}}">
			<span class="input-group-addon">
				<span class="fa fa-calendar"></span>
			</span>
		</div>
	</div>
</div>
  <div class="col-md-12">
    <div class="form-group">
        <label class="control-label mb-10 text-left">Upload Document</label>
		<div class="fileinput fileinput-new input-group" data-provides="fileinput">
			<div class="form-control" data-trigger="fileinput"> <!-- <i class="glyphicon glyphicon-file fileinput-exists"></i> --> <span class="fileinput-filename"></span></div>
			<span class="input-group-addon fileupload btn btn-info btn-anim btn-file"><i class="fa fa-upload"></i> <span class="fileinput-new btn-text">Select file</span> <span class="fileinput-exists btn-text">Change</span>
			<input type="file" name="car_documents[]">
			</span> <a href="#" class="input-group-addon btn btn-danger btn-anim fileinput-exists" data-dismiss="fileinput"><i class="fa fa-trash"></i><span class="btn-text"> Remove</span></a>
        <input type="hidden" name="car_id" value="">
        <input type="hidden" name="storage_path" id="storage_path" value="{{storage_path()}}">
        <div class="help-block with-errors"></div>
    </div>
</div>
</div>
<div class="col-md-12">
    <div class="append_edit_images">
        @foreach($data as $dat)
            <img src="{{asset('storage/driver_car/')}}/{{$dat->name}}" class="set_css" height="150" width="150" alt="image">
        @endforeach
    </div>
</div>
</div>