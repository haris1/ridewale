@extends('admin.layouts.admin-app')

@section('content')
<div class="page-wrapper">
    <!-- Main Content -->
    <div class="container-fluid pt-25">
        <div class="back_button">
            <a href="{{route('admin.users.index')}}"><button class="btn btn btn-success">Back</button></a>
        </div>
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
              <h5 class="txt-dark">Document List</h5>
            </div>
          <!-- Breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
                    <li><a href="{{route('admin.users.index')}}">Users List</a></li>
                    <li class="active"><span>Driver Profile</span></li>
                </ol>
            </div>
            <!-- /Breadcrumb -->
        </div>

    <!--  -->
           <div class="row">
            <div class="col-sm-12">

                <div class="vehicle_list">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="widthfullcolap">
                                        <a href="#collapse_driver" data-toggle="collapse" data-parent="#accordion"  class="DD_list">
                                            <span class="lefthedtitl">Driver Documents</span>  @if(isset($DDstatus) && $DDstatus > 0)<p class="document_error"> {{$DDstatus}} <span>missing</span> </p> @else <p class="document_success"> <span>Up-to-date</span></p> @endif   
                                        </a>
                                    </div>
                                </div>
                            <div id="collapse_driver" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="Driver_documents">

                                    @foreach($Driverdocuments as $key => $Driverdocument)
                                    <?php $i = 0; $Dkey = "";  $Did = $Driverdocument['id']; ?>
                                    <div class="Document_list">
                                         <div class="card">
                                            <div class="deleteBtnboxright">
                                                <h3>{{$Driverdocument['name']}}</h3>
                                                @foreach($usedDD_doc as $key => $usedDD_docs)

                                                    @if($usedDD_docs['document_id']==$Driverdocument['id']) 

                                                    <?php $i = 1; $Dkey = $key;  ?>

                                                    @endif
                                                
                                                    @endforeach

                                                    @if($i == 1)

                                                    <button name="delete_document" id="delete_document" data-id="@if(isset($usedDD_doc[$Dkey]['id'])){{$usedDD_doc[$Dkey]['id']}}@endif" onclick="delete_doc('{{$usedDD_doc[$Dkey]['id']}}')"> Delete</button>
                                                    <input type="hidden" name="docid_delete" id="docid_delete" value="@if(isset($usedDD_docs['id'])){{$usedDD_docs['id']}}@endif">
                                                   @else 

                                                    <span class="missing_class">Document Missing</span>

                                                    @endif
                                            </div>
                                            
                                            <form method="post" id="driver_document_{{$Did}}" enctype="multipart/form-data" data-toggle="validator">
                                                @csrf
                                                <input type="hidden" name="userid" value="{{$data[0]->id}}">
                                                <input type="hidden" name="docid" value="{{$Driverdocument['id']}}">
                                                <input type="hidden" name="type" value="{{'driver'}}">
                                            
                                                <div class="form-group">
                                                    <label class="control-label mb-10 text-left"></label>
                                                    @if(isset($usedDD_doc[$Dkey]['name']) && $usedDD_doc[$Dkey]['document_id'] == $Driverdocument['id'])
                                                       
                                                        @php

                                                            $image_names = explode(',',$usedDD_doc[$Dkey]['name']); 
                                                            $Dcount = count($image_names);

                                                        @endphp
                                                        @foreach($image_names as $image_name)
                                                        
                                                        <a class="fancybox" rel="group" href="@if(file_exists(storage_path().'/app/public/documents/driver_'.$id.'/'.$image_name)){{asset('storage/documents')}}/driver_{{$id}}/{{$image_name}}@else {{asset('admin_assets/img/logo2.png')}} @endif">
                                                            <img src="@if(file_exists(storage_path().'/app/public/documents/driver_'.$id.'/'.$image_name)){{asset('storage/documents')}}/driver_{{$id}}/{{$image_name}}@else {{asset('admin_assets/img/logo2.png')}} @endif" class="file_driver_input_{{$Driverdocument['id']}}"  height="150" width="150">
                                                        </a>

                                                        @endforeach
                                                         @if($usedDD_doc[$Dkey]['document_id'] == 1 && $usedDD_doc[$Dkey]['back_photo'] != '')

                                                         <a class="fancybox" rel="group" href="@if(file_exists(storage_path().'/app/public/documents/driver_'.$id.'/'.$usedDD_doc[$Dkey]['back_photo'])){{asset('storage/documents')}}/driver_{{$id}}/{{$usedDD_doc[$Dkey]['back_photo']}}@else {{asset('admin_assets/img/logo2.png')}} @endif">
                                                            <img src="@if(file_exists(storage_path().'/app/public/documents/driver_'.$id.'/'.$usedDD_doc[$Dkey]['back_photo'])){{asset('storage/documents')}}/driver_{{$id}}/{{$usedDD_doc[$Dkey]['back_photo']}}@else {{asset('admin_assets/img/logo2.png')}} @endif" class="file_driver_input_{{$Driverdocument['id']}}"  height="150" width="150">
                                                        </a>
                                                        @endif
                                                    @else
                                                    
                                                    
                                                    @if($Driverdocument['id'] == '1')
                                                    <div class="col-md-12" id="Driver_uploaded_images_licence_front"></div>

                                                    <!-- <img src="" class="file_driver_input_{{$Driverdocument['id']}} fancybox" height="150" width="150" alt="" style="display: none;" /> -->
                                                    <label class="control-label mb-10 text-left">Front Photo</label>
                                                    <input type="file" data-id="{{$Driverdocument['id']}}" name="file_{{$Driverdocument['id']}}" id="filef" class="file_driver_input" onchange="licance_multiple_upload('Driver_uploaded_images_licence_front','{{$Driverdocument['id']}}',this,)" accept="image/*" required />
                                                    <div class="help-block with-errors"></div>
                                                    <div class="col-md-12" id="Driver_uploaded_images_licence_back"></div>
                                                    
                                                    <label class="control-label mb-10 text-left">Back Photo</label>
                                                    <input type="file" data-id="{{$Driverdocument['id']}}" name="file_back_{{$Driverdocument['id']}}" id="fileb" class="file_driver_input" onchange="licance_multiple_upload('Driver_uploaded_images_licence_back','{{$Driverdocument['id']}}',this)" accept="image/*" required />
                                                    <div class="help-block with-errors"></div>
                                                    @else
                                                    <div class="Driver_uploaded_images_{{$Driverdocument['id']}}"></div>
                                                    <img src="" class="file_driver_input_{{$Driverdocument['id']}} fancybox" height="150" width="150" alt="" style="display: none;">
                                                    <input type="file" data-id="{{$Driverdocument['id']}}" name="file_{{$Driverdocument['id']}}[]" id="file" class="file_driver_input" onchange="driver_multiple_upload('Driver_uploaded_images_{{$Driverdocument['id']}}','{{$Driverdocument['id']}}',this)" accept="image/*" multiple required>
                                                    <div class="help-block with-errors"></div>
                                                     @endif
                                                    @endif
                                                    
                                                    
                                                    
                                                </div>
                                                <div class="ExpiryDateBoxSet">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10 text-left">Expiry Date</label>
                                                        <div class='input-group date' id='expiry_date_driver_{{$key}}' data-date="@if(isset($usedDD_doc[$Dkey]['expiry_date'])) {{date('m/d/Y H:i A', strtotime($usedDD_doc[$Dkey]['expiry_date']))}}@else {{date('m/d/Y H:i A')}} @endif" data-date-format="yyyy-mm-dd">
                                                            <input type='text' name="datetimepicker" class="form-control"  value="@if(isset($usedDD_doc[$Dkey]['expiry_date'])) {{date('m/d/Y H:i A', strtotime($usedDD_doc[$Dkey]['expiry_date']))}}@else {{date('m/d/Y H:i A')}} @endif" required />
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
                                                        </div>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div>
                                                @if(!isset($usedDD_doc[$Dkey]['id']))
                                                <div class="submit_button">
                                                    <input type="button" id="button_{{$Did}}" data-id="@if(isset($usedCD_docs['id'])) {{$usedDD_docs['id']}} @endif" onclick="submitdata('driver_document_{{$Did}}',{{$Did}})" class="" name="submit" value="Save" data-loading-text="Processing.....">
                                                </div>
                                                @endif
                                            </form>
                                        </div>
                                        </br>
                                    </div>
                                    @endforeach
                                </div>
                                </div>
                            </div>
                        </div>

                        @foreach($cars as $key => $car)

                        <?php $car_count = 0; $Ckey = $key; $akey = '';  ?>
                        @foreach($usedCD_doc as $bkey => $usedCD)
                            @foreach($usedCD->documents as $gkey => $checkid)
                                @if($car->id == $checkid->ownable_id)
                                   
                             <?php $car_count++;?>

                                @endif
                                                                
                            @endforeach
                        @endforeach    

                        <?php $total_missing = $countCD - $car_count; ?>

                        <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="widthfullcolap">
                                          <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{$key}}" class="DD_list">
                                            <span class="lefthedtitl">For Vehical {{$car->car_model->model_name}}</span>  @if(isset($car_count) && $total_missing > 0)<p class="document_error"> {{$total_missing}} <span> missing </span> </p> @else <p class="document_success">Up-to-date</p> @endif
                                        </a>
                                    </div>
                                </div>

                            <div id="collapse{{$key}}" class="panel-collapse collapse">
                                <div class="panel-body">
                                   <div class="Car_documents_append">
                                    @foreach($Cardocuments as $key => $Cardocument)
                                     <?php $i = 0; $Ckey = ""; $Cid = $Cardocument['id']; ?>
                                    <div class="Document_list">
                                         <div class="card">
                                            <div class="deleteBtnboxright">
                                                <h3>{{$Cardocument['name']}}</h3>

                                                <?php $k=0; $akey=$key;  ?>

                                                @foreach($usedCD_doc[0]['documents'] as $key => $usedCD_doc1)
                                                    
                                                    @if(isset($usedCD_doc1) && $Cardocument['id']== $usedCD_doc1->document_id && $usedCD_doc1->ownable_id == $car->id)
                                                        
                                                        <?php
                                                           $i = 1;
                                                           $akey=$key;
                                                           $Ckey = $usedCD_doc1->id;
                                                        ?>
                                                    @endif
                                                    <?php
                                                        //$i++;
                                                    ?>
                                                @endforeach
                                                    @if($i==1)
                                                    
                                                        <button name="delete_document" id="delete_document" data-id="@if(isset($Ckey)){{$Ckey}}@endif" onclick="delete_doc('{{$Ckey}}')"> Delete</button>
                                                    
                                                    @else

                                                        <span class="missing_class">Document Missing</span>

                                                    @endif
                                            </div>
                                            
                                            <form method="post" id="car_document_{{$car->id}}_{{$Cid}}" enctype="multipart/form-data" data-toggle="validator">
                                                @csrf
                                                <input type="hidden" name="userid" value="{{$data[0]->id}}">
                                                <input type="hidden" name="car_docid" value="{{$Cid}}">
                                                <input type="hidden" name="type" value="{{'App\CarDriver'}}">
                                                <input type="hidden" name="car_id" value="{{$car->id}}">
                                                
                                                <div class="form-group">
                                                    <label class="control-label mb-10 text-left"></label>
                                                    @if($i == 1 > 0 && $Cardocument['id'])
                                                        
                                                        
                                                        @php
                                                            $image_Cnames = explode(',',$usedCD_doc[0]->documents[$akey]['name']); 
                                                            $Ccount = count($image_Cnames);

                                                        @endphp
                                                        @foreach($image_Cnames as $image_name)
                                                            @if(!empty($image_name))
                                                                <a class="fancybox" rel="group" href="@if(file_exists(storage_path().'/app/public/documents/car_'.$car->id.'/'.$image_name)){{asset('storage/documents')}}/car_{{$car->id}}/{{$image_name}}@else {{asset('admin_assets/img/logo2.png')}} @endif">
                                                                    <img src="@if(file_exists(storage_path().'/app/public/documents/car_'.$car->id.'/'.$image_name)){{asset('storage/documents')}}/car_{{$car->id}}/{{$image_name}}@else {{asset('admin_assets/img/logo2.png')}} @endif" class="file_car_input_{{$Cardocument['id']}}"  height="150" width="150">
                                                                </a>
                                                            @endif
                                                        @endforeach
                                                    @else
                                                        <a class="fancybox" rel="group" href="">
                                                            <img src="" class="file_car_input_{{$Cardocument['id']}}" height="150" width="150" alt="" style="display: none;">
                                                        </a>
                                                        <div class="car_uploaded_images_{{$car->id}}_{{$Cardocument['id']}} col-md-12"></div>
                                                        <input type="file" name="file_{{$car->id}}_{{$Cardocument['id']}}[]" id="file" class="file_car_input_{{$Cardocument['id']}}" data-name="file_car_input_{{$Cardocument['id']}}" data-id="{{$Cardocument['id']}}"  onchange="multiple_upload('car_uploaded_images_{{$car->id}}_{{$Cardocument['id']}}','{{$Cardocument['id']}}',this)" accept="image/*" multiple>
                                                    @endif
                                                    
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                                
                                                <div class="ExpiryDateBoxSet">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10 text-left">Expiry Date</label>
                                                        <div class='input-group date' id='expiry_date_car_{{$key}}'  data-date="@if(isset($usedCD_doc[0]->documents[$akey]['expiry_date'])) {{ date('m/d/Y H:i A', strtotime($usedCD_doc[0]->documents[$akey]['expiry_date'])) }} @ else {{ date('m/d/Y H:i A') }} @endif" data-date-format="yyyy-mm-dd">
                                                            <input type='text' name="datetimepicker" class="form-control"  value="@if(isset($usedCD_doc[0]->documents[$akey]['expiry_date'])) {{ date('m/d/Y H:i A', strtotime($usedCD_doc[0]->documents[$akey]['expiry_date'])) }} @ else {{ date('m/d/Y H:i A') }} @endif"/>
                                                            
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
                                                        </div>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div>
                                                @if(!isset($usedCD_doc[$Ckey]['id']))
                                                <div class="submit_button">
                                                    <input type="button" id="button_{{$car->id}}" data-id="@if(isset($usedCD_docs['id'])) {{$usedCD_docs['id']}} @endif" onclick="submitdata('car_document_{{$car->id}}_{{$Cid}}',{{$car->id}})" class="" name="submit" value="Save" data-loading-text="Processing.....">
                                                </div>
                                                @endif
                                            </form>
                                        </div>
                                        </br>
                                    </div>
                                    @endforeach
                                </div>
                                </div>
                            </div>
                        </div>
                        @endforeach

                    </div> 
                </div>

            </div>
        </div>
        <!-- /Row -->
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">

        var i;
        var countCD = '{{$countCD}}';
        var countDD = '{{$countDD}}';

       $(".fancybox").fancybox();

       /* code for display image */


      function multiple_upload(div,id,input){
            if (input.files) {
                var filesAmount = input.files.length;
                
                $('.'+div).html('');
                for (i = 0; i <= filesAmount; i++) {
                    var reader = new FileReader();

                    reader.onload = function(event){

                        var image = '<div class="col-md-3"><a class="fancybox"><img src="'+event.target.result+'" height="150" width="150" class="Car_upload_img"></a></div>';
                        //$($.parseHTML('<img>')).attr('src', event.target.result).height('150').width('150').To('.'+div);
                        
                    $('.'+div).append(image);
                    
                    // var src = $('.Car_upload_img').attr('src');
                    // $(".fancybox").attr("href",src);
                    }
                    
                    //$('a.fancybox').attr("href",src);
                    reader.readAsDataURL(input.files[i]);
                }
            }
      }

      function driver_multiple_upload(div,id,input){
            if (input.files) {
                var filesAmount = input.files.length;
                    $('.'+div).html('');
                for (C = 1; C <= filesAmount; C++) {
                    var reader = new FileReader();

                    reader.onload = function(event) {

                        var image = '<div class="image"><a class="fancybox"><img src="'+event.target.result+'" height="150" width="150" class="Car_upload_img"></a></div>';
                        //$($.parseHTML('<img>')).attr('src', event.target.result).height('150').width('150').To('.'+div);
                        
                        $('.'+div).append(image);
                    
                    // var src = $('.Car_upload_img').attr('src');
                    // $(".fancybox").attr("href",src);
                    }
                    
                    //$('a.fancybox').attr("href",src);
                    reader.readAsDataURL(input.files[C]);
                }
            }
      }

      function licance_multiple_upload(div,id,input){
        
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
              var image = '<div class="col-md-3"><a class="fancybox"><img src="'+e.target.result+'" height="150" width="150" class="Car_upload_img"></a></div>';

               $('#'+div).append(image);
            }

            reader.readAsDataURL(input.files[0]);
          }

      }

    
        /*code for image preview */
        
        $(document).on('change',".file_car_input",function(){
            var id = $(this).data('id');
            readURL(this,id);  
        });
        

        function readURL(input,id) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                
                    $('.file_car_input_'+id).attr('src', e.target.result);
                    $('.fancybox').attr('href', e.target.result);
                    $('.file_car_input_'+id).show();
                    
                    //$('.user_update_image_append').show();
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        /*driver image preview*/
        $(document).on('change',".file_driver_input",function(){
            var id = $(this).data('id');
            readURLS(this,id);  
        });
        

        function readURLS(input,id) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {

                    $('.file_driver_input_'+id).attr('src', e.target.result);
                    $('.file_driver_input_'+id).show();
                    
                    //$('.user_update_image_append').show();
                }
                reader.readAsDataURL(input.files[0]);
            }
        }


        for (i = 0; i <= countCD; i++) { 
            
            $('#expiry_date_car_'+i+',#expiry_date_car_'+i).datetimepicker({
                useCurrent: false,
                icons: {
                        time: "fa fa-clock-o",
                        date: "fa fa-calendar",
                        up: "fa fa-arrow-up",
                        down: "fa fa-arrow-down"
                    },
                format: 'MM-DD-YYYY',
                sideBySide: true,
                minDate:new Date()
            }).on('dp.show', function() {
            if($(this).data("DateTimePicker").date() === null)
                $(this).data("DateTimePicker").date(moment());
            });

            $('#expiry_date_car_'+i).datetimepicker({
                    useCurrent: true,
                    icons: {
                            time: "fa fa-clock-o",
                            date: "fa fa-calendar",
                            up: "fa fa-arrow-up",
                            down: "fa fa-arrow-down"
                        },
                    format: 'MM-DD-YYYY',
                sideBySide: true,
                minDate:new Date()
                }).on('dp.show', function() {
                if($(this).data("DateTimePicker").date() === null)
                    $(this).data("DateTimePicker").date(moment());
            });  

            $('#expiry_date_driver_'+i+',#expiry_date_driver_'+i).datetimepicker({
                useCurrent: false,
                icons: {
                        time: "fa fa-clock-o",
                        date: "fa fa-calendar",
                        up: "fa fa-arrow-up",
                        down: "fa fa-arrow-down"
                    },
                format: 'MM-DD-YYYY',
                sideBySide: true,
                minDate:new Date()
            }).on('dp.show', function() {
            if($(this).data("DateTimePicker").date() === null)
                $(this).data("DateTimePicker").date(moment());
            });

            $('#expiry_date_driver_'+i).datetimepicker({
                    useCurrent: true,
                    icons: {
                            time: "fa fa-clock-o",
                            date: "fa fa-calendar",
                            up: "fa fa-arrow-up",
                            down: "fa fa-arrow-down"
                        },
                    format: 'MM-DD-YYYY',
                sideBySide: true,
                minDate:new Date()
                }).on('dp.show', function() {
            if($(this).data("DateTimePicker").date() === null)
                $(this).data("DateTimePicker").date(moment());
            });
        }
  
        
    

function submitdata(form,id){
    form=$('#'+form+'');
    
    var formData = new FormData(form[0]);       
    // var formData = new FormData($(this)[0]);
    console.log(formData)
    
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method:'POST',
            url:"{{route('admin.submitDocuments')}}",
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            beforeSend: function(){
               $('#button_'+id).button('loading');
            },
            success:function(data) {
                switch (data.status) {
                    case true:
                    toastr.success(data.message);
                    $('.append_driver_images').html('');
                    location.reload();
                    break;
                    case false:
                    toastr.error(data.message);
                    break;
                }
            },
            complete: function(){
              $('#button_'+id).button('reset');
            }
        });
}

    //$(document).on('click','#delete_document',function(){
        //var id = $(this).data('id');
        function delete_doc(id){
            
        swal({
            title: "Are you sure?",   
            text: "You will not be able to recover this information!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#e69a2a",   
            confirmButtonText: "Yes, delete it!",   
            cancelButtonText: "No, cancel please!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        },
        function(isConfirm){   
            if (isConfirm) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN':  "{{ csrf_token() }}"
                    },
                    method:'GET',
                    url:"{{route('admin.deleteDocuments','')}}/"+id,
                    dataType: "json",
                    success:function(data) {
                        switch (data.status) {
                        case true:
                        swal("Deleted!", "Your Document has been deleted.", "success");  
                        location.reload();
                        toastr.success(data.message);
                        break;
                        case false:
                        toastr.error(data.message);
                        break;
                    }
                    },
                });
            } else {
                swal("Cancelled", "Your Document is safe! :)", "error"); 
            }
        })
    }
    //});


</script>
@endsection

