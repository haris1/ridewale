@extends('admin.layouts.admin-app')

@section('content')

@section('css_ref')
    <link href="{{ asset('admin_assets/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection
 <!-- Main Content -->
<div class="page-wrapper">
    
    <div class="container-fluid">
				<!-- Title -->
				<div class="row heading-bg">
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
					  <h5 class="txt-dark">Manage Document</h5>
					</div>
					<!-- Breadcrumb -->
					<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
					  <ol class="breadcrumb">
						<li><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
						<li class="active"><span>Manage Document</span></li>
					  </ol>
					</div>
					<!-- /Breadcrumb -->
				</div>
				<!-- /Title -->

				 <button type="button" style="float: right;" id="filter" class="btn btn-success mr-10" data-turbolinks-action="replace" data-toggle="modal" data-target="#addDocument">Upload document</button>

								<!-- Product Row One -->
				<div class="row set_document_path">
					
					@if(count($Used_document_data) == 0)
						<div class="col-md-12">
							<div class="col-md-3"></div>
							<div class="col-md-6">
								<h5><b> No Records Found. </b></h5>
							</div>
							<div class="col-md-3"></div>
						</div>

					@else
					@foreach($Used_document_data as $Used_document)
       				<div class="col-lg-3 col-md-4 col-sm-4 col-xs-6">
       					<div class="col-lg-12">
       						<!-- <div class="form-group mt-30">
       							<button type="button" id="filter" class="btn btn-success mr-10" data-turbolinks-action="replace" data-toggle="modal" data-target="#addDocument">Upload document</button>
       						</div> -->
       					</div>
						<div class="panel panel-default card-view pa-0">
							<div class="panel-wrapper collapse in">
								<div class="panel-body pa-0">
									<article class="col-item">
										<div class="photo">
											<div class="options">
												<a href="javascript:void(0);" data-id="{{$Used_document['id']}}" class="font-18 txt-grey mr-10 pull-left edit_document"><i class="zmdi zmdi-edit"></i></a>
												<a href="javascript:void(0);" onclick="delete_document({{$Used_document['id']}})" class="font-18 txt-grey pull-left sa-warning"><i class="zmdi zmdi-close"></i></a>
											</div>
											@if(file_exists(storage_path().'/app/public/documents/'.$Used_document['name']))
											@php
												$get_ext = explode('.',$Used_document['name']);
											@endphp
												@if($get_ext[1] == 'pdf') 
												<a data-fancybox-type="iframe" class="fancy_pdf"  href="@if(!empty($Used_document['name'])){{asset('storage/documents')}}/{{$Used_document['name']}} @else {{ asset('admin_assets/images/empty-doc.png') }} @endif"> 
													<object data="{{asset('storage/documents')}}/{{$Used_document['name']}}" type="application/pdf" width="300" height="200">
														<a href="{{asset('storage/documents')}}/{{$Used_document['name']}}">{{$Used_document['name']}}</a>
													</object>
												</a>
												@else
												<a data-fancybox="preview" href="@if(!empty($Used_document['name'])){{asset('storage/documents')}}/{{$Used_document['name']}} @else {{ asset('admin_assets/images/empty-doc.png') }} @endif"> 
													<img src="@if(!empty($Used_document['name'])){{asset('storage/documents')}}/{{$Used_document['name']}} @else {{ asset('admin_assets/images/empty-doc.png') }} @endif" class="img-responsive" style="margin: 32px 0px 32px 13px;" alt="Product Image" /> 
												</a>
												@endif
												@else
													<img src="{{ asset('admin_assets/images/empty-doc.png') }}" class="img-responsive" style="margin: 32px 0px 32px 13px;" alt="Image" /> 
											@endif
										</div>
										<div class="info">
											<div class="exp_date_lbl">Expiry Date</div>
											<div class="exp_date">{{$Used_document['expiry_date']}}</div>
											<!-- <div class="product-rating inline-block">
												<a href="javascript:void(0);" class="font-12 txt-success zmdi zmdi-star mr-0"></a><a href="javascript:void(0);" class="font-12 txt-success zmdi zmdi-star mr-0"></a><a href="javascript:void(0);" class="font-12 txt-success zmdi zmdi-star mr-0"></a><a href="javascript:void(0);" class="font-12 txt-success zmdi zmdi-star mr-0"></a><a href="javascript:void(0);" class="font-12 txt-success zmdi zmdi-star-outline mr-0"></a>
											</div> -->
											<!-- <span class="head-font block text-warning font-16">$150</span> -->
										</div>
									</article>
								</div>
							</div>	
						</div>	
					</div>
					@endforeach	
					@endif
								
				</div>	
				<!-- /Product Row Four -->
			    <!-- Category Card -->
			</div>
			<!--  Code for assign car to a driver popup -->


    @include('admin.layouts.footer')
</div>
	
	<div class="modal fade in" id="addDocument" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h5 class="modal-title" id="exampleModalLabel1"> Add Document </h5>
            </div>
            <form id="addDocumentform" method="POST" enctype="multipart/form-data" data-toggle="validator">
                @csrf
                {{ method_field('POST') }}
                <div class="modal-body">
                	<div class="col-md-12">
                       	<div class="form-group">
							<label class="control-label mb-10 text-left"> Select Document Type</label>
							<select class="form-control" id="document_name" name="document_name" required="">
								<option value="">Select Company</option>
								@foreach($document_datas as $document_data)
								<option value="{{$document_data['id']}}">{{ucfirst(str_replace('_', ' ', $document_data['name']))}}</option>
								@endforeach
							</select>
						</div>
                    </div>
                    <div class="col-md-12">
                       	<div class="form-group">
							<label class="control-label mb-10 text-left"> Select Type</label>
							<select class="form-control" id="user_type" name="user_type" required="">
								<!-- <option value="">Select Type</option> -->
								<option value="car">Car</option>
								<!-- <option value="driver">Driver</option> -->
							</select>
						</div>
                    </div>
                    <div class="col-md-12">
                       <div class="form-group">
							<label class="control-label mb-10 text-left">Expiry Date</label>
							<div class='input-group date' id='expiry_date'>
								<input type='text' name="datetimepicker1" class="form-control" />
								<span class="input-group-addon">
									<span class="fa fa-calendar"></span>
								</span>
							</div>
						</div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                        	<label class="control-label mb-10 text-left">Upload Document</label>
                        	<div class="fileinput fileinput-new input-group" data-provides="fileinput">
                        		<div class="form-control" data-trigger="fileinput">
<!--                         			<i class="glyphicon glyphicon-file fileinput-exists"></i> -->
                        			<span class="fileinput-filename"></span>
                        		</div>
                        		<span class="input-group-addon fileupload btn btn-info btn-anim btn-file">
                        			<!-- <i class="fa fa-upload"></i> -->
                        			<span class="fileinput-new btn-text">Select Documents</span>
                        			<span class="fileinput-exists btn-text">Change</span>
                        			<input type="file" id="car_documents" name="car_documents[]">
                        		</span>
                        		<a href="#" class="input-group-addon btn btn-danger btn-anim fileinput-exists" data-dismiss="fileinput">
                        			<!-- <i class="fa fa-trash"></i> -->
                        			<span class="btn-text"> Remove</span>
                        		</a>
                        		<input type="hidden" name="car_id" value="{{$id}}">
                        		<div class="help-block with-errors"></div>
	                        </div>
	                    </div>
	                </div>
	                <div class="col-md-12">
					    <div class="append_images">
					       
					    </div>
					</div>
	            </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" id="insert" data-loading-text="<i class='fa fa-spinner spin'></i>" class="btn btn-success mr-10 btn-sm">ADD</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- code forf edit dcument  -->
<div class="modal fade in" id="EditDocument" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h5 class="modal-title" id="exampleModalLabel1"> Edit Document </h5>
            </div>
            <form id="editDocumentform" method="POST" enctype="multipart/form-data" data-toggle="validator">
                @csrf
                {{ method_field('PUT') }}
                <div class="modal-body" id="append_document_data">
                	<div class="col-md-12">
					   	<div class="form-group">
							<label class="control-label mb-10 text-left"> Select Document Type</label>
							<select class="form-control" id="editdocument_name" name="editdocument_name" required="">
								<option value="">Select Document</option>
								@foreach($document_datas as $document_data)
								<option value="{{$document_data['id']}}">{{ucfirst(str_replace('_', ' ', $document_data['name']))}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-12">
                       	<div class="form-group">
							<label class="control-label mb-10 text-left"> Select Type</label>
							<select class="form-control" id="edit_user_type" name="edit_user_type" required="">
								<!-- <option value="">Select Type</option> -->
								<option value="car">Car</option>
								<!-- <option value="driver">Driver</option> -->
							</select>
						</div>
                    </div>
					<div class="col-md-12">
					   <div class="form-group">
							<label class="control-label mb-10 text-left">Expiry Date</label>
							<div class='input-group date' id='edit_expiry_date'>
								<input type='text' name="datetimepicker1" class="form-control"/>
								<input type="hidden" name="datetimepicker1">
								<span class="input-group-addon">
									<span class="fa fa-calendar"></span>
								</span>
							</div>
						</div>
					</div>
					  <div class="col-md-12">
					    <div class="form-group">
					        <label class="control-label mb-10 text-left">Upload Document</label>
							<div class="fileinput fileinput-new input-group" data-provides="fileinput">
								<div class="form-control" data-trigger="fileinput">
<!-- 								 <i class="glyphicon glyphicon-file fileinput-exists"></i> -->
								  <span class="fileinput-filename"></span></div>
								<span class="input-group-addon fileupload btn btn-info btn-anim btn-file">
									<!-- <i class="fa fa-upload"></i> -->
									 <span class="fileinput-new btn-text">Select file</span> <span class="fileinput-exists btn-text">Change</span>
								<input type="file" id="edit_car_documents" name="car_documents[]">
								</span> <a href="#" class="input-group-addon btn btn-danger btn-anim fileinput-exists" data-dismiss="fileinput">
									<!-- <i class="fa fa-trash"></i> -->
									<span class="btn-text"> Remove</span></a>
					        <input type="hidden" name="edit_car_id" value="">
					        <input type="hidden" name="edit_id" id="edit_id" value="">
					        <input type="hidden" name="storage_path" id="storage_path" value="{{storage_path()}}">
					        <div class="help-block with-errors"></div>
					    </div>
					</div>
					</div>
					<div class="col-md-12">
					    <div class="edit_append_images">
					       
					    </div>
					</div>
					</div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" id="update" data-loading-text="<i class='fa fa-spinner spin'></i>" class="btn btn-success mr-10 btn-sm">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>

@section('scripts')
<script>

	<!-- code for fancy box -->

	// $(document).ready(function(event) {
 //        $('.fancybox').fancybox();
 //    });

 	// $(document).on('click','.fancy_pdf',function(){
 	// 	$(".fancy_pdf").fancybox({
	 //        type: "iframe",
	 //    }).trigger("click");
 	// });
 	$(".fancy_pdf").fancybox({
 		// openEffect: 'elastic',
 		// closeEffect: 'elastic',
 		// autoSize: true,
 		type: 'iframe',
 		iframe: {
			preload: false // fixes issue with iframe and IE
		}
 	});
    
	var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $($.parseHTML('<img>')).attr('src', event.target.result).attr('class', 'set_css').attr('height', '150').attr('width','150').appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };

    $('#car_documents').on('change', function() {
        $('.append_images').html('');
        imagesPreview(this, 'div.append_images');
    });

    $('#edit_car_documents').on('change', function() {
        $('.edit_append_images').html('');
        imagesPreview(this, 'div.edit_append_images');
    });


$('#expiry_date,#edit_expiry_date').datetimepicker({
		useCurrent: false,
		icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
	}).on('dp.show', function() {
	if($(this).data("DateTimePicker").date() === null)
		$(this).data("DateTimePicker").date(moment());
});

$('#expiry_date').datetimepicker({
		useCurrent: false,
		icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
	}).on('dp.show', function() {
	if($(this).data("DateTimePicker").date() === null)
		$(this).data("DateTimePicker").date(moment());
});

$('#addDocumentform').submit(function(e){
    e.preventDefault();

    form=$('#addDocumentform');
    var formData = new FormData(form[0]);       
    // var formData = new FormData($(this)[0]);
    
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method:'POST',
            url:"{{route('admin.manageDocuments.store')}}",
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            beforeSend: function(){
               $('#insert').button('loading');
            },
            success:function(data) {
                switch (data.status) {
                    case true:
                    toastr.success(data.message);
                    $('.append_images').html('');
                    location.reload();
                    break;
                    case false:
                    toastr.error(data.message);
                    break;
                   
                }
            },
            complete: function(){
              $('#insert').button('reset');
            }
        });
    });

	
	<!-- code for update documents -->
$('#EditDocument').submit(function(e){
    e.preventDefault();
    var id = $('#edit_id').val();
    // form=$('#editDocumentform');
    // var formData = new FormData(form[0]);  

    form=$('#editDocumentform');
    var formData = new FormData(form[0]);         
    // var formData = new FormData($(this)[0]);
    
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method:'POST',
            url:"{{route('admin.manageDocuments.update','')}}/"+id,
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            beforeSend: function(){
               $('#update').button('loading');
            },
            success:function(data) {
                switch (data.status) {
                    case true:
                    toastr.success(data.message);
                    $('.edit_append_images').html('');
                    location.reload();
                    break;
                    case false:
                    toastr.error(data.message);
                    break;
                   
                }
            },
            complete: function(){
              $('#update').button('reset');
            }
        });
    });


	$(document).on('click',".edit_document",function(){
		var id = $(this).data('id');
		$('#edit_id').val(id);
		
		$.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
	        method:'GET',
	        url:'{{ route('admin.manageDocuments.show','')}}/'+id,
	        success:function(data) {
	            console.log(data.data[0].doc_name);
	            $('select[name^="editdocument_name"] option[value="'+data.data[0].document_id+'"]').attr("selected","selected");
	            $('select[name^="edit_user_type"] option[value="'+data.data[0].ownable_type+'"]').attr("selected","selected");

	            $("#EditDocument").modal('show');
	        	
	        	var path = $('#storage_path').val();
	        	var name = data.data[0].name;

	        	var full_path = path+'/'+name;

	        	var append = '<img src="'+full_path+'" height="150" width="150" >';

	        	$('.append_edit_images').html("");
	        	$('.append_edit_images').html(append);

	        	$('input[name="datetimepicker1"]').val(data.data[0].expiry_date);
	   //      	var date = new Date(data.data[0].expiry_date);
				// var newDate = date.toString('mm/dd/yy');
	            //$('#append_document_data').html(data);
	//             $('#editdatetimepicker1').datetimepicker({
				//     date: new Date($('#exp_date_val').val())
				// });
	           // $('#editdatetimepicker1').val();
	        },
        });
	});

	function delete_document(id){ 
		var token = "{{ csrf_token() }}";

		swal({
			title: "Are you sure?",   
            text: "You will not be able to recover this information!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#e69a2a",   
            confirmButtonText: "Yes, delete it!",   
            cancelButtonText: "No, cancel please!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
		},
		function(isConfirm){   
			if (isConfirm) {
				$.ajax({
					headers: {
						'X-CSRF-TOKEN':  "{{ csrf_token() }}"
					},
					method:'DELETE',
					url:"{{route('admin.manageDocuments.destroy','')}}/"+id,
					dataType: "json",
					success:function(data) {
						switch (data.status) {
						case true:
						swal("Deleted!", "Your Document has been deleted.", "success");  
						location.reload();
						toastr.success(data.message);
						break;
						case false:
						toastr.error(data.message);
						break;
						
					}
					},
				});
			} else {
				swal("Cancelled", "Your Document is safe! :)", "error"); 
			}
		});
	}

</script>

@endsection
@endsection
