@extends('admin.layouts.admin-app')

@section('content')

@section('css_ref')
    <link href="{{ asset('admin_assets/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection
 <!-- Main Content -->
<div class="page-wrapper">
    
    <div class="container-fluid">
				<!-- Title -->
				<div class="row heading-bg">
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
					  <h5 class="txt-dark">Car Types List</h5>
					</div>
					<!-- Breadcrumb -->
					<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
					  <ol class="breadcrumb">
						<li><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
						<li class="active"><span>Car Types List</span></li>
					  </ol>
					</div>
					<!-- /Breadcrumb -->
				</div>
				<!-- /Title -->
				
				<!-- Row -->
				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default card-view settableboxadd">
							<div class="panel-heading">
								<div class="pull-left">
									<h6 class="panel-title txt-dark"></h6>
								</div>
								<div class="pull-right">
									<a class="btn btn-success" data-toggle="modal" data-target="#addcartype" data-original-title="Add">Add</a>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div class="table-wrap">
										<div class="table-responsive">
											<table id="car_Types" class="table  table-striped table-bordered mb-0 alltextbtnandiconset1 iconsize_set pb-30" >
												<thead>
													<tr>
														<th>Sr. No</th>
														<th>Type Name</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>	
					</div>
				</div>
				<!-- /Row -->
			</div>
    @include('admin.layouts.footer')
</div>
<!-- /Main Content -->

<!-- modal for add new car Type -->
	 <!-- <div class="modal fade modelsetin_newdesignall1" id="addcartype" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content taxCategoryboxsetiner">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Add Car Types</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form method="POST" action="" aria-label="{{ __('Add Car Type') }}" id="addcartype">
						@csrf
						<div class="row">
							<div class="form-group col-md-12 col-sm-12 col-xs-12">
								<label class="control-label mb-10 text-left" for="title">Car Type Name</label>
								<input type="text" id="carname" name="carname" class="form-control" placeholder="Car Type Name" data-error="Tax Category Title is required !">
								<div class="help-block with-errors"></div>
								@if ($errors->has('title'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('title') }}</strong>
								</span>
								@endif
							</div>
							<div class="clearfix"></div>
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="allbtnsetinupcl1">
									<a type="button" id="insert" class="btn btn-success mr-10 btn-sm">Submit</a>
									<a type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</a>
								</div>
							</div>

						</div>
					</form>
				</div>
			</div>
		</div>
	</div> -->

	<div class="modal fade in" id="addcartype" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Add Type </h5>
			</div>
			<form id="addcartypeform" enctype="multipart/form-data" data-toggle="validator">
				@csrf
				<div class="modal-body">
					<div class="form-group">
						<label class="control-label mb-10 text-left"> Select Company</label>
						<select class="form-control" name="company" id="company" required="">
							<option value="">Select Company</option>
							@foreach($companies as $company)
							<option value="{{$company['id']}}">{{ucfirst($company['name'])}}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<label class="control-label mb-10 text-left" for="title">Car Type Name</label>
						<input type="text" id="carname" name="carname" class="form-control" placeholder="Car Type Name" data-error="Tax Category Title is required !" required="">
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
						<label class="control-label mb-10 text-left" for="title">Car Type Icon</label>
						<input type="file" id="file" name="file" class="form-control" placeholder="Car Type Icon"></br>
						<img id="image" height="150" width="150"/>
						<div class="help-block with-errors"></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
					<button type="submit" id="insert" class="btn btn-success mr-10 btn-sm">Add</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!--  Edit new car Type modal -->
	<div class="modal fade in" id="editcartype" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Edit Car Type</h5>
			</div>
			<form id="editcartypeform" method="PUT" enctype="multipart/form-data" data-toggle="validator">
				@csrf
				{{ method_field('PUT') }}
				<div class="modal-body" id="edit_type_model_data">
					<div class="form-group">
						<label class="control-label mb-10 text-left"> Select Company</label>
						<select class="form-control" name="editcompany" id="editcompany" required="">
							<option value="">Select Company</option>
							@foreach($companies as $company)
							<option value="{{$company['id']}}">{{ucfirst($company['name'])}}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<label class="control-label mb-10 text-left" for="title_edit">Car Type Name</label>
						<input type="text" id="edit_carname" name="edit_carname"  class="form-control" placeholder="Car Type Name" data-error="Car Type Name is required !" required="">
						<input type="hidden" name="edit_type_id" id="edit_type_id">
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
						<label class="control-label mb-10 text-left" for="title">Car Type Icon</label>
						<input type="file" id="editfile" name="editfile" class="form-control" placeholder="Car Type Icon"></br>
						<img id="editimage" height="150" width="150" />
						<div class="help-block with-errors"></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-success mr-10 btn-sm">Update</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- end of edit modal -->
@section('scripts')
<script>

	$('#image').hide();
		
	// 	$(document).on('shown.bs.modal','#editcartype', function(e) {
	// 	var url = '{{ route("admin.carType.edit", ":id") }}';
	// 	url = url.replace(':id', $(e.relatedTarget).data('id'));
	// 	$.ajax({
	// 		headers: {
	// 			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	// 		},
	// 		method:'GET',
	// 		url:url,
	// 		dataType: "html",
	// 		success:function(data) {
	// 			alert(data);
	// 			$('#edit_type_model_data').html(data);
	// 		},
	// 	});
	// 	// $("#city").val($(e.relatedTarget).data('name'));
	// 	// $("#work_country_id").val($(e.relatedTarget).data('country'));
	// 			$('#editcartype').modal('show');
	// });




		<!-- code for display image before upload -->

		$(document).on('change','#file',function(){
		    var reader = new FileReader();

		    reader.onload = function (e) {
		        // get loaded data and render thumbnail.
		        $('#image').attr('src',e.target.result);
		        $('#image').attr('height','150');
		        $('#image').attr('width','150');
		    };

		    // read the image file as a data URL.
		    reader.readAsDataURL(this.files[0]);
		});

		$(document).on('change','#editfile',function(){
		
		    var reader = new FileReader();

		    reader.onload = function (e) {
		        // get loaded data and render thumbnail.
		        $('#editimage').attr('src',e.target.result);
		        $('#editimage').attr('height','150');
		        $('#editimage').attr('width','150');
		        //$('#editimage').attr('src',e.target.result);
		        //document.getElementById("image").src = e.target.result;
		    };

		    // read the image file as a data URL.
		    reader.readAsDataURL(this.files[0]);
		});


		var table = $('#car_Types').DataTable({
		pageLength:10,
		// processing: true,
		ajax: "data.json",
		serverSide: true,
		// sServerMethod: "POST",
		lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
			ajax: {
				type: "post",
				url: "{{ route('admin.cartype.list') }}",
				data: function ( d ) {
					d._token = "{{ csrf_token() }}";
				},
				complete:function(){
					if( $('[data-toggle="tooltip"]').length > 0 )
						$('[data-toggle="tooltip"]').tooltip();
				}
			},
			columns:[
				{ data:'DT_RowIndex',name:'id' },
				{ data:'name',name:'name' },
				{ data:'action',name:'action', orderable:false, searchable:false },
			]
		});

	

	$("#addcartypeform").submit(function( event ) {
			event.preventDefault();
			$('#insert').attr('disabled',true);
			var name = $('#carname').val();
			 var formData = new FormData(this);
			if ($('#addcartypeform').validator('validate').has('.has-error').length === 0) {
				$.ajax({
					method:'POST',
					url:'{{ route('admin.carType.store') }}',
					data:formData,
		            cache:false,
		            contentType: false,
		            processData: false,
					success:function(data) {
						
						$('#insert').attr('disabled',false);
						$("#addcartype").modal('hide');
						$("#carname").val('');
						table.ajax.reload();
						switch (data.status) {
							case 200:
							toastr.success(data.message);
							break;
							case 500:
							toastr.error(data.message);
							break;
							
						}
					}
				});	
			}
		});


		$(document).on('click',".edit_type",function(){
			$('#editcartypeform')[0].reset();
			var id = $(this).data('id');
			$.ajax({
				method:'POST',
				url:'{{ route('admin.cartype.edit.name') }}',
				data:{'id':id,'_token':'{{csrf_token()}}'},
				success:function(data) {
					
					$('select[name^="editcompany"] option[value="'+data.company_id+'"]').attr("selected","selected");
					var path = $('#storage_path').val();
					var photo = path+'/images/type_logo/'+data.logo;
					$('#editimage').attr('src',photo);
					$('#insert').attr('disabled',false);
					$("#addcartype").modal('hide');
					$("#carname").val('');
					switch (data.status) {
						case 200:
						$('#edit_type_id').val(data.dataid);
						$('#edit_carname').val(data.data);
						table.ajax.reload();
						break;
						case 500:
						toastr.error(data.message);
						break;
						
					}
				},
				complete:function(){},
				error:function(){}
			});
		});

		$("#editcartypeform").submit(function( e ) {
			e.preventDefault();
			var id = $('#edit_type_id').val();
			var form = $('#editcartypeform')[0];
            var data = new FormData(form);
			
			$.ajax({
				method:'POST',
				url:"{{route('admin.carType.update','')}}"+'/'+id,
				data:data,
				enctype:'multipart/form-data',
              	processData: false,  // Important!
              	contentType: false,
              	cache: false,
				success:function(data){
					
					$('#insert').attr('disabled',false);
					$("#editcartype").modal('hide');
					$("#edit_carname").val('');
					switch (data.status){
						case 200:
						table.ajax.reload();
						toastr.success(data.message);
						break;
						case 500:
						toastr.error(data.message);
						break;
						
					}
				},
				complete:function(){},
				error:function(){}
			});

		});

		$(document).on('click','.delete_type',function(){

			var id = $(this).data('id');
			$('.carTypeDelete').attr("data-id",id);
			$('.carTypeDelete').data("id",id);
			//$('#delete_Type').val(id);

			
		});

		function deleteType(id){ 
		var token = "{{ csrf_token() }}";
		
		swal({
			title: "Are you sure?",   
            text: "You will not be able to recover this information!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#e69a2a",   
            confirmButtonText: "Yes, delete it!",   
            cancelButtonText: "No, cancel please!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
		},
		function(isConfirm){   
			if (isConfirm) {
				$.ajax({
					headers: {
						'X-CSRF-TOKEN':  "{{ csrf_token() }}"
					},
					method:'DELETE',
					url:'carType/'+id,
					dataType: "json",
					success:function(data) {
						switch (data.status) {
						case 200:
						table.ajax.reload();
						swal("Deleted!", "Your information has been deleted.", "success");  
						toastr.success(data.message);
						break;
						case 500:
						toastr.error(data.message);
						break;
						
					}
					},
				});
			} else {
				swal("Cancelled", "Your information is safe! :)", "error"); 
			}
		});
	}
</script>
@endsection
@endsection