<div class="form-group">
	<label for="recipient-name" class="control-label mb-10 title">Location:</label>
	<input type="text" name="name" class="form-control" required="" value="{{$location->name}}" id="locationsrh">
	<input type="hidden" name="place_id" value="{{$location->place_id}}" id="place_id1">
	<input type="hidden" name="latitude" value="{{$location->latitude}}" id="latitude1">
	<input type="hidden" required placeholder="City" name="city" value="{{$location->city}}" id="city1">
	<input type="hidden" required placeholder="Country" name="country" value=""{{$location->country}}" id="country1">
	<input type="hidden" name="longitude" value="{{$location->longitude}}" id="longitude1">
	<input type="hidden" required placeholder="Postal Code" name="postalCode" value="{{$location->postalCode}}" id="postalCode1">
	<input type="hidden" required placeholder="State" name="state" value="{{$location->state}}" id="state1">
	<input type="hidden" required placeholder="Street" name="street" value="{{$location->street}}" id="street1">
	<input type="hidden" placeholder="Place Name" name="placeName" value="{{$location->placeName}}" id="placeName1">
	<div class="help-block with-errors"></div>
</div>
<div class="form-group">
	<label for="recipient-name" class="control-label mb-10 title">Country:</label>
	<select name="work_country_id"  required="" id="work_country_idedit" class="form-control">
		<option value="">Select Country</option>
		@foreach($countries as $country)
		<option value="{{$country->id}}" @if($location->work_country_id == $country->id) {{'selected'}} @endif>{{$country->name}}</option>
		@endforeach
	</select>
	<div class="help-block with-errors"></div>
</div>
<div class="form-group">
	<label for="recipient-name" class="control-label mb-10 title">City:</label>
	<select name="work_city_id" id="work_city_idedit" required=""  class="form-control">
		<option value="">Select City</option>
		@foreach($cities as $city)
		<option value="{{$city->id}}" @if($location->work_city_id == $city->id) {{'selected'}} @endif>{{$city->name}}</option>
		@endforeach
	</select>
	<div class="help-block with-errors"></div>
</div>
<input type="hidden" name="id" id="location_id" value="{{$location->id}}">
