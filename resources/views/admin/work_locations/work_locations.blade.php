@extends('admin.layouts.admin-app')

@section('content')

@section('css_ref')
<link href="{{ asset('admin_assets/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
<style type="text/css">
.pac-container {
	background-color: #FFF;
	z-index: 1051;
	position: fixed;
	display: inline-block;
	float: left;
}
.modal{
	z-index: 20;   
}
.modal-backdrop{
	z-index: 10;        
	}​
</style>
@endsection
<!-- Main Content -->
<div class="page-wrapper">

	<div class="container-fluid">

		<!-- Title -->
		<div class="row heading-bg">
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
				<h5 class="txt-dark">Work Locations List</h5>
			</div>
			<!-- Breadcrumb -->
			<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
				<ol class="breadcrumb">
					<li><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
					<li class="active"><span>Work Locations List</span></li>
				</ol>
			</div>
			<!-- /Breadcrumb -->
		</div>
		<!-- /Title -->
<!-- 						<input type="text" name="name" id="locationsrch" class="form-control" required="">
-->
<!-- Row -->
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default card-view settableboxadd">
			<div class="panel-heading">
				<div class="pull-left">
					<h6 class="panel-title txt-dark"></h6>
				</div>
				<div class="pull-right">
					<button class="btn btn-success" data-toggle="modal" data-target="#locationAdd_modal" data-title="Add"><span class="btn-text">Add</span></button>								
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="panel-wrapper collapse in">
				<div class="panel-body">
					<div class="table-wrap">
						<div class="table-responsive">
							<table id="workLocations" class="table  table-striped table-bordered mb-0 alltextbtnandiconset1 iconsize_set pb-30" >
								<thead>
									<tr>
										<th>Sr. No</th>
										<th>Location</th>
										<th>Country</th>
										<th>City</th>
										<th>Action</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<th>Sr. No</th>
										<th>Location</th>
										<th>Country</th>
										<th>City</th>
										<th>Action</th>
									</tr>
								</tfoot>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>
<!-- /Row -->
</div>

@include('admin.layouts.footer')
</div>
<div class="modal fade in" id="locationAdd_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Add Work City</h5>
			</div>
			<form id="addLocationFrm" data-toggle="validator">
				<div class="modal-body">
					<div class="form-group">
						<label for="recipient-name" class="control-label mb-10 title">Location:</label>
						<input type="text" name="name" id="locationsrch" class="form-control" required="">
						<input type="hidden" placeholder="Place id" name="place_id" value="{{ old('place_id') }}" id="place_id">
						<input type="hidden" name="latitude" value="{{ old('latitude') }}" id="latitude">
						<input type="hidden" required placeholder="City" name="city" value="{{ old('city') }}" id="city">
						<input type="hidden" required placeholder="Country" name="country" value="{{ old('country') }}" id="country">
						<input type="hidden" name="longitude" value="{{ old('longitude') }}" id="longitude">
						<input type="hidden" required placeholder="Postal Code" name="postalCode" value="{{ old('postalCode') }}" id="postalCode">
						<input type="hidden" required placeholder="State" name="state" value="{{ old('state') }}" id="state">
						<input type="hidden" required placeholder="Street" name="street" value="{{ old('street') }}" id="street">
						<input type="hidden" placeholder="Place Name" name="placeName" value="{{ old('placeName') }}" id="placeName">
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
						<label for="recipient-name" class="control-label mb-10 title">Country:</label>
						<select name="work_country_id" id="work_country_id" required=""  class="form-control">
							<option value="">Select Country</option>
							@foreach($countries as $country)
							<option value="{{$country->id}}">{{$country->name}}</option>
							@endforeach
						</select>
						<div class="form-group">
							<label for="recipient-name" class="control-label mb-10 title">City:</label>
							<select name="work_city_id" id="work_city_id" required=""  class="form-control">
								<option value=""></option>
							</select>
							<div class="help-block with-errors"></div>
						</div>
						<div class="help-block with-errors"></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Add</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade in" id="locationEdit_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Edit Work Location</h5>
			</div>
			<form id="editLocationFrm" data-toggle="validator">
<!-- 					<input type="text" name="name" class="form-control" required="" value="" id="locationsrh">
-->
<div class="modal-body" id="editContent">

</div>
<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	<button type="submit" class="btn btn-primary">Update</button>
</div>
</form>
</div>
</div>
</div>
<!-- /Main Content -->
@section('scripts')
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAOzSrdzRrerVtmmGbMY1vvSWr-TobQr-U&libraries=places&callback=initialize"
></script>
<script>
	$('#workLocations').DataTable({
		pageLength:10,
		// processing: true,
		serverSide: true,
		// sServerMethod: "POST",
		lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
		ajax: {
			type: "post",
			url: "{{ route('admin.work_locations.work_locations') }}",
			data: function ( d ) {
				d._token = "{{ csrf_token() }}";
			},
			complete:function(){
				if( $('[data-toggle="tooltip"]').length > 0 )
					$('[data-toggle="tooltip"]').tooltip();
			}
		},
		columns:[
		{ data:'DT_RowIndex',name:'id' },
		{ data:'name',name:'name' },
		{ data:'country',name:'country' },
		{ data:'city',name:'city' },
		{ data:'action',name:'action', orderable:false, searchable:false },
		]
	});

	$( "#addLocationFrm" ).submit(function( event ) {
		event.preventDefault();
		var data=$( "#addLocationFrm" ).serialize();
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:'POST',
			url:'{{route('admin.work_locations.store')}}',
			data:data,
			dataType: "json",
			success:function(data) {
				switch (data.status) {
					case true:
					toastr.success(data.message);
					location.reload();
					break;
					case false:
					toastr.warning(data.message);
					break;
					default:
					toastr.error("You are not Authorized to access this page");
					break;
				}
			},
		});
	});

	$(document).on('shown.bs.modal','#locationEdit_modal', function(e) {
		var url = '{{ route("admin.work_locations.edit", ":id") }}';
		url = url.replace(':id', $(e.relatedTarget).data('id'));
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:'GET',
			url:url,
			dataType: "html",
			success:function(data) {
				//alert(data);
				$('#editContent').html(data);
				initialize1();
			},
		});
		// $("#city").val($(e.relatedTarget).data('name'));
		// $("#work_country_id").val($(e.relatedTarget).data('country'));
	});


	$("#work_country_id").change(function(e){
		e.preventDefault();
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:'GET',
			url:'{{route('admin.work_locations.get_cities_by_country','')}}/'+$(this).val(),
			dataType: "html",
			success:function(data) {
				$('#work_city_id').html(data);
			},
		});
	});
	$(document).on('change', '#work_country_idedit', function(e) {
		e.preventDefault();
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:'GET',
			url:'{{route('admin.work_locations.get_cities_by_country','')}}/'+$(this).val(),
			dataType: "html",
			success:function(data) {
				$('#work_city_idedit').html(data);
			},
		});
	});

	$( "#editLocationFrm" ).submit(function( event ) {
		event.preventDefault();
		var data=$( "#editLocationFrm" ).serialize();
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:'PUT',
			url:'{{route('admin.work_locations.update','')}}/'+$("#location_id").val(),
			data:data,
			dataType: "json",
			success:function(data) {
				switch (data.status) {
					case true:
					toastr.success(data.message);
					location.reload();
					break;
					case false:
					toastr.warning(data.message);
					break;
					default:
					toastr.error("You are not Authorized to access this page");
					break;
				}
			},
		});
	});

	function deleteLocation(id){ 
		var token = "{{ csrf_token() }}";
		
		swal({
			title: "Are you sure?",   
            text: "You will not be able to recover this information!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#e69a2a",   
            confirmButtonText: "Yes, delete it!",   
            cancelButtonText: "No, cancel please!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
		},
		function(isConfirm){   
			if (isConfirm) {
				$.ajax({
					headers: {
						'X-CSRF-TOKEN':  "{{ csrf_token() }}"
					},
					method:'DELETE',
					url:'{{route('admin.work_locations.destroy','')}}/'+id,
					dataType: "json",
					success:function(data) {
						switch (data.status) {
						case 200:
						table.ajax.reload();
						swal("Deleted!", "Your information has been deleted.", "success");  
						toastr.success(data.message);
						break;
						case 500:
						toastr.error(data.message);
						break;
						default:
						toastr.error("You are not Authorized to access this page");
						break;
					}
					},
				});
			} else {
				swal("Cancelled", "Your information is safe! :)", "error"); 
			}
		});
	}

	function initialize() {
        //static coordinates
        var coordinates = {lat: 42.345573, lng: -71.098326};

        //simple map instance
        var map = new google.maps.Map(document.getElementById('locationsrch'), {
        	center: coordinates,
        	zoom: 14,
            streetViewControl: false,   // it hides the street view control (i.e. person icon) from map
        });

        // Set up the markers on the map
        var marker = new google.maps.Marker({
        	map: map,
        	center: coordinates,
        	draggable: false,
        	animation: google.maps.Animation.DROP,
        });
        marker.setVisible(true);

        //simple street view
        // setStreetViewMethod(map,coordinates);  

        //set the autocomplete
        var input = document.getElementById('locationsrch');
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);
        autocomplete.setFields(['address_components', 'geometry', 'icon', 'name','place_id']); // Set the data fields to return when the user selects a place.

        //change listener on each autocomplete action
        autocomplete.addListener('place_changed', function(){
        	marker.setVisible(false);

        	var place = autocomplete.getPlace();
        	if (!place.geometry) {
        		window.alert("No details available for input: '" + place.name + "'");
        		return;
        	}

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
            	map.fitBounds(place.geometry.viewport);
            } else {
            	map.setCenter(place.geometry.location);
                map.setZoom(17);  // Why 17? Because it looks good.
            }
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);

            var address = '';
            if (place.address_components) {
            	address = [
            	(place.address_components[0] && place.address_components[0].short_name || ''),
            	(place.address_components[1] && place.address_components[1].short_name || ''),
            	(place.address_components[2] && place.address_components[2].short_name || '')
            	].join(' ');
            }
            console.log(place);
            placeInfo = getPlaceInformation(place);
            $('#place_id').val(place.place_id);
            $('#latitude').val(placeInfo['latitude']);
            $('#city').val(placeInfo['city']);
            $('#country').val(placeInfo['country']);
            $('#longitude').val(placeInfo['longitude']);
            $('#postalCode').val(placeInfo['postalCode']);
            $('#state').val(placeInfo['state']);
            $('#street').val(placeInfo['street']);
            $('#placeName').val(placeInfo['name']);

            // setStreetViewMethod(map,place.geometry.location);  
        });


        //update the street view on dragging of marker
        google.maps.event.addListener(marker, 'dragend', function (event) {
        	var newPosition = marker.getPosition();
        	setStreetViewMethod(map,newPosition);
        	geocodePosition(newPosition);
        });
    }    
    function initialize1() {
        //static coordinates
        var coordinates = {lat: 42.345573, lng: -71.098326};
        
        //simple map instance
        var map = new google.maps.Map(document.getElementById('locationsrh'), {
        	center: coordinates,
        	zoom: 14,
            streetViewControl: false,   // it hides the street view control (i.e. person icon) from map
        });

        // Set up the markers on the map
        var marker = new google.maps.Marker({
        	map: map,
        	center: coordinates,
        	draggable: false,
        	animation: google.maps.Animation.DROP,
        });
        marker.setVisible(true);

        //simple street view
        // setStreetViewMethod(map,coordinates);  

        //set the autocomplete
        var input = document.getElementById('locationsrh');
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);
        autocomplete.setFields(['address_components', 'geometry', 'icon', 'name','place_id']); // Set the data fields to return when the user selects a place.

        //change listener on each autocomplete action
        autocomplete.addListener('place_changed', function(){
        	marker.setVisible(false);

        	var place = autocomplete.getPlace();
        	if (!place.geometry) {
        		window.alert("No details available for input: '" + place.name + "'");
        		return;
        	}

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
            	map.fitBounds(place.geometry.viewport);
            } else {
            	map.setCenter(place.geometry.location);
                map.setZoom(17);  // Why 17? Because it looks good.
            }
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);

            var address = '';
            if (place.address_components) {
            	address = [
            	(place.address_components[0] && place.address_components[0].short_name || ''),
            	(place.address_components[1] && place.address_components[1].short_name || ''),
            	(place.address_components[2] && place.address_components[2].short_name || '')
            	].join(' ');
            }
            console.log(place);
            placeInfo = getPlaceInformation(place);
            $('#place_id1').val(place.place_id);
            $('#latitude1').val(placeInfo['latitude']);
            $('#city1').val(placeInfo['city']);
            $('#country1').val(placeInfo['country']);
            $('#longitude1').val(placeInfo['longitude']);
            $('#postalCode1').val(placeInfo['postalCode']);
            $('#state1').val(placeInfo['state']);
            $('#street1').val(placeInfo['street']);
            $('#placeName1').val(placeInfo['name']);
            // setStreetViewMethod(map,place.geometry.location);  
        });

        

        //update the street view on dragging of marker
        google.maps.event.addListener(marker, 'dragend', function (event) {
        	var newPosition = marker.getPosition();
        	setStreetViewMethod(map,newPosition);
        	geocodePosition(newPosition);
        });
    }    
    function getPlaceInformation(place){
    	placeInfo = [];
    	placeInfo['latitude'] = "";
    	placeInfo['longitude'] = "";
    	placeInfo['street'] = "";
    	placeInfo['city'] = "";
    	placeInfo['state'] = "";
    	placeInfo['postalCode'] = "";
    	placeInfo['country'] = "";

    	placeInfo['name'] = place.name;
    	placeInfo['latitude'] = place.geometry.location.lat();
    	placeInfo['longitude'] = place.geometry.location.lng();
    	$.each(place.address_components,function(index,value){
    		if(value.types[0] == 'postal_code'){
    			placeInfo['postalCode'] = value['long_name'];
    		}else if(value.types[0] == 'locality' || value.types[0] == 'administrative_area_level_3'){
    			placeInfo['city'] = value['long_name'];
    		}else if(value.types[0] == 'administrative_area_level_1'){
    			placeInfo['state'] = value['long_name'];
    		}else if(value.types[0] == 'street_number1'){
    			placeInfo['street'] = value['long_name'];
    		}else if(value.types[0] == 'country'){
    			placeInfo['country'] = value['long_name'];
    		}
    	});
    	return placeInfo;
    }
    $('#locationsrch').keypress(function(){
    	initialize();
    });

    $('window').load(function(){
    	initialize();
        //initialize1();
    })
</script>
@endsection
@endsection