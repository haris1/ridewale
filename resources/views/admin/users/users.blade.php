@extends('admin.layouts.admin-app')

@section('content')

@section('css_ref')
<link href="{{ asset('admin_assets/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection
<!-- Main Content -->
<div class="page-wrapper">

    <div class="container-fluid">

        <!-- Title -->
        <div class="row heading-bg">
         <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
           <h5 class="txt-dark">Users List</h5>
       </div>
       <!-- Breadcrumb -->
       <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
           <ol class="breadcrumb">
              <li><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
              <li class="active"><span>Users List</span></li>
          </ol>
      </div>
      <!-- /Breadcrumb -->
  </div>
  <!-- /Title -->

  <!-- Row -->
  <div class="row">
     <div class="col-sm-12">
      <div class="panel panel-default card-view settableboxadd">
       <div class="panel-heading">
        <div class="pull-left">
         <!-- <h6 class="panel-title txt-dark">Riders List</h6> -->
     </div>
     <div class="clearfix"></div>
 </div>
 <div class="panel-wrapper collapse in">
    <div class="panel-body">
     <div class="table-wrap">
      <div class="table-responsive users_detateblebox">
       <table id="datable_1" class="table  table-striped table-bordered mb-0 alltextbtnandiconset1 iconsize_set pb-30" >
        <thead>
         <tr>
          <th>Sr. No</th>
          <th>Name</th>
          <th>Mobile</th>
          <th>Email</th>
          <th>Signup Date</th>
          <th>Active status</th>
          <th>Action</th>
      </tr>
  </thead>
  <tbody>
  </tbody>
</table>
</div>
</div>
</div>
</div>
</div>	
</div>
</div>
<!-- /Row -->
</div>
@include('admin.layouts.footer')
</div>
<!-- /Main Content -->

<!--  edit Rider popup -->

<div class="modal fade in" id="edit_Rider_profile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h5 class="modal-title" id="exampleModalLabel1">Edit Rider Profile</h5>
            </div>
            <form id="EditRiderFrofileForm" method="POST" enctype="multipart/form-data" data-toggle="validator">
                @csrf

                <div class="modal-body" id="append_prodile_data">
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" id="Update" data-loading-text="<i class='fa fa-spinner'></i>" class="btn btn-success mr-10 btn-sm">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
@section('scripts')
<script>
	$('#datable_1').DataTable({
		pageLength:10,
		// processing: true,
		serverSide: true,
		// sServerMethod: "POST",
		lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
		ajax: {
			type: "get",
			url: "{{ route('admin.users.getUsersList') }}",
			data: function ( d ) {
				d._token = "{{ csrf_token() }}";
			},
			complete:function(){
				if( $('[data-toggle="tooltip"]').length > 0 )
					$('[data-toggle="tooltip"]').tooltip();
			}
		},
		columns:[
       { data:'DT_RowIndex',name:'id' },
       { data:'name',name:'name' },
       { data:'mobile_no',name:'mobile_no' },
       { data:'email',name:'email' },
       { data:'signup_date',name:'signup_date' },
       { data:'active_mode',name:'active_mode' },
       { data:'action',name:'action', orderable:false, searchable:false },
       ]
   });

	/* code for get edit user profile data */

	$(document).on('click','.edit_rider_profile',function(){

		var edit_id = $(this).data('id');
		
     $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method:'GET',
        url:"{{route('admin.rider.edit.profile.data')}}",
        data:{'id':edit_id},
        dataType: "html",
        success:function(data) {

         $('#append_prodile_data').html(data);

         $("#edit_Rider_profile").modal('show');
     },
 });
 });

	/* code for get uploaded image preview*/

	var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $($.parseHTML('<img>')).attr('src', event.target.result).attr('class', 'set_css').attr('height', '150').attr('width','150').appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };

    $(document).on('change','#file', function() {
        $('.append_upload_photo').html('');
        imagesPreview(this, 'div.append_upload_photo');
    });


    /* code for update user profile */

    $('#EditRiderFrofileForm').submit(function(e){
    	e.preventDefault();
        form=$('#EditRiderFrofileForm');
        var formData = new FormData(form[0]);		
        var id = $('#id').val();

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method:'POST',
            url:"{{route('admin.users.update','')}}/"+id,
            data:formData,
            cache:false,
            processData: false,
            contentType: false,
            beforeSend: function(){
               $('#update').button('loading');
           },
           success:function(data) {
            switch (data.status) {
                case true:
                toastr.success(data.message);
                location.reload();
                break;
                case false:
                toastr.warning(data.message);
                break;
                default:
                toastr.error("You are not Authorized to access this page");
                break;
            }
        },
        complete: function(){
          $('#update').button('reset');
      }
  });
    });

    /* code for delete user profile */

    $(document).on('click','.delete_rider',function(e){
    	e.preventDefault();
        var id = $(this).data('id');

        swal({
            title: "Are you sure?",   
            text: "You will not be able to recover this information!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#e69a2a",   
            confirmButtonText: "Yes, delete it!",   
            cancelButtonText: "No, cancel please!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        },
        function(isConfirm){   
            if (isConfirm) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN':  "{{ csrf_token() }}"
                    },
                    method:'DELETE',
                    url: "{{route('admin.users.destroy','')}}/"+id,
                    dataType: "json",
                    success:function(data) {
                        switch (data.status) {
                            case 200:
                            table.ajax.reload();
                            swal("Deleted!", "Your information has been deleted.", "success");  
                            toastr.success(data.message);
                            break;
                            case 500:
                            toastr.error(data.message);
                            break;
                            default:
                            toastr.error("You are not Authorized to access this page");
                            break;
                        }
                    },
                });
            } else {
                swal("Cancelled", "Your information is safe! :)", "error"); 
            }
        });
    });
</script>
@endsection
@endsection