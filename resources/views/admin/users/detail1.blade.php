@extends('admin.layouts.admin-app')

@section('content')

<div class="page-wrapper">
    <!-- Main Content -->
    <div class="container-fluid pt-25">
        <div class="back_button">
            <a href="{{route('admin.users.index')}}"><button class="btn btn btn-success">Back</button></a>
        </div>
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <div class="row">
                    <h5 class="txt-dark">Users Details</h5>
                </div>
            </div>
          <!-- Breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb"> 
                    <li><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
                    <li><a href="{{route('admin.users.index')}}">Users List</a></li>
                    <li class="active"><span>Users Details</span></li>
                </ol>
            </div>
            <!-- /Breadcrumb -->
        </div>
    <!--  -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default card-view BoxDesignCoverInerCla1">
                    <div class="panel-heading">
                        <div class="pull-left">
                            
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="form-wrap">
                                <form  method="POST" id="EditRiderFrofileForm" enctype="multipart/form-data" data-toggle="validator">
                                    <div class="colmd12fullwidth1">
                                        <div class="update_image_append">
                                            <a class="fancybox" rel="group" href="@if(file_exists(storage_path().'/app/public/images/users/'.$data[0]->id.'/profile_pic/'.$data[0]->profile_pic)){{asset('storage/images/users/')}}/{{$data[0]->id}}/profile_pic/{{$data[0]->profile_pic}} @else {{asset('admin_assets/img/logo2.png')}} @endif">
                                                 <img src="@if(file_exists(storage_path().'/app/public/images/users/'.$data[0]->id.'/profile_pic/'.$data[0]->profile_pic)){{asset('storage/images/users/')}}/{{$data[0]->id}}/profile_pic/{{$data[0]->profile_pic}} @else {{asset('admin_assets/img/logo2.png')}} @endif" id="preview_image" height="200" width="200">
                                            </a>
                                            
                                        </div>
                                        <!-- <div class="user_update_image_append" style="display: none;">
                                                <img id="preview_image" height="150" width="150" />                         
                                        </div> -->
                                        <div class="form-group">
                                            <input type="file" id="file" name="file">
                                            <input type="hidden" id="id" value="{{$data[0]->id}}" name="id" class="form-control">
                                        </div>
                                    </div>
                                    <div class="colmd12fullwidth">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label mb-10 text-left" for="example-email"><span class="help">First Name</span></label>
                                                <input type="text" id="first_name" name="first_name" class="form-control" placeholder="First Name" required="" value="{{$data[0]->first_name}}"  >
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label mb-10 text-left" for="example-email"><span class="help">Last Name</span></label>
                                                <input type="text" id="last_name" name="last_name" class="form-control" placeholder="Last Name" required="" value="{{$data[0]->last_name}}">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="colmd12fullwidth">
                                        <div class="col-md-6">
                                            <div class="form-group MaleFemalerediobtn1">
                                                <div class="radio radio-info">
                                                    <input type="radio" id="gender" name="gender" class="form-control" data-error="Gender is required !" required="" value="male"@if($data[0]->gender == 'male') checked @endif>
                                                    <label for="male">
                                                        Male
                                                    </label>
                                                </div>
                                                <div class="radio radio-info">
                                                    <input type="radio" id="gender" name="gender" class="form-control" data-error="Gender is required !" required="" value="female" @if($data[0]->gender == 'female') checked @endif> 
                                                    <label for="female">
                                                        Female
                                                    </label>
                                                </div>  
                                            </div>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>    
                                    <div class="colmd12fullwidth">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label mb-10 text-left">Date Of Birth</label>
                                                <div class='input-group date' id='dob'>
                                                    <input type='text' name="datetimepicker1" class="form-control" value="{{date('d-m-Y H:i A', strtotime($data[0]->dob))}}"/>
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label mb-10 text-left" for="example-email"><span class="help">Phone Number</span></label>
                                                <input type="text" id="mobile" name="mobile" class="form-control" placeholder="Email" required="" value="{{$data[0]->mobile_no}}">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="colmd12fullwidth">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label mb-10 text-left" for="example-email"><span class="help">Email Address</span></label>
                                                <input type="email" id="example-email" name="example-email" class="form-control" placeholder="Email" required="" value="{{$data[0]->email}}">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>                                        
                                    </div>
                                   
                                    <div class="colmd12fullwidth2">
                                        <button type="submit" id="Update" data-loading-text="<i class='fa fa-spinner'></i>" class="btnsetm">Update</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Row -->
    </div>
</div>
<!--  edit Rider popup -->

<div class="modal fade in" id="edit_Rider_profile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h5 class="modal-title" id="exampleModalLabel1">Edit Rider Profile</h5>
            </div>
            <form id="EditRiderFrofileForm" method="POST" enctype="multipart/form-data" data-toggle="validator">
                @csrf

                <div class="modal-body" id="append_prodile_data">
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" id="Update" data-loading-text="<i class='fa fa-spinner'></i>" class="btn btn-success mr-10 btn-sm">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">

    $(".fancybox").fancybox();

    $('#dob').datetimepicker({
        useCurrent: false,
        icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
    }).on('dp.show', function() {
    if($(this).data("DateTimePicker").date() === null)
        $(this).data("DateTimePicker").date(moment());
});

$('#dob,#edit_expiry_date').datetimepicker({
        useCurrent: false,
        icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
    }).data("DateTimePicker").date(new Date());

    // function append_upload_photo(){
    //     $('div.user_update_image_append').empty();
    //     readURL(this);
    // };

    $(document).on('change',"#file",function(){
      readURL(this);  
    });

    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#preview_image').attr('src', e.target.result);
                $('.fancybox').attr('href', e.target.result);
                $('.user_update_image_append').show();
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(document).ready(function() {
        $('#user_trip_details').DataTable();
    } );

    // var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
    //     $('.js-switch-1').each(function() {
    //         new Switchery($(this)[0], $(this).data());
    //     });

    /* code use for change approval for user from admin side */

    $(document).on('change','.Profile_approval',function(){

        if($(this).prop("checked") == true){
            var value = 1;
        }else{
            var value = 0;
        }   

        var id = $(this).data('id');
        
        var data = {'id':id,'value':value};

        $.ajax({
            method:'get',
            url:'{{ route('admin.users.approve','') }}/'+id,
            data:data,
            beforeSend:function(){},
            success:function(data) {
                switch (data.status){
                    case 200:
                    toastr.success(data.message);
                    break;
                    case 500:
                    toastr.error(data.message);
                    break;
                }
            },
        });
    });

    $(document).on('click','.send_approval_mail_comment',function(){

        var id = $(this).data('id');
        var comment = $('#user_comment').val();

        if(comment != ""){

            var data = {'id':id,"comment":comment};
            $.ajax({
                method:'get',
                url:"{{ route('admin.approval.mail') }}",
                data:data,
                beforeSend:function(){
                    $('#mail').button('loading');
                },
                success:function(data) {
                    if(data.status == true){
                        toastr.success(data.message);
                    }else{
                        toastr.error(data.message);
                    }
                },
                complete: function(){
                  $('#mail').button('reset');
                }
            });
            
        }else{
            toastr.error('Message field is required.');
            return false;
        }
    });

    /* code for get edit user profile data */

    $(document).on('click','.edit_rider_profile',function(){

        var edit_id = $(this).data('id');
        
         $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method:'GET',
            url:"{{route('admin.rider.edit.profile.data')}}",
            data:{'id':edit_id},
            dataType: "html",
            success:function(data) {
                
                $('#append_prodile_data').html(data);
                
                $("#edit_Rider_profile").modal('show');
            },
        });
    });

    /* code for get uploaded image preview*/

    var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $($.parseHTML('<img>')).attr('src', event.target.result).attr('class', 'set_css').attr('height', '150').attr('width','150').appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }
    };

    $(document).on('change','#file', function() {
        $('.append_upload_photo').html('');
        imagesPreview(this, 'div.append_upload_photo');
    });
    /* code for update user profile */

    $('#EditRiderFrofileForm').submit(function(e){
        e.preventDefault();
        form=$('#EditRiderFrofileForm');
        var formData = new FormData(form[0]);       
        var id = $('#id').val();

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method:'POST',
            url:"{{route('admin.users.update','')}}/"+id,
            data:formData,
            cache:false,
            processData: false,
            contentType: false,
             beforeSend: function(){
               $('#update').button('loading');
            },
            success:function(data) {
                switch (data.status) {
                    case true:
                    toastr.success(data.message);
                    location.reload();
                    break;
                    case false:
                    toastr.error(data.message);
                    break;
                }
            },
            complete: function(){
              $('#update').button('reset');
            }
        });
    });
</script>
@endsection

