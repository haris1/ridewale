@extends('admin.layouts.admin-app')

@section('content')


<div class="page-wrapper">
    <!-- Main Content -->
    <div class="container-fluid pt-25">
        <div class="back_button">
            <a href="{{route('admin.users.index')}}"><button class="btn btn btn-success">Back</button></a>
        </div>
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <div class="row">
                    <h5 class="txt-dark">Users Details</h5>
                </div>
            </div>
          <!-- Breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb"> 
                    <li><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
                    <li><a href="{{route('admin.users.index')}}">Users List</a></li>
                    <li class="active"><span>Users Details</span></li>
                </ol>
            </div>
            <!-- /Breadcrumb -->
        </div>
    <!--  -->
        <div class="row">            
            <div class="BoxDesignCoverInerCla1 profilepageboxset">
                <div class="profile_boxoineami">
                    <div class="profile_titaltext">
                        <h3>Profile</h3>
                    </div>
                    <div class="profileleftrightbox nameproimgbox">
                        <div class="profileleftrightbox_left">
                            <p>PROFILE PHOTO</p>
                        </div>
                        @if($data[0]->profile_verified == 'true')
                        <div class="profileleftrightbox_right">
                              <img src="@if(file_exists(storage_path().'/app/public/images/users/'.$data[0]->id.'/profile_pic/'.$data[0]->profile_pic)){{asset('storage/images/users/')}}/{{$data[0]->id}}/profile_pic/{{$data[0]->profile_pic}} @else {{ asset('admin_assets/svg/profile-img.svg') }} @endif" id="preview_image">
                        </div>
                        @endif
                    </div>
                    <div class="profileleftrightbox">
                        <div class="profileleftrightbox_left">
                            <p>NAME</p>
                        </div>
                        <div class="profileleftrightbox_right">
                            <p>{{$data[0]->first_name.' '.$data[0]->last_name}}</p>
                        </div>
                    </div>
                    <div class="profileleftrightbox">
                        <div class="profileleftrightbox_left">
                            <p>EMAIL</p>
                        </div>
                        <div class="profileleftrightbox_right">
                            <p>{{$data[0]->email}}</p>
                            <div class="verified_icondiv">
                                <img src="{{ asset('admin_assets/svg/verified_icon.svg') }}">
                            </div>
                        </div>
                    </div>
                    <div class="profileleftrightbox">
                        <div class="profileleftrightbox_left">
                            <p>PHONE NUMBER</p>
                        </div>
                        <div class="profileleftrightbox_right">
                            <p>{{$data[0]->mobile_no}}</p>
                        </div>
                    </div>
                    
                    <div class="profileleftrightbox">
                        <div class="profileleftrightbox_left">
                            <p>GENDER</p>
                        </div>
                        <div class="profileleftrightbox_right">
                            <p>{{ ucfirst($data[0]->gender) }}</p>
                        </div>
                    </div>
                    <div class="profileleftrightbox">
                        <div class="profileleftrightbox_left">
                            <p>STREET</p>
                        </div>
                        <div class="profileleftrightbox_right">
                            <p>{{ (isset($data[0]['address']['street']))?$data[0]['address']['street']:'-' }}</p>
                        </div>
                    </div>
                    <div class="profileleftrightbox">
                        <div class="profileleftrightbox_left">
                            <p>CITY</p>
                        </div>
                        <div class="profileleftrightbox_right">
                            <p>{{ (isset($data[0]['address']['city'])?$data[0]['address']['city']:'-') }}</p>
                        </div>
                    </div>
                    <div class="profileleftrightbox">
                        <div class="profileleftrightbox_left">
                            <p>STATE</p>
                        </div>
                        <div class="profileleftrightbox_right">
                            <p>{{ (isset($data[0]['address']['state']))?$data[0]['address']['state']:'-' }}</p>
                        </div>
                    </div>
                    <div class="profileleftrightbox">
                        <div class="profileleftrightbox_left">
                            <p>COUNTRY</p>
                        </div>
                        <div class="profileleftrightbox_right">
                            <p>{{ (isset($data[0]['address']['country']))?$data[0]['address']['country']:'-' }}</p>
                        </div>
                    </div>
                    <div class="profileleftrightbox">
                        <div class="profileleftrightbox_left">
                            <p>PINCODE</p>
                        </div>
                        <div class="profileleftrightbox_right">
                            <p>{{ (isset($data[0]['address']['pincode']))?$data[0]['address']['pincode']:'-' }}</p>
                        </div>
                    </div>
                 <!--    <div class="profileleftrightbox">
                        <div class="profileleftrightbox_left">
                            <p>PASSWORD</p>
                        </div>
                        <div class="profileleftrightbox_right">
                            <p>Last changed Jun 15, 2018 @ 2:55 PM MT</p>
                        </div>
                    </div> -->
                    <div class="profileleftrightbox setborderbor">
                        <div class="profileleftrightbox_left">
                            <p>DEVICE</p>
                        </div>
                        <div class="profileleftrightbox_right">
                            <p>@if($data[0]->firebase_android_id != '') {{'Android'}} @elseif($data[0]->firebase_ios_id != '') {{"IOS"}} @else {{'--'}} @endif</p>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
        <!-- /Row -->
    </div>
</div>
<!--  edit Rider popup -->

<div class="modal fade in" id="edit_Rider_profile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h5 class="modal-title" id="exampleModalLabel1">Edit Rider Profile</h5>
            </div>
            <form id="EditRiderFrofileForm" method="POST" enctype="multipart/form-data" data-toggle="validator">
                @csrf

                <div class="modal-body" id="append_prodile_data">
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" id="Update" data-loading-text="<i class='fa fa-spinner'></i>" class="btn btn-success mr-10 btn-sm">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">

    $(".fancybox").fancybox();

    $('#dob').datetimepicker({
        useCurrent: false,
        icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
    }).on('dp.show', function() {
    if($(this).data("DateTimePicker").date() === null)
        $(this).data("DateTimePicker").date(moment());
});

$('#dob,#edit_expiry_date').datetimepicker({
        useCurrent: false,
        icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
    }).data("DateTimePicker").date(new Date());

    // function append_upload_photo(){
    //     $('div.user_update_image_append').empty();
    //     readURL(this);
    // };

    $(document).on('change',"#file",function(){
      readURL(this);  
    });

    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#preview_image').attr('src', e.target.result);
                $('.fancybox').attr('href', e.target.result);
                $('.user_update_image_append').show();
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(document).ready(function() {
        $('#user_trip_details').DataTable();
    } );

    // var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
    //     $('.js-switch-1').each(function() {
    //         new Switchery($(this)[0], $(this).data());
    //     });

    /* code use for change approval for user from admin side */

    $(document).on('change','.Profile_approval',function(){

        if($(this).prop("checked") == true){
            var value = 1;
        }else{
            var value = 0;
        }   

        var id = $(this).data('id');
        
        var data = {'id':id,'value':value};

        $.ajax({
            method:'get',
            url:'{{ route('admin.users.approve','') }}/'+id,
            data:data,
            beforeSend:function(){},
            success:function(data) {
                switch (data.status){
                    case 200:
                    toastr.success(data.message);
                    break;
                    case 500:
                    toastr.error(data.message);
                    break;
                }
            },
        });
    });

    $(document).on('click','.send_approval_mail_comment',function(){

        var id = $(this).data('id');
        var comment = $('#user_comment').val();

        if(comment != ""){

            var data = {'id':id,"comment":comment};
            $.ajax({
                method:'get',
                url:"{{ route('admin.approval.mail') }}",
                data:data,
                beforeSend:function(){
                    $('#mail').button('loading');
                },
                success:function(data) {
                    if(data.status == true){
                        toastr.success(data.message);
                    }else{
                        toastr.error(data.message);
                    }
                },
                complete: function(){
                  $('#mail').button('reset');
                }
            });
            
        }else{
            toastr.error('Message field is required.');
            return false;
        }
    });

    /* code for get edit user profile data */

    $(document).on('click','.edit_rider_profile',function(){

        var edit_id = $(this).data('id');
        
         $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method:'GET',
            url:"{{route('admin.rider.edit.profile.data')}}",
            data:{'id':edit_id},
            dataType: "html",
            success:function(data) {
                
                $('#append_prodile_data').html(data);
                
                $("#edit_Rider_profile").modal('show');
            },
        });
    });

    /* code for get uploaded image preview*/

    var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $($.parseHTML('<img>')).attr('src', event.target.result).attr('class', 'set_css').attr('height', '150').attr('width','150').appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }
    };

    $(document).on('change','#file', function() {
        $('.append_upload_photo').html('');
        imagesPreview(this, 'div.append_upload_photo');
    });
    /* code for update user profile */

    $('#EditRiderFrofileForm').submit(function(e){
        e.preventDefault();
        form=$('#EditRiderFrofileForm');
        var formData = new FormData(form[0]);       
        var id = $('#id').val();

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method:'POST',
            url:"{{route('admin.users.update','')}}/"+id,
            data:formData,
            cache:false,
            processData: false,
            contentType: false,
             beforeSend: function(){
               $('#update').button('loading');
            },
            success:function(data) {
                switch (data.status) {
                    case true:
                    toastr.success(data.message);
                    location.reload();
                    break;
                    case false:
                    toastr.error(data.message);
                    break;
                }
            },
            complete: function(){
              $('#update').button('reset');
            }
        });
    });
</script>
@endsection

