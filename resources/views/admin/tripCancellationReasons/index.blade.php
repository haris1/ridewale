@extends('admin.layouts.admin-app')

@section('content')

@section('css_ref')
<link href="{{ asset('admin_assets/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
<style type="text/css">
.pac-container {
	background-color: #FFF;
	z-index: 1051;
	position: fixed;
	display: inline-block;
	float: left;
}
.modal{
	z-index: 20;   
}
.modal-backdrop{
	z-index: 10;        
	}​
</style>

@endsection
<!-- Main Content -->
<div class="page-wrapper">

	<div class="container-fluid">

		<!-- Title -->
		<div class="row heading-bg">
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
				<!-- <h5 class="txt-dark">Allowed Not-allowed things management</h5> -->
			</div>
			<!-- Breadcrumb -->
			<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
				<ol class="breadcrumb">
					<li><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
					<li class="active"><span>Trip/Booking Cancellation Reasons management</span></li>
				</ol>
			</div>
			<!-- /Breadcrumb -->
		</div>
		<!-- /Title -->

		<!-- Row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default card-view settableboxadd">
					<div class="panel-heading">
						<div class="pull-left">
							<h6 class="panel-title txt-dark">Driver Trip Cancellation Reasons List</h6>
						</div>
						<div class="pull-right">
							<a href="javascript:void(0);" class="btn btn-success" data-toggle="modal" data-target="#driverTripCancellationReasonAdd_modal" data-title="Add"><span class="btn-text">Add</span></a>								
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="panel-wrapper collapse in">
						<div class="panel-body">
							<div class="table-wrap">
								<div class="table-responsive users_detateblebox">
									<table id="driverTripCancel" class="table table-striped table-bordered" style="width:100%">
										<thead>
											<tr>
												<th>Sr. No</th>
												<th>Reason</th>
												<th>Action</th>
											</tr>
										</thead>
										<tfoot>
											<tr>
												<th>Sr. No</th>
												<th>Reason</th>
												<th>Action</th>
											</tr>
										</tfoot>
										<tbody>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</div>
		<!-- /Row -->

		<!-- Row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default card-view settableboxadd">
					<div class="panel-heading">
						<div class="pull-left">
							<h6 class="panel-title txt-dark">Driver Booking Decline Reasons List</h6>
						</div>
						<div class="pull-right">
							<a href="javascript:void(0);" class="btn btn-success" data-toggle="modal" data-target="#driverBookingDeclineReasonAdd_modal" data-title="Add"><span class="btn-text">Add</span></a>								
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="panel-wrapper collapse in">
						<div class="panel-body">
							<div class="table-wrap">
								<div class="table-responsive users_detateblebox">
									<table id="driverBookingCancel" class="table table-striped table-bordered" style="width:100%">
										<thead>
											<tr>
												<th>Sr. No</th>
												<th>Reason</th>
												<th>Action</th>
											</tr>
										</thead>
										<tfoot>
											<tr>
												<th>Sr. No</th>
												<th>Reason</th>
												<th>Action</th>
											</tr>
										</tfoot>
										<tbody>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</div>
		<!-- /Row -->

		<!-- Row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default card-view settableboxadd">
					<div class="panel-heading">
						<div class="pull-left">
							<h6 class="panel-title txt-dark">Rider Booking Cancellation Reasons List</h6>
						</div>
						<div class="pull-right">
							<a href="javascript:void(0);" class="btn btn-success" data-toggle="modal" data-target="#riderBookingCancelReasonAdd_modal" data-title="Add"><span class="btn-text">Add</span></a>								
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="panel-wrapper collapse in">
						<div class="panel-body">
							<div class="table-wrap">
								<div class="table-responsive users_detateblebox">
									<table id="riderBookingCancel" class="table table-striped table-bordered" style="width:100%">
										<thead>
											<tr>
												<th>Sr. No</th>
												<th>Reason</th>
												<th>Action</th>
											</tr>
										</thead>
										<tfoot>
											<tr>
												<th>Sr. No</th>
												<th>Reason</th>
												<th>Action</th>
											</tr>
										</tfoot>
										<tbody>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</div>
		<!-- /Row -->

		<!-- code for work city maintain -->


	</div>

	@include('admin.layouts.footer')
</div>
<div class="modal fade in" id="driverTripCancellationReasonAdd_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Add Driver Trip Cancellation Reason</h5>
			</div>
			<form id="driverTripCancellationReasonAddFrm" data-toggle="validator">
				<div class="modal-body">
					<div class="form-group">
						<label for="recipient-name" class="control-label mb-10 title">Reason:</label>
						<textarea name="content" class="form-control" required=""></textarea>
						<div class="help-block with-errors"></div>
					</div>
				</div>
				<input type="hidden" name="user_type" value="driver">
				<input type="hidden" name="reason_for" value="cancel_trip">
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Add</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade in" id="driverBookingDeclineReasonAdd_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Add Driver Booking Decline Reason</h5>
			</div>
			<form id="driverBookingDeclineReasonAddFrm" data-toggle="validator">
				<div class="modal-body">
					<div class="form-group">
						<label for="recipient-name" class="control-label mb-10 title">Reason:</label>
						<textarea name="content" class="form-control" required=""></textarea>
						<div class="help-block with-errors"></div>
					</div>
				</div>
				<input type="hidden" name="user_type" value="driver">
				<input type="hidden" name="reason_for" value="decline_booking">
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Add</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- END EDIT STATE MODEL -->
<div class="modal fade in" id="cancellationResonEdit_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h5 class="modal-title">Edit Reason</h5>
			</div>
			<form id="editcancellationResonFrm" data-toggle="validator">
				<div class="modal-body">
					<div class="form-group">
						<input type="hidden" name="id" id="reason_id">
						<label for="recipient-name" class="control-label mb-10 title">Reason:</label>
						<textarea name="content" class="form-control" id="content" required=""></textarea>

						<div class="help-block with-errors"></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Update</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade in" id="riderBookingCancelReasonAdd_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Add Rider Booking Cancel Reason</h5>
			</div>
			<form id="riderBookingCancelReasonAddFrm" data-toggle="validator">
				<div class="modal-body">
					<div class="form-group">
						<label for="recipient-name" class="control-label mb-10 title">Reason:</label>
						<textarea name="content" class="form-control" required=""></textarea>
						<div class="help-block with-errors"></div>
					</div>
				</div>
				<input type="hidden" name="user_type" value="user">
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Add</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- /Main Content -->

@section('scripts')
<script>
	/** driver trip cancellation reason datatable **/
	var table = $('#driverTripCancel').DataTable({
		pageLength:10,
		serverSide: true,
		lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
		ajax: {
			type: "post",
			url: "{{ route('admin.cancellation_reasons.driver_trip_cancellation_reasons') }}",
			data: function ( d ) {
				d._token = "{{ csrf_token() }}";
			},
			complete:function(){
				if( $('[data-toggle="tooltip"]').length > 0 )
					$('[data-toggle="tooltip"]').tooltip();
			}
		},
		columns:[
		{ data:'DT_RowIndex',name:'id' },
		{ data:'content',name:'content' },
		{ data:'action',name:'action', orderable:false, searchable:false },
		]
	});

	/** add driver trip cancellation reason **/
	$( "#driverTripCancellationReasonAddFrm" ).submit(function( event ) {
		event.preventDefault();
		if ($('#driverTripCancellationReasonAddFrm').validator('validate').has('.has-error').length === 0) {

			var data=$( "#driverTripCancellationReasonAddFrm" ).serialize();
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				method:'POST',
				url:'{{route('admin.cancellation_reasons.store')}}',
				data:data,
				dataType: "json",
				success:function(data) {
					switch (data.status) {
						case true:
						toastr.success(data.message);
						table.ajax.url( "{{ route('admin.cancellation_reasons.driver_trip_cancellation_reasons') }}" ).load();
						$('#driverTripCancellationReasonAdd_modal').modal('hide');
						break;
						case false:
						toastr.warning(data.message);
						break;
						default:
						toastr.error("You are not Authorized to access this page");
						break;
					}
				},
			});
		}
	});
	
	/** driver booking decline reason datatable **/
	var table2 = $('#driverBookingCancel').DataTable({
		pageLength:10,
		serverSide: true,
		lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
		ajax: {
			type: "post",
			url: "{{ route('admin.cancellation_reasons.driver_booking_decline_reasons') }}",
			data: function ( d ) {
				d._token = "{{ csrf_token() }}";
			},
			complete:function(){
				if( $('[data-toggle="tooltip"]').length > 0 )
					$('[data-toggle="tooltip"]').tooltip();
			}
		},
		columns:[
		{ data:'DT_RowIndex',name:'id' },
		{ data:'content',name:'content' },
		{ data:'action',name:'action', orderable:false, searchable:false },
		]
	});

	/** add  driver booking decline reason  **/
	$( "#driverBookingDeclineReasonAddFrm" ).submit(function( event ) {
		event.preventDefault();
		if ($('#driverBookingDeclineReasonAddFrm').validator('validate').has('.has-error').length === 0) {

			var data=$( "#driverBookingDeclineReasonAddFrm" ).serialize();
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				method:'POST',
				url:'{{route('admin.cancellation_reasons.store')}}',
				data:data,
				dataType: "json",
				success:function(data) {
					switch (data.status) {
						case true:
						toastr.success(data.message);
						table2.ajax.url( "{{ route('admin.cancellation_reasons.driver_booking_decline_reasons') }}" ).load();
						$('#driverBookingDeclineReasonAdd_modal').modal('hide');
						break;
						case false:
						toastr.warning(data.message);
						break;
						default:
						toastr.error("You are not Authorized to access this page");
						break;
					}
				},
			});
		}
	});

	
	/**  rider booking cancel reason  datatable **/
	var table3 = $('#riderBookingCancel').DataTable({
		pageLength:10,
		serverSide: true,
		lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
		ajax: {
			type: "post",
			url: "{{ route('admin.cancellation_reasons.rider_trip_cancellation_reasons') }}",
			data: function ( d ) {
				d._token = "{{ csrf_token() }}";
			},
			complete:function(){
				if( $('[data-toggle="tooltip"]').length > 0 )
					$('[data-toggle="tooltip"]').tooltip();
			}
		},
		columns:[
		{ data:'DT_RowIndex',name:'id' },
		{ data:'content',name:'content' },
		{ data:'action',name:'action', orderable:false, searchable:false },
		]
	});

	/** add rider booking cancel reason  **/
	$( "#riderBookingCancelReasonAddFrm" ).submit(function( event ) {
		event.preventDefault();
		if ($('#riderBookingCancelReasonAddFrm').validator('validate').has('.has-error').length === 0) {

			var data=$( "#riderBookingCancelReasonAddFrm" ).serialize();
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				method:'POST',
				url:'{{route('admin.cancellation_reasons.store')}}',
				data:data,
				dataType: "json",
				success:function(data) {
					switch (data.status) {
						case true:
						toastr.success(data.message);
						table3.ajax.url( "{{ route('admin.cancellation_reasons.rider_trip_cancellation_reasons') }}" ).load();
						$('#riderBookingCancelReasonAdd_modal').modal('hide');
						break;
						case false:
						toastr.warning(data.message);
						break;
						default:
						toastr.error("You are not Authorized to access this page");
						break;
					}
				},
			});
		}
	});


	/** edit model **/
	$('#cancellationResonEdit_modal').on('shown.bs.modal', function(e) {
		$("#reason_id").val($(e.relatedTarget).data('id'));
		$("#content").val($(e.relatedTarget).data('content'));
	});

	/** edit form submit **/
	$( "#editcancellationResonFrm" ).submit(function( event ) {
		event.preventDefault();
		if ($('#editcancellationResonFrm').validator('validate').has('.has-error').length === 0) {
			var data=$( "#editcancellationResonFrm" ).serialize();
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				method:'PUT',
				url:'{{route('admin.cancellation_reasons.update','')}}/'+$("#reason_id").val(),
				data:data,
				dataType: "json",
				success:function(data) {
					switch (data.status) {
						case true:
						toastr.success(data.message);
						table.ajax.url( "{{ route('admin.cancellation_reasons.driver_trip_cancellation_reasons') }}" ).load();
						table2.ajax.url( "{{ route('admin.cancellation_reasons.driver_booking_decline_reasons') }}" ).load();
						table3.ajax.url( "{{ route('admin.cancellation_reasons.rider_trip_cancellation_reasons') }}" ).load();

						$('#cancellationResonEdit_modal').modal('hide');
						break;
						case false:
						toastr.warning(data.message);
						break;
						default:
						toastr.error("You are not Authorized to access this page");
						break;
					}
				},
			});
		}
	});

	/** delete reason **/
	function deleteCancellationReason(id){ 
		var token = "{{ csrf_token() }}";

		swal({
			title: "Are you sure?",   
			type: "warning",   
			showCancelButton: true,   
			confirmButtonColor: "#e69a2a",   
			confirmButtonText: "Yes, delete it!",   
			cancelButtonText: "No, cancel please!",   
			closeOnConfirm: false,   
			closeOnCancel: false 
		},
		function(isConfirm){   
			if (isConfirm) {
				$.ajax({
					headers: {
						'X-CSRF-TOKEN':  "{{ csrf_token() }}"
					},
					method:'DELETE',
					url:'{{route('admin.cancellation_reasons.destroy','')}}/'+id,
					dataType: "json",
					success:function(data) {

						switch (data.status) {
							case true:
							swal("Deleted!", "Your information has been deleted.", "success");  
							toastr.success(data.message);
							location.reload();
							break;
							case false:
							toastr.error(data.message);
							break;
							default:
							toastr.error("You are not Authorized to access this page");
							break;
						}
					},
				});
			} else {
				swal("Cancelled", "Your information is safe! :)", "error"); 
			}
		});
	}		

</script>
@endsection
@endsection