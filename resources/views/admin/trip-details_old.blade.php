@extends('admin.layouts.admin-app')

@section('content')


<!-- Main Content -->
<div class="page-wrapper">
    <div class="container-fluid pt-25">
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
              <h5 class="txt-dark">Trip Details</h5>
            </div>
            <!-- Breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
              <ol class="breadcrumb">
                <li><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
                <li class="active"><span>Trip Details</span></li>
              </ol>
            </div>
            <!-- /Breadcrumb -->
        </div>
    

    <!-- Row -->
    <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
            <div class="panel panel-default card-view pa-0">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body pa-0">
                        <div class="sm-data-box bg-red">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-xs-6 text-center pl-0 pr-0 data-wrap-left">

                                        <span class="txt-light block"><span class="">Trip Location</span></span>
                                        <span class="weight-500 uppercase-font txt-light block font-13">{{($trip->from_city)?$trip->from_city->name:""}} - {{($trip->to_city)?$trip->to_city->name:""}}</span>
                                    </div>
                                    <div class="col-xs-6 text-center  pl-0 pr-0 data-wrap-right">
                                        <i class="fa fa-map-marker txt-light data-right-rep-icon"></i>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
            <div class="panel panel-default card-view pa-0">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body pa-0">
                        <div class="sm-data-box bg-yellow">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-xs-6 text-center pl-0 pr-0 data-wrap-left">
                                        <span class="txt-light block"><span class="">Trip Date</span></span>
                                        <span class="weight-500 uppercase-font txt-light block">{{($trip->trip_date)?date('d M, y', strtotime($trip->trip_date)):""}} -  {{($trip->pickup_time)?date('H:i A', strtotime($trip->pickup_time)):""}}</span>
                                    </div>
                                    <div class="col-xs-6 text-center  pl-0 pr-0 data-wrap-right">
                                        <i class="fa fa-calendar txt-light data-right-rep-icon"></i>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
            <div class="panel panel-default card-view pa-0">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body pa-0">
                        <div class="sm-data-box bg-green">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-xs-6 text-center pl-0 pr-0 data-wrap-left">
                                        <span class="txt-light block">$ <span class="">{{$trip->bookings->sum('total')}}</span></span>
                                        <span class="weight-500 uppercase-font txt-light block">Total Income</span>
                                    </div>
                                    <div class="col-xs-6 text-center  pl-0 pr-0 data-wrap-right">
                                        <i class="ti-car txt-light data-right-rep-icon"></i>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
            <div class="panel panel-default card-view pa-0">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body pa-0">
                        <div class="sm-data-box bg-blue">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-xs-6 text-center pl-0 pr-0 data-wrap-left">
                                        <span class="txt-light block">$ <span class="">{{$trip->bookings->sum('admin_fees')}}</span></span>
                                        <span class="weight-500 uppercase-font txt-light block">Total Commission</span>
                                    </div>
                                    <div class="col-xs-6 text-center  pl-0 pr-0 pt-25  data-wrap-right newseticonbox">
                                        <!-- <div id="sparkline_4" style="width: 100px; overflow: hidden; margin: 0px auto;"><canvas width="115" height="50" style="display: inline-block; width: 115px; height: 50px; vertical-align: top;"></canvas></div> -->
                                        <i class="fa fa-money" aria-hidden="true"></i>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->

    <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="panel panel-default card-view panel-refresh">
            <div class="refresh-container">
                <div class="la-anim-1"></div>
            </div>
            <div class="panel-heading">
                <div class="pull-left">
                    <h6 class="panel-title txt-dark"> Driver Info </h6>
                </div>
                <div class="pull-right">
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="panel-wrapper collapse in">
                <div class="panel-body row pa-0">
                    <div class="table-wrap">
                        <div class="table-responsive">
                            <table class="table table-hover mb-0">
                                <thead>
                                    <tr>
                                        <th>Driver name</th>
                                        <th>Vehicle name</th>
                                        <th>Seats Capacity</th>
                                        <th>Vacant Seats for Trip</th>
                                        <th>Trip Created</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td> <a href="{{route('admin.driver.show',base64_encode($trip->driver->id))}}" class="a_tag_color"  data-toggle="tooltip" title="Driver Profile"><span class="txt-dark weight-500">{{ $trip->driver->user->first_name.' '.$trip->driver->user->last_name}} </span></a></td>
                                        <td> {{ $trip->vehicle->car_model->model_name }} </td>
                                        <td> {{ $trip->vehicle->car_model->capacity }} </td>
                                        <td> {{ $trip->total_seats }} </td>
                                        <td> {{ $trip->created_at->format('d M, y - H:i A') }} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>  
                </div>  
            </div>
        </div>
    </div>
</div> 

    <!-- Row -->
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <div class="panel panel-default card-view panel-refresh">
            <div class="refresh-container">
                <div class="la-anim-1"></div>
            </div>
            <div class="panel-heading">
                <div class="pull-left">
                    <h6 class="panel-title txt-dark"> Riders Info </h6>
                </div>
<!--                 <div class="pull-right">

                </div> -->
                <div class="clearfix"></div>
            </div>
            <div class="panel-wrapper collapse in">
                <div class="panel-body row pa-0">
                    <div class="table-wrap">
                        <div class="table-responsive">
                            <table class="table table-hover mb-0">
                                <thead>
                                    <tr>
                                        <th>Sr No.</th>
                                        <th>Rider Name</th>
                                        <th>Booked Seats</th>
                                        <th>Booked At</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @forelse($trip->bookings as $key => $payment)
                                    <tr>
                                        <td> {{ $key+1 }} </td>
                                        <td><a href="{{route('admin.users.show',$payment->user->id)}}" class="a_tag_color" data-toggle="tooltip" title="Trip Details"><span class="txt-dark weight-500"> {{ $payment->user->first_name.' '.$payment->user->last_name }} </span></a></td>
                                        <td>{{ $payment->total_person }}</td>
                                        <td>{{ date('d M, y - H:i A', strtotime($payment->created_at)) }}</td>
                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="4" align="center"><strong>No Users there.</strong></td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>  
                </div>  
            </div>
        </div>
    </div>
</div> 
<!-- Row -->
</div>
@include('admin.layouts.footer')
</div>
<!-- /Main Content -->
@endsection
