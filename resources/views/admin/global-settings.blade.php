@extends('admin.layouts.admin-app')
@section('styles')
<style type="text/css">
.disply_temp, .disply_transfer{
	display: none;
}
</style>
@endsection
@section('content')

<div class="adminright_detlisdivbox">
		<div class="setting_detlisdivbox">
			<div class="vehicles_topdivbox">
				<ul>
					<li>
						<div class="vehicles_innertextbox">
							<h4>Vehicles</h4>
							<a href="{{ route('admin.carCompany.index') }}"><img src="{{ asset('admin_assets/svg/right-arrow.svg') }}"></a>
							<p>{{ $total_vehicles }} Vehicles</p>
						</div>
					</li>
					<li>
						<div class="vehicles_innertextbox">
							<h4>Locations</h4>
							<a href="{{ route('admin.work_countries.index') }}"><img src="{{ asset('admin_assets/svg/right-arrow.svg') }}"></a>
							<p>{{ $total_location }} Locations</p>
						</div>
					</li>
					<li>
						<div class="vehicles_innertextbox">
							<h4>Active Cities</h4>
							<a href="{{ route('admin.officeInformations.index') }}"><img src="{{ asset('admin_assets/svg/right-arrow.svg') }}"></a>
							<p>{{ $total_city }} Cities</p>
						</div>
					</li>
				</ul>
			</div>
			<div class="ridechimpfees_mandetlisdiv">
				<div class="ridechimpfees_detlisbox">
					<form data-toggle="validator" role="form" novalidate="true" id="rideChimpForm">
						<p>Ridechimp Fees</p>
						<div class="feesinput_lefttext">
							<span>%</span>
						</div>
						<div class="ridechimpfeesinput_text">
							<input type="text" name="admin_cent" placeholder="00.00" value="{{ $admin_statistic->admin_cent }}" required="">
						</div>
						<div class="ridechimpfeessave_butten">
							<button type="submit">SAVE</button>
						</div>
					</form>
				</div>
			</div>
			<div class="ridechimpfees_mandetlisdiv">
				<form class="form" data-toggle="validator" role="form" novalidate="true" id="appSettingForm">
					<div class="ridechimpfees_detlisbox">
						<p>App Settings</p>
						<div class="maintanacradio_button">
							<p>Maintenance Mode</p>
								<div class="switch-field">
									<input type="radio" id="switch_left" name="maintanance_mode" value="0" checked @if(isset($maintanance->maintanance_mode) && $maintanance->maintanance_mode=="0"){{'checked'}}@endif>
									<label for="switch_left">OFF</label>
									<input type="radio" id="switch_right" name="maintanance_mode" value="1" @if(isset($maintanance->maintanance_mode) && $maintanance->maintanance_mode=="1"){{'checked'}}@endif>
									<label for="switch_right">ON</label>
								</div>
						</div>
						<div class="appversion_detlisdiv">
							<p>Android App Version</p>
							<input type="text" name="android_version" placeholder="0.0" value="@if($maintanance != ''){{$maintanance->android_version}}@endif" required="">
						</div>
						<div class="appversion_detlisdiv">
							<p for="ios_version">IOS App Version</p>
							<input type="text" name="ios_version" placeholder="0.0" value="@if($maintanance != ''){{$maintanance->ios_version}}@endif" id="ios_version" required="">
						</div>
						<div class="bottomsave_btn">
							<button type="submit">SAVE</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>




@endsection
@section('scripts')
<script>
		//APP SETTING
		$( "#appSettingForm" ).submit(function( event ) {
			event.preventDefault();
			var data=$("#appSettingForm").serialize();
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				method:'POST',
				url:'{{route('admin.products.appSetting')}}',
				data:data,
				dataType: "json",
				beforeSend:function(){},
				success:function(data) {
					switch (data.status) {
						case true:
						toastr.success(data.message);
						break;
						case false:
						toastr.error(data.message);
						break;
					}
				},
			});
		});


		//RIDECHIMP FEES
		$("#rideChimpForm" ).submit(function( event ) {
			event.preventDefault();
			var data=$("#rideChimpForm").serialize();
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				method:'POST',
				url:'{{route('admin.products.adminFees')}}',
				data:data,
				dataType: "json",
				beforeSend:function(){},
				success:function(data) {
					switch (data.status) {
						case true:
						toastr.success(data.message);
						break;
						case false:
						toastr.error(data.message);
						break;
					}
				},
			});
		});

</script>
@endsection
