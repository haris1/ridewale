@extends('admin.layouts.admin-app')

@section('content')
<div class="page-wrapper" style="min-height: 436px;">
	<div class="container-fluid">

		<!-- Title -->
		<div class="row heading-bg">
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			  <h5 class="txt-dark">Product Management</h5>
			</div>
			<!-- Breadcrumb -->
			<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
			  <ol class="breadcrumb">
				<li><a href="index-2.html">Dashboard</a></li>
				<li class="active"><span>Product Management List</span></li>
			  </ol>
			</div>
			<!-- /Breadcrumb -->
		</div>
		<!-- /Title -->

		<!-- Row -->
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default card-view">
					<div class="panel-heading">
						<div class="pull-left">
							<h6 class="panel-title txt-dark">Service</h6>
						</div>
						<div class="pull-right">
							<a href="#" class="pull-left inline-block full-screen">
								<i class="zmdi zmdi-fullscreen"></i>
							</a>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="panel-wrapper collapse in">
						<div class="panel-body row pa-0">
							<div class="table-wrap">
								<div class="table-responsive">
									<div id="support_table_wrapper" class="dataTables_wrapper no-footer">
										<table class="table display product-overview border-none dataTable no-footer" id="service_table" role="grid">
											<thead>
												<tr role="row">
													<th class="sorting" tabindex="0" aria-controls="support_table" rowspan="1" colspan="1" aria-label="ticket ID: activate to sort column ascending" style="width: 169px;">Sr No</th>
													<th class="sorting" tabindex="0" aria-controls="support_table" rowspan="1" colspan="1" aria-label="ticket ID: activate to sort column ascending" style="width: 169px;">Service Name</th>
													<th class="sorting" tabindex="0" aria-controls="support_table" rowspan="1" colspan="1" aria-label="ticket ID: activate to sort column ascending" style="width: 169px;">Rate</th>
												</tr>
											</thead>
											<tbody>
												@forelse($services as $key => $service)
												<tr role="row" class="odd">
													<td>{{ $key+1 }}</td>
													<td class="text-uppercase">{{ $service->name }}</td>
													<td>{{ $service->rate }}</td>
												</tr>
												@empty
													<td colspan="3">No service found.</td>
												@endforelse
											</tbody>
										</table>
									</div>
								</div>
							</div>	
						</div>	
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="panel panel-default card-view">
					<div class="panel-heading">
						<div class="pull-left">
							<h6 class="panel-title txt-dark">Sidwalks</h6>
						</div>
						<div class="pull-right">
							<a href="#" class="pull-left inline-block full-screen">
								<i class="zmdi zmdi-fullscreen"></i>
							</a>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="panel-wrapper collapse in">
						<div class="panel-body row pa-0">
							<div class="table-wrap">
								<div class="table-responsive">
									<div id="support_table_wrapper" class="dataTables_wrapper no-footer">
										<table class="table display product-overview border-none dataTable no-footer" id="sidewalks_table" role="grid">
											<thead>
												<tr role="row">
													<th class="sorting" tabindex="0" aria-controls="support_table" rowspan="1" colspan="1" aria-label="ticket ID: activate to sort column ascending" style="width: 169px;">Sr No</th>
													<th class="sorting" tabindex="0" aria-controls="support_table" rowspan="1" colspan="1" aria-label="ticket ID: activate to sort column ascending" style="width: 169px;">Sidwalks Name</th>
													<th class="sorting" tabindex="0" aria-controls="support_table" rowspan="1" colspan="1" aria-label="ticket ID: activate to sort column ascending" style="width: 169px;">Rate</th>
												</tr>
											</thead>
											<tbody>
												@forelse($sidewalks as $key => $sidewalk)
												<tr role="row" class="odd">
													<td>{{ $key+1 }}</td>
													<td>{{ $sidewalk->name }}</td>
													<td>{{ $sidewalk->rate }}</td>
												</tr>
												@empty
													<td colspan="3">No sidewalks found.</td>
												@endforelse
											</tbody>
										</table>
									</div>
								</div>
							</div>	
						</div>	
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="panel panel-default card-view">
					<div class="panel-heading">
						<div class="pull-left">
							<h6 class="panel-title txt-dark">Driveways</h6>
						</div>
						<div class="pull-right">
							<a href="#" class="pull-left inline-block full-screen">
								<i class="zmdi zmdi-fullscreen"></i>
							</a>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="panel-wrapper collapse in">
						<div class="panel-body row pa-0">
							<div class="table-wrap">
								<div class="table-responsive">
									<div id="support_table_wrapper" class="dataTables_wrapper no-footer">
										<table class="table display product-overview border-none dataTable no-footer" id="driveways_table" role="grid">
											<thead>
												<tr role="row">
													<th class="sorting" tabindex="0" aria-controls="support_table" rowspan="1" colspan="1" aria-label="ticket ID: activate to sort column ascending" style="width: 169px;">Sr No</th>
													<th class="sorting" tabindex="0" aria-controls="support_table" rowspan="1" colspan="1" aria-label="ticket ID: activate to sort column ascending" style="width: 169px;">Driveways</th>
													<th class="sorting" tabindex="0" aria-controls="support_table" rowspan="1" colspan="1" aria-label="ticket ID: activate to sort column ascending" style="width: 169px;">Rate</th>
												</tr>
											</thead>
											<tbody>
												@forelse($driveways as $key => $driveway)
												<tr role="row" class="odd">
													<td>{{ $key+1 }}</td>
													<td>{{ $driveway->name }}</td>
													<td>{{ $driveway->rate }}</td>
												</tr>
												@empty
													<td colspan="3">No driveways found.</td>
												@endforelse
											</tbody>
										</table>
									</div>
								</div>
							</div>	
						</div>	
					</div>
				</div>
			</div>
		</div>
		<!-- /Row -->
	</div>
</div>
@section('scripts')
<script>
	$('#service_table,#sidewalks_table,#driveways_table').DataTable({
		"bFilter": false,
		"bLengthChange": false,
		"bPaginate": false,
		"bInfo": false,
	});
</script>
@endsection
@endsection