@extends('admin.layouts.admin-app')

@section('content')
<div class="wrapper pa-0">
    <header class="sp-header">       
        <!-- <div class="form-group mb-0 pull-right">
            <span class="inline-block pr-10">Don't have an account?</span>
            <a class="inline-block btn btn-info btn-rounded btn-outline" href="signup.html">Sign Up</a>
        </div> -->
        <div class="clearfix"></div>
    </header>
    
    <!-- Main Content -->
    <div class="page-wrapper pa-0 ma-0 auth-page">
        <div class="container-fluid">
            <!-- Row -->
            <div class="table-struct full-width full-height">
                <div class="table-cell vertical-align-middle auth-form-wrap">
                    <div class="auth-form  ml-auto mr-auto no-float">
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                 <div class="sp-logo-wrap pull-left setlogobox_text">
                                    <a href="javascript:void(0);">
                                        <img class="brand-img mr-10" src="{{ asset('admin_assets/img/ride-logo.svg') }}" alt="brand"/>
                                        <!-- <span class="brand-text">Ridewale</span> -->
                                    </a>
                                </div>
                                <div class="mb-30">
                                    <h3 class="text-center txt-dark mb-10">Sign in to Admin Panel</h3>
                                    <h6 class="text-center nonecase-font txt-grey">Enter your details below</h6>
                                </div>  
                                <div class="form-wrap">
                                    <form method="POST" action="{{ route('admin.login') }}" aria-label="{{ __('Admin Login') }}">
                                        @csrf
                                        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                            <label class="control-label mb-10" for="email">Email address</label>
                                            <input type="email" class="form-control" name="email"  value="{{ old('email') }}" id="email" placeholder="Enter email" required autofocus>
                                            @if ($errors->has('email'))
                                                <div class="help-block with-errors">
                                                    <ul class="list-unstyled">
                                                        <li>{{ $errors->first('email') }}</li>
                                                    </ul>
                                                </div>
                                            @endif
                                        </div>
                                        
                                        <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                                            <label class="pull-left control-label mb-10" for="password">Password</label>
                                            <a class="capitalize-font txt-primary block mb-10 pull-right font-12" href="{{ route('admin.password.request') }}">{{ __('Forgot Password ?') }}</a>
                                            <div class="clearfix"></div>
                                            <input type="password" class="form-control" name="password"  value="{{ old('password') }}" id="password" placeholder="Enter password" required>
                                            @if ($errors->has('password'))
                                                <div class="help-block with-errors">
                                                    <ul class="list-unstyled">
                                                        <li>{{ $errors->first('password') }}</li>
                                                    </ul>
                                                </div>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <div class="checkbox checkbox-primary pr-10 pull-left">
                                                <input id="remember" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                                <label for="remember"> {{ __('Keep me logged in') }}</label>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="form-group text-center btnsetcolor1">
                                            <button type="submit" class="btn btn-info btn-rounded">sign in</button>
                                        </div>
                                    </form>
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Row -->   
        </div>
        
    </div>
    <!-- /Main Content -->
</div>
<!-- /#wrapper -->
@endsection
