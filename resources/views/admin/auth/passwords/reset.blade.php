@extends('admin.layouts.admin-app')

@section('content')
    <div class="wrapper pa-0">
                
        <!-- Main Content -->
        <div class="page-wrapper pa-0 ma-0 auth-page">
            <div class="container-fluid">
                <!-- Row -->
                <div class="table-struct full-width full-height">
                    <div class="table-cell vertical-align-middle auth-form-wrap">
                        <div class="auth-form  ml-auto mr-auto no-float">
                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <div class="sp-logo-wrap text-center pa-0 mb-30">
                                        <a href="index-2.html">
                                            <img class="brand-img mr-10" src="{{ asset('admin_assets/img/logo.png') }}" alt="brand"/>
                                            <span class="brand-text">Ridewale</span>
                                        </a>
                                    </div>
                                    <div class="mb-30">
                                        <h3 class="text-center txt-dark mb-10">{{ __('Reset Password') }}</h3>
                                    </div>  
                                    <div class="form-wrap">
                                        <form method="POST" action="{{ route('admin.password.request') }}" aria-label="{{ __('Reset Password') }}">
                                            @csrf
                                            <input type="hidden" name="token" value="{{ $token }}">
                                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                <label class="pull-left control-label mb-10" for="email">{{ __('E-Mail Address') }}</label>
                                                <input type="email" class="form-control" name="email" value="{{ $email ?? old('email') }}" required="" id="email" placeholder="Enter email">
                                                @if ($errors->has('email'))
                                                    <div class="help-block with-errors">
                                                        <ul class="list-unstyled">
                                                            <li>{{ $errors->first('email') }}</li>
                                                        </ul>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                                <label class="pull-left control-label mb-10" for="password">{{ __('Password') }}</label>
                                                <input type="password" class="form-control" name="password" required="" id="password" placeholder="Enter New pwd">
                                                @if ($errors->has('password'))
                                                    <div class="help-block with-errors">
                                                        <ul class="list-unstyled">
                                                            <li>{{ $errors->first('password') }}</li>
                                                        </ul>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <label class="pull-left control-label mb-10" for="confirm_password">{{ __('Confirm Password') }}</label>
                                                <input type="password" class="form-control" name="password_confirmation" required="" id="confirm_password" placeholder="Re-Enter pwd">
                                            </div>
                                            <div class="form-group text-center">
                                                <button type="submit" class="btn btn-info btn-rounded">{{ __('Reset Password') }}</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Row -->   
            </div>
            
        </div>
        <!-- /Main Content -->

    </div>
    <!-- /#wrapper -->
@endsection
