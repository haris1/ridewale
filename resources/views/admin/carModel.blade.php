@extends('admin.layouts.admin-app')

@section('content')

@section('css_ref')
    <link href="{{ asset('admin_assets/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection
 <!-- Main Content -->
<div class="page-wrapper">
    
    <div class="container-fluid">
				
				<!-- Title -->
				<div class="row heading-bg">
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
					  <h5 class="txt-dark">Car Model List</h5>
					</div>
					<!-- Breadcrumb -->
					<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
					  <ol class="breadcrumb">
						<li><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
						<li class="active"><span>Car Model List</span></li>
					  </ol>
					</div>
					<!-- /Breadcrumb -->
				</div>
				<!-- /Title -->
				
				<!-- Row -->
				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default card-view settableboxadd">
							<div class="panel-heading">
								<div class="pull-left">
									<h6 class="panel-title txt-dark"></h6>
								</div>
								<div class="pull-right">
									<a class="btn btn-success" data-toggle="modal" data-target="#addcarModel" data-original-title="Add">Add</a>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div class="table-wrap">
										<div class="table-responsive">
											<table id="car_Model" class="table  table-striped table-bordered mb-0 alltextbtnandiconset1 iconsize_set pb-30" >
												<thead>
													<tr>
														<th>Sr. No</th>
														<th>Model Name</th>
														<th>Company Name</th>
														<th>Type Name</th>
														<th>Capacity Name</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>	
					</div>
				</div>
				<!-- /Row -->
			</div>
    @include('admin.layouts.footer')
</div>
<!-- /Main Content -->

<!-- modal for add new car Model -->

	<div class="modal fade in" id="addcarModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Add Company Name</h5>
			</div>
			<form id="addcarModelform" data-toggle="validator">
				<div class="modal-body">
					<div class="form-group">
						<label class="control-label mb-10 text-left"> Select Company</label>
						<select class="form-control" id="company" required="">
							<option value="">Select Company</option>
							@foreach($coms as $com)
							<option value="{{$com['id']}}">{{ucfirst($com['name'])}}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group ">
						<label class="control-label mb-10 text-left"> Select Type </label>
						<select class="form-control" id="type" required="">
							<option value="">Select Type</option>
							@foreach($types as $type)
							<option value="{{$type['id']}}">{{ucfirst($type['name'])}}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group ">
						<label class="control-label mb-10 text-left"> Select Capacity </label>
						<select class="form-control" id="capacity" required="">
							<option value="">Select Capacity</option>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
						</select>
					</div>
					<div class="form-group ">
						<label class="control-label mb-10 text-left" for="title">Car Model Name</label>
						<input type="text" id="carname" name="carname" class="form-control" placeholder="Car Model Name" data-error="Tax Category Title is required !" required="">
						<div class="help-block with-errors"></div>
						@if ($errors->has('title'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('title') }}</strong>
						</span>
						@endif
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
					<button type="submit" id="insert" class="btn btn-success mr-10 btn-sm">ADD</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!--  Edit new car Model modal -->

	<div class="modal fade in" id="editcarModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Edit Car Model</h5>
			</div>
			<form id="editcarModelform" data-toggle="validator">
				<div class="modal-body">
					<div class="form-group">
						<label class="control-label mb-10 text-left"> Select Company</label>
						<select class="form-control" id="editcompany" name="editcompany" required="">
							<option value="">Select Company</option>
							@foreach($coms as $com)
							<option value="{{$com['id']}}">{{ucfirst($com['name'])}}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<label class="control-label mb-10 text-left"> Select Type </label>
						<select class="form-control" id="edittype" name="edittype" required="">
							<option value="">Select Type</option>
							@foreach($types as $type)
							<option value="{{$type['id']}}">{{ucfirst($type['name'])}}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<label class="control-label mb-10 text-left"> Select Capacity </label>
						<select class="form-control" id="editcapacity" name="editcapacity" required="">
							<option value="">Select Capacity</option>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
						</select>
					</div>
					<div class="form-group">
						<label class="control-label mb-10 text-left" for="title">Car Model Name</label>
						<input type="text" id="editcarname" name="carname" class="form-control" placeholder="Car Model Name" data-error="Tax Category Title is required !" required="">
						
						<div class="help-block with-errors"></div>
						@if ($errors->has('title'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('title') }}</strong>
						</span>
						@endif
					</div>
					<input type="hidden" name="edit_Model_id" id="edit_Model_id">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
					<button type="submit" id="update" class="btn btn-success mr-10 btn-sm">Submit</button>
				</div>
			</form>
		</div>
	</div>
</div>
	<!-- end of edit modal -->
@section('scripts')
<script>

		var table = $('#car_Model').DataTable({
		pageLength:10,
		// processing: true,
		ajax: "data.json",
		serverSide: true,
		// sServerMethod: "POST",
		lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
			ajax: {
				type: "post",
				url: "{{ route('admin.carmodel.list') }}",
				data: function ( d ) {
					d._token = "{{ csrf_token() }}";
				},
				complete:function(){
					if( $('[data-toggle="tooltip"]').length > 0 )
						$('[data-toggle="tooltip"]').tooltip();
				}
			},
			columns:[
				{ data:'DT_RowIndex',name:'id' },
				{ data:'model_name',name:'name' },
				{ data:'comname',name:'company' },
				{ data:'cartype',name:'type' },
				{ data:'capacity',name:'capacity' },
				{ data:'action',name:'action', orderable:false, searchable:false },
			]
		});

	
	$("#addcarModelform").submit(function( event ) {
				event.preventDefault();
			var type = $('#type').val(); 
			var company = $('#company').val(); 
			var name = $('#carname').val();
			var capacity = $('#capacity').val();

			var data = {'type':type,'company':company,'name':name,'capacity':capacity,'_token':'{{csrf_token()}}'};

			$('#insert').attr('disabled',true);
			if ($('#addcarModelform').validator('validate').has('.has-error').length === 0) {
				$.ajax({
					method:'POST',
					url:'{{ route('admin.carModel.store') }}',
					data:data,
					success:function(data) {
						
						$('#insert').attr('disabled',false);
						$("#addcarModel").modal('hide');
						$("#carname").val('');
						table.ajax.reload();
						switch (data.status) {
							case 200:
							toastr.success(data.message);
							break;
							case 500:
							toastr.error(data.message);
							break;
						}
					}
				});
			}
		});


		$(document).on('click',".edit_Model",function(){
			$('#editcarModelform')[0].reset();
			var id = $(this).data('id');
			$.ajax({
				method:'POST',
				url:'{{ route('admin.carmodel.edit.name') }}',
				data:{'id':id,'_token':'{{csrf_token()}}'},
				success:function(data) {
							


					$('select[name^="edittype"] option[value="'+data.data.car_type_id+'"]').attr("selected","selected");
					$('select[name^="editcompany"] option[value="'+data.data.car_company_id+'"]').attr("selected","selected");
					$('select[name^="editcapacity"] option[value="'+data.data.capacity+'"]').attr("selected","selected");

					// $('#edittype option:selected').val(data.data.car_type_id).attr('selected','selected'); 
					// $('#editcompany option:selected').val(data.data.car_company_id).attr('selected','selected');
					// $('#editcapacity option:selected').val(data.data.capacity).attr('selected','selected');
					//alert(data.data.id);

					$('#editcarname').val(data.data.model_name);
					$('#edit_Model_id').val(data.data.id);

					$('#insert').attr('disabled',false);
					$("#addcarModel").modal('hide');
					$("#carname").val('');
					switch (data.status) {
						case 200:
						$('#edit_carname').val(data.data);
						// toastr.success(data.message);
						table.ajax.reload();
						break;
						case 500:
						toastr.error(data.message);
						break;
					}
				},
				complete:function(){},
				error:function(){}
			});
		});

		$(document).on('click','#update',function(){

			var type = $('#edittype').val(); 
			var company = $('#editcompany').val(); 
			var name = $('#editcarname').val();
			var capacity = $('#editcapacity').val();
			var id = $('#edit_Model_id').val();
			var data = {'edittype':type,'id':id,'editcompany':company,'editname':name,'editcapacity':capacity,'_token':'{{csrf_token()}}'};

			$.ajax({
				method:'PATCH',
				url:'carModel/'+id,
				data:data,
				success:function(data) {
					
					$('#insert').attr('disabled',false);
					$("#editcarModel").modal('hide');
					$("#edit_carname").val('');
					switch (data.status) {
						case 200:
						table.ajax.reload();
						toastr.success(data.message);
						break;
						case 500:
						toastr.error(data.message);
						break;
					}
				},
				complete:function(){},
				error:function(){}
			});

		});

		$(document).on('click','.carModelDelete',function(){

			var id = $(this).data('id');
			
			$.ajax({
				method:'DELETE',
				url:'carModel/'+id,
				data:{'id':id,'_token':'{{csrf_token()}}'},
				success:function(data) {
					
					$('#insert').attr('disabled',false);
					$('.carModelDelete').data('id',"");
					$("#deletecarModel").modal('hide');
					switch (data.status) {
						case 200:
						table.ajax.reload();
						toastr.success(data.message);
						break;
						case 500:
						toastr.error(data.message);
						break;
					}
				},
				complete:function(){},
				error:function(){}
			});
		});

		function deleteModel(id){ 
		var token = "{{ csrf_token() }}";

		swal({
			title: "Are you sure?",   
            text: "You will not be able to recover this information!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#e69a2a",   
            confirmButtonText: "Yes, delete it!",   
            cancelButtonText: "No, cancel please!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
		},
		function(isConfirm){   
			if (isConfirm) {
				$.ajax({
					headers: {
						'X-CSRF-TOKEN':  "{{ csrf_token() }}"
					},
					method:'DELETE',
					url:'carModel/'+id,
					dataType: "json",
					success:function(data) {
						switch (data.status) {
						case 200:
						table.ajax.reload();
						swal("Deleted!", "Your information has been deleted.", "success");  
						toastr.success(data.message);
						break;
						case 500:
						toastr.error(data.message);
						break;
					}
					},
				});
			} else {
				swal("Cancelled", "Your information is safe! :)", "error"); 
			}
		});
	}
</script>
@endsection
@endsection