<?php use Carbon\Carbon; ?>
@extends('admin.layouts.admin-app')

@section('content')
<div class="adminright_detlisdivbox">
	<div class="setting_detlisdivbox">
		<div class="ordersbooked_detlis">
			<h4>ORDER#{{ $booking->id }}</h4>
			<P>Booking Status:
				@if($booking->status == 'pending')
				<span> Pending </span>
				@elseif($booking->status == 'confirmed')
				<span> Confirmed </span>
				@elseif($booking->status == 'cancelled')
				<span class="text-danger"> Cancelled </span>
				@elseif($booking->status == 'picked_up')
				<span> Picked Up </span>
				@elseif($booking->status == 'no_show')
				<span class="text-danger"> No Show </span>
				@elseif($booking->status == 'drop_off')
				<span> Dropped Off </span>
				@elseif($booking->status == 'rejected')
				<span class="text-danger"> Rejected </span>
				@endif
			</P>
			<P>Trip Status:
				@if($booking->trip->status == 'cancelled')
				<span class="text-danger"> Cancelled by driver </span>
				@elseif($booking->status == 'pending' || $booking->status == 'confirmed')
				<span> Scheduled </span>
				@elseif($booking->status == 'cancelled')
				<span class="text-danger"> Cancelled by rider </span>
				@elseif($booking->status == 'picked_up')
				<span> Started </span>
				@elseif($booking->status == 'no_show')
				<span class="text-danger">Marked as No show </span>
				@elseif($booking->status == 'drop_off')
				<span> Completed </span>
				@elseif($booking->status == 'rejected')
				<span class="text-danger"> Declined </span>
				@endif
			</P>
			<P>Payment Status:
				<span>
					<?php
					$payment_status = 'Pending';
					if($booking->payment != ''){
						$payment_status = 'Processed';
					}

					if($booking->status == 'cancelled' ||  $booking->trip->status == 'cancelled'){
						if($booking->payment['refunded_id'] == ''){
							$payment_status = 'Refund Pending';
						} else {
							$payment_status = 'Refunded';
						}
					}

					if($booking->status == 'rejected' && $booking->payment == ''){
						$payment_status = 'Not Charged';
					}

					echo $payment_status;
					?>
				</span>
                @if($booking->driver_not_show_at != '' && $booking->payment['refunded_id'] == '' && $booking->trip->dropoff_time < Carbon::today() && $booking->trip->status == 'scheduled')
				<div class="ridechimpfeessave_butten" style="width: 55%;">
					<button type="button" id="send_refund">SEND REFUND</button>
				</div>				
				@endif
			</P>
		</div>
		<div class="bookingdetlis_text">
			<h4>Booking Details</h4>
			<p>Booked By: {{ $booking->user->first_name .' '.$booking->user->last_name }}
				<a href="{{ route('admin.users.show',$booking->user->id) }}"> <span>(View Profile)</span></a>
			</p>
			<p>Booked Date: {{ $booking->created_date.' MDT' }}</p>
			<p>Seats Booked: {{ $booking->trip->total_seats-$booking->trip->available_seats }}</p>
		</div>
		<div class="Tripdetlis_mandivtextbox">
			<div class="Tripdetlis_textbox">
				<h4>Trip Datails</h4>
				<p>Travel Date: {{ $booking->trip->trip_dated }}</p>
			</div>
			<div class="mandivsteetleft_textbox">
				<div class="steetleft_textbox">
					<h4>From</h4>
					<h3>{{ $booking->trip->from_location->work_city->name }}</h3>
					<p>{{ $booking->trip->from_location->name }}</p>
				</div>
				<div class="distancearrow_div">
					<img src="{{ asset('admin_assets/svg/right-arrow.svg') }}">
				</div>
				<div class="steetleft_textbox">
					<h4>to</h4>
					<h3>{{ $booking->trip->to_location->work_city->name }}</h3>
					<p>{{ $booking->trip->to_location->name }}</p>
				</div>
			</div>
			<div class="mandivsteetleft_textbox">
				<div class="pickuploction_detlis">
					<p>Pick-up location & Time</p>
					<h4><!-- {{ $booking->trip->from_location->name }} -->
						<?php
						echo wordwrap($booking->trip->from_location->name,40,'<br>');
						?>
						<!-- <a href="{{ route('admin.maps') }}"><span>(View on Map)</span></a> -->
						<a href="http://maps.google.com/?q={{ $booking->trip->from_location->latitude.','.$booking->trip->from_location->longitude }}" target="_blank"><span>(View on Map)</span></a>
					</h4>
					<h4>{{ $booking->picked_uptime }}</h4>

				</div>
			</div>
			<div class="mandivsteetleft_textbox">
				<div class="pickuploction_detlis">
					<p>Drop-off location:</p>
					<h4><!-- {{ wordwrap($booking->trip->to_location->name,25,'\n') }} --> 
						<?php
						echo wordwrap($booking->trip->to_location->name,40,'<br>');
						?>
						<a href="http://maps.google.com/?q={{ $booking->trip->to_location->latitude.','.$booking->trip->to_location->longitude }}" target="_blank">
							<span>(View on Map)</span>
						</a>
					</h4>
					<h4>{{$booking->dropoff_on}}</h4>
					
				</div>
			</div>
			<div class="mandivsteetleft_textbox">
				<div class="about_textdetlisbox">
					<h4>About Driver</h4>
					<p>Name:{{ $booking->trip->driver->user->first_name.' '.$booking->trip->driver->user->last_name }}
						<a href="{{ route('admin.users.show',$booking->trip->driver->user->id) }}"><span>(View Profile)</span></a>
					</p>
					@if(isset($booking->trip->vehicle))
					<p>Car:  {{ $booking->trip->vehicle->car_model->model_name }} - color : {{ $booking->trip->vehicle->color }}</p>
					@endif
					<p>Total rides: {{ $totalRides-1 }}</p>
				</div>
			</div>
			@if($booking->payment != '')
			<div class="mandivsteetleft_textbox">
				<div class="billingdetlis_textbox">
					<h4>Billing Details</h4>
					<p>Payment Method : {{ ucfirst($charge->source['funding']).' Card ending in '.$charge->source['last4'] }}</p>
					<?php
					$number = $booking->user->mobile_no;
					?>
					<p>Phone Number : {{ substr($number, 0 ,1).'xx-xxx-xxx'.substr($number, -1,strlen($number)) }}</p>
				</div>
			</div>
			@endif
			<div class="billinginner_detlisdiv">
				<div class="billingleft_text">
					<p>Price / seat :</p>
				</div>
				<div class="billingright_text">
					<p>${{ number_format($booking->trip->price,2) }}</p>
				</div>
			</div>
			<div class="billinginner_detlisdiv">
				<div class="billingleft_text">
					<p>Number of Seats: </p>
				</div>
				<div class="billingright_text">
					<p>{{ $booking->seats }} (Adults)</p>
				</div>
			</div>
			<div class="billinginner_detlisdiv">
				<div class="billingleft_text">
					<p>Base Fare: </p>
				</div>
				<div class="billingright_text">
					<p> ${{ number_format($booking->base_fare,2) }} </p>
				</div>
			</div>
			<div class="billinginner_detlisdiv">
				<div class="billingleft_text">
					<p>Tax ({{$booking->tax_percentage}}%) :</p>
				</div>
				<div class="billingright_text">
					<p> ${{number_format($booking->tax_fare,2)}} </p>
				</div>
			</div>
			<div class="billinginner_detlisdiv">
				<div class="billingleft_text">
					<p>Total: </p>
				</div>
				<div class="billingright_text">
					<p>${{ number_format($booking->chargeable_amount,2) }}</p>
				</div>
			</div>
			<div class="billinginner_detlisdiv">
				<div class="billingleft_text">
					<p>Payment Received: </p>
				</div>
				<div class="billingright_text">
					<p>$@if($booking->payment != ''){{ number_format($booking->payment->amount,2) }} @else 0.00 @endif</p>
				</div>
			</div>
			@if($booking->status == 'cancelled' ||  $booking->trip->status == 'cancelled')
			<div class="billinginner_detlisdiv">
				<div class="billingleft_text">
					<p>Refunded Amount: </p>
				</div>
				<div class="billingright_text">
					<p>$@if($booking->payment != ''){{ number_format($booking->payment->refunded_amount,2) }} @else 0.00 @endif</p>
				</div>
			</div>
			@endif

			<div class="billinginner_detlisdiv">
				<div class="billingleft_text">
					<p>Balance Due: </p>
				</div>
				<div class="billingright_text">
					@if($booking->payment != '')
					<p>$0.00</p>
					@else
					<p>${{ number_format($booking->chargeable_amount,2) }}</p>
					@endif
				</div>
			</div>
			<!-- <div class="mandivsteetleft_textbox">
				<div class="billingdetlis_textbox">
					<h4>Booking History</h4>
					{{ ($booking->status=='cancelled')?'<p>Cancelled At:'.date('M-d,Y',strtotime($booking->updated_at)).'</p>':'' }}
					
					<p>Driver Trip Cancel Date: {{ ($booking->trip->status == 'cancelled')?date('M-d,Y',strtotime($booking->trip->updated_at)):"-" }}</p>
				</div>
			</div> -->
		</div> 
	</div> 
</div>
@endsection

@section('scripts')
<script type="text/javascript">
$('#send_refund').click(function(e) {
	location.replace('{{ route('admin.trip.giveBookingRefund',$booking->id) }}')
})
</script>
@endsection