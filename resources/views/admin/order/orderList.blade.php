@extends('admin.layouts.admin-app')

@section('content')

<style type="text/css">
	td > a{
    	color: #482080;
    }
</style>
<div class="page-wrapper ">
    <!-- Main Content -->
    <div class="container-fluid">
        <!-- <div class="back_button">
            <a href="{{route('admin.users.index')}}"><button class="btn btn btn-success">Back</button></a>
        </div> -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
              <h5 class="txt-dark">Bookings list</h5>
            </div>
          <!-- Breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
                    <li class="active"><span> Booking list</span></li>
                </ol>
            </div>
            <!-- /Breadcrumb -->
        </div>
    <!--  -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default card-view settableboxadd">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark"></h6>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="table-wrap">
                                <div class="table-responsive users_detateblebox">
                                    <table id="oraderTable" class="table table-striped table-bordered" style="width:100%">
										<thead>
											<tr>
												<th>Sr.</th>
												<th>Booking Date</th>
												<th>Booking No.</th>
												<th>Booking By (Rider)</th>
												<th>Status</th>
												<!-- <th>Seat Booked</th> -->
												<!-- <th>Price</th> -->
												<th>Total Amount</th>
												<th>TO Driver</th>
												<th>Ridechimp Fees</th>
												<th>Taxes</th>
												<th>Pending Seats</th>
												<!-- <th>Transfer Fare</th> -->
											</tr>
										</thead>

									</table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
@section('scripts')
<script type="text/javascript">
	var table=$('#oraderTable').DataTable({
		dom: 'Bfrtip',
		// buttons: [
		// 'copy', 'csv', 'excel', 'pdf', 'print'
		// ],
		"order"      : [[ 0, 'desc' ]],
		"serverSide": true,
		"sServerMethod": "GET",
		"lengthMenu": [[10, 50, 100, -1], [10, 50, 100, "All"]],
		"ajax": {
			url: "{{route('admin.getOrderList')}}",
			data: function ( d ) {
				d._token = "{{ csrf_token() }}";
				d.started_at=$('#started_at').val();
				d.completed_at=$('#completed_at').val();
			}
		},
		"columns":[
			{ data:'DT_RowIndex',name:'id'},
			{ data:'date',name:'date',"width": "13%"},
			{ data:'order_id',name:'order_id',"width": "13%"},
			{ data:'name',name:'name',"width": "20%"},
			//{ data:'seats',name:'seats',"width": "5%"},
			{ data:'status',name:'status',"width": "13%"},
			{ data:'total',name:'total',"width": "13%"},
			//{ data:'price',name:'price',"width": "10%"},
			{ data:'driver_fare',name:'driver_fare',"width": "10%"},
			//{ data:'transfer_fare',name:'transfer_fare',"width": "10%"},
			{ data:'admin_fare',name:'admin_fare',"width": "15%" },
			{ data:'tax_fare',name:'tax_fare',"width": "10%"},
			{ data:'pending_seat',name:'pending_seat',"width": "10%"},
			// { data:'action',name:'action', orderable:false, searchable:false },
		]
	});
</script>
@endsection
