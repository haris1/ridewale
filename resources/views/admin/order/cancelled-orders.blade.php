@extends('admin.layouts.admin-app')
<style>
</style>
@section('content')
<div class="page-wrapper ">
    <!-- Main Content -->
    <div class="container-fluid pt-25">
        <div class="back_button">
            <a href="{{route('admin.users.index')}}"><button class="btn btn btn-success">Back</button></a>
        </div>
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
              <h5 class="txt-dark">Cancelled Bookings</h5>
            </div>
          <!-- Breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
                    <li><a href="{{route('admin.orderList')}}">Order list</a></li>
                    <li class="active"><span> Maps</span></li>
                </ol>
            </div>
            <!-- /Breadcrumb -->
        </div>
        <!--  -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default card-view bookingsetboxpart">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark"></h6>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="form-wrap">
                                <div class="cancelledbyriders_mandiv">
                                    <div class="cancelledbookin_toptext">
                                        <h3>Cancelled by Riders</h3>
                                        <p>Status :</p>
                                        <div class="switch-field">                  
                                            <input type="radio" id="switch_left" name="maintanance_mode" value="0" checked="">
                                            <label for="switch_left">Pending</label>
                                            <input type="radio" id="switch_right" name="maintanance_mode" value="1" checked="">
                                            <label for="switch_right">Refunded</label>
                                        </div>
                                    </div>

                                    <div class="booking_detatebaldivbox">
                                        <table id="example" class="display nowrap" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>Sr</th>
                                                    <th>Order Date</th>
                                                    <th>Booking No.</th>
                                                    <th>Cancelled D.</th>
                                                    <th>Amount Rcvd.</th>
                                                    <th>Refund Amt.</th>
                                                    <th>penalty Amt</th>
                                                    <th>Taxes</th>
                                                    <th>Net Amt.</th>
                                                    <th>More</th>
                                                    <th>More</th>
                                                    <th>More</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Tiger</td>
                                                    <td>Nixon</td>
                                                    <td>System Architect</td>
                                                    <td>Edinburgh</td>
                                                    <td>61</td>
                                                    <td>2011/04/25</td>
                                                    <td>$320,800</td>
                                                    <td>5421</td>
                                                    <td>5421</td>
                                                    <td>5421</td>
                                                    <td>5421</td>
                                                    <td>5421</td>
                                                </tr>
                                                <tr>
                                                    <td>Garrett</td>
                                                    <td>Winters</td>
                                                    <td>Accountant</td>
                                                    <td>Tokyo</td>
                                                    <td>63</td>
                                                    <td>2011/07/25</td>
                                                    <td>$170,750</td>
                                                    <td>8422</td>
                                                    <td>8422</td>
                                                    <td>8422</td>         
                                                    <td>8422</td>         
                                                    <td>8422</td>         
                                                </tr>
                                                <tr>
                                                    <td>Ashton</td>
                                                    <td>Cox</td>
                                                    <td>Junior Technical Author</td>
                                                    <td>San Francisco</td>
                                                    <td>66</td>
                                                    <td>2009/01/12</td>
                                                    <td>$86,000</td>
                                                    <td>1562</td>
                                                    <td>1562</td>
                                                    <td>1562</td>          
                                                    <td>1562</td>          
                                                    <td>1562</td>          
                                                </tr>
                                            </tbody>                                   
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
</script>
<script type="text/javascript">
    $(document).ready(function() {
    $('#example').DataTable( {
        responsive: {
            details: {
                type: 'column',
                target: 13
            }
        },
        columnDefs: [ {
            className: 'control',
            orderable: false,
            targets:   13
        } ]
    } );
} );
</script>
@endsection