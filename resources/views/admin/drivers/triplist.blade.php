@extends('admin.layouts.admin-app')

@section('content')
<div class="page-wrapper">
    <!-- Main Content -->
    <div class="container-fluid pt-25">
        <div class="back_button">
            <a href="{{route('admin.users.index')}}"><button class="btn btn btn-success">Back</button></a>
        </div>
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
              <h5 class="txt-dark">Driver Trip list</h5>
            </div>
          <!-- Breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
                    <li><a href="{{route('admin.users.index')}}">Users List</a></li>
                    <li class="active"><span>Driver Trip list</span></li>
                </ol>
            </div>
            <!-- /Breadcrumb -->
        </div>
    <!--  -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default card-view settableboxadd">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark"></h6>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="table-wrap">
                                <div class="table-responsive">
                                    <table id="paymet_history" class="table  table-striped table-bordered mb-0 alltextbtnandiconset1 iconsize_set pb-30" >
                                        <thead>
                                            <tr>
                                                <th>Sr. No</th>
                                                <th>From</th>
                                                <th>To</th>
                                                <th>Filled Seats</th>
                                                <th>Driver Fees</th>
                                                <th>Admin Fees</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                             @forelse($trips as $key => $trip)
                                            <tr>
                                                <td> {{ $key+1 }} </td>
                                                <td>{{($trip->from_location)?$trip->from_location->work_city->name:''}}</td>
                                                <td>{{($trip->to_location)?$trip->to_location->work_city->name:''}}</td>
                                                <td>{{ $trip->total_seats - $trip->available_seats }}</td>
                                                <td>{{ '$'.number_format($trip->bookings->sum('driver_fare'),2) }}</td>
                                                <td>{{ '$'.number_format($trip->bookings->sum('chargeable_amount'),2) }}</td>
                                                <td><a href="{{route('admin.trip.details',$trip->id)}}" class="text-inverse" title="View Detail" data-toggle="tooltip"><i class="fa fa-eye" aria-hidden="false"></i></a></td>
                                            </tr>
                                            @empty
                                            <tr>
                                                <td colspan="7" align="center"><strong>No trips there.</strong></td>
                                            </tr>
                                            @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
        
</script>
@endsection

