@extends('admin.layouts.admin-app')
@section('content')
<div class="page-wrapper">
    <!-- Main Content -->
    <div class="container-fluid pt-25">
       <div class="back_button">
        <a href="{{route('admin.ViewVehicalesList',$data[0]->id)}}"><button class="btn btn btn-success">Back</button></a>
    </div>
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h5 class="txt-dark">Add Vehicles</h5>
      </div>

      <!-- Breadcrumb -->
      <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
            <li><a href="{{route('admin.ViewVehicalesList',$data[0]->id)}}">Vehicles List</a></li>
            <li class="active"><span>Add Vehicle</span></li>
        </ol>
    </div>
    <!-- /Breadcrumb -->
</div>
<!--  -->
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default card-view">
            <div class="panel-heading">
                <div class="pull-left">

                </div>
                <div class="clearfix"></div>
            </div>
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <div class="form-wrap">
                        <form  method="POST" id="Add_vehicles" enctype="multipart/form-data" data-toggle="validator">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label mb-10 text-left"> Select Model</label>
                                        <select class="form-control selectpicker" id="model" name="model" required="" data-show-subtext="true" data-live-search="true">
                                            <option value="">Select Model</option>
                                            @foreach($models as $model)
                                            <option value="{{$model['id']}}" >{{ucfirst($model['model_name'])}}</option>
                                            @endforeach
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label mb-10 text-left" for="example-email"><span class="help">Year</span></label>
                                        <input type="text" id="year" name="year" class="form-control" maxlength="4" minlength="4" placeholder="Year" required="" data-bv-digits="true">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                   <div class="form-group">
                                    <label class="control-label mb-10 text-left" for="example-email"><span class="help">Color</span></label>
                                    <input type="text" id="color" name="color" class="form-control" placeholder="Color" required="" >
                                    <input type="hidden" name="id" name="id" value="{{$data[0]->id}}">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left" for="example-email"><span class="help">Licence Plate</span></label>
                                    <input type="text" name="licence_plate" id="licence_plate" class="form-control" placeholder="Licence Plate" required="" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <label class="control-label mb-10 text-left" for="title">Rules And Regulations</label>
                            </div>
                            <div class="col-md-6">
                                <label class="control-label mb-10 text-left" for="title">Allowed Things</label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                @foreach($allowed_things as $allowed_thing)
                                <div class="custom-control custom-checkbox">
                                  <input type="checkbox" class="custom-control-input" id="{{$allowed_thing}}_allowed_thing" name="allowed_things[]" value="{{$allowed_thing->description}}">
                                  <label class="custom-control-label" for="{{$allowed_thing}}_allowed_thing">{{$allowed_thing->description}}</label>
                              </div>
                              @endforeach
                          </div>


                          <div class="col-md-6">
                            @foreach($not_allowed_things as $not_allowed_thing)
                            <div class="custom-control custom-checkbox">
                              <input type="checkbox" class="custom-control-input" name="rules_and_regulations[]" value="{{$not_allowed_thing->description}}" id="{{$not_allowed_thing}}_not_allowed_thing">
                              <label class="custom-control-label" for="{{$not_allowed_thing}}_not_allowed_thing">{{$not_allowed_thing->description}}</label>
                          </div>
                          @endforeach
                      </div>

                  </div>
                  <div class="col-md-12">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label mb-10 text-left" for="example-email"><span class="help">Vehicle Images</span></label>
                            <input type="file" name="file[]" id="file" class="form-control" required="" multiple accept="image/*" onchange="vehicle_multiple_upload('display_vehicle_image',this)">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-12">
                        <div class="display_vehicle_image">

                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-6">
                        <button type="submit" id="assign_car" data-loading-text="Processing....." class="btn btn-success mr-10 btn-sm">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
</div>
</div>
<!-- /Row -->
</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">

   <!--  code for set ck editor -->
   // CKEDITOR.replace( 'rules_and_regulations-ckeditor' );
   // CKEDITOR.replace( 'allowed_things-ckeditor' );


   $(".fancybox").fancybox();

    //display imaes before upadte

    function vehicle_multiple_upload(div,input){
        if (input.files) {
            $('.'+div).html('');
            var filesAmount = input.files.length;
            for (C = 0; C <= filesAmount; C++) {
                var reader = new FileReader();

                reader.onload = function(event) {

                    var image = '<div class="image"><a class="fancybox"><img src="'+event.target.result+'" height="150" width="150" class="Car_upload_img"></a></div>';
                        //$($.parseHTML('<img>')).attr('src', event.target.result).height('150').width('150').To('.'+div);
                        
                        $('.'+div).append(image);

                    // var src = $('.Car_upload_img').attr('src');
                    // $(".fancybox").attr("href",src);
                }

                    //$('a.fancybox').attr("href",src);
                    reader.readAsDataURL(input.files[C]);
                }
            }
        }


        /*code for add driver address*/

        $('#Add_vehicles').submit(function(e){
            e.preventDefault();
            form=$('#Add_vehicles');
            var formData = new FormData(form[0]);       

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method:'POST',
                url:"{{route('admin.addVehiclesForm')}}",
                data:formData,
                cache:false,
                processData: false,
                contentType: false,
                beforeSend: function(){
                 $('#assign_car').button('loading');
             },
             success:function(data) {

                switch (data.status) {
                    case true:
                    toastr.success(data.message);
                    
                    window.location.href = data.location
                    //window.location = '{{url("listVehicles/'+data.user_id+'")}}';
                    break;
                    case false:
                    toastr.error(data.message);
                    break;
                }
            },
            complete: function(){
              $('#assign_car').button('reset');
          }
      });
        });
    </script>
    @endsection

