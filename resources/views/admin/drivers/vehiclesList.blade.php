@extends('admin.layouts.admin-app')

@section('content')
<div class="page-wrapper">
    <!-- Main Content -->
    <div class="container-fluid pt-25">
        <div class="back_button">
            <a href="{{route('admin.users.index')}}"><button class="btn btn btn-success">Back</button></a>
        </div>
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
              <h5 class="txt-dark">Vehicles list</h5>
            </div>
          <!-- Breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
                    <li><a href="{{route('admin.users.index')}}">Users List</a></li>
                    <li class="active"><span>Vehicles list</span></li>
                </ol>
            </div>
            <!-- /Breadcrumb -->
        </div>
    <!--  -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default card-view BoxDesignCoverInerCla1">
                    <div class="panel-heading">
                        <div class="MyVehicles_maintitlebox">
                           <!-- <p>My Vehicles</p> -->
                        </div>
                        <div class="AddVehicles_Btnbox">
                                <a href="{{route('admin.addVehicles',$data[0]->id)}}" id="Update" data-loading-text="<i class='fa fa-spinner'></i>" class="">Add Vehicle</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="vehicle_list">
                                <div class="car">
                                    @foreach($datass as $key => $datas)
                                    <div class="car_list">
                                        <div class="VehiclesListBoxdesign">
                                            {{$datas->year}} {{$datas->car_model->model_name}} - {{$datas->color}} - {{$datas->car_model->capacity}} Seats<a href="javascript:void(0);" class="delete_vehicles" data-id="{{$datas->id}}"><i class="fa fa-trash" aria-hidden="true"></i></a>

                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
        <!-- /Row -->
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
        
    $(document).on('click','.delete_vehicles',function(e){
        var id = $(this).data('id');

        e.preventDefault();

        swal({
            title: "Are you sure?",   
            text: "You will not be able to recover this information!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#e69a2a",   
            confirmButtonText: "Yes, delete it!",   
            cancelButtonText: "No, cancel please!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        },
        function(isConfirm){   
            if (isConfirm) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN':  "{{ csrf_token() }}"
                    },
                    method:'POST',
                    url:"{{route('admin.deleteVehiclesForm')}}",
                    data:{'id':id,"_token":'{{csrf_token()}}'},
                    beforeSend: function(){
                       $('#update').button('loading');
                    },
                    success:function(data) {
                        $('#docid_delete').val('');
                        switch (data.status) {
                        case true:
                        swal("Deleted!", "Your Document has been deleted.", "success");  
                        location.reload();
                        toastr.success(data.message);
                        break;
                        case false:
                        toastr.error(data.message);
                        break;
                    }
                    },
                });
            } else {
                swal("Cancelled", "Your Document is safe! :)", "error"); 
            }
        })
    });

</script>
@endsection

