<div class="col-md-12">
    <div class="col-md-6">
        <input type="hidden" name="car_driver_id" id="car_driver_id" value="{{ $res->id }}">
        <div class="form-group">
            <label class="control-label mb-10 text-left"> Select Company</label>
            <select class="form-control" id="editcompany" onchange="editcompanychange();" name="editcompany" required="">
                <option value="">Select Company</option>
                @foreach($coms as $com)
                <option value="{{$com['id']}}" @if($res->comid == $com['id']) {{'selected'}} @endif >{{ucfirst($com['name'])}}</option>
                @endforeach
            </select>
            <div class="help-block with-errors"></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group ">
            <label class="control-label mb-10 text-left"> Select Model </label>
            <select class="form-control" id="editmodel" name="editmodel" required="">
                <option value="">Select Model</option>
                @foreach($CarModels as $CarModel)
                     <option value="{{$CarModel['id']}}" @if($res->car_model_id == $CarModel['id']) {{'selected'}} @endif >{{ucfirst($CarModel['model_name'])}}</option>
                @endforeach
            </select>
            <div class="help-block with-errors"></div>
        </div>
    </div>
</div>
<div class="col-md-12">
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label mb-10 text-left" for="title">Car Number</label>
            <input type="text" id="editcarnumber" name="editcarnumber" class="form-control" placeholder="Register car Number" data-error="Tax Category Title is required !" value="{{$res->car_number}}" required="">
            <div class="help-block with-errors"></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label mb-10 text-left" for="title">Car Color</label>
            <input type="text" id="editcarcolor" name="editcarcolor" class="form-control" placeholder="Car color" data-error="Tax Category Title is required !" value="{{$res->color}}" required="">
            <div class="help-block with-errors"></div>
        </div>
    </div>
</div>
 <div class="col-md-12">
   <div class="form-group">
        <label class="control-label mb-10 text-left" for="title">Model Year</label>
        <input type="text" id="editmodel_year" name="editmodel_year" class="form-control" placeholder="Register Model Year" data-error="Medel Year is required !" value="{{$res->year}}" required="">
        <div class="help-block with-errors"></div>
    </div>
</div>
<div class="col-md-12">
    <div class="append_edit_images">
        @forelse($res->carPhotoes  as $photo)
            <img src="{{asset('storage/driver_car/')}}/{{$res->driver_id}}/{{$photo->photo}}" class="set_css" height="150" width="150" alt="image">
        @empty
        @endforelse
        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
            <div class="form-control" data-trigger="fileinput">
                <!--                                     <i class="glyphicon glyphicon-file fileinput-exists"></i> -->
                <span class="fileinput-filename"></span>
            </div>
            <span class="input-group-addon fileupload btn btn-info btn-anim btn-file">
                <!-- <i class="fa fa-upload"></i> -->
                <span class="fileinput-new btn-text">Select Car Image</span>
                <span class="fileinput-exists btn-text">Change</span>
                <input type="file" id="file" name="file[]" multiple>
                <input type="hidden" id="driver_id" name="driver_id" value="{{$res->driver_id}}">
            </span>
            <a href="#" class="input-group-addon btn btn-danger btn-anim fileinput-exists" data-dismiss="fileinput">
                <!-- <i class="fa fa-trash"></i> -->
                <span class="btn-text"> Remove</span>
            </a>
            <div class="help-block with-errors"></div>
        </div>
    </div>
</div>
<div class="col-md-12">
    <div class="form-group">
        <label class="control-label mb-10 text-left" for="title">Rules And Regulations</label>
        <!-- $res->rules_and_regulations  -->
        <!-- <textarea name="rules_and_regulations[]" class="form-control" data-error="Rules And Regulations Is Required !" required=""></textarea> -->
        <div class="append_rules_regulation">
            @forelse($res->rules_and_regulations as $key => $rules)
                <div class="row">
                    <div class="col-md-10">
                        <textarea name="rules_and_regulations[]" class="form-control rules_and_regulations" data-error="Rules And Regulations Is Required !" required="">{{ $rules }}</textarea>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="col-md-2 text-right">
                        <?php
                            $rulesTotal = count($res->rules_and_regulations);  
                        ?>
                        @if($rulesTotal==$key+1)
                            <a href="javascript:void(0)" class="text-primary btn_add_rr">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                            </a>
                        @else
                            <a href="javascript:void(0)" class="text-danger btn_delete_rr">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                            </a>
                        @endif
                    </div>
                </div>
            @empty
                <div class="row">
                    <div class="col-md-10">
                        <textarea name="rules_and_regulations[]" class="form-control rules_and_regulations" data-error="Rules And Regulations Is Required !" required=""></textarea>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="col-md-2 text-right">
                        <a href="javascript:void(0)" class="text-primary btn_add_rr">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
            @endforelse
        </div>
    </div>
</div>
<div class="col-md-12">
    <div class="form-group">
        <label class="control-label mb-10 text-left" for="title">Allowed Things</label>
        <!-- $res->allowed_things -->
        <!-- <textarea name="allowed_things[]" class="form-control" data-error="Allowed Things is required !" required=""></textarea> -->
        <div class="append_allowed_things">
            @forelse($res->allowed_things as $key => $allowed_thing)
                <div class="row">
                    <div class="col-md-10">
                        <textarea name="allowed_things[]" class="form-control allowed_things" data-error="Allowed Things is required !" required="">{{ $allowed_thing }}</textarea>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="col-md-2 text-right">
                        <?php
                            $allowedThingsTotal = count($res->allowed_things);  
                        ?>
                        @if($allowedThingsTotal==$key+1)
                            <a href="javascript:void(0)" class="text-primary btn_add_allowed_things">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                            </a>
                        @else
                            <a href="javascript:void(0)" class="text-danger btn_delete_allow_things">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                            </a>
                        @endif
                    </div>
                </div>
            @empty
                <div class="row">
                    <div class="col-md-10">
                        <textarea name="allowed_things[]" class="form-control allowed_things" data-error="Allowed Things is required !" required=""></textarea>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="col-md-2 text-right">
                        <a href="javascript:void(0)" class="text-primary btn_add_allowed_things">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
            @endforelse
        </div>
    </div>
</div>