@extends('admin.layouts.admin-app')
@section('content')
<style type="text/css">
.set_css{
    margin-right: 15px;
    margin-left: 12px;
    margin-top: 13px;
    border-radius: 10px;
    object-fit: cover;
}

/* DROPEZONE CSS  */
.dropzone {
    background: white;
    border-radius: 5px;
    border: 2px dashed rgb(0, 135, 247);
    border-image: none;
    max-width: 500px;
    margin-left: auto;
    margin-right: auto;
}

.modal,
.modal-backdrop {
    display: none;
}

.modal.open,
.modal-backdrop.open {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
}

.modal {
    display: none; /* Hidden by default */
    padding-top: 100px; /* Location of the box */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}
.modal-dialog{margin:auto;}
.modal-content {
    position: relative;
    margin: auto;
    animation-name: animatetop;
    animation-duration: 0.4s
}

@keyframes animatetop {
    from {top:-300px; opacity:0}
    to {top:0; opacity:1}
}
</style>
<div class="page-wrapper">

    <!-- Main Content -->
    <div class="container-fluid pt-25">
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
              <h5 class="txt-dark">Driver Details</h5>
          </div>
          <!-- Breadcrumb -->
          <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
              <ol class="breadcrumb">
                <li><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
                <li class="active"><span>Driver Details</span></li>
            </ol>
        </div>
        <!-- /Breadcrumb -->
    </div>
    <!-- Row -->
    <div class="row">
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
            <div class="panel panel-default card-view pa-0">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body pa-0">
                        <div class="sm-data-box bg-green">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-xs-8 text-center pl-0 pr-0 data-wrap-left boxcontentsetinerbox1">

                                        <span class="txt-light block counter"><span class="counter-anim">{{$totaltrip}}</span></span>
                                        <span class="weight-500 uppercase-font txt-light block">Total Trips</span>
                                    </div>
                                    <div class="col-xs-4 text-center  pl-0 pr-0 data-wrap-right boxconiconsetinerbox1">
                                        <i class="zmdi zmdi-play-for-work txt-light data-right-rep-icon"></i>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
            <div class="panel panel-default card-view pa-0">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body pa-0">
                        <div class="sm-data-box bg-blue">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-xs-8 text-center pl-0 pr-0 data-wrap-left boxcontentsetinerbox1">

                                        <span class="txt-light block counter">$<span class="counter-anim">{{ number_format($earning_detail[0]->total_income,2) }}</span></span>
                                        <span class="weight-500 uppercase-font txt-light block">Total Completed Request</span>
                                    </div>
                                    <div class="col-xs-4 text-center pl-0 pr-0 data-wrap-right boxconiconsetinerbox1">
                                        <i class="zmdi zmdi-play-for-work txt-light data-right-rep-icon"></i>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
            <div class="panel panel-default card-view pa-0">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body pa-0">
                        <div class="sm-data-box bg-red">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-xs-8 text-center pl-0 pr-0 data-wrap-left boxcontentsetinerbox1">

                                        <span class="txt-light block counter">$<span class="counter-anim">{{ number_format($earning_detail[0]->admin_commission,2) }}</span></span>
                                        <span class="weight-500 uppercase-font txt-light block">Total Commission</span>
                                    </div>
                                    <div class="col-xs-4 text-center  pl-0 pr-0 data-wrap-right boxconiconsetinerbox1">
                                        <i class="zmdi zmdi-money-box txt-light data-right-rep-icon"></i>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- /Row-->
    <div class="row">

        <div class="col-lg-3 col-md-4 col-sm-5 col-xs-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Profile Picture</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in profile_picsetbox">

                 <img src="@if(!empty($data[0]['profile_pic'])) {{asset('storage/images/users/')}}/{{$data[0]['id']}}/profile_pic/{{$data[0]['profile_pic']}} @else {{asset('admin_assets/img/logo.png')}} @endif" id="profile_pic" height="276" alt="image description" style="width:100%">
             </div>
         </div>
     </div>
     <div class="col-lg-9 col-md-8 col-sm-7 col-xs-12">
        <div class="panel panel-default card-view settableboxadd">
            <div class="panel-heading topbtnand_titlesetinbox1">
                <div class="pull-left">
                    <h6 class="panel-title txt-dark">Driver Detail</h6>
                </div>
                <div class="pull-right">
                    <a href="javascript:void(0);" class="btn btn-success edit_driver" data-id="{{$data[0]->id}}" data-turbolinks-action="replace"><span class="btn-text">Edit</span></a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="panel-wrapper collapse in">
                <div class="panel-body row pa-0">
                    <div class="table-wrap">
                        <div class="table-responsive">
                          <table class="table table-hover mb-0">
                        <!-- <thead>
                          <tr>
                            <th>Property</th>
                            <th>Value</th>
                        </tr>
                    </thead> -->

                    <tbody>
                      <tr>
                        <th>Name</th>
                        <td>{{$data[0]->first_name.' '.$data[0]->last_name}}</td>
                    </tr>
                    <tr>
                        <th>Mobile No.</th>
                        <td>{{$data[0]->mobile_no}}</td>
                    </tr>
                    <tr>
                        <th>Date of Birth</th>
                        <td>{{$data[0]->dob}}</td>
                    </tr>
                    <tr>
                        <th>Gender</th>
                        <td>{{$data[0]->gender}}</td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <td>{{$data[0]->email}}</td>
                    </tr>
                    <tr>
                        <th>Driver Documents</th>
                        <td>
                            <a href="{{route('admin.driverManageDocuments.view',$data[0]->id)}}" class="btn btn-success " data-id="{{$data[0]->id}}" data-turbolinks-action="replace"><span class="btn-text">Manage Documents</span></a>
                        </td>
                    </tr>
                    <tr>

                        <th>Payment Infromation</th>
                        <td>
                            <a href="javascript:void(0);" data-toggle="modal" data-target="#paymentInfo" class="btn btn-success " data-id="{{$data[0]->id}}" data-turbolinks-action="replace"><span class="btn-text">Manage Details</span></a>
                            <?php $readonly=''; ?>
                            @if(empty($accont_info))<span class="label label-danger">{{'Pending'}}</span>@elseif(empty($accont_info->verification->fields_needed))<span class="label label-success"> {{'Verified'}}</span><?php $readonly='readonly'; ?>@else<span class="label label-primary"> {{'Unverified'}}</span>@endif
                        </td>
                    </tr>
                    <tr>
                        <th>Account Status</th>
                        <td>
                            <input type="checkbox" data-id="{{$data[0]->id}}" class="js-switch js-switch-1 driver_Profile_approval" data-color="#61A42C" data-secondary-color="#ff0000" data-switchery="false" @if($data[0]->is_approved == 1) checked @endif/>
                        </td>
                    </tr>
                    
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>
<div class="row">
    <div class="">
        <div class="col-md-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Message to driver</h6>
                    </div>

                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="form-wrap">
                            <form method="post" action="">
                                <div class="form-group">
                                    <textarea class="form-control" id="user_comment" rows="5"></textarea>
                                </div>
                                <input type="hidden" id="driver_data" value="{{$data[0]->id}}">
                                <button type="button" id="mail" data-loading-text="<i class='fa fa-spinner spin'></i>" data-id="{{$data[0]->id}}" class="btn btn-success btn-anim send_approval_mail_comment"><!-- <i class="icon-rocket"> --></i><span class="btn-text">submit</span></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-md-11 col-sm-10 col-xs-12">
        <div class="panel panel-default card-view settableboxadd">
            <div class="panel-heading">
                <div class="pull-left">
                    <h6 class="panel-title txt-dark">Driver Car Detail</h6>
                </div>
                <div class="pull-right">
                    <button class="btn btn-success" data-turbolinks-action="replace" data-toggle="modal" data-target="#addcardetails"><span class="btn-text">Add</span>
                    </button>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <div class="table-wrap">
                        <div class="table-responsive">
                            <table id="car_of_drivers" class="table  table-striped table-bordered mb-0 alltextbtnandiconset1 iconsize_set pb-30" >
                                <thead>
                                    <tr>
                                        <th>Sr. No</th>
                                        <th>Company Name</th>
                                        <th>Car Type</th>
                                        <th>Model Name</th>
                                        <th>Capacity</th>
                                        <th>Registerd Car Number</th>
                                        <th>Registerd Year</th>
                                        <th>Color</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>

<!--  Code for assign car to a driver popup -->

<div class="modal fade in" id="addcardetails" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h5 class="modal-title" id="exampleModalLabel1"> Assign Car </h5>
            </div>
            <form id="addcardetailsform" method="POST" action="{{route('admin.add.carDetails')}}" enctype="multipart/form-data" data-toggle="validator">
                @csrf
                {{ method_field('POST') }}
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label mb-10 text-left"> Select Car Company</label>
                                <select class="form-control" id="company" name="company" required="">
                                    <option value="">Select Company</option>
                                    @foreach($coms as $com)
                                    <option value="{{$com['id']}}">{{ucfirst($com['name'])}}</option>
                                    @endforeach
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group ">
                                <label class="control-label mb-10 text-left"> Select Car Model </label>
                                <select class="form-control" id="model" name="model" required="">
                                    <option value="">Select Model</option>
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label mb-10 text-left" for="title">Car Number</label>
                                <input type="text" id="carnumber" name="carnumber" class="form-control" placeholder="Register car Number" maxlength="15" minlength="7" data-error="Car Number is required !" required="">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label mb-10 text-left" for="title">Car Color</label>
                                <input type="text" id="carcolor" name="carcolor" class="form-control" placeholder="Car color" data-error="Car Color is required !" required="">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                     <div class="form-group">
                        <label class="control-label mb-10 text-left" for="title"> Car Model Year</label>
                        <input type="text" id="model_year" name="model_year" class="form-control" placeholder="Register Model Year" maxlength="4" minlength="4" data-error="Medel Year is required !" required="">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label mb-10 text-left">Car Image</label>
                        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                            <div class="form-control" data-trigger="fileinput">
                                <!--                                     <i class="glyphicon glyphicon-file fileinput-exists"></i> -->
                                <span class="fileinput-filename"></span>
                            </div>
                            <span class="input-group-addon fileupload btn btn-info btn-anim btn-file">
                                <!-- <i class="fa fa-upload"></i> -->
                                <span class="fileinput-new btn-text">Select Car Image</span>
                                <span class="fileinput-exists btn-text">Change</span>
                                <input type="file" id="file" name="file[]" multiple>
                                <input type="hidden" id="driver_id" name="driver_id" value="{{$data[0]->driver_id}}">
                            </span>
                            <a href="#" class="input-group-addon btn btn-danger btn-anim fileinput-exists" data-dismiss="fileinput">
                                <!-- <i class="fa fa-trash"></i> -->
                                <span class="btn-text"> Remove</span>
                            </a>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label mb-10 text-left" for="title">Rules And Regulations</label>
                        <!-- <textarea id="rules_and_regulations-ckeditor" name="rules_and_regulations" class="form-control" data-error="Rules And Regulations Is Required !" required=""></textarea> -->
                        <div class="append_rules_regulation">
                            <div class="row">
                                <div class="col-md-10">
                                    <textarea name="rules_and_regulations[]" class="form-control rules_and_regulations" data-error="Rules And Regulations Is Required !" required=""></textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="col-md-2 text-right">
                                    <a href="javascript:void(0)" class="text-primary btn_add_rr">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label mb-10 text-left" for="title">Allowed Things</label>
                        <!-- <textarea id="allowed_things-ckeditor" name="allowed_things" class="form-control" data-error="Allowed Things is required !" required=""></textarea> -->
                        <div class="append_allowed_things">
                            <div class="row">
                                <div class="col-md-10">
                                    <textarea name="allowed_things[]" class="form-control allowed_things" data-error="Allowed Things is required !" required=""></textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="col-md-2 text-right">
                                    <a href="javascript:void(0)" class="text-primary btn_add_allowed_things">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="append_add_car_photo">
                    </div>
                </div>
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                <button type="submit" id="insert" data-loading-text="<i class='fa fa-spinner'></i>" class="btn btn-success mr-10 btn-sm" >ADD</button>
                <input type="hidden" id="company_id">
                <input type="hidden" id="type_id">
            </div>
        </form>
    </div>
</div>
</div>

<div class="modal fade in" id="paymentInfo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h5 class="modal-title" id="exampleModalLabel1"> Bank Account Detail </h5>
            </div>
            <form id="updateBankDetail" method="POST" action="{{route('admin.add.carDetails')}}" enctype="multipart/form-data" data-toggle="validator">
                @csrf
                {{ method_field('POST') }}
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label mb-10 text-left"> First Name</label>
                                <input type="text" name="first_name" id="" value="<?php echo $accont_info->legal_entity->first_name; ?>" class="form-control" {{ $readonly }} required="">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group ">
                                <label class="control-label mb-10 text-left">Last Name</label>
                                <?php $readonly=''; ?>
                                @if(isset($accont_info->legal_entity->last_name))
                                <?php $readonly='readonly'; ?>
                                @endif
                                <input type="text" name="last_name" id="" value="<?php echo $accont_info->legal_entity->last_name; ?>" class="form-control" {{ $readonly }} required="">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                              <?php $readonly=''; ?>
                              @if(isset($accont_info->legal_entity->dob->day) && isset($accont_info->legal_entity->dob->month) && isset($accont_info->legal_entity->dob->year))
                              <?php $readonly='readonly'; ?>
                              @endif
                              <label class="control-label mb-10 text-left" for="title">Date Of Birth</label>
                              <input type="text" name="dob" id="dob" value="<?php echo $accont_info->legal_entity->dob->day.'/'.$accont_info->legal_entity->dob->month.'/'.$accont_info->legal_entity->dob->year; ?>" class="form-control" {{ $readonly }} required="" {{ $readonly }}>
                              <div class="help-block with-errors"></div>
                          </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label mb-10 text-left" for="title">City</label>
                            <input type="text" name="city" id="" value="<?php echo $accont_info->legal_entity->address->city; ?>" class="form-control" required="" {{ $readonly }}>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group col-xs-12">
                        <label class="control-label mb-10 text-left" for="title">Address</label>
                        <input type="text" name="line1" id="" value="<?php echo $accont_info->legal_entity->address->line1; ?>" class="form-control" required="" {{ $readonly }}>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="form-group">

                            <label class="control-label mb-10 text-left" for="title">Postal Code</label>
                            <input type="text" name="postal_code" id="" value="<?php echo $accont_info->legal_entity->address->postal_code; ?>" class="form-control" required="" {{ $readonly }}>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label mb-10 text-left" for="title">State Code</label>
                            <input type="text" name="state" id="" value="<?php echo $accont_info->legal_entity->address->state; ?>" class="form-control" required="" {{ $readonly }}>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    @if($readonly == '')
                    <div class="col-md-6">
                        <div class="form-group">

                            <label class="control-label mb-10 text-left" for="title">Personal Id Number</label>

                            <input type="text" name="personal_id_number" id="personal_id_number" value="<?php echo $accont_info->legal_entity->personal_id_number; ?>" class="form-control" required="" {{ $readonly }}>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    @endif
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label mb-10 text-left" for="title">Phone Number</label>
                            <input type="text" name="state" id="" value="<?php echo $accont_info->legal_entity->phone_number; ?>" class="form-control" required="" {{ $readonly }}>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="col-md-12">
                   <h5 class="modal-title"> Bank Detail : </h5>
               </div>
               <div class="col-xs-12 form-group">
                <label class="control-label mb-10 text-left"> Account Holder Name</label>

                <input type="text" name="account_holder_name" id="" value="@if(!empty($accont_info->external_accounts->data[0]))<?php echo $accont_info->external_accounts->data[0]->account_holder_name; ?>@endif" class="form-control" required="" {{ $readonly }}>
                <div class="help-block with-errors"></div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
              @if(isset($accont_info->external_accounts->data[0]->routing_number))
              <?php $routing= explode('-',$accont_info->external_accounts->data[0]->routing_number); ?> 
              @endif
              <div class="form-group listar-dashboardfield">
                <label>Transit number</label>
                <input type="text" name="transit_number" id="transit_number" value="@if(isset($accont_info->external_accounts->data[0]->routing_number))<?php echo $routing[0]; ?>@endif" placeholder="11111" class="form-control" @if(isset($accont_info->external_accounts->data[0]->routing_number)) {{ 'readonly' }} @endif required="">
            </div>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
          <div class="form-group listar-dashboardfield">
            <label>Institution number</label>
            <input type="text" name="institution_number" id="institution_number" value="@if(isset($accont_info->external_accounts->data[0]->routing_number))<?php echo $routing[1]; ?>@endif" placeholder="000" class="form-control" @if(isset($accont_info->external_accounts->data[0]->routing_number)) {{ 'readonly' }} @endif required="">
        </div>
    </div>
    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
      <div class="form-group listar-dashboardfield">
        <label>Account number</label>
        @if(!empty($accont_info->external_accounts->data[0]))
        <input type="text" name="account_no" id="account_no" value="@if(isset($accont_info->external_accounts->data[0]->last4))<?php echo '-----'.$accont_info->external_accounts->data[0]->last4; ?>@endif" class="form-control" readonly required="">
        @else
        <input type="text" name="account_no" id="account_no" value="" class="form-control" required="">
        @endif
    </div>
</div>
<div class="clearfix"></div>
@if($readonly == '')
<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
  <div class="form-group listar-dashboardfield">
    <label>Verification Document</label>
    <input type="file" name="varification_doc" id="varification_doc" value="" class="form-control" accept="image/*" required="" {{ $readonly }}>
</div>
</div>
<div class="clearfix"></div>
@endif
</div>
<div class="modal-footer">

    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
    <button type="submit" id="updatebankdetail" data-loading-text="<i class='fa fa-spinner'></i>" class="btn btn-success mr-10 btn-sm" @if(empty($accont_info->verification->fields_needed)){{'disabled'}}@endif>UPDATE</button>
</div>
</form>
</div>
</div>
</div>
<!-- edit car details popup -->
<div class="modal fade in" id="Editcardetails" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h5 class="modal-title" id="exampleModalLabel1"> Update Car </h5>
            </div>
            <form id="Editcardetailsform" method="POST" action="{{route('admin.add.carDetails')}}" enctype="multipart/form-data" data-toggle="validator">
                @csrf
                {{ method_field('PUT') }}
                <div class="modal-body" id="append_edit_popup">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" id="update" data-loading-text="<i class='fa fa-spinner'></i>" class="btn btn-success mr-10 btn-sm">Update</button>

                    <input type="hidden" id="edit_company_id">
                    <input type="hidden" id="type_id">
                    <input type="hidden" id="path" value="{{storage_path()}}">
                </div>
            </form>
        </div>
    </div>
</div>


<!-- view car details popup -->
<div class="modal fade in" id="Viewardetails" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h5 class="modal-title" id="exampleModalLabel1"> View Car </h5>
            </div>
            <form method="POST" id="view_popup_form" enctype="multipart/form-data" data-toggle="validator">
                <div class="modal-body" id="Viewardetails_popup">

                </div>
                <div class="modal-footer">

                </div>
            </form>
        </div>
    </div>
</div>

<!--  edit driver popup -->

<div class="modal fade in" id="edit_driver_profile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h5 class="modal-title" id="exampleModalLabel1">Edit Driver Profile</h5>
            </div>
            <form id="EditDriverFrofileForm" method="POST" enctype="multipart/form-data" data-toggle="validator">
                @csrf
                @method('put')
                <div class="modal-body" id="append_prodile_data">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" id="Update" data-loading-text="<i class='fa fa-spinner'></i>" class="btn btn-success mr-10 btn-sm">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">

    /*instant preview image  */

    //APPEND RULES AND REGULATION
    $('body').delegate( ".btn_add_rr", "click",function(){
        $('.btn_add_rr , .btn_delete_rr').remove();
        var html = '<div class="row"><div class="col-md-10">\
                        <textarea name="rules_and_regulations[]" class="form-control rules_and_regulations" data-error="Rules And Regulations Is Required !" required=""></textarea>\
                            <div class="help-block with-errors"></div></div><div class="col-md-2 text-right"></div></div>\
                    ';
        $('.append_rules_regulation').find('.row:last').after(html);

        $('.rules_and_regulations:last').parents('.form-group').find('.col-md-2:last').append('<a href="javascript:void(0)" class="text-primary btn_add_rr">\
                            <i class="fa fa-plus" aria-hidden="true"></i>\
                        </a>');
        $('.rules_and_regulations:not(:last)').parents('.form-group').find('.col-md-2:not(:last)').append('<a href="javascript:void(0)" class="text-danger btn_delete_rr">\
                            <i class="fa fa-trash" aria-hidden="true"></i>\
                        </a>');
    });

    
    $('body').delegate( ".btn_delete_rr", "click",function(){
        $(this).parents('.row').remove();
    })

    //ALLOW THINGS
    $('body').delegate( ".btn_add_allowed_things", "click",function(){
        $('.btn_add_allowed_things , .btn_delete_allow_things').remove();
        var html = '<div class="row"><div class="col-md-10">\
                        <textarea name="allowed_things[]" class="form-control allowed_things" data-error="Allowed Things is required !" required=""></textarea>\
                            <div class="help-block with-errors"></div></div><div class="col-md-2 text-right"></div></div>\
                    ';
        $('.append_allowed_things').find('.row:last').after(html);

        $('.allowed_things:last').parents('.form-group').find('.col-md-2:last').append('<a href="javascript:void(0)" class="text-primary btn_add_allowed_things">\
                            <i class="fa fa-plus" aria-hidden="true"></i>\
                        </a>');
        $('.allowed_things:not(:last)').parents('.form-group').find('.col-md-2:not(:last)').append('<a href="javascript:void(0)" class="text-danger btn_delete_allow_things">\
                            <i class="fa fa-trash" aria-hidden="true"></i>\
                        </a>');
    });

    
    $('body').delegate( ".btn_delete_allow_things", "click",function(){
        $(this).parents('.row').remove();
    })

    var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $($.parseHTML('<img>')).attr('src', event.target.result).attr('class', 'set_css').attr('height', '150').attr('width','150').appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };

    $(document).on('change','#file', function() {
        $('.append_upload_photo').html('');
        imagesPreview(this, 'div.append_upload_photo');
    });



    <!-- code for Edit driver profile -->

    $(document).on('click','.edit_driver',function(){

        var edit_id = $(this).data('id');
        
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method:'GET',
            url:"{{route('admin.driver.edit.profile.data')}}",
            data:{'id':edit_id},
            dataType: "html",
            success:function(data) {

                $('#append_prodile_data').html(data);
                
                $("#edit_driver_profile").modal('show');
            },
        });
    });


    /* update driver profile code */

    $('#EditDriverFrofileForm').submit(function(e){
        e.preventDefault();
        form=$('#EditDriverFrofileForm');
        var formData = new FormData(form[0]);       
        var id = $('#id').val();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method:'POST',
            url:"{{route('admin.drivers.update','')}}/"+id,
            data:formData,
            cache:false,
            processData: false,
            contentType: false,
            beforeSend: function(){
             $('#update').button('loading');
         },
         success:function(data) {
            switch (data.status) {
                case true:
                toastr.success(data.message);
                location.reload();
                break;
                case false:
                toastr.error(data.message);
                break;
            }
        },
        complete: function(){
          $('#update').button('reset');
      }
  });
    });

    <!--  code for set ck editor -->
    //CKEDITOR.replace( 'rules_and_regulations-ckeditor' );
    //CKEDITOR.replace( 'allowed_things-ckeditor' );

    <!-- code for display view popup -->

    $(document).on('click','.view_car_details_popup ',function(){
        let id = $(this).data('id');
        var url = '{{ route("admin.driverCarDetails.edit", ":id") }}';
        url = url.replace(':id', id);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method:'GET',
            url:url,
            dataType: "html",
            success:function(data) {
                $('#Viewardetails_popup').html(data);
                $('#view_popup_form input').attr('disabled', true);
                $('#view_popup_form select').attr('disabled', true);
                $('#Viewardetails').modal('show');
            },
        });
    });

    
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $($.parseHTML('<img>')).attr('src', event.target.result).attr('class', 'set_css').attr('height', '150').attr('width','150').appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };

    $('#file').on('change', function() {
        $('.append_add_car_photo').html('');
        imagesPreview(this, 'div.append_add_car_photo');
    });

    /* code for display edit images */

    $(document).on('change','#editfile', function() {
        $('.append_edit_images').html('');
        imagesPreview(this, 'div.append_edit_images');
    });

    /* List the assigned car of a driver */

    var table = $('#car_of_drivers').DataTable({
        pageLength:10,
        //processing: true,
        //serverSide: true
        //sServerMethod: "POST",
        lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
        ajax: {
            type: "post",
            url: "{{ route('admin.driverCarDetails.list') }}",
            data: function ( d ) {
                d._token = "{{ csrf_token() }}";
                d.id = $('#driver_id').val();
            },
            complete:function(){
                if( $('[data-toggle="tooltip"]').length > 0 )
                    $('[data-toggle="tooltip"]').tooltip();
            }
        },
        columns:[
        { data:'DT_RowIndex',name:'id' },
        { data:'companyname',name:'Company Name' },
        { data:'typename',name:'Car Type' },
        { data:'model_name',name:'Model Name' },
        { data:'capacity',name:'Capacity' },
        { data:'car_number',name:'Registerd Car Number' },
        { data:'year',name:'Registerd Year' },
        { data:'color',name:'Color' },
        { data:'action',name:'action', orderable:false, searchable:false },
        ]
    });

    $(document).on('click','.edit_car_details_popup',function(){
        let id = $(this).data('id');
        var url = '{{ route("admin.driverCarDetails.edit", ":id") }}';
        url = url.replace(':id', id);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method:'GET',
            url:url,
            dataType: "html",
            success:function(data) {
                $('#append_edit_popup').html(data);

                //CKEDITOR.replace( 'edit_rules_and_regulations-ckeditor' );
                //CKEDITOR.replace( 'edit_allowed_things-ckeditor' );
                $('#Editcardetails').modal('show');
            },
        });
    });

    {{-- submit data for car details --}}

    $('#Editcardetailsform').submit(function(e){
        e.preventDefault();

        /*for (instance in CKEDITOR.instances){
            CKEDITOR.instances[instance].updateElement();
        }*/


        var formData = new FormData(this);
        var id = $('#car_driver_id').val();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method:'POST',
            url:"{{route('admin.driverCarDetails.update','')}}/"+id,
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            beforeSend: function(){
             $('#update').button('loading');
         },
         success:function(data) {
            switch (data.status) {
                case true:
                toastr.success(data.message);
                location.reload();
                break;
                case false:
                toastr.error(data.message);
                break;
            }
        },
        complete: function(){
          $('#update').button('reset');
      }
  });
    });
    $('#dob').datetimepicker({
        format:'YYYY-MM-DD',
        useCurrent: false,
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-arrow-up",
            down: "fa fa-arrow-down"
        },
    }).on('dp.show', function() {
        if($(this).data("DateTimePicker").date() === null)
            $(this).data("DateTimePicker").date(moment());
    });


    $('#addcardetailsform').submit(function(e){
        e.preventDefault();

        /*for (instance in CKEDITOR.instances){
            CKEDITOR.instances[instance].updateElement();
        }*/

        var formData = new FormData(this);

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method:'POST',
            url:"{{route('admin.drivers.store')}}",
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            beforeSend: function(){
             $('#insert').button('loading');
         },
         success:function(data) {
            switch (data.status) {
                case true:
                toastr.success(data.message);
                location.reload();
                break;
                case false:
                toastr.error(data.message);
                break;
            }
        },
        complete: function(){
          $('#insert').button('reset');
      }
  });
    });

    $('#updateBankDetail').submit(function(e){
        e.preventDefault();

        var formData = new FormData(this);

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method:'POST',
            url:"{{route('admin.drivers.updateBankDetails')}}",
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            beforeSend: function(){
             $('#updatebankdetail').button('loading');
         },
         success:function(data) {
            switch (data.status) {
                case true:
                toastr.success(data.message);
                location.reload();
                break;
                case false:
                toastr.error(data.message);
                break;
            }
        },
        complete: function(){
          $('#updatebankdetail').button('reset');
      }
  });
    });

    $("#company").change(function(e){
        e.preventDefault();
        var com_id = $('#company').val();
        var data = {'com_id':com_id};

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method:'POST',
            url:'{{route('admin.carmodel.cartype')}}',
            dataType: "json",
            data:data,
            success:function(data) {
                $('#model').html(data.data);
            },
        });
    });

    function editcompanychange() {
        var com_id = $('#editcompany').val();
        var data = {'com_id':com_id};

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method:'POST',
            url:'{{route('admin.carmodel.cartype')}}',
            dataType: "json",
            data:data,
            success:function(data) {
                $('#editmodel').html(data.data);
            },
        });
    }


    /* code use for change approval for user from admin side */
    $(document).on('change','.driver_Profile_approval',function(){
        if($(this).prop("checked") == true){
            var value = 1;
        }else{
            var value = 0;
        }   
        var id = $(this).data('id');
        var data = {'id':id,'value':value};
        $.ajax({
            method:'get',
            url:'{{ route('admin.driver.approve') }}',
            data:data,
             beforeSend:function(){
                $('#mail').button('loading');
            },
            success:function(data) {
                switch (data.status){
                    case 200:
                    toastr.success(data.message);
                    break;
                    case 500:
                    toastr.error(data.message);
                    break;
                }
            },
            complete: function(){
                  $('#mail').button('reset');
                }
        });
    });

    $(document).on('click','.send_approval_mail_comment',function(){

        var id = $(this).data('id');
        var comment = $('#user_comment').val();

        if(comment != ""){

            var data = {'id':id,"comment":comment};
            $.ajax({
                method:'get',
                url:'{{ route('admin.approvals.mail') }}',
                data:data,
                beforeSend:function(){},
                success:function(data) {
                    if(data.status == true){
                        toastr.success(data.message);
                    }else{
                        toastr.error(data.message);
                    }
                },
            });

        }else{
            toastr.error('Message field is required.');
            return false;
        }
    });

    function deletecar(id){ 
        var token = "{{ csrf_token() }}";

        swal({
            title: "Are you sure?",   
            text: "You will not be able to recover this information!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#e69a2a",   
            confirmButtonText: "Yes, delete it!",   
            cancelButtonText: "No, cancel please!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        },
        function(isConfirm){   
            if (isConfirm) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN':  "{{ csrf_token() }}"
                    },
                    method:'DELETE',
                    url:"{{route('admin.driverCarDetails.destroy','')}}/"+id,
                    dataType: "json",
                    success:function(data) {
                        switch (data.status) {
                            case true:
                            swal("Deleted!", "Your information has been deleted.", "success");  
                            location.reload();
                            toastr.success(data.message);
                            break;
                            case false:
                            toastr.error(data.message);
                            break;
                        }
                    },
                });
            } else {
                swal("Cancelled", "Your information is safe! :)", "error"); 
            }
        });
    }
</script>
@endsection