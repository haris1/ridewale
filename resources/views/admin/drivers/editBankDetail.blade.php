@extends('admin.layouts.admin-app')

@section('content') 
<div class="page-wrapper">
    <!-- Main Content -->
    <div class="container-fluid pt-25">
        <div class="back_button">
            <a href="{{route('admin.users.index')}}"><button class="btn btn btn-success">Back</button></a>
        </div>
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark">Driver Bank Detail ({{ @$accont_info->legal_entity->verification->status }})</h5>
            </div>
            <!-- Breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
                    <li><a href="{{route('admin.users.index')}}">Users List</a></li>
                    <li class="active"><span>Driver Bank Detail </span></li>
                </ol>
            </div>
            <!-- /Breadcrumb -->
        </div>
        <!--  -->

        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default card-view BoxDesignCoverInerCla1">
                    <div class="panel-heading">
                        <div class="pull-left">

                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="form-wrap">
                                <form id="updateBankDetail" method="POST" action="{{route('admin.add.carDetails')}}" enctype="multipart/form-data" data-toggle="validator">
                                    @csrf
                                    {{ method_field('POST') }}
                                    <input type="hidden" name="user_id" value="{{$data[0]->id}}">
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <div class="form-group"
                                            <?php $readonly='';
                                            $first_name='';
                                            ?>
                                            @if(!empty($accont_info) && isset($accont_info->legal_entity->first_name))
                                            <?php $readonly='readonly'; 
                                            $first_name=$accont_info->legal_entity->first_name; ?>
                                            @endif
                                            <label class="control-label mb-10 text-left"> First Name</label>
                                            <input type="text" name="first_name" id="" value="<?php echo $first_name; ?>" class="form-control" {{ $readonly }} required="">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group ">
                                            <label class="control-label mb-10 text-left">Last Name</label>
                                            <?php $readonly=''; 
                                            $last_name='';
                                            ?>
                                            @if(isset($accont_info) && isset($accont_info->legal_entity->last_name))
                                            <?php $readonly='readonly';
                                            $last_name=$accont_info->legal_entity->last_name; ?>
                                            
                                            @endif
                                            <input type="text" name="last_name" id="" value="<?php echo $last_name; ?>" class="form-control" {{ $readonly }} required="">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                          <?php $readonly=''; 
                                          $dob='';
                                          ?>
                                          @if(!empty($accont_info) && isset($accont_info->legal_entity->dob->day) && isset($accont_info->legal_entity->dob->month) && isset($accont_info->legal_entity->dob->year))
                                          <?php $readonly='readonly'; 
                                          $dob=$accont_info->legal_entity->dob->day.'/'.$accont_info->legal_entity->dob->month.'/'.$accont_info->legal_entity->dob->year;?>
                                          @endif
                                          <label class="control-label mb-10 text-left" for="title">Date Of Birth</label>
                                          <input type="text" name="dob" id="dob" value="<?php echo $dob; ?>" class="form-control" required="" {{ $readonly }}>
                                          <div class="help-block with-errors"></div>
                                      </div>
                                  </div>
                                  <div class="col-md-6">
                                    <div class="form-group">
                                        <?php $readonly=''; 
                                        $city='';
                                        ?>
                                        @if(!empty($accont_info) && isset($accont_info->legal_entity->address->city))
                                        <?php $readonly='readonly'; 
                                        $city=$accont_info->legal_entity->address->city;?>
                                        @endif
                                        <label class="control-label mb-10 text-left" for="title">City</label>
                                        <input type="text" name="city" id="" value="<?php echo $city; ?>" class="form-control" required="" {{ $readonly }}>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <?php $address=''; ?>
                                @if(!empty($accont_info) && isset($accont_info->legal_entity->address->line1))
                                <?php $readonly='readonly'; 
                                $address=$accont_info->legal_entity->address->line1;?>
                                @endif
                                <div class="form-group col-xs-12">
                                    <label class="control-label mb-10 text-left" for="title">Address</label>
                                    <input type="text" name="line1" id="" value="<?php echo $address; ?>" class="form-control" required="" {{ $readonly }}>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <?php $postal_code=''; ?>
                                @if(!empty($accont_info) && isset($accont_info->legal_entity->address->postal_code))
                                <?php $readonly='readonly'; 
                                $postal_code=$accont_info->legal_entity->address->postal_code;?>
                                @endif
                                <div class="col-md-6">
                                    <div class="form-group">

                                        <label class="control-label mb-10 text-left" for="title">Postal Code</label>
                                        <input type="text" name="postal_code" id="" value="<?php echo $postal_code; ?>" class="form-control" required="" {{ $readonly }}>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <?php $state=''; ?>
                                @if(!empty($accont_info) && isset($accont_info->legal_entity->address->state))
                                <?php $readonly='readonly'; 
                                $state=$accont_info->legal_entity->address->state;?>
                                @endif
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label mb-10 text-left" for="title">State Code</label>
                                        <input type="text" name="state" id="state" value="<?php echo $state; ?>" class="form-control" required="" {{ $readonly }}>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                             <?php $personal_id_number=''; ?>
                             @if(!empty($accont_info) && !empty($accont_info->verification->fields_needed))
                             <?php $readonly='readonly'; 
                             $personal_id_number=$accont_info->legal_entity->personal_id_number;?>
                             <div class="col-md-6">
                                <div class="form-group">

                                    <label class="control-label mb-10 text-left" for="title">Personal Id Number</label>

                                    <input type="text" name="personal_id_number" id="personal_id_number" value="<?php echo $personal_id_number; ?>" class="form-control" required="">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                             @endif
                            <?php $readonly=''; $phone_number=''; ?>
                            @if(!empty($accont_info) && isset($accont_info->legal_entity->phone_number))
                            <?php $readonly='readonly'; 
                            $phone_number=$accont_info->legal_entity->phone_number;?>
                            @endif
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left" for="title">Phone Number</label>
                                    <input type="text" name="phone_number" id="" value="<?php echo $phone_number; ?>" class="form-control" required="" {{ $readonly }}>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="col-md-12">
                         <h5 class="modal-title"> Bank Detail : </h5>
                     </div>
                     <div class="col-xs-12 form-group">
                        <label class="control-label mb-10 text-left"> Account Holder Name</label>
                        <?php $readonly='';  ?>
                        @if(isset($accont_info->external_accounts->data[0]->account_holder_name))
                        <?php $readonly='readonly'; ?>
                        @endif
                        <input type="text" name="account_holder_name" id="" value="@if(!empty($accont_info->external_accounts->data[0]))<?php echo $accont_info->external_accounts->data[0]->account_holder_name; ?>@endif" class="form-control" required="" {{ $readonly }}>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                      @if(isset($accont_info->external_accounts->data[0]->routing_number))
                      <?php $routing= explode('-',$accont_info->external_accounts->data[0]->routing_number); ?> 
                      @endif
                      <div class="form-group listar-dashboardfield">
                        <label>Transit number</label>
                        <input type="text" name="transit_number" id="transit_number" value="@if(isset($accont_info->external_accounts->data[0]->routing_number))<?php echo $routing[0]; ?>@endif" placeholder="11111" class="form-control" @if(isset($accont_info->external_accounts->data[0]->routing_number)) {{ 'readonly' }} @endif required="">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                  <div class="form-group listar-dashboardfield">
                    <label>Institution number</label>
                    <input type="text" name="institution_number" id="institution_number" value="@if(isset($accont_info->external_accounts->data[0]->routing_number))<?php echo $routing[1]; ?>@endif" placeholder="000" class="form-control" @if(isset($accont_info->external_accounts->data[0]->routing_number)) {{ 'readonly' }} @endif required="">
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
              <div class="form-group listar-dashboardfield">
                <label>Account number</label>
                @if(!empty($accont_info->external_accounts->data[0]))
                <input type="text" name="account_no" id="account_no" value="@if(isset($accont_info->external_accounts->data[0]->last4))<?php echo '-----'.$accont_info->external_accounts->data[0]->last4; ?>@endif" class="form-control" readonly required="">
                @else
                <input type="text" name="account_no" id="account_no" value="" class="form-control" required="">
                @endif
            </div>
        </div>
        <div class="clearfix"></div>
        @if($readonly == '')
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group listar-dashboardfield">
            <label>Verification Document</label>
            <input type="file" name="varification_doc" id="varification_doc" value="" class="form-control" accept="image/*" required="" {{ $readonly }}>
        </div>
    </div>
    <div class="clearfix"></div>
    @endif
    <div class="colmd12fullwidth2">
        <button type="submit" id="updatebankdetail" data-loading-text="<i class='fa fa-spinner'></i>" class="btnsetm" @if(isset($accont_info->legal_entity->verification->status) && $accont_info->legal_entity->verification->status == 'verified'){{'disabled'}}@endif>Update</button>
    </div>

</form>

</div>
</div>
</div>
</div>
</div>
</div>
<!-- /Row -->
</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">

    /* code use for change approval for user from admin side */

    $('#updateBankDetail').submit(function(e){
        e.preventDefault();

        var formData = new FormData(this);

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method:'POST',
            url:"{{route('admin.drivers.updateBankDetails')}}",
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            beforeSend: function(){
               $('#updatebankdetail').button('loading');
           },
           success:function(data) {
            switch (data.status) {
                case true:
                toastr.success(data.message);
                location.reload();
                break;
                case false:
                toastr.error(data.message);
                break;
            }
        },
        complete: function(){
          $('#updatebankdetail').button('reset');
      }
  });
    });
</script>
@endsection

