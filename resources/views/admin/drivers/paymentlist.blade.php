@extends('admin.layouts.admin-app')

@section('content')
<div class="page-wrapper">
    <!-- Main Content -->
    <div class="container-fluid pt-25">
        <div class="back_button">
            <a href="{{route('admin.users.index')}}"><button class="btn btn btn-success">Back</button></a>
        </div>
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
              <h5 class="txt-dark">Driver Payment List</h5>
            </div>
          <!-- Breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
                    <li><a href="{{route('admin.users.index')}}">Users List</a></li>
                    <li class="active"><span>Driver Payment List</span></li>
                </ol>
            </div>
            <!-- /Breadcrumb -->
        </div>
    <!--  -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default card-view settableboxadd">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark">Payment List</h6>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="table-wrap">
                                <div class="table-responsive">
                                    <table id="paymet_history" class="table  table-striped table-bordered mb-0 alltextbtnandiconset1 iconsize_set pb-30" >
                                        <thead>
                                            <tr>
                                                <th>Sr. No</th>
                                                <th>User</th>
                                                <th>Trip From</th>
                                                <th>Trip To</th>
                                                <th>Total Persons</th>
                                                <th>Admin Fees</th>
                                                <th>Driver Fees</th>
                                                <!-- <th>Cashback</th> -->
                                                <th>Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                             @forelse($Payments as $key => $Payment)
                                            <tr>
                                                <td> {{ $key+1 }} </td>
                                                <td>{{$Payment->user->first_name}} {{$Payment->user->last_name}}</td>
                                                <td>{{($Payment->trip->from_location)?$Payment->trip->from_location->work_city->name:''}}</td>
                                                <td>{{($Payment->trip->to_location)?$Payment->trip->to_location->work_city->name:''}}</td>
                                                <td>{{ $Payment->total_person}}</td>
                                                <td>{{ $Payment->admin_fees}}</td>
                                                <td>{{ $Payment->driver_fees}}</td>
                                                <td>{{ $Payment->total}}</td>
                                            </tr>
                                            @empty
                                            <tr>
                                                <td colspan="8" align="center"><strong>No Payment history there.</strong></td>
                                            </tr>
                                            @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
        </div>    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
        
</script>
@endsection

