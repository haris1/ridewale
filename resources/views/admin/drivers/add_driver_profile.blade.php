@extends('admin.layouts.admin-app')
<?php //die(); ?>
@section('content') 
<div class="page-wrapper">
    <!-- Main Content -->
    <div class="container-fluid pt-25">
        <div class="back_button">
            <a href="{{route('admin.users.index')}}"><button class="btn btn btn-success">Back</button></a>
        </div>
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
              <h5 class="txt-dark">Driver Profile</h5>
            </div>
          <!-- Breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
                    <li><a href="{{route('admin.users.index')}}">Users List</a></li>
                    <li class="active"><span>Driver Profile</span></li>
                </ol>
            </div>
            <!-- /Breadcrumb -->
        </div>
    <!--  -->
   
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default card-view BoxDesignCoverInerCla1">
                    <div class="panel-heading">
                        <div class="pull-left">
                            
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="form-wrap">
                                <form  method="POST" id="Add_driver_address" data-toggle="validator">
                                    <div class="colmd12fullwidth">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label mb-10 text-left" for="example-email"><span class="help">Street Address</span></label>
                                                <input type="text" id="street_address" name="street_address" class="form-control" placeholder="Street Address" required=""  value="{{$datas['street_address']}}">
                                                <input type="hidden" name="id" id="id" value="@if(isset($User_id)){{$User_id}}@endif">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label mb-10 text-left" for="example-email"><span class="help">City</span></label>
                                                <input type="text" id="city" name="city" class="form-control" placeholder="City" required="" value="@if(isset($id)){{$datas['city']}}@endif">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="colmd12fullwidth">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label mb-10 text-left" for="example-email"><span class="help">state</span></label>
                                                <input type="text" name="state" class="form-control" placeholder="State" required=""  value="@if(isset($id)){{$datas['state']}}@endif">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                         <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label mb-10 text-left" for="example-email"><span class="help">Country</span></label>
                                                <input type="text" id="country" name="country" class="form-control" placeholder="Country" required="" value="@if(isset($id)){{$datas['country']}}@endif">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="colmd12fullwidth">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label mb-10 text-left" for="example-email"><span class="help">Postal Code</span></label>
                                                <input type="text" id="pincode" minlength="6" maxlength="6" name="pincode" class="form-control" placeholder="Postal Code" required=""  value="@if(isset($id)){{$datas['pincode']}}@endif">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        {{--<div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label mb-10 text-left" for="example-email"><span class="help">Profile Approval</span></label>
                                                <input type="checkbox" class="js-switch js-switch-1 user_Profile_approval" data-switchery="false" data-color="#dc4666" data-secondary-color="#ea6c41"  data-id="{{$id}}" @if(!empty($datas['is_approved']) && $datas['is_approved'] == '1') checked="checked" @endif/>
                                            </div>
                                        </div>  --}}    
                                    </div>
                                    <div class="colmd12fullwidth2">
                                        <button type="submit" id="Update" data-loading-text="<i class='fa fa-spinner'></i>" class="btnsetm">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Row -->
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">

    

    /*code for add driver address*/

    $('#Add_driver_address').submit(function(e){
        e.preventDefault();
        form=$('#Add_driver_address');
        var formData = new FormData(form[0]);       
        var id = $('#id').val();

        if ($('#Add_driver_address').validator('validate').has('.has-error').length === 0) {

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method:'POST',
                url:"{{route('admin.updateDriverProfile','')}}/"+id,
                data:formData,
                cache:false,
                processData: false,
                contentType: false,
                 beforeSend: function(){
                   $('#update').button('loading');
                },
                success:function(data) {
                    switch (data.status) {
                        case true:
                        toastr.success(data.message);
                        location.reload();
                        break;
                        case false:
                        toastr.error(data.message);
                        break;
                    }
                },
                complete: function(){
                  $('#update').button('reset');
                }
            });
        }
    });
</script>
@endsection

