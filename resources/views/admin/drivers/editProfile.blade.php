<div class="col-md-12">
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label mb-10 text-left"> First Name</label>
             <input type="text" id="first_name" name="first_name" class="form-control" data-error="First Name is required !" required="" value="{{$data[0]->first_name}}">
            <div class="help-block with-errors"></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label mb-10 text-left"> Last Name</label>
             <input type="text" id="last_name" name="last_name" class="form-control" data-error="Last Name is required !" required="" value="{{$data[0]->last_name}}">
            <div class="help-block with-errors"></div>
        </div>
    </div>
</div>
<div class="col-md-12">
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label mb-10 text-left"> Email</label>
             <input type="email" id="email" name="email" class="form-control" data-error="Email is required !" required="" value="{{$data[0]->email}}" disabled>
            <div class="help-block with-errors"></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label mb-10 text-left">date time pick</label>
            <div class='input-group date' id='datetimepicker1'>
                <input type='text' name="dob" class="form-control" value="{{$data[0]->dob}}" disabled/>
                <!-- <input type="hidden" id="date"  > -->
            </div>
        </div>
    </div>
</div>
<div class="col-md-12">
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label mb-10 text-left"> Mobile No. </label>
             <input type="text" id="mobile" name="mobile" class="form-control" data-error="Mobile No. is required !" required=""  value="{{$data[0]->mobile_no}}" disabled>
            <div class="help-block with-errors"></div>
        </div>
    </div>
    <div class="col-md-6">
        <!-- <div class="form-group">
            <label class="control-label mb-10 text-left"> Gender </label>
             <input type="radio" id="gender" name="gender" class="form-control" data-error="Gender is required !" required="" value="male"@if($data[0]->gender == 'male') checked @endif>Male
             <input type="radio" id="gender" name="gender" class="form-control" data-error="Gender is required !" required="" value="female" @if($data[0]->gender == 'female') checked @endif>Female
            <div class="help-block with-errors"></div>
        </div> -->
        <div class="form-group">
            <label class="control-label mb-10 text-left"> Gender </label>
            <div class="maddiv_setboxpart ">
                <div class="radio_btnsetbox ">
                    <div class="radio radio-info col-xs-12 col-md-6">
                       <input type="radio" id="gender" name="gender" class="form-control" data-error="Gender is required !" required="" value="male"@if($data[0]->gender == 'male') checked @endif>
                        <label for="male">
                            Male
                        </label>                 
                    </div>
                </div>
                 <div class="stewidthaddclass">
                        <div class="radio radio-info col-xs-12 col-md-6">
                            <input type="radio" id="gender" name="gender" class="form-control" data-error="Gender is required !" required="" value="female" @if($data[0]->gender == 'female') checked @endif>
                            <label for="female">
                                Female
                            </label>
                        </div>
                    </div>             
                </div>             
            <div class="help-block with-errors"></div>
        </div>
    </div>
</div>
<div class="col-md-12">                      
   <div class="form-group">
        <label class="control-label mb-10 text-left" for="title">Address</label>
        <textarea name="address" id="address" class="form-control" data-error="Address is required !" required="">{{$data[0]->address}}</textarea>
        <div class="help-block with-errors"></div>
    </div>
</div> 
<div class="col-md-12">
    <div class="form-group">
        <label class="control-label mb-10 text-left">Profile Image</label>
        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
            <div class="form-control" data-trigger="fileinput">
<!--                 <i class="glyphicon glyphicon-file fileinput-exists"></i> -->
                <span class="fileinput-filename"></span>
            </div>
            <span class="input-group-addon fileupload btn btn-info btn-anim btn-file">
                <!-- <i class="fa fa-upload"></i> -->
                <span class="fileinput-new btn-text">Select Profile Image</span>
                <span class="fileinput-exists btn-text">Change</span>
                <input type="file" id="file" name="file">
            </span>
            <a href="#" class="input-group-addon btn btn-danger btn-anim fileinput-exists" data-dismiss="fileinput">
                <!-- <i class="fa fa-trash"></i> -->
                <span class="btn-text"> Remove</span>
            </a>
            <div class="help-block with-errors"></div>
        </div>
    </div>
</div>
<div class="col-md-12">
    <div class="form-group">
        <div class="append_upload_photo">
            <img src="{{asset('storage/images/users/')}}/{{$data[0]->id}}/profile_pic/{{$data[0]->profile_pic}}" height="200" width="200">
            <input type="hidden" id="id" value="{{$data[0]->id}}" name="id" class="form-control">
        </div>
    </div>
</div>