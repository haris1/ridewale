@extends('admin.layouts.admin-app')

@section('content')

@section('css_ref')
    <link href="{{ asset('admin_assets/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection
 <!-- Main Content -->
<div class="page-wrapper">
    
    <div class="container-fluid">
				
				<!-- Title -->
				<div class="row heading-bg">
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
					  <h5 class="txt-dark">Drivers List</h5>
					</div>
					<!-- Breadcrumb -->
					<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
					  <ol class="breadcrumb">
						<li><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
						<li class="active"><span>Drivers List</span></li>
					  </ol>
					</div>
					<!-- /Breadcrumb -->
				</div>
				<!-- /Title -->
				
				<!-- Row -->
				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default card-view settableboxadd">
							<div class="panel-heading">
								<div class="pull-right">
									<!-- <a href="" class="btn btn-success" data-toggle="modal" data-target="#addcardetails" data-turbolinks-action="replace"><span class="btn-text">Add driver</span></a> -->
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div class="table-wrap">
										<div class="table-responsive">
											<table id="driver_list" class="table  table-striped table-bordered mb-0 alltextbtnandiconset1 iconsize_set pb-30" >
												<thead>
													<tr>
														<th>Sr. No</th>
														<th>First name</th>
														<th>Last Name</th>
														<th>Mobile</th>
														<th>Email</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>	
					</div>
				</div>
				<!-- /Row -->
			</div>
    @include('admin.layouts.footer')
</div>
<!-- /Main Content -->

<!--  edit driver popup -->

<div class="modal fade in" id="edit_driver_profile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h5 class="modal-title" id="exampleModalLabel1">Edit Driver Profile</h5>
            </div>
            <form id="EditDriverFrofileForm" method="POST" enctype="multipart/form-data" data-toggle="validator">
                @csrf
                <div class="modal-body" id="append_prodile_data">
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" id="Update" data-loading-text="<i class='fa fa-spinner'></i>" class="btn btn-success mr-10 btn-sm">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade in" id="addcardetails" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h5 class="modal-title" id="exampleModalLabel1"> Add Driver </h5>
            </div>
            <form id="addcardetailsform" method="POST" action="{{route('admin.add.carDetails')}}" enctype="multipart/form-data" data-toggle="validator">
                @csrf
                {{ method_field('POST') }}
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label mb-10 text-left" for="title">First Name</label>
                                <input type="text" id="first_name" name="first_name" class="form-control" placeholder="First Name" data-error="First Name is required !" required="">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label mb-10 text-left" for="title">Last Name</label>
                                <input type="text" id="last_name" name="last_name" class="form-control" placeholder="Last Name" data-error="Last Name is required !" required="">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                       <div class="form-group">
                            <label class="control-label mb-10 text-left" for="title"> Email</label>
                            <input type="text" id="email" name="email" class="form-control" placeholder="Email Address" maxlength="4" minlength="4" data-error="Email is required !" required="">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="col-md-12">
                       <div class="form-group">
                            <label class="control-label mb-10 text-left" for="title"> Password</label>
                            <input type="Password" id="Password" name="Password" class="form-control" placeholder="Email Address" maxlength="4" minlength="4" data-error="Email is required !" required="">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="col-md-12">
                       <div class="form-group">
                            <label class="control-label mb-10 text-left" for="title"> Phone No.</label>
                            <input type="text" id="Phone" name="Phone" class="form-control" placeholder="Phone" maxlength="10" minlength="7" data-error="Phone No. is required !" required="">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label mb-10 text-left" for="title"> Date of Birth</label>
                                <input type="text" id="dob" name="dob" class="form-control" placeholder="Date Of Birth" data-error="Date of birth is required !" required="">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                         <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label mb-10 text-left"> Gender </label>
                                <div class="maddiv_setboxpart ">
                                    <div class="radio_btnsetbox ">
                                        <div class="radio radio-info col-xs-12 col-md-6">
                                           <input type="radio" id="gender" name="gender" class="form-control" data-error="Gender is required !" required="" value="male">
                                            <label for="male">
                                                Male
                                            </label>                 
                                        </div>
                                    </div>
                                     <div class="stewidthaddclass">
                                            <div class="radio radio-info col-xs-12 col-md-6">
                                                <input type="radio" id="gender" name="gender" class="form-control" data-error="Gender is required !" required="" value="female">
                                                <label for="female">
                                                    Female
                                                </label>
                                            </div>
                                        </div>             
                                    </div>             
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                     <div class="col-md-12">
                        <div class="append_add_car_photo">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" id="insert" data-loading-text="<i class='fa fa-spinner'></i>" class="btn btn-success mr-10 btn-sm">ADD</button>
                    <input type="hidden" id="company_id">
                    <input type="hidden" id="type_id">
                </div>
            </form>
        </div>
    </div>
</div>

@section('scripts')
<script>

	//$(document).ready(function() {

    //$('.datepicker').datepicker();
//});
	
	var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $($.parseHTML('<img>')).attr('src', event.target.result).attr('class', 'set_css').attr('height', '150').attr('width','150').appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };

    $(document).on('change','#file', function() {
        $('.append_upload_photo').html('');
        imagesPreview(this, 'div.append_upload_photo');
    });


	var table = $('#driver_list').DataTable({
		pageLength:10,
		// processing: true,
		serverSide: true,
		// sServerMethod: "POST",
		lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
		ajax: {
			type: "post",
			url: "{{ route('admin.drivers.getDriversList') }}",
			data: function ( d ) {
				d._token = "{{ csrf_token() }}";
			},
			complete:function(){
				if( $('[data-toggle="tooltip"]').length > 0 )
					$('[data-toggle="tooltip"]').tooltip();
			}
		},
		columns:[
			{ data:'DT_RowIndex',name:'id' },
			{ data:'first_name',name:'first_name' },
			{ data:'last_name',name:'last_name' },
			{ data:'mobile_no',name:'mobile_no' },
			{ data:'email',name:'email' },
			{ data:'action',name:'action', orderable:false, searchable:false },
		]
	});

	$(document).on('click','.edit_driver',function(){

		var edit_id = $(this).data('id');
		
         $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method:'GET',
            url:"{{route('admin.driver.edit.profile.data')}}",
            data:{'id':edit_id},
            dataType: "html",
            success:function(data) {
            	
            	$('#append_prodile_data').html(data);
          		
          		$("#edit_driver_profile").modal('show');
            },
        });
	});

	$('#EditDriverFrofileForm').submit(function(e){
    	e.preventDefault();
  		var form = $('#EditDriverFrofileForm');
        var formData = new FormData(form[0]);		
        var id = $('#id').val();
     
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method:'POST',
            url:"{{route('admin.drivers.update','')}}/"+id,
            data:formData,
            cache:false,
            processData: false,
            contentType: false,
             beforeSend: function(){
               $('#update').button('loading');
            },
            success:function(data) {
                switch (data.status) {
                    case true:
                    toastr.success(data.message);
                    location.reload();
                    break;
                    case false:
                    toastr.error(data.message);
                    break;
                }
            },
            complete: function(){
              $('#update').button('reset');
            }
        });
    });

    $(document).on('click','.delete_driver',function(e){
    	e.preventDefault();
        var id = $(this).data('id');

        swal({
            title: "Are you sure?",   
            text: "You will not be able to recover this information!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#e69a2a",   
            confirmButtonText: "Yes, delete it!",   
            cancelButtonText: "No, cancel please!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        },
        function(isConfirm){   
            if (isConfirm) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN':  "{{ csrf_token() }}"
                    },
                    method:'DELETE',
                    url:"{{route('admin.drivers.destroy','')}}/"+id,
                    dataType: "json",
                    success:function(data) {
                        switch (data.status) {
                        case 200:
                        table.ajax.reload();
                        swal("Deleted!", "Your information has been deleted.", "success");  
                        toastr.success(data.message);
                        break;
                        case 500:
                        toastr.error(data.message);
                        break;
                    }
                    },
                });
            } else {
                swal("Cancelled", "Your information is safe! :)", "error"); 
            }
        });
    });
</script>
@endsection
@endsection