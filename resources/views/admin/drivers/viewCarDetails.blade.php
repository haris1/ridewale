<div class="col-md-12">
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label mb-10 text-left"> Select Company</label>
            <select class="form-control"  disabled>
                <option value="">Select Company</option>
                @foreach($coms as $com)
                <option value="{{$com['id']}}" @if($res[0]->comid == $com['id']) {{'selected'}} @endif >{{ucfirst($com['name'])}}</option>
                @endforeach
            </select>
            <div class="help-block with-errors"></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group ">
            <label class="control-label mb-10 text-left"> Select Type </label>
            <select class="form-control" disabled>
                <option value="">Select Type</option>
                @foreach($CarType as $Type)
                <option value="{{$Type['id']}}" @if($res[0]->typeid == $Type['id']) {{'selected'}} @endif >{{ucfirst($Type['name'])}}</option>
                @endforeach
            </select>
            <div class="help-block with-errors"></div>
        </div>
    </div>
</div>
<div class="col-md-12">
    <div class="col-md-6">
        <div class="form-group ">
            <label class="control-label mb-10 text-left"> Select Model </label>
            <select class="form-control" disabled>
                <option value="">Select Model</option>
                @foreach($CarModels as $CarModel)
                     <option value="{{$CarModel['id']}}" @if($res[0]->car_model_id == $CarModel['id']) {{'selected'}} @endif >{{ucfirst($CarModel['model_name'])}}</option>
                @endforeach
            </select>
            <div class="help-block with-errors"></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group ">
            <label class="control-label mb-10 text-left"> Select Capacity </label>
            <select class="form-control" disabled>
                <option value="">Select Capacity</option>
                <option value="1" @if($res[0]->capacity == 1) {{'selected'}} @endif >1</option>
                <option value="2" @if($res[0]->capacity == 2) {{'selected'}} @endif >2</option>
                <option value="3" @if($res[0]->capacity == 3) {{'selected'}} @endif >3</option>
                <option value="4" @if($res[0]->capacity == 4) {{'selected'}} @endif >4</option>
            </select>
            <div class="help-block with-errors"></div>
        </div>
    </div>
</div>
<div class="col-md-12">
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label mb-10 text-left" for="title">Car Number</label>
            <input type="text" class="form-control" placeholder="Register car Number" data-error="Tax Category Title is required !" value="{{$res[0]->car_number}}" disabled>
            <div class="help-block with-errors"></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label mb-10 text-left" for="title">Car Color</label>
            <input type="text" class="form-control" placeholder="Car color" data-error="Tax Category Title is required !" value="{{$res[0]->color}}" disabled>
            <div class="help-block with-errors"></div>
        </div>
    </div>
</div>
<div class="col-md-6">
   <div class="form-group">
        <label class="control-label mb-10 text-left" for="title">Model Year</label>
        <input type="text" class="form-control" placeholder="Register Model Year" data-error="Medel Year is required !" value="{{$res[0]->year}}" disabled>
        <div class="help-block with-errors"></div>
    </div>
</div>
<div class="col-md-12">
    <div class="append_edit_images">
        @foreach($res as $row)
            <img src="{{asset('storage/driver_car/')}}/{{$row->photo}}" class="set_css" height="150" width="150" alt="image">
        @endforeach
    </div>
</div>