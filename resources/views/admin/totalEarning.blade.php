@extends('admin.layouts.admin-app')

@section('content')

@section('css_ref')
<link href="{{ asset('admin_assets/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection
<!-- Main Content -->
<div class="page-wrapper">

	<div class="container-fluid">
		<!-- Title -->
		<div class="row heading-bg">
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
				<h5 class="txt-dark">Payment</h5>
			</div>
			<!-- Breadcrumb -->
			<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
				<ol class="breadcrumb">
					<li><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
					<li class="active"><span>Payment</span></li>
				</ol>
			</div>
			<!-- /Breadcrumb -->
		</div>
		<!-- /Title -->


		<div class="collectdrefudrecv">
			<div class="collectotl_stripall">
				<div class="collectotl_inerbox1">
					<div class="intextsetboxcolt">
						<h3>${{ number_format($data['totalCollect'],2) }}</h3>
						<p>Total Collected</p>
					</div>
				</div>
				<div class="collectotl_inerbox2">
					<div class="intextsetboxcolt">
						<h3 class="redtext">${{ number_format($data['stripFees'],2) }}</h3>
						<p>Stripe Fees</p>
					</div>
				</div>
				<div class="collectotl_inerbox3">
					<div class="intextsetboxcolt">
						<h3>${{ number_format($data['totalReceived'],2) }}</h3>
						<p>Total Received</p>
					</div>
				</div>
			</div>

			<div class="collectotl_stripall">
				<div class="collectotl_inerbox1">
					<h5>Collected</h5>
					<div class="intextsetboxcolt">
						<h3>${{ number_format($data['totalReceived'],2) }}</h3>
						<p>Total Collected</p>
						<h3>${{ number_format($data['taxReceived'],2) }}</h3>
						<p>Tax Recieved</p>
						<h3>${{ number_format($data['collect_otherAmount'],2) }}</h3>
						<p>Other Amount</p>
					</div>
				</div>
				<div class="collectotl_inerbox2">
					<h5>Refunded</h5>
					<div class="intextsetboxcolt">
						<h3 class="redtext">${{ number_format($data['totalRefunded'],2) }}</h3>
						<p>Total Refunded</p>
						<h3 class="redtext">${{ number_format($data['taxRefunded'],2) }}</h3>
						<p>Tax Refunded</p>
						<h3 class="redtext">${{ number_format($data['refund_otherAmount'],2) }}</h3>
						<p>Other Amount</p>
					</div>
				</div>
				<div class="collectotl_inerbox3">
					<h5>Received</h5>
					<div class="intextsetboxcolt">
						<h3>${{ number_format($data['totalReceived']-$data['totalRefunded'],2) }}</h3>
						<p>Total Received</p>
						<h3>${{ number_format($data['taxReceived']-$data['taxRefunded'],2) }}</h3>
						<p>Tax Received</p>
						<h3>${{ number_format($data['toRidechimp'],2) }}</h3>
						<p>Net Received</p>
					</div>
				</div>
			</div>

			<div class="estmttodritorid">
				<div class="estmttodritorid_child1">
					<h5>Estimated</h5>
					<div class="intextsetboxcolt">
						<h3>${{ number_format($data['estimatedEarnings'],2) }}</h3>
						<p>Estimated Earnings</p>
						<!-- <h3>${{ number_format($data['totalPenaltie'],2) }}</h3>
						<p>Total Penalties</p> -->
						<h3>${{ number_format($data['penaltiePais'],2) }}</h3>
						<p>Penalties Paid</p>
						<hr class="divaider">
						<h3 class="redtext">${{ number_format($data['netEarnings'],2) }}</h3>
						<p>Net Earnings</p>
					</div>
				</div>
				<div class="estmttodritorid_child2">
					<h5>To Drivers</h5>
					<div class="intextsetboxcolt">
						<div class="divddivbox">
							<h3>${{ number_format($data['netEarnings'],2) }}</h3>
							<p>Net Earnings</p>
						</div>
						<div class="divddivbox">
							<h3 class="redtext">${{ number_format($data['pendingCredits'],2) }}</h3>
							<p>Pending Credits</p>
						</div>
						<div class="divddivbox">
							<h3 class="redtext">${{ number_format($data['paidTodriver'],2) }}</h3>
							<p>Paid to drivers</p>
						</div>
						<div class="divddivbox">
							<h3 class="redtext">${{ number_format($data['credits'],2) }}</h3>
							<p>Credits</p>
						</div>
						<hr class="divaider">
						<div class="divddivbox">
							<h3>${{ number_format($data['penaltiePais'],2) }}</h3>
							<p>Penalties Paid</p>
						</div>
						<div class="divddivbox">
							<h3>${{ number_format($data['penaltiePending'],2) }}</h3>
							<p>Penalties Pending</p>
						</div>
					</div>
				</div>
				<div class="estmttodritorid_child3">
					<h5>To Ridechimp</h5>
					<div class="intextsetboxcolt">
						<h3>${{ number_format($data['toRidechimp'],2) }}</h3>
						<p>To Ridechimp</p>
						<h3 class="redtext">${{ number_format($data['netEarnings'],2) }}</h3>
						<p>To Driver</p>
						<!-- <h3 class="redtext">${{ number_format($data['penaltiePending'],2) }}</h3>
						<p>Pending Penalties</p> -->
						<hr class="divaider">
						<h3 class="grintext">${{ number_format($data['earning'],2) }}</h3>
						<p>Earnings</p>
					</div>
				</div>
			</div>

			<div class="collectotl_stripall">
				<div class="collectotl_inerbox1">
					<h5>Upcoming</h5>
					<div class="intextsetboxcolt">
						<h3>${{ number_format($data['upcoming'],2) }}</h3>
						<p>Pending Bookings</p>
					</div>
				</div>
			</div>
		</div>

	</div>
	@include('admin.layouts.footer')
</div>


@endsection
