@extends('admin.layouts.admin-app')
@section('styles')
<style type="text/css">
.disply_temp, .disply_transfer{
	display: none;
}
</style>
@endsection
@section('content')
<div class="page-wrapper" style="min-height: 436px;">
	<div class="container-fluid">

		<!-- Title -->
		<div class="row heading-bg">
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
				<h5 class="txt-dark">Global Settings</h5>
			</div>
			<!-- Breadcrumb -->
			<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
				<ol class="breadcrumb">
					<li><a href="javascript:void(0);">Dashboard</a></li>
					<li class="active"><span>Global Settings</span></li>
				</ol>
			</div>
			<!-- /Breadcrumb -->
		</div>
		<!-- /Title -->

		<!-- Row -->
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default card-view">
					<div class="panel-heading">
						<div class="pull-left">
<!-- 							<h6 class="panel-title txt-dark">Form Validation</h6>
 -->						</div>
						<div class="clearfix"></div>
					</div>
					<div class="panel-wrapper collapse in">
						<div class="panel-body">
							<div class="row">
								<div class="col-md-12">
									<div class="form-wrap">
										<form data-toggle="validator" role="form" novalidate="true" id="globalSettingForm">
											
											<div class="col-md-4">
												<div class="form-group has-danger">
													<div class="radio">
														<input type="radio" name="admin_charge" value="fixed" id="admin_charge_fixed" required="" @if($admin_statistic->admin_fees != '' && $admin_statistic->admin_fees != 0){{'checked'}}@endif>
														<label for="admin_charge_fixed"> Admin Fees Fixed </label>
													</div>
													<div class="radio">
														<input type="radio" name="admin_charge" id="admin_charge_percentage" value="percentage" required="" @if($admin_statistic->admin_cent != '' && $admin_statistic->admin_cent != 0){{'checked'}}@endif>
														<label for="admin_charge_percentage"> Admin Fees in Percentage </label>
													</div>
												</div>
											</div>
											<div class="col-md-4 @if($admin_statistic->admin_fees == '' || $admin_statistic->admin_fees == 0){{'disply_temp'}}@endif  admin_charge_fixed">
												<div class="form-group has-danger">
													<label for="inputName" class="control-label mb-10">Admin Fees Fixed</label>
													<input type="text" name="admin_fees" value="{{$admin_statistic->admin_fees}}" class="form-control" id="inputName" placeholder="Temprature Fixed Charge" required="">
												</div>
											</div>
											<div class="col-md-4 @if($admin_statistic->admin_cent == '' || $admin_statistic->admin_cent == 0){{'disply_temp'}}@endif admin_charge_percent">
												<div class="form-group has-danger">
													<label for="inputName" class="control-label mb-10">Admin Fees in Percentage</label>
													<input type="text" name="admin_cent" value="{{$admin_statistic->admin_cent}}" class="form-control" id="inputName" placeholder="Temprature Charge in Percentage" required="">
												</div>

											</div>
											<div class="clearfix"></div>
											<div class="col-md-4">
												<div class="form-group has-danger">
													<div class="radio">
														<input type="radio" name="transfer_charge" id="transfer_charge_fixed"  value="fixed" required="" @if($admin_statistic->transfer_fees != '' && $admin_statistic->transfer_fees != 0){{'checked'}}@endif>
														<label for="transfer_charge_percentage"> Transfer Fixed Charge </label>
													</div>
													<div class="radio">
														<input type="radio" name="transfer_charge" value="percentage" id="transfer_charge_percentage" required="" @if($admin_statistic->transfer_cent != '' && $admin_statistic->transfer_cent != 0){{'checked'}}@endif>
														<label for="transfer_charge_fixed"> Transfer Charge in Percentage</label>
													</div>
													
												</div>
											</div>
											<div class="col-md-4 @if($admin_statistic->transfer_cent == '' || $admin_statistic->transfer_cent == 0){{'disply_transfer'}}@endif transfer_percentage">
												<div class="form-group has-danger">
													<label for="inputName" class="control-label mb-10">Transfer Percentage</label>
													<input type="text" name="transfer_cent" value="{{$admin_statistic->transfer_cent}}" class="form-control" id="inputName" placeholder="Transfer Percentage" required="">
												</div>

											</div>
											<div class="col-md-4 @if($admin_statistic->transfer_fees == '' || $admin_statistic->transfer_fees == 0){{'disply_transfer'}}@endif transfer_fixed">
												<div class="form-group has-danger">
													<label for="inputName" class="control-label mb-10">Transfer Fixed Charge</label>
													<input type="text" name="transfer_fees" class="form-control" value="{{$admin_statistic->transfer_fees}}" id="inputName" placeholder="Transfer Fixed Charge" required="">
												</div>

											</div>
											<div class="clearfix"></div>
											<div class="col-xs-12">
												<h5 class="txt-dark">App Settings : </h5>
											</div>
											<hr>
											<div class="col-md-4">
												<div class="form-group has-danger">
													<label for="android_version" class="control-label mb-10">Maintenance Mode</label>

													<div class="radio">
														<input type="radio" name="maintanance_mode" value="1" id="maintanance_mode_on" required="" @if($maintanance != '' && $maintanance->maintanance_mode){{'checked'}}@endif>
														<label for="maintanance_mode_on"> On </label>
													</div>
													<div class="radio">
														<input type="radio" name="maintanance_mode" id="maintanance_mode_off"  value="0" required="" @if($maintanance != ''&& !$maintanance->maintanance_mode){{'checked'}}@endif>
														<label for="maintanance_mode_off"> Off </label>
													</div>
												</div>
											</div>
											<div class="col-md-4 ">
												<div class="form-group has-danger">
													<label for="android_version" class="control-label mb-10">Android App Version</label>
													<div class="input-group">
														<div class="input-group-addon"><i class="fa fa-android"></i></div>
														<input type="text" name="android_version" value="@if($maintanance != ''){{$maintanance->android_version}}@endif" class="form-control" id="android_version" placeholder="Android App Version" required="">
													</div>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group has-danger">
													<label for="ios_version" class="control-label mb-10">IOS App Version</label>
													<div class="input-group">
														<div class="input-group-addon"><i class="fa fa-apple"></i></div>
														<input type="text" name="ios_version" class="form-control" value="@if($maintanance != ''){{$maintanance->ios_version}}@endif" id="ios_version" placeholder="IOS App Version" required="">
													</div>
												</div>
											</div>

											<div class="clearfix"></div>
											<div class="clearfix"></div>
											<div class="col-md-4">
												<div class="form-group mb-0">
													<button type="submit" class="btn btn-success btn-anim disabled"><i class="icon-rocket"></i><span class="btn-text">submit</span></button>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /Row -->
	</div>
</div>
@endsection
@section('scripts')
<script>
	$('input[type=radio][name=admin_charge]').change(function() {
		if (this.value == 'fixed') {
			$('.admin_charge_fixed').removeClass('disply_temp');
		}else{
			$('.admin_charge_fixed').addClass('disply_temp');
		}
		if (this.value == 'percentage') {
			$('.admin_charge_percent').removeClass('disply_temp');
		}else{
			$('.admin_charge_percent').addClass('disply_temp');
		}
	});
	$('input[type=radio][name=transfer_charge]').change(function() {
		//alert(this.value);
		if (this.value == 'fixed') {
			$('.transfer_fixed').removeClass('disply_transfer');
		}else{
			$('.transfer_fixed').addClass('disply_transfer');
		}
		if (this.value == 'percentage') {
			$('.transfer_percentage').removeClass('disply_transfer');
		}else{
			$('.transfer_percentage').addClass('disply_transfer');
		}
	});


		$( "#globalSettingForm" ).submit(function( event ) {
			event.preventDefault();
			var data=$( "#globalSettingForm" ).serialize();
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				method:'POST',
				url:'{{route('admin.products.store')}}',
				data:data,
				dataType: "json",
				beforeSend:function(){},
				success:function(data) {
					switch (data.status) {
						case true:
						toastr.success(data.message);
						break;
						case false:
						toastr.error(data.message);
						break;
					}
				},
			});
		});
	
</script>
@endsection