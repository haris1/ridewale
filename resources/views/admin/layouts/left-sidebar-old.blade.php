<!-- Left Sidebar Menu -->
<div class="fixed-sidebar-left">
	<ul class="nav navbar-nav side-nav nicescroll-bar">
		<li>
			<a class="@if(Route::currentRouteName() == 'admin.dashboard') {{ 'active' }} @endif" href="{{ url('admin') }}" data-toggle="collapse" data-target="#dashboard_dr"><div class="pull-left"><i class="fa fa-tachometer" aria-hidden="true"></i></i><span class="right-nav-text">Dashboard</span></div><div class="pull-right"><!-- <i class="zmdi zmdi-caret-down"></i> --></div><div class="clearfix"></div></a>
			<ul id="dashboard_dr" class="collapse collapse-level-1">
				
			</ul>
		</li>
		<li>
			<a class="@if(Route::currentRouteName() == 'admin.users.index' || Route::currentRouteName() == 'admin.users.show') {{ 'active' }} @endif" href="{{ route('admin.users.index') }}">
				<div class="pull-left">
					<i class="fa fa-users" aria-hidden="true"></i>
					<span class="right-nav-text">Users</span>
				</div>
				<div class="clearfix"></div>
			</a>
		</li>
		<!-- <li>
			<a class="@if(Route::currentRouteName() == 'admin.drivers.index' || Route::currentRouteName() == 'admin.drivers.show' || Route::currentRouteName() == 'admin.manageDocuments.view' || Route::currentRouteName() == 'admin.driver.show') {{ 'active' }} @endif" href="{{ route('admin.drivers.index') }}">
				<div class="pull-left">
					<i class="zmdi zmdi-accounts mr-20"></i>
					<i class="fa fa-life-ring" aria-hidden="true"></i>
					<span class="right-nav-text">Drivers</span>
				</div>
				<div class="clearfix"></div>
			</a>
		</li> -->
		<li>
			<a href="{{ route('admin.carCompany.index') }}" class="@if(Route::currentRouteName() == 'admin.carCompany.index' || Route::currentRouteName() == 'admin.carCompany.show' || Route::currentRouteName() == 'admin.carType.index' || Route::currentRouteName() == 'admin.carType.show' || Route::currentRouteName() == 'admin.carModel.index' || Route::currentRouteName() == 'admin.carModel.show') {{ 'active' }} @endif" data-toggle="collapse" data-target="#cardepartment"><div class="pull-left"><i class="fa fa-briefcase" aria-hidden="true"></i><span class="right-nav-text"> Vehicle management</span></div><div class="pull-right"></div><div class="clearfix"></div></a>
			<!-- <ul id="cardepartment" class="collapse collapse-level-1">
				<li>
					<a class="@if(Route::currentRouteName() == 'admin.carCompany.index' || Route::currentRouteName() == 'admin.carCompany.show') {{ 'active-page' }} @endif" href="{{ route('admin.carCompany.index') }}">
						<div class="pull-left">
							<i class="fa fa-building" aria-hidden="true"></i>
							<span class="right-nav-text">Car Company</span>
						</div>
						<div class="clearfix"></div>
					</a>
				</li>
				<li>
					<a class="@if(Route::currentRouteName() == 'admin.carType.index' || Route::currentRouteName() == 'admin.carType.show') {{ 'active-page' }} @endif" href="{{ route('admin.carType.index') }}">
						<div class="pull-left">
							<i class="ti-car mr-20"></i>
							<span class="right-nav-text">Car Types</span>
						</div>
						<div class="clearfix"></div>
					</a>
				</li>
				<li>
					<a class="@if(Route::currentRouteName() == 'admin.carModel.index' || Route::currentRouteName() == 'admin.carModel.show') {{ 'active-page' }} @endif" href="{{ route('admin.carModel.index') }}">
						<div class="pull-left">
							<i class="fa fa-car" aria-hidden="true"></i>
							<span class="right-nav-text">Car Models</span>
						</div>
						<div class="clearfix"></div>
					</a>
				</li>
			</ul> -->
		</li>		
		<!-- <li>
			<a class="javascript:void(0);">
				<div class="pull-left">
					<i class="fa fa-cogs mr-20"></i>
					<span class="right-nav-text">Product management</span>
				</div>
				<div class="clearfix"></div>
			</a>
		</li> -->
		<li>
			<a class="@if(Route::currentRouteName() == 'admin.products.index') {{ 'active' }} @endif" href="{{ route('admin.products.index') }}">
				<div class="pull-left">
					<i class="zmdi zmdi-settings mr-20"></i>
					<span class="right-nav-text">Globel Settings</span>
				</div>
				<div class="clearfix"></div>
			</a>
		</li>
		<!-- <li>
			<a href="javascript:void(0);">
				<div class="pull-left">
					<i class="zmdi zmdi-email mr-20"></i>
					<span class="right-nav-text">Email templates</span>
				</div>
				<div class="clearfix"></div>
			</a>
		</li> -->
		<li>
			<a href="{{ route('admin.work_countries.index') }}" class="@if(Route::currentRouteName() == 'admin.work_countries.index' || Route::currentRouteName() == 'admin.work_countries.show' || Route::currentRouteName() == 'admin.work_cities.index' || Route::currentRouteName() == 'admin.work_cities.show' || Route::currentRouteName() == 'admin.work_locations.index' || Route::currentRouteName() == 'admin.work_locations.show') {{ 'active' }} @endif" data-toggle="collapse" data-target="#workdepartment"><div class="pull-left"><i class="fa fa-briefcase" aria-hidden="true"></i><span class="right-nav-text">Location management</span></div><div class="pull-right"></div><div class="clearfix"></div></a>

			<!-- <i class="zmdi zmdi-caret-down"></i> -->

			<!-- <ul id="workdepartment" class="collapse collapse-level-1">
				<li>
					<a class="@if(Route::currentRouteName() == 'admin.work_countries.index' || Route::currentRouteName() == 'admin.work_countries.show') {{ 'active' }} @endif" href="{{ route('admin.work_countries.index') }}">
						<div class="pull-left">
							<i class="zmdi zmdi-globe mr-20"></i>
							<span class="right-nav-text">Work Countries</span>
						</div>
						<div class="clearfix"></div>
					</a>
				</li>
				<li>
					<a class="@if(Route::currentRouteName() == 'admin.work_cities.index' || Route::currentRouteName() == 'admin.work_cities.show') {{ 'active' }} @endif" href="{{ route('admin.work_cities.index') }}">
						<div class="pull-left">
							<i class="zmdi zmdi-globe-alt mr-20"></i>
							<span class="right-nav-text">Work Cities</span>
						</div>
						<div class="clearfix"></div>
					</a>
				</li>
				<li>
					<a class="@if(Route::currentRouteName() == 'admin.work_locations.index' || Route::currentRouteName() == 'admin.work_locations.show') {{ 'active' }} @endif" href="{{ route('admin.work_locations.index') }}">
						<div class="pull-left">
							<i class="zmdi zmdi-google-maps mr-20"></i>
							<span class="right-nav-text">Work place</span>
						</div>
						<div class="clearfix"></div>
					</a>	
				</li>			
			</ul> -->
		</li>
		<li>
			<a class="@if(Route::currentRouteName() == 'admin.payment_history') {{ 'active' }} @endif" href="{{ route('admin.payment_history') }}">
				<div class="pull-left">
					<i class="fa fa-usd" aria-hidden="true"></i>
					<span class="right-nav-text">Payment History</span>
				</div>
				<div class="clearfix"></div>
			</a>	
		</li>
		<li>
			<a class="@if(Route::currentRouteName() == 'admin.upcoming.trips' || Route::currentRouteName() == 'admin.complete.trips' || Route::currentRouteName() == 'admin.trip.details') {{ 'active' }} @endif" href="{{ route('admin.upcoming.trips') }}">
				<div class="pull-left">
					<i class="fa fa-history" aria-hidden="true"></i>
					<span class="right-nav-text">Trip History</span>
				</div>
				<div class="clearfix"></div>
			</a>	
		</li>
		<li>
			<a class="@if(Route::currentRouteName() == 'admin.officeInformations.index' || Route::currentRouteName() == 'admin.office.create' || Route::currentRouteName() == 'admin.office.list' || Route::currentRouteName() == 'admin.office.edit') {{ 'active' }} @endif" href="{{ route('admin.officeInformations.index') }}">
				<div class="pull-left">
					<i class="fa fa-briefcase" aria-hidden="true"></i>
					<span class="right-nav-text">Office Informations</span>
				</div>
				<div class="clearfix"></div>
			</a>	
		</li>
	</ul>
</div>
<!-- /Left Sidebar Menu