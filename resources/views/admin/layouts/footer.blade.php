<!-- Footer -->
<footer class="footer container-fluid pl-30 pr-30">
    <div class="row">
        <div class="col-sm-12">
            <p>Copyright &copy; {{ date('Y') }} Ridechimp.</p>
        </div>
    </div>
</footer>
<!-- /Footer -->
