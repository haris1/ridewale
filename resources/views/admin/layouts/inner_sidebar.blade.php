<!--Left Sidebar Menu -->
<div class="fixed-sidebar-left Sidebar_NewDesignCust">
	
	<ul class="nav navbar-nav side-nav nicescroll-bar">
			<div class="sidebar_left1box">
				<h3 class="heading_newsideBar_boxsnew">Rider Profile</h3>
				<li>
					<a class="@if(Route::currentRouteName() == 'admin.users.show') {{ 'active' }} @endif" href="{{ route('admin.users.show',$data[0]->id) }}" data-toggle="collapse" data-target="#dashboard_dr"><span class="right-nav-text">Profile</span></a>
					<ul id="dashboard_dr" class="collapse collapse-level-1">
					</ul>
				</li>
				<li>
					<a class="@if(Route::currentRouteName() == 'admin.displayUserTrips') {{ 'active' }} @endif" href="{{ route('admin.displayUserTrips',$data[0]->id) }}">
						<span class="right-nav-text">Trips</span>
					</a>
				</li>
				<li>
					<a class="@if(Route::currentRouteName() == 'admin.displayUserpayments') {{ 'active' }} @endif" href="{{ route('admin.displayUserpayments',$data[0]->id) }}">
						<span class="right-nav-text">Payments</span>
					</a>
				</li>
			</div>

			@if(isset($data[0]->is_driver) && $data[0]->is_driver == '0')
			<div class="driver_button">
				<button type="button"> <a href="javascript:void(0);" data-id="{{$data[0]->id}}" id="add_driver_profile"> Add Driver Profile </a> </button>
			</div>
			@endif
			@if($data[0]->is_driver == '1')
		{{--@if(Route::currentRouteName() == 'admin.addDriverProfile' || Route::currentRouteName() == 'admin.ViewVehicalesList' || Route::currentRouteName() == 'admin.listDocuments' || Route::currentRouteName() == 'admin.displayDriverTrips' || Route::currentRouteName() == 'admin.displayDriverpayments'  || Route::currentRouteName() == 'admin.addDriverProfile'  || Route::currentRouteName() == 'admin.users.show' || Route::currentRouteName() == 'admin.displayUserpayments' || Route::currentRouteName() == 'admin.displayUserTrips' || Route::currentRouteName() == 'admin.addVehicles') --}}
			<div class="sidebar_left2box">
				<div class="status_display">
					<h3 class="heading_newsideBar_boxsnew user_Profile_approval" style="cursor: pointer;" data-id="{{$data[0]->id}}" >Status <span class="dotsetin">:</span> @if($data[0]->is_driver == '1' && $data[0]->is_approved == '1') <span class="Inactivestatt" style="color:#51A351;">Active</span> @else <span class="Inactivestatt">Inactive</span> @endif</h3>
				</div>	
{{-- <input type="checkbox" class="js-switch js-switch-1 user_Profile_approval" data-switchery="false" data-color="#dc4666" data-secondary-color="#ea6c41"  data-id="{{$data[0]->id}}" @if(!empty($data[0]->is_approved) && $data[0]->is_approved == '1') checked="checked" @endif/>--}}
				<div class="DriverPro_textsetb">
					<h4>Driver Profile</h4>
				</div>
				<li>
					<a class="@if(Route::currentRouteName() == 'admin.addDriverProfile') {{ 'active' }} @endif"  href="@if($data[0]->is_driver == '1'){{ route('admin.addDriverProfile',$data[0]->id) }}@else {{'javascript:void(0)'}} @endif" data-toggle="collapse" data-target="#dashboard_dr" disabled><span class="right-nav-text">Profile</span>@if(!$data[0]->profile)<span style="color:#ef4756;"> 
					<img src="{{ asset('admin_assets/svg/exclamation.svg') }}"></span>@endif </p></a>
				</li>
				<li>
					<a class="@if(Route::currentRouteName() == 'admin.ViewVehicalesList' || Route::currentRouteName() == 'admin.addVehicles') {{ 'active' }} @endif" href="@if($data[0]->is_driver == '1'){{ route('admin.ViewVehicalesList',$data[0]->id)}}@else {{'javascript:void(0)'}} @endif">
						<span class="right-nav-text">Vehicles</span>@if(!$data[0]->vehicles)<span style="color:#ef4756;"> <img src="{{ asset('admin_assets/svg/exclamation.svg') }}"></span>@endif 
					</a>
				</li>
				<li>
					<a class="@if(Route::currentRouteName() == 'admin.listDocuments') {{ 'active' }} @endif" href="@if($data[0]->is_driver == '1'){{ route('admin.listDocuments',$data[0]->id) }}@else {{'javascript:void(0)'}} @endif">
						<span class="right-nav-text">Documents</span>@if(!$data[0]->documents)<span style="color:#ef4756;"> <img src="{{ asset('admin_assets/svg/exclamation.svg') }}"></span>@endif 
					</a>
				</li>
				
				<li>
					<a class="@if(Route::currentRouteName() == 'admin.displayDriverTrips') {{ 'active' }} @endif" href="@if($data[0]->is_driver == '1'){{ route('admin.displayDriverTrips',$data[0]->id) }}@else {{'javascript:void(0)'}} @endif">
						<span class="right-nav-text">Trips</span> 
					</a>
				</li>
				<li>
					<a class="@if(Route::currentRouteName() == 'admin.displayDriverpayments') {{ 'active' }} @endif" href="@if($data[0]->is_driver == '1'){{ route('admin.displayDriverpayments',$data[0]->id) }}@else {{'javascript:void(0)'}} @endif">
						<span class="right-nav-text">Payments</span>
					</a>
				</li>
				<li>
					<a class="@if(Route::currentRouteName() == 'admin.editBankDetails') {{ 'active' }} @endif" href="@if($data[0]->is_driver == '1'){{ route('admin.editBankDetails',$data[0]->id) }}@else {{'javascript:void(0)'}} @endif">
						<span class="right-nav-text">Payment Detail</span>@if(!$data[0]->payment_detail)<span style="color:#ef4756;"> <img src="{{ asset('admin_assets/svg/exclamation.svg') }}"></span>@endif
					</a>
				</li>
			</div>
		@endif
	</ul>
</div>
<!-- /Left Sidebar Menu -->






