<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title> @yield('title','RideChimp | Admin Panel') </title>


  <!-- morris chart css -->


  <link rel="apple-touch-icon-precomposed" sizes="57x57" href="{{ asset('admin_assets/favicon/apple-touch-icon-57x57.png') }}" />
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('admin_assets/favicon/apple-touch-icon-114x114.png') }}" />
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('admin_assets/favicon/apple-touch-icon-72x72.png') }}" />
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('admin_assets/favicon/apple-touch-icon-144x144.png') }}" />
  <link rel="apple-touch-icon-precomposed" sizes="60x60" href="{{ asset('admin_assets/favicon/apple-touch-icon-60x60.png') }}" />
  <link rel="apple-touch-icon-precomposed" sizes="120x120" href="{{ asset('admin_assets/favicon/apple-touch-icon-120x120.png') }}" />
  <link rel="apple-touch-icon-precomposed" sizes="76x76" href="{{ asset('admin_assets/favicon/apple-touch-icon-76x76.png') }}" />
  <link rel="apple-touch-icon-precomposed" sizes="152x152" href="{{ asset('admin_assets/favicon/apple-touch-icon-152x152.png') }}" />
  <link rel="icon" type="image/png" href="{{ asset('admin_assets/favicon/favicon-196x196.png') }}" sizes="196x196" />
  <link rel="icon" type="image/png" href="{{ asset('admin_assets/favicon/favicon-96x96.png') }}" sizes="96x96" />
  <link rel="icon" type="image/png" href="{{ asset('admin_assets/favicon/favicon-32x32.png') }}" sizes="32x32" />
  <link rel="icon" type="image/png" href="{{ asset('admin_assets/favicon/favicon-16x16.png') }}" sizes="16x16" />
  <link rel="icon" type="image/png" href="{{ asset('admin_assets/favicon/favicon-128.png') }}" sizes="128x128" />

    <!-- <link rel="icon" type="image/png" href="" />
      <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16" /> -->
      <!-- <link href="{{ asset('admin_assets/vendors/bower_components/morris.js/morris.css') }}" rel="stylesheet" type="text/css"/> -->

      <!-- Chartist CSS -->
      <!-- <link href="{{ asset('admin_assets/vendors/bower_components/chartist/dist/chartist.min.css') }}" rel="stylesheet" type="text/css"/> -->

      <!-- Customizable Theme Assets -->
      <link href="{{ asset('admin_assets/vendors/bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
      <link href="{{ asset('admin_assets/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.css') }}" rel="stylesheet" type="text/css">

      <!-- bootstrap-select CSS -->
      <!-- <link href="{{ asset('admin_assets/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css"/> -->
      <!-- switchery CSS -->
      <!-- <link href="{{ asset('admin_assets/vendors/bower_components/switchery/dist/switchery.min.css') }}" rel="stylesheet" type="text/css"/> -->
      <!-- vector map CSS -->
      <!-- <link href="{{ asset('admin_assets/vendors/vectormap/jquery-jvectormap-2.0.2.css') }}" rel="stylesheet" type="text/css"/> -->

      <!-- Bootstrap Datetimepicker CSS -->
      <link href="{{ asset('admin_assets/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css"/>

      <!-- Bootstrap Daterangepicker CSS -->
      <link href="{{ asset('admin_assets/vendors/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet" type="text/css"/>
      <link rel="stylesheet" href="{{ asset('css/toastr.min.css') }}">
      <link rel="stylesheet" href="{{ asset('css/datepicker.css') }}">
      <link rel="stylesheet" href="{{ asset('css/jquery.fancybox.min.css') }}" type="text/css" media="screen" />

      <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.css"> -->
      <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.css" rel="stylesheet" type="text/css"> -->
      @yield('css_ref')
      @yield('styles')
      <link href="{{ asset('css/basic.css') }}" rel="stylesheet" type="text/css">
      <link href="{{ asset('admin_assets/full-width-light-dist/css/style.css') }}" rel="stylesheet" type="text/css">
      <link href="{{ asset('admin_assets/css/admin-style.css') }}" rel="stylesheet" type="text/css">
      <link href="{{ asset('admin_assets/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
      <link href="{{ asset('admin_assets/vendors/bower_components/datatables.net-responsive/css/responsive.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
      <!-- <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" /> -->
      <link href="{{ asset('css/sweetalert.css') }}" rel="stylesheet" type="text/css"/>
      <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6/css/select2.min.css" rel="stylesheet" /> -->

      <!-- <link href="{{ asset('css/jquery-ui.min.css') }}" rel="stylesheet" type="text/css"/> -->
      <!-- <script src="{{ asset('js/app.js') }}"></script> -->

    </head>
    <body>

      <!--Preloader-->
      <!--<div class="preloader-it">
        <div class="la-anim-1"></div>
      </div>-->
      <!--/Preloader-->

      <div id="app">
        @if(Auth::guard('admin')->check())
        <div class="wrapper theme-1-active pimary-color-red slide-nav-toggle">

            @if(Route::currentRouteName() == 'admin.users.show' || Route::currentRouteName() == 'admin.addDriverProfile' || Route::currentRouteName() == 'admin.ViewVehicalesList' || Route::currentRouteName() == 'admin.addVehicles' || Route::currentRouteName() == 'admin.listDocuments' || Route::currentRouteName() == 'admin.displayUserTrips' || Route::currentRouteName() == 'admin.displayDriverTrips' || Route::currentRouteName() == 'admin.displayDriverpayments' || Route::currentRouteName() == 'admin.displayUserpayments' || Route::currentRouteName() == 'admin.editBankDetails')
                @include('admin.layouts.header')
                @include('admin.layouts.inner_sidebar')
                @include('admin.layouts.right-sidebar')
            @else
                @include('admin.layouts.header')
                @if((Route::currentRouteName() != 'admin.orderDetails') && (Route::currentRouteName() != 'admin.trip.details'))
                    @include('admin.layouts.left-sidebar')
                @endif
                    @include('admin.layouts.right-sidebar')
                @endif
                @yield('content')
        </div>
        @else
            @yield('content')
        @endif

     </div>
   </body>

   <!-- Customizable Theme Assets -->
   <!-- jQuery -->
   <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script> -->
   <script src="{{ asset('admin_assets/vendors/bower_components/jquery/dist/jquery.min.js') }}"></script>
   <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>

   <!-- Bootstrap Core JavaScript -->
   <script src="{{ asset('admin_assets/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
   <script src="{{ asset('admin_assets/vendors/bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js') }}"></script>
   <!-- <script src="{{ asset('js/jquery-ui.min.js') }}"></script> -->

   <!-- Moment JavaScript -->
   <script type="text/javascript" src="{{ asset('admin_assets/vendors/bower_components/moment/min/moment-with-locales.min.js') }}"></script>
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6/js/select2.min.js"></script> -->

   <!-- Bootstrap Datetimepicker JavaScript -->
   <script type="text/javascript" src="{{ asset('admin_assets/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>

   <!-- Bootstrap Daterangepicker JavaScript -->
   <script src="{{ asset('admin_assets/vendors/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

   <!-- Vector Maps JavaScript -->
   <!-- <script src="{{ asset('admin_assets/vendors/vectormap/jquery-jvectormap-2.0.2.min.js') }}"></script>
   <script src="{{ asset('admin_assets/vendors/vectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
   <script src="{{ asset('admin_assets/full-width-light-dist/js/vectormap-data.js') }}"></script> -->

   <!-- Counter Animation JavaScript -->
   <!-- <script src="{{ asset('admin_assets/vendors/bower_components/waypoints/lib/jquery.waypoints.min.js') }}"></script>
   <script src="{{ asset('admin_assets/vendors/bower_components/jquery.counterup/jquery.counterup.min.js') }}"></script> -->

   <!-- Data table JavaScript -->
   <script src="{{ asset('admin_assets/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
   <script src="{{ asset('admin_assets/vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>

   <!-- Flot Charts JavaScript -->
   <!-- <script src="{{ asset('admin_assets/vendors/bower_components/Flot/excanvas.min.js') }}"></script> -->
   <!-- <script src="{{ asset('admin_assets/vendors/bower_components/Flot/jquery.flot.js') }}"></script> -->
   <!-- <script src="{{ asset('admin_assets/vendors/bower_components/Flot/jquery.flot.pie.js') }}"></script> -->
   <!-- <script src="{{ asset('admin_assets/vendors/bower_components/Flot/jquery.flot.resize.js') }}"></script>
   <script src="{{ asset('admin_assets/vendors/bower_components/Flot/jquery.flot.time.js') }}"></script>
   <script src="{{ asset('admin_assets/vendors/bower_components/Flot/jquery.flot.stack.js') }}"></script>
   <script src="{{ asset('admin_assets/vendors/bower_components/Flot/jquery.flot.crosshair.js') }}"></script>
   <script src="{{ asset('admin_assets/vendors/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js') }}"></script>
   <script src="{{ asset('admin_assets/full-width-light-dist/js/flot-data.js') }}"></script> -->

   <!-- Slimscroll JavaScript -->
   <script src="{{ asset('admin_assets/full-width-light-dist/js/jquery.slimscroll.js') }}"></script>

   <!-- simpleWeather JavaScript -->
   <script src="{{ asset('admin_assets/vendors/bower_components/moment/min/moment.min.js') }}"></script>
   <script src="{{ asset('admin_assets/js/bootstrap-datetimepicker.min.js') }}"></script>
   <script src="{{ asset('admin_assets/js/bootstrap-colorpicker.min.js') }}"></script>
   <!-- <script src="{{ asset('admin_assets/js/form-picker-data.js') }}"></script> -->
   <!-- <script src="{{ asset('admin_assets/vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js') }}"></script>
   <script src="{{ asset('admin_assets/full-width-light-dist/js/simpleweather-data.js') }}"></script> -->

   <!-- Progressbar Animation JavaScript -->
   <script src="{{ asset('admin_assets/vendors/bower_components/waypoints/lib/jquery.waypoints.min.js') }}"></script>
   <script src="{{ asset('admin_assets/vendors/bower_components/jquery.counterup/jquery.counterup.min.js') }}"></script>

   <!-- Fancy Dropdown JS -->
   <script src="{{ asset('admin_assets/full-width-light-dist/js/dropdown-bootstrap-extended.js') }}"></script>

   <!-- Sparkline JavaScript -->
   <!-- <script src="{{ asset('admin_assets/vendors/jquery.sparkline/dist/jquery.sparkline.min.js') }}"></script> -->

   <!-- Owl JavaScript -->
   <!-- <script src="{{ asset('admin_assets/vendors/bower_components/owl.carousel/dist/owl.carousel.min.js') }}"></script> -->

   <!-- ChartJS JavaScript -->
   <!-- <script src="{{ asset('admin_assets/vendors/chart.js/Chart.min.js') }}"></script> -->

   <!-- Chartist JavaScript -->
   <!-- script src="{{ asset('admin_assets/vendors/bower_components/chartist/dist/chartist.min.js') }}"></script> -->

   <!-- EasyPieChart JavaScript -->
   <!-- <script src="{{ asset('admin_assets/vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js') }}"></script> -->

   <!-- Morris Charts JavaScript -->
   <!-- <script src="{{ asset('admin_assets/vendors/bower_components/raphael/raphael.min.js') }}"></script>
   <script src="{{ asset('admin_assets/vendors/bower_components/morris.js/morris.min.js') }}"></script>
   <script src="{{ asset('admin_assets/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js') }}"></script> -->

   <!-- Switchery JavaScript -->
   <script src="{{ asset('admin_assets/vendors/bower_components/switchery/dist/switchery.min.js') }}"></script>

   <!-- Bootstrap Select JavaScript -->
   <!-- <script src="{{ asset('admin_assets/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script> -->

   <!-- Init JavaScript -->
   <script src="{{ asset('admin_assets/full-width-light-dist/js/init.js') }}"></script>
   <script type="text/javascript" src="{{ asset('/admin_assets/vendors/bower_components/bootstrap-validator/dist/validator.min.js') }}"></script>

   @if (\Request::is('admin/secondDashboard'))
   <script src="{{ asset('admin_assets/full-width-light-dist/js/dashboard2-data.js') }}"></script>
   @elseif(\Request::is('admin/thirdDashboard'))
   <script src="{{ asset('admin_assets/full-width-light-dist/js/dashboard3-data.js') }}"></script>
   <script src="{{ asset('admin_assets/full-width-light-dist/js/productorders-data.js') }}"></script>
   <script src="{{ asset('admin_assets/full-width-light-dist/js/chartist-data.js') }}"></script>
   @elseif(\Request::is('admin/fourthDashboard'))
   @elseif(\Request::is('admin'))
   <!-- <script src="{{ asset('admin_assets/full-width-light-dist/js/dashboard-data.js') }}"></script> -->
   @endif
   <script src="{{ asset('admin_assets/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
   <script src="{{ asset('js/toastr.min.js') }}"></script>
   <script src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
   <script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>

   <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/3.8.4/dropzone.js"></script>

   <!-- <script src="{{asset('js/form-advance-data.js')}}"></script> -->
   <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script> -->
   <!-- <script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.js"></script>
   <script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.auto.js"></script> -->
   <script src="{{ asset('js/sweetalert.min.js') }}"></script>
   <script src="{{ asset('js/sweetalert-data.js') }}"></script>
   <script type="text/javascript" src="//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.js"></script>
   <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>

   <script type="text/javascript">
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    if( $('[data-toggle="tooltip"]').length > 0 ){
      $('[data-toggle="tooltip"]').tooltip();
    }

    // var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
    // $('.js-switch-1').each(function() {
    //   new Switchery($(this)[0], $(this).data());
    // });

    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
    $('.js-switch-1').each(function() {
      new Switchery($(this)[0], $(this).data());
    });
    /* code use for change approval for user from admin side */

    $(document).on('click','.user_Profile_approval',function(){
      // alert();
      var token = "{{ csrf_token() }}";
      var id    = $(this).data('id');
      var data = {'id':id};

      swal({
        title: "Are you sure?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#e69a2a",
        confirmButtonText: "Yes",
        cancelButtonText: "No, cancel please!",
        closeOnConfirm: false,
        closeOnCancel: false
      },
      function(isConfirm){
        if (isConfirm) {
          $.ajax({
            headers: {
              'X-CSRF-TOKEN':  "{{ csrf_token() }}"
            },
            method:'GET',
            data:data,
            url:'{{route('admin.users.approve','')}}/'+id,
            dataType: "json",
            success:function(data) {
              switch (data.status) {
                case true:
                swal(data.message, "success");
                //toastr.success(data.message);
                location.reload();
                break;
                case false:
                toastr.error(data.message);
                break;
                default:
                toastr.error("You are not Authorized to access this page");
                break;
              }
            },
          });
        } else {
          swal("Cancelled", "Your information is safe! :)", "error");
        }
      });

    });


    $('#add_driver_profile').click(function() {
      var token = "{{ csrf_token() }}";
      var id    = $(this).data('id');
      swal({
        title: "Are you sure?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#e69a2a",
        confirmButtonText: "Yes",
        cancelButtonText: "No, cancel please!",
        closeOnConfirm: false,
        closeOnCancel: false
      },
      function(isConfirm){
        if (isConfirm) {
          $.ajax({
            headers: {
              'X-CSRF-TOKEN':  "{{ csrf_token() }}"
            },
            method:'GET',
            url:'{{route('admin.adddriver_profile','')}}/'+id,
            dataType: "json",
            success:function(data) {
              switch (data.status) {
                case true:
                swal("Driver Profile created successfully.", "success");
                //toastr.success(data.message);
                location.reload();
                break;
                case false:
                toastr.error(data.message);
                break;
                default:
                toastr.error("You are not Authorized to access this page");
                break;
              }
            },
          });
        } else {
          swal("Cancelled", "Your information is safe! :)", "error");
        }
      });
    });

  </script>
  @yield('scripts')
  </html>
