<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Plow') }}</title>
    
    <!-- morris chart css -->
    <link href="{{ asset('admin_assets//vendors/bower_components/morris.js/morris.css') }}" rel="stylesheet" type="text/css"/>

    <!-- Customizable Theme Assets -->
    <link href="{{ asset('admin_assets/vendors/bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('admin_assets/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('admin_assets/full-width-light-dist/css/style.css') }}" rel="stylesheet" type="text/css">
</head>
<body>

    <!--Preloader-->
    <div class="preloader-it">
        <div class="la-anim-1"></div>
    </div>
    <!--/Preloader-->

    <div id="app">
        @if(Auth::guard('admin')->check())
            <div class="wrapper theme-1-active pimary-color-red">
                @include('admin.layouts.header')
                @include('admin.layouts.left-sidebar')
                @include('admin.layouts.right-sidebar')
                @yield('content')
            </div>
        @else
            @yield('content')
        @endif
        
    </div>
</body>

<!-- Customizable Theme Assets -->
<!-- jQuery -->
<script src="{{ asset('admin_assets/vendors/bower_components/jquery/dist/jquery.min.js') }}"></script>
        
<!-- Bootstrap Core JavaScript -->
<script src="{{ asset('admin_assets/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('admin_assets/vendors/bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js') }}"></script>

<!-- Data table JavaScript -->
<script src="{{ asset('admin_assets/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>

<!-- Slimscroll JavaScript -->
<script src="{{ asset('admin_assets/full-width-light-dist/js/jquery.slimscroll.js') }}"></script>

<!-- simpleWeather JavaScript -->
<script src="{{ asset('admin_assets/vendors/bower_components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('admin_assets/vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js') }}"></script>
<script src="{{ asset('admin_assets/full-width-light-dist/js/simpleweather-data.js') }}"></script>

<!-- Progressbar Animation JavaScript -->
<script src="{{ asset('admin_assets/vendors/bower_components/waypoints/lib/jquery.waypoints.min.js') }}"></script>
<script src="{{ asset('admin_assets/vendors/bower_components/jquery.counterup/jquery.counterup.min.js') }}"></script>

<!-- Fancy Dropdown JS -->
<script src="{{ asset('admin_assets/full-width-light-dist/js/dropdown-bootstrap-extended.js') }}"></script>

<!-- Sparkline JavaScript -->
<script src="{{ asset('admin_assets/vendors/jquery.sparkline/dist/jquery.sparkline.min.js') }}"></script>

<!-- Owl JavaScript -->
<script src="{{ asset('admin_assets/vendors/bower_components/owl.carousel/dist/owl.carousel.min.js') }}"></script>

<!-- ChartJS JavaScript -->
<script src="{{ asset('admin_assets/vendors/chart.js/Chart.min.js') }}"></script>

<!-- Morris Charts JavaScript -->
<script src="{{ asset('admin_assets/vendors/bower_components/raphael/raphael.min.js') }}"></script>
<script src="{{ asset('admin_assets/vendors/bower_components/morris.js/morris.min.js') }}"></script>
<script src="{{ asset('admin_assets/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js') }}"></script>

<!-- Switchery JavaScript -->
<script src="{{ asset('admin_assets/vendors/bower_components/switchery/dist/switchery.min.js') }}"></script>

<!-- Init JavaScript -->
<script src="{{ asset('admin_assets/full-width-light-dist/js/init.js') }}"></script>
<script src="{{ asset('admin_assets/full-width-light-dist/js/dashboard-data.js') }}"></script>
</html>
