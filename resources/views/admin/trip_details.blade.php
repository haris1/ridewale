<?php use Carbon\Carbon; ?>
@extends('admin.layouts.admin-app')
<style type="text/css">
b, strong {
    font-weight: 700 !important;
}
li {
    padding-bottom: 10px !important;
}
.success{
    color:#0DC07F !important;    
}
.danger{
    color:#DC143C !important;
}
.info{
    color:#FA942D !important;   
}

</style>
@section('content')
<div class="adminright_detlisdivbox">
    <div class="setting_detlisdivbox">
        <div class="ordersbooked_detlis">
            <h4>TRIP#{{ $trip->id }}</h4>
            <P>Trip Status:
                @if($trip->status == 'scheduled')
                <span> Scheduled </span>
                @elseif($trip->status == 'started')
                <span> Started </span>
                @elseif($trip->status == 'completed')
                <span> Completed </span>
                @elseif($trip->status == 'cancelled')
                <span class="text-danger"> Cancelled by driver </span>
                @endif
                
            </P>
            @if($trip->status == 'cancelled')
            <p>Reason For Cancellation : 
                {{$trip->cancelled_reason}}
            </p>
            @endif

        </div>
    </div>
    <div class="bookingdetlis_text">
        <h4>Booking Details</h4>
        <?php $driver_penalty = false; ?>
        @forelse ($trip->bookings as $booking)
        <p>
            <a href="{{ route('admin.orderDetails',$booking->id) }}">
                <span>BOOKING ID #{{ $booking->id }}</span>
            </a>
        </p>
        <p>Booked By: {{ $booking->user->first_name .' '.$booking->user->last_name }}
            <a href="{{ route('admin.users.show',$booking->user->id) }}"> <span>(View Profile)</span></a>
        </p>
        <p>Booked Date: {{ $booking->created_date.' MDT' }}</p>
        <p>Seats Booked: {{ $booking->seats }}</p>
        <p>Booking Status : 
            @if($booking->status == 'pending')
            <span> Pending </span>
            @elseif($booking->status == 'confirmed')
            <span> Confirmed </span>
            @elseif($booking->status == 'cancelled')
            <span class="text-danger"> Cancelled by rider </span>
            @elseif($booking->status == 'picked_up')
            <span > Picked up </span>
            @elseif($booking->status == 'no_show')
            <span class="text-danger"> No Show </span>
            @elseif($booking->status == 'drop_off')
            <span> Dropped Off </span>
            @elseif($booking->status == 'rejected')
            <span class="text-danger"> Rejected </span>
            @endif
        </p>
        @if($booking->status == 'cancelled')
        <p>Reason For Cancellation : 
            {{$booking->cancellation_reason}}
        </p>
        @endif
        @if($booking->status == 'rejected')
        <p>Reason For Decline : 
            {{$booking->decline_reason}}
        </p>
        @endif

        <p>Payment Status : 
            <?php
            $payment_status = 'Pending';
            if($booking->payment != ''){
                $payment_status = 'Processed';
            }

            if($booking->status == 'cancelled' ||  $booking->trip->status == 'cancelled'){
                if($booking->payment['refunded_id'] == ''){
                    $payment_status = 'Refund Pending';
                } else {
                    $payment_status = 'Refunded';
                }
            }

            if($booking->status == 'rejected' && $booking->payment == ''){
                $payment_status = 'Not Charged';
            }

            echo $payment_status;
            ?>
            @if($booking->driver_not_show_at != '' && $booking->payment['refunded_id'] == '' && $booking->trip->dropoff_time < Carbon::today() && $booking->trip->status == 'scheduled')
            <?php $driver_penalty = true; ?>
            <a href="{{ route('admin.trip.giveBookingRefund',$booking->id) }}"> <span>Send Refund</span></a>             @endif
        </p>
        <br>

        @empty
        <p>No data found</p>
        @endforelse

    </div>
    @if($driver_penalty && $trip->driver_penalties == '')
    <div class="ridechimpfeessave_butten" style="width: 55%;padding-top: 20px;">
        <button type="button" id="charge_driver">ADD PENALTY TO DRIVER WALLET</button>
    </div>  
    @endif
    <div class="Tripdetlis_mandivtextbox">
        <div class="Tripdetlis_textbox">
            <h4>Trip Datails</h4>
            <p>Travel Date: {{ $trip->trip_dated }}</p>
            <p>Price / seat: ${{ number_format($trip->price,2) }}</p>
        </div>
        <div class="mandivsteetleft_textbox">
            <div class="steetleft_textbox">
                <h4>From</h4>
                <h3>{{ $trip->from_location->work_city->name }}</h3>
                <p>{{ $trip->from_location->name }}</p>
            </div>
            <div class="distancearrow_div">
                <img src="{{ asset('admin_assets/svg/right-arrow.svg') }}">
            </div>
            <div class="steetleft_textbox">
                <h4>to</h4>
                <h3>{{ $trip->to_location->work_city->name }}</h3>
                <p>{{ $trip->to_location->name }}</p>
            </div>
        </div>
        <div class="mandivsteetleft_textbox">
            <div class="pickuploction_detlis">
                <p>Pick-up location & Time</p>
                <h4><!-- {{ wordwrap($trip->from_location->name,40,'\n') }} -->
                    <?php
                    echo wordwrap($trip->from_location->name,40,'<br>');
                    ?>
                    <a href="http://maps.google.com/?q={{ $trip->from_location->latitude.','.$trip->from_location->longitude }}" target="_blank"><span>(View on Map)</span></a>
                </h4>
                <h4>{{ $trip->picked_uptime.' MDT on '. $trip->trip_date->format('d-M-Y') }}</h4>

            </div>
        </div>
        <div class="mandivsteetleft_textbox">
            <div class="pickuploction_detlis">
                <p>Drop-off location:</p>
                <h4><!-- {{ $trip->to_location->name }} -->
                    <?php
                    echo wordwrap($trip->to_location->name,40,'<br>');
                    ?>
                    <a href="http://maps.google.com/?q={{ $trip->to_location->latitude.','.$trip->to_location->longitude }}" target="_blank">
                        <span>(View on Map)</span>
                    </a>
                </h4>
                <h4>{{$trip->dropoff_on}}</h4>

            </div>
        </div>
    </div> 
    @if($html != '')
    <div class="Tripdetlis_mandivtextbox">
        <div class="Tripdetlis_textbox">
            <h4>Trip Activity</h4>
        </div>

        <div class="mandivsteetleft_textbox">
            <div class="pickuploction_detlis">
                <ul>
                 {!!$html!!}

             </ul>
         </div>
     </div>
 </div>
 @endif
</div> 
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $('#charge_driver').click(function(e) {
        location.replace('{{ route('admin.trip.chargeDriver',$trip->id) }}')
    })
</script>
@endsection