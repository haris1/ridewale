  <!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Chat App</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.2.0/socket.io.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/additional-methods.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io-stream/0.9.1/socket.io-stream.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/BrowserFS/2.0.0/browserfs.js"></script>
</head>
<body>
  	<style>
  		.identityDiv{
  	    width: 15%;
    border-radius: 6px;
    background: #fff;
    padding: 11px;
    margin-left: 43%;
    margin-bottom: 27px;
  }
  	</style>
  	<form id="identityForm">
  		<div class="col-md-4 offset-md-4 mt-4 identityDiv">
  			<div class="card">
  				<div class="card-body">
  					<div class="form-group">
  						<label for="name">Your Name</label>
  						<input type="text" name="name" class="form-control" id="name" placeholder="Enter Your Name">
  					</div>
  					<div class="form-group">
  						<label for="user_id">Your ID</label>
  						<input type="text" name="user_id" class="form-control" id="user_id" placeholder="Enter Your ID">
  					</div>
  					<button type="subtmi" class="btn btn-primary">SUBMIT</button>
  				</div>
  			</div>
  		</div>
  	</form>
  <div class="container clearfix chatMania">
  </div> <!-- end container -->

<input type="text" id="writeMessage">
<div id="typing">
	<span></span>
</div>
<script>

	let userName = "";
	let userId = "";
	// socket = io();
	$('#identityForm').validate({
		rules:{
			name:{
				required:true,
			},
			user_id:{
				required:true,
				digits:true,
			}
		},
		submitHandler: function(form){

			userName = $('input[name=name]').val();
			userId = $('input[name=user_id]').val();
			socket = io('http://foremostdigitalcloud.ca:4002',{query:"user_id="+userId+"&user_name="+userName});
		    
		    // siofu = new SocketIOFileUpload(socket);
		    // siofu.listenOnInput(document.getElementById("siofu_input"));

			$('#identityForm').remove();
			socket.on('connect',() => {
				console.log(socket.id);

			});

			socket.on('wantToCatchVideoBrowser', (data) => {
				alert('chimnp');
			})

			socket.on('typing_response', (data) => {
				console.log(data);
				console.log('data');

				$('#typing span').html(data.message);
			})

			socket.on('typing_stops_response', (data) => {
				console.log(data);
				console.log('data1');
				
				$('#typing span').html('');
			})

			socket.on('image', (data) => {
				alert();
				console.log('data');
				console.log(data);
			});

			
			socket.on('catchLiveStream', (data) => {
				console.log('event occures -->');
				console.log(data);
			})

			socket.on('catchLiveStream', (data) => {
			})

			socket.on('message_found', (data) => {
				console.log(data);
			});
		}
	});

	$(function() {

		$('#siofu_input').change(function(e) {
			// var file = e.target.files[0];
			// var stream = ss.createStream();
			file = e.target.files[0];
			stream = ss.createStream();

		    // upload a file to the server.
		    ss(socket).emit('filestream', stream, {size: file.size, name:file.name});
		    ss.createBlobReadStream(file).pipe(stream);

		    var blobStream = ss.createBlobReadStream(file);
		    var size = 0;
		    var uploadedSize;
		    blobStream.on('data', function(chunk) {
		    	size += chunk.length;
		    	uploadedSize = Math.floor(size / file.size * 100)
		    	$('#alerts').html(uploadedSize + '%');
		    	console.log(uploadedSize + '%');
		    	if (uploadedSize == 100) {
		    		console.log("inside uploadedSize");
		    		// socket.emit('wantToCatchVideo');
		    	}
		    });

		    blobStream.pipe(stream);
		});
	});

	$('#submitMessage').on('click', (e) => {
		let files = $('input[name=files]');
		let filesVal = $('input[name=files]').val();
		let message = $('input[name=message]').val();
		if (filesVal) {
			
		    // Do something when a file is in progress:
		    // siofu.addEventListener("progress", function(event){
		    //     var percent = event.bytesLoaded / event.file.size * 100;
		    //     console.log("File is", percent.toFixed(2), "percent loaded");
		    // });
		 
		    // Do something when a file is uploaded:
		    // siofu.addEventListener("complete", function(event){
		    //     console.log(event.success);
		    //     console.log(event.file);
		    // });

		    // console.log(siofu);

			socket.emit('message', {message_type:'message',message:message});
		}else{
			socket.emit('message', {message_type:'message',message:message});
		}
	});

	$('#writeMessage').on('keypress', () => {
		socket.emit('typing',{user_id:2})
	});

	$('#writeMessage').on('keyup', () => {
		socket.emit('typing_stops',{user_id:2})
	});

	$('#upload_btn').on('click', function(){
		socket.emit('showvideo');
	});
// var binaryData = [];
// 	ss(socket).on('wantToCatchVideoBrowser', function(stream, data, callback) {
// binaryData.push(stream);
// 		console.log(stream);		
// 		console.log(data);		
// 		// document.getElementById('videoss').src = window.URL.createObjectURL(stream);
// 		document.getElementById('videoss').src = window.URL.createObjectURL(new Blob(binaryData))
// 	});

// socket.on('wantToCatchVideoBrowser', function(stream) {
// 	console.log(data);		
// });
</script>
</body>
    <script src="//cdnjs.cloudflare.com/ajax/libs/handlebars.js/3.0.0/handlebars.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/list.js/1.1.1/list.min.js"></script>
</html>