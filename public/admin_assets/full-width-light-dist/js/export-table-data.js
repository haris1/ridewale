/*Export Table Init*/

"use strict"; 

$(document).ready(function() {
	$('#example').DataTable( {
		dom: 'Bfrtip',
		buttons: [
			'copy', 'csv', 'excel', 'pdf', 'print'
		],
		"processing": true,
        "serverSide": true,
        "ajax": "../server_side/scripts/server_processing.php"
	} );
} );